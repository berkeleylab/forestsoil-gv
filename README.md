### Supporting data for  

## Hidden diversity of soil gv 
  
_Frederik Schulz, Lauren Altei, Danielle Goudeau, Elizabeth M. Ryan, Feiqiao B. Yu, Rex R. Malmstrom, Jeffrey Blanchard, Tanja Woyke_

This repository contains all phylogenetic trees and underlying sequence alignments described in the paper. Genomes of soil gv are provided in this repository and will soon be available at GenBank.

Please contact Frederik Schulz (fschulz@lbl.gov) for any questions regarding the data and their use.
