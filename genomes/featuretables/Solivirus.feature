>Solivirus_1
124	5	CDS
			locus_tag	Solivirus_1_1
			product	hypothetical protein
			transl_table	11
746	168	CDS
			locus_tag	Solivirus_1_2
			product	hypothetical protein
			transl_table	11
877	1110	CDS
			locus_tag	Solivirus_1_3
			product	hypothetical protein
			transl_table	11
1421	1206	CDS
			locus_tag	Solivirus_1_4
			product	hypothetical protein
			transl_table	11
1699	1911	CDS
			locus_tag	Solivirus_1_5
			product	hypothetical protein
			transl_table	11
1978	2169	CDS
			locus_tag	Solivirus_1_6
			product	hypothetical protein
			transl_table	11
2257	2838	CDS
			locus_tag	Solivirus_1_7
			product	hypothetical protein
			transl_table	11
2893	3462	CDS
			locus_tag	Solivirus_1_8
			product	hypothetical protein
			transl_table	11
3517	4245	CDS
			locus_tag	Solivirus_1_9
			product	hypothetical protein
			transl_table	11
4310	5761	CDS
			locus_tag	Solivirus_1_10
			product	FAD-binding oxidoreductase
			transl_table	11
5796	6854	CDS
			locus_tag	Solivirus_1_11
			product	hypothetical protein
			transl_table	11
6851	7018	CDS
			locus_tag	Solivirus_1_12
			product	hypothetical protein
			transl_table	11
7590	7006	CDS
			locus_tag	Solivirus_1_13
			product	hypothetical protein
			transl_table	11
7659	8255	CDS
			locus_tag	Solivirus_1_14
			product	hypothetical protein
			transl_table	11
8897	8379	CDS
			locus_tag	Solivirus_1_15
			product	GTP-binding nuclear protein RAN
			transl_table	11
9069	9392	CDS
			locus_tag	Solivirus_1_16
			product	hypothetical protein
			transl_table	11
9882	9430	CDS
			locus_tag	Solivirus_1_17
			product	hypothetical protein
			transl_table	11
10017	10688	CDS
			locus_tag	Solivirus_1_18
			product	hypothetical protein
			transl_table	11
13166	10803	CDS
			locus_tag	Solivirus_1_19
			product	hypothetical protein
			transl_table	11
14382	13234	CDS
			locus_tag	Solivirus_1_20
			product	Collagen triple helix repeat protein
			transl_table	11
16777	14450	CDS
			locus_tag	Solivirus_1_21
			product	hypothetical protein BRC91_06685
			transl_table	11
17124	16774	CDS
			locus_tag	Solivirus_1_22
			product	hypothetical protein
			transl_table	11
20833	17717	CDS
			locus_tag	Solivirus_1_23
			product	hypothetical protein B6247_21185
			transl_table	11
20924	21313	CDS
			locus_tag	Solivirus_1_24
			product	hypothetical protein
			transl_table	11
21689	21336	CDS
			locus_tag	Solivirus_1_25
			product	Archaea-specific enzyme related to ProFAR isomerase
			transl_table	11
22604	21753	CDS
			locus_tag	Solivirus_1_26
			product	hypothetical protein
			transl_table	11
23419	22601	CDS
			locus_tag	Solivirus_1_27
			product	hypothetical protein
			transl_table	11
23452	24318	CDS
			locus_tag	Solivirus_1_28
			product	hypothetical protein
			transl_table	11
24453	25262	CDS
			locus_tag	Solivirus_1_29
			product	hypothetical protein
			transl_table	11
25304	25624	CDS
			locus_tag	Solivirus_1_30
			product	hypothetical protein
			transl_table	11
26830	25646	CDS
			locus_tag	Solivirus_1_31
			product	hypothetical protein
			transl_table	11
26917	27345	CDS
			locus_tag	Solivirus_1_32
			product	hypothetical protein
			transl_table	11
27363	27458	CDS
			locus_tag	Solivirus_1_33
			product	hypothetical protein
			transl_table	11
27445	28674	CDS
			locus_tag	Solivirus_1_34
			product	hypothetical protein
			transl_table	11
29557	28703	CDS
			locus_tag	Solivirus_1_35
			product	hypothetical protein
			transl_table	11
30714	29704	CDS
			locus_tag	Solivirus_1_36
			product	hypothetical protein
			transl_table	11
30842	31150	CDS
			locus_tag	Solivirus_1_37
			product	hypothetical protein
			transl_table	11
31197	32003	CDS
			locus_tag	Solivirus_1_38
			product	hypothetical protein
			transl_table	11
32439	32110	CDS
			locus_tag	Solivirus_1_39
			product	hypothetical protein
			transl_table	11
32513	33337	CDS
			locus_tag	Solivirus_1_40
			product	hypothetical protein
			transl_table	11
35125	33422	CDS
			locus_tag	Solivirus_1_41
			product	putative pyridoxal phosphate-dependent transferase
			transl_table	11
36583	35195	CDS
			locus_tag	Solivirus_1_42
			product	hypothetical protein
			transl_table	11
37279	36737	CDS
			locus_tag	Solivirus_1_43
			product	hypothetical protein
			transl_table	11
37333	38121	CDS
			locus_tag	Solivirus_1_44
			product	resolvase
			transl_table	11
38388	39317	CDS
			locus_tag	Solivirus_1_45
			product	hypothetical protein
			transl_table	11
39781	39356	CDS
			locus_tag	Solivirus_1_46
			product	cytidine and deoxycytidylate deaminase
			transl_table	11
40479	39817	CDS
			locus_tag	Solivirus_1_47
			product	hypothetical protein
			transl_table	11
40556	41071	CDS
			locus_tag	Solivirus_1_48
			product	hypothetical protein
			transl_table	11
41781	41107	CDS
			locus_tag	Solivirus_1_49
			product	cupin
			transl_table	11
41828	43681	CDS
			locus_tag	Solivirus_1_50
			product	hypothetical protein
			transl_table	11
44519	43719	CDS
			locus_tag	Solivirus_1_51
			product	hypothetical protein
			transl_table	11
44590	45222	CDS
			locus_tag	Solivirus_1_52
			product	FkbM methyltransferase
			transl_table	11
45297	45695	CDS
			locus_tag	Solivirus_1_53
			product	hypothetical protein
			transl_table	11
46496	45717	CDS
			locus_tag	Solivirus_1_54
			product	hypothetical protein
			transl_table	11
46578	49196	CDS
			locus_tag	Solivirus_1_55
			product	DNA double-stranded break repair ATPase
			transl_table	11
50113	49598	CDS
			locus_tag	Solivirus_1_56
			product	hypothetical protein
			transl_table	11
51040	50210	CDS
			locus_tag	Solivirus_1_57
			product	hypothetical protein pv_195
			transl_table	11
52578	51058	CDS
			locus_tag	Solivirus_1_58
			product	VV A18-like helicase
			transl_table	11
52703	53446	CDS
			locus_tag	Solivirus_1_59
			product	hypothetical protein
			transl_table	11
53526	54389	CDS
			locus_tag	Solivirus_1_60
			product	hypothetical protein
			transl_table	11
54418	55065	CDS
			locus_tag	Solivirus_1_61
			product	hypothetical protein
			transl_table	11
55155	55592	CDS
			locus_tag	Solivirus_1_62
			product	hypothetical protein
			transl_table	11
56523	55756	CDS
			locus_tag	Solivirus_1_63
			product	hypothetical protein
			transl_table	11
57058	56567	CDS
			locus_tag	Solivirus_1_64
			product	hypothetical protein
			transl_table	11
57369	57148	CDS
			locus_tag	Solivirus_1_65
			product	hypothetical protein
			transl_table	11
57424	59319	CDS
			locus_tag	Solivirus_1_66
			product	hypothetical protein
			transl_table	11
59343	59885	CDS
			locus_tag	Solivirus_1_67
			product	structure-specific endonuclease subunit slx1
			transl_table	11
60662	59988	CDS
			locus_tag	Solivirus_1_68
			product	hypothetical protein
			transl_table	11
60739	62253	CDS
			locus_tag	Solivirus_1_69
			product	Pox-VLTF3-like transcription factor
			transl_table	11
62456	63640	CDS
			locus_tag	Solivirus_1_70
			product	hypothetical protein
			transl_table	11
64207	63788	CDS
			locus_tag	Solivirus_1_71
			product	hypothetical protein
			transl_table	11
64270	65592	CDS
			locus_tag	Solivirus_1_72
			product	hypothetical protein
			transl_table	11
65652	66035	CDS
			locus_tag	Solivirus_1_73
			product	hypothetical protein
			transl_table	11
66769	66410	CDS
			locus_tag	Solivirus_1_74
			product	hypothetical protein
			transl_table	11
67108	66800	CDS
			locus_tag	Solivirus_1_75
			product	hypothetical protein
			transl_table	11
67189	67533	CDS
			locus_tag	Solivirus_1_76
			product	hypothetical protein
			transl_table	11
67564	68100	CDS
			locus_tag	Solivirus_1_77
			product	hypothetical protein
			transl_table	11
68283	68672	CDS
			locus_tag	Solivirus_1_78
			product	hypothetical protein
			transl_table	11
68639	69370	CDS
			locus_tag	Solivirus_1_79
			product	hypothetical protein
			transl_table	11
69459	70283	CDS
			locus_tag	Solivirus_1_80
			product	hypothetical protein
			transl_table	11
71465	70329	CDS
			locus_tag	Solivirus_1_81
			product	hypothetical protein
			transl_table	11
71516	72154	CDS
			locus_tag	Solivirus_1_82
			product	hypothetical protein
			transl_table	11
72195	72755	CDS
			locus_tag	Solivirus_1_83
			product	hypothetical protein
			transl_table	11
72810	73448	CDS
			locus_tag	Solivirus_1_84
			product	hypothetical protein
			transl_table	11
74243	73476	CDS
			locus_tag	Solivirus_1_85
			product	hypothetical protein
			transl_table	11
74323	74613	CDS
			locus_tag	Solivirus_1_86
			product	hypothetical protein
			transl_table	11
>Solivirus_2
43344	43416	tRNA	Solivirus_tRNA_1
			product	tRNA-Ile(TAT)
43243	43315	tRNA	Solivirus_tRNA_2
			product	tRNA-Arg(TCT)
135	1	CDS
			locus_tag	Solivirus_2_1
			product	hypothetical protein
			transl_table	11
180	1568	CDS
			locus_tag	Solivirus_2_2
			product	hypothetical protein Klosneuvirus_2_206
			transl_table	11
1695	2336	CDS
			locus_tag	Solivirus_2_3
			product	hypothetical protein PBCVCviKI_066L
			transl_table	11
2397	3389	CDS
			locus_tag	Solivirus_2_4
			product	GDP-mannose 4,6-dehydratase
			transl_table	11
3419	4249	CDS
			locus_tag	Solivirus_2_5
			product	hypothetical protein
			transl_table	11
5287	4271	CDS
			locus_tag	Solivirus_2_6
			product	dTDP-D-glucose 4,6-dehydratase
			transl_table	11
6330	5317	CDS
			locus_tag	Solivirus_2_7
			product	hypothetical protein
			transl_table	11
6389	7429	CDS
			locus_tag	Solivirus_2_8
			product	hypothetical protein COA94_02120
			transl_table	11
9393	7642	CDS
			locus_tag	Solivirus_2_9
			product	hypothetical protein
			transl_table	11
10042	9440	CDS
			locus_tag	Solivirus_2_10
			product	hypothetical protein
			transl_table	11
10085	10408	CDS
			locus_tag	Solivirus_2_11
			product	hypothetical protein
			transl_table	11
11192	10413	CDS
			locus_tag	Solivirus_2_12
			product	hypothetical protein
			transl_table	11
12058	11189	CDS
			locus_tag	Solivirus_2_13
			product	hypothetical protein
			transl_table	11
12147	12947	CDS
			locus_tag	Solivirus_2_14
			product	hypothetical protein
			transl_table	11
13202	12984	CDS
			locus_tag	Solivirus_2_15
			product	hypothetical protein
			transl_table	11
13499	13272	CDS
			locus_tag	Solivirus_2_16
			product	hypothetical protein
			transl_table	11
14636	13548	CDS
			locus_tag	Solivirus_2_17
			product	hypothetical protein
			transl_table	11
14660	15241	CDS
			locus_tag	Solivirus_2_18
			product	hypothetical protein
			transl_table	11
15335	15529	CDS
			locus_tag	Solivirus_2_19
			product	hypothetical protein
			transl_table	11
16173	15568	CDS
			locus_tag	Solivirus_2_20
			product	hypothetical protein
			transl_table	11
16690	16220	CDS
			locus_tag	Solivirus_2_21
			product	hypothetical protein
			transl_table	11
16743	17555	CDS
			locus_tag	Solivirus_2_22
			product	FkbM family methyltransferase
			transl_table	11
17978	17565	CDS
			locus_tag	Solivirus_2_23
			product	hypothetical protein
			transl_table	11
18010	19152	CDS
			locus_tag	Solivirus_2_24
			product	hypothetical protein
			transl_table	11
19607	19185	CDS
			locus_tag	Solivirus_2_25
			product	hypothetical protein
			transl_table	11
19676	20647	CDS
			locus_tag	Solivirus_2_26
			product	hypothetical protein
			transl_table	11
20804	21859	CDS
			locus_tag	Solivirus_2_27
			product	hypothetical protein
			transl_table	11
21914	22825	CDS
			locus_tag	Solivirus_2_28
			product	hypothetical protein Klosneuvirus_3_129
			transl_table	11
24193	22853	CDS
			locus_tag	Solivirus_2_29
			product	hypothetical protein
			transl_table	11
24297	25214	CDS
			locus_tag	Solivirus_2_30
			product	hypothetical protein
			transl_table	11
25644	25931	CDS
			locus_tag	Solivirus_2_31
			product	hypothetical protein
			transl_table	11
30036	26491	CDS
			locus_tag	Solivirus_2_32
			product	DNA-directed RNA polymerase subunit RPB2
			transl_table	11
31615	30155	CDS
			locus_tag	Solivirus_2_33
			product	hypothetical protein
			transl_table	11
31681	32550	CDS
			locus_tag	Solivirus_2_34
			product	hypothetical protein
			transl_table	11
33170	32592	CDS
			locus_tag	Solivirus_2_35
			product	unnamed protein product
			transl_table	11
33297	34127	CDS
			locus_tag	Solivirus_2_36
			product	hypothetical protein
			transl_table	11
37376	34197	CDS
			locus_tag	Solivirus_2_37
			product	DNA-directed RNA polymerase subunit RPB1
			transl_table	11
37445	38752	CDS
			locus_tag	Solivirus_2_38
			product	hypothetical protein
			transl_table	11
38907	39740	CDS
			locus_tag	Solivirus_2_39
			product	hypothetical protein
			transl_table	11
39862	40110	CDS
			locus_tag	Solivirus_2_40
			product	hypothetical protein
			transl_table	11
41045	40347	CDS
			locus_tag	Solivirus_2_41
			product	hypothetical protein
			transl_table	11
41114	41992	CDS
			locus_tag	Solivirus_2_42
			product	hypothetical protein
			transl_table	11
42002	42958	CDS
			locus_tag	Solivirus_2_43
			product	uncharacterized protein LOC111246002
			transl_table	11
44478	43537	CDS
			locus_tag	Solivirus_2_44
			product	hypothetical protein
			transl_table	11
44896	44558	CDS
			locus_tag	Solivirus_2_45
			product	hypothetical protein
			transl_table	11
45019	45363	CDS
			locus_tag	Solivirus_2_46
			product	hypothetical protein
			transl_table	11
46017	45355	CDS
			locus_tag	Solivirus_2_47
			product	hypothetical protein
			transl_table	11
46056	47366	CDS
			locus_tag	Solivirus_2_48
			product	hypothetical protein
			transl_table	11
47813	47388	CDS
			locus_tag	Solivirus_2_49
			product	hypothetical protein
			transl_table	11
49412	47880	CDS
			locus_tag	Solivirus_2_50
			product	AAA family ATPase
			transl_table	11
49499	50374	CDS
			locus_tag	Solivirus_2_51
			product	hypothetical protein
			transl_table	11
50388	50771	CDS
			locus_tag	Solivirus_2_52
			product	hypothetical protein
			transl_table	11
51290	50814	CDS
			locus_tag	Solivirus_2_53
			product	hypothetical protein
			transl_table	11
52210	51413	CDS
			locus_tag	Solivirus_2_54
			product	hypothetical protein
			transl_table	11
53151	52360	CDS
			locus_tag	Solivirus_2_55
			product	hypothetical protein
			transl_table	11
53545	53297	CDS
			locus_tag	Solivirus_2_56
			product	hypothetical protein
			transl_table	11
53829	53614	CDS
			locus_tag	Solivirus_2_57
			product	hypothetical protein
			transl_table	11
54263	53907	CDS
			locus_tag	Solivirus_2_58
			product	hypothetical protein A2X59_05265
			transl_table	11
54846	54412	CDS
			locus_tag	Solivirus_2_59
			product	hypothetical protein
			transl_table	11
55464	54994	CDS
			locus_tag	Solivirus_2_60
			product	putative ribonuclease H1-like
			transl_table	11
55543	56106	CDS
			locus_tag	Solivirus_2_61
			product	hypothetical protein
			transl_table	11
57052	56135	CDS
			locus_tag	Solivirus_2_62
			product	hypothetical protein
			transl_table	11
57072	58235	CDS
			locus_tag	Solivirus_2_63
			product	hypothetical protein
			transl_table	11
58237	59760	CDS
			locus_tag	Solivirus_2_64
			product	hypothetical protein CBC12_05255
			transl_table	11
63901	59792	CDS
			locus_tag	Solivirus_2_65
			product	VV D6-like helicase
			transl_table	11
65908	63962	CDS
			locus_tag	Solivirus_2_66
			product	helix-turn-helix domain of resolvase family protein (plasmid)
			transl_table	11
66107	66790	CDS
			locus_tag	Solivirus_2_67
			product	hypothetical protein
			transl_table	11
66824	67846	CDS
			locus_tag	Solivirus_2_68
			product	hypothetical protein
			transl_table	11
68726	67962	CDS
			locus_tag	Solivirus_2_69
			product	hypothetical protein
			transl_table	11
69193	68777	CDS
			locus_tag	Solivirus_2_70
			product	hypothetical protein
			transl_table	11
70714	69281	CDS
			locus_tag	Solivirus_2_71
			product	glycosyltransferase
			transl_table	11
>Solivirus_3
2	475	CDS
			locus_tag	Solivirus_3_1
			product	hypothetical protein
			transl_table	11
528	1334	CDS
			locus_tag	Solivirus_3_2
			product	hypothetical protein ISTM_22
			transl_table	11
1384	1920	CDS
			locus_tag	Solivirus_3_3
			product	hypothetical protein
			transl_table	11
1964	2518	CDS
			locus_tag	Solivirus_3_4
			product	hypothetical protein
			transl_table	11
3156	2521	CDS
			locus_tag	Solivirus_3_5
			product	hypothetical protein
			transl_table	11
4820	3207	CDS
			locus_tag	Solivirus_3_6
			product	hypothetical protein
			transl_table	11
5884	4991	CDS
			locus_tag	Solivirus_3_7
			product	hypothetical protein
			transl_table	11
6130	6429	CDS
			locus_tag	Solivirus_3_8
			product	hypothetical protein
			transl_table	11
7096	6452	CDS
			locus_tag	Solivirus_3_9
			product	putative deoxynucleoside monophosphate kinase
			transl_table	11
7317	7162	CDS
			locus_tag	Solivirus_3_10
			product	hypothetical protein
			transl_table	11
7491	7363	CDS
			locus_tag	Solivirus_3_11
			product	hypothetical protein
			transl_table	11
8501	7647	CDS
			locus_tag	Solivirus_3_12
			product	hypothetical protein
			transl_table	11
9401	8556	CDS
			locus_tag	Solivirus_3_13
			product	Uracil-DNA glycosylase
			transl_table	11
9508	9600	CDS
			locus_tag	Solivirus_3_14
			product	hypothetical protein
			transl_table	11
9648	10439	CDS
			locus_tag	Solivirus_3_15
			product	hypothetical protein
			transl_table	11
10470	11276	CDS
			locus_tag	Solivirus_3_16
			product	hypothetical protein
			transl_table	11
11291	12727	CDS
			locus_tag	Solivirus_3_17
			product	hypothetical protein T552_02603
			transl_table	11
15080	12750	CDS
			locus_tag	Solivirus_3_18
			product	hypothetical protein COA94_02940
			transl_table	11
16647	15139	CDS
			locus_tag	Solivirus_3_19
			product	Hypothetical protein BRZCDTV_92
			transl_table	11
17414	16716	CDS
			locus_tag	Solivirus_3_20
			product	hypothetical protein
			transl_table	11
18051	17440	CDS
			locus_tag	Solivirus_3_21
			product	hypothetical protein
			transl_table	11
18202	21768	CDS
			locus_tag	Solivirus_3_22
			product	hypothetical protein
			transl_table	11
21911	22570	CDS
			locus_tag	Solivirus_3_23
			product	hypothetical protein A2451_01250
			transl_table	11
22630	23289	CDS
			locus_tag	Solivirus_3_24
			product	hypothetical protein
			transl_table	11
23330	24310	CDS
			locus_tag	Solivirus_3_25
			product	hypothetical protein
			transl_table	11
24344	25417	CDS
			locus_tag	Solivirus_3_26
			product	hypothetical protein
			transl_table	11
25490	25765	CDS
			locus_tag	Solivirus_3_27
			product	hypothetical protein
			transl_table	11
25787	25909	CDS
			locus_tag	Solivirus_3_28
			product	hypothetical protein
			transl_table	11
28144	26069	CDS
			locus_tag	Solivirus_3_29
			product	hypothetical protein KFL_010150030
			transl_table	11
28143	28865	CDS
			locus_tag	Solivirus_3_30
			product	hypothetical protein
			transl_table	11
28924	29658	CDS
			locus_tag	Solivirus_3_31
			product	hypothetical protein
			transl_table	11
31607	29709	CDS
			locus_tag	Solivirus_3_32
			product	hypothetical protein
			transl_table	11
31959	32558	CDS
			locus_tag	Solivirus_3_33
			product	hypothetical protein
			transl_table	11
32625	33593	CDS
			locus_tag	Solivirus_3_34
			product	hypothetical protein
			transl_table	11
37543	33995	CDS
			locus_tag	Solivirus_3_35
			product	topoisomerase IIA
			transl_table	11
37916	41755	CDS
			locus_tag	Solivirus_3_36
			product	alpha (1,3)-fucosyltransferase
			transl_table	11
41791	42066	CDS
			locus_tag	Solivirus_3_37
			product	hypothetical protein
			transl_table	11
42797	42102	CDS
			locus_tag	Solivirus_3_38
			product	hypothetical protein CBD38_00655
			transl_table	11
43222	42791	CDS
			locus_tag	Solivirus_3_39
			product	acetyltransferase
			transl_table	11
>Solivirus_4
480	1	CDS
			locus_tag	Solivirus_4_1
			product	hypothetical protein
			transl_table	11
1467	919	CDS
			locus_tag	Solivirus_4_2
			product	hypothetical protein
			transl_table	11
2078	1509	CDS
			locus_tag	Solivirus_4_3
			product	hypothetical protein
			transl_table	11
2864	2103	CDS
			locus_tag	Solivirus_4_4
			product	hypothetical protein
			transl_table	11
2927	4051	CDS
			locus_tag	Solivirus_4_5
			product	hypothetical protein
			transl_table	11
4437	4189	CDS
			locus_tag	Solivirus_4_6
			product	hypothetical protein
			transl_table	11
5593	4541	CDS
			locus_tag	Solivirus_4_7
			product	flap endonuclease 1
			transl_table	11
7251	5671	CDS
			locus_tag	Solivirus_4_8
			product	hypothetical protein
			transl_table	11
7442	8443	CDS
			locus_tag	Solivirus_4_9
			product	hypothetical protein
			transl_table	11
8493	9521	CDS
			locus_tag	Solivirus_4_10
			product	hypothetical protein
			transl_table	11
10130	9696	CDS
			locus_tag	Solivirus_4_11
			product	hypothetical protein
			transl_table	11
10949	10212	CDS
			locus_tag	Solivirus_4_12
			product	hypothetical protein
			transl_table	11
14563	11027	CDS
			locus_tag	Solivirus_4_13
			product	DNA polymerase delta catalytic subunit
			transl_table	11
14617	15387	CDS
			locus_tag	Solivirus_4_14
			product	hypothetical protein
			transl_table	11
15413	16534	CDS
			locus_tag	Solivirus_4_15
			product	hypothetical protein
			transl_table	11
16574	17329	CDS
			locus_tag	Solivirus_4_16
			product	hypothetical protein
			transl_table	11
17373	18116	CDS
			locus_tag	Solivirus_4_17
			product	hypothetical protein
			transl_table	11
18188	19174	CDS
			locus_tag	Solivirus_4_18
			product	hypothetical protein
			transl_table	11
20976	19345	CDS
			locus_tag	Solivirus_4_19
			product	hypothetical protein pv_465
			transl_table	11
21053	22348	CDS
			locus_tag	Solivirus_4_20
			product	ATP-dependent DNA ligase
			transl_table	11
22567	22926	CDS
			locus_tag	Solivirus_4_21
			product	hypothetical protein
			transl_table	11
23104	24942	CDS
			locus_tag	Solivirus_4_22
			product	putative HNH endonuclease
			transl_table	11
25005	25832	CDS
			locus_tag	Solivirus_4_23
			product	hypothetical protein
			transl_table	11
27355	25865	CDS
			locus_tag	Solivirus_4_24
			product	glycosyltransferase
			transl_table	11
27386	28069	CDS
			locus_tag	Solivirus_4_25
			product	hypothetical protein
			transl_table	11
28114	29094	CDS
			locus_tag	Solivirus_4_26
			product	DHH family phosphohydrolase-like protein
			transl_table	11
29114	30028	CDS
			locus_tag	Solivirus_4_27
			product	hypothetical protein
			transl_table	11
31641	30508	CDS
			locus_tag	Solivirus_4_28
			product	hypothetical protein
			transl_table	11
32574	31996	CDS
			locus_tag	Solivirus_4_29
			product	hypothetical protein
			transl_table	11
32682	33353	CDS
			locus_tag	Solivirus_4_30
			product	hypothetical protein pv_445
			transl_table	11
34616	33396	CDS
			locus_tag	Solivirus_4_31
			product	hypothetical protein
			transl_table	11
37765	34835	CDS
			locus_tag	Solivirus_4_32
			product	hypothetical protein
			transl_table	11
38096	37803	CDS
			locus_tag	Solivirus_4_33
			product	hypothetical protein
			transl_table	11
>Solivirus_5
2	580	CDS
			locus_tag	Solivirus_5_1
			product	hypothetical protein
			transl_table	11
1249	608	CDS
			locus_tag	Solivirus_5_2
			product	hypothetical protein
			transl_table	11
1419	1967	CDS
			locus_tag	Solivirus_5_3
			product	DNA-directed RNA polymerases I, II, and III 23 kDa, putative
			transl_table	11
2029	3279	CDS
			locus_tag	Solivirus_5_4
			product	hypothetical protein
			transl_table	11
3326	4807	CDS
			locus_tag	Solivirus_5_5
			product	hypothetical protein
			transl_table	11
4847	5293	CDS
			locus_tag	Solivirus_5_6
			product	hypothetical protein
			transl_table	11
5351	9292	CDS
			locus_tag	Solivirus_5_7
			product	VETF-like early transcription factor large subunit
			transl_table	11
9394	9957	CDS
			locus_tag	Solivirus_5_8
			product	hypothetical protein
			transl_table	11
10835	10047	CDS
			locus_tag	Solivirus_5_9
			product	hypothetical protein
			transl_table	11
11166	10882	CDS
			locus_tag	Solivirus_5_10
			product	hypothetical protein
			transl_table	11
11192	12034	CDS
			locus_tag	Solivirus_5_11
			product	hypothetical protein
			transl_table	11
12158	12006	CDS
			locus_tag	Solivirus_5_12
			product	hypothetical protein
			transl_table	11
14192	12501	CDS
			locus_tag	Solivirus_5_13
			product	putative polynucleotide kinase
			transl_table	11
14726	14289	CDS
			locus_tag	Solivirus_5_14
			product	hypothetical protein
			transl_table	11
15418	14753	CDS
			locus_tag	Solivirus_5_15
			product	hypothetical protein
			transl_table	11
15564	16154	CDS
			locus_tag	Solivirus_5_16
			product	hypothetical protein
			transl_table	11
17571	16246	CDS
			locus_tag	Solivirus_5_17
			product	hypothetical protein
			transl_table	11
17984	17673	CDS
			locus_tag	Solivirus_5_18
			product	hypothetical protein
			transl_table	11
18708	18334	CDS
			locus_tag	Solivirus_5_19
			product	hypothetical protein
			transl_table	11
>Solivirus_6
1	984	CDS
			locus_tag	Solivirus_6_1
			product	hypothetical protein
			transl_table	11
1352	2596	CDS
			locus_tag	Solivirus_6_2
			product	hypothetical protein
			transl_table	11
2602	2880	CDS
			locus_tag	Solivirus_6_3
			product	hypothetical protein
			transl_table	11
3275	2877	CDS
			locus_tag	Solivirus_6_4
			product	hypothetical protein
			transl_table	11
4249	3308	CDS
			locus_tag	Solivirus_6_5
			product	hypothetical protein
			transl_table	11
4344	4649	CDS
			locus_tag	Solivirus_6_6
			product	hypothetical protein
			transl_table	11
6163	4697	CDS
			locus_tag	Solivirus_6_7
			product	AP-endonuclease
			transl_table	11
7439	6309	CDS
			locus_tag	Solivirus_6_8
			product	NUDIX hydrolase
			transl_table	11
7504	8718	CDS
			locus_tag	Solivirus_6_9
			product	hypothetical protein
			transl_table	11
9012	9893	CDS
			locus_tag	Solivirus_6_10
			product	hypothetical protein
			transl_table	11
11550	10189	CDS
			locus_tag	Solivirus_6_11
			product	7-methylguanosine mRNA capping enzyme
			transl_table	11
>Solivirus_7
3	482	CDS
			locus_tag	Solivirus_7_1
			product	hypothetical protein
			transl_table	11
502	1161	CDS
			locus_tag	Solivirus_7_2
			product	hypothetical protein
			transl_table	11
1198	1920	CDS
			locus_tag	Solivirus_7_3
			product	hypothetical protein CVT99_00205
			transl_table	11
2026	2619	CDS
			locus_tag	Solivirus_7_4
			product	alkylated DNA repair protein
			transl_table	11
3228	2671	CDS
			locus_tag	Solivirus_7_5
			product	hypothetical protein
			transl_table	11
3329	3640	CDS
			locus_tag	Solivirus_7_6
			product	hypothetical protein
			transl_table	11
3700	5184	CDS
			locus_tag	Solivirus_7_7
			product	hypothetical protein
			transl_table	11
5208	5627	CDS
			locus_tag	Solivirus_7_8
			product	hypothetical protein
			transl_table	11
5854	6825	CDS
			locus_tag	Solivirus_7_9
			product	hypothetical protein
			transl_table	11
7592	6807	CDS
			locus_tag	Solivirus_7_10
			product	hypothetical protein
			transl_table	11
8585	7653	CDS
			locus_tag	Solivirus_7_11
			product	hypothetical protein
			transl_table	11
8636	10813	CDS
			locus_tag	Solivirus_7_12
			product	Protein kinase
			transl_table	11
>Solivirus_8
105	1	CDS
			locus_tag	Solivirus_8_1
			product	hypothetical protein
			transl_table	11
95	1921	CDS
			locus_tag	Solivirus_8_2
			product	VV D5-like helicase
			transl_table	11
2383	2490	CDS
			locus_tag	Solivirus_8_3
			product	hypothetical protein
			transl_table	11
2492	4243	CDS
			locus_tag	Solivirus_8_4
			product	hypothetical protein
			transl_table	11
4709	4864	CDS
			locus_tag	Solivirus_8_5
			product	hypothetical protein
			transl_table	11
>Solivirus_9
1	1068	CDS
			locus_tag	Solivirus_9_1
			product	hypothetical protein
			transl_table	11
1195	1533	CDS
			locus_tag	Solivirus_9_2
			product	hypothetical protein
			transl_table	11
1557	2276	CDS
			locus_tag	Solivirus_9_3
			product	hypothetical protein
			transl_table	11
3203	2346	CDS
			locus_tag	Solivirus_9_4
			product	hypothetical protein
			transl_table	11
3248	3694	CDS
			locus_tag	Solivirus_9_5
			product	hypothetical protein
			transl_table	11
3870	3715	CDS
			locus_tag	Solivirus_9_6
			product	hypothetical protein
			transl_table	11
