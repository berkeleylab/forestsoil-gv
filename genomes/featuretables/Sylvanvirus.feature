>Sylvanvirus_1
104466	104536	tRNA	Sylvanvirus_tRNA_1
			product	tRNA-Gln(TTG)
865	2	CDS
			locus_tag	Sylvanvirus_1_1
			product	hypothetical protein
			transl_table	11
1788	949	CDS
			locus_tag	Sylvanvirus_1_2
			product	m7GpppN-mRNA hydrolase
			transl_table	11
2101	2814	CDS
			locus_tag	Sylvanvirus_1_3
			product	hypothetical protein
			transl_table	11
5288	3276	CDS
			locus_tag	Sylvanvirus_1_4
			product	hypothetical protein KFL_007020080
			transl_table	11
5741	6448	CDS
			locus_tag	Sylvanvirus_1_5
			product	hypothetical protein
			transl_table	11
7203	6538	CDS
			locus_tag	Sylvanvirus_1_6
			product	hypothetical protein
			transl_table	11
7939	7214	CDS
			locus_tag	Sylvanvirus_1_7
			product	hypothetical protein
			transl_table	11
8206	9447	CDS
			locus_tag	Sylvanvirus_1_8
			product	hypothetical protein
			transl_table	11
10627	9539	CDS
			locus_tag	Sylvanvirus_1_9
			product	hypothetical protein F443_15863
			transl_table	11
11318	14644	CDS
			locus_tag	Sylvanvirus_1_10
			product	hypothetical protein
			transl_table	11
15145	14801	CDS
			locus_tag	Sylvanvirus_1_11
			product	hypothetical protein
			transl_table	11
15144	15278	CDS
			locus_tag	Sylvanvirus_1_12
			product	hypothetical protein
			transl_table	11
16596	15286	CDS
			locus_tag	Sylvanvirus_1_13
			product	hypothetical protein
			transl_table	11
16781	17803	CDS
			locus_tag	Sylvanvirus_1_14
			product	hypothetical protein
			transl_table	11
17872	18177	CDS
			locus_tag	Sylvanvirus_1_15
			product	hypothetical protein
			transl_table	11
19047	18265	CDS
			locus_tag	Sylvanvirus_1_16
			product	2OG-FeII oxygenase
			transl_table	11
19421	19287	CDS
			locus_tag	Sylvanvirus_1_17
			product	hypothetical protein
			transl_table	11
19729	20037	CDS
			locus_tag	Sylvanvirus_1_18
			product	hypothetical protein
			transl_table	11
20356	20084	CDS
			locus_tag	Sylvanvirus_1_19
			product	hypothetical protein
			transl_table	11
20556	21863	CDS
			locus_tag	Sylvanvirus_1_20
			product	hypothetical protein
			transl_table	11
21905	22324	CDS
			locus_tag	Sylvanvirus_1_21
			product	hypothetical protein
			transl_table	11
22873	22550	CDS
			locus_tag	Sylvanvirus_1_22
			product	hypothetical protein
			transl_table	11
23153	23968	CDS
			locus_tag	Sylvanvirus_1_23
			product	Lysosomal Pro-X carboxypeptidase
			transl_table	11
24045	25307	CDS
			locus_tag	Sylvanvirus_1_24
			product	hypothetical protein GPECTOR_69g429
			transl_table	11
25374	26537	CDS
			locus_tag	Sylvanvirus_1_25
			product	hypothetical protein
			transl_table	11
27285	26572	CDS
			locus_tag	Sylvanvirus_1_26
			product	hypothetical protein
			transl_table	11
28555	27524	CDS
			locus_tag	Sylvanvirus_1_27
			product	hypothetical protein
			transl_table	11
28853	31996	CDS
			locus_tag	Sylvanvirus_1_28
			product	hypothetical protein
			transl_table	11
32764	32141	CDS
			locus_tag	Sylvanvirus_1_29
			product	hypothetical protein
			transl_table	11
33187	33456	CDS
			locus_tag	Sylvanvirus_1_30
			product	hypothetical protein
			transl_table	11
34376	34254	CDS
			locus_tag	Sylvanvirus_1_31
			product	hypothetical protein
			transl_table	11
34481	34636	CDS
			locus_tag	Sylvanvirus_1_32
			product	hypothetical protein
			transl_table	11
35795	34734	CDS
			locus_tag	Sylvanvirus_1_33
			product	hypothetical protein
			transl_table	11
36195	36659	CDS
			locus_tag	Sylvanvirus_1_34
			product	hypothetical protein
			transl_table	11
37196	36735	CDS
			locus_tag	Sylvanvirus_1_35
			product	hypothetical protein
			transl_table	11
37449	38150	CDS
			locus_tag	Sylvanvirus_1_36
			product	hypothetical protein
			transl_table	11
38784	38221	CDS
			locus_tag	Sylvanvirus_1_37
			product	hypothetical protein
			transl_table	11
39665	38892	CDS
			locus_tag	Sylvanvirus_1_38
			product	hypothetical protein
			transl_table	11
40537	39674	CDS
			locus_tag	Sylvanvirus_1_39
			product	hypothetical protein TRIADDRAFT_32617
			transl_table	11
42717	40897	CDS
			locus_tag	Sylvanvirus_1_40
			product	hypothetical protein
			transl_table	11
42950	43738	CDS
			locus_tag	Sylvanvirus_1_41
			product	hypothetical protein
			transl_table	11
44158	45324	CDS
			locus_tag	Sylvanvirus_1_42
			product	replication factor C small subunit
			transl_table	11
45407	46222	CDS
			locus_tag	Sylvanvirus_1_43
			product	hypothetical protein
			transl_table	11
47272	46313	CDS
			locus_tag	Sylvanvirus_1_44
			product	hypothetical protein
			transl_table	11
48291	47314	CDS
			locus_tag	Sylvanvirus_1_45
			product	hypothetical protein glt_00498
			transl_table	11
49320	48412	CDS
			locus_tag	Sylvanvirus_1_46
			product	hypothetical protein SAGO17_0077
			transl_table	11
49627	51333	CDS
			locus_tag	Sylvanvirus_1_47
			product	TNF receptor-associated factor 3-like isoform X1
			transl_table	11
52841	51594	CDS
			locus_tag	Sylvanvirus_1_48
			product	hypothetical protein
			transl_table	11
53520	54122	CDS
			locus_tag	Sylvanvirus_1_49
			product	hypothetical protein
			transl_table	11
57199	54176	CDS
			locus_tag	Sylvanvirus_1_50
			product	hypothetical protein AMSG_01946
			transl_table	11
57401	57622	CDS
			locus_tag	Sylvanvirus_1_51
			product	hypothetical protein
			transl_table	11
58742	57849	CDS
			locus_tag	Sylvanvirus_1_52
			product	hypothetical protein
			transl_table	11
58741	60870	CDS
			locus_tag	Sylvanvirus_1_53
			product	hypothetical protein
			transl_table	11
60981	61259	CDS
			locus_tag	Sylvanvirus_1_54
			product	hypothetical protein
			transl_table	11
61541	62581	CDS
			locus_tag	Sylvanvirus_1_55
			product	predicted protein
			transl_table	11
63549	62611	CDS
			locus_tag	Sylvanvirus_1_56
			product	hypothetical protein
			transl_table	11
63762	66224	CDS
			locus_tag	Sylvanvirus_1_57
			product	putative superfamily III helicase
			transl_table	11
66647	68038	CDS
			locus_tag	Sylvanvirus_1_58
			product	predicted protein
			transl_table	11
68270	68977	CDS
			locus_tag	Sylvanvirus_1_59
			product	hypothetical protein
			transl_table	11
70437	69088	CDS
			locus_tag	Sylvanvirus_1_60
			product	hypothetical protein
			transl_table	11
71733	71053	CDS
			locus_tag	Sylvanvirus_1_61
			product	hypothetical protein
			transl_table	11
72079	71837	CDS
			locus_tag	Sylvanvirus_1_62
			product	hypothetical protein
			transl_table	11
74638	72194	CDS
			locus_tag	Sylvanvirus_1_63
			product	hypothetical protein
			transl_table	11
74901	75134	CDS
			locus_tag	Sylvanvirus_1_64
			product	hypothetical protein
			transl_table	11
75131	75538	CDS
			locus_tag	Sylvanvirus_1_65
			product	hypothetical protein
			transl_table	11
76554	75547	CDS
			locus_tag	Sylvanvirus_1_66
			product	TIGR03118 family protein
			transl_table	11
77070	77300	CDS
			locus_tag	Sylvanvirus_1_67
			product	hypothetical protein
			transl_table	11
78022	77414	CDS
			locus_tag	Sylvanvirus_1_68
			product	hypothetical protein DICPUDRAFT_155188
			transl_table	11
79408	78191	CDS
			locus_tag	Sylvanvirus_1_69
			product	hypothetical protein
			transl_table	11
79641	80417	CDS
			locus_tag	Sylvanvirus_1_70
			product	hypothetical protein
			transl_table	11
80513	82657	CDS
			locus_tag	Sylvanvirus_1_71
			product	hypothetical protein
			transl_table	11
82844	83908	CDS
			locus_tag	Sylvanvirus_1_72
			product	protein disulfide-isomerase A5-like
			transl_table	11
85351	83990	CDS
			locus_tag	Sylvanvirus_1_73
			product	hypothetical protein
			transl_table	11
85663	86076	CDS
			locus_tag	Sylvanvirus_1_74
			product	hypothetical protein
			transl_table	11
86142	87008	CDS
			locus_tag	Sylvanvirus_1_75
			product	hypothetical protein
			transl_table	11
87859	87023	CDS
			locus_tag	Sylvanvirus_1_76
			product	YchF/TatD family DNA exonuclease
			transl_table	11
88160	88053	CDS
			locus_tag	Sylvanvirus_1_77
			product	hypothetical protein
			transl_table	11
88177	88635	CDS
			locus_tag	Sylvanvirus_1_78
			product	hypothetical protein
			transl_table	11
90358	88670	CDS
			locus_tag	Sylvanvirus_1_79
			product	hypothetical protein
			transl_table	11
90521	91855	CDS
			locus_tag	Sylvanvirus_1_80
			product	hypothetical protein
			transl_table	11
93698	92241	CDS
			locus_tag	Sylvanvirus_1_81
			product	hypothetical protein
			transl_table	11
94102	93896	CDS
			locus_tag	Sylvanvirus_1_82
			product	hypothetical protein
			transl_table	11
94171	94812	CDS
			locus_tag	Sylvanvirus_1_83
			product	hypothetical protein
			transl_table	11
94932	95504	CDS
			locus_tag	Sylvanvirus_1_84
			product	hypothetical protein PV06_10746
			transl_table	11
95599	96639	CDS
			locus_tag	Sylvanvirus_1_85
			product	hypothetical protein
			transl_table	11
98290	97559	CDS
			locus_tag	Sylvanvirus_1_86
			product	hypothetical protein
			transl_table	11
98535	99260	CDS
			locus_tag	Sylvanvirus_1_87
			product	hypothetical protein
			transl_table	11
99502	100449	CDS
			locus_tag	Sylvanvirus_1_88
			product	hypothetical protein
			transl_table	11
100524	100874	CDS
			locus_tag	Sylvanvirus_1_89
			product	hypothetical protein Kpol_1018p100
			transl_table	11
102620	101004	CDS
			locus_tag	Sylvanvirus_1_90
			product	hypothetical protein PENFLA_c091G01721
			transl_table	11
102645	102908	CDS
			locus_tag	Sylvanvirus_1_91
			product	hypothetical protein
			transl_table	11
102958	103080	CDS
			locus_tag	Sylvanvirus_1_92
			product	hypothetical protein
			transl_table	11
104138	103137	CDS
			locus_tag	Sylvanvirus_1_93
			product	hypothetical protein PLEOSDRAFT_1073153
			transl_table	11
104772	104683	CDS
			locus_tag	Sylvanvirus_1_94
			product	hypothetical protein
			transl_table	11
105347	106483	CDS
			locus_tag	Sylvanvirus_1_95
			product	hypothetical protein
			transl_table	11
109022	106551	CDS
			locus_tag	Sylvanvirus_1_96
			product	DEXDc helicase
			transl_table	11
109252	109752	CDS
			locus_tag	Sylvanvirus_1_97
			product	hypothetical protein
			transl_table	11
110736	109786	CDS
			locus_tag	Sylvanvirus_1_98
			product	hypothetical protein PBRA_005056
			transl_table	11
111437	110982	CDS
			locus_tag	Sylvanvirus_1_99
			product	hypothetical protein
			transl_table	11
111960	111688	CDS
			locus_tag	Sylvanvirus_1_100
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_2
11357	11429	tRNA	Sylvanvirus_tRNA_2
			product	tRNA-Arg(ACG)
467	3	CDS
			locus_tag	Sylvanvirus_2_1
			product	hypothetical protein
			transl_table	11
1060	800	CDS
			locus_tag	Sylvanvirus_2_2
			product	hypothetical protein
			transl_table	11
1375	2295	CDS
			locus_tag	Sylvanvirus_2_3
			product	hypothetical protein
			transl_table	11
2450	3514	CDS
			locus_tag	Sylvanvirus_2_4
			product	PREDICTED: uncharacterized protein LOC106804774
			transl_table	11
4028	3483	CDS
			locus_tag	Sylvanvirus_2_5
			product	hypothetical protein
			transl_table	11
4383	11192	CDS
			locus_tag	Sylvanvirus_2_6
			product	beta and beta-prime subunits of DNA dependent RNA-polymerase
			transl_table	11
11901	11431	CDS
			locus_tag	Sylvanvirus_2_7
			product	hypothetical protein
			transl_table	11
12994	12056	CDS
			locus_tag	Sylvanvirus_2_8
			product	hypothetical protein
			transl_table	11
13333	15645	CDS
			locus_tag	Sylvanvirus_2_9
			product	hypothetical protein CBC12_07300
			transl_table	11
15871	18831	CDS
			locus_tag	Sylvanvirus_2_10
			product	Uvr helicase/DDEDDh 3'-5' exonuclease
			transl_table	11
20451	19024	CDS
			locus_tag	Sylvanvirus_2_11
			product	hypothetical protein
			transl_table	11
22138	20558	CDS
			locus_tag	Sylvanvirus_2_12
			product	hypothetical protein
			transl_table	11
25572	22408	CDS
			locus_tag	Sylvanvirus_2_13
			product	hypothetical protein
			transl_table	11
28609	26105	CDS
			locus_tag	Sylvanvirus_2_14
			product	hypothetical protein
			transl_table	11
28973	28722	CDS
			locus_tag	Sylvanvirus_2_15
			product	hypothetical protein
			transl_table	11
29625	29128	CDS
			locus_tag	Sylvanvirus_2_16
			product	polyubiquitin
			transl_table	11
30466	29723	CDS
			locus_tag	Sylvanvirus_2_17
			product	hypothetical protein
			transl_table	11
30944	31183	CDS
			locus_tag	Sylvanvirus_2_18
			product	hypothetical protein
			transl_table	11
31753	31280	CDS
			locus_tag	Sylvanvirus_2_19
			product	hypothetical protein
			transl_table	11
32185	32511	CDS
			locus_tag	Sylvanvirus_2_20
			product	hypothetical protein
			transl_table	11
32626	33690	CDS
			locus_tag	Sylvanvirus_2_21
			product	hypothetical protein
			transl_table	11
33855	34898	CDS
			locus_tag	Sylvanvirus_2_22
			product	hypothetical protein
			transl_table	11
35061	35519	CDS
			locus_tag	Sylvanvirus_2_23
			product	hypothetical protein
			transl_table	11
36323	35820	CDS
			locus_tag	Sylvanvirus_2_24
			product	hypothetical protein
			transl_table	11
36652	36957	CDS
			locus_tag	Sylvanvirus_2_25
			product	hypothetical protein
			transl_table	11
37430	37074	CDS
			locus_tag	Sylvanvirus_2_26
			product	hypothetical protein
			transl_table	11
37484	38545	CDS
			locus_tag	Sylvanvirus_2_27
			product	hypothetical protein
			transl_table	11
38666	39346	CDS
			locus_tag	Sylvanvirus_2_28
			product	hypothetical protein
			transl_table	11
41995	39404	CDS
			locus_tag	Sylvanvirus_2_29
			product	hypothetical protein
			transl_table	11
42183	43262	CDS
			locus_tag	Sylvanvirus_2_30
			product	casein kinase 1-like protein 5
			transl_table	11
43404	44063	CDS
			locus_tag	Sylvanvirus_2_31
			product	hypothetical protein
			transl_table	11
44760	44272	CDS
			locus_tag	Sylvanvirus_2_32
			product	hypothetical protein
			transl_table	11
45504	45061	CDS
			locus_tag	Sylvanvirus_2_33
			product	hypothetical protein
			transl_table	11
46122	45745	CDS
			locus_tag	Sylvanvirus_2_34
			product	hypothetical protein
			transl_table	11
46440	49427	CDS
			locus_tag	Sylvanvirus_2_35
			product	hypothetical protein
			transl_table	11
51456	49591	CDS
			locus_tag	Sylvanvirus_2_36
			product	hypothetical protein
			transl_table	11
52905	52006	CDS
			locus_tag	Sylvanvirus_2_37
			product	hypothetical protein
			transl_table	11
53332	53096	CDS
			locus_tag	Sylvanvirus_2_38
			product	hypothetical protein
			transl_table	11
53416	53679	CDS
			locus_tag	Sylvanvirus_2_39
			product	hypothetical protein
			transl_table	11
55128	53863	CDS
			locus_tag	Sylvanvirus_2_40
			product	hypothetical protein
			transl_table	11
55777	55337	CDS
			locus_tag	Sylvanvirus_2_41
			product	hypothetical protein
			transl_table	11
56309	55767	CDS
			locus_tag	Sylvanvirus_2_42
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_3
3	824	CDS
			locus_tag	Sylvanvirus_3_1
			product	hypothetical protein
			transl_table	11
945	2588	CDS
			locus_tag	Sylvanvirus_3_2
			product	hypothetical protein PBRA_001494
			transl_table	11
2802	2572	CDS
			locus_tag	Sylvanvirus_3_3
			product	hypothetical protein
			transl_table	11
2894	3907	CDS
			locus_tag	Sylvanvirus_3_4
			product	hypothetical protein
			transl_table	11
4041	5216	CDS
			locus_tag	Sylvanvirus_3_5
			product	hypothetical protein
			transl_table	11
5806	5345	CDS
			locus_tag	Sylvanvirus_3_6
			product	hypothetical protein
			transl_table	11
7636	5906	CDS
			locus_tag	Sylvanvirus_3_7
			product	hypothetical protein
			transl_table	11
8494	7802	CDS
			locus_tag	Sylvanvirus_3_8
			product	hypothetical protein
			transl_table	11
8883	9452	CDS
			locus_tag	Sylvanvirus_3_9
			product	hypothetical protein
			transl_table	11
9760	9569	CDS
			locus_tag	Sylvanvirus_3_10
			product	hypothetical protein
			transl_table	11
10046	10333	CDS
			locus_tag	Sylvanvirus_3_11
			product	hypothetical protein
			transl_table	11
10472	11734	CDS
			locus_tag	Sylvanvirus_3_12
			product	hypothetical protein crov111
			transl_table	11
12081	11737	CDS
			locus_tag	Sylvanvirus_3_13
			product	hypothetical protein
			transl_table	11
12371	13999	CDS
			locus_tag	Sylvanvirus_3_14
			product	hypothetical protein
			transl_table	11
14306	15727	CDS
			locus_tag	Sylvanvirus_3_15
			product	hypothetical protein
			transl_table	11
16592	15783	CDS
			locus_tag	Sylvanvirus_3_16
			product	hypothetical protein
			transl_table	11
17253	16870	CDS
			locus_tag	Sylvanvirus_3_17
			product	hypothetical protein
			transl_table	11
17653	17360	CDS
			locus_tag	Sylvanvirus_3_18
			product	hypothetical protein
			transl_table	11
17875	19176	CDS
			locus_tag	Sylvanvirus_3_19
			product	PREDICTED: serine/threonine-protein kinase AFC2
			transl_table	11
19755	19327	CDS
			locus_tag	Sylvanvirus_3_20
			product	hypothetical protein
			transl_table	11
20056	19733	CDS
			locus_tag	Sylvanvirus_3_21
			product	hypothetical protein
			transl_table	11
22553	21636	CDS
			locus_tag	Sylvanvirus_3_22
			product	hypothetical protein
			transl_table	11
23087	23287	CDS
			locus_tag	Sylvanvirus_3_23
			product	hypothetical protein
			transl_table	11
24630	23368	CDS
			locus_tag	Sylvanvirus_3_24
			product	hypothetical protein
			transl_table	11
25609	25737	CDS
			locus_tag	Sylvanvirus_3_25
			product	hypothetical protein
			transl_table	11
25913	27442	CDS
			locus_tag	Sylvanvirus_3_26
			product	predicted protein
			transl_table	11
29822	27951	CDS
			locus_tag	Sylvanvirus_3_27
			product	hypothetical protein
			transl_table	11
32053	30038	CDS
			locus_tag	Sylvanvirus_3_28
			product	hypothetical protein
			transl_table	11
34294	32339	CDS
			locus_tag	Sylvanvirus_3_29
			product	mechanosensitive ion channel family protein
			transl_table	11
34913	36154	CDS
			locus_tag	Sylvanvirus_3_30
			product	hypothetical protein
			transl_table	11
36355	36257	CDS
			locus_tag	Sylvanvirus_3_31
			product	hypothetical protein
			transl_table	11
37027	36386	CDS
			locus_tag	Sylvanvirus_3_32
			product	hypothetical protein
			transl_table	11
38049	37330	CDS
			locus_tag	Sylvanvirus_3_33
			product	hypothetical protein
			transl_table	11
38313	39341	CDS
			locus_tag	Sylvanvirus_3_34
			product	hypothetical protein
			transl_table	11
39580	39798	CDS
			locus_tag	Sylvanvirus_3_35
			product	hypothetical protein
			transl_table	11
39931	40923	CDS
			locus_tag	Sylvanvirus_3_36
			product	hypothetical protein
			transl_table	11
42863	41064	CDS
			locus_tag	Sylvanvirus_3_37
			product	hypothetical protein
			transl_table	11
43124	44185	CDS
			locus_tag	Sylvanvirus_3_38
			product	hypothetical protein
			transl_table	11
44621	45253	CDS
			locus_tag	Sylvanvirus_3_39
			product	hypothetical protein
			transl_table	11
46981	45656	CDS
			locus_tag	Sylvanvirus_3_40
			product	DNA phosphorothioation-dependent restriction protein DptG
			transl_table	11
>Sylvanvirus_4
259	441	CDS
			locus_tag	Sylvanvirus_4_1
			product	hypothetical protein
			transl_table	11
434	1819	CDS
			locus_tag	Sylvanvirus_4_2
			product	hypothetical protein Klosneuvirus_3_120
			transl_table	11
2767	1868	CDS
			locus_tag	Sylvanvirus_4_3
			product	hypothetical protein
			transl_table	11
3828	3085	CDS
			locus_tag	Sylvanvirus_4_4
			product	PREDICTED: uncharacterized protein LOC105318987
			transl_table	11
4237	5706	CDS
			locus_tag	Sylvanvirus_4_5
			product	hypothetical protein
			transl_table	11
5894	7840	CDS
			locus_tag	Sylvanvirus_4_6
			product	hypothetical protein
			transl_table	11
8344	7904	CDS
			locus_tag	Sylvanvirus_4_7
			product	hypothetical protein
			transl_table	11
8731	9426	CDS
			locus_tag	Sylvanvirus_4_8
			product	hypothetical protein
			transl_table	11
9713	10348	CDS
			locus_tag	Sylvanvirus_4_9
			product	hypothetical protein
			transl_table	11
10992	10444	CDS
			locus_tag	Sylvanvirus_4_10
			product	hypothetical protein
			transl_table	11
11538	11233	CDS
			locus_tag	Sylvanvirus_4_11
			product	hypothetical protein
			transl_table	11
11751	11921	CDS
			locus_tag	Sylvanvirus_4_12
			product	hypothetical protein
			transl_table	11
12481	11942	CDS
			locus_tag	Sylvanvirus_4_13
			product	hypothetical protein
			transl_table	11
13405	12503	CDS
			locus_tag	Sylvanvirus_4_14
			product	hypothetical protein
			transl_table	11
13935	14057	CDS
			locus_tag	Sylvanvirus_4_15
			product	hypothetical protein
			transl_table	11
14054	15706	CDS
			locus_tag	Sylvanvirus_4_16
			product	casein kinase
			transl_table	11
15817	16545	CDS
			locus_tag	Sylvanvirus_4_17
			product	hypothetical protein
			transl_table	11
16750	17238	CDS
			locus_tag	Sylvanvirus_4_18
			product	hypothetical protein
			transl_table	11
18124	17825	CDS
			locus_tag	Sylvanvirus_4_19
			product	hypothetical protein
			transl_table	11
18390	18202	CDS
			locus_tag	Sylvanvirus_4_20
			product	hypothetical protein
			transl_table	11
20010	18394	CDS
			locus_tag	Sylvanvirus_4_21
			product	hypothetical protein
			transl_table	11
20077	20973	CDS
			locus_tag	Sylvanvirus_4_22
			product	hypothetical protein
			transl_table	11
21163	22563	CDS
			locus_tag	Sylvanvirus_4_23
			product	hypothetical protein
			transl_table	11
23186	22575	CDS
			locus_tag	Sylvanvirus_4_24
			product	hypothetical protein
			transl_table	11
23797	24501	CDS
			locus_tag	Sylvanvirus_4_25
			product	hypothetical protein
			transl_table	11
24522	24647	CDS
			locus_tag	Sylvanvirus_4_26
			product	hypothetical protein
			transl_table	11
24863	25450	CDS
			locus_tag	Sylvanvirus_4_27
			product	hypothetical protein
			transl_table	11
25544	25642	CDS
			locus_tag	Sylvanvirus_4_28
			product	hypothetical protein
			transl_table	11
25829	26449	CDS
			locus_tag	Sylvanvirus_4_29
			product	hypothetical protein
			transl_table	11
27626	26511	CDS
			locus_tag	Sylvanvirus_4_30
			product	kinase-like protein
			transl_table	11
28115	27696	CDS
			locus_tag	Sylvanvirus_4_31
			product	hypothetical protein
			transl_table	11
28618	28295	CDS
			locus_tag	Sylvanvirus_4_32
			product	hypothetical protein
			transl_table	11
28905	30206	CDS
			locus_tag	Sylvanvirus_4_33
			product	hypothetical protein
			transl_table	11
32822	30258	CDS
			locus_tag	Sylvanvirus_4_34
			product	hypothetical protein
			transl_table	11
33117	33686	CDS
			locus_tag	Sylvanvirus_4_35
			product	hypothetical protein
			transl_table	11
33714	34253	CDS
			locus_tag	Sylvanvirus_4_36
			product	hypothetical protein
			transl_table	11
34369	34653	CDS
			locus_tag	Sylvanvirus_4_37
			product	hypothetical protein
			transl_table	11
34931	34800	CDS
			locus_tag	Sylvanvirus_4_38
			product	hypothetical protein
			transl_table	11
34915	35349	CDS
			locus_tag	Sylvanvirus_4_39
			product	hypothetical protein
			transl_table	11
35626	37482	CDS
			locus_tag	Sylvanvirus_4_40
			product	hypothetical protein
			transl_table	11
38169	37684	CDS
			locus_tag	Sylvanvirus_4_41
			product	hypothetical protein
			transl_table	11
40330	38903	CDS
			locus_tag	Sylvanvirus_4_42
			product	EXX78192.1
			transl_table	11
41317	40592	CDS
			locus_tag	Sylvanvirus_4_43
			product	serine hydrolase
			transl_table	11
41525	41950	CDS
			locus_tag	Sylvanvirus_4_44
			product	hypothetical protein
			transl_table	11
43795	42197	CDS
			locus_tag	Sylvanvirus_4_45
			product	PREDICTED: serine/threonine-protein kinase Aurora-3-like
			transl_table	11
46119	43903	CDS
			locus_tag	Sylvanvirus_4_46
			product	ATP-dependent DNA helicase PIF1
			transl_table	11
>Sylvanvirus_5
2311	467	CDS
			locus_tag	Sylvanvirus_5_1
			product	voltage-gated chloride channel protein
			transl_table	11
2313	2510	CDS
			locus_tag	Sylvanvirus_5_2
			product	hypothetical protein
			transl_table	11
3545	3360	CDS
			locus_tag	Sylvanvirus_5_3
			product	hypothetical protein
			transl_table	11
5818	4040	CDS
			locus_tag	Sylvanvirus_5_4
			product	hypothetical protein
			transl_table	11
6337	6981	CDS
			locus_tag	Sylvanvirus_5_5
			product	hypothetical protein
			transl_table	11
8453	8695	CDS
			locus_tag	Sylvanvirus_5_6
			product	hypothetical protein
			transl_table	11
9061	9492	CDS
			locus_tag	Sylvanvirus_5_7
			product	hypothetical protein
			transl_table	11
14880	11503	CDS
			locus_tag	Sylvanvirus_5_8
			product	sensory box sensor histidine kinase/response regulator
			transl_table	11
15222	15022	CDS
			locus_tag	Sylvanvirus_5_9
			product	hypothetical protein
			transl_table	11
15441	17822	CDS
			locus_tag	Sylvanvirus_5_10
			product	putative two-component hybrid sensor and regulator
			transl_table	11
21403	19259	CDS
			locus_tag	Sylvanvirus_5_11
			product	hypothetical protein SteCoe_30195
			transl_table	11
22333	22488	CDS
			locus_tag	Sylvanvirus_5_12
			product	hypothetical protein
			transl_table	11
25125	24442	CDS
			locus_tag	Sylvanvirus_5_13
			product	hypothetical protein
			transl_table	11
26009	25839	CDS
			locus_tag	Sylvanvirus_5_14
			product	hypothetical protein
			transl_table	11
27751	26405	CDS
			locus_tag	Sylvanvirus_5_15
			product	hypothetical protein
			transl_table	11
28023	27892	CDS
			locus_tag	Sylvanvirus_5_16
			product	hypothetical protein
			transl_table	11
29010	28816	CDS
			locus_tag	Sylvanvirus_5_17
			product	hypothetical protein
			transl_table	11
30434	32611	CDS
			locus_tag	Sylvanvirus_5_18
			product	hypothetical protein
			transl_table	11
33657	32713	CDS
			locus_tag	Sylvanvirus_5_19
			product	hypothetical protein
			transl_table	11
33964	34404	CDS
			locus_tag	Sylvanvirus_5_20
			product	hypothetical protein
			transl_table	11
34371	35165	CDS
			locus_tag	Sylvanvirus_5_21
			product	hypothetical protein
			transl_table	11
36047	35922	CDS
			locus_tag	Sylvanvirus_5_22
			product	hypothetical protein
			transl_table	11
36091	36702	CDS
			locus_tag	Sylvanvirus_5_23
			product	hypothetical protein
			transl_table	11
36671	37936	CDS
			locus_tag	Sylvanvirus_5_24
			product	hypothetical protein VITISV_023139
			transl_table	11
40809	39268	CDS
			locus_tag	Sylvanvirus_5_25
			product	hypothetical protein
			transl_table	11
41298	44012	CDS
			locus_tag	Sylvanvirus_5_26
			product	hypothetical protein V499_07591
			transl_table	11
45914	45675	CDS
			locus_tag	Sylvanvirus_5_27
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_6
216	1100	CDS
			locus_tag	Sylvanvirus_6_1
			product	hypothetical protein
			transl_table	11
1214	1738	CDS
			locus_tag	Sylvanvirus_6_2
			product	hypothetical protein
			transl_table	11
1740	2861	CDS
			locus_tag	Sylvanvirus_6_3
			product	phosphatase 2C
			transl_table	11
3170	2928	CDS
			locus_tag	Sylvanvirus_6_4
			product	hypothetical protein
			transl_table	11
3185	3466	CDS
			locus_tag	Sylvanvirus_6_5
			product	hypothetical protein
			transl_table	11
4350	3493	CDS
			locus_tag	Sylvanvirus_6_6
			product	hypothetical protein
			transl_table	11
4981	5739	CDS
			locus_tag	Sylvanvirus_6_7
			product	hypothetical protein
			transl_table	11
5789	7660	CDS
			locus_tag	Sylvanvirus_6_8
			product	hypothetical protein
			transl_table	11
7891	9183	CDS
			locus_tag	Sylvanvirus_6_9
			product	Collagen triple helix repeat protein
			transl_table	11
9345	9797	CDS
			locus_tag	Sylvanvirus_6_10
			product	hypothetical protein
			transl_table	11
9938	10885	CDS
			locus_tag	Sylvanvirus_6_11
			product	hypothetical protein
			transl_table	11
12047	11037	CDS
			locus_tag	Sylvanvirus_6_12
			product	hypothetical protein
			transl_table	11
13497	12001	CDS
			locus_tag	Sylvanvirus_6_13
			product	desaturase
			transl_table	11
14854	13859	CDS
			locus_tag	Sylvanvirus_6_14
			product	hypothetical protein PBRA_002700
			transl_table	11
15217	15603	CDS
			locus_tag	Sylvanvirus_6_15
			product	hypothetical protein
			transl_table	11
16420	15650	CDS
			locus_tag	Sylvanvirus_6_16
			product	hypothetical protein
			transl_table	11
17008	17220	CDS
			locus_tag	Sylvanvirus_6_17
			product	hypothetical protein
			transl_table	11
17540	18985	CDS
			locus_tag	Sylvanvirus_6_18
			product	hypothetical protein
			transl_table	11
19797	19042	CDS
			locus_tag	Sylvanvirus_6_19
			product	hypothetical protein
			transl_table	11
20747	20151	CDS
			locus_tag	Sylvanvirus_6_20
			product	hypothetical protein
			transl_table	11
20987	22402	CDS
			locus_tag	Sylvanvirus_6_21
			product	hypothetical protein
			transl_table	11
22603	23157	CDS
			locus_tag	Sylvanvirus_6_22
			product	hypothetical protein
			transl_table	11
23704	23417	CDS
			locus_tag	Sylvanvirus_6_23
			product	hypothetical protein
			transl_table	11
25369	23810	CDS
			locus_tag	Sylvanvirus_6_24
			product	hypothetical protein
			transl_table	11
25974	26327	CDS
			locus_tag	Sylvanvirus_6_25
			product	hypothetical protein
			transl_table	11
27281	26340	CDS
			locus_tag	Sylvanvirus_6_26
			product	hypothetical protein Catovirus_2_144
			transl_table	11
27985	27701	CDS
			locus_tag	Sylvanvirus_6_27
			product	hypothetical protein
			transl_table	11
28397	28125	CDS
			locus_tag	Sylvanvirus_6_28
			product	hypothetical protein
			transl_table	11
29464	28715	CDS
			locus_tag	Sylvanvirus_6_29
			product	hypothetical protein
			transl_table	11
30946	29777	CDS
			locus_tag	Sylvanvirus_6_30
			product	E3 ubiquitin-protein ligase RNF14
			transl_table	11
31199	31936	CDS
			locus_tag	Sylvanvirus_6_31
			product	hypothetical protein
			transl_table	11
32197	33348	CDS
			locus_tag	Sylvanvirus_6_32
			product	hypothetical protein
			transl_table	11
33505	33909	CDS
			locus_tag	Sylvanvirus_6_33
			product	hypothetical protein
			transl_table	11
33906	34010	CDS
			locus_tag	Sylvanvirus_6_34
			product	hypothetical protein
			transl_table	11
36922	34037	CDS
			locus_tag	Sylvanvirus_6_35
			product	hypothetical protein
			transl_table	11
37129	38091	CDS
			locus_tag	Sylvanvirus_6_36
			product	hypothetical protein
			transl_table	11
38473	38126	CDS
			locus_tag	Sylvanvirus_6_37
			product	hypothetical protein
			transl_table	11
38886	40370	CDS
			locus_tag	Sylvanvirus_6_38
			product	hypothetical protein RclHR1_02680014
			transl_table	11
40605	41321	CDS
			locus_tag	Sylvanvirus_6_39
			product	hypothetical protein
			transl_table	11
41679	41362	CDS
			locus_tag	Sylvanvirus_6_40
			product	hypothetical protein
			transl_table	11
42395	42021	CDS
			locus_tag	Sylvanvirus_6_41
			product	hypothetical protein
			transl_table	11
42731	43282	CDS
			locus_tag	Sylvanvirus_6_42
			product	hypothetical protein
			transl_table	11
43893	43681	CDS
			locus_tag	Sylvanvirus_6_43
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_7
3	182	CDS
			locus_tag	Sylvanvirus_7_1
			product	hypothetical protein
			transl_table	11
340	188	CDS
			locus_tag	Sylvanvirus_7_2
			product	hypothetical protein
			transl_table	11
1821	640	CDS
			locus_tag	Sylvanvirus_7_3
			product	hypothetical protein
			transl_table	11
2148	3095	CDS
			locus_tag	Sylvanvirus_7_4
			product	hypothetical protein
			transl_table	11
4647	3304	CDS
			locus_tag	Sylvanvirus_7_5
			product	hypothetical protein
			transl_table	11
4716	4865	CDS
			locus_tag	Sylvanvirus_7_6
			product	hypothetical protein
			transl_table	11
6269	5223	CDS
			locus_tag	Sylvanvirus_7_7
			product	hypothetical protein Phum_PHUM275760
			transl_table	11
7187	6768	CDS
			locus_tag	Sylvanvirus_7_8
			product	hypothetical protein
			transl_table	11
7409	8128	CDS
			locus_tag	Sylvanvirus_7_9
			product	hypothetical protein
			transl_table	11
8287	8895	CDS
			locus_tag	Sylvanvirus_7_10
			product	hypothetical protein
			transl_table	11
9337	8966	CDS
			locus_tag	Sylvanvirus_7_11
			product	hypothetical protein
			transl_table	11
10272	9433	CDS
			locus_tag	Sylvanvirus_7_12
			product	hypothetical protein
			transl_table	11
10761	11261	CDS
			locus_tag	Sylvanvirus_7_13
			product	hypothetical protein
			transl_table	11
11353	11460	CDS
			locus_tag	Sylvanvirus_7_14
			product	hypothetical protein
			transl_table	11
12426	11746	CDS
			locus_tag	Sylvanvirus_7_15
			product	hypothetical protein
			transl_table	11
13331	12960	CDS
			locus_tag	Sylvanvirus_7_16
			product	hypothetical protein
			transl_table	11
13803	14630	CDS
			locus_tag	Sylvanvirus_7_17
			product	hypothetical protein
			transl_table	11
14948	15481	CDS
			locus_tag	Sylvanvirus_7_18
			product	hypothetical protein
			transl_table	11
16325	16459	CDS
			locus_tag	Sylvanvirus_7_19
			product	hypothetical protein
			transl_table	11
16641	17162	CDS
			locus_tag	Sylvanvirus_7_20
			product	hypothetical protein
			transl_table	11
17543	17896	CDS
			locus_tag	Sylvanvirus_7_21
			product	hypothetical protein
			transl_table	11
18408	18082	CDS
			locus_tag	Sylvanvirus_7_22
			product	hypothetical protein
			transl_table	11
18814	19242	CDS
			locus_tag	Sylvanvirus_7_23
			product	hypothetical protein
			transl_table	11
19503	20219	CDS
			locus_tag	Sylvanvirus_7_24
			product	hypothetical protein
			transl_table	11
20853	20245	CDS
			locus_tag	Sylvanvirus_7_25
			product	hypothetical protein
			transl_table	11
21105	21794	CDS
			locus_tag	Sylvanvirus_7_26
			product	hypothetical protein AN958_01098
			transl_table	11
21999	22613	CDS
			locus_tag	Sylvanvirus_7_27
			product	hypothetical protein
			transl_table	11
23166	22756	CDS
			locus_tag	Sylvanvirus_7_28
			product	hypothetical protein
			transl_table	11
23391	24116	CDS
			locus_tag	Sylvanvirus_7_29
			product	hypothetical protein
			transl_table	11
24293	25633	CDS
			locus_tag	Sylvanvirus_7_30
			product	predicted protein
			transl_table	11
26642	25695	CDS
			locus_tag	Sylvanvirus_7_31
			product	hypothetical protein
			transl_table	11
27071	27496	CDS
			locus_tag	Sylvanvirus_7_32
			product	hypothetical protein
			transl_table	11
28150	27770	CDS
			locus_tag	Sylvanvirus_7_33
			product	hypothetical protein Klosneuvirus_1_115
			transl_table	11
28744	28187	CDS
			locus_tag	Sylvanvirus_7_34
			product	hypothetical protein Klosneuvirus_1_119
			transl_table	11
30075	29074	CDS
			locus_tag	Sylvanvirus_7_35
			product	Transposable element Tc1 transposase
			transl_table	11
30319	31071	CDS
			locus_tag	Sylvanvirus_7_36
			product	hypothetical protein
			transl_table	11
31654	33012	CDS
			locus_tag	Sylvanvirus_7_37
			product	hypothetical protein
			transl_table	11
33767	33558	CDS
			locus_tag	Sylvanvirus_7_38
			product	hypothetical protein
			transl_table	11
34095	35150	CDS
			locus_tag	Sylvanvirus_7_39
			product	hypothetical protein A3D21_04885
			transl_table	11
35372	35701	CDS
			locus_tag	Sylvanvirus_7_40
			product	hypothetical protein
			transl_table	11
38070	36391	CDS
			locus_tag	Sylvanvirus_7_41
			product	class I SAM-dependent methyltransferase
			transl_table	11
38143	38247	CDS
			locus_tag	Sylvanvirus_7_42
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_8
663	1	CDS
			locus_tag	Sylvanvirus_8_1
			product	hypothetical protein PBRA_007725
			transl_table	11
1888	974	CDS
			locus_tag	Sylvanvirus_8_2
			product	hypothetical protein
			transl_table	11
1959	2180	CDS
			locus_tag	Sylvanvirus_8_3
			product	hypothetical protein
			transl_table	11
2275	3273	CDS
			locus_tag	Sylvanvirus_8_4
			product	hypothetical protein
			transl_table	11
3908	4453	CDS
			locus_tag	Sylvanvirus_8_5
			product	hypothetical protein
			transl_table	11
6433	4517	CDS
			locus_tag	Sylvanvirus_8_6
			product	hypothetical protein
			transl_table	11
6945	7721	CDS
			locus_tag	Sylvanvirus_8_7
			product	hypothetical protein
			transl_table	11
8803	7754	CDS
			locus_tag	Sylvanvirus_8_8
			product	hypothetical protein
			transl_table	11
9983	8961	CDS
			locus_tag	Sylvanvirus_8_9
			product	hypothetical protein
			transl_table	11
10310	11182	CDS
			locus_tag	Sylvanvirus_8_10
			product	hypothetical protein
			transl_table	11
11680	12234	CDS
			locus_tag	Sylvanvirus_8_11
			product	hypothetical protein
			transl_table	11
13341	12343	CDS
			locus_tag	Sylvanvirus_8_12
			product	hypothetical protein
			transl_table	11
13594	14961	CDS
			locus_tag	Sylvanvirus_8_13
			product	FAD-binding oxidoreductase
			transl_table	11
15280	14978	CDS
			locus_tag	Sylvanvirus_8_14
			product	hypothetical protein
			transl_table	11
16762	15443	CDS
			locus_tag	Sylvanvirus_8_15
			product	predicted protein
			transl_table	11
17279	17121	CDS
			locus_tag	Sylvanvirus_8_16
			product	hypothetical protein
			transl_table	11
17486	20575	CDS
			locus_tag	Sylvanvirus_8_17
			product	hypothetical protein
			transl_table	11
21796	20705	CDS
			locus_tag	Sylvanvirus_8_18
			product	GDP-D-mannose-3',5'-epimerase
			transl_table	11
23399	22068	CDS
			locus_tag	Sylvanvirus_8_19
			product	hypothetical protein
			transl_table	11
23721	25502	CDS
			locus_tag	Sylvanvirus_8_20
			product	hypothetical protein Klosneuvirus_3_305
			transl_table	11
29560	25523	CDS
			locus_tag	Sylvanvirus_8_21
			product	hypothetical protein COCSUDRAFT_41538
			transl_table	11
29804	31846	CDS
			locus_tag	Sylvanvirus_8_22
			product	hypothetical protein COCSUDRAFT_41538
			transl_table	11
32031	33965	CDS
			locus_tag	Sylvanvirus_8_23
			product	hypothetical protein
			transl_table	11
34169	35224	CDS
			locus_tag	Sylvanvirus_8_24
			product	hypothetical protein COB55_03630
			transl_table	11
35335	35814	CDS
			locus_tag	Sylvanvirus_8_25
			product	hypothetical protein
			transl_table	11
36262	36429	CDS
			locus_tag	Sylvanvirus_8_26
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_9
1	501	CDS
			locus_tag	Sylvanvirus_9_1
			product	hypothetical protein
			transl_table	11
918	520	CDS
			locus_tag	Sylvanvirus_9_2
			product	hypothetical protein
			transl_table	11
1161	1310	CDS
			locus_tag	Sylvanvirus_9_3
			product	hypothetical protein
			transl_table	11
2082	1540	CDS
			locus_tag	Sylvanvirus_9_4
			product	hypothetical protein
			transl_table	11
2316	4259	CDS
			locus_tag	Sylvanvirus_9_5
			product	CMGC/MAPK protein kinase, variant 1
			transl_table	11
4768	4490	CDS
			locus_tag	Sylvanvirus_9_6
			product	hypothetical protein
			transl_table	11
5298	5516	CDS
			locus_tag	Sylvanvirus_9_7
			product	hypothetical protein
			transl_table	11
5841	7469	CDS
			locus_tag	Sylvanvirus_9_8
			product	hypothetical protein AK812_SmicGene24248
			transl_table	11
8303	7515	CDS
			locus_tag	Sylvanvirus_9_9
			product	mitochondrial carrier-like protein
			transl_table	11
8706	10811	CDS
			locus_tag	Sylvanvirus_9_10
			product	hypothetical protein
			transl_table	11
11192	10893	CDS
			locus_tag	Sylvanvirus_9_11
			product	hypothetical protein
			transl_table	11
11473	14880	CDS
			locus_tag	Sylvanvirus_9_12
			product	hypothetical protein
			transl_table	11
14907	15416	CDS
			locus_tag	Sylvanvirus_9_13
			product	hypothetical protein
			transl_table	11
15477	16100	CDS
			locus_tag	Sylvanvirus_9_14
			product	hypothetical protein
			transl_table	11
16301	16753	CDS
			locus_tag	Sylvanvirus_9_15
			product	hypothetical protein
			transl_table	11
17348	16992	CDS
			locus_tag	Sylvanvirus_9_16
			product	hypothetical protein
			transl_table	11
17946	18590	CDS
			locus_tag	Sylvanvirus_9_17
			product	hypothetical protein
			transl_table	11
19929	18976	CDS
			locus_tag	Sylvanvirus_9_18
			product	forkhead associated FHA domain and RING domain protein
			transl_table	11
19980	20075	CDS
			locus_tag	Sylvanvirus_9_19
			product	hypothetical protein
			transl_table	11
20243	21730	CDS
			locus_tag	Sylvanvirus_9_20
			product	hypothetical protein
			transl_table	11
22154	21702	CDS
			locus_tag	Sylvanvirus_9_21
			product	hypothetical protein
			transl_table	11
22153	22530	CDS
			locus_tag	Sylvanvirus_9_22
			product	hypothetical protein
			transl_table	11
24916	22733	CDS
			locus_tag	Sylvanvirus_9_23
			product	hypothetical protein
			transl_table	11
26046	25255	CDS
			locus_tag	Sylvanvirus_9_24
			product	hypothetical protein
			transl_table	11
27506	26196	CDS
			locus_tag	Sylvanvirus_9_25
			product	zinc metalloprotease
			transl_table	11
28567	27602	CDS
			locus_tag	Sylvanvirus_9_26
			product	hypothetical protein
			transl_table	11
30149	28680	CDS
			locus_tag	Sylvanvirus_9_27
			product	hypothetical protein
			transl_table	11
30472	30212	CDS
			locus_tag	Sylvanvirus_9_28
			product	hypothetical protein
			transl_table	11
30878	32056	CDS
			locus_tag	Sylvanvirus_9_29
			product	hypothetical protein
			transl_table	11
32460	32242	CDS
			locus_tag	Sylvanvirus_9_30
			product	hypothetical protein
			transl_table	11
33520	32522	CDS
			locus_tag	Sylvanvirus_9_31
			product	replication factor C small subunit
			transl_table	11
33874	36003	CDS
			locus_tag	Sylvanvirus_9_32
			product	hypothetical protein
			transl_table	11
36413	36021	CDS
			locus_tag	Sylvanvirus_9_33
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_10
3	290	CDS
			locus_tag	Sylvanvirus_10_1
			product	hypothetical protein
			transl_table	11
540	950	CDS
			locus_tag	Sylvanvirus_10_2
			product	hypothetical protein
			transl_table	11
1372	971	CDS
			locus_tag	Sylvanvirus_10_3
			product	hypothetical protein
			transl_table	11
1737	2243	CDS
			locus_tag	Sylvanvirus_10_4
			product	hypothetical protein
			transl_table	11
2355	2924	CDS
			locus_tag	Sylvanvirus_10_5
			product	hypothetical protein
			transl_table	11
3047	3367	CDS
			locus_tag	Sylvanvirus_10_6
			product	hypothetical protein
			transl_table	11
3813	3490	CDS
			locus_tag	Sylvanvirus_10_7
			product	hypothetical protein
			transl_table	11
4052	4564	CDS
			locus_tag	Sylvanvirus_10_8
			product	hypothetical protein
			transl_table	11
6757	4700	CDS
			locus_tag	Sylvanvirus_10_9
			product	hypothetical protein
			transl_table	11
7460	6876	CDS
			locus_tag	Sylvanvirus_10_10
			product	hypothetical protein
			transl_table	11
8827	7562	CDS
			locus_tag	Sylvanvirus_10_11
			product	hypothetical protein Klosneuvirus_2_130
			transl_table	11
9242	9370	CDS
			locus_tag	Sylvanvirus_10_12
			product	hypothetical protein
			transl_table	11
10822	9353	CDS
			locus_tag	Sylvanvirus_10_13
			product	hypothetical protein
			transl_table	11
11172	13517	CDS
			locus_tag	Sylvanvirus_10_14
			product	hypothetical protein
			transl_table	11
14673	13612	CDS
			locus_tag	Sylvanvirus_10_15
			product	hypothetical protein
			transl_table	11
15344	14919	CDS
			locus_tag	Sylvanvirus_10_16
			product	hypothetical protein
			transl_table	11
15706	15467	CDS
			locus_tag	Sylvanvirus_10_17
			product	hypothetical protein
			transl_table	11
16505	16125	CDS
			locus_tag	Sylvanvirus_10_18
			product	hypothetical protein
			transl_table	11
16779	17534	CDS
			locus_tag	Sylvanvirus_10_19
			product	hypothetical protein
			transl_table	11
19452	17740	CDS
			locus_tag	Sylvanvirus_10_20
			product	hypothetical protein
			transl_table	11
19964	20233	CDS
			locus_tag	Sylvanvirus_10_21
			product	hypothetical protein
			transl_table	11
21025	22377	CDS
			locus_tag	Sylvanvirus_10_22
			product	hypothetical protein
			transl_table	11
22543	23613	CDS
			locus_tag	Sylvanvirus_10_23
			product	hypothetical protein
			transl_table	11
25398	23626	CDS
			locus_tag	Sylvanvirus_10_24
			product	CMGC/MAPK protein kinase
			transl_table	11
25731	26069	CDS
			locus_tag	Sylvanvirus_10_25
			product	hypothetical protein
			transl_table	11
27837	26116	CDS
			locus_tag	Sylvanvirus_10_26
			product	hypothetical protein AB986_15415
			transl_table	11
28431	27973	CDS
			locus_tag	Sylvanvirus_10_27
			product	hypothetical protein
			transl_table	11
28715	29290	CDS
			locus_tag	Sylvanvirus_10_28
			product	hypothetical protein
			transl_table	11
29725	29312	CDS
			locus_tag	Sylvanvirus_10_29
			product	hypothetical protein
			transl_table	11
30596	29910	CDS
			locus_tag	Sylvanvirus_10_30
			product	hypothetical protein
			transl_table	11
30858	32114	CDS
			locus_tag	Sylvanvirus_10_31
			product	hypothetical protein
			transl_table	11
32347	32631	CDS
			locus_tag	Sylvanvirus_10_32
			product	hypothetical protein
			transl_table	11
33790	33221	CDS
			locus_tag	Sylvanvirus_10_33
			product	hypothetical protein
			transl_table	11
34165	34479	CDS
			locus_tag	Sylvanvirus_10_34
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_11
760	2	CDS
			locus_tag	Sylvanvirus_11_1
			product	hypothetical protein
			transl_table	11
876	2051	CDS
			locus_tag	Sylvanvirus_11_2
			product	hypothetical protein
			transl_table	11
2294	2617	CDS
			locus_tag	Sylvanvirus_11_3
			product	hypothetical protein
			transl_table	11
2905	3450	CDS
			locus_tag	Sylvanvirus_11_4
			product	hypothetical protein
			transl_table	11
4163	3897	CDS
			locus_tag	Sylvanvirus_11_5
			product	hypothetical protein
			transl_table	11
4395	4760	CDS
			locus_tag	Sylvanvirus_11_6
			product	hypothetical protein
			transl_table	11
5474	4776	CDS
			locus_tag	Sylvanvirus_11_7
			product	E3 ubiquitin-protein ligase MARCH5
			transl_table	11
5575	6021	CDS
			locus_tag	Sylvanvirus_11_8
			product	hypothetical protein
			transl_table	11
6458	6069	CDS
			locus_tag	Sylvanvirus_11_9
			product	hypothetical protein
			transl_table	11
6759	6592	CDS
			locus_tag	Sylvanvirus_11_10
			product	hypothetical protein
			transl_table	11
6890	7210	CDS
			locus_tag	Sylvanvirus_11_11
			product	hypothetical protein
			transl_table	11
7532	8095	CDS
			locus_tag	Sylvanvirus_11_12
			product	hypothetical protein
			transl_table	11
8264	9277	CDS
			locus_tag	Sylvanvirus_11_13
			product	protein-ADP-ribose hydrolase
			transl_table	11
9299	9685	CDS
			locus_tag	Sylvanvirus_11_14
			product	hypothetical protein
			transl_table	11
11185	9761	CDS
			locus_tag	Sylvanvirus_11_15
			product	hypothetical protein
			transl_table	11
11820	12107	CDS
			locus_tag	Sylvanvirus_11_16
			product	hypothetical protein
			transl_table	11
12369	12614	CDS
			locus_tag	Sylvanvirus_11_17
			product	hypothetical protein
			transl_table	11
12743	12934	CDS
			locus_tag	Sylvanvirus_11_18
			product	hypothetical protein
			transl_table	11
13090	13983	CDS
			locus_tag	Sylvanvirus_11_19
			product	metallophosphatase
			transl_table	11
14143	13985	CDS
			locus_tag	Sylvanvirus_11_20
			product	hypothetical protein
			transl_table	11
14137	14535	CDS
			locus_tag	Sylvanvirus_11_21
			product	hypothetical protein
			transl_table	11
14832	19724	CDS
			locus_tag	Sylvanvirus_11_22
			product	DNA-directed RNA polymerase II
			transl_table	11
20971	19775	CDS
			locus_tag	Sylvanvirus_11_23
			product	hypothetical protein
			transl_table	11
21254	21523	CDS
			locus_tag	Sylvanvirus_11_24
			product	hypothetical protein
			transl_table	11
21668	22906	CDS
			locus_tag	Sylvanvirus_11_25
			product	hypothetical protein
			transl_table	11
23630	23028	CDS
			locus_tag	Sylvanvirus_11_26
			product	aldo/keto reductase
			transl_table	11
25880	23895	CDS
			locus_tag	Sylvanvirus_11_27
			product	hypothetical protein FOYG_01645
			transl_table	11
26808	28061	CDS
			locus_tag	Sylvanvirus_11_28
			product	hypothetical protein
			transl_table	11
29903	29346	CDS
			locus_tag	Sylvanvirus_11_29
			product	hypothetical protein
			transl_table	11
30532	29930	CDS
			locus_tag	Sylvanvirus_11_30
			product	hypothetical protein
			transl_table	11
30812	30726	CDS
			locus_tag	Sylvanvirus_11_31
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_12
417	1	CDS
			locus_tag	Sylvanvirus_12_1
			product	hypothetical protein
			transl_table	11
861	1418	CDS
			locus_tag	Sylvanvirus_12_2
			product	hypothetical protein
			transl_table	11
2257	1580	CDS
			locus_tag	Sylvanvirus_12_3
			product	hypothetical protein
			transl_table	11
6170	2517	CDS
			locus_tag	Sylvanvirus_12_4
			product	unnamed protein product
			transl_table	11
6551	7549	CDS
			locus_tag	Sylvanvirus_12_5
			product	hypothetical protein
			transl_table	11
7609	7950	CDS
			locus_tag	Sylvanvirus_12_6
			product	hypothetical protein
			transl_table	11
8000	8251	CDS
			locus_tag	Sylvanvirus_12_7
			product	hypothetical protein
			transl_table	11
8475	9332	CDS
			locus_tag	Sylvanvirus_12_8
			product	hypothetical protein
			transl_table	11
9771	10811	CDS
			locus_tag	Sylvanvirus_12_9
			product	hypothetical protein
			transl_table	11
11594	11136	CDS
			locus_tag	Sylvanvirus_12_10
			product	hypothetical protein
			transl_table	11
12012	11821	CDS
			locus_tag	Sylvanvirus_12_11
			product	hypothetical protein
			transl_table	11
13068	12208	CDS
			locus_tag	Sylvanvirus_12_12
			product	hypothetical protein
			transl_table	11
13469	13233	CDS
			locus_tag	Sylvanvirus_12_13
			product	hypothetical protein
			transl_table	11
14017	15243	CDS
			locus_tag	Sylvanvirus_12_14
			product	hypothetical protein
			transl_table	11
15395	16510	CDS
			locus_tag	Sylvanvirus_12_15
			product	hypothetical protein
			transl_table	11
17104	16646	CDS
			locus_tag	Sylvanvirus_12_16
			product	hypothetical protein
			transl_table	11
17312	19756	CDS
			locus_tag	Sylvanvirus_12_17
			product	hypothetical protein
			transl_table	11
19935	21671	CDS
			locus_tag	Sylvanvirus_12_18
			product	hypothetical protein Klosneuvirus_3_123
			transl_table	11
23314	21731	CDS
			locus_tag	Sylvanvirus_12_19
			product	hypothetical protein
			transl_table	11
23787	24344	CDS
			locus_tag	Sylvanvirus_12_20
			product	PREDICTED: E3 ubiquitin-protein ligase RNF12-B-like
			transl_table	11
24450	27275	CDS
			locus_tag	Sylvanvirus_12_21
			product	hypothetical protein
			transl_table	11
27404	28456	CDS
			locus_tag	Sylvanvirus_12_22
			product	hypothetical protein
			transl_table	11
28649	28494	CDS
			locus_tag	Sylvanvirus_12_23
			product	hypothetical protein
			transl_table	11
29191	28646	CDS
			locus_tag	Sylvanvirus_12_24
			product	hypothetical protein
			transl_table	11
29710	29964	CDS
			locus_tag	Sylvanvirus_12_25
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_13
94	669	CDS
			locus_tag	Sylvanvirus_13_1
			product	hypothetical protein
			transl_table	11
984	1259	CDS
			locus_tag	Sylvanvirus_13_2
			product	hypothetical protein
			transl_table	11
1966	1448	CDS
			locus_tag	Sylvanvirus_13_3
			product	hypothetical protein
			transl_table	11
2309	4588	CDS
			locus_tag	Sylvanvirus_13_4
			product	hypothetical protein
			transl_table	11
4680	6413	CDS
			locus_tag	Sylvanvirus_13_5
			product	GMC family oxidoreductase
			transl_table	11
7183	6485	CDS
			locus_tag	Sylvanvirus_13_6
			product	hypothetical protein
			transl_table	11
7198	7374	CDS
			locus_tag	Sylvanvirus_13_7
			product	hypothetical protein
			transl_table	11
7941	7510	CDS
			locus_tag	Sylvanvirus_13_8
			product	hypothetical protein
			transl_table	11
8564	9880	CDS
			locus_tag	Sylvanvirus_13_9
			product	hypothetical protein RclHR1_02680014
			transl_table	11
12000	9940	CDS
			locus_tag	Sylvanvirus_13_10
			product	hypothetical protein Ctob_001971
			transl_table	11
12869	12228	CDS
			locus_tag	Sylvanvirus_13_11
			product	hypothetical protein
			transl_table	11
13413	12955	CDS
			locus_tag	Sylvanvirus_13_12
			product	hypothetical protein
			transl_table	11
13541	13693	CDS
			locus_tag	Sylvanvirus_13_13
			product	hypothetical protein
			transl_table	11
13902	16838	CDS
			locus_tag	Sylvanvirus_13_14
			product	hypothetical protein
			transl_table	11
18256	18378	CDS
			locus_tag	Sylvanvirus_13_15
			product	hypothetical protein
			transl_table	11
18614	18375	CDS
			locus_tag	Sylvanvirus_13_16
			product	hypothetical protein
			transl_table	11
18883	19467	CDS
			locus_tag	Sylvanvirus_13_17
			product	hypothetical protein
			transl_table	11
20148	22625	CDS
			locus_tag	Sylvanvirus_13_18
			product	hypothetical protein
			transl_table	11
23289	22774	CDS
			locus_tag	Sylvanvirus_13_19
			product	hypothetical protein
			transl_table	11
23508	24761	CDS
			locus_tag	Sylvanvirus_13_20
			product	hypothetical protein
			transl_table	11
24805	25431	CDS
			locus_tag	Sylvanvirus_13_21
			product	hypothetical protein
			transl_table	11
25913	26044	CDS
			locus_tag	Sylvanvirus_13_22
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_14
518	3	CDS
			locus_tag	Sylvanvirus_14_1
			product	hypothetical protein
			transl_table	11
2501	696	CDS
			locus_tag	Sylvanvirus_14_2
			product	hypothetical protein
			transl_table	11
2760	3389	CDS
			locus_tag	Sylvanvirus_14_3
			product	hypothetical protein
			transl_table	11
4931	3429	CDS
			locus_tag	Sylvanvirus_14_4
			product	hypothetical protein
			transl_table	11
6211	5231	CDS
			locus_tag	Sylvanvirus_14_5
			product	hypothetical protein
			transl_table	11
6586	6918	CDS
			locus_tag	Sylvanvirus_14_6
			product	hypothetical protein
			transl_table	11
7917	6985	CDS
			locus_tag	Sylvanvirus_14_7
			product	hypothetical protein PBRA_003026
			transl_table	11
8444	8100	CDS
			locus_tag	Sylvanvirus_14_8
			product	hypothetical protein
			transl_table	11
8809	10320	CDS
			locus_tag	Sylvanvirus_14_9
			product	hypothetical protein
			transl_table	11
10570	10388	CDS
			locus_tag	Sylvanvirus_14_10
			product	hypothetical protein
			transl_table	11
11279	10896	CDS
			locus_tag	Sylvanvirus_14_11
			product	hypothetical protein
			transl_table	11
12143	11280	CDS
			locus_tag	Sylvanvirus_14_12
			product	hypothetical protein
			transl_table	11
12586	12293	CDS
			locus_tag	Sylvanvirus_14_13
			product	hypothetical protein
			transl_table	11
12922	14781	CDS
			locus_tag	Sylvanvirus_14_14
			product	hypothetical protein
			transl_table	11
14978	16003	CDS
			locus_tag	Sylvanvirus_14_15
			product	hypothetical protein
			transl_table	11
16153	18051	CDS
			locus_tag	Sylvanvirus_14_16
			product	hypothetical protein
			transl_table	11
19761	18076	CDS
			locus_tag	Sylvanvirus_14_17
			product	hypothetical protein
			transl_table	11
19760	19912	CDS
			locus_tag	Sylvanvirus_14_18
			product	hypothetical protein
			transl_table	11
20005	20991	CDS
			locus_tag	Sylvanvirus_14_19
			product	hypothetical protein
			transl_table	11
21159	22037	CDS
			locus_tag	Sylvanvirus_14_20
			product	hypothetical protein BOX15_Mlig016214g1, partial
			transl_table	11
22322	24274	CDS
			locus_tag	Sylvanvirus_14_21
			product	hypothetical protein Klosneuvirus_1_99
			transl_table	11
24391	24792	CDS
			locus_tag	Sylvanvirus_14_22
			product	hypothetical protein
			transl_table	11
25100	25195	CDS
			locus_tag	Sylvanvirus_14_23
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_15
602	3	CDS
			locus_tag	Sylvanvirus_15_1
			product	hypothetical protein
			transl_table	11
901	1365	CDS
			locus_tag	Sylvanvirus_15_2
			product	hypothetical protein
			transl_table	11
1445	3166	CDS
			locus_tag	Sylvanvirus_15_3
			product	hypothetical protein
			transl_table	11
3315	4994	CDS
			locus_tag	Sylvanvirus_15_4
			product	hypothetical protein
			transl_table	11
5345	5196	CDS
			locus_tag	Sylvanvirus_15_5
			product	hypothetical protein
			transl_table	11
5776	5411	CDS
			locus_tag	Sylvanvirus_15_6
			product	protein of unknown function DUF4326
			transl_table	11
7355	5853	CDS
			locus_tag	Sylvanvirus_15_7
			product	Ubiquitin protein ligase E3 component n-recognin 7
			transl_table	11
8612	7641	CDS
			locus_tag	Sylvanvirus_15_8
			product	hypothetical protein
			transl_table	11
8918	9325	CDS
			locus_tag	Sylvanvirus_15_9
			product	e3 ubiquitin-protein ligase rbx1
			transl_table	11
9736	9479	CDS
			locus_tag	Sylvanvirus_15_10
			product	RWD-domain-containing protein
			transl_table	11
10584	9871	CDS
			locus_tag	Sylvanvirus_15_11
			product	hypothetical protein
			transl_table	11
10698	12239	CDS
			locus_tag	Sylvanvirus_15_12
			product	hypothetical protein DICPUDRAFT_92938
			transl_table	11
14460	12289	CDS
			locus_tag	Sylvanvirus_15_13
			product	hypothetical protein
			transl_table	11
15447	14692	CDS
			locus_tag	Sylvanvirus_15_14
			product	hypothetical protein
			transl_table	11
16091	17317	CDS
			locus_tag	Sylvanvirus_15_15
			product	hypothetical protein
			transl_table	11
17825	17712	CDS
			locus_tag	Sylvanvirus_15_16
			product	hypothetical protein
			transl_table	11
17824	19656	CDS
			locus_tag	Sylvanvirus_15_17
			product	metallophosphoesterase
			transl_table	11
19926	19774	CDS
			locus_tag	Sylvanvirus_15_18
			product	hypothetical protein
			transl_table	11
20088	20501	CDS
			locus_tag	Sylvanvirus_15_19
			product	hypothetical protein
			transl_table	11
21777	20566	CDS
			locus_tag	Sylvanvirus_15_20
			product	hypothetical protein
			transl_table	11
23064	22273	CDS
			locus_tag	Sylvanvirus_15_21
			product	Flotillin domain-containing protein
			transl_table	11
>Sylvanvirus_16
436	239	CDS
			locus_tag	Sylvanvirus_16_1
			product	hypothetical protein
			transl_table	11
708	1289	CDS
			locus_tag	Sylvanvirus_16_2
			product	hypothetical protein
			transl_table	11
1453	1875	CDS
			locus_tag	Sylvanvirus_16_3
			product	hypothetical protein
			transl_table	11
2053	1949	CDS
			locus_tag	Sylvanvirus_16_4
			product	hypothetical protein
			transl_table	11
2052	2588	CDS
			locus_tag	Sylvanvirus_16_5
			product	PREDICTED: dual specificity protein phosphatase 1B-like
			transl_table	11
2725	3081	CDS
			locus_tag	Sylvanvirus_16_6
			product	hypothetical protein
			transl_table	11
3271	4311	CDS
			locus_tag	Sylvanvirus_16_7
			product	hypothetical protein LSAT_4X18440
			transl_table	11
6422	4776	CDS
			locus_tag	Sylvanvirus_16_8
			product	hypothetical protein
			transl_table	11
6896	6642	CDS
			locus_tag	Sylvanvirus_16_9
			product	hypothetical protein
			transl_table	11
7237	7377	CDS
			locus_tag	Sylvanvirus_16_10
			product	hypothetical protein
			transl_table	11
7454	7834	CDS
			locus_tag	Sylvanvirus_16_11
			product	hypothetical protein
			transl_table	11
8606	7974	CDS
			locus_tag	Sylvanvirus_16_12
			product	hypothetical protein ml_109
			transl_table	11
8851	9933	CDS
			locus_tag	Sylvanvirus_16_13
			product	tRNAHis guanylyltransferase
			transl_table	11
10153	13317	CDS
			locus_tag	Sylvanvirus_16_14
			product	hypothetical protein
			transl_table	11
14528	13614	CDS
			locus_tag	Sylvanvirus_16_15
			product	RING-H2 finger protein ATL73-like
			transl_table	11
14856	15902	CDS
			locus_tag	Sylvanvirus_16_16
			product	inhibitor of apoptosis-promoting Bax1 protein
			transl_table	11
15966	16961	CDS
			locus_tag	Sylvanvirus_16_17
			product	hypothetical protein
			transl_table	11
17320	17060	CDS
			locus_tag	Sylvanvirus_16_18
			product	hypothetical protein
			transl_table	11
17623	17880	CDS
			locus_tag	Sylvanvirus_16_19
			product	hypothetical protein
			transl_table	11
18767	17982	CDS
			locus_tag	Sylvanvirus_16_20
			product	hypothetical protein FISHEDRAFT_66677
			transl_table	11
19033	21957	CDS
			locus_tag	Sylvanvirus_16_21
			product	hypothetical protein PBRA_007849
			transl_table	11
22244	22122	CDS
			locus_tag	Sylvanvirus_16_22
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_17
417	827	CDS
			locus_tag	Sylvanvirus_17_1
			product	nucleic acid-binding protein
			transl_table	11
1360	884	CDS
			locus_tag	Sylvanvirus_17_2
			product	hypothetical protein
			transl_table	11
1770	2153	CDS
			locus_tag	Sylvanvirus_17_3
			product	hypothetical protein
			transl_table	11
2394	3026	CDS
			locus_tag	Sylvanvirus_17_4
			product	hypothetical protein
			transl_table	11
3346	3083	CDS
			locus_tag	Sylvanvirus_17_5
			product	hypothetical protein
			transl_table	11
3827	3471	CDS
			locus_tag	Sylvanvirus_17_6
			product	hypothetical protein
			transl_table	11
4081	7935	CDS
			locus_tag	Sylvanvirus_17_7
			product	hypothetical protein
			transl_table	11
8846	8574	CDS
			locus_tag	Sylvanvirus_17_8
			product	hypothetical protein
			transl_table	11
9698	9111	CDS
			locus_tag	Sylvanvirus_17_9
			product	hypothetical protein
			transl_table	11
10252	10142	CDS
			locus_tag	Sylvanvirus_17_10
			product	hypothetical protein
			transl_table	11
12170	10380	CDS
			locus_tag	Sylvanvirus_17_11
			product	GMC-type oxidoreductase
			transl_table	11
13290	13649	CDS
			locus_tag	Sylvanvirus_17_12
			product	hypothetical protein
			transl_table	11
13809	15059	CDS
			locus_tag	Sylvanvirus_17_13
			product	patatin
			transl_table	11
15268	19509	CDS
			locus_tag	Sylvanvirus_17_14
			product	PREDICTED: DNA topoisomerase 2-like isoform X1
			transl_table	11
19815	19576	CDS
			locus_tag	Sylvanvirus_17_15
			product	hypothetical protein
			transl_table	11
21284	19932	CDS
			locus_tag	Sylvanvirus_17_16
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_18
1750	2	CDS
			locus_tag	Sylvanvirus_18_1
			product	hypothetical protein
			transl_table	11
2130	2987	CDS
			locus_tag	Sylvanvirus_18_2
			product	hypothetical protein
			transl_table	11
3620	3105	CDS
			locus_tag	Sylvanvirus_18_3
			product	hypothetical protein
			transl_table	11
3929	5098	CDS
			locus_tag	Sylvanvirus_18_4
			product	hypothetical protein G7K_5926-t1
			transl_table	11
5641	5183	CDS
			locus_tag	Sylvanvirus_18_5
			product	hypothetical protein
			transl_table	11
7655	5814	CDS
			locus_tag	Sylvanvirus_18_6
			product	serine/threonine protein kinase, partial
			transl_table	11
7929	7672	CDS
			locus_tag	Sylvanvirus_18_7
			product	hypothetical protein
			transl_table	11
8914	8345	CDS
			locus_tag	Sylvanvirus_18_8
			product	hypothetical protein
			transl_table	11
9645	9061	CDS
			locus_tag	Sylvanvirus_18_9
			product	hypothetical protein
			transl_table	11
9930	10730	CDS
			locus_tag	Sylvanvirus_18_10
			product	hypothetical protein
			transl_table	11
11378	10992	CDS
			locus_tag	Sylvanvirus_18_11
			product	hypothetical protein
			transl_table	11
12044	11574	CDS
			locus_tag	Sylvanvirus_18_12
			product	hypothetical protein
			transl_table	11
13185	12235	CDS
			locus_tag	Sylvanvirus_18_13
			product	MULTISPECIES: alpha/beta hydrolase
			transl_table	11
13764	14780	CDS
			locus_tag	Sylvanvirus_18_14
			product	hypothetical protein
			transl_table	11
16040	15078	CDS
			locus_tag	Sylvanvirus_18_15
			product	DUF4804 domain-containing protein
			transl_table	11
16515	16162	CDS
			locus_tag	Sylvanvirus_18_16
			product	hypothetical protein
			transl_table	11
17358	16786	CDS
			locus_tag	Sylvanvirus_18_17
			product	hypothetical protein
			transl_table	11
17732	18097	CDS
			locus_tag	Sylvanvirus_18_18
			product	hypothetical protein
			transl_table	11
18470	18156	CDS
			locus_tag	Sylvanvirus_18_19
			product	hypothetical protein
			transl_table	11
20106	18544	CDS
			locus_tag	Sylvanvirus_18_20
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_19
2219	654	CDS
			locus_tag	Sylvanvirus_19_1
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
2464	3729	CDS
			locus_tag	Sylvanvirus_19_2
			product	hypothetical protein
			transl_table	11
4125	3856	CDS
			locus_tag	Sylvanvirus_19_3
			product	hypothetical protein
			transl_table	11
5027	4287	CDS
			locus_tag	Sylvanvirus_19_4
			product	hypothetical protein
			transl_table	11
5324	6373	CDS
			locus_tag	Sylvanvirus_19_5
			product	hypothetical protein
			transl_table	11
6527	7384	CDS
			locus_tag	Sylvanvirus_19_6
			product	hypothetical protein
			transl_table	11
7509	7381	CDS
			locus_tag	Sylvanvirus_19_7
			product	hypothetical protein
			transl_table	11
7508	8101	CDS
			locus_tag	Sylvanvirus_19_8
			product	uncharacterized protein LOC111128849
			transl_table	11
9065	8151	CDS
			locus_tag	Sylvanvirus_19_9
			product	hypothetical protein
			transl_table	11
9350	10927	CDS
			locus_tag	Sylvanvirus_19_10
			product	hypothetical protein
			transl_table	11
10966	12147	CDS
			locus_tag	Sylvanvirus_19_11
			product	Hypothetical protein ORPV_228
			transl_table	11
13115	12252	CDS
			locus_tag	Sylvanvirus_19_12
			product	HNH endonuclease
			transl_table	11
14263	13271	CDS
			locus_tag	Sylvanvirus_19_13
			product	hypothetical protein
			transl_table	11
14523	14963	CDS
			locus_tag	Sylvanvirus_19_14
			product	hypothetical protein
			transl_table	11
15643	15071	CDS
			locus_tag	Sylvanvirus_19_15
			product	NADAR family protein
			transl_table	11
15952	16908	CDS
			locus_tag	Sylvanvirus_19_16
			product	hypothetical protein
			transl_table	11
17098	17316	CDS
			locus_tag	Sylvanvirus_19_17
			product	hypothetical protein
			transl_table	11
17971	17354	CDS
			locus_tag	Sylvanvirus_19_18
			product	hypothetical protein KFL_016910010
			transl_table	11
>Sylvanvirus_20
3512	3585	tRNA	Sylvanvirus_tRNA_3
			product	tRNA-Arg(TCG)
175	732	CDS
			locus_tag	Sylvanvirus_20_1
			product	hypothetical protein
			transl_table	11
1571	924	CDS
			locus_tag	Sylvanvirus_20_2
			product	hypothetical protein
			transl_table	11
2627	1755	CDS
			locus_tag	Sylvanvirus_20_3
			product	syntaxin-2 isoform X3
			transl_table	11
3411	2713	CDS
			locus_tag	Sylvanvirus_20_4
			product	ribonuclease HI
			transl_table	11
3403	3591	CDS
			locus_tag	Sylvanvirus_20_5
			product	hypothetical protein
			transl_table	11
3741	4415	CDS
			locus_tag	Sylvanvirus_20_6
			product	hypothetical protein
			transl_table	11
4713	6470	CDS
			locus_tag	Sylvanvirus_20_7
			product	hypothetical protein
			transl_table	11
9476	6510	CDS
			locus_tag	Sylvanvirus_20_8
			product	hypothetical protein
			transl_table	11
9727	10467	CDS
			locus_tag	Sylvanvirus_20_9
			product	hypothetical protein
			transl_table	11
10573	11031	CDS
			locus_tag	Sylvanvirus_20_10
			product	putative nnucleoredoxin
			transl_table	11
12177	11059	CDS
			locus_tag	Sylvanvirus_20_11
			product	hypothetical protein
			transl_table	11
12448	13524	CDS
			locus_tag	Sylvanvirus_20_12
			product	hypothetical protein
			transl_table	11
13753	17184	CDS
			locus_tag	Sylvanvirus_20_13
			product	hypothetical protein
			transl_table	11
17393	17253	CDS
			locus_tag	Sylvanvirus_20_14
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_21
6260	6180	tRNA	Sylvanvirus_tRNA_4
			product	tRNA-Leu(AAG)
1907	468	CDS
			locus_tag	Sylvanvirus_21_1
			product	hypothetical protein SAMD00019534_059800
			transl_table	11
2463	2828	CDS
			locus_tag	Sylvanvirus_21_2
			product	hypothetical protein
			transl_table	11
3347	3778	CDS
			locus_tag	Sylvanvirus_21_3
			product	hypothetical protein
			transl_table	11
3970	5109	CDS
			locus_tag	Sylvanvirus_21_4
			product	hypothetical protein
			transl_table	11
5797	5312	CDS
			locus_tag	Sylvanvirus_21_5
			product	hypothetical protein
			transl_table	11
6594	7559	CDS
			locus_tag	Sylvanvirus_21_6
			product	hypothetical protein
			transl_table	11
10090	7598	CDS
			locus_tag	Sylvanvirus_21_7
			product	hypothetical protein
			transl_table	11
10496	10912	CDS
			locus_tag	Sylvanvirus_21_8
			product	Histone H3
			transl_table	11
11078	11413	CDS
			locus_tag	Sylvanvirus_21_9
			product	hypothetical protein
			transl_table	11
12667	11441	CDS
			locus_tag	Sylvanvirus_21_10
			product	hypothetical protein
			transl_table	11
12887	13912	CDS
			locus_tag	Sylvanvirus_21_11
			product	hypothetical protein
			transl_table	11
14561	13884	CDS
			locus_tag	Sylvanvirus_21_12
			product	hypothetical protein
			transl_table	11
15936	14677	CDS
			locus_tag	Sylvanvirus_21_13
			product	PREDICTED: cyclin-dependent kinase A-1
			transl_table	11
>Sylvanvirus_22
69	164	CDS
			locus_tag	Sylvanvirus_22_1
			product	hypothetical protein
			transl_table	11
2204	261	CDS
			locus_tag	Sylvanvirus_22_2
			product	hypothetical protein
			transl_table	11
2744	2652	CDS
			locus_tag	Sylvanvirus_22_3
			product	hypothetical protein
			transl_table	11
3327	3911	CDS
			locus_tag	Sylvanvirus_22_4
			product	hypothetical protein
			transl_table	11
4464	4985	CDS
			locus_tag	Sylvanvirus_22_5
			product	hypothetical protein
			transl_table	11
5276	5734	CDS
			locus_tag	Sylvanvirus_22_6
			product	hypothetical protein
			transl_table	11
6420	5800	CDS
			locus_tag	Sylvanvirus_22_7
			product	hypothetical protein PSTG_13015
			transl_table	11
6809	8167	CDS
			locus_tag	Sylvanvirus_22_8
			product	hypothetical protein
			transl_table	11
8539	8742	CDS
			locus_tag	Sylvanvirus_22_9
			product	hypothetical protein
			transl_table	11
11673	9256	CDS
			locus_tag	Sylvanvirus_22_10
			product	hypothetical protein H257_07846
			transl_table	11
11798	11959	CDS
			locus_tag	Sylvanvirus_22_11
			product	hypothetical protein
			transl_table	11
12406	13203	CDS
			locus_tag	Sylvanvirus_22_12
			product	IS5/IS1182 family transposase
			transl_table	11
14519	13968	CDS
			locus_tag	Sylvanvirus_22_13
			product	hypothetical protein
			transl_table	11
15091	14954	CDS
			locus_tag	Sylvanvirus_22_14
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_23
1876	2	CDS
			locus_tag	Sylvanvirus_23_1
			product	hypothetical protein
			transl_table	11
2217	3119	CDS
			locus_tag	Sylvanvirus_23_2
			product	Hypothetical protein ORPV_1171
			transl_table	11
4418	3204	CDS
			locus_tag	Sylvanvirus_23_3
			product	hypothetical protein
			transl_table	11
4696	6690	CDS
			locus_tag	Sylvanvirus_23_4
			product	mitogenactivated protein kinase putative
			transl_table	11
7280	7855	CDS
			locus_tag	Sylvanvirus_23_5
			product	hypothetical protein
			transl_table	11
8938	7967	CDS
			locus_tag	Sylvanvirus_23_6
			product	PREDICTED: fatty acid hydroxylase domain-containing protein 2
			transl_table	11
10569	9043	CDS
			locus_tag	Sylvanvirus_23_7
			product	hypothetical protein
			transl_table	11
10877	13042	CDS
			locus_tag	Sylvanvirus_23_8
			product	probable RNA helicase SDE3
			transl_table	11
13223	13987	CDS
			locus_tag	Sylvanvirus_23_9
			product	hypothetical protein
			transl_table	11
14236	14625	CDS
			locus_tag	Sylvanvirus_23_10
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_24
330	1	CDS
			locus_tag	Sylvanvirus_24_1
			product	hypothetical protein
			transl_table	11
1001	552	CDS
			locus_tag	Sylvanvirus_24_2
			product	hypothetical protein
			transl_table	11
1313	2869	CDS
			locus_tag	Sylvanvirus_24_3
			product	hypothetical protein
			transl_table	11
3075	4403	CDS
			locus_tag	Sylvanvirus_24_4
			product	hypothetical protein
			transl_table	11
5230	4433	CDS
			locus_tag	Sylvanvirus_24_5
			product	PREDICTED: uncharacterized protein LOC103969238
			transl_table	11
5331	5999	CDS
			locus_tag	Sylvanvirus_24_6
			product	hypothetical protein
			transl_table	11
6230	6904	CDS
			locus_tag	Sylvanvirus_24_7
			product	hypothetical protein
			transl_table	11
7169	7017	CDS
			locus_tag	Sylvanvirus_24_8
			product	hypothetical protein
			transl_table	11
9248	7647	CDS
			locus_tag	Sylvanvirus_24_9
			product	hypothetical protein
			transl_table	11
10768	9740	CDS
			locus_tag	Sylvanvirus_24_10
			product	hypothetical protein
			transl_table	11
13332	11251	CDS
			locus_tag	Sylvanvirus_24_11
			product	TNF receptor-associated factor 6-like isoform X4
			transl_table	11
13580	14137	CDS
			locus_tag	Sylvanvirus_24_12
			product	hypothetical protein
			transl_table	11
14475	14389	CDS
			locus_tag	Sylvanvirus_24_13
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_25
4055	3984	tRNA	Sylvanvirus_tRNA_5
			product	tRNA-Pro(CGG)
87	1	CDS
			locus_tag	Sylvanvirus_25_1
			product	hypothetical protein
			transl_table	11
353	3253	CDS
			locus_tag	Sylvanvirus_25_2
			product	PREDICTED: puromycin-sensitive aminopeptidase isoform X1
			transl_table	11
3494	3946	CDS
			locus_tag	Sylvanvirus_25_3
			product	hypothetical protein
			transl_table	11
4769	4110	CDS
			locus_tag	Sylvanvirus_25_4
			product	hypothetical protein
			transl_table	11
5787	4849	CDS
			locus_tag	Sylvanvirus_25_5
			product	hypothetical protein
			transl_table	11
6057	6374	CDS
			locus_tag	Sylvanvirus_25_6
			product	hypothetical protein
			transl_table	11
6858	6400	CDS
			locus_tag	Sylvanvirus_25_7
			product	hypothetical protein
			transl_table	11
7132	8958	CDS
			locus_tag	Sylvanvirus_25_8
			product	phospholipase
			transl_table	11
10799	9114	CDS
			locus_tag	Sylvanvirus_25_9
			product	hypothetical protein
			transl_table	11
11000	11920	CDS
			locus_tag	Sylvanvirus_25_10
			product	hypothetical protein
			transl_table	11
12490	12029	CDS
			locus_tag	Sylvanvirus_25_11
			product	hypothetical protein
			transl_table	11
13985	14098	CDS
			locus_tag	Sylvanvirus_25_12
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_26
211	1566	CDS
			locus_tag	Sylvanvirus_26_1
			product	hypothetical protein
			transl_table	11
1973	1668	CDS
			locus_tag	Sylvanvirus_26_2
			product	hypothetical protein
			transl_table	11
2219	2713	CDS
			locus_tag	Sylvanvirus_26_3
			product	hypothetical protein
			transl_table	11
4495	3002	CDS
			locus_tag	Sylvanvirus_26_4
			product	hypothetical protein ENSA5_28510
			transl_table	11
4599	4483	CDS
			locus_tag	Sylvanvirus_26_5
			product	hypothetical protein
			transl_table	11
5221	4925	CDS
			locus_tag	Sylvanvirus_26_6
			product	hypothetical protein
			transl_table	11
6032	6388	CDS
			locus_tag	Sylvanvirus_26_7
			product	hypothetical protein
			transl_table	11
6819	6460	CDS
			locus_tag	Sylvanvirus_26_8
			product	hypothetical protein
			transl_table	11
7417	6905	CDS
			locus_tag	Sylvanvirus_26_9
			product	hypothetical protein H257_11752
			transl_table	11
8188	7568	CDS
			locus_tag	Sylvanvirus_26_10
			product	hypothetical protein PBRA_004022
			transl_table	11
8523	8771	CDS
			locus_tag	Sylvanvirus_26_11
			product	hypothetical protein
			transl_table	11
8848	9186	CDS
			locus_tag	Sylvanvirus_26_12
			product	hypothetical protein
			transl_table	11
9372	9575	CDS
			locus_tag	Sylvanvirus_26_13
			product	hypothetical protein
			transl_table	11
10427	9669	CDS
			locus_tag	Sylvanvirus_26_14
			product	hypothetical protein
			transl_table	11
11381	11160	CDS
			locus_tag	Sylvanvirus_26_15
			product	hypothetical protein
			transl_table	11
12148	12321	CDS
			locus_tag	Sylvanvirus_26_16
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_27
522	2009	CDS
			locus_tag	Sylvanvirus_27_1
			product	hypothetical protein
			transl_table	11
2197	3291	CDS
			locus_tag	Sylvanvirus_27_2
			product	hypothetical protein
			transl_table	11
3300	4205	CDS
			locus_tag	Sylvanvirus_27_3
			product	hypothetical protein
			transl_table	11
4305	5315	CDS
			locus_tag	Sylvanvirus_27_4
			product	hypothetical protein pv_1
			transl_table	11
5525	6022	CDS
			locus_tag	Sylvanvirus_27_5
			product	hypothetical protein
			transl_table	11
6338	6063	CDS
			locus_tag	Sylvanvirus_27_6
			product	hypothetical protein
			transl_table	11
6505	6335	CDS
			locus_tag	Sylvanvirus_27_7
			product	hypothetical protein
			transl_table	11
6636	6995	CDS
			locus_tag	Sylvanvirus_27_8
			product	hypothetical protein
			transl_table	11
8127	7453	CDS
			locus_tag	Sylvanvirus_27_9
			product	hypothetical protein
			transl_table	11
8472	10622	CDS
			locus_tag	Sylvanvirus_27_10
			product	hypothetical protein P12024L_26
			transl_table	11
>Sylvanvirus_28
1	234	CDS
			locus_tag	Sylvanvirus_28_1
			product	hypothetical protein
			transl_table	11
1757	345	CDS
			locus_tag	Sylvanvirus_28_2
			product	hypothetical protein
			transl_table	11
2655	1882	CDS
			locus_tag	Sylvanvirus_28_3
			product	hypothetical protein
			transl_table	11
2907	4178	CDS
			locus_tag	Sylvanvirus_28_4
			product	RecName: Full=Replication factor C small subunit; Short=RFC small subunit; AltName: Full=Clamp loader small subunit >BAA80521.2 replication factor C small subunit
			transl_table	11
4760	4191	CDS
			locus_tag	Sylvanvirus_28_5
			product	hypothetical protein Catovirus_1_29
			transl_table	11
5301	4954	CDS
			locus_tag	Sylvanvirus_28_6
			product	hypothetical protein
			transl_table	11
6651	5767	CDS
			locus_tag	Sylvanvirus_28_7
			product	hypothetical protein
			transl_table	11
6998	7141	CDS
			locus_tag	Sylvanvirus_28_8
			product	hypothetical protein
			transl_table	11
8116	7658	CDS
			locus_tag	Sylvanvirus_28_9
			product	hypothetical protein
			transl_table	11
8598	9569	CDS
			locus_tag	Sylvanvirus_28_10
			product	hypothetical protein
			transl_table	11
9800	10393	CDS
			locus_tag	Sylvanvirus_28_11
			product	hypothetical protein
			transl_table	11
10555	10818	CDS
			locus_tag	Sylvanvirus_28_12
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_29
7773	7878	tRNA	Sylvanvirus_tRNA_6
			product	tRNA-Ser(GCT)
1746	1	CDS
			locus_tag	Sylvanvirus_29_1
			product	hypothetical protein KFL_007940060
			transl_table	11
2077	2580	CDS
			locus_tag	Sylvanvirus_29_2
			product	hypothetical protein
			transl_table	11
2903	3853	CDS
			locus_tag	Sylvanvirus_29_3
			product	hypothetical protein
			transl_table	11
3988	4416	CDS
			locus_tag	Sylvanvirus_29_4
			product	hypothetical protein
			transl_table	11
4812	5165	CDS
			locus_tag	Sylvanvirus_29_5
			product	hypothetical protein
			transl_table	11
6973	5186	CDS
			locus_tag	Sylvanvirus_29_6
			product	hypothetical protein
			transl_table	11
7082	7528	CDS
			locus_tag	Sylvanvirus_29_7
			product	SNARE domain protein
			transl_table	11
7736	7915	CDS
			locus_tag	Sylvanvirus_29_8
			product	hypothetical protein
			transl_table	11
8025	7912	CDS
			locus_tag	Sylvanvirus_29_9
			product	hypothetical protein
			transl_table	11
8024	9832	CDS
			locus_tag	Sylvanvirus_29_10
			product	PAS domain S-box protein
			transl_table	11
10258	10395	CDS
			locus_tag	Sylvanvirus_29_11
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_30
622	2	CDS
			locus_tag	Sylvanvirus_30_1
			product	hypothetical protein
			transl_table	11
1114	788	CDS
			locus_tag	Sylvanvirus_30_2
			product	hypothetical protein
			transl_table	11
1454	1759	CDS
			locus_tag	Sylvanvirus_30_3
			product	hypothetical protein
			transl_table	11
1750	2193	CDS
			locus_tag	Sylvanvirus_30_4
			product	hypothetical protein
			transl_table	11
2144	4105	CDS
			locus_tag	Sylvanvirus_30_5
			product	PREDICTED: cyclin-D5-1
			transl_table	11
4262	6367	CDS
			locus_tag	Sylvanvirus_30_6
			product	hypothetical protein
			transl_table	11
6465	6707	CDS
			locus_tag	Sylvanvirus_30_7
			product	hypothetical protein
			transl_table	11
6759	6929	CDS
			locus_tag	Sylvanvirus_30_8
			product	hypothetical protein
			transl_table	11
7054	7791	CDS
			locus_tag	Sylvanvirus_30_9
			product	hypothetical protein
			transl_table	11
8154	7807	CDS
			locus_tag	Sylvanvirus_30_10
			product	hypothetical protein
			transl_table	11
9680	8784	CDS
			locus_tag	Sylvanvirus_30_11
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_31
1375	779	CDS
			locus_tag	Sylvanvirus_31_1
			product	hypothetical protein
			transl_table	11
1454	2821	CDS
			locus_tag	Sylvanvirus_31_2
			product	hypothetical protein
			transl_table	11
4058	3582	CDS
			locus_tag	Sylvanvirus_31_3
			product	hypothetical protein
			transl_table	11
4389	5429	CDS
			locus_tag	Sylvanvirus_31_4
			product	hypothetical protein
			transl_table	11
7182	5959	CDS
			locus_tag	Sylvanvirus_31_5
			product	hypothetical protein IMG5_024510
			transl_table	11
8055	7498	CDS
			locus_tag	Sylvanvirus_31_6
			product	hypothetical protein
			transl_table	11
8958	8128	CDS
			locus_tag	Sylvanvirus_31_7
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_32
259	2	CDS
			locus_tag	Sylvanvirus_32_1
			product	hypothetical protein
			transl_table	11
1105	455	CDS
			locus_tag	Sylvanvirus_32_2
			product	hypothetical protein
			transl_table	11
2187	1243	CDS
			locus_tag	Sylvanvirus_32_3
			product	hypothetical protein H257_04392
			transl_table	11
4210	2318	CDS
			locus_tag	Sylvanvirus_32_4
			product	hypothetical protein
			transl_table	11
4299	4589	CDS
			locus_tag	Sylvanvirus_32_5
			product	hypothetical protein
			transl_table	11
5652	4654	CDS
			locus_tag	Sylvanvirus_32_6
			product	zf-CHY-domain-containing protein
			transl_table	11
7444	5846	CDS
			locus_tag	Sylvanvirus_32_7
			product	hypothetical protein
			transl_table	11
7903	8049	CDS
			locus_tag	Sylvanvirus_32_8
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_33
2	1657	CDS
			locus_tag	Sylvanvirus_33_1
			product	hypothetical protein
			transl_table	11
3023	1710	CDS
			locus_tag	Sylvanvirus_33_2
			product	hypothetical protein
			transl_table	11
3439	3206	CDS
			locus_tag	Sylvanvirus_33_3
			product	hypothetical protein
			transl_table	11
5183	3666	CDS
			locus_tag	Sylvanvirus_33_4
			product	putative tRNA 2'-phosphotransferase
			transl_table	11
5711	5959	CDS
			locus_tag	Sylvanvirus_33_5
			product	hypothetical protein
			transl_table	11
6294	6983	CDS
			locus_tag	Sylvanvirus_33_6
			product	hypothetical protein
			transl_table	11
7479	7210	CDS
			locus_tag	Sylvanvirus_33_7
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_34
1	111	CDS
			locus_tag	Sylvanvirus_34_1
			product	hypothetical protein
			transl_table	11
307	855	CDS
			locus_tag	Sylvanvirus_34_2
			product	hypothetical protein
			transl_table	11
959	1114	CDS
			locus_tag	Sylvanvirus_34_3
			product	hypothetical protein
			transl_table	11
1479	1700	CDS
			locus_tag	Sylvanvirus_34_4
			product	hypothetical protein
			transl_table	11
1956	1711	CDS
			locus_tag	Sylvanvirus_34_5
			product	hypothetical protein
			transl_table	11
2200	1961	CDS
			locus_tag	Sylvanvirus_34_6
			product	hypothetical protein
			transl_table	11
2484	2356	CDS
			locus_tag	Sylvanvirus_34_7
			product	hypothetical protein
			transl_table	11
3242	2562	CDS
			locus_tag	Sylvanvirus_34_8
			product	hypothetical protein
			transl_table	11
3726	3199	CDS
			locus_tag	Sylvanvirus_34_9
			product	hypothetical protein
			transl_table	11
3679	4617	CDS
			locus_tag	Sylvanvirus_34_10
			product	hypothetical protein
			transl_table	11
4947	6440	CDS
			locus_tag	Sylvanvirus_34_11
			product	hypothetical protein JH06_1394
			transl_table	11
>Sylvanvirus_35
696	1	CDS
			locus_tag	Sylvanvirus_35_1
			product	hypothetical protein
			transl_table	11
2898	1153	CDS
			locus_tag	Sylvanvirus_35_2
			product	hypothetical protein
			transl_table	11
2897	2995	CDS
			locus_tag	Sylvanvirus_35_3
			product	hypothetical protein
			transl_table	11
3264	3127	CDS
			locus_tag	Sylvanvirus_35_4
			product	hypothetical protein
			transl_table	11
3370	3756	CDS
			locus_tag	Sylvanvirus_35_5
			product	hypothetical protein
			transl_table	11
4456	4202	CDS
			locus_tag	Sylvanvirus_35_6
			product	hypothetical protein
			transl_table	11
4787	4590	CDS
			locus_tag	Sylvanvirus_35_7
			product	hypothetical protein
			transl_table	11
5105	4833	CDS
			locus_tag	Sylvanvirus_35_8
			product	hypothetical protein
			transl_table	11
5861	5748	CDS
			locus_tag	Sylvanvirus_35_9
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_36
242	3	CDS
			locus_tag	Sylvanvirus_36_1
			product	hypothetical protein
			transl_table	11
2015	372	CDS
			locus_tag	Sylvanvirus_36_2
			product	hypothetical protein
			transl_table	11
2212	3444	CDS
			locus_tag	Sylvanvirus_36_3
			product	hypothetical protein
			transl_table	11
4935	3478	CDS
			locus_tag	Sylvanvirus_36_4
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_37
1947	1	CDS
			locus_tag	Sylvanvirus_37_1
			product	hypothetical protein
			transl_table	11
2407	2135	CDS
			locus_tag	Sylvanvirus_37_2
			product	hypothetical protein
			transl_table	11
2705	4231	CDS
			locus_tag	Sylvanvirus_37_3
			product	hypothetical protein RirG_037770
			transl_table	11
4401	4619	CDS
			locus_tag	Sylvanvirus_37_4
			product	hypothetical protein
			transl_table	11
4761	4663	CDS
			locus_tag	Sylvanvirus_37_5
			product	hypothetical protein
			transl_table	11
5333	5452	CDS
			locus_tag	Sylvanvirus_37_6
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_38
195	1	CDS
			locus_tag	Sylvanvirus_38_1
			product	hypothetical protein
			transl_table	11
389	1045	CDS
			locus_tag	Sylvanvirus_38_2
			product	hypothetical protein
			transl_table	11
1860	1186	CDS
			locus_tag	Sylvanvirus_38_3
			product	hypothetical protein
			transl_table	11
2189	2812	CDS
			locus_tag	Sylvanvirus_38_4
			product	hypothetical protein
			transl_table	11
3691	2858	CDS
			locus_tag	Sylvanvirus_38_5
			product	predicted protein
			transl_table	11
4018	4398	CDS
			locus_tag	Sylvanvirus_38_6
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_39
368	991	CDS
			locus_tag	Sylvanvirus_39_1
			product	hypothetical protein
			transl_table	11
2643	1021	CDS
			locus_tag	Sylvanvirus_39_2
			product	hypothetical protein
			transl_table	11
2855	3571	CDS
			locus_tag	Sylvanvirus_39_3
			product	hypothetical protein
			transl_table	11
3902	3639	CDS
			locus_tag	Sylvanvirus_39_4
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_40
980	255	CDS
			locus_tag	Sylvanvirus_40_1
			product	hypothetical protein
			transl_table	11
1622	2134	CDS
			locus_tag	Sylvanvirus_40_2
			product	hypothetical protein
			transl_table	11
2387	2151	CDS
			locus_tag	Sylvanvirus_40_3
			product	hypothetical protein
			transl_table	11
3581	3769	CDS
			locus_tag	Sylvanvirus_40_4
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_41
1	522	CDS
			locus_tag	Sylvanvirus_41_1
			product	hypothetical protein
			transl_table	11
545	1696	CDS
			locus_tag	Sylvanvirus_41_2
			product	Bifunctional aspartate aminotransferase
			transl_table	11
2059	3063	CDS
			locus_tag	Sylvanvirus_41_3
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_42
2	106	CDS
			locus_tag	Sylvanvirus_42_1
			product	hypothetical protein
			transl_table	11
464	195	CDS
			locus_tag	Sylvanvirus_42_2
			product	hypothetical protein
			transl_table	11
757	509	CDS
			locus_tag	Sylvanvirus_42_3
			product	hypothetical protein
			transl_table	11
1342	1025	CDS
			locus_tag	Sylvanvirus_42_4
			product	hypothetical protein
			transl_table	11
1436	3160	CDS
			locus_tag	Sylvanvirus_42_5
			product	hypothetical protein
			transl_table	11
3586	3672	CDS
			locus_tag	Sylvanvirus_42_6
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_43
445	948	CDS
			locus_tag	Sylvanvirus_43_1
			product	hypothetical protein
			transl_table	11
970	1269	CDS
			locus_tag	Sylvanvirus_43_2
			product	hypothetical protein
			transl_table	11
1664	1383	CDS
			locus_tag	Sylvanvirus_43_3
			product	hypothetical protein
			transl_table	11
1842	1973	CDS
			locus_tag	Sylvanvirus_43_4
			product	hypothetical protein
			transl_table	11
1958	3310	CDS
			locus_tag	Sylvanvirus_43_5
			product	Sputnik V21 virophage-like protein
			transl_table	11
3568	3377	CDS
			locus_tag	Sylvanvirus_43_6
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_44
944	2464	CDS
			locus_tag	Sylvanvirus_44_1
			product	phospholipase
			transl_table	11
3533	2616	CDS
			locus_tag	Sylvanvirus_44_2
			product	hypothetical protein
			transl_table	11
>Sylvanvirus_45
946	2	CDS
			locus_tag	Sylvanvirus_45_1
			product	hypothetical protein
			transl_table	11
1898	1083	CDS
			locus_tag	Sylvanvirus_45_2
			product	hypothetical protein
			transl_table	11
3287	2013	CDS
			locus_tag	Sylvanvirus_45_3
			product	hypothetical protein
			transl_table	11
