>Terrestrivirus_1
461	3	CDS
			locus_tag	Terrestrivirus_1_1
			product	Fad/fmn-containing dehydrogenase
			transl_table	11
3280	533	CDS
			locus_tag	Terrestrivirus_1_2
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
3820	3305	CDS
			locus_tag	Terrestrivirus_1_3
			product	hypothetical protein
			transl_table	11
4941	3931	CDS
			locus_tag	Terrestrivirus_1_4
			product	hypothetical protein 162275868
			transl_table	11
5385	5855	CDS
			locus_tag	Terrestrivirus_1_5
			product	hypothetical protein
			transl_table	11
5949	7025	CDS
			locus_tag	Terrestrivirus_1_6
			product	hypothetical protein
			transl_table	11
8389	7037	CDS
			locus_tag	Terrestrivirus_1_7
			product	Rieske domain-containing protein
			transl_table	11
9221	8472	CDS
			locus_tag	Terrestrivirus_1_8
			product	hypothetical protein
			transl_table	11
10138	9302	CDS
			locus_tag	Terrestrivirus_1_9
			product	hypothetical protein
			transl_table	11
10112	10288	CDS
			locus_tag	Terrestrivirus_1_10
			product	hypothetical protein
			transl_table	11
11088	10294	CDS
			locus_tag	Terrestrivirus_1_11
			product	glycerophosphodiester phosphodiesterase
			transl_table	11
11854	11183	CDS
			locus_tag	Terrestrivirus_1_12
			product	ras-domain-containing protein
			transl_table	11
11989	13824	CDS
			locus_tag	Terrestrivirus_1_13
			product	DUF229 domain-containing protein
			transl_table	11
13894	14268	CDS
			locus_tag	Terrestrivirus_1_14
			product	hypothetical protein
			transl_table	11
14461	15390	CDS
			locus_tag	Terrestrivirus_1_15
			product	TIGR03118 family protein
			transl_table	11
15518	16150	CDS
			locus_tag	Terrestrivirus_1_16
			product	ras-related protein RIC1-like
			transl_table	11
16833	16156	CDS
			locus_tag	Terrestrivirus_1_17
			product	MULTISPECIES: hypothetical protein
			transl_table	11
17326	16946	CDS
			locus_tag	Terrestrivirus_1_18
			product	5'-AMP-activated protein kinase subunit beta-2-like
			transl_table	11
17698	17546	CDS
			locus_tag	Terrestrivirus_1_19
			product	hypothetical protein
			transl_table	11
17839	20598	CDS
			locus_tag	Terrestrivirus_1_20
			product	plasma membrane ATPase
			transl_table	11
20695	20802	CDS
			locus_tag	Terrestrivirus_1_21
			product	hypothetical protein
			transl_table	11
20821	21822	CDS
			locus_tag	Terrestrivirus_1_22
			product	Restriction endonuclease
			transl_table	11
21841	22161	CDS
			locus_tag	Terrestrivirus_1_23
			product	hypothetical protein c7_R1171
			transl_table	11
23857	22163	CDS
			locus_tag	Terrestrivirus_1_24
			product	hypothetical protein
			transl_table	11
27240	23950	CDS
			locus_tag	Terrestrivirus_1_25
			product	hypothetical protein
			transl_table	11
27388	28188	CDS
			locus_tag	Terrestrivirus_1_26
			product	hypothetical protein
			transl_table	11
28265	28888	CDS
			locus_tag	Terrestrivirus_1_27
			product	FkbM family methyltransferase
			transl_table	11
28957	29988	CDS
			locus_tag	Terrestrivirus_1_28
			product	predicted protein
			transl_table	11
30090	31001	CDS
			locus_tag	Terrestrivirus_1_29
			product	hypothetical protein
			transl_table	11
31684	31019	CDS
			locus_tag	Terrestrivirus_1_30
			product	DNA/RNA endonuclease
			transl_table	11
32472	31798	CDS
			locus_tag	Terrestrivirus_1_31
			product	GTP-binding protein
			transl_table	11
32882	32514	CDS
			locus_tag	Terrestrivirus_1_32
			product	hypothetical protein
			transl_table	11
33092	33688	CDS
			locus_tag	Terrestrivirus_1_33
			product	putative methyltransferase
			transl_table	11
33770	34789	CDS
			locus_tag	Terrestrivirus_1_34
			product	hypothetical protein DDB_G0288313
			transl_table	11
36757	34841	CDS
			locus_tag	Terrestrivirus_1_35
			product	glycosyltransferase
			transl_table	11
38745	36841	CDS
			locus_tag	Terrestrivirus_1_36
			product	glycosyltransferase
			transl_table	11
38886	40388	CDS
			locus_tag	Terrestrivirus_1_37
			product	hypothetical protein BCR33DRAFT_763042
			transl_table	11
40508	43195	CDS
			locus_tag	Terrestrivirus_1_38
			product	vWA-like protein
			transl_table	11
43753	43217	CDS
			locus_tag	Terrestrivirus_1_39
			product	hypothetical protein
			transl_table	11
43924	44586	CDS
			locus_tag	Terrestrivirus_1_40
			product	hypoxanthine-guanine phosphoribosyltransferase
			transl_table	11
45681	44623	CDS
			locus_tag	Terrestrivirus_1_41
			product	hypothetical protein
			transl_table	11
46180	47394	CDS
			locus_tag	Terrestrivirus_1_42
			product	hypothetical protein
			transl_table	11
47492	47764	CDS
			locus_tag	Terrestrivirus_1_43
			product	hypothetical protein
			transl_table	11
49592	47787	CDS
			locus_tag	Terrestrivirus_1_44
			product	hypothetical protein C0625_13085
			transl_table	11
50736	49639	CDS
			locus_tag	Terrestrivirus_1_45
			product	hypothetical protein
			transl_table	11
51050	50802	CDS
			locus_tag	Terrestrivirus_1_46
			product	hypothetical protein
			transl_table	11
51763	51107	CDS
			locus_tag	Terrestrivirus_1_47
			product	hypothetical protein Klosneuvirus_1_117
			transl_table	11
51939	52268	CDS
			locus_tag	Terrestrivirus_1_48
			product	hypothetical protein
			transl_table	11
52364	52972	CDS
			locus_tag	Terrestrivirus_1_49
			product	hypothetical protein
			transl_table	11
53742	52945	CDS
			locus_tag	Terrestrivirus_1_50
			product	metallophosphatase
			transl_table	11
55277	53799	CDS
			locus_tag	Terrestrivirus_1_51
			product	hypothetical protein mv_R738
			transl_table	11
55414	56112	CDS
			locus_tag	Terrestrivirus_1_52
			product	hypothetical protein AY601_0876
			transl_table	11
57031	56117	CDS
			locus_tag	Terrestrivirus_1_53
			product	hypothetical protein
			transl_table	11
57113	57280	CDS
			locus_tag	Terrestrivirus_1_54
			product	hypothetical protein
			transl_table	11
58416	57802	CDS
			locus_tag	Terrestrivirus_1_55
			product	dual specificity protein phosphatase 19-like
			transl_table	11
59198	58695	CDS
			locus_tag	Terrestrivirus_1_56
			product	hypothetical protein Klosneuvirus_1_117
			transl_table	11
59356	59859	CDS
			locus_tag	Terrestrivirus_1_57
			product	hypothetical protein Catovirus_1_984
			transl_table	11
59908	60255	CDS
			locus_tag	Terrestrivirus_1_58
			product	hypothetical protein Catovirus_1_983
			transl_table	11
60481	60972	CDS
			locus_tag	Terrestrivirus_1_59
			product	hypothetical protein
			transl_table	11
61421	60996	CDS
			locus_tag	Terrestrivirus_1_60
			product	hypothetical protein UU67_C0063G0005
			transl_table	11
61601	62602	CDS
			locus_tag	Terrestrivirus_1_61
			product	acid ceramidase-like protein
			transl_table	11
62673	63095	CDS
			locus_tag	Terrestrivirus_1_62
			product	hypothetical protein
			transl_table	11
63260	64480	CDS
			locus_tag	Terrestrivirus_1_63
			product	unknown
			transl_table	11
64576	65847	CDS
			locus_tag	Terrestrivirus_1_64
			product	Protein kinase-like domain
			transl_table	11
65914	66507	CDS
			locus_tag	Terrestrivirus_1_65
			product	uncharacterized protein ZYRO0F03872g
			transl_table	11
66551	67165	CDS
			locus_tag	Terrestrivirus_1_66
			product	hypothetical protein
			transl_table	11
67214	67831	CDS
			locus_tag	Terrestrivirus_1_67
			product	hypothetical protein
			transl_table	11
67904	68272	CDS
			locus_tag	Terrestrivirus_1_68
			product	hypothetical protein
			transl_table	11
68556	68299	CDS
			locus_tag	Terrestrivirus_1_69
			product	PREDICTED: cytochrome b5
			transl_table	11
69712	68627	CDS
			locus_tag	Terrestrivirus_1_70
			product	hypothetical protein
			transl_table	11
69821	70015	CDS
			locus_tag	Terrestrivirus_1_71
			product	hypothetical protein
			transl_table	11
70932	70012	CDS
			locus_tag	Terrestrivirus_1_72
			product	hypothetical protein
			transl_table	11
71063	71731	CDS
			locus_tag	Terrestrivirus_1_73
			product	hypothetical protein
			transl_table	11
72490	71714	CDS
			locus_tag	Terrestrivirus_1_74
			product	hypothetical protein
			transl_table	11
72612	73967	CDS
			locus_tag	Terrestrivirus_1_75
			product	hypothetical protein
			transl_table	11
74969	74328	CDS
			locus_tag	Terrestrivirus_1_76
			product	hypothetical protein
			transl_table	11
75883	75140	CDS
			locus_tag	Terrestrivirus_1_77
			product	hypothetical protein A9Q86_03325
			transl_table	11
75979	76596	CDS
			locus_tag	Terrestrivirus_1_78
			product	hypothetical protein
			transl_table	11
77200	76598	CDS
			locus_tag	Terrestrivirus_1_79
			product	hypothetical protein
			transl_table	11
78487	77915	CDS
			locus_tag	Terrestrivirus_1_80
			product	hypothetical protein
			transl_table	11
78609	79232	CDS
			locus_tag	Terrestrivirus_1_81
			product	HNH endonuclease
			transl_table	11
79560	79249	CDS
			locus_tag	Terrestrivirus_1_82
			product	hypothetical protein
			transl_table	11
80923	79625	CDS
			locus_tag	Terrestrivirus_1_83
			product	Protoporphyrinogen oxidase
			transl_table	11
81283	80963	CDS
			locus_tag	Terrestrivirus_1_84
			product	cysteine methyltransferase
			transl_table	11
81979	81410	CDS
			locus_tag	Terrestrivirus_1_85
			product	hypothetical protein
			transl_table	11
82719	82084	CDS
			locus_tag	Terrestrivirus_1_86
			product	hypothetical protein
			transl_table	11
83887	82772	CDS
			locus_tag	Terrestrivirus_1_87
			product	alpha/beta hydrolase
			transl_table	11
85252	83987	CDS
			locus_tag	Terrestrivirus_1_88
			product	hypothetical protein
			transl_table	11
85443	85904	CDS
			locus_tag	Terrestrivirus_1_89
			product	hypothetical protein O9G_002264
			transl_table	11
86005	86664	CDS
			locus_tag	Terrestrivirus_1_90
			product	hypothetical protein
			transl_table	11
86766	87965	CDS
			locus_tag	Terrestrivirus_1_91
			product	hypothetical protein
			transl_table	11
88068	88733	CDS
			locus_tag	Terrestrivirus_1_92
			product	putative orfan
			transl_table	11
89897	88782	CDS
			locus_tag	Terrestrivirus_1_93
			product	hypothetical protein
			transl_table	11
90061	90417	CDS
			locus_tag	Terrestrivirus_1_94
			product	hypothetical protein
			transl_table	11
90498	91019	CDS
			locus_tag	Terrestrivirus_1_95
			product	hypothetical protein
			transl_table	11
91098	92399	CDS
			locus_tag	Terrestrivirus_1_96
			product	alpha-amylase
			transl_table	11
92480	93268	CDS
			locus_tag	Terrestrivirus_1_97
			product	serine/threonine-protein kinase
			transl_table	11
93333	93935	CDS
			locus_tag	Terrestrivirus_1_98
			product	hypothetical protein
			transl_table	11
95430	93964	CDS
			locus_tag	Terrestrivirus_1_99
			product	Uncharacterized protein XD72_2258
			transl_table	11
95521	96327	CDS
			locus_tag	Terrestrivirus_1_100
			product	hypothetical protein TSOC_014669
			transl_table	11
96903	96334	CDS
			locus_tag	Terrestrivirus_1_101
			product	hypothetical protein
			transl_table	11
97078	97992	CDS
			locus_tag	Terrestrivirus_1_102
			product	E3 ubiquitin-protein ligase RNF181-like
			transl_table	11
98059	98811	CDS
			locus_tag	Terrestrivirus_1_103
			product	Protein CLEC16A
			transl_table	11
98881	99252	CDS
			locus_tag	Terrestrivirus_1_104
			product	predicted protein
			transl_table	11
99368	99904	CDS
			locus_tag	Terrestrivirus_1_105
			product	hypothetical protein
			transl_table	11
100031	100396	CDS
			locus_tag	Terrestrivirus_1_106
			product	hypothetical protein
			transl_table	11
100490	102124	CDS
			locus_tag	Terrestrivirus_1_107
			product	hypothetical protein B5M56_03265
			transl_table	11
102226	103095	CDS
			locus_tag	Terrestrivirus_1_108
			product	Hypothetical protein ORPV_315
			transl_table	11
103203	103880	CDS
			locus_tag	Terrestrivirus_1_109
			product	hypothetical protein
			transl_table	11
105538	103901	CDS
			locus_tag	Terrestrivirus_1_110
			product	hypothetical protein CBD55_02575
			transl_table	11
105764	106195	CDS
			locus_tag	Terrestrivirus_1_111
			product	hypothetical protein
			transl_table	11
106801	106220	CDS
			locus_tag	Terrestrivirus_1_112
			product	Alkylated (methylated) DNA repair protein
			transl_table	11
106848	106964	CDS
			locus_tag	Terrestrivirus_1_113
			product	hypothetical protein
			transl_table	11
107006	107284	CDS
			locus_tag	Terrestrivirus_1_114
			product	hypothetical protein
			transl_table	11
107281	107973	CDS
			locus_tag	Terrestrivirus_1_115
			product	hypothetical protein
			transl_table	11
108485	107970	CDS
			locus_tag	Terrestrivirus_1_116
			product	hypothetical protein
			transl_table	11
108630	109724	CDS
			locus_tag	Terrestrivirus_1_117
			product	protein kinase domain protein
			transl_table	11
109764	110180	CDS
			locus_tag	Terrestrivirus_1_118
			product	hypothetical protein
			transl_table	11
111763	110219	CDS
			locus_tag	Terrestrivirus_1_119
			product	hypothetical protein
			transl_table	11
111951	112151	CDS
			locus_tag	Terrestrivirus_1_120
			product	hypothetical protein
			transl_table	11
112699	112163	CDS
			locus_tag	Terrestrivirus_1_121
			product	hypothetical protein
			transl_table	11
112854	113033	CDS
			locus_tag	Terrestrivirus_1_122
			product	hypothetical protein
			transl_table	11
113100	113939	CDS
			locus_tag	Terrestrivirus_1_123
			product	hypothetical protein BNJ_00291
			transl_table	11
114004	114189	CDS
			locus_tag	Terrestrivirus_1_124
			product	hypothetical protein BMW23_0336
			transl_table	11
114392	114871	CDS
			locus_tag	Terrestrivirus_1_125
			product	hypothetical protein
			transl_table	11
115285	114893	CDS
			locus_tag	Terrestrivirus_1_126
			product	hypothetical protein
			transl_table	11
115435	116220	CDS
			locus_tag	Terrestrivirus_1_127
			product	hypothetical protein
			transl_table	11
116319	116831	CDS
			locus_tag	Terrestrivirus_1_128
			product	predicted protein
			transl_table	11
116951	117421	CDS
			locus_tag	Terrestrivirus_1_129
			product	hypothetical protein
			transl_table	11
118215	119372	CDS
			locus_tag	Terrestrivirus_1_130
			product	hypothetical protein SAMN02787076_04850
			transl_table	11
119500	120345	CDS
			locus_tag	Terrestrivirus_1_131
			product	hypothetical protein
			transl_table	11
121083	120382	CDS
			locus_tag	Terrestrivirus_1_132
			product	swarming motility protein YbiA
			transl_table	11
121309	122337	CDS
			locus_tag	Terrestrivirus_1_133
			product	5'-3' exonuclease, putative
			transl_table	11
122834	123445	CDS
			locus_tag	Terrestrivirus_1_134
			product	hypothetical protein
			transl_table	11
123512	124561	CDS
			locus_tag	Terrestrivirus_1_135
			product	putative ORFan
			transl_table	11
125702	124650	CDS
			locus_tag	Terrestrivirus_1_136
			product	hypothetical protein
			transl_table	11
125871	127604	CDS
			locus_tag	Terrestrivirus_1_137
			product	hypothetical protein A2513_08830
			transl_table	11
128381	127647	CDS
			locus_tag	Terrestrivirus_1_138
			product	predicted protein
			transl_table	11
128631	129188	CDS
			locus_tag	Terrestrivirus_1_139
			product	Protein tyrosine phosphatase type IVA protein 1
			transl_table	11
129927	129250	CDS
			locus_tag	Terrestrivirus_1_140
			product	PREDICTED: elongation factor 1-gamma-A-like
			transl_table	11
130077	130316	CDS
			locus_tag	Terrestrivirus_1_141
			product	hypothetical protein
			transl_table	11
130450	131619	CDS
			locus_tag	Terrestrivirus_1_142
			product	hypothetical protein
			transl_table	11
133327	131717	CDS
			locus_tag	Terrestrivirus_1_143
			product	AAA family ATPase
			transl_table	11
133552	134286	CDS
			locus_tag	Terrestrivirus_1_144
			product	hypothetical protein
			transl_table	11
134384	135115	CDS
			locus_tag	Terrestrivirus_1_145
			product	hypothetical protein
			transl_table	11
135914	135123	CDS
			locus_tag	Terrestrivirus_1_146
			product	PREDICTED: protein-tyrosine kinase 6-like
			transl_table	11
136089	136901	CDS
			locus_tag	Terrestrivirus_1_147
			product	hypothetical protein
			transl_table	11
137591	136887	CDS
			locus_tag	Terrestrivirus_1_148
			product	hypothetical protein
			transl_table	11
137753	138172	CDS
			locus_tag	Terrestrivirus_1_149
			product	hypothetical protein
			transl_table	11
139716	138193	CDS
			locus_tag	Terrestrivirus_1_150
			product	hypothetical protein
			transl_table	11
140443	139808	CDS
			locus_tag	Terrestrivirus_1_151
			product	hypothetical protein
			transl_table	11
140850	140608	CDS
			locus_tag	Terrestrivirus_1_152
			product	hypothetical protein
			transl_table	11
141018	141161	CDS
			locus_tag	Terrestrivirus_1_153
			product	hypothetical protein
			transl_table	11
141619	141173	CDS
			locus_tag	Terrestrivirus_1_154
			product	YbhB/YbcL family Raf kinase inhibitor-like protein
			transl_table	11
141767	144583	CDS
			locus_tag	Terrestrivirus_1_155
			product	Ankyrin
			transl_table	11
144684	145181	CDS
			locus_tag	Terrestrivirus_1_156
			product	hypothetical protein
			transl_table	11
145613	145314	CDS
			locus_tag	Terrestrivirus_1_157
			product	hypothetical protein
			transl_table	11
145859	146866	CDS
			locus_tag	Terrestrivirus_1_158
			product	class I SAM-dependent methyltransferase
			transl_table	11
146921	148210	CDS
			locus_tag	Terrestrivirus_1_159
			product	putative serine/threonine-protein kinase
			transl_table	11
148332	149216	CDS
			locus_tag	Terrestrivirus_1_160
			product	hypothetical protein COW78_12250
			transl_table	11
150010	149225	CDS
			locus_tag	Terrestrivirus_1_161
			product	hypothetical protein BWK79_15395
			transl_table	11
150215	150688	CDS
			locus_tag	Terrestrivirus_1_162
			product	hypothetical protein
			transl_table	11
151586	150717	CDS
			locus_tag	Terrestrivirus_1_163
			product	hypothetical protein
			transl_table	11
152080	151631	CDS
			locus_tag	Terrestrivirus_1_164
			product	hypothetical protein
			transl_table	11
152218	152853	CDS
			locus_tag	Terrestrivirus_1_165
			product	DUF296 domain-containing protein
			transl_table	11
153631	152885	CDS
			locus_tag	Terrestrivirus_1_166
			product	hypothetical protein
			transl_table	11
154010	153840	CDS
			locus_tag	Terrestrivirus_1_167
			product	hypothetical protein
			transl_table	11
156646	154001	CDS
			locus_tag	Terrestrivirus_1_168
			product	hypothetical protein BKI52_37700
			transl_table	11
156679	157455	CDS
			locus_tag	Terrestrivirus_1_169
			product	hypothetical protein
			transl_table	11
158576	157497	CDS
			locus_tag	Terrestrivirus_1_170
			product	hypothetical protein
			transl_table	11
158745	161282	CDS
			locus_tag	Terrestrivirus_1_171
			product	hypothetical protein BMW23_1092
			transl_table	11
161415	162263	CDS
			locus_tag	Terrestrivirus_1_172
			product	hypothetical protein
			transl_table	11
162347	163030	CDS
			locus_tag	Terrestrivirus_1_173
			product	hypothetical protein
			transl_table	11
163115	163474	CDS
			locus_tag	Terrestrivirus_1_174
			product	hypothetical protein
			transl_table	11
163578	164054	CDS
			locus_tag	Terrestrivirus_1_175
			product	hypothetical protein
			transl_table	11
164157	164861	CDS
			locus_tag	Terrestrivirus_1_176
			product	hypothetical protein
			transl_table	11
165522	164878	CDS
			locus_tag	Terrestrivirus_1_177
			product	hypothetical protein
			transl_table	11
168587	165681	CDS
			locus_tag	Terrestrivirus_1_178
			product	hypothetical protein
			transl_table	11
168769	169218	CDS
			locus_tag	Terrestrivirus_1_179
			product	hypothetical protein
			transl_table	11
171138	169324	CDS
			locus_tag	Terrestrivirus_1_180
			product	cell surface protein
			transl_table	11
171218	172195	CDS
			locus_tag	Terrestrivirus_1_181
			product	hypothetical protein A2Z85_04100
			transl_table	11
172345	172199	CDS
			locus_tag	Terrestrivirus_1_182
			product	hypothetical protein
			transl_table	11
172396	174687	CDS
			locus_tag	Terrestrivirus_1_183
			product	hypothetical protein CAOG_06260
			transl_table	11
174801	175799	CDS
			locus_tag	Terrestrivirus_1_184
			product	hypothetical protein
			transl_table	11
175968	177782	CDS
			locus_tag	Terrestrivirus_1_185
			product	putative glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
177891	178808	CDS
			locus_tag	Terrestrivirus_1_186
			product	hypothetical protein
			transl_table	11
178877	179128	CDS
			locus_tag	Terrestrivirus_1_187
			product	hypothetical protein mvi_27
			transl_table	11
179724	179161	CDS
			locus_tag	Terrestrivirus_1_188
			product	hypothetical protein
			transl_table	11
179954	181549	CDS
			locus_tag	Terrestrivirus_1_189
			product	Tetratricopeptide repeat protein
			transl_table	11
181613	182146	CDS
			locus_tag	Terrestrivirus_1_190
			product	hypothetical protein
			transl_table	11
182799	182143	CDS
			locus_tag	Terrestrivirus_1_191
			product	hypothetical protein
			transl_table	11
183340	182867	CDS
			locus_tag	Terrestrivirus_1_192
			product	hypothetical protein
			transl_table	11
183428	184213	CDS
			locus_tag	Terrestrivirus_1_193
			product	hypothetical protein
			transl_table	11
184290	184823	CDS
			locus_tag	Terrestrivirus_1_194
			product	hypothetical protein
			transl_table	11
184948	185919	CDS
			locus_tag	Terrestrivirus_1_195
			product	hypothetical protein PPERSA_09713
			transl_table	11
187187	186009	CDS
			locus_tag	Terrestrivirus_1_196
			product	hypothetical protein
			transl_table	11
187242	187376	CDS
			locus_tag	Terrestrivirus_1_197
			product	hypothetical protein
			transl_table	11
187380	188444	CDS
			locus_tag	Terrestrivirus_1_198
			product	PREDICTED: UPF0415 protein C7orf25 homolog
			transl_table	11
188538	190259	CDS
			locus_tag	Terrestrivirus_1_199
			product	hypothetical protein
			transl_table	11
191268	190321	CDS
			locus_tag	Terrestrivirus_1_200
			product	hypothetical protein
			transl_table	11
191594	191920	CDS
			locus_tag	Terrestrivirus_1_201
			product	hypothetical protein
			transl_table	11
192745	191975	CDS
			locus_tag	Terrestrivirus_1_202
			product	hypothetical protein
			transl_table	11
192913	196926	CDS
			locus_tag	Terrestrivirus_1_203
			product	putative bifunctional E2/E3 enzyme
			transl_table	11
197037	197630	CDS
			locus_tag	Terrestrivirus_1_204
			product	DNA primase
			transl_table	11
197729	197905	CDS
			locus_tag	Terrestrivirus_1_205
			product	hypothetical protein
			transl_table	11
197996	198490	CDS
			locus_tag	Terrestrivirus_1_206
			product	hypothetical protein
			transl_table	11
199087	198545	CDS
			locus_tag	Terrestrivirus_1_207
			product	hypothetical protein
			transl_table	11
199412	199621	CDS
			locus_tag	Terrestrivirus_1_208
			product	hypothetical protein
			transl_table	11
201453	199648	CDS
			locus_tag	Terrestrivirus_1_209
			product	hypothetical protein A2513_08830
			transl_table	11
201983	202465	CDS
			locus_tag	Terrestrivirus_1_210
			product	hypothetical protein
			transl_table	11
202941	202462	CDS
			locus_tag	Terrestrivirus_1_211
			product	hypothetical protein
			transl_table	11
204162	203014	CDS
			locus_tag	Terrestrivirus_1_212
			product	hypothetical protein
			transl_table	11
204417	205322	CDS
			locus_tag	Terrestrivirus_1_213
			product	hypothetical protein
			transl_table	11
205712	206656	CDS
			locus_tag	Terrestrivirus_1_214
			product	hypothetical protein 162300064
			transl_table	11
207757	206684	CDS
			locus_tag	Terrestrivirus_1_215
			product	hypothetical protein
			transl_table	11
207901	208896	CDS
			locus_tag	Terrestrivirus_1_216
			product	hypothetical protein Klosneuvirus_3_10
			transl_table	11
209399	208989	CDS
			locus_tag	Terrestrivirus_1_217
			product	hypothetical protein
			transl_table	11
209549	211396	CDS
			locus_tag	Terrestrivirus_1_218
			product	hypothetical protein
			transl_table	11
211377	211478	CDS
			locus_tag	Terrestrivirus_1_219
			product	hypothetical protein
			transl_table	11
211500	212531	CDS
			locus_tag	Terrestrivirus_1_220
			product	hypothetical protein
			transl_table	11
212716	213423	CDS
			locus_tag	Terrestrivirus_1_221
			product	hypothetical protein
			transl_table	11
213495	214037	CDS
			locus_tag	Terrestrivirus_1_222
			product	hypothetical protein
			transl_table	11
214433	214038	CDS
			locus_tag	Terrestrivirus_1_223
			product	hypothetical protein
			transl_table	11
215770	214553	CDS
			locus_tag	Terrestrivirus_1_224
			product	uncharacterized serpin-like protein
			transl_table	11
215955	216365	CDS
			locus_tag	Terrestrivirus_1_225
			product	hypothetical protein
			transl_table	11
216880	216401	CDS
			locus_tag	Terrestrivirus_1_226
			product	hypothetical protein
			transl_table	11
217066	218121	CDS
			locus_tag	Terrestrivirus_1_227
			product	hypothetical protein
			transl_table	11
218802	218158	CDS
			locus_tag	Terrestrivirus_1_228
			product	hypothetical protein
			transl_table	11
220224	218893	CDS
			locus_tag	Terrestrivirus_1_229
			product	hypothetical protein
			transl_table	11
221590	220313	CDS
			locus_tag	Terrestrivirus_1_230
			product	hypothetical protein
			transl_table	11
221734	223095	CDS
			locus_tag	Terrestrivirus_1_231
			product	hypothetical protein
			transl_table	11
223170	224003	CDS
			locus_tag	Terrestrivirus_1_232
			product	hypothetical protein
			transl_table	11
224119	224700	CDS
			locus_tag	Terrestrivirus_1_233
			product	hypothetical protein
			transl_table	11
225285	224707	CDS
			locus_tag	Terrestrivirus_1_234
			product	hypothetical protein
			transl_table	11
225686	225333	CDS
			locus_tag	Terrestrivirus_1_235
			product	hypothetical protein
			transl_table	11
226923	225937	CDS
			locus_tag	Terrestrivirus_1_236
			product	hypothetical protein
			transl_table	11
227982	227293	CDS
			locus_tag	Terrestrivirus_1_237
			product	hypothetical protein A2X00_04170, partial
			transl_table	11
228140	228700	CDS
			locus_tag	Terrestrivirus_1_238
			product	hypothetical protein
			transl_table	11
228775	229521	CDS
			locus_tag	Terrestrivirus_1_239
			product	hypothetical protein Klosneuvirus_3_121
			transl_table	11
230795	229548	CDS
			locus_tag	Terrestrivirus_1_240
			product	hypothetical protein
			transl_table	11
230915	231712	CDS
			locus_tag	Terrestrivirus_1_241
			product	proliferating cell nuclear antigen
			transl_table	11
231791	233353	CDS
			locus_tag	Terrestrivirus_1_242
			product	serine peptidase
			transl_table	11
233428	234246	CDS
			locus_tag	Terrestrivirus_1_243
			product	hypothetical protein A2513_08830
			transl_table	11
234392	234204	CDS
			locus_tag	Terrestrivirus_1_244
			product	hypothetical protein
			transl_table	11
234416	235285	CDS
			locus_tag	Terrestrivirus_1_245
			product	hypothetical protein
			transl_table	11
236259	235333	CDS
			locus_tag	Terrestrivirus_1_246
			product	formamidopyrimidine-DNA glycosylase
			transl_table	11
236383	236799	CDS
			locus_tag	Terrestrivirus_1_247
			product	hypothetical protein
			transl_table	11
236955	237398	CDS
			locus_tag	Terrestrivirus_1_248
			product	hypothetical protein
			transl_table	11
238027	237428	CDS
			locus_tag	Terrestrivirus_1_249
			product	hypothetical protein
			transl_table	11
238150	238755	CDS
			locus_tag	Terrestrivirus_1_250
			product	hypothetical protein
			transl_table	11
239084	240031	CDS
			locus_tag	Terrestrivirus_1_251
			product	hypothetical protein 162300064
			transl_table	11
240245	240823	CDS
			locus_tag	Terrestrivirus_1_252
			product	hypothetical protein
			transl_table	11
240925	241842	CDS
			locus_tag	Terrestrivirus_1_253
			product	hypothetical protein
			transl_table	11
241905	242303	CDS
			locus_tag	Terrestrivirus_1_254
			product	hypothetical protein
			transl_table	11
242422	243459	CDS
			locus_tag	Terrestrivirus_1_255
			product	hypothetical protein CVV64_10245
			transl_table	11
243924	243415	CDS
			locus_tag	Terrestrivirus_1_256
			product	hypothetical protein
			transl_table	11
244022	244333	CDS
			locus_tag	Terrestrivirus_1_257
			product	hypothetical protein
			transl_table	11
244443	244664	CDS
			locus_tag	Terrestrivirus_1_258
			product	hypothetical protein
			transl_table	11
245336	244665	CDS
			locus_tag	Terrestrivirus_1_259
			product	hypothetical protein
			transl_table	11
246335	245457	CDS
			locus_tag	Terrestrivirus_1_260
			product	hypothetical protein
			transl_table	11
246502	247695	CDS
			locus_tag	Terrestrivirus_1_261
			product	hypothetical protein
			transl_table	11
247915	247709	CDS
			locus_tag	Terrestrivirus_1_262
			product	hypothetical protein mv_R1046
			transl_table	11
248248	248030	CDS
			locus_tag	Terrestrivirus_1_263
			product	hypothetical protein
			transl_table	11
248471	248914	CDS
			locus_tag	Terrestrivirus_1_264
			product	hypothetical protein
			transl_table	11
249235	248945	CDS
			locus_tag	Terrestrivirus_1_265
			product	hypothetical protein
			transl_table	11
249407	249676	CDS
			locus_tag	Terrestrivirus_1_266
			product	hypothetical protein
			transl_table	11
249854	250174	CDS
			locus_tag	Terrestrivirus_1_267
			product	hypothetical protein
			transl_table	11
250283	252109	CDS
			locus_tag	Terrestrivirus_1_268
			product	hypothetical protein
			transl_table	11
252202	252561	CDS
			locus_tag	Terrestrivirus_1_269
			product	hypothetical protein
			transl_table	11
253070	252606	CDS
			locus_tag	Terrestrivirus_1_270
			product	hypothetical protein
			transl_table	11
253716	254504	CDS
			locus_tag	Terrestrivirus_1_271
			product	hypothetical protein
			transl_table	11
254638	255450	CDS
			locus_tag	Terrestrivirus_1_272
			product	hypothetical protein
			transl_table	11
255507	256037	CDS
			locus_tag	Terrestrivirus_1_273
			product	hypothetical protein
			transl_table	11
256207	256764	CDS
			locus_tag	Terrestrivirus_1_274
			product	hypothetical protein
			transl_table	11
257372	256863	CDS
			locus_tag	Terrestrivirus_1_275
			product	hypothetical protein
			transl_table	11
257949	257497	CDS
			locus_tag	Terrestrivirus_1_276
			product	DEXDc helicase
			transl_table	11
258219	258025	CDS
			locus_tag	Terrestrivirus_1_277
			product	hypothetical protein
			transl_table	11
258898	258317	CDS
			locus_tag	Terrestrivirus_1_278
			product	hypothetical protein
			transl_table	11
259631	258960	CDS
			locus_tag	Terrestrivirus_1_279
			product	hypothetical protein
			transl_table	11
259809	260102	CDS
			locus_tag	Terrestrivirus_1_280
			product	hypothetical protein
			transl_table	11
260190	260654	CDS
			locus_tag	Terrestrivirus_1_281
			product	peptide-methionine (R)-S-oxide reductase
			transl_table	11
261334	260672	CDS
			locus_tag	Terrestrivirus_1_282
			product	2OG-FeII oxygenase superfamily protein
			transl_table	11
261505	262173	CDS
			locus_tag	Terrestrivirus_1_283
			product	hypothetical protein
			transl_table	11
262569	262222	CDS
			locus_tag	Terrestrivirus_1_284
			product	hypothetical protein
			transl_table	11
262700	262566	CDS
			locus_tag	Terrestrivirus_1_285
			product	hypothetical protein
			transl_table	11
262789	262700	CDS
			locus_tag	Terrestrivirus_1_286
			product	hypothetical protein
			transl_table	11
262857	263282	CDS
			locus_tag	Terrestrivirus_1_287
			product	hypothetical protein
			transl_table	11
263367	263957	CDS
			locus_tag	Terrestrivirus_1_288
			product	hypothetical protein
			transl_table	11
264234	263959	CDS
			locus_tag	Terrestrivirus_1_289
			product	hypothetical protein
			transl_table	11
264694	264302	CDS
			locus_tag	Terrestrivirus_1_290
			product	hypothetical protein
			transl_table	11
265057	264758	CDS
			locus_tag	Terrestrivirus_1_291
			product	hypothetical protein
			transl_table	11
265195	265719	CDS
			locus_tag	Terrestrivirus_1_292
			product	hypothetical protein
			transl_table	11
265784	266776	CDS
			locus_tag	Terrestrivirus_1_293
			product	SNF1-related kinase regulatory subunit beta-2
			transl_table	11
266839	267567	CDS
			locus_tag	Terrestrivirus_1_294
			product	hypothetical protein
			transl_table	11
269776	267725	CDS
			locus_tag	Terrestrivirus_1_295
			product	hypothetical protein
			transl_table	11
270156	270968	CDS
			locus_tag	Terrestrivirus_1_296
			product	hypothetical protein
			transl_table	11
272049	270952	CDS
			locus_tag	Terrestrivirus_1_297
			product	hypothetical protein
			transl_table	11
272207	272890	CDS
			locus_tag	Terrestrivirus_1_298
			product	hypothetical protein
			transl_table	11
273246	274190	CDS
			locus_tag	Terrestrivirus_1_299
			product	hypothetical protein 162275868
			transl_table	11
274644	274192	CDS
			locus_tag	Terrestrivirus_1_300
			product	hypothetical protein
			transl_table	11
274717	275604	CDS
			locus_tag	Terrestrivirus_1_301
			product	hypothetical protein Catovirus_1_66
			transl_table	11
275704	275934	CDS
			locus_tag	Terrestrivirus_1_302
			product	hypothetical protein
			transl_table	11
276690	275953	CDS
			locus_tag	Terrestrivirus_1_303
			product	hypothetical protein
			transl_table	11
278795	277011	CDS
			locus_tag	Terrestrivirus_1_304
			product	Carboxylesterase NlhH
			transl_table	11
278970	279539	CDS
			locus_tag	Terrestrivirus_1_305
			product	hypothetical protein
			transl_table	11
280042	279557	CDS
			locus_tag	Terrestrivirus_1_306
			product	hypothetical protein
			transl_table	11
280187	280852	CDS
			locus_tag	Terrestrivirus_1_307
			product	hypothetical protein
			transl_table	11
281065	280859	CDS
			locus_tag	Terrestrivirus_1_308
			product	hypothetical protein
			transl_table	11
281398	281120	CDS
			locus_tag	Terrestrivirus_1_309
			product	hypothetical protein
			transl_table	11
281571	281978	CDS
			locus_tag	Terrestrivirus_1_310
			product	hypothetical protein
			transl_table	11
282350	282766	CDS
			locus_tag	Terrestrivirus_1_311
			product	hypothetical protein
			transl_table	11
283092	282811	CDS
			locus_tag	Terrestrivirus_1_312
			product	hypothetical protein
			transl_table	11
283323	284000	CDS
			locus_tag	Terrestrivirus_1_313
			product	hypothetical protein
			transl_table	11
284224	284721	CDS
			locus_tag	Terrestrivirus_1_314
			product	hypothetical protein
			transl_table	11
284871	285257	CDS
			locus_tag	Terrestrivirus_1_315
			product	hypothetical protein
			transl_table	11
285620	285291	CDS
			locus_tag	Terrestrivirus_1_316
			product	hypothetical protein
			transl_table	11
287307	285700	CDS
			locus_tag	Terrestrivirus_1_317
			product	hypothetical protein Q606_CBAC00305G0008
			transl_table	11
287445	288566	CDS
			locus_tag	Terrestrivirus_1_318
			product	cystathionine beta-lyase/cystathionine gamma-synthase
			transl_table	11
288756	289874	CDS
			locus_tag	Terrestrivirus_1_319
			product	hypothetical protein
			transl_table	11
290310	290789	CDS
			locus_tag	Terrestrivirus_1_320
			product	hypothetical protein
			transl_table	11
290944	291276	CDS
			locus_tag	Terrestrivirus_1_321
			product	hypothetical protein
			transl_table	11
291897	291295	CDS
			locus_tag	Terrestrivirus_1_322
			product	hypothetical protein
			transl_table	11
292400	293563	CDS
			locus_tag	Terrestrivirus_1_323
			product	cystathionine gamma-synthase
			transl_table	11
293819	296755	CDS
			locus_tag	Terrestrivirus_1_324
			product	multicopper oxidase
			transl_table	11
296849	297193	CDS
			locus_tag	Terrestrivirus_1_325
			product	hypothetical protein
			transl_table	11
297523	297233	CDS
			locus_tag	Terrestrivirus_1_326
			product	hypothetical protein
			transl_table	11
297684	298739	CDS
			locus_tag	Terrestrivirus_1_327
			product	hypothetical protein
			transl_table	11
299020	299682	CDS
			locus_tag	Terrestrivirus_1_328
			product	hypothetical protein
			transl_table	11
299811	300068	CDS
			locus_tag	Terrestrivirus_1_329
			product	hypothetical protein
			transl_table	11
300107	300697	CDS
			locus_tag	Terrestrivirus_1_330
			product	hypothetical protein
			transl_table	11
301101	301769	CDS
			locus_tag	Terrestrivirus_1_331
			product	transmembrane protein
			transl_table	11
302869	301796	CDS
			locus_tag	Terrestrivirus_1_332
			product	hypothetical protein
			transl_table	11
302926	303090	CDS
			locus_tag	Terrestrivirus_1_333
			product	hypothetical protein
			transl_table	11
303884	303162	CDS
			locus_tag	Terrestrivirus_1_334
			product	hypothetical protein Klosneuvirus_4_137
			transl_table	11
304004	305014	CDS
			locus_tag	Terrestrivirus_1_335
			product	hypothetical protein Klosneuvirus_3_10
			transl_table	11
305100	305774	CDS
			locus_tag	Terrestrivirus_1_336
			product	hypothetical protein
			transl_table	11
305892	306518	CDS
			locus_tag	Terrestrivirus_1_337
			product	hypothetical protein
			transl_table	11
307406	306618	CDS
			locus_tag	Terrestrivirus_1_338
			product	hypothetical protein
			transl_table	11
307540	308286	CDS
			locus_tag	Terrestrivirus_1_339
			product	hypothetical protein
			transl_table	11
309563	308364	CDS
			locus_tag	Terrestrivirus_1_340
			product	serine proteinase inhibitor
			transl_table	11
309924	309724	CDS
			locus_tag	Terrestrivirus_1_341
			product	hypothetical protein
			transl_table	11
310031	310282	CDS
			locus_tag	Terrestrivirus_1_342
			product	hypothetical protein
			transl_table	11
311125	310289	CDS
			locus_tag	Terrestrivirus_1_343
			product	hypothetical protein
			transl_table	11
312520	311246	CDS
			locus_tag	Terrestrivirus_1_344
			product	PREDICTED: cytochrome P450 4C1-like isoform X2
			transl_table	11
313019	314347	CDS
			locus_tag	Terrestrivirus_1_345
			product	GIY-YIG nuclease
			transl_table	11
314753	315511	CDS
			locus_tag	Terrestrivirus_1_346
			product	hypothetical protein
			transl_table	11
316084	315680	CDS
			locus_tag	Terrestrivirus_1_347
			product	hypothetical protein
			transl_table	11
316410	316769	CDS
			locus_tag	Terrestrivirus_1_348
			product	hypothetical protein
			transl_table	11
317008	317325	CDS
			locus_tag	Terrestrivirus_1_349
			product	hypothetical protein
			transl_table	11
317995	317399	CDS
			locus_tag	Terrestrivirus_1_350
			product	hypothetical protein
			transl_table	11
319202	318156	CDS
			locus_tag	Terrestrivirus_1_351
			product	hypothetical protein
			transl_table	11
319443	319847	CDS
			locus_tag	Terrestrivirus_1_352
			product	hypothetical protein
			transl_table	11
320564	320022	CDS
			locus_tag	Terrestrivirus_1_353
			product	hypothetical protein
			transl_table	11
320578	320718	CDS
			locus_tag	Terrestrivirus_1_354
			product	hypothetical protein
			transl_table	11
321423	320689	CDS
			locus_tag	Terrestrivirus_1_355
			product	hypothetical protein
			transl_table	11
322590	321559	CDS
			locus_tag	Terrestrivirus_1_356
			product	hypothetical protein
			transl_table	11
323000	323539	CDS
			locus_tag	Terrestrivirus_1_357
			product	hypothetical protein
			transl_table	11
323618	324010	CDS
			locus_tag	Terrestrivirus_1_358
			product	hypothetical protein
			transl_table	11
324490	324365	CDS
			locus_tag	Terrestrivirus_1_359
			product	hypothetical protein
			transl_table	11
325647	327383	CDS
			locus_tag	Terrestrivirus_1_360
			product	hypothetical protein
			transl_table	11
328099	327434	CDS
			locus_tag	Terrestrivirus_1_361
			product	GTP-binding protein YPTC1
			transl_table	11
328669	328421	CDS
			locus_tag	Terrestrivirus_1_362
			product	hypothetical protein
			transl_table	11
328977	329216	CDS
			locus_tag	Terrestrivirus_1_363
			product	hypothetical protein
			transl_table	11
329757	329996	CDS
			locus_tag	Terrestrivirus_1_364
			product	hypothetical protein
			transl_table	11
331072	330272	CDS
			locus_tag	Terrestrivirus_1_365
			product	metallophosphoesterase
			transl_table	11
332092	332307	CDS
			locus_tag	Terrestrivirus_1_366
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_2
161420	161492	tRNA	Terrestrivirus_tRNA_1
			product	tRNA-Tyr(GTA)
161313	161383	tRNA	Terrestrivirus_tRNA_2
			product	tRNA-Thr(TGT)
161237	161308	tRNA	Terrestrivirus_tRNA_3
			product	tRNA-Glu(TTC)
161161	161231	tRNA	Terrestrivirus_tRNA_4
			product	tRNA-Gly(TCC)
161068	161139	tRNA	Terrestrivirus_tRNA_5
			product	tRNA-Glu(CTC)
160894	160964	tRNA	Terrestrivirus_tRNA_6
			product	tRNA-Thr(AGT)
160784	160857	tRNA	Terrestrivirus_tRNA_7
			product	tRNA-Asn(GTT)
159275	159347	tRNA	Terrestrivirus_tRNA_8
			product	tRNA-Gln(TTG)
159197	159269	tRNA	Terrestrivirus_tRNA_9
			product	tRNA-Gln(TTG)
159121	159193	tRNA	Terrestrivirus_tRNA_10
			product	tRNA-Phe(GAA)
159038	159110	tRNA	Terrestrivirus_tRNA_11
			product	tRNA-Asp(GTC)
158923	158995	tRNA	Terrestrivirus_tRNA_12
			product	tRNA-Ala(TGC)
158832	158903	tRNA	Terrestrivirus_tRNA_13
			product	tRNA-Leu(AAG)
158721	158792	tRNA	Terrestrivirus_tRNA_14
			product	tRNA-Leu(TAG)
155471	155542	tRNA	Terrestrivirus_tRNA_15
			product	tRNA-Arg(TCT)
155292	155363	tRNA	Terrestrivirus_tRNA_16
			product	tRNA-Ile(TAT)
153554	153625	tRNA	Terrestrivirus_tRNA_17
			product	tRNA-Ile(TAT)
153369	153449	tRNA	Terrestrivirus_tRNA_18
			product	tRNA-Ser(GCT)
153207	153278	tRNA	Terrestrivirus_tRNA_19
			product	tRNA-Ile(TAT)
152914	152986	tRNA	Terrestrivirus_tRNA_20
			product	tRNA-Phe(GAA)
150702	150773	tRNA	Terrestrivirus_tRNA_21
			product	tRNA-Thr(CGT)
150629	150701	tRNA	Terrestrivirus_tRNA_22
			product	tRNA-Asn(GTT)
150515	150589	tRNA	Terrestrivirus_tRNA_23
			product	tRNA-Tyr(GTA)
150407	150478	tRNA	Terrestrivirus_tRNA_24
			product	tRNA-Ile(TAT)
150247	150318	tRNA	Terrestrivirus_tRNA_25
			product	tRNA-Arg(TCT)
150071	150142	tRNA	Terrestrivirus_tRNA_26
			product	tRNA-Ile(TAT)
149818	149889	tRNA	Terrestrivirus_tRNA_27
			product	tRNA-Glu(TTC)
149670	149745	tRNA	Terrestrivirus_tRNA_28
			product	tRNA-Pseudo(GCG)
147178	147249	tRNA	Terrestrivirus_tRNA_29
			product	tRNA-Cys(GCA)
147091	147161	tRNA	Terrestrivirus_tRNA_30
			product	tRNA-Pro(TGG)
147011	147081	tRNA	Terrestrivirus_tRNA_31
			product	tRNA-Arg(ACG)
146919	146989	tRNA	Terrestrivirus_tRNA_32
			product	tRNA-Arg(TCG)
146839	146909	tRNA	Terrestrivirus_tRNA_33
			product	tRNA-Arg(CCT)
146748	146819	tRNA	Terrestrivirus_tRNA_34
			product	tRNA-Arg(TCT)
145019	145089	tRNA	Terrestrivirus_tRNA_35
			product	tRNA-Pro(AGG)
144903	144975	tRNA	Terrestrivirus_tRNA_36
			product	tRNA-Lys(TTT)
144734	144804	tRNA	Terrestrivirus_tRNA_37
			product	tRNA-Pseudo(GAC)
144646	144716	tRNA	Terrestrivirus_tRNA_38
			product	tRNA-Pro(CGG)
144452	144523	tRNA	Terrestrivirus_tRNA_39
			product	tRNA-Lys(CTT)
144287	144359	tRNA	Terrestrivirus_tRNA_40
			product	tRNA-Ile(GAT)
144109	144179	tRNA	Terrestrivirus_tRNA_41
			product	tRNA-Val(TAC)
144014	144084	tRNA	Terrestrivirus_tRNA_42
			product	tRNA-Trp(CCA)
143926	143997	tRNA	Terrestrivirus_tRNA_43
			product	tRNA-Gln(CTG)
136631	136702	tRNA	Terrestrivirus_tRNA_44
			product	tRNA-Gly(GCC)
136396	136476	tRNA	Terrestrivirus_tRNA_45
			product	tRNA-Ser(ACT)
136248	136318	tRNA	Terrestrivirus_tRNA_46
			product	tRNA-Met(CAT)
136167	136247	tRNA	Terrestrivirus_tRNA_47
			product	tRNA-Ser(GGA)
136036	136107	tRNA	Terrestrivirus_tRNA_48
			product	tRNA-Met(CAT)
135928	136007	tRNA	Terrestrivirus_tRNA_49
			product	tRNA-Ser(TGA)
135741	135813	tRNA	Terrestrivirus_tRNA_50
			product	tRNA-His(GTG)
1	90	CDS
			locus_tag	Terrestrivirus_2_1
			product	hypothetical protein
			transl_table	11
212	697	CDS
			locus_tag	Terrestrivirus_2_2
			product	hypothetical protein
			transl_table	11
924	718	CDS
			locus_tag	Terrestrivirus_2_3
			product	hypothetical protein
			transl_table	11
1153	1986	CDS
			locus_tag	Terrestrivirus_2_4
			product	hypothetical protein
			transl_table	11
2428	2033	CDS
			locus_tag	Terrestrivirus_2_5
			product	hypothetical protein
			transl_table	11
4284	2566	CDS
			locus_tag	Terrestrivirus_2_6
			product	hypothetical protein Klosneuvirus_1_181
			transl_table	11
4437	5675	CDS
			locus_tag	Terrestrivirus_2_7
			product	putative FAD-dependent dehydrogenase
			transl_table	11
6385	5690	CDS
			locus_tag	Terrestrivirus_2_8
			product	hypothetical protein
			transl_table	11
7342	6467	CDS
			locus_tag	Terrestrivirus_2_9
			product	hypothetical protein
			transl_table	11
8477	7437	CDS
			locus_tag	Terrestrivirus_2_10
			product	hypothetical protein
			transl_table	11
10216	8558	CDS
			locus_tag	Terrestrivirus_2_11
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
13410	10312	CDS
			locus_tag	Terrestrivirus_2_12
			product	glycosyltransferase
			transl_table	11
13569	15095	CDS
			locus_tag	Terrestrivirus_2_13
			product	lipopolysaccharide protein
			transl_table	11
15147	16019	CDS
			locus_tag	Terrestrivirus_2_14
			product	hypothetical protein CBD38_00845
			transl_table	11
16571	16029	CDS
			locus_tag	Terrestrivirus_2_15
			product	hypothetical protein
			transl_table	11
17036	16710	CDS
			locus_tag	Terrestrivirus_2_16
			product	hypothetical protein
			transl_table	11
17508	17146	CDS
			locus_tag	Terrestrivirus_2_17
			product	hypothetical protein
			transl_table	11
18668	18426	CDS
			locus_tag	Terrestrivirus_2_18
			product	hypothetical protein
			transl_table	11
18926	19045	CDS
			locus_tag	Terrestrivirus_2_19
			product	hypothetical protein
			transl_table	11
19053	20777	CDS
			locus_tag	Terrestrivirus_2_20
			product	HNH endonuclease
			transl_table	11
21266	21090	CDS
			locus_tag	Terrestrivirus_2_21
			product	hypothetical protein
			transl_table	11
21682	21284	CDS
			locus_tag	Terrestrivirus_2_22
			product	peptidyl-tRNA hydrolase
			transl_table	11
22219	21800	CDS
			locus_tag	Terrestrivirus_2_23
			product	Iron-binding protein IscA
			transl_table	11
22335	22805	CDS
			locus_tag	Terrestrivirus_2_24
			product	hypothetical protein
			transl_table	11
23201	22830	CDS
			locus_tag	Terrestrivirus_2_25
			product	hypothetical protein
			transl_table	11
28261	23297	CDS
			locus_tag	Terrestrivirus_2_26
			product	ankyrin repeat protein
			transl_table	11
29104	28415	CDS
			locus_tag	Terrestrivirus_2_27
			product	hypothetical protein
			transl_table	11
30149	29202	CDS
			locus_tag	Terrestrivirus_2_28
			product	hypothetical protein CBC91_00715
			transl_table	11
30577	30461	CDS
			locus_tag	Terrestrivirus_2_29
			product	hypothetical protein
			transl_table	11
31788	30850	CDS
			locus_tag	Terrestrivirus_2_30
			product	hypothetical protein 162300064
			transl_table	11
34780	32135	CDS
			locus_tag	Terrestrivirus_2_31
			product	hypothetical protein A3H53_02540
			transl_table	11
34979	36133	CDS
			locus_tag	Terrestrivirus_2_32
			product	hypothetical protein BATDEDRAFT_34892
			transl_table	11
36517	36146	CDS
			locus_tag	Terrestrivirus_2_33
			product	hypothetical protein
			transl_table	11
37470	36655	CDS
			locus_tag	Terrestrivirus_2_34
			product	ankyrin repeat protein
			transl_table	11
38962	37574	CDS
			locus_tag	Terrestrivirus_2_35
			product	hypothetical protein
			transl_table	11
39141	41492	CDS
			locus_tag	Terrestrivirus_2_36
			product	molybdenum cofactor biosynthesis protein A
			transl_table	11
41535	42206	CDS
			locus_tag	Terrestrivirus_2_37
			product	hemerythrin
			transl_table	11
42876	42208	CDS
			locus_tag	Terrestrivirus_2_38
			product	hypothetical protein
			transl_table	11
43532	42993	CDS
			locus_tag	Terrestrivirus_2_39
			product	hypothetical protein
			transl_table	11
44855	43623	CDS
			locus_tag	Terrestrivirus_2_40
			product	hypothetical protein
			transl_table	11
45064	46482	CDS
			locus_tag	Terrestrivirus_2_41
			product	hypothetical protein
			transl_table	11
47927	46548	CDS
			locus_tag	Terrestrivirus_2_42
			product	seryl-tRNA synthetase
			transl_table	11
49138	48053	CDS
			locus_tag	Terrestrivirus_2_43
			product	putative ADP-ribosylglycohydrolase
			transl_table	11
49944	49258	CDS
			locus_tag	Terrestrivirus_2_44
			product	hypothetical protein BCR44DRAFT_69335
			transl_table	11
50560	50123	CDS
			locus_tag	Terrestrivirus_2_45
			product	hypothetical protein PROFUN_02331
			transl_table	11
50751	52325	CDS
			locus_tag	Terrestrivirus_2_46
			product	glutamine tRNA-synthetase
			transl_table	11
53372	52332	CDS
			locus_tag	Terrestrivirus_2_47
			product	hypothetical protein RFI_08711
			transl_table	11
53532	54458	CDS
			locus_tag	Terrestrivirus_2_48
			product	glycosyltransferase family 25
			transl_table	11
54747	54460	CDS
			locus_tag	Terrestrivirus_2_49
			product	hypothetical protein
			transl_table	11
54921	55334	CDS
			locus_tag	Terrestrivirus_2_50
			product	Conserved protein mycobacteriophage D29, Gp61 (DUF2493)
			transl_table	11
56141	55365	CDS
			locus_tag	Terrestrivirus_2_51
			product	hypothetical protein
			transl_table	11
56314	56538	CDS
			locus_tag	Terrestrivirus_2_52
			product	hypothetical protein
			transl_table	11
56595	56906	CDS
			locus_tag	Terrestrivirus_2_53
			product	hypothetical protein
			transl_table	11
57116	57496	CDS
			locus_tag	Terrestrivirus_2_54
			product	hypothetical protein
			transl_table	11
58061	57504	CDS
			locus_tag	Terrestrivirus_2_55
			product	hypothetical protein
			transl_table	11
60561	58171	CDS
			locus_tag	Terrestrivirus_2_56
			product	hypothetical protein Indivirus_2_103
			transl_table	11
61392	60655	CDS
			locus_tag	Terrestrivirus_2_57
			product	hypothetical protein
			transl_table	11
62887	61427	CDS
			locus_tag	Terrestrivirus_2_58
			product	DnaJ-like subfamily C member 3
			transl_table	11
63307	63011	CDS
			locus_tag	Terrestrivirus_2_59
			product	hypothetical protein
			transl_table	11
63675	64112	CDS
			locus_tag	Terrestrivirus_2_60
			product	hypothetical protein
			transl_table	11
64277	65050	CDS
			locus_tag	Terrestrivirus_2_61
			product	hypothetical protein
			transl_table	11
65112	65516	CDS
			locus_tag	Terrestrivirus_2_62
			product	hypothetical protein
			transl_table	11
66161	65559	CDS
			locus_tag	Terrestrivirus_2_63
			product	HD superfamily hydrolase, putative (macronuclear)
			transl_table	11
66324	67010	CDS
			locus_tag	Terrestrivirus_2_64
			product	putative orfan
			transl_table	11
67824	67054	CDS
			locus_tag	Terrestrivirus_2_65
			product	Mitochondrial carrier protein
			transl_table	11
68019	69479	CDS
			locus_tag	Terrestrivirus_2_66
			product	MGC52527 protein
			transl_table	11
70163	69606	CDS
			locus_tag	Terrestrivirus_2_67
			product	hypothetical protein
			transl_table	11
71497	70160	CDS
			locus_tag	Terrestrivirus_2_68
			product	hypothetical protein AB834_00070
			transl_table	11
72645	71551	CDS
			locus_tag	Terrestrivirus_2_69
			product	hypothetical protein M378DRAFT_67282
			transl_table	11
72749	73267	CDS
			locus_tag	Terrestrivirus_2_70
			product	hypothetical protein
			transl_table	11
73960	73280	CDS
			locus_tag	Terrestrivirus_2_71
			product	methyltransferase
			transl_table	11
74190	74909	CDS
			locus_tag	Terrestrivirus_2_72
			product	hypothetical protein CBC05_03300
			transl_table	11
75619	74951	CDS
			locus_tag	Terrestrivirus_2_73
			product	hypothetical protein
			transl_table	11
76409	75675	CDS
			locus_tag	Terrestrivirus_2_74
			product	hypothetical protein
			transl_table	11
77569	76565	CDS
			locus_tag	Terrestrivirus_2_75
			product	hypothetical protein
			transl_table	11
77832	77635	CDS
			locus_tag	Terrestrivirus_2_76
			product	hypothetical protein
			transl_table	11
78214	77840	CDS
			locus_tag	Terrestrivirus_2_77
			product	hypothetical protein
			transl_table	11
81605	78306	CDS
			locus_tag	Terrestrivirus_2_78
			product	hypothetical protein
			transl_table	11
82043	81807	CDS
			locus_tag	Terrestrivirus_2_79
			product	thymidine kinase
			transl_table	11
83170	82040	CDS
			locus_tag	Terrestrivirus_2_80
			product	D5-like helicase-primase
			transl_table	11
84103	83267	CDS
			locus_tag	Terrestrivirus_2_81
			product	hypothetical protein
			transl_table	11
84765	84178	CDS
			locus_tag	Terrestrivirus_2_82
			product	hypothetical protein
			transl_table	11
84969	87080	CDS
			locus_tag	Terrestrivirus_2_83
			product	putative vesicle-fusion ATPase
			transl_table	11
87236	89377	CDS
			locus_tag	Terrestrivirus_2_84
			product	hypothetical protein
			transl_table	11
91490	89379	CDS
			locus_tag	Terrestrivirus_2_85
			product	valine--tRNA ligase
			transl_table	11
92266	91628	CDS
			locus_tag	Terrestrivirus_2_86
			product	NADAR family protein
			transl_table	11
95502	92392	CDS
			locus_tag	Terrestrivirus_2_87
			product	hypothetical protein
			transl_table	11
96695	95646	CDS
			locus_tag	Terrestrivirus_2_88
			product	hypothetical protein
			transl_table	11
98913	96757	CDS
			locus_tag	Terrestrivirus_2_89
			product	hypothetical protein
			transl_table	11
99125	100288	CDS
			locus_tag	Terrestrivirus_2_90
			product	hypothetical protein
			transl_table	11
100845	100330	CDS
			locus_tag	Terrestrivirus_2_91
			product	hypothetical protein
			transl_table	11
102424	100820	CDS
			locus_tag	Terrestrivirus_2_92
			product	cysteinyl-tRNA synthetase
			transl_table	11
102819	102574	CDS
			locus_tag	Terrestrivirus_2_93
			product	hypothetical protein
			transl_table	11
104095	102953	CDS
			locus_tag	Terrestrivirus_2_94
			product	hypothetical protein GUITHDRAFT_145506
			transl_table	11
104245	104958	CDS
			locus_tag	Terrestrivirus_2_95
			product	hypothetical protein
			transl_table	11
104994	105491	CDS
			locus_tag	Terrestrivirus_2_96
			product	hypothetical protein
			transl_table	11
106551	105910	CDS
			locus_tag	Terrestrivirus_2_97
			product	serine hydrolase
			transl_table	11
106708	107919	CDS
			locus_tag	Terrestrivirus_2_98
			product	PREDICTED: E3 ubiquitin-protein ligase At4g11680 isoform X2
			transl_table	11
108076	107936	CDS
			locus_tag	Terrestrivirus_2_99
			product	hypothetical protein
			transl_table	11
108543	108244	CDS
			locus_tag	Terrestrivirus_2_100
			product	hypothetical protein
			transl_table	11
109316	108666	CDS
			locus_tag	Terrestrivirus_2_101
			product	hypothetical protein
			transl_table	11
110778	109444	CDS
			locus_tag	Terrestrivirus_2_102
			product	hypothetical protein
			transl_table	11
112359	110833	CDS
			locus_tag	Terrestrivirus_2_103
			product	hypothetical protein Indivirus_1_31
			transl_table	11
113065	112538	CDS
			locus_tag	Terrestrivirus_2_104
			product	hypothetical protein Indivirus_1_31
			transl_table	11
115278	113248	CDS
			locus_tag	Terrestrivirus_2_105
			product	putative tRNA(Ile)lysidine synthase
			transl_table	11
115532	116269	CDS
			locus_tag	Terrestrivirus_2_106
			product	Syntaxin-51
			transl_table	11
117813	116266	CDS
			locus_tag	Terrestrivirus_2_107
			product	nucleotidyltransferase
			transl_table	11
118913	117909	CDS
			locus_tag	Terrestrivirus_2_108
			product	hypothetical protein
			transl_table	11
119682	118996	CDS
			locus_tag	Terrestrivirus_2_109
			product	mitochondrial fission process protein 1
			transl_table	11
119869	120423	CDS
			locus_tag	Terrestrivirus_2_110
			product	hypothetical protein
			transl_table	11
120612	121256	CDS
			locus_tag	Terrestrivirus_2_111
			product	hypothetical protein
			transl_table	11
121927	121268	CDS
			locus_tag	Terrestrivirus_2_112
			product	ras-related protein Rab-2-A
			transl_table	11
122090	123340	CDS
			locus_tag	Terrestrivirus_2_113
			product	THS1-like protein
			transl_table	11
124149	123361	CDS
			locus_tag	Terrestrivirus_2_114
			product	hypothetical protein Catovirus_1_847
			transl_table	11
124259	124149	CDS
			locus_tag	Terrestrivirus_2_115
			product	hypothetical protein
			transl_table	11
125874	124240	CDS
			locus_tag	Terrestrivirus_2_116
			product	hypothetical protein
			transl_table	11
126077	126517	CDS
			locus_tag	Terrestrivirus_2_117
			product	hypothetical protein
			transl_table	11
128185	126554	CDS
			locus_tag	Terrestrivirus_2_118
			product	hypothetical protein
			transl_table	11
129869	128280	CDS
			locus_tag	Terrestrivirus_2_119
			product	polyADP-ribose polymerase
			transl_table	11
129855	130634	CDS
			locus_tag	Terrestrivirus_2_120
			product	hypothetical protein Klosneuvirus_3_29
			transl_table	11
132798	130636	CDS
			locus_tag	Terrestrivirus_2_121
			product	hypothetical protein MIMI_R757b
			transl_table	11
134709	132925	CDS
			locus_tag	Terrestrivirus_2_122
			product	hypothetical protein Catovirus_1_853
			transl_table	11
135637	134801	CDS
			locus_tag	Terrestrivirus_2_123
			product	RNA polymerase II C-terminal domain phosphatase-like 4
			transl_table	11
138778	136748	CDS
			locus_tag	Terrestrivirus_2_124
			product	hypothetical protein
			transl_table	11
139893	138850	CDS
			locus_tag	Terrestrivirus_2_125
			product	hypothetical protein
			transl_table	11
142260	139987	CDS
			locus_tag	Terrestrivirus_2_126
			product	ATPase, AAA domain containing protein
			transl_table	11
142993	142388	CDS
			locus_tag	Terrestrivirus_2_127
			product	restriction endonuclease subunit S
			transl_table	11
143750	143088	CDS
			locus_tag	Terrestrivirus_2_128
			product	hypothetical protein
			transl_table	11
146640	145183	CDS
			locus_tag	Terrestrivirus_2_129
			product	putative Bro-N domain-containing protein
			transl_table	11
147512	148126	CDS
			locus_tag	Terrestrivirus_2_130
			product	hypothetical protein
			transl_table	11
149509	148133	CDS
			locus_tag	Terrestrivirus_2_131
			product	hypothetical protein mv_L2
			transl_table	11
151130	152833	CDS
			locus_tag	Terrestrivirus_2_132
			product	putative BRO-F
			transl_table	11
154432	153875	CDS
			locus_tag	Terrestrivirus_2_133
			product	hypothetical protein
			transl_table	11
154547	155167	CDS
			locus_tag	Terrestrivirus_2_134
			product	putative BRO-F
			transl_table	11
157839	155677	CDS
			locus_tag	Terrestrivirus_2_135
			product	hypothetical protein
			transl_table	11
158357	157911	CDS
			locus_tag	Terrestrivirus_2_136
			product	hypothetical protein
			transl_table	11
160097	159465	CDS
			locus_tag	Terrestrivirus_2_137
			product	hypothetical protein
			transl_table	11
160102	160713	CDS
			locus_tag	Terrestrivirus_2_138
			product	hypothetical protein
			transl_table	11
161732	163000	CDS
			locus_tag	Terrestrivirus_2_139
			product	ubiquitin fusion degradation protein 1 homolog, partial
			transl_table	11
163096	164454	CDS
			locus_tag	Terrestrivirus_2_140
			product	hypothetical protein Catovirus_1_872
			transl_table	11
165258	164473	CDS
			locus_tag	Terrestrivirus_2_141
			product	phosphorylcholine metabolism protein LicD and glycosyltransferase family 92 domain containing protein
			transl_table	11
165631	165335	CDS
			locus_tag	Terrestrivirus_2_142
			product	hypothetical protein
			transl_table	11
165733	166983	CDS
			locus_tag	Terrestrivirus_2_143
			product	hypothetical protein PBI_SCTP2_208
			transl_table	11
167956	167000	CDS
			locus_tag	Terrestrivirus_2_144
			product	hypothetical protein
			transl_table	11
168058	168588	CDS
			locus_tag	Terrestrivirus_2_145
			product	hypothetical protein A2335_00505, partial
			transl_table	11
172654	168602	CDS
			locus_tag	Terrestrivirus_2_146
			product	DEAD/SNF2-like helicase
			transl_table	11
173465	172722	CDS
			locus_tag	Terrestrivirus_2_147
			product	hypothetical protein FRACYDRAFT_235058
			transl_table	11
173883	173575	CDS
			locus_tag	Terrestrivirus_2_148
			product	hypothetical protein
			transl_table	11
174816	173971	CDS
			locus_tag	Terrestrivirus_2_149
			product	hypothetical protein
			transl_table	11
175038	176813	CDS
			locus_tag	Terrestrivirus_2_150
			product	hypothetical protein
			transl_table	11
176960	178165	CDS
			locus_tag	Terrestrivirus_2_151
			product	hypothetical protein
			transl_table	11
178511	178176	CDS
			locus_tag	Terrestrivirus_2_152
			product	hypothetical protein
			transl_table	11
178989	178591	CDS
			locus_tag	Terrestrivirus_2_153
			product	thioredoxin-like protein
			transl_table	11
179199	181415	CDS
			locus_tag	Terrestrivirus_2_154
			product	ATPase, AAA domain containing protein
			transl_table	11
182089	181454	CDS
			locus_tag	Terrestrivirus_2_155
			product	hypothetical protein
			transl_table	11
182133	184346	CDS
			locus_tag	Terrestrivirus_2_156
			product	Multi-sensor hybrid histidine kinase
			transl_table	11
184857	184351	CDS
			locus_tag	Terrestrivirus_2_157
			product	PAS domain-containing sensor histidine kinase
			transl_table	11
185422	184994	CDS
			locus_tag	Terrestrivirus_2_158
			product	Ubiquitin-conjugating enzyme spm2
			transl_table	11
186239	185544	CDS
			locus_tag	Terrestrivirus_2_159
			product	hypothetical protein
			transl_table	11
186856	186275	CDS
			locus_tag	Terrestrivirus_2_160
			product	hypothetical protein
			transl_table	11
187963	186935	CDS
			locus_tag	Terrestrivirus_2_161
			product	hypothetical protein
			transl_table	11
188076	188561	CDS
			locus_tag	Terrestrivirus_2_162
			product	thioredoxin
			transl_table	11
190590	188611	CDS
			locus_tag	Terrestrivirus_2_163
			product	TROVE domain protein
			transl_table	11
191358	190735	CDS
			locus_tag	Terrestrivirus_2_164
			product	CDP-alcohol phosphatidyltransferase
			transl_table	11
194376	191419	CDS
			locus_tag	Terrestrivirus_2_165
			product	ubiquitin-activating enzyme E1
			transl_table	11
195071	194598	CDS
			locus_tag	Terrestrivirus_2_166
			product	phosphohistidine phosphatase
			transl_table	11
195275	196063	CDS
			locus_tag	Terrestrivirus_2_167
			product	hypothetical protein
			transl_table	11
196555	196067	CDS
			locus_tag	Terrestrivirus_2_168
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
197352	196672	CDS
			locus_tag	Terrestrivirus_2_169
			product	hypothetical protein Klosneuvirus_1_405
			transl_table	11
197557	198276	CDS
			locus_tag	Terrestrivirus_2_170
			product	hypothetical protein
			transl_table	11
198363	198785	CDS
			locus_tag	Terrestrivirus_2_171
			product	hypothetical protein
			transl_table	11
199524	199426	CDS
			locus_tag	Terrestrivirus_2_172
			product	hypothetical protein
			transl_table	11
199752	200084	CDS
			locus_tag	Terrestrivirus_2_173
			product	hypothetical protein
			transl_table	11
200181	200498	CDS
			locus_tag	Terrestrivirus_2_174
			product	hypothetical protein
			transl_table	11
200594	201112	CDS
			locus_tag	Terrestrivirus_2_175
			product	dCMP deaminase
			transl_table	11
201189	201344	CDS
			locus_tag	Terrestrivirus_2_176
			product	hypothetical protein
			transl_table	11
201602	201492	CDS
			locus_tag	Terrestrivirus_2_177
			product	hypothetical protein
			transl_table	11
202024	201671	CDS
			locus_tag	Terrestrivirus_2_178
			product	GIY-YIG nuclease
			transl_table	11
202893	202336	CDS
			locus_tag	Terrestrivirus_2_179
			product	phosphoribosyl-ATP pyrophosphohydrolase
			transl_table	11
203098	203511	CDS
			locus_tag	Terrestrivirus_2_180
			product	hypothetical protein ml_125
			transl_table	11
203598	205058	CDS
			locus_tag	Terrestrivirus_2_181
			product	DEAD/SNF2-like helicase
			transl_table	11
206091	205255	CDS
			locus_tag	Terrestrivirus_2_182
			product	hypothetical protein DLAC_10967
			transl_table	11
207317	206166	CDS
			locus_tag	Terrestrivirus_2_183
			product	hypothetical protein BOX15_Mlig014772g2
			transl_table	11
207472	208167	CDS
			locus_tag	Terrestrivirus_2_184
			product	hypothetical protein Indivirus_1_202
			transl_table	11
208285	209739	CDS
			locus_tag	Terrestrivirus_2_185
			product	putative AAA+ family ATPase
			transl_table	11
210210	209845	CDS
			locus_tag	Terrestrivirus_2_186
			product	hypothetical protein
			transl_table	11
210284	211234	CDS
			locus_tag	Terrestrivirus_2_187
			product	hypothetical protein Indivirus_1_200
			transl_table	11
211292	212494	CDS
			locus_tag	Terrestrivirus_2_188
			product	hypothetical protein
			transl_table	11
212607	213719	CDS
			locus_tag	Terrestrivirus_2_189
			product	hypothetical protein
			transl_table	11
214669	213788	CDS
			locus_tag	Terrestrivirus_2_190
			product	hypothetical protein
			transl_table	11
215303	214791	CDS
			locus_tag	Terrestrivirus_2_191
			product	hypothetical protein
			transl_table	11
215495	216079	CDS
			locus_tag	Terrestrivirus_2_192
			product	hypothetical protein
			transl_table	11
218265	216292	CDS
			locus_tag	Terrestrivirus_2_193
			product	hypothetical protein
			transl_table	11
218487	219452	CDS
			locus_tag	Terrestrivirus_2_194
			product	putative pAp phosphatase
			transl_table	11
219525	221216	CDS
			locus_tag	Terrestrivirus_2_195
			product	hypothetical protein SZ59_C0002G0337
			transl_table	11
221299	221607	CDS
			locus_tag	Terrestrivirus_2_196
			product	hypothetical protein
			transl_table	11
221768	222739	CDS
			locus_tag	Terrestrivirus_2_197
			product	hypothetical protein
			transl_table	11
223148	222858	CDS
			locus_tag	Terrestrivirus_2_198
			product	hypothetical protein
			transl_table	11
223324	223746	CDS
			locus_tag	Terrestrivirus_2_199
			product	hypothetical protein
			transl_table	11
223896	223762	CDS
			locus_tag	Terrestrivirus_2_200
			product	hypothetical protein
			transl_table	11
224692	223982	CDS
			locus_tag	Terrestrivirus_2_201
			product	HNH endonuclease
			transl_table	11
225282	224749	CDS
			locus_tag	Terrestrivirus_2_202
			product	PREDICTED: RING finger and CHY zinc finger domain-containing protein 1-like
			transl_table	11
227351	225369	CDS
			locus_tag	Terrestrivirus_2_203
			product	elongator complex protein putative
			transl_table	11
228293	227481	CDS
			locus_tag	Terrestrivirus_2_204
			product	hypothetical protein Klosneuvirus_1_369
			transl_table	11
228964	228392	CDS
			locus_tag	Terrestrivirus_2_205
			product	hypothetical protein Catovirus_1_903
			transl_table	11
229111	230295	CDS
			locus_tag	Terrestrivirus_2_206
			product	putative orfan
			transl_table	11
230894	230292	CDS
			locus_tag	Terrestrivirus_2_207
			product	hypothetical protein Klosneuvirus_1_362
			transl_table	11
233365	231005	CDS
			locus_tag	Terrestrivirus_2_208
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
233413	233520	CDS
			locus_tag	Terrestrivirus_2_209
			product	hypothetical protein
			transl_table	11
233546	235486	CDS
			locus_tag	Terrestrivirus_2_210
			product	dynamin family protein
			transl_table	11
236103	235516	CDS
			locus_tag	Terrestrivirus_2_211
			product	hypothetical protein
			transl_table	11
237065	236220	CDS
			locus_tag	Terrestrivirus_2_212
			product	hypothetical protein
			transl_table	11
238819	237179	CDS
			locus_tag	Terrestrivirus_2_213
			product	glycyl-tRNA synthetase
			transl_table	11
239978	239001	CDS
			locus_tag	Terrestrivirus_2_214
			product	hypothetical protein CVV23_15630
			transl_table	11
240144	241292	CDS
			locus_tag	Terrestrivirus_2_215
			product	hypothetical protein
			transl_table	11
242186	241314	CDS
			locus_tag	Terrestrivirus_2_216
			product	hypothetical protein
			transl_table	11
243004	242264	CDS
			locus_tag	Terrestrivirus_2_217
			product	glutamine amidotransferase
			transl_table	11
243203	243688	CDS
			locus_tag	Terrestrivirus_2_218
			product	hypothetical protein
			transl_table	11
245415	243703	CDS
			locus_tag	Terrestrivirus_2_219
			product	leucine-rich repeat protein
			transl_table	11
246532	245570	CDS
			locus_tag	Terrestrivirus_2_220
			product	hypothetical protein
			transl_table	11
246994	246668	CDS
			locus_tag	Terrestrivirus_2_221
			product	hypothetical protein
			transl_table	11
248187	247081	CDS
			locus_tag	Terrestrivirus_2_222
			product	hypothetical protein
			transl_table	11
248358	248873	CDS
			locus_tag	Terrestrivirus_2_223
			product	hypothetical protein
			transl_table	11
248947	251493	CDS
			locus_tag	Terrestrivirus_2_224
			product	glycosyltransferase
			transl_table	11
252268	251519	CDS
			locus_tag	Terrestrivirus_2_225
			product	hypothetical protein
			transl_table	11
252997	252371	CDS
			locus_tag	Terrestrivirus_2_226
			product	hypothetical protein Catovirus_1_911
			transl_table	11
254762	253095	CDS
			locus_tag	Terrestrivirus_2_227
			product	hypothetical protein
			transl_table	11
254900	255697	CDS
			locus_tag	Terrestrivirus_2_228
			product	hypothetical protein Catovirus_1_919
			transl_table	11
259377	255712	CDS
			locus_tag	Terrestrivirus_2_229
			product	hypothetical protein
			transl_table	11
263600	259455	CDS
			locus_tag	Terrestrivirus_2_230
			product	hypothetical protein
			transl_table	11
265916	263682	CDS
			locus_tag	Terrestrivirus_2_231
			product	hypothetical protein
			transl_table	11
266600	266007	CDS
			locus_tag	Terrestrivirus_2_232
			product	hypothetical protein
			transl_table	11
267354	266674	CDS
			locus_tag	Terrestrivirus_2_233
			product	hypothetical protein
			transl_table	11
267399	267857	CDS
			locus_tag	Terrestrivirus_2_234
			product	hypothetical protein
			transl_table	11
268761	267880	CDS
			locus_tag	Terrestrivirus_2_235
			product	hypothetical protein Catovirus_1_922
			transl_table	11
268871	269272	CDS
			locus_tag	Terrestrivirus_2_236
			product	hypothetical protein Catovirus_1_923
			transl_table	11
269385	272537	CDS
			locus_tag	Terrestrivirus_2_237
			product	tRNA 4-thiouridine(8) synthase ThiI
			transl_table	11
273653	272559	CDS
			locus_tag	Terrestrivirus_2_238
			product	hypothetical protein
			transl_table	11
274447	273752	CDS
			locus_tag	Terrestrivirus_2_239
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_3
1017	1	CDS
			locus_tag	Terrestrivirus_3_1
			product	hypothetical protein
			transl_table	11
1530	1120	CDS
			locus_tag	Terrestrivirus_3_2
			product	hypothetical protein BMW23_0448
			transl_table	11
4012	1625	CDS
			locus_tag	Terrestrivirus_3_3
			product	putative minor capsid protein
			transl_table	11
4178	8050	CDS
			locus_tag	Terrestrivirus_3_4
			product	DNA gyrase/topoisomerase IV
			transl_table	11
8999	8064	CDS
			locus_tag	Terrestrivirus_3_5
			product	hypothetical protein Catovirus_1_1095
			transl_table	11
9089	11512	CDS
			locus_tag	Terrestrivirus_3_6
			product	U-box domain protein
			transl_table	11
11593	12141	CDS
			locus_tag	Terrestrivirus_3_7
			product	N-acyltransferase
			transl_table	11
12246	13688	CDS
			locus_tag	Terrestrivirus_3_8
			product	methyltransferase domain-containing protein
			transl_table	11
13744	16053	CDS
			locus_tag	Terrestrivirus_3_9
			product	metallophosphoesterase
			transl_table	11
16114	16350	CDS
			locus_tag	Terrestrivirus_3_10
			product	hypothetical protein
			transl_table	11
17149	16352	CDS
			locus_tag	Terrestrivirus_3_11
			product	apurinic endonuclease
			transl_table	11
17360	18619	CDS
			locus_tag	Terrestrivirus_3_12
			product	replication factor C small subunit
			transl_table	11
18780	20486	CDS
			locus_tag	Terrestrivirus_3_13
			product	asparagine synthase
			transl_table	11
20788	20510	CDS
			locus_tag	Terrestrivirus_3_14
			product	hypothetical protein Catovirus_2_326
			transl_table	11
20874	21647	CDS
			locus_tag	Terrestrivirus_3_15
			product	hypothetical protein PGCG_00340
			transl_table	11
22966	21728	CDS
			locus_tag	Terrestrivirus_3_16
			product	hypothetical protein Catovirus_2_325
			transl_table	11
28205	23049	CDS
			locus_tag	Terrestrivirus_3_17
			product	hypothetical protein Catovirus_2_323
			transl_table	11
28681	30186	CDS
			locus_tag	Terrestrivirus_3_18
			product	RNA ligase
			transl_table	11
31272	30178	CDS
			locus_tag	Terrestrivirus_3_19
			product	DNA directed RNA polymerase subunit L
			transl_table	11
31417	32067	CDS
			locus_tag	Terrestrivirus_3_20
			product	hypothetical protein Indivirus_1_43
			transl_table	11
32137	32553	CDS
			locus_tag	Terrestrivirus_3_21
			product	hypothetical protein Klosneuvirus_1_126
			transl_table	11
32642	33463	CDS
			locus_tag	Terrestrivirus_3_22
			product	hypothetical protein
			transl_table	11
33597	34922	CDS
			locus_tag	Terrestrivirus_3_23
			product	hypothetical protein Catovirus_2_317
			transl_table	11
35002	35850	CDS
			locus_tag	Terrestrivirus_3_24
			product	hypothetical protein
			transl_table	11
36107	36979	CDS
			locus_tag	Terrestrivirus_3_25
			product	hypothetical protein Catovirus_2_315
			transl_table	11
38156	36996	CDS
			locus_tag	Terrestrivirus_3_26
			product	hypothetical protein Klosneuvirus_1_122
			transl_table	11
38417	39499	CDS
			locus_tag	Terrestrivirus_3_27
			product	tyrosyl-tRNA synthetase
			transl_table	11
40605	39508	CDS
			locus_tag	Terrestrivirus_3_28
			product	polynucleotide phosphatase/kinase
			transl_table	11
40786	42393	CDS
			locus_tag	Terrestrivirus_3_29
			product	serpin
			transl_table	11
43004	42396	CDS
			locus_tag	Terrestrivirus_3_30
			product	hypothetical protein Catovirus_2_312
			transl_table	11
43103	44608	CDS
			locus_tag	Terrestrivirus_3_31
			product	hypothetical protein Catovirus_2_311
			transl_table	11
45685	44624	CDS
			locus_tag	Terrestrivirus_3_32
			product	hypothetical protein
			transl_table	11
45779	46444	CDS
			locus_tag	Terrestrivirus_3_33
			product	hypothetical protein Catovirus_2_310
			transl_table	11
46597	47064	CDS
			locus_tag	Terrestrivirus_3_34
			product	Ubiquitin-conjugating enzyme E2-17 kDa 3
			transl_table	11
47291	47938	CDS
			locus_tag	Terrestrivirus_3_35
			product	hypothetical protein Catovirus_2_308
			transl_table	11
48014	51715	CDS
			locus_tag	Terrestrivirus_3_36
			product	hypothetical protein Catovirus_2_307
			transl_table	11
52919	51786	CDS
			locus_tag	Terrestrivirus_3_37
			product	TATA box binding protein
			transl_table	11
53102	53836	CDS
			locus_tag	Terrestrivirus_3_38
			product	hypothetical protein
			transl_table	11
54021	54446	CDS
			locus_tag	Terrestrivirus_3_39
			product	hypothetical protein
			transl_table	11
55587	54376	CDS
			locus_tag	Terrestrivirus_3_40
			product	hypothetical protein Klosneuvirus_1_57
			transl_table	11
55781	56884	CDS
			locus_tag	Terrestrivirus_3_41
			product	hypothetical protein
			transl_table	11
57917	56991	CDS
			locus_tag	Terrestrivirus_3_42
			product	hypothetical protein Catovirus_2_305
			transl_table	11
58060	59868	CDS
			locus_tag	Terrestrivirus_3_43
			product	NCLDV major capsid protein
			transl_table	11
60179	65086	CDS
			locus_tag	Terrestrivirus_3_44
			product	NCLDV major capsid protein
			transl_table	11
65191	65373	CDS
			locus_tag	Terrestrivirus_3_45
			product	hypothetical protein
			transl_table	11
66963	65398	CDS
			locus_tag	Terrestrivirus_3_46
			product	NCLDV major capsid protein
			transl_table	11
69301	67109	CDS
			locus_tag	Terrestrivirus_3_47
			product	hypothetical protein Catovirus_2_301
			transl_table	11
69510	69986	CDS
			locus_tag	Terrestrivirus_3_48
			product	PREDICTED: thioredoxin domain-containing protein 5
			transl_table	11
71260	70025	CDS
			locus_tag	Terrestrivirus_3_49
			product	ADP-ribosylglycohydrolase
			transl_table	11
71384	72589	CDS
			locus_tag	Terrestrivirus_3_50
			product	DnaJ domain protein
			transl_table	11
72696	73466	CDS
			locus_tag	Terrestrivirus_3_51
			product	hypothetical protein
			transl_table	11
73569	75227	CDS
			locus_tag	Terrestrivirus_3_52
			product	ATP binding cassette sub family F
			transl_table	11
75334	75224	CDS
			locus_tag	Terrestrivirus_3_53
			product	hypothetical protein
			transl_table	11
75401	76330	CDS
			locus_tag	Terrestrivirus_3_54
			product	hypothetical protein SARC_06465
			transl_table	11
76426	76830	CDS
			locus_tag	Terrestrivirus_3_55
			product	hypothetical protein
			transl_table	11
77323	76913	CDS
			locus_tag	Terrestrivirus_3_56
			product	hypothetical protein
			transl_table	11
77888	77397	CDS
			locus_tag	Terrestrivirus_3_57
			product	putative ubiquitin-conjugating enzyme E2
			transl_table	11
78923	78000	CDS
			locus_tag	Terrestrivirus_3_58
			product	patatin-like phospholipase
			transl_table	11
79034	79486	CDS
			locus_tag	Terrestrivirus_3_59
			product	hypothetical protein Catovirus_2_296
			transl_table	11
80506	79469	CDS
			locus_tag	Terrestrivirus_3_60
			product	hypothetical protein
			transl_table	11
80578	80904	CDS
			locus_tag	Terrestrivirus_3_61
			product	hypothetical protein
			transl_table	11
81060	83201	CDS
			locus_tag	Terrestrivirus_3_62
			product	hypothetical protein Catovirus_2_294
			transl_table	11
84159	83293	CDS
			locus_tag	Terrestrivirus_3_63
			product	hypothetical protein
			transl_table	11
84325	85794	CDS
			locus_tag	Terrestrivirus_3_64
			product	hypothetical protein Catovirus_2_292
			transl_table	11
85931	86296	CDS
			locus_tag	Terrestrivirus_3_65
			product	hypothetical protein
			transl_table	11
87533	86331	CDS
			locus_tag	Terrestrivirus_3_66
			product	hypothetical protein Catovirus_2_291
			transl_table	11
88151	87624	CDS
			locus_tag	Terrestrivirus_3_67
			product	hypothetical protein
			transl_table	11
89112	88231	CDS
			locus_tag	Terrestrivirus_3_68
			product	phosphorylcholine metabolism protein LicD and glycosyltransferase family 92 domain containing protein
			transl_table	11
89859	89209	CDS
			locus_tag	Terrestrivirus_3_69
			product	hypothetical protein A3F40_03365
			transl_table	11
90429	89965	CDS
			locus_tag	Terrestrivirus_3_70
			product	hypothetical protein
			transl_table	11
90663	93758	CDS
			locus_tag	Terrestrivirus_3_71
			product	hypothetical protein
			transl_table	11
93890	94468	CDS
			locus_tag	Terrestrivirus_3_72
			product	hypothetical protein
			transl_table	11
95338	94505	CDS
			locus_tag	Terrestrivirus_3_73
			product	hypothetical protein
			transl_table	11
95498	95869	CDS
			locus_tag	Terrestrivirus_3_74
			product	hypothetical protein
			transl_table	11
95942	97573	CDS
			locus_tag	Terrestrivirus_3_75
			product	hypothetical protein
			transl_table	11
97667	99301	CDS
			locus_tag	Terrestrivirus_3_76
			product	hypothetical protein
			transl_table	11
99619	99308	CDS
			locus_tag	Terrestrivirus_3_77
			product	hypothetical protein
			transl_table	11
100352	99690	CDS
			locus_tag	Terrestrivirus_3_78
			product	hypothetical protein
			transl_table	11
100501	101115	CDS
			locus_tag	Terrestrivirus_3_79
			product	GTP-binding protein ypt1
			transl_table	11
101237	101893	CDS
			locus_tag	Terrestrivirus_3_80
			product	putative ORFan
			transl_table	11
102236	101925	CDS
			locus_tag	Terrestrivirus_3_81
			product	PREDICTED: AN1-type zinc finger protein 2A-like
			transl_table	11
102339	102223	CDS
			locus_tag	Terrestrivirus_3_82
			product	hypothetical protein
			transl_table	11
102507	103097	CDS
			locus_tag	Terrestrivirus_3_83
			product	hypothetical protein
			transl_table	11
103170	103814	CDS
			locus_tag	Terrestrivirus_3_84
			product	hypothetical protein
			transl_table	11
103910	105259	CDS
			locus_tag	Terrestrivirus_3_85
			product	hypothetical protein Indivirus_4_15
			transl_table	11
106323	105349	CDS
			locus_tag	Terrestrivirus_3_86
			product	zinc-binding loop containing protein
			transl_table	11
108706	106811	CDS
			locus_tag	Terrestrivirus_3_87
			product	NCLDV major capsid protein
			transl_table	11
110607	109330	CDS
			locus_tag	Terrestrivirus_3_88
			product	hypothetical protein CBB92_04050
			transl_table	11
111165	110680	CDS
			locus_tag	Terrestrivirus_3_89
			product	hypothetical protein
			transl_table	11
111503	111222	CDS
			locus_tag	Terrestrivirus_3_90
			product	hypothetical protein
			transl_table	11
111719	111600	CDS
			locus_tag	Terrestrivirus_3_91
			product	hypothetical protein
			transl_table	11
112047	113099	CDS
			locus_tag	Terrestrivirus_3_92
			product	late transcription factor VLTF3-like protein
			transl_table	11
113168	114451	CDS
			locus_tag	Terrestrivirus_3_93
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
114957	115052	CDS
			locus_tag	Terrestrivirus_3_94
			product	hypothetical protein
			transl_table	11
115621	115851	CDS
			locus_tag	Terrestrivirus_3_95
			product	late transcription factor VLTF3-like protein
			transl_table	11
115917	116177	CDS
			locus_tag	Terrestrivirus_3_96
			product	hypothetical protein
			transl_table	11
116526	117305	CDS
			locus_tag	Terrestrivirus_3_97
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
117383	118873	CDS
			locus_tag	Terrestrivirus_3_98
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
120481	119189	CDS
			locus_tag	Terrestrivirus_3_99
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
120687	122246	CDS
			locus_tag	Terrestrivirus_3_100
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
123185	122262	CDS
			locus_tag	Terrestrivirus_3_101
			product	packaging ATPase
			transl_table	11
123677	123264	CDS
			locus_tag	Terrestrivirus_3_102
			product	hypothetical protein BMW23_0542
			transl_table	11
124278	124472	CDS
			locus_tag	Terrestrivirus_3_103
			product	hypothetical protein
			transl_table	11
125861	124530	CDS
			locus_tag	Terrestrivirus_3_104
			product	hypothetical protein Catovirus_2_274
			transl_table	11
126051	126803	CDS
			locus_tag	Terrestrivirus_3_105
			product	invertase
			transl_table	11
127530	126907	CDS
			locus_tag	Terrestrivirus_3_106
			product	hypothetical protein BMW23_0534
			transl_table	11
128819	127629	CDS
			locus_tag	Terrestrivirus_3_107
			product	hypothetical protein Catovirus_2_272
			transl_table	11
128897	129937	CDS
			locus_tag	Terrestrivirus_3_108
			product	hypothetical protein
			transl_table	11
129997	130557	CDS
			locus_tag	Terrestrivirus_3_109
			product	hypothetical protein Indivirus_4_37
			transl_table	11
130679	131797	CDS
			locus_tag	Terrestrivirus_3_110
			product	hypothetical protein Catovirus_2_270
			transl_table	11
131879	132379	CDS
			locus_tag	Terrestrivirus_3_111
			product	hypothetical protein
			transl_table	11
132467	134134	CDS
			locus_tag	Terrestrivirus_3_112
			product	replication factor C large subunit
			transl_table	11
134291	136051	CDS
			locus_tag	Terrestrivirus_3_113
			product	P4B major core protein
			transl_table	11
137081	136116	CDS
			locus_tag	Terrestrivirus_3_114
			product	hypothetical protein
			transl_table	11
137957	137157	CDS
			locus_tag	Terrestrivirus_3_115
			product	putative alpha/beta hydrolase
			transl_table	11
139926	138052	CDS
			locus_tag	Terrestrivirus_3_116
			product	hypothetical protein B6I17_03770
			transl_table	11
140647	140024	CDS
			locus_tag	Terrestrivirus_3_117
			product	hypothetical protein Catovirus_2_265
			transl_table	11
140841	140716	CDS
			locus_tag	Terrestrivirus_3_118
			product	hypothetical protein
			transl_table	11
141284	140838	CDS
			locus_tag	Terrestrivirus_3_119
			product	cytoplasmic protein
			transl_table	11
141405	142007	CDS
			locus_tag	Terrestrivirus_3_120
			product	hypothetical protein
			transl_table	11
142108	142662	CDS
			locus_tag	Terrestrivirus_3_121
			product	hypothetical protein
			transl_table	11
144960	142690	CDS
			locus_tag	Terrestrivirus_3_122
			product	hypothetical protein Catovirus_2_262
			transl_table	11
147665	144978	CDS
			locus_tag	Terrestrivirus_3_123
			product	divergent protein kinase
			transl_table	11
149171	147816	CDS
			locus_tag	Terrestrivirus_3_124
			product	serine/threonine protein kinase
			transl_table	11
151231	149423	CDS
			locus_tag	Terrestrivirus_3_125
			product	XRN 5'-3' exonuclease
			transl_table	11
151370	152230	CDS
			locus_tag	Terrestrivirus_3_126
			product	hypothetical protein
			transl_table	11
152321	153547	CDS
			locus_tag	Terrestrivirus_3_127
			product	hypothetical protein
			transl_table	11
154809	153550	CDS
			locus_tag	Terrestrivirus_3_128
			product	metallophosphatase/phosphoesterase
			transl_table	11
155873	154902	CDS
			locus_tag	Terrestrivirus_3_129
			product	hypothetical protein
			transl_table	11
157349	155961	CDS
			locus_tag	Terrestrivirus_3_130
			product	hypothetical protein Klosneuvirus_9_8
			transl_table	11
157616	160579	CDS
			locus_tag	Terrestrivirus_3_131
			product	superfamily II DNA or RNA helicase
			transl_table	11
160727	162535	CDS
			locus_tag	Terrestrivirus_3_132
			product	HD phosphohydrolase
			transl_table	11
162887	164005	CDS
			locus_tag	Terrestrivirus_3_133
			product	hypothetical protein CBB97_24705
			transl_table	11
165172	164039	CDS
			locus_tag	Terrestrivirus_3_134
			product	replication factor C small subunit
			transl_table	11
166130	165306	CDS
			locus_tag	Terrestrivirus_3_135
			product	inositol oxygenase
			transl_table	11
166472	168403	CDS
			locus_tag	Terrestrivirus_3_136
			product	molecular chaperone DnaK
			transl_table	11
169433	168519	CDS
			locus_tag	Terrestrivirus_3_137
			product	hypothetical protein
			transl_table	11
170143	169706	CDS
			locus_tag	Terrestrivirus_3_138
			product	hypothetical protein Catovirus_2_249
			transl_table	11
170447	170211	CDS
			locus_tag	Terrestrivirus_3_139
			product	hypothetical protein
			transl_table	11
170428	171633	CDS
			locus_tag	Terrestrivirus_3_140
			product	hypothetical protein
			transl_table	11
171755	173374	CDS
			locus_tag	Terrestrivirus_3_141
			product	hypothetical protein RvY_02281
			transl_table	11
173622	173356	CDS
			locus_tag	Terrestrivirus_3_142
			product	hypothetical protein
			transl_table	11
174452	173601	CDS
			locus_tag	Terrestrivirus_3_143
			product	hypothetical protein
			transl_table	11
174710	174462	CDS
			locus_tag	Terrestrivirus_3_144
			product	hypothetical protein
			transl_table	11
176511	174793	CDS
			locus_tag	Terrestrivirus_3_145
			product	hypothetical protein Klosneuvirus_16_2
			transl_table	11
176761	177003	CDS
			locus_tag	Terrestrivirus_3_146
			product	polyubiquitin
			transl_table	11
177204	178109	CDS
			locus_tag	Terrestrivirus_3_147
			product	hypothetical protein
			transl_table	11
178190	180868	CDS
			locus_tag	Terrestrivirus_3_148
			product	hypothetical protein Klosneuvirus_5_30
			transl_table	11
180975	181592	CDS
			locus_tag	Terrestrivirus_3_149
			product	hypothetical protein
			transl_table	11
181650	182828	CDS
			locus_tag	Terrestrivirus_3_150
			product	hypothetical protein
			transl_table	11
183566	182832	CDS
			locus_tag	Terrestrivirus_3_151
			product	antibiotic biosynthesis monooxygenase
			transl_table	11
183709	183957	CDS
			locus_tag	Terrestrivirus_3_152
			product	hypothetical protein CE11_00045
			transl_table	11
184612	183962	CDS
			locus_tag	Terrestrivirus_3_153
			product	hypothetical protein Klosneuvirus_4_137
			transl_table	11
186481	184670	CDS
			locus_tag	Terrestrivirus_3_154
			product	serine/threonine protein kinase
			transl_table	11
186576	187550	CDS
			locus_tag	Terrestrivirus_3_155
			product	hypothetical protein
			transl_table	11
188015	187554	CDS
			locus_tag	Terrestrivirus_3_156
			product	hypothetical protein
			transl_table	11
188214	189929	CDS
			locus_tag	Terrestrivirus_3_157
			product	methionyl-tRNA synthetase
			transl_table	11
190040	191626	CDS
			locus_tag	Terrestrivirus_3_158
			product	hypothetical protein Catovirus_2_243
			transl_table	11
192500	191685	CDS
			locus_tag	Terrestrivirus_3_159
			product	serine/threonine protein phosphatase
			transl_table	11
193875	192565	CDS
			locus_tag	Terrestrivirus_3_160
			product	eukaryotic translation initiation factor 2 gamma subunit
			transl_table	11
194607	193996	CDS
			locus_tag	Terrestrivirus_3_161
			product	putative Ras-related protein Rab7
			transl_table	11
195761	194694	CDS
			locus_tag	Terrestrivirus_3_162
			product	hypothetical protein
			transl_table	11
195840	196631	CDS
			locus_tag	Terrestrivirus_3_163
			product	hypothetical protein
			transl_table	11
196729	198042	CDS
			locus_tag	Terrestrivirus_3_164
			product	tubulin-tyrosine ligase family protein
			transl_table	11
199162	198104	CDS
			locus_tag	Terrestrivirus_3_165
			product	hypothetical protein
			transl_table	11
199337	200047	CDS
			locus_tag	Terrestrivirus_3_166
			product	Ring finger and CHY zinc finger domain containing 1
			transl_table	11
200107	200289	CDS
			locus_tag	Terrestrivirus_3_167
			product	hypothetical protein
			transl_table	11
200395	201342	CDS
			locus_tag	Terrestrivirus_3_168
			product	RING finger domain protein
			transl_table	11
201470	202708	CDS
			locus_tag	Terrestrivirus_3_169
			product	signal peptide peptidase
			transl_table	11
203155	202754	CDS
			locus_tag	Terrestrivirus_3_170
			product	hypothetical protein Catovirus_1_665
			transl_table	11
204063	203290	CDS
			locus_tag	Terrestrivirus_3_171
			product	hypothetical protein
			transl_table	11
204286	204717	CDS
			locus_tag	Terrestrivirus_3_172
			product	hypothetical protein
			transl_table	11
204762	206975	CDS
			locus_tag	Terrestrivirus_3_173
			product	dehydrogenase/oxidoreductase
			transl_table	11
207142	207011	CDS
			locus_tag	Terrestrivirus_3_174
			product	hypothetical protein
			transl_table	11
208167	207445	CDS
			locus_tag	Terrestrivirus_3_175
			product	hypothetical protein
			transl_table	11
208309	209208	CDS
			locus_tag	Terrestrivirus_3_176
			product	serine/threonine protein kinase
			transl_table	11
209908	209300	CDS
			locus_tag	Terrestrivirus_3_177
			product	alkylated DNA repair protein
			transl_table	11
211230	210124	CDS
			locus_tag	Terrestrivirus_3_178
			product	ribonucleoside diphosphate reductase beta subunit
			transl_table	11
211360	211791	CDS
			locus_tag	Terrestrivirus_3_179
			product	hypothetical protein
			transl_table	11
211791	211931	CDS
			locus_tag	Terrestrivirus_3_180
			product	hypothetical protein
			transl_table	11
211976	214462	CDS
			locus_tag	Terrestrivirus_3_181
			product	ribonucleoside diphosphate reductase large subunit
			transl_table	11
214567	216876	CDS
			locus_tag	Terrestrivirus_3_182
			product	ankyrin repeat domain-containing protein
			transl_table	11
217711	216878	CDS
			locus_tag	Terrestrivirus_3_183
			product	hypothetical protein
			transl_table	11
218810	217809	CDS
			locus_tag	Terrestrivirus_3_184
			product	DNA polymerase family X protein
			transl_table	11
218946	219761	CDS
			locus_tag	Terrestrivirus_3_185
			product	metallophosphatase
			transl_table	11
219876	221219	CDS
			locus_tag	Terrestrivirus_3_186
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
222084	221419	CDS
			locus_tag	Terrestrivirus_3_187
			product	putative RNA methylase
			transl_table	11
223081	222215	CDS
			locus_tag	Terrestrivirus_3_188
			product	hypothetical protein Catovirus_2_228
			transl_table	11
223315	227238	CDS
			locus_tag	Terrestrivirus_3_189
			product	DNA polymerase family B elongation subunit
			transl_table	11
228185	227322	CDS
			locus_tag	Terrestrivirus_3_190
			product	hypothetical protein
			transl_table	11
228398	228919	CDS
			locus_tag	Terrestrivirus_3_191
			product	hypothetical protein Catovirus_2_225
			transl_table	11
229084	232461	CDS
			locus_tag	Terrestrivirus_3_192
			product	oxidoreductase FAD-binding
			transl_table	11
232544	238669	CDS
			locus_tag	Terrestrivirus_3_193
			product	early transcription factor VETF large subunit
			transl_table	11
238751	239587	CDS
			locus_tag	Terrestrivirus_3_194
			product	hypothetical protein
			transl_table	11
240600	239590	CDS
			locus_tag	Terrestrivirus_3_195
			product	hypothetical protein
			transl_table	11
240847	241524	CDS
			locus_tag	Terrestrivirus_3_196
			product	hypothetical protein Klosneuvirus_2_263
			transl_table	11
241605	242000	CDS
			locus_tag	Terrestrivirus_3_197
			product	hypothetical protein Catovirus_2_222
			transl_table	11
242061	242627	CDS
			locus_tag	Terrestrivirus_3_198
			product	hypothetical protein Catovirus_2_221
			transl_table	11
243458	242628	CDS
			locus_tag	Terrestrivirus_3_199
			product	hypothetical protein Indivirus_3_27
			transl_table	11
243652	244578	CDS
			locus_tag	Terrestrivirus_3_200
			product	hypothetical protein Catovirus_2_218
			transl_table	11
244652	245542	CDS
			locus_tag	Terrestrivirus_3_201
			product	hypothetical protein Klosneuvirus_2_258
			transl_table	11
246109	245534	CDS
			locus_tag	Terrestrivirus_3_202
			product	hypothetical protein
			transl_table	11
246312	248915	CDS
			locus_tag	Terrestrivirus_3_203
			product	cullin family protein
			transl_table	11
249079	251898	CDS
			locus_tag	Terrestrivirus_3_204
			product	hypothetical protein Catovirus_2_214
			transl_table	11
253656	251926	CDS
			locus_tag	Terrestrivirus_3_205
			product	hypothetical protein Klosneuvirus_2_254
			transl_table	11
254142	253924	CDS
			locus_tag	Terrestrivirus_3_206
			product	hypothetical protein
			transl_table	11
254575	254450	CDS
			locus_tag	Terrestrivirus_3_207
			product	hypothetical protein
			transl_table	11
255374	254718	CDS
			locus_tag	Terrestrivirus_3_208
			product	transcription elongation factor TFIIS
			transl_table	11
255552	257288	CDS
			locus_tag	Terrestrivirus_3_209
			product	hypothetical protein Catovirus_2_211
			transl_table	11
257328	257930	CDS
			locus_tag	Terrestrivirus_3_210
			product	hypothetical protein
			transl_table	11
257986	259524	CDS
			locus_tag	Terrestrivirus_3_211
			product	hypothetical protein BMW23_0667
			transl_table	11
259613	261205	CDS
			locus_tag	Terrestrivirus_3_212
			product	hypothetical protein
			transl_table	11
261253	262422	CDS
			locus_tag	Terrestrivirus_3_213
			product	hypothetical protein
			transl_table	11
262641	263585	CDS
			locus_tag	Terrestrivirus_3_214
			product	hypothetical protein Klosneuvirus_2_228
			transl_table	11
263607	264677	CDS
			locus_tag	Terrestrivirus_3_215
			product	hypothetical protein
			transl_table	11
264825	265793	CDS
			locus_tag	Terrestrivirus_3_216
			product	hypothetical protein
			transl_table	11
265921	267075	CDS
			locus_tag	Terrestrivirus_3_217
			product	hypothetical protein
			transl_table	11
267461	267072	CDS
			locus_tag	Terrestrivirus_3_218
			product	hypothetical protein
			transl_table	11
268820	267489	CDS
			locus_tag	Terrestrivirus_3_219
			product	hypothetical protein Catovirus_2_204
			transl_table	11
269011	271287	CDS
			locus_tag	Terrestrivirus_3_220
			product	putative minor capsid protein
			transl_table	11
271809	271922	CDS
			locus_tag	Terrestrivirus_3_221
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_4
1	993	CDS
			locus_tag	Terrestrivirus_4_1
			product	hypothetical protein Catovirus_1_929
			transl_table	11
1053	1277	CDS
			locus_tag	Terrestrivirus_4_2
			product	hypothetical protein
			transl_table	11
1795	1496	CDS
			locus_tag	Terrestrivirus_4_3
			product	UvrD-family helicase
			transl_table	11
3675	2005	CDS
			locus_tag	Terrestrivirus_4_4
			product	UvrD-family helicase
			transl_table	11
4725	3751	CDS
			locus_tag	Terrestrivirus_4_5
			product	DEAD/SNF2-like helicase
			transl_table	11
5768	4719	CDS
			locus_tag	Terrestrivirus_4_6
			product	DEAD/SNF2-like helicase
			transl_table	11
7007	5916	CDS
			locus_tag	Terrestrivirus_4_7
			product	hypothetical protein Indivirus_1_153
			transl_table	11
7792	7112	CDS
			locus_tag	Terrestrivirus_4_8
			product	hypothetical protein
			transl_table	11
9801	7888	CDS
			locus_tag	Terrestrivirus_4_9
			product	eukaryotic translation initiation factor 5B
			transl_table	11
10567	9905	CDS
			locus_tag	Terrestrivirus_4_10
			product	hypothetical protein
			transl_table	11
11052	10738	CDS
			locus_tag	Terrestrivirus_4_11
			product	hypothetical protein
			transl_table	11
11279	11719	CDS
			locus_tag	Terrestrivirus_4_12
			product	hypothetical protein
			transl_table	11
13602	11716	CDS
			locus_tag	Terrestrivirus_4_13
			product	SNF2-like helicase
			transl_table	11
14871	13675	CDS
			locus_tag	Terrestrivirus_4_14
			product	hypothetical protein
			transl_table	11
15901	14912	CDS
			locus_tag	Terrestrivirus_4_15
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
16680	16057	CDS
			locus_tag	Terrestrivirus_4_16
			product	hypothetical protein Catovirus_1_948
			transl_table	11
16828	17145	CDS
			locus_tag	Terrestrivirus_4_17
			product	hypothetical protein Catovirus_1_944
			transl_table	11
17228	17923	CDS
			locus_tag	Terrestrivirus_4_18
			product	peptide chain release factor 1 eRF1
			transl_table	11
18179	17961	CDS
			locus_tag	Terrestrivirus_4_19
			product	hypothetical protein
			transl_table	11
18536	18282	CDS
			locus_tag	Terrestrivirus_4_20
			product	hypothetical protein
			transl_table	11
19803	18616	CDS
			locus_tag	Terrestrivirus_4_21
			product	metallopeptidase family M24
			transl_table	11
21418	19889	CDS
			locus_tag	Terrestrivirus_4_22
			product	glutamyl-tRNA synthetase
			transl_table	11
21754	23328	CDS
			locus_tag	Terrestrivirus_4_23
			product	DUF229 domain-containing protein
			transl_table	11
24138	23380	CDS
			locus_tag	Terrestrivirus_4_24
			product	Clp protease
			transl_table	11
25887	24292	CDS
			locus_tag	Terrestrivirus_4_25
			product	ATP-dependent Lon protease
			transl_table	11
26050	27027	CDS
			locus_tag	Terrestrivirus_4_26
			product	DNA topoisomerase 1b
			transl_table	11
27458	27024	CDS
			locus_tag	Terrestrivirus_4_27
			product	hypothetical protein
			transl_table	11
27626	30205	CDS
			locus_tag	Terrestrivirus_4_28
			product	hypothetical protein M436DRAFT_42072
			transl_table	11
30650	30231	CDS
			locus_tag	Terrestrivirus_4_29
			product	hypothetical protein
			transl_table	11
30770	31465	CDS
			locus_tag	Terrestrivirus_4_30
			product	aspartyl/asparaginyl beta-hydroxylase
			transl_table	11
31748	31485	CDS
			locus_tag	Terrestrivirus_4_31
			product	hypothetical protein
			transl_table	11
33370	31907	CDS
			locus_tag	Terrestrivirus_4_32
			product	AAA family ATPase
			transl_table	11
33593	35656	CDS
			locus_tag	Terrestrivirus_4_33
			product	NAD-dependent DNA ligase
			transl_table	11
36113	35670	CDS
			locus_tag	Terrestrivirus_4_34
			product	translation initiation factor 2 subunit beta
			transl_table	11
37349	36219	CDS
			locus_tag	Terrestrivirus_4_35
			product	acetoin utilization deacetylase
			transl_table	11
37785	37426	CDS
			locus_tag	Terrestrivirus_4_36
			product	hypothetical protein
			transl_table	11
38362	37955	CDS
			locus_tag	Terrestrivirus_4_37
			product	hypothetical protein
			transl_table	11
39217	38462	CDS
			locus_tag	Terrestrivirus_4_38
			product	hypothetical protein
			transl_table	11
41923	39350	CDS
			locus_tag	Terrestrivirus_4_39
			product	hypothetical protein Catovirus_1_954
			transl_table	11
43211	42084	CDS
			locus_tag	Terrestrivirus_4_40
			product	hypothetical protein
			transl_table	11
43613	43275	CDS
			locus_tag	Terrestrivirus_4_41
			product	hypothetical protein
			transl_table	11
44512	43694	CDS
			locus_tag	Terrestrivirus_4_42
			product	hypothetical protein
			transl_table	11
44672	45049	CDS
			locus_tag	Terrestrivirus_4_43
			product	hypothetical protein
			transl_table	11
45759	45103	CDS
			locus_tag	Terrestrivirus_4_44
			product	hypothetical protein
			transl_table	11
46461	45814	CDS
			locus_tag	Terrestrivirus_4_45
			product	hypothetical protein
			transl_table	11
48320	46866	CDS
			locus_tag	Terrestrivirus_4_46
			product	stage V sporulation protein K
			transl_table	11
48475	50160	CDS
			locus_tag	Terrestrivirus_4_47
			product	hypothetical protein
			transl_table	11
51449	50157	CDS
			locus_tag	Terrestrivirus_4_48
			product	hypothetical protein Klosneuvirus_1_256
			transl_table	11
51538	51888	CDS
			locus_tag	Terrestrivirus_4_49
			product	hypothetical protein
			transl_table	11
51885	52010	CDS
			locus_tag	Terrestrivirus_4_50
			product	hypothetical protein
			transl_table	11
51974	52456	CDS
			locus_tag	Terrestrivirus_4_51
			product	hypothetical protein MegaChil _gp0300
			transl_table	11
52565	54253	CDS
			locus_tag	Terrestrivirus_4_52
			product	serine/threonine protein kinase
			transl_table	11
54804	54424	CDS
			locus_tag	Terrestrivirus_4_53
			product	hypothetical protein
			transl_table	11
54958	55554	CDS
			locus_tag	Terrestrivirus_4_54
			product	hypothetical protein
			transl_table	11
58102	55559	CDS
			locus_tag	Terrestrivirus_4_55
			product	ADP-ribosyltransferase exoenzyme domain protein
			transl_table	11
58675	58145	CDS
			locus_tag	Terrestrivirus_4_56
			product	hypothetical protein
			transl_table	11
60009	58768	CDS
			locus_tag	Terrestrivirus_4_57
			product	hypothetical protein CML47_07175
			transl_table	11
60351	60103	CDS
			locus_tag	Terrestrivirus_4_58
			product	hypothetical protein
			transl_table	11
64214	60516	CDS
			locus_tag	Terrestrivirus_4_59
			product	D5-like helicase-primase
			transl_table	11
64444	64839	CDS
			locus_tag	Terrestrivirus_4_60
			product	zinc finger A20 and AN1 domain-containing stress-associated protein 6-like
			transl_table	11
66568	64832	CDS
			locus_tag	Terrestrivirus_4_61
			product	hypothetical protein BB560_006828
			transl_table	11
67644	66745	CDS
			locus_tag	Terrestrivirus_4_62
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
68234	67737	CDS
			locus_tag	Terrestrivirus_4_63
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
71109	68329	CDS
			locus_tag	Terrestrivirus_4_64
			product	DNA topoisomerase IA
			transl_table	11
73101	71311	CDS
			locus_tag	Terrestrivirus_4_65
			product	serine/threonine protein kinase
			transl_table	11
74537	73167	CDS
			locus_tag	Terrestrivirus_4_66
			product	Processing protease
			transl_table	11
74705	75484	CDS
			locus_tag	Terrestrivirus_4_67
			product	DNA-directed RNA polymerase subunit 5
			transl_table	11
78462	75496	CDS
			locus_tag	Terrestrivirus_4_68
			product	hypothetical protein Indivirus_1_109
			transl_table	11
78597	79844	CDS
			locus_tag	Terrestrivirus_4_69
			product	hypothetical protein Klosneuvirus_1_232
			transl_table	11
79905	80519	CDS
			locus_tag	Terrestrivirus_4_70
			product	hypothetical protein
			transl_table	11
81036	80521	CDS
			locus_tag	Terrestrivirus_4_71
			product	hypothetical protein Klosneuvirus_1_112
			transl_table	11
81649	81017	CDS
			locus_tag	Terrestrivirus_4_72
			product	hypothetical protein
			transl_table	11
82453	81794	CDS
			locus_tag	Terrestrivirus_4_73
			product	hypothetical protein
			transl_table	11
82551	84368	CDS
			locus_tag	Terrestrivirus_4_74
			product	PREDICTED: glycylpeptide N-tetradecanoyltransferase 2
			transl_table	11
85104	84370	CDS
			locus_tag	Terrestrivirus_4_75
			product	Ras-related protein Rab-19
			transl_table	11
85261	86094	CDS
			locus_tag	Terrestrivirus_4_76
			product	elongation factor Ts
			transl_table	11
86307	87008	CDS
			locus_tag	Terrestrivirus_4_77
			product	hypothetical protein
			transl_table	11
88067	86967	CDS
			locus_tag	Terrestrivirus_4_78
			product	glycosyl transferase
			transl_table	11
88694	88128	CDS
			locus_tag	Terrestrivirus_4_79
			product	hypothetical protein
			transl_table	11
89211	88768	CDS
			locus_tag	Terrestrivirus_4_80
			product	hypothetical protein
			transl_table	11
89873	89445	CDS
			locus_tag	Terrestrivirus_4_81
			product	hypothetical protein
			transl_table	11
91499	89976	CDS
			locus_tag	Terrestrivirus_4_82
			product	phage antirepressor protein
			transl_table	11
94299	91669	CDS
			locus_tag	Terrestrivirus_4_83
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
95323	94649	CDS
			locus_tag	Terrestrivirus_4_84
			product	putative nuclease
			transl_table	11
95630	95466	CDS
			locus_tag	Terrestrivirus_4_85
			product	hypothetical protein Catovirus_1_1036
			transl_table	11
98954	96045	CDS
			locus_tag	Terrestrivirus_4_86
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
99983	99081	CDS
			locus_tag	Terrestrivirus_4_87
			product	hypothetical protein
			transl_table	11
100256	102256	CDS
			locus_tag	Terrestrivirus_4_88
			product	hypothetical protein
			transl_table	11
103068	102295	CDS
			locus_tag	Terrestrivirus_4_89
			product	uracil-DNA glycosylase
			transl_table	11
103235	104719	CDS
			locus_tag	Terrestrivirus_4_90
			product	transcription initiation factor IIB
			transl_table	11
104725	104823	CDS
			locus_tag	Terrestrivirus_4_91
			product	hypothetical protein
			transl_table	11
104849	105538	CDS
			locus_tag	Terrestrivirus_4_92
			product	hypothetical protein
			transl_table	11
106023	105535	CDS
			locus_tag	Terrestrivirus_4_93
			product	hypothetical protein
			transl_table	11
109346	106104	CDS
			locus_tag	Terrestrivirus_4_94
			product	ATP-dependent Lon protease
			transl_table	11
109620	110477	CDS
			locus_tag	Terrestrivirus_4_95
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
110570	112306	CDS
			locus_tag	Terrestrivirus_4_96
			product	hypothetical protein Catovirus_1_1049
			transl_table	11
112377	112607	CDS
			locus_tag	Terrestrivirus_4_97
			product	hypothetical protein
			transl_table	11
113261	112602	CDS
			locus_tag	Terrestrivirus_4_98
			product	hypothetical protein
			transl_table	11
113425	114639	CDS
			locus_tag	Terrestrivirus_4_99
			product	hypothetical protein
			transl_table	11
117874	114671	CDS
			locus_tag	Terrestrivirus_4_100
			product	Hsp70 protein
			transl_table	11
118380	117997	CDS
			locus_tag	Terrestrivirus_4_101
			product	hypothetical protein
			transl_table	11
119875	118475	CDS
			locus_tag	Terrestrivirus_4_102
			product	hypothetical protein Catovirus_1_1053
			transl_table	11
119985	119887	CDS
			locus_tag	Terrestrivirus_4_103
			product	hypothetical protein
			transl_table	11
119963	121138	CDS
			locus_tag	Terrestrivirus_4_104
			product	DnaJ domain protein
			transl_table	11
121192	122661	CDS
			locus_tag	Terrestrivirus_4_105
			product	hypothetical protein crov314
			transl_table	11
122765	123904	CDS
			locus_tag	Terrestrivirus_4_106
			product	PREDICTED: flap endonuclease 1-like
			transl_table	11
124449	123955	CDS
			locus_tag	Terrestrivirus_4_107
			product	hypothetical protein
			transl_table	11
124639	125955	CDS
			locus_tag	Terrestrivirus_4_108
			product	hypothetical protein EIN_359140
			transl_table	11
126375	126010	CDS
			locus_tag	Terrestrivirus_4_109
			product	hypothetical protein
			transl_table	11
126542	127078	CDS
			locus_tag	Terrestrivirus_4_110
			product	hypothetical protein
			transl_table	11
127230	127667	CDS
			locus_tag	Terrestrivirus_4_111
			product	translation initiation factor eIF-5A family protein
			transl_table	11
129779	127689	CDS
			locus_tag	Terrestrivirus_4_112
			product	hypothetical protein
			transl_table	11
131258	129912	CDS
			locus_tag	Terrestrivirus_4_113
			product	hypothetical protein Klosneuvirus_2_14
			transl_table	11
131725	131540	CDS
			locus_tag	Terrestrivirus_4_114
			product	hypothetical protein
			transl_table	11
133519	133010	CDS
			locus_tag	Terrestrivirus_4_115
			product	hypothetical protein
			transl_table	11
133716	135611	CDS
			locus_tag	Terrestrivirus_4_116
			product	hypothetical protein SPPG_02343
			transl_table	11
136161	135676	CDS
			locus_tag	Terrestrivirus_4_117
			product	hypothetical protein
			transl_table	11
138572	136299	CDS
			locus_tag	Terrestrivirus_4_118
			product	hsp82-like protein
			transl_table	11
138720	140057	CDS
			locus_tag	Terrestrivirus_4_119
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
140142	141011	CDS
			locus_tag	Terrestrivirus_4_120
			product	hypothetical protein
			transl_table	11
141230	141027	CDS
			locus_tag	Terrestrivirus_4_121
			product	hypothetical protein
			transl_table	11
141924	141358	CDS
			locus_tag	Terrestrivirus_4_122
			product	hypothetical protein
			transl_table	11
142344	142126	CDS
			locus_tag	Terrestrivirus_4_123
			product	hypothetical protein
			transl_table	11
143051	142470	CDS
			locus_tag	Terrestrivirus_4_124
			product	hypothetical protein BCR33DRAFT_711812
			transl_table	11
143516	143157	CDS
			locus_tag	Terrestrivirus_4_125
			product	hypothetical protein
			transl_table	11
144302	143592	CDS
			locus_tag	Terrestrivirus_4_126
			product	hypothetical protein
			transl_table	11
144478	144900	CDS
			locus_tag	Terrestrivirus_4_127
			product	hypothetical protein
			transl_table	11
145072	147174	CDS
			locus_tag	Terrestrivirus_4_128
			product	phosphoinositide 3-kinase
			transl_table	11
147269	148447	CDS
			locus_tag	Terrestrivirus_4_129
			product	hypothetical protein mc_173
			transl_table	11
148552	150687	CDS
			locus_tag	Terrestrivirus_4_130
			product	AAA family ATPase
			transl_table	11
150776	151696	CDS
			locus_tag	Terrestrivirus_4_131
			product	hypothetical protein
			transl_table	11
152203	151727	CDS
			locus_tag	Terrestrivirus_4_132
			product	hypothetical protein
			transl_table	11
152327	152956	CDS
			locus_tag	Terrestrivirus_4_133
			product	putative glutamineamido transferase-like protein
			transl_table	11
153178	152960	CDS
			locus_tag	Terrestrivirus_4_134
			product	hypothetical protein
			transl_table	11
153608	153321	CDS
			locus_tag	Terrestrivirus_4_135
			product	hypothetical protein
			transl_table	11
153726	155039	CDS
			locus_tag	Terrestrivirus_4_136
			product	hypothetical protein
			transl_table	11
156308	155034	CDS
			locus_tag	Terrestrivirus_4_137
			product	hypothetical protein
			transl_table	11
157001	156348	CDS
			locus_tag	Terrestrivirus_4_138
			product	cathepsin
			transl_table	11
157159	157746	CDS
			locus_tag	Terrestrivirus_4_139
			product	hypothetical protein Catovirus_1_413
			transl_table	11
158381	157752	CDS
			locus_tag	Terrestrivirus_4_140
			product	hypothetical protein
			transl_table	11
158623	159681	CDS
			locus_tag	Terrestrivirus_4_141
			product	hypothetical protein
			transl_table	11
160044	159715	CDS
			locus_tag	Terrestrivirus_4_142
			product	hypothetical protein
			transl_table	11
160860	162683	CDS
			locus_tag	Terrestrivirus_4_143
			product	putative ADP-ribosylglycohydrolase
			transl_table	11
164007	162694	CDS
			locus_tag	Terrestrivirus_4_144
			product	hypothetical protein AK812_SmicGene32768
			transl_table	11
164484	164128	CDS
			locus_tag	Terrestrivirus_4_145
			product	hypothetical protein
			transl_table	11
166547	164586	CDS
			locus_tag	Terrestrivirus_4_146
			product	T9SS C-terminal target domain-containing protein
			transl_table	11
168604	166655	CDS
			locus_tag	Terrestrivirus_4_147
			product	transmembrane protein, putative
			transl_table	11
170659	168692	CDS
			locus_tag	Terrestrivirus_4_148
			product	hypothetical protein
			transl_table	11
172940	170766	CDS
			locus_tag	Terrestrivirus_4_149
			product	hypothetical protein C5B51_20200, partial
			transl_table	11
173110	173343	CDS
			locus_tag	Terrestrivirus_4_150
			product	hypothetical protein
			transl_table	11
173461	174651	CDS
			locus_tag	Terrestrivirus_4_151
			product	hypothetical protein EMIHUDRAFT_470197
			transl_table	11
174812	175513	CDS
			locus_tag	Terrestrivirus_4_152
			product	hypothetical protein
			transl_table	11
175691	175921	CDS
			locus_tag	Terrestrivirus_4_153
			product	hypothetical protein
			transl_table	11
176477	176001	CDS
			locus_tag	Terrestrivirus_4_154
			product	hypothetical protein
			transl_table	11
176646	176993	CDS
			locus_tag	Terrestrivirus_4_155
			product	hypothetical protein
			transl_table	11
177118	177846	CDS
			locus_tag	Terrestrivirus_4_156
			product	hypothetical protein
			transl_table	11
178220	177900	CDS
			locus_tag	Terrestrivirus_4_157
			product	hypothetical protein
			transl_table	11
179076	178372	CDS
			locus_tag	Terrestrivirus_4_158
			product	hypothetical protein
			transl_table	11
179248	180186	CDS
			locus_tag	Terrestrivirus_4_159
			product	MBL fold metallo-hydrolase
			transl_table	11
180714	180199	CDS
			locus_tag	Terrestrivirus_4_160
			product	hypothetical protein
			transl_table	11
181840	180803	CDS
			locus_tag	Terrestrivirus_4_161
			product	hypothetical protein Hokovirus_3_254
			transl_table	11
182213	183256	CDS
			locus_tag	Terrestrivirus_4_162
			product	hypothetical protein CBC91_00715
			transl_table	11
184552	183272	CDS
			locus_tag	Terrestrivirus_4_163
			product	hypothetical protein
			transl_table	11
184534	184653	CDS
			locus_tag	Terrestrivirus_4_164
			product	hypothetical protein
			transl_table	11
185804	184662	CDS
			locus_tag	Terrestrivirus_4_165
			product	HNH endonuclease
			transl_table	11
186163	187155	CDS
			locus_tag	Terrestrivirus_4_166
			product	hypothetical protein 162300064
			transl_table	11
189129	187156	CDS
			locus_tag	Terrestrivirus_4_167
			product	DUF262 domain-containing protein
			transl_table	11
189327	189521	CDS
			locus_tag	Terrestrivirus_4_168
			product	hypothetical protein
			transl_table	11
189604	190329	CDS
			locus_tag	Terrestrivirus_4_169
			product	hypothetical protein
			transl_table	11
190953	190339	CDS
			locus_tag	Terrestrivirus_4_170
			product	hypothetical protein
			transl_table	11
191101	191409	CDS
			locus_tag	Terrestrivirus_4_171
			product	hypothetical protein
			transl_table	11
191524	192018	CDS
			locus_tag	Terrestrivirus_4_172
			product	hypothetical protein
			transl_table	11
192856	192080	CDS
			locus_tag	Terrestrivirus_4_173
			product	hypothetical protein
			transl_table	11
193061	193558	CDS
			locus_tag	Terrestrivirus_4_174
			product	hypothetical protein
			transl_table	11
193995	193594	CDS
			locus_tag	Terrestrivirus_4_175
			product	hypothetical protein
			transl_table	11
194402	194049	CDS
			locus_tag	Terrestrivirus_4_176
			product	hypothetical protein
			transl_table	11
194691	194449	CDS
			locus_tag	Terrestrivirus_4_177
			product	hypothetical protein
			transl_table	11
195141	194779	CDS
			locus_tag	Terrestrivirus_4_178
			product	hypothetical protein
			transl_table	11
195345	195929	CDS
			locus_tag	Terrestrivirus_4_179
			product	hypothetical protein
			transl_table	11
196037	197008	CDS
			locus_tag	Terrestrivirus_4_180
			product	hypothetical protein
			transl_table	11
197126	198901	CDS
			locus_tag	Terrestrivirus_4_181
			product	hypothetical protein
			transl_table	11
199021	199830	CDS
			locus_tag	Terrestrivirus_4_182
			product	hypothetical protein
			transl_table	11
199982	200821	CDS
			locus_tag	Terrestrivirus_4_183
			product	hypothetical protein
			transl_table	11
200934	201827	CDS
			locus_tag	Terrestrivirus_4_184
			product	hypothetical protein SPPG_09384
			transl_table	11
202152	201799	CDS
			locus_tag	Terrestrivirus_4_185
			product	hypothetical protein LAU_0057
			transl_table	11
203098	202265	CDS
			locus_tag	Terrestrivirus_4_186
			product	hypothetical protein
			transl_table	11
203374	203547	CDS
			locus_tag	Terrestrivirus_4_187
			product	hypothetical protein
			transl_table	11
207317	203556	CDS
			locus_tag	Terrestrivirus_4_188
			product	hypothetical protein
			transl_table	11
207450	208715	CDS
			locus_tag	Terrestrivirus_4_189
			product	hypothetical protein
			transl_table	11
208821	209735	CDS
			locus_tag	Terrestrivirus_4_190
			product	hypothetical protein
			transl_table	11
209824	210195	CDS
			locus_tag	Terrestrivirus_4_191
			product	hypothetical protein
			transl_table	11
210249	211607	CDS
			locus_tag	Terrestrivirus_4_192
			product	hypothetical protein DICPUDRAFT_80790
			transl_table	11
211941	212711	CDS
			locus_tag	Terrestrivirus_4_193
			product	hypothetical protein
			transl_table	11
213224	212727	CDS
			locus_tag	Terrestrivirus_4_194
			product	hypothetical protein
			transl_table	11
213857	213333	CDS
			locus_tag	Terrestrivirus_4_195
			product	DEXDc helicase
			transl_table	11
215557	213914	CDS
			locus_tag	Terrestrivirus_4_196
			product	putative lipoxygenase
			transl_table	11
216086	215625	CDS
			locus_tag	Terrestrivirus_4_197
			product	hypothetical protein
			transl_table	11
216966	216148	CDS
			locus_tag	Terrestrivirus_4_198
			product	capsid protein 1
			transl_table	11
218055	217123	CDS
			locus_tag	Terrestrivirus_4_199
			product	hypothetical protein Catovirus_1_1055
			transl_table	11
218241	221408	CDS
			locus_tag	Terrestrivirus_4_200
			product	DEAD/SNF2-like helicase
			transl_table	11
222465	221293	CDS
			locus_tag	Terrestrivirus_4_201
			product	patatin-like phospholipase
			transl_table	11
222641	223675	CDS
			locus_tag	Terrestrivirus_4_202
			product	hypothetical protein Catovirus_1_1058
			transl_table	11
223783	224073	CDS
			locus_tag	Terrestrivirus_4_203
			product	hypothetical protein
			transl_table	11
225294	224299	CDS
			locus_tag	Terrestrivirus_4_204
			product	hypothetical protein
			transl_table	11
225408	226688	CDS
			locus_tag	Terrestrivirus_4_205
			product	hypothetical protein Hokovirus_2_225
			transl_table	11
227913	226786	CDS
			locus_tag	Terrestrivirus_4_206
			product	hypothetical protein Klosneuvirus_1_204
			transl_table	11
228141	228920	CDS
			locus_tag	Terrestrivirus_4_207
			product	hypothetical protein Indivirus_1_80
			transl_table	11
229953	228964	CDS
			locus_tag	Terrestrivirus_4_208
			product	hypothetical protein
			transl_table	11
230833	230090	CDS
			locus_tag	Terrestrivirus_4_209
			product	hypothetical protein
			transl_table	11
235741	230954	CDS
			locus_tag	Terrestrivirus_4_210
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
236882	235947	CDS
			locus_tag	Terrestrivirus_4_211
			product	GIY-YIG-like endonuclease
			transl_table	11
237588	237124	CDS
			locus_tag	Terrestrivirus_4_212
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
239093	238053	CDS
			locus_tag	Terrestrivirus_4_213
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
240002	239376	CDS
			locus_tag	Terrestrivirus_4_214
			product	hypothetical protein
			transl_table	11
240658	240125	CDS
			locus_tag	Terrestrivirus_4_215
			product	hypothetical protein
			transl_table	11
241068	240742	CDS
			locus_tag	Terrestrivirus_4_216
			product	hypothetical protein
			transl_table	11
241181	242242	CDS
			locus_tag	Terrestrivirus_4_217
			product	replication factor C small subunit
			transl_table	11
242295	243587	CDS
			locus_tag	Terrestrivirus_4_218
			product	translation initiation factor 4E
			transl_table	11
243836	244132	CDS
			locus_tag	Terrestrivirus_4_219
			product	hypothetical protein
			transl_table	11
246997	244256	CDS
			locus_tag	Terrestrivirus_4_220
			product	hypothetical protein Catovirus_1_69
			transl_table	11
248346	247138	CDS
			locus_tag	Terrestrivirus_4_221
			product	proliferating cell nuclear antigen
			transl_table	11
248494	249078	CDS
			locus_tag	Terrestrivirus_4_222
			product	hypothetical protein Klosneuvirus_1_190
			transl_table	11
249844	249146	CDS
			locus_tag	Terrestrivirus_4_223
			product	hypothetical protein
			transl_table	11
250206	250114	CDS
			locus_tag	Terrestrivirus_4_224
			product	hypothetical protein
			transl_table	11
250489	250253	CDS
			locus_tag	Terrestrivirus_4_225
			product	hypothetical protein
			transl_table	11
251437	250580	CDS
			locus_tag	Terrestrivirus_4_226
			product	hypothetical protein Catovirus_1_1084
			transl_table	11
>Terrestrivirus_5
915	67	CDS
			locus_tag	Terrestrivirus_5_1
			product	hypothetical protein
			transl_table	11
2225	1185	CDS
			locus_tag	Terrestrivirus_5_2
			product	PREDICTED: uncharacterized protein LOC107337226
			transl_table	11
2390	3415	CDS
			locus_tag	Terrestrivirus_5_3
			product	hypothetical protein AUK02_01385
			transl_table	11
3553	4353	CDS
			locus_tag	Terrestrivirus_5_4
			product	hypothetical protein
			transl_table	11
4472	5086	CDS
			locus_tag	Terrestrivirus_5_5
			product	hypothetical protein
			transl_table	11
5923	5147	CDS
			locus_tag	Terrestrivirus_5_6
			product	hypothetical protein
			transl_table	11
6120	6416	CDS
			locus_tag	Terrestrivirus_5_7
			product	hypothetical protein
			transl_table	11
6727	6431	CDS
			locus_tag	Terrestrivirus_5_8
			product	hypothetical protein
			transl_table	11
7520	7047	CDS
			locus_tag	Terrestrivirus_5_9
			product	hypothetical protein
			transl_table	11
9982	7604	CDS
			locus_tag	Terrestrivirus_5_10
			product	hypothetical protein RhiirA5_500298
			transl_table	11
10240	10920	CDS
			locus_tag	Terrestrivirus_5_11
			product	hypothetical protein
			transl_table	11
11030	11245	CDS
			locus_tag	Terrestrivirus_5_12
			product	hypothetical protein
			transl_table	11
11495	11875	CDS
			locus_tag	Terrestrivirus_5_13
			product	hypothetical protein
			transl_table	11
11942	12079	CDS
			locus_tag	Terrestrivirus_5_14
			product	hypothetical protein
			transl_table	11
12246	15359	CDS
			locus_tag	Terrestrivirus_5_15
			product	DEAD/SNF2-like helicase
			transl_table	11
19393	15383	CDS
			locus_tag	Terrestrivirus_5_16
			product	DEAD/SNF2-like helicase
			transl_table	11
20877	19468	CDS
			locus_tag	Terrestrivirus_5_17
			product	hypothetical protein
			transl_table	11
23409	21064	CDS
			locus_tag	Terrestrivirus_5_18
			product	hypothetical protein
			transl_table	11
23653	23985	CDS
			locus_tag	Terrestrivirus_5_19
			product	hypothetical protein
			transl_table	11
25443	24019	CDS
			locus_tag	Terrestrivirus_5_20
			product	deoxyribodipyrimidine photo-lyase
			transl_table	11
25963	25427	CDS
			locus_tag	Terrestrivirus_5_21
			product	hypothetical protein
			transl_table	11
26964	26038	CDS
			locus_tag	Terrestrivirus_5_22
			product	hypothetical protein
			transl_table	11
27175	27804	CDS
			locus_tag	Terrestrivirus_5_23
			product	hypothetical protein
			transl_table	11
28264	27812	CDS
			locus_tag	Terrestrivirus_5_24
			product	hypothetical protein
			transl_table	11
29577	28357	CDS
			locus_tag	Terrestrivirus_5_25
			product	major capsid protein
			transl_table	11
30714	29650	CDS
			locus_tag	Terrestrivirus_5_26
			product	major capsid protein VP54
			transl_table	11
31654	30791	CDS
			locus_tag	Terrestrivirus_5_27
			product	hypothetical protein
			transl_table	11
32903	31803	CDS
			locus_tag	Terrestrivirus_5_28
			product	hypothetical protein
			transl_table	11
33135	33959	CDS
			locus_tag	Terrestrivirus_5_29
			product	hypothetical protein DICPUDRAFT_43149
			transl_table	11
34067	34807	CDS
			locus_tag	Terrestrivirus_5_30
			product	hypothetical protein ABL78_5728
			transl_table	11
35569	34874	CDS
			locus_tag	Terrestrivirus_5_31
			product	hypothetical protein
			transl_table	11
36380	35664	CDS
			locus_tag	Terrestrivirus_5_32
			product	hypothetical protein
			transl_table	11
36835	36614	CDS
			locus_tag	Terrestrivirus_5_33
			product	hypothetical protein
			transl_table	11
38349	37579	CDS
			locus_tag	Terrestrivirus_5_34
			product	hypothetical protein
			transl_table	11
38519	41566	CDS
			locus_tag	Terrestrivirus_5_35
			product	hypothetical protein
			transl_table	11
42994	41591	CDS
			locus_tag	Terrestrivirus_5_36
			product	hypothetical protein GSI_12838
			transl_table	11
44196	43132	CDS
			locus_tag	Terrestrivirus_5_37
			product	hypothetical protein PTSG_06648
			transl_table	11
45797	44274	CDS
			locus_tag	Terrestrivirus_5_38
			product	dehydrogenase
			transl_table	11
45975	46901	CDS
			locus_tag	Terrestrivirus_5_39
			product	DUF3050 domain-containing protein
			transl_table	11
46975	48654	CDS
			locus_tag	Terrestrivirus_5_40
			product	hypothetical protein
			transl_table	11
51439	48680	CDS
			locus_tag	Terrestrivirus_5_41
			product	nuclease
			transl_table	11
51552	51737	CDS
			locus_tag	Terrestrivirus_5_42
			product	hypothetical protein
			transl_table	11
51850	53052	CDS
			locus_tag	Terrestrivirus_5_43
			product	hypothetical protein
			transl_table	11
53981	53070	CDS
			locus_tag	Terrestrivirus_5_44
			product	hypothetical protein
			transl_table	11
54216	55079	CDS
			locus_tag	Terrestrivirus_5_45
			product	hypothetical protein
			transl_table	11
56029	55091	CDS
			locus_tag	Terrestrivirus_5_46
			product	DUF1868 domain-containing protein
			transl_table	11
56179	56544	CDS
			locus_tag	Terrestrivirus_5_47
			product	hypothetical protein
			transl_table	11
57640	56546	CDS
			locus_tag	Terrestrivirus_5_48
			product	GIY-YIG nuclease and ricin-type beta-trefoil lectin domain protein
			transl_table	11
58330	57749	CDS
			locus_tag	Terrestrivirus_5_49
			product	PREDICTED: NADPH:quinone oxidoreductase-like
			transl_table	11
59175	58393	CDS
			locus_tag	Terrestrivirus_5_50
			product	hypothetical protein
			transl_table	11
59353	60753	CDS
			locus_tag	Terrestrivirus_5_51
			product	hypothetical protein
			transl_table	11
60857	61741	CDS
			locus_tag	Terrestrivirus_5_52
			product	hypothetical protein
			transl_table	11
62379	61798	CDS
			locus_tag	Terrestrivirus_5_53
			product	ras-domain-containing protein
			transl_table	11
63183	62491	CDS
			locus_tag	Terrestrivirus_5_54
			product	hypothetical protein
			transl_table	11
63381	63890	CDS
			locus_tag	Terrestrivirus_5_55
			product	hypothetical protein
			transl_table	11
63944	65419	CDS
			locus_tag	Terrestrivirus_5_56
			product	P-loop NTPase domain protein
			transl_table	11
65528	66529	CDS
			locus_tag	Terrestrivirus_5_57
			product	translation elongation factor EF-1 subunit alpha
			transl_table	11
68060	66552	CDS
			locus_tag	Terrestrivirus_5_58
			product	hypothetical protein
			transl_table	11
68213	68443	CDS
			locus_tag	Terrestrivirus_5_59
			product	hypothetical protein
			transl_table	11
68568	68798	CDS
			locus_tag	Terrestrivirus_5_60
			product	hypothetical protein
			transl_table	11
68901	70022	CDS
			locus_tag	Terrestrivirus_5_61
			product	hypothetical protein, variant 1
			transl_table	11
70891	70073	CDS
			locus_tag	Terrestrivirus_5_62
			product	hypothetical protein
			transl_table	11
71152	72183	CDS
			locus_tag	Terrestrivirus_5_63
			product	hypothetical protein
			transl_table	11
72968	72225	CDS
			locus_tag	Terrestrivirus_5_64
			product	hypothetical protein
			transl_table	11
73123	73530	CDS
			locus_tag	Terrestrivirus_5_65
			product	ASCH domain protein
			transl_table	11
73876	73580	CDS
			locus_tag	Terrestrivirus_5_66
			product	hypothetical protein
			transl_table	11
73989	74363	CDS
			locus_tag	Terrestrivirus_5_67
			product	hypothetical protein
			transl_table	11
75288	74368	CDS
			locus_tag	Terrestrivirus_5_68
			product	hypothetical protein
			transl_table	11
77018	75339	CDS
			locus_tag	Terrestrivirus_5_69
			product	xaa-Pro dipeptidase-like
			transl_table	11
77609	77097	CDS
			locus_tag	Terrestrivirus_5_70
			product	hypothetical protein
			transl_table	11
78856	77705	CDS
			locus_tag	Terrestrivirus_5_71
			product	hypothetical protein
			transl_table	11
79082	79762	CDS
			locus_tag	Terrestrivirus_5_72
			product	hypothetical protein
			transl_table	11
80291	79797	CDS
			locus_tag	Terrestrivirus_5_73
			product	hypothetical protein
			transl_table	11
81215	80415	CDS
			locus_tag	Terrestrivirus_5_74
			product	hypothetical protein
			transl_table	11
82097	81600	CDS
			locus_tag	Terrestrivirus_5_75
			product	ribonuclease H
			transl_table	11
82242	83882	CDS
			locus_tag	Terrestrivirus_5_76
			product	hypothetical protein A3E83_03615
			transl_table	11
84739	83948	CDS
			locus_tag	Terrestrivirus_5_77
			product	hypothetical protein CEUSTIGMA_g14094.t1
			transl_table	11
85418	84861	CDS
			locus_tag	Terrestrivirus_5_78
			product	phosphatidate cytidylyltransferase
			transl_table	11
86555	85650	CDS
			locus_tag	Terrestrivirus_5_79
			product	peptidase
			transl_table	11
88998	86794	CDS
			locus_tag	Terrestrivirus_5_80
			product	RING finger protein 166-like
			transl_table	11
91094	89082	CDS
			locus_tag	Terrestrivirus_5_81
			product	RING finger protein 166-like
			transl_table	11
93658	91178	CDS
			locus_tag	Terrestrivirus_5_82
			product	RING finger protein 166-like
			transl_table	11
94346	93762	CDS
			locus_tag	Terrestrivirus_5_83
			product	CuZnSOD
			transl_table	11
94539	95234	CDS
			locus_tag	Terrestrivirus_5_84
			product	hypothetical protein
			transl_table	11
95823	95257	CDS
			locus_tag	Terrestrivirus_5_85
			product	hypothetical protein
			transl_table	11
95925	96896	CDS
			locus_tag	Terrestrivirus_5_86
			product	hypothetical protein
			transl_table	11
97240	96980	CDS
			locus_tag	Terrestrivirus_5_87
			product	hypothetical protein
			transl_table	11
98350	97337	CDS
			locus_tag	Terrestrivirus_5_88
			product	Ubiquitin carboxyl-terminal hydrolase 2
			transl_table	11
98459	100198	CDS
			locus_tag	Terrestrivirus_5_89
			product	AAA family ATPase
			transl_table	11
100292	101524	CDS
			locus_tag	Terrestrivirus_5_90
			product	hypothetical protein
			transl_table	11
101725	103539	CDS
			locus_tag	Terrestrivirus_5_91
			product	putative AAA+ family ATPase
			transl_table	11
104314	103547	CDS
			locus_tag	Terrestrivirus_5_92
			product	hypothetical protein
			transl_table	11
104538	104807	CDS
			locus_tag	Terrestrivirus_5_93
			product	hypothetical protein
			transl_table	11
107125	104828	CDS
			locus_tag	Terrestrivirus_5_94
			product	Hypothetical Protein FCC1311_031102
			transl_table	11
107223	107648	CDS
			locus_tag	Terrestrivirus_5_95
			product	hypothetical protein
			transl_table	11
107793	108356	CDS
			locus_tag	Terrestrivirus_5_96
			product	hypothetical protein
			transl_table	11
109078	108377	CDS
			locus_tag	Terrestrivirus_5_97
			product	DUF2270 domain-containing protein
			transl_table	11
110369	109212	CDS
			locus_tag	Terrestrivirus_5_98
			product	hypothetical protein
			transl_table	11
111110	110508	CDS
			locus_tag	Terrestrivirus_5_99
			product	hypothetical protein
			transl_table	11
111348	111968	CDS
			locus_tag	Terrestrivirus_5_100
			product	unknown
			transl_table	11
112038	112841	CDS
			locus_tag	Terrestrivirus_5_101
			product	hypothetical protein
			transl_table	11
115995	112849	CDS
			locus_tag	Terrestrivirus_5_102
			product	hypothetical protein
			transl_table	11
116946	116110	CDS
			locus_tag	Terrestrivirus_5_103
			product	E3 ubiquitin-protein ligase Siah2
			transl_table	11
117429	117136	CDS
			locus_tag	Terrestrivirus_5_104
			product	hypothetical protein
			transl_table	11
117678	118157	CDS
			locus_tag	Terrestrivirus_5_105
			product	hypothetical protein
			transl_table	11
118213	118713	CDS
			locus_tag	Terrestrivirus_5_106
			product	hypothetical protein
			transl_table	11
119236	118745	CDS
			locus_tag	Terrestrivirus_5_107
			product	hypothetical protein
			transl_table	11
119439	119813	CDS
			locus_tag	Terrestrivirus_5_108
			product	hypothetical protein
			transl_table	11
119908	120357	CDS
			locus_tag	Terrestrivirus_5_109
			product	hypothetical protein
			transl_table	11
120443	120853	CDS
			locus_tag	Terrestrivirus_5_110
			product	hypothetical protein
			transl_table	11
121427	120870	CDS
			locus_tag	Terrestrivirus_5_111
			product	hypothetical protein SAMD00019534_033070, partial
			transl_table	11
122566	121538	CDS
			locus_tag	Terrestrivirus_5_112
			product	unknown
			transl_table	11
123252	123692	CDS
			locus_tag	Terrestrivirus_5_113
			product	hypothetical protein
			transl_table	11
123767	124171	CDS
			locus_tag	Terrestrivirus_5_114
			product	hypothetical protein
			transl_table	11
124267	125676	CDS
			locus_tag	Terrestrivirus_5_115
			product	hypothetical protein
			transl_table	11
127068	125749	CDS
			locus_tag	Terrestrivirus_5_116
			product	unnamed protein product
			transl_table	11
128129	127164	CDS
			locus_tag	Terrestrivirus_5_117
			product	hypothetical protein
			transl_table	11
128289	128690	CDS
			locus_tag	Terrestrivirus_5_118
			product	PREDICTED: autophagy-related protein 8a isoform X1
			transl_table	11
128770	129705	CDS
			locus_tag	Terrestrivirus_5_119
			product	protein of unknown function DUF3626
			transl_table	11
130530	129742	CDS
			locus_tag	Terrestrivirus_5_120
			product	hypothetical protein
			transl_table	11
130635	131966	CDS
			locus_tag	Terrestrivirus_5_121
			product	amino oxidase family protein
			transl_table	11
132066	134015	CDS
			locus_tag	Terrestrivirus_5_122
			product	hypothetical protein AKH19_06785
			transl_table	11
134107	135843	CDS
			locus_tag	Terrestrivirus_5_123
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
135909	137111	CDS
			locus_tag	Terrestrivirus_5_124
			product	hypothetical protein
			transl_table	11
137981	137124	CDS
			locus_tag	Terrestrivirus_5_125
			product	hypothetical protein
			transl_table	11
138522	138106	CDS
			locus_tag	Terrestrivirus_5_126
			product	GIY-YIG nuclease
			transl_table	11
139895	138564	CDS
			locus_tag	Terrestrivirus_5_127
			product	histidine phosphatase domain-containing protein
			transl_table	11
140745	139954	CDS
			locus_tag	Terrestrivirus_5_128
			product	hypothetical protein BATDEDRAFT_16333
			transl_table	11
140919	141545	CDS
			locus_tag	Terrestrivirus_5_129
			product	hypothetical protein
			transl_table	11
141711	142733	CDS
			locus_tag	Terrestrivirus_5_130
			product	double-stranded RNA binding motif-containing protein
			transl_table	11
142835	143206	CDS
			locus_tag	Terrestrivirus_5_131
			product	hypothetical protein
			transl_table	11
144005	143235	CDS
			locus_tag	Terrestrivirus_5_132
			product	hypothetical protein
			transl_table	11
146146	144089	CDS
			locus_tag	Terrestrivirus_5_133
			product	hypothetical protein
			transl_table	11
146327	147181	CDS
			locus_tag	Terrestrivirus_5_134
			product	hypothetical protein
			transl_table	11
147712	147188	CDS
			locus_tag	Terrestrivirus_5_135
			product	conserved Plasmodium protein, unknown function
			transl_table	11
148474	147824	CDS
			locus_tag	Terrestrivirus_5_136
			product	hypothetical protein PVX_231290
			transl_table	11
150201	148582	CDS
			locus_tag	Terrestrivirus_5_137
			product	hypothetical protein LAESUDRAFT_727400
			transl_table	11
150177	150356	CDS
			locus_tag	Terrestrivirus_5_138
			product	hypothetical protein
			transl_table	11
150781	150383	CDS
			locus_tag	Terrestrivirus_5_139
			product	hypothetical protein
			transl_table	11
153546	150868	CDS
			locus_tag	Terrestrivirus_5_140
			product	ATP-dependent metallopeptidase FtsH/Yme1/Tma family protein
			transl_table	11
153767	154429	CDS
			locus_tag	Terrestrivirus_5_141
			product	Clp protease
			transl_table	11
154553	155938	CDS
			locus_tag	Terrestrivirus_5_142
			product	hypothetical protein
			transl_table	11
156335	155946	CDS
			locus_tag	Terrestrivirus_5_143
			product	hypothetical protein
			transl_table	11
156528	158168	CDS
			locus_tag	Terrestrivirus_5_144
			product	putative membrane protein At3g27390
			transl_table	11
159306	158203	CDS
			locus_tag	Terrestrivirus_5_145
			product	STE/STE20/FRAY protein kinase
			transl_table	11
160602	159397	CDS
			locus_tag	Terrestrivirus_5_146
			product	hypothetical protein
			transl_table	11
164485	160841	CDS
			locus_tag	Terrestrivirus_5_147
			product	Uvr helicase/DDEDDh 3'-5' exonuclease
			transl_table	11
167005	164600	CDS
			locus_tag	Terrestrivirus_5_148
			product	hypothetical protein
			transl_table	11
167151	167696	CDS
			locus_tag	Terrestrivirus_5_149
			product	hypothetical protein
			transl_table	11
167768	170152	CDS
			locus_tag	Terrestrivirus_5_150
			product	putative exonuclease
			transl_table	11
171231	170158	CDS
			locus_tag	Terrestrivirus_5_151
			product	hypothetical protein CBC91_00715
			transl_table	11
171610	172056	CDS
			locus_tag	Terrestrivirus_5_152
			product	hypothetical protein Catovirus_2_73
			transl_table	11
172123	172434	CDS
			locus_tag	Terrestrivirus_5_153
			product	iodothyronine deiodinase
			transl_table	11
172985	172578	CDS
			locus_tag	Terrestrivirus_5_154
			product	hypothetical protein
			transl_table	11
173479	173204	CDS
			locus_tag	Terrestrivirus_5_155
			product	hypothetical protein
			transl_table	11
174366	173962	CDS
			locus_tag	Terrestrivirus_5_156
			product	hypothetical protein
			transl_table	11
175644	174571	CDS
			locus_tag	Terrestrivirus_5_157
			product	ubiquitin family protein
			transl_table	11
175614	176246	CDS
			locus_tag	Terrestrivirus_5_158
			product	hypothetical protein
			transl_table	11
178088	176289	CDS
			locus_tag	Terrestrivirus_5_159
			product	pyruvate oxidase
			transl_table	11
179311	178160	CDS
			locus_tag	Terrestrivirus_5_160
			product	hypothetical protein
			transl_table	11
180268	179327	CDS
			locus_tag	Terrestrivirus_5_161
			product	hypothetical protein
			transl_table	11
181464	180337	CDS
			locus_tag	Terrestrivirus_5_162
			product	unnamed protein product
			transl_table	11
182530	181565	CDS
			locus_tag	Terrestrivirus_5_163
			product	hypothetical protein
			transl_table	11
182714	183319	CDS
			locus_tag	Terrestrivirus_5_164
			product	hypothetical protein Catovirus_2_75
			transl_table	11
186668	183345	CDS
			locus_tag	Terrestrivirus_5_165
			product	DEAD-like helicases family protein
			transl_table	11
189455	186807	CDS
			locus_tag	Terrestrivirus_5_166
			product	leucyl-tRNA synthetase
			transl_table	11
190687	189572	CDS
			locus_tag	Terrestrivirus_5_167
			product	hypothetical protein Catovirus_1_858
			transl_table	11
190875	190693	CDS
			locus_tag	Terrestrivirus_5_168
			product	hypothetical protein
			transl_table	11
190867	191664	CDS
			locus_tag	Terrestrivirus_5_169
			product	hypothetical protein
			transl_table	11
191724	192482	CDS
			locus_tag	Terrestrivirus_5_170
			product	hypothetical protein Catovirus_2_78
			transl_table	11
192600	194327	CDS
			locus_tag	Terrestrivirus_5_171
			product	XRN 5'-3' exonuclease
			transl_table	11
195835	194657	CDS
			locus_tag	Terrestrivirus_5_172
			product	hypothetical protein Catovirus_2_83
			transl_table	11
197119	195944	CDS
			locus_tag	Terrestrivirus_5_173
			product	serine/threonine protein kinase
			transl_table	11
199252	197267	CDS
			locus_tag	Terrestrivirus_5_174
			product	procyclic acidic repetitive protein PARP
			transl_table	11
199989	199354	CDS
			locus_tag	Terrestrivirus_5_175
			product	ribonuclease H
			transl_table	11
200685	200053	CDS
			locus_tag	Terrestrivirus_5_176
			product	hypothetical protein Catovirus_2_86
			transl_table	11
202782	200710	CDS
			locus_tag	Terrestrivirus_5_177
			product	ankyrin repeat protein
			transl_table	11
203645	202851	CDS
			locus_tag	Terrestrivirus_5_178
			product	Clp protease
			transl_table	11
203805	203951	CDS
			locus_tag	Terrestrivirus_5_179
			product	hypothetical protein
			transl_table	11
203973	205862	CDS
			locus_tag	Terrestrivirus_5_180
			product	hypothetical protein
			transl_table	11
207810	205870	CDS
			locus_tag	Terrestrivirus_5_181
			product	ABC transporter E family member 2
			transl_table	11
208567	207950	CDS
			locus_tag	Terrestrivirus_5_182
			product	hypothetical protein
			transl_table	11
209184	208687	CDS
			locus_tag	Terrestrivirus_5_183
			product	hypothetical protein
			transl_table	11
209432	209824	CDS
			locus_tag	Terrestrivirus_5_184
			product	hypothetical protein
			transl_table	11
210694	209864	CDS
			locus_tag	Terrestrivirus_5_185
			product	hypothetical protein
			transl_table	11
211632	210820	CDS
			locus_tag	Terrestrivirus_5_186
			product	hypothetical protein Klosneuvirus_3_39
			transl_table	11
214347	211717	CDS
			locus_tag	Terrestrivirus_5_187
			product	DEXDc helicase
			transl_table	11
214494	215486	CDS
			locus_tag	Terrestrivirus_5_188
			product	Erv1/Alr family disulfide thiol oxidoreductase
			transl_table	11
215823	215515	CDS
			locus_tag	Terrestrivirus_5_189
			product	hypothetical protein
			transl_table	11
217553	215958	CDS
			locus_tag	Terrestrivirus_5_190
			product	DHH family phosphohydrolase
			transl_table	11
219052	217637	CDS
			locus_tag	Terrestrivirus_5_191
			product	hypothetical protein
			transl_table	11
219243	219677	CDS
			locus_tag	Terrestrivirus_5_192
			product	translation initiation factor 2 subunit beta
			transl_table	11
219787	219674	CDS
			locus_tag	Terrestrivirus_5_193
			product	hypothetical protein
			transl_table	11
222227	219756	CDS
			locus_tag	Terrestrivirus_5_194
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
222545	222931	CDS
			locus_tag	Terrestrivirus_5_195
			product	hypothetical protein
			transl_table	11
223156	223061	CDS
			locus_tag	Terrestrivirus_5_196
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_6
2	121	CDS
			locus_tag	Terrestrivirus_6_1
			product	hypothetical protein
			transl_table	11
539	174	CDS
			locus_tag	Terrestrivirus_6_2
			product	hypothetical protein
			transl_table	11
2128	662	CDS
			locus_tag	Terrestrivirus_6_3
			product	hypothetical protein CBC91_00685
			transl_table	11
2367	3074	CDS
			locus_tag	Terrestrivirus_6_4
			product	hypothetical protein BMW23_0318
			transl_table	11
3905	3207	CDS
			locus_tag	Terrestrivirus_6_5
			product	hypothetical protein
			transl_table	11
4076	4600	CDS
			locus_tag	Terrestrivirus_6_6
			product	hypothetical protein
			transl_table	11
5434	4595	CDS
			locus_tag	Terrestrivirus_6_7
			product	alpha/beta hydrolase family protein
			transl_table	11
5637	7373	CDS
			locus_tag	Terrestrivirus_6_8
			product	hypothetical protein
			transl_table	11
8374	7364	CDS
			locus_tag	Terrestrivirus_6_9
			product	hypothetical protein
			transl_table	11
8474	8361	CDS
			locus_tag	Terrestrivirus_6_10
			product	hypothetical protein
			transl_table	11
8617	10362	CDS
			locus_tag	Terrestrivirus_6_11
			product	hypothetical protein Catovirus_1_656
			transl_table	11
11618	10368	CDS
			locus_tag	Terrestrivirus_6_12
			product	superfamily II helicase
			transl_table	11
12639	11680	CDS
			locus_tag	Terrestrivirus_6_13
			product	hypothetical protein A2406_01650
			transl_table	11
12746	14065	CDS
			locus_tag	Terrestrivirus_6_14
			product	alpha/beta hydrolase fold
			transl_table	11
14129	14827	CDS
			locus_tag	Terrestrivirus_6_15
			product	hypothetical protein
			transl_table	11
14946	15782	CDS
			locus_tag	Terrestrivirus_6_16
			product	mitochondrial carrier
			transl_table	11
17640	15790	CDS
			locus_tag	Terrestrivirus_6_17
			product	dna topoisomerase 1b
			transl_table	11
17752	18096	CDS
			locus_tag	Terrestrivirus_6_18
			product	hypothetical protein
			transl_table	11
18217	19881	CDS
			locus_tag	Terrestrivirus_6_19
			product	protein kinase
			transl_table	11
19962	22220	CDS
			locus_tag	Terrestrivirus_6_20
			product	putative helicase/exonuclease
			transl_table	11
22791	22240	CDS
			locus_tag	Terrestrivirus_6_21
			product	hypothetical protein
			transl_table	11
22997	23566	CDS
			locus_tag	Terrestrivirus_6_22
			product	hypothetical protein
			transl_table	11
23682	24059	CDS
			locus_tag	Terrestrivirus_6_23
			product	vesicle-associated membrane protein 4
			transl_table	11
24154	25218	CDS
			locus_tag	Terrestrivirus_6_24
			product	hypothetical protein
			transl_table	11
25929	25234	CDS
			locus_tag	Terrestrivirus_6_25
			product	Rab GTPase
			transl_table	11
26098	26949	CDS
			locus_tag	Terrestrivirus_6_26
			product	hypothetical protein
			transl_table	11
27151	28869	CDS
			locus_tag	Terrestrivirus_6_27
			product	hypothetical protein BATDEDRAFT_33807
			transl_table	11
29016	30455	CDS
			locus_tag	Terrestrivirus_6_28
			product	hypothetical protein THRCLA_04012
			transl_table	11
30593	31111	CDS
			locus_tag	Terrestrivirus_6_29
			product	hypothetical protein
			transl_table	11
31766	31125	CDS
			locus_tag	Terrestrivirus_6_30
			product	hypothetical protein BofuT4P22000051001
			transl_table	11
31955	35368	CDS
			locus_tag	Terrestrivirus_6_31
			product	PE_PGRS
			transl_table	11
35666	35379	CDS
			locus_tag	Terrestrivirus_6_32
			product	hypothetical protein
			transl_table	11
36558	35794	CDS
			locus_tag	Terrestrivirus_6_33
			product	hypothetical protein Catovirus_2_144
			transl_table	11
36967	36626	CDS
			locus_tag	Terrestrivirus_6_34
			product	acetyltransferase GNAT family protein
			transl_table	11
38428	37013	CDS
			locus_tag	Terrestrivirus_6_35
			product	hypothetical protein
			transl_table	11
39486	38860	CDS
			locus_tag	Terrestrivirus_6_36
			product	hypothetical protein
			transl_table	11
39695	40231	CDS
			locus_tag	Terrestrivirus_6_37
			product	PREDICTED: vesicle transport protein SFT2B isoform X1
			transl_table	11
40349	41575	CDS
			locus_tag	Terrestrivirus_6_38
			product	hypothetical protein mc_810
			transl_table	11
42076	41606	CDS
			locus_tag	Terrestrivirus_6_39
			product	hypothetical protein
			transl_table	11
44485	42227	CDS
			locus_tag	Terrestrivirus_6_40
			product	HrpA-like RNA helicase
			transl_table	11
44693	44490	CDS
			locus_tag	Terrestrivirus_6_41
			product	hypothetical protein
			transl_table	11
45242	44742	CDS
			locus_tag	Terrestrivirus_6_42
			product	hypothetical protein
			transl_table	11
45495	47546	CDS
			locus_tag	Terrestrivirus_6_43
			product	hypothetical protein
			transl_table	11
48111	49145	CDS
			locus_tag	Terrestrivirus_6_44
			product	chitinase-like protein cluster D, partial
			transl_table	11
49224	52133	CDS
			locus_tag	Terrestrivirus_6_45
			product	putative hydrolase
			transl_table	11
52621	52148	CDS
			locus_tag	Terrestrivirus_6_46
			product	hypothetical protein
			transl_table	11
52784	52957	CDS
			locus_tag	Terrestrivirus_6_47
			product	hypothetical protein
			transl_table	11
53048	53641	CDS
			locus_tag	Terrestrivirus_6_48
			product	hypothetical protein
			transl_table	11
53744	54919	CDS
			locus_tag	Terrestrivirus_6_49
			product	hypothetical protein
			transl_table	11
56706	54958	CDS
			locus_tag	Terrestrivirus_6_50
			product	protein asteroid homolog 1-like
			transl_table	11
56779	57627	CDS
			locus_tag	Terrestrivirus_6_51
			product	patatin-like phospholipase domain-containing protein 3
			transl_table	11
58116	57613	CDS
			locus_tag	Terrestrivirus_6_52
			product	hypothetical protein
			transl_table	11
58313	58885	CDS
			locus_tag	Terrestrivirus_6_53
			product	hypothetical protein
			transl_table	11
60539	58950	CDS
			locus_tag	Terrestrivirus_6_54
			product	hypothetical protein
			transl_table	11
60734	61345	CDS
			locus_tag	Terrestrivirus_6_55
			product	hypothetical protein
			transl_table	11
61387	61644	CDS
			locus_tag	Terrestrivirus_6_56
			product	hypothetical protein
			transl_table	11
61820	62362	CDS
			locus_tag	Terrestrivirus_6_57
			product	hypothetical protein
			transl_table	11
62567	63046	CDS
			locus_tag	Terrestrivirus_6_58
			product	hypothetical protein
			transl_table	11
63186	63824	CDS
			locus_tag	Terrestrivirus_6_59
			product	hypothetical protein
			transl_table	11
64063	63848	CDS
			locus_tag	Terrestrivirus_6_60
			product	hypothetical protein
			transl_table	11
64134	64280	CDS
			locus_tag	Terrestrivirus_6_61
			product	hypothetical protein
			transl_table	11
65209	64340	CDS
			locus_tag	Terrestrivirus_6_62
			product	hypothetical protein
			transl_table	11
65405	66049	CDS
			locus_tag	Terrestrivirus_6_63
			product	hypothetical protein
			transl_table	11
66193	66882	CDS
			locus_tag	Terrestrivirus_6_64
			product	hypothetical protein NAEGRDRAFT_59759
			transl_table	11
67653	66907	CDS
			locus_tag	Terrestrivirus_6_65
			product	hypothetical protein
			transl_table	11
67788	68150	CDS
			locus_tag	Terrestrivirus_6_66
			product	hypothetical protein Indivirus_6_33
			transl_table	11
68205	68543	CDS
			locus_tag	Terrestrivirus_6_67
			product	hypothetical protein
			transl_table	11
69168	68548	CDS
			locus_tag	Terrestrivirus_6_68
			product	hypothetical protein
			transl_table	11
70262	69432	CDS
			locus_tag	Terrestrivirus_6_69
			product	hypothetical protein
			transl_table	11
70482	71066	CDS
			locus_tag	Terrestrivirus_6_70
			product	hypothetical protein
			transl_table	11
73507	71072	CDS
			locus_tag	Terrestrivirus_6_71
			product	GTP cyclohydrolase
			transl_table	11
75069	73603	CDS
			locus_tag	Terrestrivirus_6_72
			product	IMP dehydrogenase/GMP reductase
			transl_table	11
75327	75584	CDS
			locus_tag	Terrestrivirus_6_73
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_7
3	434	CDS
			locus_tag	Terrestrivirus_7_1
			product	hypothetical protein Catovirus_2_175
			transl_table	11
2042	447	CDS
			locus_tag	Terrestrivirus_7_2
			product	FtsJ-like methyltransferase
			transl_table	11
2227	6150	CDS
			locus_tag	Terrestrivirus_7_3
			product	mRNA capping enzyme
			transl_table	11
6246	7853	CDS
			locus_tag	Terrestrivirus_7_4
			product	SWIB/MDM2 domain protein
			transl_table	11
7961	9424	CDS
			locus_tag	Terrestrivirus_7_5
			product	bifunctional dihydrofolate reductase/thymidylate synthase
			transl_table	11
9521	10486	CDS
			locus_tag	Terrestrivirus_7_6
			product	eukaryotic translation initiation factor 4G
			transl_table	11
10702	10571	CDS
			locus_tag	Terrestrivirus_7_7
			product	hypothetical protein
			transl_table	11
10978	11406	CDS
			locus_tag	Terrestrivirus_7_8
			product	hypothetical protein
			transl_table	11
11566	13059	CDS
			locus_tag	Terrestrivirus_7_9
			product	hypothetical protein JAAARDRAFT_37783
			transl_table	11
13910	13062	CDS
			locus_tag	Terrestrivirus_7_10
			product	MULTISPECIES: hypothetical protein
			transl_table	11
13998	15083	CDS
			locus_tag	Terrestrivirus_7_11
			product	putative short-chain type dehydrogenase/reductase
			transl_table	11
15227	16621	CDS
			locus_tag	Terrestrivirus_7_12
			product	alanine--tRNA ligase-like
			transl_table	11
17639	16635	CDS
			locus_tag	Terrestrivirus_7_13
			product	hypothetical protein 162300064
			transl_table	11
18051	19703	CDS
			locus_tag	Terrestrivirus_7_14
			product	ABC transporter
			transl_table	11
21373	19700	CDS
			locus_tag	Terrestrivirus_7_15
			product	GTPase
			transl_table	11
21502	22581	CDS
			locus_tag	Terrestrivirus_7_16
			product	hypothetical protein
			transl_table	11
22705	24261	CDS
			locus_tag	Terrestrivirus_7_17
			product	FLAP-like endonuclease XPG
			transl_table	11
25654	24290	CDS
			locus_tag	Terrestrivirus_7_18
			product	histidine tRNA synthetase
			transl_table	11
25860	27275	CDS
			locus_tag	Terrestrivirus_7_19
			product	DnaJ domain protein
			transl_table	11
28105	27365	CDS
			locus_tag	Terrestrivirus_7_20
			product	hypothetical protein
			transl_table	11
28353	28712	CDS
			locus_tag	Terrestrivirus_7_21
			product	protein of unknown function DUF814
			transl_table	11
29302	28709	CDS
			locus_tag	Terrestrivirus_7_22
			product	deoxynucleoside monophosphate kinase
			transl_table	11
29871	29398	CDS
			locus_tag	Terrestrivirus_7_23
			product	hypothetical protein
			transl_table	11
30517	29966	CDS
			locus_tag	Terrestrivirus_7_24
			product	hypothetical protein
			transl_table	11
30713	31927	CDS
			locus_tag	Terrestrivirus_7_25
			product	replication factor C small subunit
			transl_table	11
32058	32921	CDS
			locus_tag	Terrestrivirus_7_26
			product	hypothetical protein
			transl_table	11
33969	32923	CDS
			locus_tag	Terrestrivirus_7_27
			product	hypothetical protein Catovirus_2_154
			transl_table	11
34143	35438	CDS
			locus_tag	Terrestrivirus_7_28
			product	N-myristoyltransferase
			transl_table	11
35568	36104	CDS
			locus_tag	Terrestrivirus_7_29
			product	hypothetical protein
			transl_table	11
36245	39022	CDS
			locus_tag	Terrestrivirus_7_30
			product	Zn-dependent peptidase
			transl_table	11
39121	39951	CDS
			locus_tag	Terrestrivirus_7_31
			product	hypothetical protein
			transl_table	11
41542	39977	CDS
			locus_tag	Terrestrivirus_7_32
			product	hypothetical protein
			transl_table	11
42061	41663	CDS
			locus_tag	Terrestrivirus_7_33
			product	hypothetical protein
			transl_table	11
42935	42132	CDS
			locus_tag	Terrestrivirus_7_34
			product	hypothetical protein Catovirus_2_148
			transl_table	11
44585	43011	CDS
			locus_tag	Terrestrivirus_7_35
			product	hypothetical protein
			transl_table	11
44764	48198	CDS
			locus_tag	Terrestrivirus_7_36
			product	isoleucyl-tRNA synthetase
			transl_table	11
48238	48675	CDS
			locus_tag	Terrestrivirus_7_37
			product	hypothetical protein
			transl_table	11
48745	48948	CDS
			locus_tag	Terrestrivirus_7_38
			product	hypothetical protein
			transl_table	11
49051	49293	CDS
			locus_tag	Terrestrivirus_7_39
			product	hypothetical protein
			transl_table	11
49420	50274	CDS
			locus_tag	Terrestrivirus_7_40
			product	hypothetical protein Catovirus_2_133
			transl_table	11
50372	51292	CDS
			locus_tag	Terrestrivirus_7_41
			product	hypothetical protein Catovirus_2_132
			transl_table	11
51775	51305	CDS
			locus_tag	Terrestrivirus_7_42
			product	I cysteine dioxygenase
			transl_table	11
52784	51849	CDS
			locus_tag	Terrestrivirus_7_43
			product	formamidopyrimidine-DNA glycosylase
			transl_table	11
54953	52866	CDS
			locus_tag	Terrestrivirus_7_44
			product	AMP-binding enzyme
			transl_table	11
55056	56276	CDS
			locus_tag	Terrestrivirus_7_45
			product	lysophospholipid acyltransferase
			transl_table	11
56607	56284	CDS
			locus_tag	Terrestrivirus_7_46
			product	hypothetical protein
			transl_table	11
57375	56659	CDS
			locus_tag	Terrestrivirus_7_47
			product	ankyrin repeat protein
			transl_table	11
57666	57454	CDS
			locus_tag	Terrestrivirus_7_48
			product	hypothetical protein
			transl_table	11
58097	57726	CDS
			locus_tag	Terrestrivirus_7_49
			product	hypothetical protein
			transl_table	11
59529	58132	CDS
			locus_tag	Terrestrivirus_7_50
			product	exodeoxyribonuclease VII large subunit
			transl_table	11
59698	59898	CDS
			locus_tag	Terrestrivirus_7_51
			product	hypothetical protein
			transl_table	11
61047	59872	CDS
			locus_tag	Terrestrivirus_7_52
			product	hypothetical protein Catovirus_2_128
			transl_table	11
62137	61145	CDS
			locus_tag	Terrestrivirus_7_53
			product	alpha/beta hydrolase family protein
			transl_table	11
63000	62170	CDS
			locus_tag	Terrestrivirus_7_54
			product	hypothetical protein pv_1
			transl_table	11
63934	63140	CDS
			locus_tag	Terrestrivirus_7_55
			product	uncharacterized protein Dvir_GJ23890
			transl_table	11
64060	66315	CDS
			locus_tag	Terrestrivirus_7_56
			product	hypothetical protein Indivirus_2_58
			transl_table	11
66757	67836	CDS
			locus_tag	Terrestrivirus_7_57
			product	hypothetical protein Catovirus_2_124
			transl_table	11
67999	68241	CDS
			locus_tag	Terrestrivirus_7_58
			product	hypothetical protein
			transl_table	11
68243	68617	CDS
			locus_tag	Terrestrivirus_7_59
			product	hypothetical protein Catovirus_2_123
			transl_table	11
69375	68668	CDS
			locus_tag	Terrestrivirus_7_60
			product	hypothetical protein Catovirus_2_122
			transl_table	11
>Terrestrivirus_8
3342	322	CDS
			locus_tag	Terrestrivirus_8_1
			product	putative hydrolase
			transl_table	11
3883	3452	CDS
			locus_tag	Terrestrivirus_8_2
			product	hypothetical protein Catovirus_1_112
			transl_table	11
4777	3908	CDS
			locus_tag	Terrestrivirus_8_3
			product	hypothetical protein
			transl_table	11
5447	4872	CDS
			locus_tag	Terrestrivirus_8_4
			product	hypothetical protein
			transl_table	11
5907	5614	CDS
			locus_tag	Terrestrivirus_8_5
			product	hypothetical protein
			transl_table	11
7190	6252	CDS
			locus_tag	Terrestrivirus_8_6
			product	hypothetical protein
			transl_table	11
7237	7404	CDS
			locus_tag	Terrestrivirus_8_7
			product	hypothetical protein
			transl_table	11
7486	7872	CDS
			locus_tag	Terrestrivirus_8_8
			product	hypothetical protein
			transl_table	11
8578	7874	CDS
			locus_tag	Terrestrivirus_8_9
			product	hypothetical protein
			transl_table	11
8847	8758	CDS
			locus_tag	Terrestrivirus_8_10
			product	hypothetical protein
			transl_table	11
9655	8834	CDS
			locus_tag	Terrestrivirus_8_11
			product	hypothetical protein
			transl_table	11
10076	10306	CDS
			locus_tag	Terrestrivirus_8_12
			product	hypothetical protein
			transl_table	11
10552	10424	CDS
			locus_tag	Terrestrivirus_8_13
			product	hypothetical protein
			transl_table	11
10845	10720	CDS
			locus_tag	Terrestrivirus_8_14
			product	hypothetical protein
			transl_table	11
11363	10983	CDS
			locus_tag	Terrestrivirus_8_15
			product	hypothetical protein
			transl_table	11
12002	11709	CDS
			locus_tag	Terrestrivirus_8_16
			product	hypothetical protein
			transl_table	11
12447	12088	CDS
			locus_tag	Terrestrivirus_8_17
			product	hypothetical protein
			transl_table	11
13339	12608	CDS
			locus_tag	Terrestrivirus_8_18
			product	hypothetical protein
			transl_table	11
14459	13869	CDS
			locus_tag	Terrestrivirus_8_19
			product	hypothetical protein
			transl_table	11
14583	14440	CDS
			locus_tag	Terrestrivirus_8_20
			product	hypothetical protein
			transl_table	11
15242	14688	CDS
			locus_tag	Terrestrivirus_8_21
			product	hypothetical protein
			transl_table	11
16750	15596	CDS
			locus_tag	Terrestrivirus_8_22
			product	hypothetical protein
			transl_table	11
18332	17187	CDS
			locus_tag	Terrestrivirus_8_23
			product	hypothetical protein
			transl_table	11
19658	18621	CDS
			locus_tag	Terrestrivirus_8_24
			product	hypothetical protein
			transl_table	11
21315	19942	CDS
			locus_tag	Terrestrivirus_8_25
			product	hypothetical protein
			transl_table	11
22351	21779	CDS
			locus_tag	Terrestrivirus_8_26
			product	hypothetical protein Hokovirus_1_321
			transl_table	11
23288	22617	CDS
			locus_tag	Terrestrivirus_8_27
			product	uncharacterized protein Dmoj_GI23689, isoform A
			transl_table	11
23675	24859	CDS
			locus_tag	Terrestrivirus_8_28
			product	serpin
			transl_table	11
27425	24876	CDS
			locus_tag	Terrestrivirus_8_29
			product	ATP-dependent endonuclease
			transl_table	11
27985	27656	CDS
			locus_tag	Terrestrivirus_8_30
			product	hypothetical protein
			transl_table	11
29365	28370	CDS
			locus_tag	Terrestrivirus_8_31
			product	hypothetical protein CBC91_00715
			transl_table	11
29904	30305	CDS
			locus_tag	Terrestrivirus_8_32
			product	hypothetical protein
			transl_table	11
32390	30315	CDS
			locus_tag	Terrestrivirus_8_33
			product	dynamin family GTPase
			transl_table	11
33363	32617	CDS
			locus_tag	Terrestrivirus_8_34
			product	putative endonuclease
			transl_table	11
35353	33479	CDS
			locus_tag	Terrestrivirus_8_35
			product	dynamin family GTPase
			transl_table	11
35706	37277	CDS
			locus_tag	Terrestrivirus_8_36
			product	hypothetical protein
			transl_table	11
37317	37850	CDS
			locus_tag	Terrestrivirus_8_37
			product	hypothetical protein
			transl_table	11
38762	37860	CDS
			locus_tag	Terrestrivirus_8_38
			product	hypothetical protein
			transl_table	11
41522	39153	CDS
			locus_tag	Terrestrivirus_8_39
			product	ATP-dependent endonuclease
			transl_table	11
41678	43579	CDS
			locus_tag	Terrestrivirus_8_40
			product	hypothetical protein A2X12_10260
			transl_table	11
46557	43576	CDS
			locus_tag	Terrestrivirus_8_41
			product	type I restriction-modification DNA methylase
			transl_table	11
48939	46645	CDS
			locus_tag	Terrestrivirus_8_42
			product	superfamily II DNA or RNA helicase
			transl_table	11
49559	50443	CDS
			locus_tag	Terrestrivirus_8_43
			product	hypothetical protein
			transl_table	11
51064	50612	CDS
			locus_tag	Terrestrivirus_8_44
			product	hypothetical protein
			transl_table	11
51221	51568	CDS
			locus_tag	Terrestrivirus_8_45
			product	UBC11-like protein
			transl_table	11
51916	52710	CDS
			locus_tag	Terrestrivirus_8_46
			product	hypothetical protein
			transl_table	11
52818	53549	CDS
			locus_tag	Terrestrivirus_8_47
			product	hypothetical protein
			transl_table	11
53735	54214	CDS
			locus_tag	Terrestrivirus_8_48
			product	hypothetical protein
			transl_table	11
55453	54419	CDS
			locus_tag	Terrestrivirus_8_49
			product	hypothetical protein
			transl_table	11
55866	56087	CDS
			locus_tag	Terrestrivirus_8_50
			product	hypothetical protein
			transl_table	11
56461	56754	CDS
			locus_tag	Terrestrivirus_8_51
			product	hypothetical protein
			transl_table	11
56823	57212	CDS
			locus_tag	Terrestrivirus_8_52
			product	hypothetical protein
			transl_table	11
57630	57328	CDS
			locus_tag	Terrestrivirus_8_53
			product	hypothetical protein
			transl_table	11
57735	58355	CDS
			locus_tag	Terrestrivirus_8_54
			product	hypothetical protein
			transl_table	11
59477	58410	CDS
			locus_tag	Terrestrivirus_8_55
			product	hypothetical protein
			transl_table	11
60085	59534	CDS
			locus_tag	Terrestrivirus_8_56
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_9
448	2	CDS
			locus_tag	Terrestrivirus_9_1
			product	hypothetical protein
			transl_table	11
1479	655	CDS
			locus_tag	Terrestrivirus_9_2
			product	protein of unknown function DUF3626
			transl_table	11
2640	1567	CDS
			locus_tag	Terrestrivirus_9_3
			product	hypothetical protein
			transl_table	11
2870	4537	CDS
			locus_tag	Terrestrivirus_9_4
			product	hypothetical protein SPAPADRAFT_143323
			transl_table	11
4711	5886	CDS
			locus_tag	Terrestrivirus_9_5
			product	hypothetical protein
			transl_table	11
6009	7169	CDS
			locus_tag	Terrestrivirus_9_6
			product	hypothetical protein
			transl_table	11
7307	7687	CDS
			locus_tag	Terrestrivirus_9_7
			product	hypothetical protein
			transl_table	11
8495	7854	CDS
			locus_tag	Terrestrivirus_9_8
			product	hypothetical protein
			transl_table	11
9374	8649	CDS
			locus_tag	Terrestrivirus_9_9
			product	hypothetical protein
			transl_table	11
10253	9555	CDS
			locus_tag	Terrestrivirus_9_10
			product	hypothetical protein
			transl_table	11
11364	10432	CDS
			locus_tag	Terrestrivirus_9_11
			product	hypothetical protein
			transl_table	11
12584	11514	CDS
			locus_tag	Terrestrivirus_9_12
			product	hypothetical protein
			transl_table	11
13933	12767	CDS
			locus_tag	Terrestrivirus_9_13
			product	hypothetical protein
			transl_table	11
14976	14167	CDS
			locus_tag	Terrestrivirus_9_14
			product	hypothetical protein
			transl_table	11
15890	15093	CDS
			locus_tag	Terrestrivirus_9_15
			product	hypothetical protein
			transl_table	11
16831	16004	CDS
			locus_tag	Terrestrivirus_9_16
			product	hypothetical protein
			transl_table	11
17763	16951	CDS
			locus_tag	Terrestrivirus_9_17
			product	aldo/keto reductase
			transl_table	11
17952	18689	CDS
			locus_tag	Terrestrivirus_9_18
			product	hypothetical protein
			transl_table	11
19710	18682	CDS
			locus_tag	Terrestrivirus_9_19
			product	hypothetical protein
			transl_table	11
20502	19840	CDS
			locus_tag	Terrestrivirus_9_20
			product	hypothetical protein Catovirus_1_795
			transl_table	11
21176	20637	CDS
			locus_tag	Terrestrivirus_9_21
			product	hypothetical protein THRCLA_07794
			transl_table	11
21498	21235	CDS
			locus_tag	Terrestrivirus_9_22
			product	hypothetical protein
			transl_table	11
22511	21600	CDS
			locus_tag	Terrestrivirus_9_23
			product	hypothetical protein
			transl_table	11
23278	22595	CDS
			locus_tag	Terrestrivirus_9_24
			product	predicted protein
			transl_table	11
24606	23386	CDS
			locus_tag	Terrestrivirus_9_25
			product	Cyclopropane-fatty-acyl-phospholipid synthase
			transl_table	11
25048	24701	CDS
			locus_tag	Terrestrivirus_9_26
			product	protein of unknown function DUF4326
			transl_table	11
25289	25912	CDS
			locus_tag	Terrestrivirus_9_27
			product	hypothetical protein
			transl_table	11
27937	26246	CDS
			locus_tag	Terrestrivirus_9_28
			product	hypothetical protein BWG23_11685
			transl_table	11
29424	27982	CDS
			locus_tag	Terrestrivirus_9_29
			product	DEXDc helicase
			transl_table	11
29565	30026	CDS
			locus_tag	Terrestrivirus_9_30
			product	hypothetical protein
			transl_table	11
31185	30028	CDS
			locus_tag	Terrestrivirus_9_31
			product	hypothetical protein
			transl_table	11
31691	31224	CDS
			locus_tag	Terrestrivirus_9_32
			product	ribosomal-protein-alanine N-acetyltransferase
			transl_table	11
32633	31767	CDS
			locus_tag	Terrestrivirus_9_33
			product	Metal-dependent hydrolase, endonuclease/exonuclease/phosphatase family
			transl_table	11
32838	33449	CDS
			locus_tag	Terrestrivirus_9_34
			product	phosphomevalonate kinase-like
			transl_table	11
34545	33451	CDS
			locus_tag	Terrestrivirus_9_35
			product	hypothetical protein SAMD00019534_100840
			transl_table	11
34797	34603	CDS
			locus_tag	Terrestrivirus_9_36
			product	hypothetical protein
			transl_table	11
34813	36243	CDS
			locus_tag	Terrestrivirus_9_37
			product	hypothetical protein FVEG_04830
			transl_table	11
36655	36254	CDS
			locus_tag	Terrestrivirus_9_38
			product	hypothetical protein
			transl_table	11
36783	37520	CDS
			locus_tag	Terrestrivirus_9_39
			product	hypothetical protein
			transl_table	11
37719	38258	CDS
			locus_tag	Terrestrivirus_9_40
			product	hypothetical protein
			transl_table	11
38312	39121	CDS
			locus_tag	Terrestrivirus_9_41
			product	BAX inhibitor BI-1
			transl_table	11
39280	40137	CDS
			locus_tag	Terrestrivirus_9_42
			product	hypothetical protein RclHR1_19810001
			transl_table	11
40428	40598	CDS
			locus_tag	Terrestrivirus_9_43
			product	hypothetical protein
			transl_table	11
40662	41333	CDS
			locus_tag	Terrestrivirus_9_44
			product	tRNA pseudouridine synthase B, tRNA pseudouridine55 synthase
			transl_table	11
42206	41337	CDS
			locus_tag	Terrestrivirus_9_45
			product	hypothetical protein CEUSTIGMA_g145.t1
			transl_table	11
43898	42324	CDS
			locus_tag	Terrestrivirus_9_46
			product	hypothetical protein
			transl_table	11
44319	43966	CDS
			locus_tag	Terrestrivirus_9_47
			product	hypothetical protein Klosneuvirus_6_19
			transl_table	11
44493	45377	CDS
			locus_tag	Terrestrivirus_9_48
			product	hypothetical protein
			transl_table	11
46399	45389	CDS
			locus_tag	Terrestrivirus_9_49
			product	hypothetical protein
			transl_table	11
46632	47264	CDS
			locus_tag	Terrestrivirus_9_50
			product	hypothetical protein
			transl_table	11
48958	47312	CDS
			locus_tag	Terrestrivirus_9_51
			product	AAA family ATPase
			transl_table	11
49269	49126	CDS
			locus_tag	Terrestrivirus_9_52
			product	hypothetical protein
			transl_table	11
51141	49417	CDS
			locus_tag	Terrestrivirus_9_53
			product	hypothetical protein Klosneuvirus_1_394
			transl_table	11
>Terrestrivirus_10
341	3	CDS
			locus_tag	Terrestrivirus_10_1
			product	YbhB/YbcL family Raf kinase inhibitor-like protein
			transl_table	11
457	783	CDS
			locus_tag	Terrestrivirus_10_2
			product	hypothetical protein
			transl_table	11
867	1781	CDS
			locus_tag	Terrestrivirus_10_3
			product	hypothetical protein
			transl_table	11
2912	1839	CDS
			locus_tag	Terrestrivirus_10_4
			product	hypothetical protein
			transl_table	11
3101	3781	CDS
			locus_tag	Terrestrivirus_10_5
			product	hypothetical protein
			transl_table	11
6016	3788	CDS
			locus_tag	Terrestrivirus_10_6
			product	von willebrand factor type A domain protein, putative
			transl_table	11
6920	6135	CDS
			locus_tag	Terrestrivirus_10_7
			product	hypothetical protein
			transl_table	11
8363	7026	CDS
			locus_tag	Terrestrivirus_10_8
			product	MBL fold metallo-hydrolase
			transl_table	11
9872	8445	CDS
			locus_tag	Terrestrivirus_10_9
			product	mannose-1-phosphate guanylyltransferase/mannose-6-phosphate isomerase
			transl_table	11
10080	12215	CDS
			locus_tag	Terrestrivirus_10_10
			product	hypothetical protein DFA_05541
			transl_table	11
12299	13060	CDS
			locus_tag	Terrestrivirus_10_11
			product	hypothetical protein
			transl_table	11
15535	13100	CDS
			locus_tag	Terrestrivirus_10_12
			product	hypothetical protein RclHR1_00180045
			transl_table	11
17274	15643	CDS
			locus_tag	Terrestrivirus_10_13
			product	hypothetical protein
			transl_table	11
17430	18932	CDS
			locus_tag	Terrestrivirus_10_14
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
20803	18983	CDS
			locus_tag	Terrestrivirus_10_15
			product	DEAD/SNF2-like helicase
			transl_table	11
21316	20987	CDS
			locus_tag	Terrestrivirus_10_16
			product	hypothetical protein
			transl_table	11
22272	21649	CDS
			locus_tag	Terrestrivirus_10_17
			product	GTP-binding protein YPTC1
			transl_table	11
23352	22675	CDS
			locus_tag	Terrestrivirus_10_18
			product	ras-related protein Rab-35
			transl_table	11
24577	23441	CDS
			locus_tag	Terrestrivirus_10_19
			product	hypothetical protein
			transl_table	11
25737	24619	CDS
			locus_tag	Terrestrivirus_10_20
			product	RNA ligase
			transl_table	11
26294	25800	CDS
			locus_tag	Terrestrivirus_10_21
			product	hypothetical protein
			transl_table	11
27557	26358	CDS
			locus_tag	Terrestrivirus_10_22
			product	hypothetical protein BGO43_08425
			transl_table	11
27784	28368	CDS
			locus_tag	Terrestrivirus_10_23
			product	hypothetical protein Indivirus_9_11
			transl_table	11
28536	29351	CDS
			locus_tag	Terrestrivirus_10_24
			product	YchF/TatD family DNA exonuclease
			transl_table	11
33212	29496	CDS
			locus_tag	Terrestrivirus_10_25
			product	Ubiquitinconjugating enzyme subfamily protein
			transl_table	11
34530	33310	CDS
			locus_tag	Terrestrivirus_10_26
			product	hypothetical protein
			transl_table	11
34638	35009	CDS
			locus_tag	Terrestrivirus_10_27
			product	hypothetical protein
			transl_table	11
35746	35042	CDS
			locus_tag	Terrestrivirus_10_28
			product	hypothetical protein
			transl_table	11
35864	36349	CDS
			locus_tag	Terrestrivirus_10_29
			product	hypothetical protein
			transl_table	11
37900	36335	CDS
			locus_tag	Terrestrivirus_10_30
			product	restriction endonuclease
			transl_table	11
39544	37943	CDS
			locus_tag	Terrestrivirus_10_31
			product	Flotillin domain-containing protein
			transl_table	11
40644	39811	CDS
			locus_tag	Terrestrivirus_10_32
			product	hypothetical protein
			transl_table	11
41471	40749	CDS
			locus_tag	Terrestrivirus_10_33
			product	hypothetical protein
			transl_table	11
41517	41681	CDS
			locus_tag	Terrestrivirus_10_34
			product	hypothetical protein
			transl_table	11
42095	41820	CDS
			locus_tag	Terrestrivirus_10_35
			product	RecName: Full=Putative acyl-CoA-binding protein; Short=ACBP
			transl_table	11
42239	42655	CDS
			locus_tag	Terrestrivirus_10_36
			product	hypothetical protein
			transl_table	11
42739	43656	CDS
			locus_tag	Terrestrivirus_10_37
			product	beta-lactamase superfamily domain
			transl_table	11
45091	44594	CDS
			locus_tag	Terrestrivirus_10_38
			product	hypothetical protein
			transl_table	11
45564	45157	CDS
			locus_tag	Terrestrivirus_10_39
			product	hypothetical protein
			transl_table	11
46625	45666	CDS
			locus_tag	Terrestrivirus_10_40
			product	AGAP004597-PA
			transl_table	11
46711	46622	CDS
			locus_tag	Terrestrivirus_10_41
			product	hypothetical protein
			transl_table	11
46835	47140	CDS
			locus_tag	Terrestrivirus_10_42
			product	hypothetical protein
			transl_table	11
49228	47147	CDS
			locus_tag	Terrestrivirus_10_43
			product	phosphoribosylpyrophosphate synthetase
			transl_table	11
>Terrestrivirus_11
3	473	CDS
			locus_tag	Terrestrivirus_11_1
			product	hypothetical protein
			transl_table	11
2810	486	CDS
			locus_tag	Terrestrivirus_11_2
			product	DEAD/SNF2-like helicase
			transl_table	11
3002	2874	CDS
			locus_tag	Terrestrivirus_11_3
			product	hypothetical protein
			transl_table	11
4028	3186	CDS
			locus_tag	Terrestrivirus_11_4
			product	hypothetical protein
			transl_table	11
4149	4838	CDS
			locus_tag	Terrestrivirus_11_5
			product	hypothetical protein Klosneuvirus_1_180
			transl_table	11
5894	4920	CDS
			locus_tag	Terrestrivirus_11_6
			product	ribonuclease III
			transl_table	11
8121	6013	CDS
			locus_tag	Terrestrivirus_11_7
			product	YqaJ-like viral recombinase domain
			transl_table	11
8311	9972	CDS
			locus_tag	Terrestrivirus_11_8
			product	GTP binding translation elongation factor
			transl_table	11
10227	10000	CDS
			locus_tag	Terrestrivirus_11_9
			product	hypothetical protein Catovirus_2_197
			transl_table	11
10331	10918	CDS
			locus_tag	Terrestrivirus_11_10
			product	hypothetical protein Catovirus_2_196
			transl_table	11
11299	10943	CDS
			locus_tag	Terrestrivirus_11_11
			product	hypothetical protein Catovirus_2_195
			transl_table	11
12089	11394	CDS
			locus_tag	Terrestrivirus_11_12
			product	hypothetical protein Catovirus_2_194
			transl_table	11
12196	12732	CDS
			locus_tag	Terrestrivirus_11_13
			product	hypothetical protein
			transl_table	11
12818	13300	CDS
			locus_tag	Terrestrivirus_11_14
			product	hypothetical protein Catovirus_2_192
			transl_table	11
13393	14328	CDS
			locus_tag	Terrestrivirus_11_15
			product	eukaryotic translation initiation factor eIF-2 alpha
			transl_table	11
17452	14345	CDS
			locus_tag	Terrestrivirus_11_16
			product	DNA mismatch repair ATPase MutS
			transl_table	11
17957	18751	CDS
			locus_tag	Terrestrivirus_11_17
			product	hypothetical protein Catovirus_2_189
			transl_table	11
18878	18735	CDS
			locus_tag	Terrestrivirus_11_18
			product	hypothetical protein
			transl_table	11
19641	18958	CDS
			locus_tag	Terrestrivirus_11_19
			product	hypothetical protein Hokovirus_3_100
			transl_table	11
20497	19706	CDS
			locus_tag	Terrestrivirus_11_20
			product	hypothetical protein Catovirus_2_187
			transl_table	11
20687	21004	CDS
			locus_tag	Terrestrivirus_11_21
			product	hypothetical protein
			transl_table	11
22702	20999	CDS
			locus_tag	Terrestrivirus_11_22
			product	DEAD/SNF2-like helicase
			transl_table	11
22986	22792	CDS
			locus_tag	Terrestrivirus_11_23
			product	hypothetical protein
			transl_table	11
24313	22955	CDS
			locus_tag	Terrestrivirus_11_24
			product	Acyl-coenzyme A oxidase-like protein
			transl_table	11
24496	29580	CDS
			locus_tag	Terrestrivirus_11_25
			product	HrpA-like RNA helicase
			transl_table	11
29663	31282	CDS
			locus_tag	Terrestrivirus_11_26
			product	trypsin-like peptidase
			transl_table	11
31406	31837	CDS
			locus_tag	Terrestrivirus_11_27
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
34150	31853	CDS
			locus_tag	Terrestrivirus_11_28
			product	ankyrin repeat protein
			transl_table	11
34285	35133	CDS
			locus_tag	Terrestrivirus_11_29
			product	hypothetical protein
			transl_table	11
35801	35130	CDS
			locus_tag	Terrestrivirus_11_30
			product	hypothetical protein Catovirus_2_178
			transl_table	11
35961	37529	CDS
			locus_tag	Terrestrivirus_11_31
			product	prolyl-tRNA synthetase
			transl_table	11
37842	37573	CDS
			locus_tag	Terrestrivirus_11_32
			product	hypothetical protein
			transl_table	11
38015	38725	CDS
			locus_tag	Terrestrivirus_11_33
			product	endonuclease V
			transl_table	11
39126	38791	CDS
			locus_tag	Terrestrivirus_11_34
			product	hypothetical protein
			transl_table	11
39769	39224	CDS
			locus_tag	Terrestrivirus_11_35
			product	hypothetical protein
			transl_table	11
39884	40582	CDS
			locus_tag	Terrestrivirus_11_36
			product	DNA ligase III
			transl_table	11
40887	40579	CDS
			locus_tag	Terrestrivirus_11_37
			product	hypothetical protein
			transl_table	11
46142	46267	CDS
			locus_tag	Terrestrivirus_11_38
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_12
1662	1	CDS
			locus_tag	Terrestrivirus_12_1
			product	hypothetical protein
			transl_table	11
2078	2362	CDS
			locus_tag	Terrestrivirus_12_2
			product	hypothetical protein
			transl_table	11
2413	3282	CDS
			locus_tag	Terrestrivirus_12_3
			product	hypothetical protein
			transl_table	11
3398	4234	CDS
			locus_tag	Terrestrivirus_12_4
			product	hypothetical protein
			transl_table	11
6148	4619	CDS
			locus_tag	Terrestrivirus_12_5
			product	restriction endonuclease
			transl_table	11
7710	6211	CDS
			locus_tag	Terrestrivirus_12_6
			product	eukaryotic translation elongation factor 1 alpha
			transl_table	11
8792	7803	CDS
			locus_tag	Terrestrivirus_12_7
			product	hypothetical protein
			transl_table	11
9968	8865	CDS
			locus_tag	Terrestrivirus_12_8
			product	hypothetical protein
			transl_table	11
10625	10020	CDS
			locus_tag	Terrestrivirus_12_9
			product	nicotinate (nicotinamide) nucleotide adenylyltransferase
			transl_table	11
11842	10724	CDS
			locus_tag	Terrestrivirus_12_10
			product	hypothetical protein
			transl_table	11
12566	11922	CDS
			locus_tag	Terrestrivirus_12_11
			product	UBC-like protein
			transl_table	11
12731	13201	CDS
			locus_tag	Terrestrivirus_12_12
			product	hypothetical protein
			transl_table	11
13765	13226	CDS
			locus_tag	Terrestrivirus_12_13
			product	hypothetical protein SE17_33960
			transl_table	11
15067	13868	CDS
			locus_tag	Terrestrivirus_12_14
			product	hypothetical protein CAOG_00562
			transl_table	11
15236	15820	CDS
			locus_tag	Terrestrivirus_12_15
			product	hypothetical protein
			transl_table	11
17401	15827	CDS
			locus_tag	Terrestrivirus_12_16
			product	AAA family ATPase
			transl_table	11
17637	18059	CDS
			locus_tag	Terrestrivirus_12_17
			product	hypothetical protein
			transl_table	11
20079	18118	CDS
			locus_tag	Terrestrivirus_12_18
			product	zincin
			transl_table	11
20845	20177	CDS
			locus_tag	Terrestrivirus_12_19
			product	hypothetical protein
			transl_table	11
21161	20940	CDS
			locus_tag	Terrestrivirus_12_20
			product	hypothetical protein
			transl_table	11
25115	21237	CDS
			locus_tag	Terrestrivirus_12_21
			product	glycosyltransferase
			transl_table	11
27521	25254	CDS
			locus_tag	Terrestrivirus_12_22
			product	hypothetical protein A3E83_03615
			transl_table	11
28580	27741	CDS
			locus_tag	Terrestrivirus_12_23
			product	BRO-N domain containing protein
			transl_table	11
29730	28657	CDS
			locus_tag	Terrestrivirus_12_24
			product	hypothetical protein
			transl_table	11
29944	30519	CDS
			locus_tag	Terrestrivirus_12_25
			product	alpha-ketoglutarate-dependent dioxygenase AlkB
			transl_table	11
30658	31488	CDS
			locus_tag	Terrestrivirus_12_26
			product	hypothetical protein
			transl_table	11
31568	32365	CDS
			locus_tag	Terrestrivirus_12_27
			product	hypothetical protein
			transl_table	11
32908	32402	CDS
			locus_tag	Terrestrivirus_12_28
			product	tail fiber domain-containing protein
			transl_table	11
33110	34033	CDS
			locus_tag	Terrestrivirus_12_29
			product	hypothetical protein
			transl_table	11
35256	34060	CDS
			locus_tag	Terrestrivirus_12_30
			product	hypothetical protein
			transl_table	11
35854	35330	CDS
			locus_tag	Terrestrivirus_12_31
			product	hypothetical protein
			transl_table	11
36221	36919	CDS
			locus_tag	Terrestrivirus_12_32
			product	hypothetical protein
			transl_table	11
36997	37374	CDS
			locus_tag	Terrestrivirus_12_33
			product	hypothetical protein
			transl_table	11
38680	37385	CDS
			locus_tag	Terrestrivirus_12_34
			product	hypothetical protein
			transl_table	11
38830	39144	CDS
			locus_tag	Terrestrivirus_12_35
			product	hypothetical protein
			transl_table	11
39326	39466	CDS
			locus_tag	Terrestrivirus_12_36
			product	hypothetical protein
			transl_table	11
39884	39624	CDS
			locus_tag	Terrestrivirus_12_37
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_13
253	2	CDS
			locus_tag	Terrestrivirus_13_1
			product	hypothetical protein
			transl_table	11
374	2983	CDS
			locus_tag	Terrestrivirus_13_2
			product	hypothetical protein
			transl_table	11
3077	3631	CDS
			locus_tag	Terrestrivirus_13_3
			product	hypothetical protein
			transl_table	11
3759	5171	CDS
			locus_tag	Terrestrivirus_13_4
			product	hypothetical protein KAFR_0A00940
			transl_table	11
5243	6781	CDS
			locus_tag	Terrestrivirus_13_5
			product	HNH endonuclease
			transl_table	11
7635	6790	CDS
			locus_tag	Terrestrivirus_13_6
			product	hypothetical protein
			transl_table	11
8117	7722	CDS
			locus_tag	Terrestrivirus_13_7
			product	hypothetical protein
			transl_table	11
8560	8946	CDS
			locus_tag	Terrestrivirus_13_8
			product	hypothetical protein
			transl_table	11
9005	9742	CDS
			locus_tag	Terrestrivirus_13_9
			product	hypothetical protein
			transl_table	11
10719	9754	CDS
			locus_tag	Terrestrivirus_13_10
			product	hypothetical protein Catovirus_1_431
			transl_table	11
10899	11747	CDS
			locus_tag	Terrestrivirus_13_11
			product	hypothetical protein
			transl_table	11
12395	11784	CDS
			locus_tag	Terrestrivirus_13_12
			product	hypothetical protein
			transl_table	11
12681	13430	CDS
			locus_tag	Terrestrivirus_13_13
			product	hypothetical protein
			transl_table	11
14304	13456	CDS
			locus_tag	Terrestrivirus_13_14
			product	hypothetical protein
			transl_table	11
14455	15651	CDS
			locus_tag	Terrestrivirus_13_15
			product	hypothetical protein
			transl_table	11
15736	16167	CDS
			locus_tag	Terrestrivirus_13_16
			product	hypothetical protein
			transl_table	11
16272	16691	CDS
			locus_tag	Terrestrivirus_13_17
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_14
1292	63	CDS
			locus_tag	Terrestrivirus_14_1
			product	hypothetical protein PBRA_002699
			transl_table	11
1473	2027	CDS
			locus_tag	Terrestrivirus_14_2
			product	hypothetical protein
			transl_table	11
4256	2031	CDS
			locus_tag	Terrestrivirus_14_3
			product	hypothetical protein
			transl_table	11
4462	4761	CDS
			locus_tag	Terrestrivirus_14_4
			product	hypothetical protein
			transl_table	11
7573	4796	CDS
			locus_tag	Terrestrivirus_14_5
			product	XRN 5'-3' exonuclease
			transl_table	11
8331	7663	CDS
			locus_tag	Terrestrivirus_14_6
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_15
3	722	CDS
			locus_tag	Terrestrivirus_15_1
			product	hypothetical protein AK812_SmicGene647
			transl_table	11
1408	728	CDS
			locus_tag	Terrestrivirus_15_2
			product	putative amino acid/polyamine transporter I
			transl_table	11
1955	1512	CDS
			locus_tag	Terrestrivirus_15_3
			product	hypothetical protein
			transl_table	11
3148	2093	CDS
			locus_tag	Terrestrivirus_15_4
			product	hypothetical protein
			transl_table	11
3774	3241	CDS
			locus_tag	Terrestrivirus_15_5
			product	hypothetical protein
			transl_table	11
4308	3853	CDS
			locus_tag	Terrestrivirus_15_6
			product	hypothetical protein
			transl_table	11
5106	4399	CDS
			locus_tag	Terrestrivirus_15_7
			product	FAD-binding oxidoreductase
			transl_table	11
>Terrestrivirus_16
802	2	CDS
			locus_tag	Terrestrivirus_16_1
			product	glycosyltransferase family 25
			transl_table	11
867	2003	CDS
			locus_tag	Terrestrivirus_16_2
			product	hypothetical protein CVU91_02440
			transl_table	11
4244	4146	CDS
			locus_tag	Terrestrivirus_16_3
			product	hypothetical protein
			transl_table	11
4367	4561	CDS
			locus_tag	Terrestrivirus_16_4
			product	tryptophan--tRNA ligase, putative
			transl_table	11
>Terrestrivirus_17
202	1539	CDS
			locus_tag	Terrestrivirus_17_1
			product	beta-lactamase superfamily domain
			transl_table	11
1709	2605	CDS
			locus_tag	Terrestrivirus_17_2
			product	hypothetical protein
			transl_table	11
3628	2651	CDS
			locus_tag	Terrestrivirus_17_3
			product	peptidase
			transl_table	11
>Terrestrivirus_18
3	1160	CDS
			locus_tag	Terrestrivirus_18_1
			product	hypothetical protein
			transl_table	11
1273	2175	CDS
			locus_tag	Terrestrivirus_18_2
			product	hypothetical protein
			transl_table	11
2547	2185	CDS
			locus_tag	Terrestrivirus_18_3
			product	hypothetical protein
			transl_table	11
2976	2626	CDS
			locus_tag	Terrestrivirus_18_4
			product	hypothetical protein
			transl_table	11
3469	3573	CDS
			locus_tag	Terrestrivirus_18_5
			product	hypothetical protein
			transl_table	11
>Terrestrivirus_19
1	1269	CDS
			locus_tag	Terrestrivirus_19_1
			product	hypothetical protein
			transl_table	11
2189	1698	CDS
			locus_tag	Terrestrivirus_19_2
			product	hypothetical protein
			transl_table	11
