>Harvfovirus_1
429	1	CDS
			locus_tag	Harvfovirus_1_1
			product	alpha/beta hydrolase
			transl_table	11
501	1433	CDS
			locus_tag	Harvfovirus_1_2
			product	tRNase Z TRZ1
			transl_table	11
2356	1430	CDS
			locus_tag	Harvfovirus_1_3
			product	hypothetical protein
			transl_table	11
2433	3215	CDS
			locus_tag	Harvfovirus_1_4
			product	hypothetical protein
			transl_table	11
4440	3172	CDS
			locus_tag	Harvfovirus_1_5
			product	MULTISPECIES: glycosyltransferase
			transl_table	11
5623	4403	CDS
			locus_tag	Harvfovirus_1_6
			product	response regulator
			transl_table	11
5849	6001	CDS
			locus_tag	Harvfovirus_1_7
			product	hypothetical protein
			transl_table	11
6037	6453	CDS
			locus_tag	Harvfovirus_1_8
			product	hypothetical protein
			transl_table	11
6507	7241	CDS
			locus_tag	Harvfovirus_1_9
			product	CHY zinc finger family protein
			transl_table	11
7244	8443	CDS
			locus_tag	Harvfovirus_1_10
			product	hypothetical protein OBBRIDRAFT_795550
			transl_table	11
8507	8938	CDS
			locus_tag	Harvfovirus_1_11
			product	hypothetical protein
			transl_table	11
10248	8923	CDS
			locus_tag	Harvfovirus_1_12
			product	DEAD/SNF2-like helicase
			transl_table	11
10346	11797	CDS
			locus_tag	Harvfovirus_1_13
			product	cytochrome P450 704B1
			transl_table	11
11864	12604	CDS
			locus_tag	Harvfovirus_1_14
			product	hypothetical protein
			transl_table	11
12667	13296	CDS
			locus_tag	Harvfovirus_1_15
			product	hypothetical protein
			transl_table	11
13302	13574	CDS
			locus_tag	Harvfovirus_1_16
			product	hypothetical protein
			transl_table	11
13600	14181	CDS
			locus_tag	Harvfovirus_1_17
			product	hypothetical protein
			transl_table	11
14493	14621	CDS
			locus_tag	Harvfovirus_1_18
			product	hypothetical protein
			transl_table	11
14793	15548	CDS
			locus_tag	Harvfovirus_1_19
			product	fatty acid elongase, putative
			transl_table	11
15598	16461	CDS
			locus_tag	Harvfovirus_1_20
			product	PREDICTED: ubiquitin carboxyl-terminal hydrolase 2-like
			transl_table	11
16517	16834	CDS
			locus_tag	Harvfovirus_1_21
			product	hypothetical protein
			transl_table	11
17080	17505	CDS
			locus_tag	Harvfovirus_1_22
			product	methyltransferase domain-containing protein
			transl_table	11
17558	18475	CDS
			locus_tag	Harvfovirus_1_23
			product	hypothetical protein
			transl_table	11
18538	20946	CDS
			locus_tag	Harvfovirus_1_24
			product	prohibitin domain-containing protein
			transl_table	11
21004	21576	CDS
			locus_tag	Harvfovirus_1_25
			product	hypothetical protein
			transl_table	11
21645	22241	CDS
			locus_tag	Harvfovirus_1_26
			product	hypothetical protein
			transl_table	11
22318	25863	CDS
			locus_tag	Harvfovirus_1_27
			product	RxLR-like protein
			transl_table	11
27732	25864	CDS
			locus_tag	Harvfovirus_1_28
			product	calcineurin-like phosphoesterase
			transl_table	11
28895	27756	CDS
			locus_tag	Harvfovirus_1_29
			product	fatty acyl delta4 desaturase
			transl_table	11
30582	28945	CDS
			locus_tag	Harvfovirus_1_30
			product	hypothetical protein
			transl_table	11
31472	30747	CDS
			locus_tag	Harvfovirus_1_31
			product	exonuclease
			transl_table	11
31594	32616	CDS
			locus_tag	Harvfovirus_1_32
			product	glutamine synthetase
			transl_table	11
33811	32618	CDS
			locus_tag	Harvfovirus_1_33
			product	hypothetical protein
			transl_table	11
33931	36972	CDS
			locus_tag	Harvfovirus_1_34
			product	hypothetical protein
			transl_table	11
37032	37328	CDS
			locus_tag	Harvfovirus_1_35
			product	hypothetical protein
			transl_table	11
38298	37330	CDS
			locus_tag	Harvfovirus_1_36
			product	GDP-mannose 6-dehydrogenase 2
			transl_table	11
38479	39123	CDS
			locus_tag	Harvfovirus_1_37
			product	hypothetical protein
			transl_table	11
39188	42523	CDS
			locus_tag	Harvfovirus_1_38
			product	hypothetical protein
			transl_table	11
42558	43181	CDS
			locus_tag	Harvfovirus_1_39
			product	hypothetical protein
			transl_table	11
44188	43178	CDS
			locus_tag	Harvfovirus_1_40
			product	2OG-FeII oxygenase
			transl_table	11
44493	44227	CDS
			locus_tag	Harvfovirus_1_41
			product	hypothetical protein
			transl_table	11
45063	44578	CDS
			locus_tag	Harvfovirus_1_42
			product	hypothetical protein
			transl_table	11
46079	45099	CDS
			locus_tag	Harvfovirus_1_43
			product	hypothetical protein
			transl_table	11
47837	46113	CDS
			locus_tag	Harvfovirus_1_44
			product	hypothetical protein
			transl_table	11
48664	47867	CDS
			locus_tag	Harvfovirus_1_45
			product	hypothetical protein
			transl_table	11
49362	48721	CDS
			locus_tag	Harvfovirus_1_46
			product	hypothetical protein
			transl_table	11
49895	49395	CDS
			locus_tag	Harvfovirus_1_47
			product	MULTISPECIES: nucleoside deaminase
			transl_table	11
50034	50567	CDS
			locus_tag	Harvfovirus_1_48
			product	hypothetical protein
			transl_table	11
50603	51253	CDS
			locus_tag	Harvfovirus_1_49
			product	hypothetical protein A2161_04525
			transl_table	11
51301	52914	CDS
			locus_tag	Harvfovirus_1_50
			product	hypothetical protein Catovirus_2_104
			transl_table	11
52978	53982	CDS
			locus_tag	Harvfovirus_1_51
			product	hypothetical protein
			transl_table	11
56375	54315	CDS
			locus_tag	Harvfovirus_1_52
			product	hypothetical protein Indivirus_1_31
			transl_table	11
56702	57382	CDS
			locus_tag	Harvfovirus_1_53
			product	hypothetical protein Catovirus_2_105
			transl_table	11
59479	57368	CDS
			locus_tag	Harvfovirus_1_54
			product	hypothetical protein
			transl_table	11
60994	59543	CDS
			locus_tag	Harvfovirus_1_55
			product	homospermidine synthase
			transl_table	11
61221	61027	CDS
			locus_tag	Harvfovirus_1_56
			product	hypothetical protein
			transl_table	11
62161	61241	CDS
			locus_tag	Harvfovirus_1_57
			product	Hypothetical protein ORPV_249
			transl_table	11
63245	62244	CDS
			locus_tag	Harvfovirus_1_58
			product	hypothetical protein
			transl_table	11
63306	63653	CDS
			locus_tag	Harvfovirus_1_59
			product	hypothetical protein
			transl_table	11
64320	63655	CDS
			locus_tag	Harvfovirus_1_60
			product	hypothetical protein
			transl_table	11
64446	65612	CDS
			locus_tag	Harvfovirus_1_61
			product	hypothetical protein
			transl_table	11
66740	65574	CDS
			locus_tag	Harvfovirus_1_62
			product	hypothetical protein
			transl_table	11
66862	67206	CDS
			locus_tag	Harvfovirus_1_63
			product	hypothetical protein
			transl_table	11
67472	67161	CDS
			locus_tag	Harvfovirus_1_64
			product	hypothetical protein
			transl_table	11
67614	67946	CDS
			locus_tag	Harvfovirus_1_65
			product	hypothetical protein
			transl_table	11
67989	71216	CDS
			locus_tag	Harvfovirus_1_66
			product	hypothetical protein
			transl_table	11
71487	71242	CDS
			locus_tag	Harvfovirus_1_67
			product	hypothetical protein
			transl_table	11
72505	71501	CDS
			locus_tag	Harvfovirus_1_68
			product	hypothetical protein Klosneuvirus_3_188
			transl_table	11
72630	73142	CDS
			locus_tag	Harvfovirus_1_69
			product	hypothetical protein
			transl_table	11
73202	75094	CDS
			locus_tag	Harvfovirus_1_70
			product	hypothetical protein ACD_30C00112G0001, partial
			transl_table	11
75133	76083	CDS
			locus_tag	Harvfovirus_1_71
			product	hypothetical protein
			transl_table	11
76492	76061	CDS
			locus_tag	Harvfovirus_1_72
			product	hypothetical protein
			transl_table	11
76572	77204	CDS
			locus_tag	Harvfovirus_1_73
			product	putative ORFan
			transl_table	11
77271	78245	CDS
			locus_tag	Harvfovirus_1_74
			product	hypothetical protein
			transl_table	11
78276	79142	CDS
			locus_tag	Harvfovirus_1_75
			product	bifunctional methylenetetrahydrofolate dehydrogenase/methenyltetrahydrofolate cyclohydrolase
			transl_table	11
79295	80626	CDS
			locus_tag	Harvfovirus_1_76
			product	hypothetical protein
			transl_table	11
80759	81928	CDS
			locus_tag	Harvfovirus_1_77
			product	hypothetical protein
			transl_table	11
82291	81890	CDS
			locus_tag	Harvfovirus_1_78
			product	hypothetical protein
			transl_table	11
82417	83835	CDS
			locus_tag	Harvfovirus_1_79
			product	hypothetical protein
			transl_table	11
84819	83815	CDS
			locus_tag	Harvfovirus_1_80
			product	hypothetical protein
			transl_table	11
84900	86081	CDS
			locus_tag	Harvfovirus_1_81
			product	hypothetical protein
			transl_table	11
86104	86442	CDS
			locus_tag	Harvfovirus_1_82
			product	hypothetical protein
			transl_table	11
87092	86448	CDS
			locus_tag	Harvfovirus_1_83
			product	hypothetical protein
			transl_table	11
88099	87155	CDS
			locus_tag	Harvfovirus_1_84
			product	hypothetical protein
			transl_table	11
88192	88905	CDS
			locus_tag	Harvfovirus_1_85
			product	hypothetical protein
			transl_table	11
88966	89553	CDS
			locus_tag	Harvfovirus_1_86
			product	hypothetical protein
			transl_table	11
89525	90223	CDS
			locus_tag	Harvfovirus_1_87
			product	hypothetical protein
			transl_table	11
90283	90930	CDS
			locus_tag	Harvfovirus_1_88
			product	hypothetical protein TCSYLVIO_005366
			transl_table	11
90983	92080	CDS
			locus_tag	Harvfovirus_1_89
			product	hypothetical protein
			transl_table	11
92481	92083	CDS
			locus_tag	Harvfovirus_1_90
			product	hypothetical protein
			transl_table	11
92445	93290	CDS
			locus_tag	Harvfovirus_1_91
			product	uncharacterized protein LOC110466162
			transl_table	11
93508	93287	CDS
			locus_tag	Harvfovirus_1_92
			product	hypothetical protein
			transl_table	11
94451	93543	CDS
			locus_tag	Harvfovirus_1_93
			product	hypothetical protein
			transl_table	11
95671	94508	CDS
			locus_tag	Harvfovirus_1_94
			product	hypothetical protein
			transl_table	11
96142	95735	CDS
			locus_tag	Harvfovirus_1_95
			product	hypothetical protein
			transl_table	11
>Harvfovirus_2
179	3	CDS
			locus_tag	Harvfovirus_2_1
			product	hypothetical protein
			transl_table	11
1109	360	CDS
			locus_tag	Harvfovirus_2_2
			product	ubiquitin family protein
			transl_table	11
2932	1166	CDS
			locus_tag	Harvfovirus_2_3
			product	hypothetical protein
			transl_table	11
3055	3741	CDS
			locus_tag	Harvfovirus_2_4
			product	hypothetical protein
			transl_table	11
5054	3759	CDS
			locus_tag	Harvfovirus_2_5
			product	hypothetical protein
			transl_table	11
5181	5639	CDS
			locus_tag	Harvfovirus_2_6
			product	hypothetical protein OlV2_214
			transl_table	11
5700	7715	CDS
			locus_tag	Harvfovirus_2_7
			product	von willebrand factor type A domain protein, putative
			transl_table	11
7775	8179	CDS
			locus_tag	Harvfovirus_2_8
			product	hypothetical protein
			transl_table	11
8230	10047	CDS
			locus_tag	Harvfovirus_2_9
			product	hypothetical protein
			transl_table	11
10099	10962	CDS
			locus_tag	Harvfovirus_2_10
			product	hypothetical protein
			transl_table	11
11015	11959	CDS
			locus_tag	Harvfovirus_2_11
			product	RNA ligase
			transl_table	11
12200	12676	CDS
			locus_tag	Harvfovirus_2_12
			product	hypothetical protein
			transl_table	11
14114	12669	CDS
			locus_tag	Harvfovirus_2_13
			product	hypothetical protein
			transl_table	11
14559	14158	CDS
			locus_tag	Harvfovirus_2_14
			product	hypothetical protein
			transl_table	11
14638	15225	CDS
			locus_tag	Harvfovirus_2_15
			product	hypothetical protein
			transl_table	11
15291	16157	CDS
			locus_tag	Harvfovirus_2_16
			product	hypothetical protein
			transl_table	11
16211	17317	CDS
			locus_tag	Harvfovirus_2_17
			product	hypothetical protein
			transl_table	11
17372	17776	CDS
			locus_tag	Harvfovirus_2_18
			product	hypothetical protein
			transl_table	11
18234	17773	CDS
			locus_tag	Harvfovirus_2_19
			product	hypothetical protein O9G_002264
			transl_table	11
18354	19892	CDS
			locus_tag	Harvfovirus_2_20
			product	histidine tRNA synthetase
			transl_table	11
19941	21098	CDS
			locus_tag	Harvfovirus_2_21
			product	hypothetical protein
			transl_table	11
21116	22579	CDS
			locus_tag	Harvfovirus_2_22
			product	hypothetical protein
			transl_table	11
23762	22563	CDS
			locus_tag	Harvfovirus_2_23
			product	hypothetical protein BGO43_02360
			transl_table	11
23902	24918	CDS
			locus_tag	Harvfovirus_2_24
			product	hypothetical protein
			transl_table	11
24933	26297	CDS
			locus_tag	Harvfovirus_2_25
			product	p13-like protein
			transl_table	11
27135	26272	CDS
			locus_tag	Harvfovirus_2_26
			product	hypothetical protein
			transl_table	11
27169	28368	CDS
			locus_tag	Harvfovirus_2_27
			product	hypothetical protein
			transl_table	11
28442	29758	CDS
			locus_tag	Harvfovirus_2_28
			product	hypothetical protein
			transl_table	11
30838	29762	CDS
			locus_tag	Harvfovirus_2_29
			product	hypothetical protein
			transl_table	11
32122	30860	CDS
			locus_tag	Harvfovirus_2_30
			product	hypothetical protein
			transl_table	11
32256	33194	CDS
			locus_tag	Harvfovirus_2_31
			product	hypothetical protein
			transl_table	11
33273	34667	CDS
			locus_tag	Harvfovirus_2_32
			product	hypothetical protein
			transl_table	11
34844	36193	CDS
			locus_tag	Harvfovirus_2_33
			product	zinc finger SWIM domain protein
			transl_table	11
36931	36152	CDS
			locus_tag	Harvfovirus_2_34
			product	TNF receptor-associated factor 5-like
			transl_table	11
39909	36988	CDS
			locus_tag	Harvfovirus_2_35
			product	hypothetical protein
			transl_table	11
40052	40891	CDS
			locus_tag	Harvfovirus_2_36
			product	hypothetical protein
			transl_table	11
41675	40896	CDS
			locus_tag	Harvfovirus_2_37
			product	hypothetical protein
			transl_table	11
42465	41713	CDS
			locus_tag	Harvfovirus_2_38
			product	hypothetical protein
			transl_table	11
43653	42526	CDS
			locus_tag	Harvfovirus_2_39
			product	hypothetical protein
			transl_table	11
43739	44839	CDS
			locus_tag	Harvfovirus_2_40
			product	hypothetical protein
			transl_table	11
45129	44866	CDS
			locus_tag	Harvfovirus_2_41
			product	hypothetical protein
			transl_table	11
45193	46974	CDS
			locus_tag	Harvfovirus_2_42
			product	hypothetical protein
			transl_table	11
46985	47236	CDS
			locus_tag	Harvfovirus_2_43
			product	hypothetical protein
			transl_table	11
47297	48736	CDS
			locus_tag	Harvfovirus_2_44
			product	glutamyl-tRNA synthetase
			transl_table	11
48800	49723	CDS
			locus_tag	Harvfovirus_2_45
			product	hypothetical protein
			transl_table	11
50388	49720	CDS
			locus_tag	Harvfovirus_2_46
			product	hypothetical protein
			transl_table	11
51051	50443	CDS
			locus_tag	Harvfovirus_2_47
			product	hypothetical protein
			transl_table	11
51071	51241	CDS
			locus_tag	Harvfovirus_2_48
			product	hypothetical protein
			transl_table	11
51258	51677	CDS
			locus_tag	Harvfovirus_2_49
			product	hypothetical protein
			transl_table	11
51729	52622	CDS
			locus_tag	Harvfovirus_2_50
			product	hypothetical protein
			transl_table	11
53344	52643	CDS
			locus_tag	Harvfovirus_2_51
			product	hypothetical protein
			transl_table	11
53437	53838	CDS
			locus_tag	Harvfovirus_2_52
			product	hypothetical protein
			transl_table	11
54798	53833	CDS
			locus_tag	Harvfovirus_2_53
			product	hypothetical protein
			transl_table	11
56244	54859	CDS
			locus_tag	Harvfovirus_2_54
			product	hypothetical protein
			transl_table	11
56374	57210	CDS
			locus_tag	Harvfovirus_2_55
			product	hypothetical protein
			transl_table	11
57505	57218	CDS
			locus_tag	Harvfovirus_2_56
			product	hypothetical protein
			transl_table	11
57636	58277	CDS
			locus_tag	Harvfovirus_2_57
			product	hypothetical protein
			transl_table	11
58679	58245	CDS
			locus_tag	Harvfovirus_2_58
			product	hypothetical protein
			transl_table	11
58805	59539	CDS
			locus_tag	Harvfovirus_2_59
			product	hypothetical protein
			transl_table	11
59588	61177	CDS
			locus_tag	Harvfovirus_2_60
			product	hypothetical protein
			transl_table	11
61902	61174	CDS
			locus_tag	Harvfovirus_2_61
			product	hypothetical protein
			transl_table	11
62293	63369	CDS
			locus_tag	Harvfovirus_2_62
			product	hypothetical protein Indivirus_7_20
			transl_table	11
64710	63370	CDS
			locus_tag	Harvfovirus_2_63
			product	hypothetical protein
			transl_table	11
65284	64766	CDS
			locus_tag	Harvfovirus_2_64
			product	hypothetical protein
			transl_table	11
66096	65356	CDS
			locus_tag	Harvfovirus_2_65
			product	hypothetical protein
			transl_table	11
66198	66506	CDS
			locus_tag	Harvfovirus_2_66
			product	hypothetical protein
			transl_table	11
66556	66873	CDS
			locus_tag	Harvfovirus_2_67
			product	hypothetical protein
			transl_table	11
66985	67578	CDS
			locus_tag	Harvfovirus_2_68
			product	transposase
			transl_table	11
68003	70060	CDS
			locus_tag	Harvfovirus_2_69
			product	hypothetical protein Indivirus_1_31
			transl_table	11
70542	70781	CDS
			locus_tag	Harvfovirus_2_70
			product	hypothetical protein
			transl_table	11
71144	71935	CDS
			locus_tag	Harvfovirus_2_71
			product	hypothetical protein C0198_01670
			transl_table	11
72007	73284	CDS
			locus_tag	Harvfovirus_2_72
			product	TRAF-type zinc finger protein
			transl_table	11
74400	73327	CDS
			locus_tag	Harvfovirus_2_73
			product	serine/threonine protein kinase
			transl_table	11
74792	74472	CDS
			locus_tag	Harvfovirus_2_74
			product	hypothetical protein
			transl_table	11
75705	74848	CDS
			locus_tag	Harvfovirus_2_75
			product	hypothetical protein COLO4_19929
			transl_table	11
76671	75769	CDS
			locus_tag	Harvfovirus_2_76
			product	hypothetical protein
			transl_table	11
77385	76723	CDS
			locus_tag	Harvfovirus_2_77
			product	hypothetical protein
			transl_table	11
78356	77475	CDS
			locus_tag	Harvfovirus_2_78
			product	hypothetical protein
			transl_table	11
79327	78428	CDS
			locus_tag	Harvfovirus_2_79
			product	hypothetical protein
			transl_table	11
79976	80188	CDS
			locus_tag	Harvfovirus_2_80
			product	hypothetical protein
			transl_table	11
80200	80412	CDS
			locus_tag	Harvfovirus_2_81
			product	hypothetical protein
			transl_table	11
80424	80687	CDS
			locus_tag	Harvfovirus_2_82
			product	hypothetical protein
			transl_table	11
80684	80911	CDS
			locus_tag	Harvfovirus_2_83
			product	hypothetical protein
			transl_table	11
81199	82518	CDS
			locus_tag	Harvfovirus_2_84
			product	hypothetical protein
			transl_table	11
83654	83815	CDS
			locus_tag	Harvfovirus_2_85
			product	hypothetical protein
			transl_table	11
>Harvfovirus_3
1080	19	CDS
			locus_tag	Harvfovirus_3_1
			product	hypothetical protein
			transl_table	11
1232	1672	CDS
			locus_tag	Harvfovirus_3_2
			product	hypothetical protein
			transl_table	11
1709	2968	CDS
			locus_tag	Harvfovirus_3_3
			product	hypothetical protein
			transl_table	11
4183	2948	CDS
			locus_tag	Harvfovirus_3_4
			product	hypothetical protein A2472_07695
			transl_table	11
5629	4226	CDS
			locus_tag	Harvfovirus_3_5
			product	hypothetical protein
			transl_table	11
6692	5670	CDS
			locus_tag	Harvfovirus_3_6
			product	hypothetical protein
			transl_table	11
6835	7761	CDS
			locus_tag	Harvfovirus_3_7
			product	hypothetical protein
			transl_table	11
8370	7744	CDS
			locus_tag	Harvfovirus_3_8
			product	hypothetical protein
			transl_table	11
9642	8425	CDS
			locus_tag	Harvfovirus_3_9
			product	hypothetical protein
			transl_table	11
11231	9696	CDS
			locus_tag	Harvfovirus_3_10
			product	putative FAD-linked oxidoreductase YgaK
			transl_table	11
11278	11829	CDS
			locus_tag	Harvfovirus_3_11
			product	oxidoreductase, 2OG-Fe(II) oxygenase
			transl_table	11
12538	11819	CDS
			locus_tag	Harvfovirus_3_12
			product	hypothetical protein
			transl_table	11
12890	13525	CDS
			locus_tag	Harvfovirus_3_13
			product	hypothetical protein
			transl_table	11
13578	14741	CDS
			locus_tag	Harvfovirus_3_14
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
14793	15887	CDS
			locus_tag	Harvfovirus_3_15
			product	hypothetical protein
			transl_table	11
16221	16006	CDS
			locus_tag	Harvfovirus_3_16
			product	hypothetical protein
			transl_table	11
17006	16287	CDS
			locus_tag	Harvfovirus_3_17
			product	hypothetical protein
			transl_table	11
17119	17316	CDS
			locus_tag	Harvfovirus_3_18
			product	hypothetical protein
			transl_table	11
17323	17481	CDS
			locus_tag	Harvfovirus_3_19
			product	hypothetical protein Hokovirus_2_45
			transl_table	11
17605	18039	CDS
			locus_tag	Harvfovirus_3_20
			product	hypothetical protein
			transl_table	11
18087	19781	CDS
			locus_tag	Harvfovirus_3_21
			product	hypothetical protein
			transl_table	11
22001	22756	CDS
			locus_tag	Harvfovirus_3_22
			product	hypothetical protein
			transl_table	11
22796	23926	CDS
			locus_tag	Harvfovirus_3_23
			product	hypothetical protein
			transl_table	11
24976	24041	CDS
			locus_tag	Harvfovirus_3_24
			product	hypothetical protein Catovirus_1_66
			transl_table	11
26501	24999	CDS
			locus_tag	Harvfovirus_3_25
			product	hypothetical protein
			transl_table	11
27571	26636	CDS
			locus_tag	Harvfovirus_3_26
			product	hypothetical protein
			transl_table	11
27701	28219	CDS
			locus_tag	Harvfovirus_3_27
			product	Homeobox protein 4
			transl_table	11
28274	28909	CDS
			locus_tag	Harvfovirus_3_28
			product	hypothetical protein
			transl_table	11
28890	29138	CDS
			locus_tag	Harvfovirus_3_29
			product	hypothetical protein
			transl_table	11
31644	29365	CDS
			locus_tag	Harvfovirus_3_30
			product	hypothetical protein Catovirus_1_615
			transl_table	11
31753	31634	CDS
			locus_tag	Harvfovirus_3_31
			product	hypothetical protein
			transl_table	11
31982	31737	CDS
			locus_tag	Harvfovirus_3_32
			product	hypothetical protein A3Q56_07625, partial
			transl_table	11
32135	32503	CDS
			locus_tag	Harvfovirus_3_33
			product	hypothetical protein OXYTRI_08399 (macronuclear)
			transl_table	11
33990	32500	CDS
			locus_tag	Harvfovirus_3_34
			product	aminotransferase class I/II-fold pyridoxal phosphate-dependent enzyme
			transl_table	11
34746	34012	CDS
			locus_tag	Harvfovirus_3_35
			product	hypothetical protein LOTGIDRAFT_108079
			transl_table	11
34834	35379	CDS
			locus_tag	Harvfovirus_3_36
			product	hypothetical protein
			transl_table	11
36821	35373	CDS
			locus_tag	Harvfovirus_3_37
			product	hypothetical protein
			transl_table	11
37389	37132	CDS
			locus_tag	Harvfovirus_3_38
			product	hypothetical protein
			transl_table	11
37521	37997	CDS
			locus_tag	Harvfovirus_3_39
			product	hypothetical protein AXG93_4486s1020
			transl_table	11
38005	38784	CDS
			locus_tag	Harvfovirus_3_40
			product	hypothetical protein
			transl_table	11
38833	39186	CDS
			locus_tag	Harvfovirus_3_41
			product	hypothetical protein
			transl_table	11
39840	39196	CDS
			locus_tag	Harvfovirus_3_42
			product	hypothetical protein
			transl_table	11
39887	40456	CDS
			locus_tag	Harvfovirus_3_43
			product	hypothetical protein
			transl_table	11
40517	41824	CDS
			locus_tag	Harvfovirus_3_44
			product	hypothetical protein
			transl_table	11
41824	42441	CDS
			locus_tag	Harvfovirus_3_45
			product	hypothetical protein
			transl_table	11
42777	42442	CDS
			locus_tag	Harvfovirus_3_46
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
42874	43389	CDS
			locus_tag	Harvfovirus_3_47
			product	hypothetical protein
			transl_table	11
43790	43497	CDS
			locus_tag	Harvfovirus_3_48
			product	hypothetical protein
			transl_table	11
43844	44422	CDS
			locus_tag	Harvfovirus_3_49
			product	hypothetical protein
			transl_table	11
44435	45019	CDS
			locus_tag	Harvfovirus_3_50
			product	DUF4291 domain-containing protein
			transl_table	11
45104	45574	CDS
			locus_tag	Harvfovirus_3_51
			product	hypothetical protein
			transl_table	11
45722	46444	CDS
			locus_tag	Harvfovirus_3_52
			product	hypothetical protein
			transl_table	11
46929	46468	CDS
			locus_tag	Harvfovirus_3_53
			product	hypothetical protein
			transl_table	11
47050	47160	CDS
			locus_tag	Harvfovirus_3_54
			product	hypothetical protein
			transl_table	11
48872	47163	CDS
			locus_tag	Harvfovirus_3_55
			product	hypothetical protein
			transl_table	11
49695	49090	CDS
			locus_tag	Harvfovirus_3_56
			product	hypothetical protein
			transl_table	11
50242	49763	CDS
			locus_tag	Harvfovirus_3_57
			product	2,3-bisphosphoglycerate-dependent phosphoglycerate mutase
			transl_table	11
51409	50318	CDS
			locus_tag	Harvfovirus_3_58
			product	hypothetical protein
			transl_table	11
52135	51467	CDS
			locus_tag	Harvfovirus_3_59
			product	hypothetical protein
			transl_table	11
52934	52287	CDS
			locus_tag	Harvfovirus_3_60
			product	hypothetical protein
			transl_table	11
53257	53069	CDS
			locus_tag	Harvfovirus_3_61
			product	hypothetical protein
			transl_table	11
53383	53895	CDS
			locus_tag	Harvfovirus_3_62
			product	hypothetical protein Y032_0025g1181
			transl_table	11
54774	53884	CDS
			locus_tag	Harvfovirus_3_63
			product	hypothetical protein
			transl_table	11
55018	54929	CDS
			locus_tag	Harvfovirus_3_64
			product	hypothetical protein
			transl_table	11
55303	56007	CDS
			locus_tag	Harvfovirus_3_65
			product	hypothetical protein
			transl_table	11
56820	56011	CDS
			locus_tag	Harvfovirus_3_66
			product	hypothetical protein
			transl_table	11
56938	57378	CDS
			locus_tag	Harvfovirus_3_67
			product	hypothetical protein
			transl_table	11
58405	57347	CDS
			locus_tag	Harvfovirus_3_68
			product	hypothetical protein
			transl_table	11
58404	58745	CDS
			locus_tag	Harvfovirus_3_69
			product	hypothetical protein
			transl_table	11
58801	59400	CDS
			locus_tag	Harvfovirus_3_70
			product	hypothetical protein DCAR_002412
			transl_table	11
59397	59705	CDS
			locus_tag	Harvfovirus_3_71
			product	hypothetical protein
			transl_table	11
60341	59691	CDS
			locus_tag	Harvfovirus_3_72
			product	hypothetical protein
			transl_table	11
60731	60570	CDS
			locus_tag	Harvfovirus_3_73
			product	hypothetical protein
			transl_table	11
63507	61942	CDS
			locus_tag	Harvfovirus_3_74
			product	ubiquitin carboxyl-terminal hydrolase 18-like
			transl_table	11
64172	63558	CDS
			locus_tag	Harvfovirus_3_75
			product	hypothetical protein
			transl_table	11
64828	64262	CDS
			locus_tag	Harvfovirus_3_76
			product	hypothetical protein
			transl_table	11
65819	64884	CDS
			locus_tag	Harvfovirus_3_77
			product	hypothetical protein
			transl_table	11
66033	66608	CDS
			locus_tag	Harvfovirus_3_78
			product	hypothetical protein
			transl_table	11
67205	66675	CDS
			locus_tag	Harvfovirus_3_79
			product	hypothetical protein
			transl_table	11
67235	68122	CDS
			locus_tag	Harvfovirus_3_80
			product	hypothetical protein
			transl_table	11
69073	68234	CDS
			locus_tag	Harvfovirus_3_81
			product	hypothetical protein
			transl_table	11
>Harvfovirus_4
757	2	CDS
			locus_tag	Harvfovirus_4_1
			product	hypothetical protein
			transl_table	11
804	1214	CDS
			locus_tag	Harvfovirus_4_2
			product	hypothetical protein
			transl_table	11
1279	1953	CDS
			locus_tag	Harvfovirus_4_3
			product	hypothetical protein
			transl_table	11
1982	2443	CDS
			locus_tag	Harvfovirus_4_4
			product	hypothetical protein
			transl_table	11
2973	2440	CDS
			locus_tag	Harvfovirus_4_5
			product	hypothetical protein
			transl_table	11
3864	2986	CDS
			locus_tag	Harvfovirus_4_6
			product	phospholipase, patatin family
			transl_table	11
4006	3857	CDS
			locus_tag	Harvfovirus_4_7
			product	hypothetical protein
			transl_table	11
4673	4017	CDS
			locus_tag	Harvfovirus_4_8
			product	hypothetical protein
			transl_table	11
5475	4726	CDS
			locus_tag	Harvfovirus_4_9
			product	phosphonoacetaldehyde hydrolase 2
			transl_table	11
5579	5911	CDS
			locus_tag	Harvfovirus_4_10
			product	hypothetical protein
			transl_table	11
5931	6752	CDS
			locus_tag	Harvfovirus_4_11
			product	hypothetical protein
			transl_table	11
6806	7042	CDS
			locus_tag	Harvfovirus_4_12
			product	hypothetical protein
			transl_table	11
7106	8185	CDS
			locus_tag	Harvfovirus_4_13
			product	hypothetical protein
			transl_table	11
8251	9612	CDS
			locus_tag	Harvfovirus_4_14
			product	hypothetical protein
			transl_table	11
10292	9609	CDS
			locus_tag	Harvfovirus_4_15
			product	hypothetical protein
			transl_table	11
10566	11249	CDS
			locus_tag	Harvfovirus_4_16
			product	hypothetical protein
			transl_table	11
11284	12039	CDS
			locus_tag	Harvfovirus_4_17
			product	BAX inhibitor BI-1
			transl_table	11
12351	12046	CDS
			locus_tag	Harvfovirus_4_18
			product	hypothetical protein
			transl_table	11
14710	12404	CDS
			locus_tag	Harvfovirus_4_19
			product	PREDICTED: tryptophan--tRNA ligase, cytoplasmic-like isoform X1
			transl_table	11
16666	17358	CDS
			locus_tag	Harvfovirus_4_20
			product	glycosyltransferase family 25
			transl_table	11
17434	18519	CDS
			locus_tag	Harvfovirus_4_21
			product	hypothetical protein
			transl_table	11
18867	18571	CDS
			locus_tag	Harvfovirus_4_22
			product	vesicle-associated membrane protein 7B
			transl_table	11
19336	18926	CDS
			locus_tag	Harvfovirus_4_23
			product	hypothetical protein
			transl_table	11
20143	19379	CDS
			locus_tag	Harvfovirus_4_24
			product	hypothetical protein B6240_02170
			transl_table	11
20426	20145	CDS
			locus_tag	Harvfovirus_4_25
			product	putative Cytochrome b5
			transl_table	11
20507	20962	CDS
			locus_tag	Harvfovirus_4_26
			product	hypothetical protein
			transl_table	11
22590	20959	CDS
			locus_tag	Harvfovirus_4_27
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
22734	22639	CDS
			locus_tag	Harvfovirus_4_28
			product	hypothetical protein
			transl_table	11
23394	22780	CDS
			locus_tag	Harvfovirus_4_29
			product	hypothetical protein Indivirus_1_41
			transl_table	11
23479	23643	CDS
			locus_tag	Harvfovirus_4_30
			product	hypothetical protein
			transl_table	11
25537	24284	CDS
			locus_tag	Harvfovirus_4_31
			product	hypothetical protein
			transl_table	11
26186	25605	CDS
			locus_tag	Harvfovirus_4_32
			product	hypothetical protein
			transl_table	11
26367	26206	CDS
			locus_tag	Harvfovirus_4_33
			product	hypothetical protein
			transl_table	11
28375	26516	CDS
			locus_tag	Harvfovirus_4_34
			product	puromycin sensitive aminopeptidase
			transl_table	11
29193	28441	CDS
			locus_tag	Harvfovirus_4_35
			product	hypothetical protein
			transl_table	11
29281	31128	CDS
			locus_tag	Harvfovirus_4_36
			product	hypothetical protein
			transl_table	11
31154	31774	CDS
			locus_tag	Harvfovirus_4_37
			product	hypothetical protein PBRA_003807
			transl_table	11
33140	31761	CDS
			locus_tag	Harvfovirus_4_38
			product	seryl-tRNA synthetase
			transl_table	11
33230	34423	CDS
			locus_tag	Harvfovirus_4_39
			product	putative fucosyltransferase
			transl_table	11
36123	34432	CDS
			locus_tag	Harvfovirus_4_40
			product	glycosyltransferase
			transl_table	11
36201	37625	CDS
			locus_tag	Harvfovirus_4_41
			product	glutaminyl-tRNA synthetase
			transl_table	11
37957	37622	CDS
			locus_tag	Harvfovirus_4_42
			product	hypothetical protein
			transl_table	11
38086	38535	CDS
			locus_tag	Harvfovirus_4_43
			product	putative ORFan
			transl_table	11
38603	39100	CDS
			locus_tag	Harvfovirus_4_44
			product	hypothetical protein
			transl_table	11
39669	39097	CDS
			locus_tag	Harvfovirus_4_45
			product	hypothetical protein FF38_02002, partial
			transl_table	11
39988	40767	CDS
			locus_tag	Harvfovirus_4_46
			product	glycosyl transferase group 1 family protein
			transl_table	11
41959	40751	CDS
			locus_tag	Harvfovirus_4_47
			product	hypothetical protein SDRG_10325
			transl_table	11
42252	43019	CDS
			locus_tag	Harvfovirus_4_48
			product	hypothetical protein CBC05_03300
			transl_table	11
43686	43009	CDS
			locus_tag	Harvfovirus_4_49
			product	hypothetical protein
			transl_table	11
43779	44246	CDS
			locus_tag	Harvfovirus_4_50
			product	Ubiquitin-conjugating enzyme E2-17 kDa
			transl_table	11
45233	44247	CDS
			locus_tag	Harvfovirus_4_51
			product	hypothetical protein
			transl_table	11
45280	46362	CDS
			locus_tag	Harvfovirus_4_52
			product	hypothetical protein
			transl_table	11
48427	46346	CDS
			locus_tag	Harvfovirus_4_53
			product	valine--tRNA ligase
			transl_table	11
50521	48488	CDS
			locus_tag	Harvfovirus_4_54
			product	hypothetical protein
			transl_table	11
52143	50584	CDS
			locus_tag	Harvfovirus_4_55
			product	unnamed protein product
			transl_table	11
53106	52207	CDS
			locus_tag	Harvfovirus_4_56
			product	hypothetical protein Catovirus_1_431
			transl_table	11
54182	53124	CDS
			locus_tag	Harvfovirus_4_57
			product	Tpt1/KptA family RNA 2'-phosphotransferase
			transl_table	11
54603	54238	CDS
			locus_tag	Harvfovirus_4_58
			product	hypothetical protein
			transl_table	11
54680	55249	CDS
			locus_tag	Harvfovirus_4_59
			product	ras-related protein Rab-28-like
			transl_table	11
55309	55746	CDS
			locus_tag	Harvfovirus_4_60
			product	hypothetical protein
			transl_table	11
>Harvfovirus_5
8296	8223	tRNA	Harvfovirus_tRNA_1
			product	tRNA-Arg(TCT)
8369	8297	tRNA	Harvfovirus_tRNA_2
			product	tRNA-Arg(TCT)
580	2	CDS
			locus_tag	Harvfovirus_5_1
			product	hypothetical protein
			transl_table	11
652	1296	CDS
			locus_tag	Harvfovirus_5_2
			product	hypothetical protein
			transl_table	11
1339	1980	CDS
			locus_tag	Harvfovirus_5_3
			product	hypothetical protein
			transl_table	11
1996	2631	CDS
			locus_tag	Harvfovirus_5_4
			product	hypothetical protein
			transl_table	11
3485	2628	CDS
			locus_tag	Harvfovirus_5_5
			product	hypothetical protein
			transl_table	11
3555	3884	CDS
			locus_tag	Harvfovirus_5_6
			product	hypothetical protein
			transl_table	11
5969	3879	CDS
			locus_tag	Harvfovirus_5_7
			product	putative ORFan
			transl_table	11
7272	6025	CDS
			locus_tag	Harvfovirus_5_8
			product	hypothetical protein
			transl_table	11
7387	7593	CDS
			locus_tag	Harvfovirus_5_9
			product	hypothetical protein
			transl_table	11
7637	8137	CDS
			locus_tag	Harvfovirus_5_10
			product	hypothetical protein
			transl_table	11
8458	9204	CDS
			locus_tag	Harvfovirus_5_11
			product	unnamed protein product
			transl_table	11
9946	10230	CDS
			locus_tag	Harvfovirus_5_12
			product	hypothetical protein
			transl_table	11
10285	10980	CDS
			locus_tag	Harvfovirus_5_13
			product	hypothetical protein
			transl_table	11
11038	11376	CDS
			locus_tag	Harvfovirus_5_14
			product	hypothetical protein
			transl_table	11
11431	12636	CDS
			locus_tag	Harvfovirus_5_15
			product	hypothetical protein
			transl_table	11
12684	13151	CDS
			locus_tag	Harvfovirus_5_16
			product	hypothetical protein
			transl_table	11
14288	13128	CDS
			locus_tag	Harvfovirus_5_17
			product	hypothetical protein
			transl_table	11
15342	14344	CDS
			locus_tag	Harvfovirus_5_18
			product	hypothetical protein
			transl_table	11
15479	16339	CDS
			locus_tag	Harvfovirus_5_19
			product	PREDICTED: E3 ubiquitin-protein ligase parkin
			transl_table	11
16387	16992	CDS
			locus_tag	Harvfovirus_5_20
			product	hypothetical protein
			transl_table	11
17050	17991	CDS
			locus_tag	Harvfovirus_5_21
			product	hypothetical protein
			transl_table	11
18057	18284	CDS
			locus_tag	Harvfovirus_5_22
			product	hypothetical protein
			transl_table	11
18314	19396	CDS
			locus_tag	Harvfovirus_5_23
			product	hypothetical protein
			transl_table	11
20250	19381	CDS
			locus_tag	Harvfovirus_5_24
			product	hypothetical protein
			transl_table	11
20366	21808	CDS
			locus_tag	Harvfovirus_5_25
			product	hypothetical protein
			transl_table	11
22549	21782	CDS
			locus_tag	Harvfovirus_5_26
			product	hypothetical protein
			transl_table	11
23335	22592	CDS
			locus_tag	Harvfovirus_5_27
			product	hypothetical protein
			transl_table	11
23474	24466	CDS
			locus_tag	Harvfovirus_5_28
			product	hypothetical protein
			transl_table	11
24865	24470	CDS
			locus_tag	Harvfovirus_5_29
			product	hypothetical protein
			transl_table	11
26755	24914	CDS
			locus_tag	Harvfovirus_5_30
			product	hypothetical protein Klosneuvirus_5_56
			transl_table	11
26900	27439	CDS
			locus_tag	Harvfovirus_5_31
			product	hypothetical protein
			transl_table	11
27915	27445	CDS
			locus_tag	Harvfovirus_5_32
			product	hypothetical protein
			transl_table	11
28274	27936	CDS
			locus_tag	Harvfovirus_5_33
			product	hypothetical protein
			transl_table	11
28991	28452	CDS
			locus_tag	Harvfovirus_5_34
			product	hypothetical protein
			transl_table	11
30312	29029	CDS
			locus_tag	Harvfovirus_5_35
			product	hypothetical protein
			transl_table	11
30783	30367	CDS
			locus_tag	Harvfovirus_5_36
			product	hypothetical protein UY64_C0030G0006
			transl_table	11
31796	30840	CDS
			locus_tag	Harvfovirus_5_37
			product	hypothetical protein
			transl_table	11
33255	31852	CDS
			locus_tag	Harvfovirus_5_38
			product	hypothetical protein
			transl_table	11
33356	34552	CDS
			locus_tag	Harvfovirus_5_39
			product	hypothetical protein
			transl_table	11
36025	34553	CDS
			locus_tag	Harvfovirus_5_40
			product	hypothetical protein
			transl_table	11
36132	37805	CDS
			locus_tag	Harvfovirus_5_41
			product	hypothetical protein
			transl_table	11
37865	38281	CDS
			locus_tag	Harvfovirus_5_42
			product	hypothetical protein
			transl_table	11
38349	38873	CDS
			locus_tag	Harvfovirus_5_43
			product	hypothetical protein
			transl_table	11
39052	40278	CDS
			locus_tag	Harvfovirus_5_44
			product	hypothetical protein
			transl_table	11
40340	40945	CDS
			locus_tag	Harvfovirus_5_45
			product	hypothetical protein
			transl_table	11
42107	40974	CDS
			locus_tag	Harvfovirus_5_46
			product	hypothetical protein
			transl_table	11
42202	44640	CDS
			locus_tag	Harvfovirus_5_47
			product	glycosyltransferase family 2
			transl_table	11
45130	44627	CDS
			locus_tag	Harvfovirus_5_48
			product	hypothetical protein PPL_09843
			transl_table	11
45541	45161	CDS
			locus_tag	Harvfovirus_5_49
			product	hypothetical protein
			transl_table	11
45623	46393	CDS
			locus_tag	Harvfovirus_5_50
			product	hypothetical protein
			transl_table	11
47597	46386	CDS
			locus_tag	Harvfovirus_5_51
			product	hypothetical protein
			transl_table	11
48741	47656	CDS
			locus_tag	Harvfovirus_5_52
			product	hypothetical protein
			transl_table	11
50131	48812	CDS
			locus_tag	Harvfovirus_5_53
			product	hypothetical protein
			transl_table	11
50214	50468	CDS
			locus_tag	Harvfovirus_5_54
			product	hypothetical protein
			transl_table	11
>Harvfovirus_6
27629	27558	tRNA	Harvfovirus_tRNA_3
			product	tRNA-Gly(TCC)
334	933	CDS
			locus_tag	Harvfovirus_6_1
			product	hypothetical protein
			transl_table	11
1253	1894	CDS
			locus_tag	Harvfovirus_6_2
			product	hypothetical protein
			transl_table	11
1956	2558	CDS
			locus_tag	Harvfovirus_6_3
			product	hypothetical protein
			transl_table	11
3427	2564	CDS
			locus_tag	Harvfovirus_6_4
			product	hypothetical protein
			transl_table	11
3568	3443	CDS
			locus_tag	Harvfovirus_6_5
			product	hypothetical protein
			transl_table	11
3561	4196	CDS
			locus_tag	Harvfovirus_6_6
			product	hypothetical protein
			transl_table	11
4250	5164	CDS
			locus_tag	Harvfovirus_6_7
			product	hypothetical protein
			transl_table	11
5197	6957	CDS
			locus_tag	Harvfovirus_6_8
			product	hypothetical protein GALMADRAFT_251438
			transl_table	11
6990	7787	CDS
			locus_tag	Harvfovirus_6_9
			product	hypothetical protein
			transl_table	11
8433	7822	CDS
			locus_tag	Harvfovirus_6_10
			product	hypothetical protein
			transl_table	11
8509	9585	CDS
			locus_tag	Harvfovirus_6_11
			product	hypothetical protein
			transl_table	11
9611	10744	CDS
			locus_tag	Harvfovirus_6_12
			product	hypothetical protein
			transl_table	11
10765	11874	CDS
			locus_tag	Harvfovirus_6_13
			product	hypothetical protein
			transl_table	11
11931	12587	CDS
			locus_tag	Harvfovirus_6_14
			product	hypothetical protein
			transl_table	11
12644	13081	CDS
			locus_tag	Harvfovirus_6_15
			product	hypothetical protein
			transl_table	11
13127	13816	CDS
			locus_tag	Harvfovirus_6_16
			product	hypothetical protein
			transl_table	11
14827	13772	CDS
			locus_tag	Harvfovirus_6_17
			product	hypothetical protein
			transl_table	11
15618	14923	CDS
			locus_tag	Harvfovirus_6_18
			product	hypothetical protein
			transl_table	11
15898	15665	CDS
			locus_tag	Harvfovirus_6_19
			product	hypothetical protein
			transl_table	11
16711	16133	CDS
			locus_tag	Harvfovirus_6_20
			product	hypothetical protein
			transl_table	11
17034	16729	CDS
			locus_tag	Harvfovirus_6_21
			product	hypothetical protein
			transl_table	11
17224	17829	CDS
			locus_tag	Harvfovirus_6_22
			product	hypothetical protein
			transl_table	11
19609	18209	CDS
			locus_tag	Harvfovirus_6_23
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
20069	19923	CDS
			locus_tag	Harvfovirus_6_24
			product	hypothetical protein
			transl_table	11
20429	20313	CDS
			locus_tag	Harvfovirus_6_25
			product	hypothetical protein
			transl_table	11
21112	20483	CDS
			locus_tag	Harvfovirus_6_26
			product	hypothetical protein
			transl_table	11
21318	21115	CDS
			locus_tag	Harvfovirus_6_27
			product	hypothetical protein
			transl_table	11
21417	22688	CDS
			locus_tag	Harvfovirus_6_28
			product	hypothetical protein
			transl_table	11
23040	22672	CDS
			locus_tag	Harvfovirus_6_29
			product	hypothetical protein
			transl_table	11
24049	23138	CDS
			locus_tag	Harvfovirus_6_30
			product	hypothetical protein
			transl_table	11
24394	24233	CDS
			locus_tag	Harvfovirus_6_31
			product	hypothetical protein
			transl_table	11
24900	24736	CDS
			locus_tag	Harvfovirus_6_32
			product	hypothetical protein
			transl_table	11
25000	25512	CDS
			locus_tag	Harvfovirus_6_33
			product	hypothetical protein
			transl_table	11
25817	25503	CDS
			locus_tag	Harvfovirus_6_34
			product	hypothetical protein
			transl_table	11
26545	25880	CDS
			locus_tag	Harvfovirus_6_35
			product	hypothetical protein
			transl_table	11
26670	27428	CDS
			locus_tag	Harvfovirus_6_36
			product	hypothetical protein
			transl_table	11
27864	28376	CDS
			locus_tag	Harvfovirus_6_37
			product	hypothetical protein
			transl_table	11
28444	28899	CDS
			locus_tag	Harvfovirus_6_38
			product	hypothetical protein
			transl_table	11
29320	28874	CDS
			locus_tag	Harvfovirus_6_39
			product	hypothetical protein
			transl_table	11
29850	29398	CDS
			locus_tag	Harvfovirus_6_40
			product	hypothetical protein
			transl_table	11
30036	31379	CDS
			locus_tag	Harvfovirus_6_41
			product	hypothetical protein
			transl_table	11
31560	32468	CDS
			locus_tag	Harvfovirus_6_42
			product	hypothetical protein
			transl_table	11
32523	33392	CDS
			locus_tag	Harvfovirus_6_43
			product	hypothetical protein
			transl_table	11
33863	33372	CDS
			locus_tag	Harvfovirus_6_44
			product	hypothetical protein
			transl_table	11
34599	33922	CDS
			locus_tag	Harvfovirus_6_45
			product	hypothetical protein
			transl_table	11
35548	34646	CDS
			locus_tag	Harvfovirus_6_46
			product	hypothetical protein
			transl_table	11
36715	35606	CDS
			locus_tag	Harvfovirus_6_47
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
36838	37095	CDS
			locus_tag	Harvfovirus_6_48
			product	hypothetical protein AN481_12630
			transl_table	11
37118	38452	CDS
			locus_tag	Harvfovirus_6_49
			product	PREDICTED: serine/threonine-protein kinase pim-2-like
			transl_table	11
39534	38449	CDS
			locus_tag	Harvfovirus_6_50
			product	hypothetical protein
			transl_table	11
39622	40329	CDS
			locus_tag	Harvfovirus_6_51
			product	DNA ligase
			transl_table	11
40388	41932	CDS
			locus_tag	Harvfovirus_6_52
			product	hypothetical protein BHV77_01790
			transl_table	11
41964	42530	CDS
			locus_tag	Harvfovirus_6_53
			product	hypothetical protein
			transl_table	11
>Harvfovirus_7
733	77	CDS
			locus_tag	Harvfovirus_7_1
			product	ras-related protein RABB1a isoform X2
			transl_table	11
1676	762	CDS
			locus_tag	Harvfovirus_7_2
			product	NTE family protein
			transl_table	11
1758	2462	CDS
			locus_tag	Harvfovirus_7_3
			product	hypothetical protein
			transl_table	11
4435	2459	CDS
			locus_tag	Harvfovirus_7_4
			product	heat shock 70 kDa protein
			transl_table	11
4530	5660	CDS
			locus_tag	Harvfovirus_7_5
			product	hypothetical protein
			transl_table	11
5729	6820	CDS
			locus_tag	Harvfovirus_7_6
			product	replication factor C small subunit
			transl_table	11
8248	6767	CDS
			locus_tag	Harvfovirus_7_7
			product	HD phosphohydrolase
			transl_table	11
10767	8281	CDS
			locus_tag	Harvfovirus_7_8
			product	superfamily II DNA or RNA helicase
			transl_table	11
10846	11115	CDS
			locus_tag	Harvfovirus_7_9
			product	hypothetical protein
			transl_table	11
11145	12242	CDS
			locus_tag	Harvfovirus_7_10
			product	metallophosphatase/phosphoesterase
			transl_table	11
13225	12239	CDS
			locus_tag	Harvfovirus_7_11
			product	hypothetical protein
			transl_table	11
13314	15092	CDS
			locus_tag	Harvfovirus_7_12
			product	XRN 5'-3' exonuclease
			transl_table	11
15118	16473	CDS
			locus_tag	Harvfovirus_7_13
			product	serine/threonine protein kinase
			transl_table	11
16533	18971	CDS
			locus_tag	Harvfovirus_7_14
			product	divergent protein kinase
			transl_table	11
19244	19023	CDS
			locus_tag	Harvfovirus_7_15
			product	hypothetical protein
			transl_table	11
19340	21139	CDS
			locus_tag	Harvfovirus_7_16
			product	hypothetical protein Catovirus_2_262
			transl_table	11
21179	21664	CDS
			locus_tag	Harvfovirus_7_17
			product	hypothetical protein Catovirus_2_265
			transl_table	11
21715	22284	CDS
			locus_tag	Harvfovirus_7_18
			product	hypothetical protein
			transl_table	11
24238	22487	CDS
			locus_tag	Harvfovirus_7_19
			product	P4B major core protein
			transl_table	11
25937	24312	CDS
			locus_tag	Harvfovirus_7_20
			product	replication factor C large subunit
			transl_table	11
26713	25979	CDS
			locus_tag	Harvfovirus_7_21
			product	hypothetical protein Catovirus_2_270
			transl_table	11
27192	26743	CDS
			locus_tag	Harvfovirus_7_22
			product	hypothetical protein Klosneuvirus_1_13
			transl_table	11
27271	28404	CDS
			locus_tag	Harvfovirus_7_23
			product	hypothetical protein Catovirus_2_272
			transl_table	11
28434	28919	CDS
			locus_tag	Harvfovirus_7_24
			product	hypothetical protein Hokovirus_2_164
			transl_table	11
29630	28905	CDS
			locus_tag	Harvfovirus_7_25
			product	serine recombinase
			transl_table	11
29732	29643	CDS
			locus_tag	Harvfovirus_7_26
			product	hypothetical protein
			transl_table	11
29731	30969	CDS
			locus_tag	Harvfovirus_7_27
			product	hypothetical protein Catovirus_2_274
			transl_table	11
30966	31379	CDS
			locus_tag	Harvfovirus_7_28
			product	hypothetical protein Catovirus_2_275
			transl_table	11
31425	32363	CDS
			locus_tag	Harvfovirus_7_29
			product	packaging ATPase
			transl_table	11
33071	32349	CDS
			locus_tag	Harvfovirus_7_30
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
33359	34348	CDS
			locus_tag	Harvfovirus_7_31
			product	hypothetical protein
			transl_table	11
34376	35305	CDS
			locus_tag	Harvfovirus_7_32
			product	SDR family NAD(P)-dependent oxidoreductase
			transl_table	11
36795	35302	CDS
			locus_tag	Harvfovirus_7_33
			product	hypothetical protein
			transl_table	11
38347	36836	CDS
			locus_tag	Harvfovirus_7_34
			product	hypothetical protein
			transl_table	11
41393	39945	CDS
			locus_tag	Harvfovirus_7_35
			product	hypothetical protein
			transl_table	11
42388	41429	CDS
			locus_tag	Harvfovirus_7_36
			product	hypothetical protein
			transl_table	11
>Harvfovirus_8
447	1	CDS
			locus_tag	Harvfovirus_8_1
			product	hypothetical protein Catovirus_1_1055
			transl_table	11
578	1099	CDS
			locus_tag	Harvfovirus_8_2
			product	hypothetical protein BCR33DRAFT_711812
			transl_table	11
4155	1096	CDS
			locus_tag	Harvfovirus_8_3
			product	DEAD/SNF2-like helicase
			transl_table	11
4734	4213	CDS
			locus_tag	Harvfovirus_8_4
			product	hypoxanthine phosphoribosyltransferase
			transl_table	11
5358	6005	CDS
			locus_tag	Harvfovirus_8_5
			product	hypothetical protein Catovirus_1_1058
			transl_table	11
6838	6008	CDS
			locus_tag	Harvfovirus_8_6
			product	hypothetical protein Catovirus_1_1059
			transl_table	11
7997	6903	CDS
			locus_tag	Harvfovirus_8_7
			product	hypothetical protein Catovirus_1_1060
			transl_table	11
8167	8943	CDS
			locus_tag	Harvfovirus_8_8
			product	hypothetical protein Catovirus_1_1061
			transl_table	11
9906	8944	CDS
			locus_tag	Harvfovirus_8_9
			product	hypothetical protein
			transl_table	11
9993	10598	CDS
			locus_tag	Harvfovirus_8_10
			product	hypothetical protein AYO44_11180
			transl_table	11
11765	10602	CDS
			locus_tag	Harvfovirus_8_11
			product	hypothetical protein
			transl_table	11
12077	12565	CDS
			locus_tag	Harvfovirus_8_12
			product	hypothetical protein
			transl_table	11
13116	12562	CDS
			locus_tag	Harvfovirus_8_13
			product	hypothetical protein
			transl_table	11
14594	13164	CDS
			locus_tag	Harvfovirus_8_14
			product	hypothetical protein
			transl_table	11
15383	14655	CDS
			locus_tag	Harvfovirus_8_15
			product	peptidase c14 caspase catalytic subunit p20
			transl_table	11
18331	15458	CDS
			locus_tag	Harvfovirus_8_16
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
18546	18683	CDS
			locus_tag	Harvfovirus_8_17
			product	hypothetical protein
			transl_table	11
20473	18704	CDS
			locus_tag	Harvfovirus_8_18
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
22376	21240	CDS
			locus_tag	Harvfovirus_8_19
			product	GIY-YIG-like endonuclease
			transl_table	11
23356	23030	CDS
			locus_tag	Harvfovirus_8_20
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
23976	23887	CDS
			locus_tag	Harvfovirus_8_21
			product	hypothetical protein
			transl_table	11
25428	24412	CDS
			locus_tag	Harvfovirus_8_22
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
25683	25486	CDS
			locus_tag	Harvfovirus_8_23
			product	hypothetical protein
			transl_table	11
27726	28487	CDS
			locus_tag	Harvfovirus_8_24
			product	hypothetical protein
			transl_table	11
30779	28488	CDS
			locus_tag	Harvfovirus_8_25
			product	hypothetical protein
			transl_table	11
30830	32218	CDS
			locus_tag	Harvfovirus_8_26
			product	hypothetical protein
			transl_table	11
32258	33718	CDS
			locus_tag	Harvfovirus_8_27
			product	hypothetical protein
			transl_table	11
33743	35797	CDS
			locus_tag	Harvfovirus_8_28
			product	hypothetical protein
			transl_table	11
35842	36222	CDS
			locus_tag	Harvfovirus_8_29
			product	hypothetical protein
			transl_table	11
36229	38001	CDS
			locus_tag	Harvfovirus_8_30
			product	hypothetical protein
			transl_table	11
38764	38006	CDS
			locus_tag	Harvfovirus_8_31
			product	hypothetical protein
			transl_table	11
>Harvfovirus_9
1044	1	CDS
			locus_tag	Harvfovirus_9_1
			product	exo-alpha-sialidase
			transl_table	11
1175	1966	CDS
			locus_tag	Harvfovirus_9_2
			product	hypothetical protein
			transl_table	11
2362	1979	CDS
			locus_tag	Harvfovirus_9_3
			product	putative uncharacterized protein
			transl_table	11
2532	2861	CDS
			locus_tag	Harvfovirus_9_4
			product	hypothetical protein
			transl_table	11
3083	3775	CDS
			locus_tag	Harvfovirus_9_5
			product	hypothetical protein
			transl_table	11
3948	4940	CDS
			locus_tag	Harvfovirus_9_6
			product	hypothetical protein
			transl_table	11
6495	4927	CDS
			locus_tag	Harvfovirus_9_7
			product	hypothetical protein A2020_14590
			transl_table	11
6557	6886	CDS
			locus_tag	Harvfovirus_9_8
			product	hypothetical protein
			transl_table	11
6945	8786	CDS
			locus_tag	Harvfovirus_9_9
			product	hypothetical protein GALMADRAFT_251438
			transl_table	11
8847	9014	CDS
			locus_tag	Harvfovirus_9_10
			product	hypothetical protein
			transl_table	11
9450	9022	CDS
			locus_tag	Harvfovirus_9_11
			product	hypothetical protein
			transl_table	11
10660	9485	CDS
			locus_tag	Harvfovirus_9_12
			product	zinc finger protein
			transl_table	11
10863	10756	CDS
			locus_tag	Harvfovirus_9_13
			product	hypothetical protein
			transl_table	11
12647	11142	CDS
			locus_tag	Harvfovirus_9_14
			product	hypothetical protein
			transl_table	11
13438	12716	CDS
			locus_tag	Harvfovirus_9_15
			product	hypothetical protein
			transl_table	11
13570	14403	CDS
			locus_tag	Harvfovirus_9_16
			product	serine dehydrogenasease
			transl_table	11
15934	14459	CDS
			locus_tag	Harvfovirus_9_17
			product	chromosome condensation regulator
			transl_table	11
16029	16640	CDS
			locus_tag	Harvfovirus_9_18
			product	hypothetical protein
			transl_table	11
16798	17532	CDS
			locus_tag	Harvfovirus_9_19
			product	hypothetical protein
			transl_table	11
17868	18425	CDS
			locus_tag	Harvfovirus_9_20
			product	hypothetical protein
			transl_table	11
19690	19400	CDS
			locus_tag	Harvfovirus_9_21
			product	hypothetical protein
			transl_table	11
20732	19872	CDS
			locus_tag	Harvfovirus_9_22
			product	hypothetical protein
			transl_table	11
20886	21629	CDS
			locus_tag	Harvfovirus_9_23
			product	hypothetical protein
			transl_table	11
21701	22591	CDS
			locus_tag	Harvfovirus_9_24
			product	hypothetical protein
			transl_table	11
23540	25528	CDS
			locus_tag	Harvfovirus_9_25
			product	hypothetical protein
			transl_table	11
25567	26259	CDS
			locus_tag	Harvfovirus_9_26
			product	hypothetical protein
			transl_table	11
26342	26641	CDS
			locus_tag	Harvfovirus_9_27
			product	hypothetical protein
			transl_table	11
26730	34094	CDS
			locus_tag	Harvfovirus_9_28
			product	hypothetical protein Hokovirus_2_216
			transl_table	11
34216	34446	CDS
			locus_tag	Harvfovirus_9_29
			product	hypothetical protein
			transl_table	11
34555	35112	CDS
			locus_tag	Harvfovirus_9_30
			product	hypothetical protein
			transl_table	11
35284	36216	CDS
			locus_tag	Harvfovirus_9_31
			product	hypothetical protein crov231
			transl_table	11
36881	36348	CDS
			locus_tag	Harvfovirus_9_32
			product	hypothetical protein
			transl_table	11
>Harvfovirus_10
3	1601	CDS
			locus_tag	Harvfovirus_10_1
			product	NCLDV major capsid protein
			transl_table	11
1666	2130	CDS
			locus_tag	Harvfovirus_10_2
			product	polyubiquitin
			transl_table	11
2841	2143	CDS
			locus_tag	Harvfovirus_10_3
			product	hypothetical protein
			transl_table	11
4342	2894	CDS
			locus_tag	Harvfovirus_10_4
			product	NCLDV major capsid protein
			transl_table	11
6623	4386	CDS
			locus_tag	Harvfovirus_10_5
			product	hypothetical protein Catovirus_2_301
			transl_table	11
6697	7080	CDS
			locus_tag	Harvfovirus_10_6
			product	thioredoxin domain-containing protein 5 homolog isoform X3
			transl_table	11
8199	7081	CDS
			locus_tag	Harvfovirus_10_7
			product	ADP-ribosylglycohydrolase
			transl_table	11
8281	9372	CDS
			locus_tag	Harvfovirus_10_8
			product	DnaJ domain protein
			transl_table	11
10286	9369	CDS
			locus_tag	Harvfovirus_10_9
			product	patatin-like phospholipase
			transl_table	11
10279	10779	CDS
			locus_tag	Harvfovirus_10_10
			product	hypothetical protein Catovirus_2_296
			transl_table	11
11295	10765	CDS
			locus_tag	Harvfovirus_10_11
			product	hypothetical protein
			transl_table	11
11414	13126	CDS
			locus_tag	Harvfovirus_10_12
			product	hypothetical protein Catovirus_2_294
			transl_table	11
13188	14318	CDS
			locus_tag	Harvfovirus_10_13
			product	hypothetical protein Catovirus_2_292
			transl_table	11
14315	14644	CDS
			locus_tag	Harvfovirus_10_14
			product	hypothetical protein
			transl_table	11
15615	14641	CDS
			locus_tag	Harvfovirus_10_15
			product	hypothetical protein Catovirus_2_291
			transl_table	11
15978	15652	CDS
			locus_tag	Harvfovirus_10_16
			product	hypothetical protein
			transl_table	11
16847	16008	CDS
			locus_tag	Harvfovirus_10_17
			product	FBox-LRR protein
			transl_table	11
17266	16910	CDS
			locus_tag	Harvfovirus_10_18
			product	hypothetical protein
			transl_table	11
17841	17338	CDS
			locus_tag	Harvfovirus_10_19
			product	hypothetical protein Klosneuvirus_1_39
			transl_table	11
17986	18831	CDS
			locus_tag	Harvfovirus_10_20
			product	hypothetical protein
			transl_table	11
18818	18922	CDS
			locus_tag	Harvfovirus_10_21
			product	hypothetical protein
			transl_table	11
18924	19025	CDS
			locus_tag	Harvfovirus_10_22
			product	hypothetical protein
			transl_table	11
19173	20027	CDS
			locus_tag	Harvfovirus_10_23
			product	hypothetical protein
			transl_table	11
20087	20887	CDS
			locus_tag	Harvfovirus_10_24
			product	hypothetical protein
			transl_table	11
21384	23093	CDS
			locus_tag	Harvfovirus_10_25
			product	divergent HNH endonuclease
			transl_table	11
23290	24630	CDS
			locus_tag	Harvfovirus_10_26
			product	hypothetical protein
			transl_table	11
26329	24935	CDS
			locus_tag	Harvfovirus_10_27
			product	NCLDV major capsid protein
			transl_table	11
27157	26603	CDS
			locus_tag	Harvfovirus_10_28
			product	NCLDV major capsid protein
			transl_table	11
27405	28943	CDS
			locus_tag	Harvfovirus_10_29
			product	hypothetical protein BNJ_00111
			transl_table	11
29622	30827	CDS
			locus_tag	Harvfovirus_10_30
			product	hypothetical protein
			transl_table	11
31092	30970	CDS
			locus_tag	Harvfovirus_10_31
			product	hypothetical protein
			transl_table	11
31962	31201	CDS
			locus_tag	Harvfovirus_10_32
			product	predicted protein
			transl_table	11
32552	32061	CDS
			locus_tag	Harvfovirus_10_33
			product	hypothetical protein
			transl_table	11
32778	32587	CDS
			locus_tag	Harvfovirus_10_34
			product	hypothetical protein
			transl_table	11
32991	34037	CDS
			locus_tag	Harvfovirus_10_35
			product	late transcription factor VLTF3-like protein
			transl_table	11
34075	34986	CDS
			locus_tag	Harvfovirus_10_36
			product	hypothetical protein
			transl_table	11
>Harvfovirus_11
1841	3	CDS
			locus_tag	Harvfovirus_11_1
			product	putative minor capsid protein
			transl_table	11
1949	5707	CDS
			locus_tag	Harvfovirus_11_2
			product	DNA gyrase/topoisomerase IV
			transl_table	11
5770	7998	CDS
			locus_tag	Harvfovirus_11_3
			product	U-box domain protein
			transl_table	11
8053	9408	CDS
			locus_tag	Harvfovirus_11_4
			product	methyltransferase domain
			transl_table	11
9455	11737	CDS
			locus_tag	Harvfovirus_11_5
			product	metallophosphoesterase
			transl_table	11
12547	11732	CDS
			locus_tag	Harvfovirus_11_6
			product	apurinic endonuclease
			transl_table	11
12623	13810	CDS
			locus_tag	Harvfovirus_11_7
			product	replication factor C small subunit
			transl_table	11
14936	13821	CDS
			locus_tag	Harvfovirus_11_8
			product	hypothetical protein
			transl_table	11
16695	14983	CDS
			locus_tag	Harvfovirus_11_9
			product	asparagine synthase
			transl_table	11
17718	16759	CDS
			locus_tag	Harvfovirus_11_10
			product	hypothetical protein Catovirus_2_325
			transl_table	11
22811	17745	CDS
			locus_tag	Harvfovirus_11_11
			product	hypothetical protein Catovirus_2_323
			transl_table	11
22925	24235	CDS
			locus_tag	Harvfovirus_11_12
			product	RNA ligase
			transl_table	11
25329	24232	CDS
			locus_tag	Harvfovirus_11_13
			product	DNA directed RNA polymerase subunit L
			transl_table	11
25410	26033	CDS
			locus_tag	Harvfovirus_11_14
			product	hypothetical protein Indivirus_1_43
			transl_table	11
26061	26498	CDS
			locus_tag	Harvfovirus_11_15
			product	hypothetical protein Indivirus_1_42
			transl_table	11
26541	27905	CDS
			locus_tag	Harvfovirus_11_16
			product	hypothetical protein Catovirus_2_317
			transl_table	11
27941	28630	CDS
			locus_tag	Harvfovirus_11_17
			product	hypothetical protein
			transl_table	11
28681	29091	CDS
			locus_tag	Harvfovirus_11_18
			product	dual specificity protein phosphatase 1B
			transl_table	11
29114	29590	CDS
			locus_tag	Harvfovirus_11_19
			product	hypothetical protein
			transl_table	11
29632	30675	CDS
			locus_tag	Harvfovirus_11_20
			product	tyrosine-tRNA synthetase
			transl_table	11
31734	30667	CDS
			locus_tag	Harvfovirus_11_21
			product	polynucleotide phosphatase/kinase
			transl_table	11
31845	33188	CDS
			locus_tag	Harvfovirus_11_22
			product	serpin
			transl_table	11
33754	33179	CDS
			locus_tag	Harvfovirus_11_23
			product	hypothetical protein Catovirus_2_312
			transl_table	11
33813	34571	CDS
			locus_tag	Harvfovirus_11_24
			product	hypothetical protein Catovirus_2_311
			transl_table	11
35119	34574	CDS
			locus_tag	Harvfovirus_11_25
			product	hypothetical protein
			transl_table	11
35203	35652	CDS
			locus_tag	Harvfovirus_11_26
			product	hypothetical protein Catovirus_2_310
			transl_table	11
>Harvfovirus_12
3	401	CDS
			locus_tag	Harvfovirus_12_1
			product	hypothetical protein
			transl_table	11
616	1641	CDS
			locus_tag	Harvfovirus_12_2
			product	ABC transporter ATP-binding protein
			transl_table	11
1706	2464	CDS
			locus_tag	Harvfovirus_12_3
			product	hypothetical protein
			transl_table	11
2493	3728	CDS
			locus_tag	Harvfovirus_12_4
			product	cyclopropane-fatty-acyl-phospholipid synthase
			transl_table	11
3942	3730	CDS
			locus_tag	Harvfovirus_12_5
			product	hypothetical protein
			transl_table	11
4411	3998	CDS
			locus_tag	Harvfovirus_12_6
			product	glyoxal reductase
			transl_table	11
6412	4772	CDS
			locus_tag	Harvfovirus_12_7
			product	leucine-rich repeat protein
			transl_table	11
6533	7531	CDS
			locus_tag	Harvfovirus_12_8
			product	hypothetical protein
			transl_table	11
7584	9491	CDS
			locus_tag	Harvfovirus_12_9
			product	hypothetical protein OT06_49260
			transl_table	11
10281	9496	CDS
			locus_tag	Harvfovirus_12_10
			product	metallophosphoesterase
			transl_table	11
11383	10334	CDS
			locus_tag	Harvfovirus_12_11
			product	hypothetical protein C5B45_03800
			transl_table	11
11758	11438	CDS
			locus_tag	Harvfovirus_12_12
			product	hypothetical protein
			transl_table	11
11757	11873	CDS
			locus_tag	Harvfovirus_12_13
			product	hypothetical protein
			transl_table	11
11884	12558	CDS
			locus_tag	Harvfovirus_12_14
			product	hypothetical protein
			transl_table	11
12608	12982	CDS
			locus_tag	Harvfovirus_12_15
			product	hypothetical protein
			transl_table	11
13034	14296	CDS
			locus_tag	Harvfovirus_12_16
			product	chromosome condensation regulator, partial
			transl_table	11
14352	15695	CDS
			locus_tag	Harvfovirus_12_17
			product	chromosome condensation regulator, partial
			transl_table	11
15706	17460	CDS
			locus_tag	Harvfovirus_12_18
			product	DUF229 domain-containing protein
			transl_table	11
17522	18070	CDS
			locus_tag	Harvfovirus_12_19
			product	NADPH-dependent oxidoreductase
			transl_table	11
18093	19313	CDS
			locus_tag	Harvfovirus_12_20
			product	hypothetical protein
			transl_table	11
19886	19278	CDS
			locus_tag	Harvfovirus_12_21
			product	hypothetical protein
			transl_table	11
20219	20058	CDS
			locus_tag	Harvfovirus_12_22
			product	hypothetical protein
			transl_table	11
20631	21221	CDS
			locus_tag	Harvfovirus_12_23
			product	hypothetical protein COT80_02035
			transl_table	11
21244	22422	CDS
			locus_tag	Harvfovirus_12_24
			product	DUF3494 domain-containing protein
			transl_table	11
23253	22396	CDS
			locus_tag	Harvfovirus_12_25
			product	Alginate lyase 2 domain protein
			transl_table	11
23314	27183	CDS
			locus_tag	Harvfovirus_12_26
			product	hypothetical protein
			transl_table	11
27248	29965	CDS
			locus_tag	Harvfovirus_12_27
			product	hypothetical protein
			transl_table	11
30028	31140	CDS
			locus_tag	Harvfovirus_12_28
			product	tetratricopeptide repeat protein
			transl_table	11
31212	33038	CDS
			locus_tag	Harvfovirus_12_29
			product	hypothetical protein LRAMOSA08776
			transl_table	11
33229	33035	CDS
			locus_tag	Harvfovirus_12_30
			product	hypothetical protein
			transl_table	11
>Harvfovirus_13
556	2	CDS
			locus_tag	Harvfovirus_13_1
			product	hypothetical protein BVG19_g1396
			transl_table	11
1033	611	CDS
			locus_tag	Harvfovirus_13_2
			product	dual specificity protein phosphatase 10
			transl_table	11
1236	1697	CDS
			locus_tag	Harvfovirus_13_3
			product	hypothetical protein
			transl_table	11
4299	1792	CDS
			locus_tag	Harvfovirus_13_4
			product	hypothetical protein
			transl_table	11
5012	4329	CDS
			locus_tag	Harvfovirus_13_5
			product	hypothetical protein
			transl_table	11
6393	5086	CDS
			locus_tag	Harvfovirus_13_6
			product	hypothetical protein
			transl_table	11
6427	7818	CDS
			locus_tag	Harvfovirus_13_7
			product	hypothetical protein
			transl_table	11
8704	7811	CDS
			locus_tag	Harvfovirus_13_8
			product	hypothetical protein
			transl_table	11
8830	9573	CDS
			locus_tag	Harvfovirus_13_9
			product	exonuclease 3'-5' domain-containing protein 2 isoform X1
			transl_table	11
10407	9574	CDS
			locus_tag	Harvfovirus_13_10
			product	hypothetical protein
			transl_table	11
11385	10480	CDS
			locus_tag	Harvfovirus_13_11
			product	hypothetical protein Catovirus_2_144
			transl_table	11
12155	11451	CDS
			locus_tag	Harvfovirus_13_12
			product	SET domain protein
			transl_table	11
12667	12200	CDS
			locus_tag	Harvfovirus_13_13
			product	hypothetical protein
			transl_table	11
12937	13569	CDS
			locus_tag	Harvfovirus_13_14
			product	hypothetical protein AXG93_4625s1010
			transl_table	11
13627	14598	CDS
			locus_tag	Harvfovirus_13_15
			product	hypothetical protein AMSG_06688
			transl_table	11
17000	14595	CDS
			locus_tag	Harvfovirus_13_16
			product	hypothetical protein AUK21_01435
			transl_table	11
18260	17058	CDS
			locus_tag	Harvfovirus_13_17
			product	hypothetical protein
			transl_table	11
19716	18316	CDS
			locus_tag	Harvfovirus_13_18
			product	probable mitochondrial chaperone BCS1-A
			transl_table	11
21406	19883	CDS
			locus_tag	Harvfovirus_13_19
			product	SPFH domain / Band 7 domain containing protein
			transl_table	11
21486	25451	CDS
			locus_tag	Harvfovirus_13_20
			product	DEAD/SNF2-like helicase
			transl_table	11
25730	27037	CDS
			locus_tag	Harvfovirus_13_21
			product	hypothetical protein
			transl_table	11
27163	27044	CDS
			locus_tag	Harvfovirus_13_22
			product	hypothetical protein
			transl_table	11
27957	27415	CDS
			locus_tag	Harvfovirus_13_23
			product	hypothetical protein
			transl_table	11
29216	28011	CDS
			locus_tag	Harvfovirus_13_24
			product	hypothetical protein
			transl_table	11
29476	29240	CDS
			locus_tag	Harvfovirus_13_25
			product	hypothetical protein
			transl_table	11
>Harvfovirus_14
21119	21191	tRNA	Harvfovirus_tRNA_4
			product	tRNA-Ile(TAT)
1268	3	CDS
			locus_tag	Harvfovirus_14_1
			product	superfamily II helicase
			transl_table	11
1691	1380	CDS
			locus_tag	Harvfovirus_14_2
			product	hypothetical protein Klosneuvirus_3_141
			transl_table	11
3106	1853	CDS
			locus_tag	Harvfovirus_14_3
			product	hypothetical protein
			transl_table	11
3336	3148	CDS
			locus_tag	Harvfovirus_14_4
			product	hypothetical protein
			transl_table	11
3431	4291	CDS
			locus_tag	Harvfovirus_14_5
			product	hypothetical protein
			transl_table	11
4548	5237	CDS
			locus_tag	Harvfovirus_14_6
			product	hypothetical protein
			transl_table	11
6152	5244	CDS
			locus_tag	Harvfovirus_14_7
			product	hypothetical protein
			transl_table	11
6133	6498	CDS
			locus_tag	Harvfovirus_14_8
			product	hypothetical protein
			transl_table	11
6516	6677	CDS
			locus_tag	Harvfovirus_14_9
			product	hypothetical protein
			transl_table	11
7655	6654	CDS
			locus_tag	Harvfovirus_14_10
			product	hypothetical protein
			transl_table	11
8626	7658	CDS
			locus_tag	Harvfovirus_14_11
			product	hypothetical protein
			transl_table	11
8944	8720	CDS
			locus_tag	Harvfovirus_14_12
			product	hypothetical protein
			transl_table	11
9085	9429	CDS
			locus_tag	Harvfovirus_14_13
			product	hypothetical protein
			transl_table	11
9775	9488	CDS
			locus_tag	Harvfovirus_14_14
			product	hypothetical protein
			transl_table	11
9872	10609	CDS
			locus_tag	Harvfovirus_14_15
			product	hypothetical protein
			transl_table	11
10779	11507	CDS
			locus_tag	Harvfovirus_14_16
			product	hypothetical protein
			transl_table	11
12482	11559	CDS
			locus_tag	Harvfovirus_14_17
			product	hypothetical protein
			transl_table	11
13384	12572	CDS
			locus_tag	Harvfovirus_14_18
			product	alpha/beta hydrolase
			transl_table	11
13694	13443	CDS
			locus_tag	Harvfovirus_14_19
			product	hypothetical protein
			transl_table	11
13837	14235	CDS
			locus_tag	Harvfovirus_14_20
			product	hypothetical protein
			transl_table	11
14548	14345	CDS
			locus_tag	Harvfovirus_14_21
			product	hypothetical protein
			transl_table	11
14672	15001	CDS
			locus_tag	Harvfovirus_14_22
			product	hypothetical protein
			transl_table	11
16150	15137	CDS
			locus_tag	Harvfovirus_14_23
			product	PREDICTED: TNF receptor-associated factor 5-like
			transl_table	11
16568	16335	CDS
			locus_tag	Harvfovirus_14_24
			product	hypothetical protein
			transl_table	11
17622	16639	CDS
			locus_tag	Harvfovirus_14_25
			product	hypothetical protein
			transl_table	11
18293	17682	CDS
			locus_tag	Harvfovirus_14_26
			product	hypothetical protein
			transl_table	11
18871	18362	CDS
			locus_tag	Harvfovirus_14_27
			product	hypothetical protein
			transl_table	11
19808	18930	CDS
			locus_tag	Harvfovirus_14_28
			product	hypothetical protein
			transl_table	11
19942	20199	CDS
			locus_tag	Harvfovirus_14_29
			product	hypothetical protein
			transl_table	11
20768	20328	CDS
			locus_tag	Harvfovirus_14_30
			product	hypothetical protein
			transl_table	11
21049	20852	CDS
			locus_tag	Harvfovirus_14_31
			product	hypothetical protein
			transl_table	11
21303	22754	CDS
			locus_tag	Harvfovirus_14_32
			product	hypothetical protein
			transl_table	11
23812	22751	CDS
			locus_tag	Harvfovirus_14_33
			product	hypothetical protein
			transl_table	11
23929	25299	CDS
			locus_tag	Harvfovirus_14_34
			product	chromosome condensation regulator, partial
			transl_table	11
26536	25301	CDS
			locus_tag	Harvfovirus_14_35
			product	hypothetical protein
			transl_table	11
26635	27753	CDS
			locus_tag	Harvfovirus_14_36
			product	hypothetical protein
			transl_table	11
28131	27745	CDS
			locus_tag	Harvfovirus_14_37
			product	hypothetical protein
			transl_table	11
>Harvfovirus_15
758	3	CDS
			locus_tag	Harvfovirus_15_1
			product	TNF receptor-associated factor 2-like isoform X1
			transl_table	11
1208	816	CDS
			locus_tag	Harvfovirus_15_2
			product	hypothetical protein
			transl_table	11
1330	2631	CDS
			locus_tag	Harvfovirus_15_3
			product	hypothetical protein
			transl_table	11
2657	3112	CDS
			locus_tag	Harvfovirus_15_4
			product	hypothetical protein bAD24_III06230
			transl_table	11
3120	4328	CDS
			locus_tag	Harvfovirus_15_5
			product	hypothetical protein AW11_00191
			transl_table	11
5371	4712	CDS
			locus_tag	Harvfovirus_15_6
			product	hypothetical protein
			transl_table	11
5837	5403	CDS
			locus_tag	Harvfovirus_15_7
			product	PREDICTED: TNF receptor-associated factor 4-like
			transl_table	11
6294	5887	CDS
			locus_tag	Harvfovirus_15_8
			product	hypothetical protein
			transl_table	11
6406	7323	CDS
			locus_tag	Harvfovirus_15_9
			product	hypothetical protein
			transl_table	11
7348	8280	CDS
			locus_tag	Harvfovirus_15_10
			product	hypothetical protein Klosneuvirus_1_277
			transl_table	11
8825	8277	CDS
			locus_tag	Harvfovirus_15_11
			product	hypothetical protein
			transl_table	11
10097	8868	CDS
			locus_tag	Harvfovirus_15_12
			product	hypothetical protein
			transl_table	11
11376	10129	CDS
			locus_tag	Harvfovirus_15_13
			product	glycosyltransferase
			transl_table	11
11461	12672	CDS
			locus_tag	Harvfovirus_15_14
			product	hypothetical protein
			transl_table	11
12713	13414	CDS
			locus_tag	Harvfovirus_15_15
			product	hypothetical protein
			transl_table	11
13404	13502	CDS
			locus_tag	Harvfovirus_15_16
			product	hypothetical protein
			transl_table	11
13509	14723	CDS
			locus_tag	Harvfovirus_15_17
			product	hypothetical protein
			transl_table	11
14773	15858	CDS
			locus_tag	Harvfovirus_15_18
			product	hypothetical protein
			transl_table	11
15886	16056	CDS
			locus_tag	Harvfovirus_15_19
			product	hypothetical protein
			transl_table	11
16944	16537	CDS
			locus_tag	Harvfovirus_15_20
			product	hypothetical protein
			transl_table	11
17283	16999	CDS
			locus_tag	Harvfovirus_15_21
			product	hypothetical protein
			transl_table	11
18616	17342	CDS
			locus_tag	Harvfovirus_15_22
			product	hypothetical protein
			transl_table	11
18750	19580	CDS
			locus_tag	Harvfovirus_15_23
			product	PREDICTED: polyubiquitin-A isoform X3
			transl_table	11
20662	19577	CDS
			locus_tag	Harvfovirus_15_24
			product	sel1 repeat family protein
			transl_table	11
21691	20720	CDS
			locus_tag	Harvfovirus_15_25
			product	hypothetical protein
			transl_table	11
22461	21757	CDS
			locus_tag	Harvfovirus_15_26
			product	hypothetical protein
			transl_table	11
22490	22618	CDS
			locus_tag	Harvfovirus_15_27
			product	hypothetical protein
			transl_table	11
22602	23582	CDS
			locus_tag	Harvfovirus_15_28
			product	MORN repeat-containing protein
			transl_table	11
23643	23924	CDS
			locus_tag	Harvfovirus_15_29
			product	hypothetical protein
			transl_table	11
24369	23917	CDS
			locus_tag	Harvfovirus_15_30
			product	hypothetical protein
			transl_table	11
25466	24426	CDS
			locus_tag	Harvfovirus_15_31
			product	AAA family ATPase
			transl_table	11
26994	25528	CDS
			locus_tag	Harvfovirus_15_32
			product	hypothetical protein
			transl_table	11
27793	27059	CDS
			locus_tag	Harvfovirus_15_33
			product	hypothetical protein
			transl_table	11
>Harvfovirus_16
17232	17162	tRNA	Harvfovirus_tRNA_5
			product	tRNA-Met(CAT)
17306	17235	tRNA	Harvfovirus_tRNA_6
			product	tRNA-Lys(CTT)
20059	19988	tRNA	Harvfovirus_tRNA_7
			product	tRNA-Ala(TGC)
20130	20060	tRNA	Harvfovirus_tRNA_8
			product	tRNA-Trp(CCA)
20204	20132	tRNA	Harvfovirus_tRNA_9
			product	tRNA-Asp(GTC)
1272	1472	CDS
			locus_tag	Harvfovirus_16_1
			product	hypothetical protein
			transl_table	11
6179	6054	CDS
			locus_tag	Harvfovirus_16_2
			product	hypothetical protein
			transl_table	11
12414	12551	CDS
			locus_tag	Harvfovirus_16_3
			product	hypothetical protein
			transl_table	11
12644	12877	CDS
			locus_tag	Harvfovirus_16_4
			product	hypothetical protein
			transl_table	11
13445	13759	CDS
			locus_tag	Harvfovirus_16_5
			product	hypothetical protein
			transl_table	11
13986	13804	CDS
			locus_tag	Harvfovirus_16_6
			product	hypothetical protein
			transl_table	11
14091	14207	CDS
			locus_tag	Harvfovirus_16_7
			product	hypothetical protein
			transl_table	11
15009	14779	CDS
			locus_tag	Harvfovirus_16_8
			product	hypothetical protein
			transl_table	11
16060	15848	CDS
			locus_tag	Harvfovirus_16_9
			product	hypothetical protein
			transl_table	11
19142	17445	CDS
			locus_tag	Harvfovirus_16_10
			product	hypothetical protein Klosneuvirus_1_400
			transl_table	11
19290	19964	CDS
			locus_tag	Harvfovirus_16_11
			product	hypothetical protein
			transl_table	11
20284	20937	CDS
			locus_tag	Harvfovirus_16_12
			product	hypothetical protein
			transl_table	11
20966	21076	CDS
			locus_tag	Harvfovirus_16_13
			product	hypothetical protein
			transl_table	11
21087	21917	CDS
			locus_tag	Harvfovirus_16_14
			product	hypothetical protein
			transl_table	11
22035	22730	CDS
			locus_tag	Harvfovirus_16_15
			product	hypothetical protein
			transl_table	11
22829	25126	CDS
			locus_tag	Harvfovirus_16_16
			product	hypothetical protein
			transl_table	11
25377	25490	CDS
			locus_tag	Harvfovirus_16_17
			product	hypothetical protein
			transl_table	11
>Harvfovirus_17
287	3	CDS
			locus_tag	Harvfovirus_17_1
			product	hypothetical protein
			transl_table	11
372	1382	CDS
			locus_tag	Harvfovirus_17_2
			product	hypothetical protein
			transl_table	11
2081	1359	CDS
			locus_tag	Harvfovirus_17_3
			product	hypothetical protein
			transl_table	11
2206	3447	CDS
			locus_tag	Harvfovirus_17_4
			product	hypothetical protein
			transl_table	11
4882	3473	CDS
			locus_tag	Harvfovirus_17_5
			product	hypothetical protein
			transl_table	11
5788	5021	CDS
			locus_tag	Harvfovirus_17_6
			product	hypothetical protein
			transl_table	11
6891	5800	CDS
			locus_tag	Harvfovirus_17_7
			product	hypothetical protein
			transl_table	11
7013	7990	CDS
			locus_tag	Harvfovirus_17_8
			product	hypothetical protein
			transl_table	11
8028	8924	CDS
			locus_tag	Harvfovirus_17_9
			product	hypothetical protein
			transl_table	11
10222	8927	CDS
			locus_tag	Harvfovirus_17_10
			product	hypothetical protein
			transl_table	11
11056	10271	CDS
			locus_tag	Harvfovirus_17_11
			product	hypothetical protein
			transl_table	11
11202	11729	CDS
			locus_tag	Harvfovirus_17_12
			product	hypothetical protein
			transl_table	11
11821	13323	CDS
			locus_tag	Harvfovirus_17_13
			product	hypothetical protein
			transl_table	11
14027	13302	CDS
			locus_tag	Harvfovirus_17_14
			product	hypothetical protein
			transl_table	11
15439	14105	CDS
			locus_tag	Harvfovirus_17_15
			product	uncharacterized protein Dyak_GE22356, isoform D
			transl_table	11
15580	16659	CDS
			locus_tag	Harvfovirus_17_16
			product	hypothetical protein PROFUN_07561
			transl_table	11
16714	17433	CDS
			locus_tag	Harvfovirus_17_17
			product	hypothetical protein
			transl_table	11
17493	18191	CDS
			locus_tag	Harvfovirus_17_18
			product	hypothetical protein
			transl_table	11
19228	18200	CDS
			locus_tag	Harvfovirus_17_19
			product	glycogen debranching enzyme alpha-1,6-glucosidase
			transl_table	11
19330	20430	CDS
			locus_tag	Harvfovirus_17_20
			product	hypothetical protein
			transl_table	11
20488	21138	CDS
			locus_tag	Harvfovirus_17_21
			product	hypothetical protein
			transl_table	11
21189	22004	CDS
			locus_tag	Harvfovirus_17_22
			product	hypothetical protein
			transl_table	11
22026	23663	CDS
			locus_tag	Harvfovirus_17_23
			product	putative rrp44-like exonuclease
			transl_table	11
23796	24191	CDS
			locus_tag	Harvfovirus_17_24
			product	hypothetical protein
			transl_table	11
24643	24236	CDS
			locus_tag	Harvfovirus_17_25
			product	hypothetical protein
			transl_table	11
24708	24815	CDS
			locus_tag	Harvfovirus_17_26
			product	hypothetical protein
			transl_table	11
>Harvfovirus_18
2	511	CDS
			locus_tag	Harvfovirus_18_1
			product	hypothetical protein
			transl_table	11
714	547	CDS
			locus_tag	Harvfovirus_18_2
			product	hypothetical protein
			transl_table	11
1008	1778	CDS
			locus_tag	Harvfovirus_18_3
			product	hypothetical protein
			transl_table	11
2854	1709	CDS
			locus_tag	Harvfovirus_18_4
			product	hypothetical protein Klosneuvirus_1_232
			transl_table	11
4098	2878	CDS
			locus_tag	Harvfovirus_18_5
			product	hypothetical protein Klosneuvirus_1_232
			transl_table	11
4429	4118	CDS
			locus_tag	Harvfovirus_18_6
			product	hypothetical protein
			transl_table	11
5138	4719	CDS
			locus_tag	Harvfovirus_18_7
			product	hypothetical protein
			transl_table	11
5234	5959	CDS
			locus_tag	Harvfovirus_18_8
			product	hypothetical protein
			transl_table	11
6017	6883	CDS
			locus_tag	Harvfovirus_18_9
			product	hypothetical protein
			transl_table	11
6926	7348	CDS
			locus_tag	Harvfovirus_18_10
			product	hypothetical protein
			transl_table	11
7394	8464	CDS
			locus_tag	Harvfovirus_18_11
			product	hypothetical protein
			transl_table	11
9046	8447	CDS
			locus_tag	Harvfovirus_18_12
			product	hypothetical protein
			transl_table	11
9668	9075	CDS
			locus_tag	Harvfovirus_18_13
			product	hypothetical protein
			transl_table	11
12099	9727	CDS
			locus_tag	Harvfovirus_18_14
			product	bifunctional AAA family ATPase chaperone/translocase BCS1
			transl_table	11
13412	12168	CDS
			locus_tag	Harvfovirus_18_15
			product	hypothetical protein Klosneuvirus_1_132
			transl_table	11
14479	14724	CDS
			locus_tag	Harvfovirus_18_16
			product	hypothetical protein
			transl_table	11
17867	17604	CDS
			locus_tag	Harvfovirus_18_17
			product	hypothetical protein
			transl_table	11
18797	19684	CDS
			locus_tag	Harvfovirus_18_18
			product	hypothetical protein
			transl_table	11
19726	20673	CDS
			locus_tag	Harvfovirus_18_19
			product	hypothetical protein
			transl_table	11
21311	20646	CDS
			locus_tag	Harvfovirus_18_20
			product	hypothetical protein
			transl_table	11
22803	21370	CDS
			locus_tag	Harvfovirus_18_21
			product	hypothetical protein
			transl_table	11
22932	23435	CDS
			locus_tag	Harvfovirus_18_22
			product	putative ORFan
			transl_table	11
23875	23402	CDS
			locus_tag	Harvfovirus_18_23
			product	hypothetical protein
			transl_table	11
>Harvfovirus_19
1433	153	CDS
			locus_tag	Harvfovirus_19_1
			product	hypothetical protein
			transl_table	11
2095	1520	CDS
			locus_tag	Harvfovirus_19_2
			product	hypothetical protein
			transl_table	11
2762	2166	CDS
			locus_tag	Harvfovirus_19_3
			product	hypothetical protein
			transl_table	11
3930	2839	CDS
			locus_tag	Harvfovirus_19_4
			product	hypothetical protein
			transl_table	11
4838	3975	CDS
			locus_tag	Harvfovirus_19_5
			product	hypothetical protein
			transl_table	11
4962	5426	CDS
			locus_tag	Harvfovirus_19_6
			product	hypothetical protein
			transl_table	11
6311	5445	CDS
			locus_tag	Harvfovirus_19_7
			product	hypothetical protein
			transl_table	11
6443	7786	CDS
			locus_tag	Harvfovirus_19_8
			product	hypothetical protein
			transl_table	11
8556	7783	CDS
			locus_tag	Harvfovirus_19_9
			product	hypothetical protein
			transl_table	11
9623	8604	CDS
			locus_tag	Harvfovirus_19_10
			product	hypothetical protein
			transl_table	11
9776	10198	CDS
			locus_tag	Harvfovirus_19_11
			product	PREDICTED: ankyrin repeat and MYND domain-containing protein 2
			transl_table	11
10258	10998	CDS
			locus_tag	Harvfovirus_19_12
			product	hypothetical protein
			transl_table	11
11420	10995	CDS
			locus_tag	Harvfovirus_19_13
			product	hypothetical protein
			transl_table	11
11765	11496	CDS
			locus_tag	Harvfovirus_19_14
			product	hypothetical protein
			transl_table	11
11893	13074	CDS
			locus_tag	Harvfovirus_19_15
			product	hypothetical protein BMW23_0855
			transl_table	11
14325	13087	CDS
			locus_tag	Harvfovirus_19_16
			product	PREDICTED: ankyrin repeat domain-containing protein 55
			transl_table	11
14334	15626	CDS
			locus_tag	Harvfovirus_19_17
			product	hypothetical protein
			transl_table	11
16842	15640	CDS
			locus_tag	Harvfovirus_19_18
			product	hypothetical protein
			transl_table	11
17209	17871	CDS
			locus_tag	Harvfovirus_19_19
			product	hypothetical protein
			transl_table	11
17941	18600	CDS
			locus_tag	Harvfovirus_19_20
			product	hypothetical protein
			transl_table	11
18667	19329	CDS
			locus_tag	Harvfovirus_19_21
			product	hypothetical protein
			transl_table	11
20132	19335	CDS
			locus_tag	Harvfovirus_19_22
			product	hypothetical protein
			transl_table	11
20833	20222	CDS
			locus_tag	Harvfovirus_19_23
			product	hypothetical protein
			transl_table	11
20976	22082	CDS
			locus_tag	Harvfovirus_19_24
			product	hypothetical protein
			transl_table	11
22158	22820	CDS
			locus_tag	Harvfovirus_19_25
			product	hypothetical protein
			transl_table	11
23878	22793	CDS
			locus_tag	Harvfovirus_19_26
			product	hypothetical protein
			transl_table	11
>Harvfovirus_20
450	121	CDS
			locus_tag	Harvfovirus_20_1
			product	hypothetical protein
			transl_table	11
586	1824	CDS
			locus_tag	Harvfovirus_20_2
			product	hypothetical protein
			transl_table	11
3711	1825	CDS
			locus_tag	Harvfovirus_20_3
			product	hypothetical protein
			transl_table	11
4560	3799	CDS
			locus_tag	Harvfovirus_20_4
			product	hypothetical protein
			transl_table	11
5365	4616	CDS
			locus_tag	Harvfovirus_20_5
			product	hypothetical protein
			transl_table	11
6143	5328	CDS
			locus_tag	Harvfovirus_20_6
			product	PREDICTED: E3 ubiquitin-protein ligase Os03g0188200-like
			transl_table	11
8331	6220	CDS
			locus_tag	Harvfovirus_20_7
			product	hypothetical protein
			transl_table	11
8365	8460	CDS
			locus_tag	Harvfovirus_20_8
			product	hypothetical protein
			transl_table	11
8453	9832	CDS
			locus_tag	Harvfovirus_20_9
			product	hypothetical protein
			transl_table	11
11686	9938	CDS
			locus_tag	Harvfovirus_20_10
			product	hypothetical protein
			transl_table	11
12387	11806	CDS
			locus_tag	Harvfovirus_20_11
			product	hypothetical protein
			transl_table	11
12367	12501	CDS
			locus_tag	Harvfovirus_20_12
			product	hypothetical protein
			transl_table	11
13440	14183	CDS
			locus_tag	Harvfovirus_20_13
			product	vacuolar protein-sorting-associated protein 33 homolog
			transl_table	11
15213	14203	CDS
			locus_tag	Harvfovirus_20_14
			product	hypothetical protein
			transl_table	11
16515	15262	CDS
			locus_tag	Harvfovirus_20_15
			product	hypothetical protein
			transl_table	11
16658	17899	CDS
			locus_tag	Harvfovirus_20_16
			product	hypothetical protein
			transl_table	11
18273	17896	CDS
			locus_tag	Harvfovirus_20_17
			product	hypothetical protein
			transl_table	11
19765	18404	CDS
			locus_tag	Harvfovirus_20_18
			product	chromosome condensation regulator
			transl_table	11
21186	19831	CDS
			locus_tag	Harvfovirus_20_19
			product	chromosome condensation regulator
			transl_table	11
21323	21490	CDS
			locus_tag	Harvfovirus_20_20
			product	hypothetical protein
			transl_table	11
21459	22805	CDS
			locus_tag	Harvfovirus_20_21
			product	packaging ATPase
			transl_table	11
22933	23358	CDS
			locus_tag	Harvfovirus_20_22
			product	hypothetical protein
			transl_table	11
>Harvfovirus_21
1	1221	CDS
			locus_tag	Harvfovirus_21_1
			product	DNA topoisomerase IA
			transl_table	11
1276	1731	CDS
			locus_tag	Harvfovirus_21_2
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
1790	2410	CDS
			locus_tag	Harvfovirus_21_3
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
2757	2407	CDS
			locus_tag	Harvfovirus_21_4
			product	putative zinc finger A20 and AN1 domain-containing stress-associated protein 8
			transl_table	11
2888	5635	CDS
			locus_tag	Harvfovirus_21_5
			product	D5-like helicase-primase
			transl_table	11
5707	6210	CDS
			locus_tag	Harvfovirus_21_6
			product	hypothetical protein
			transl_table	11
6258	8594	CDS
			locus_tag	Harvfovirus_21_7
			product	ADP-ribosyltransferase exoenzyme domain protein
			transl_table	11
9616	8591	CDS
			locus_tag	Harvfovirus_21_8
			product	hypothetical protein
			transl_table	11
9707	9982	CDS
			locus_tag	Harvfovirus_21_9
			product	hypothetical protein
			transl_table	11
11439	9979	CDS
			locus_tag	Harvfovirus_21_10
			product	serine/threonine protein kinase
			transl_table	11
11611	12477	CDS
			locus_tag	Harvfovirus_21_11
			product	hypothetical protein Catovirus_1_965
			transl_table	11
13737	12478	CDS
			locus_tag	Harvfovirus_21_12
			product	hypothetical protein Catovirus_1_964
			transl_table	11
13789	14475	CDS
			locus_tag	Harvfovirus_21_13
			product	hypothetical protein
			transl_table	11
14738	14466	CDS
			locus_tag	Harvfovirus_21_14
			product	hypothetical protein Catovirus_1_958
			transl_table	11
14821	15582	CDS
			locus_tag	Harvfovirus_21_15
			product	hypothetical protein
			transl_table	11
15653	15901	CDS
			locus_tag	Harvfovirus_21_16
			product	hypothetical protein
			transl_table	11
15942	16889	CDS
			locus_tag	Harvfovirus_21_17
			product	hypothetical protein
			transl_table	11
16923	19211	CDS
			locus_tag	Harvfovirus_21_18
			product	hypothetical protein Catovirus_1_954
			transl_table	11
19820	19227	CDS
			locus_tag	Harvfovirus_21_19
			product	hypothetical protein
			transl_table	11
20705	19887	CDS
			locus_tag	Harvfovirus_21_20
			product	hypothetical protein
			transl_table	11
22208	22086	CDS
			locus_tag	Harvfovirus_21_21
			product	hypothetical protein
			transl_table	11
22402	22692	CDS
			locus_tag	Harvfovirus_21_22
			product	NAD-dependent DNA ligase
			transl_table	11
>Harvfovirus_22
500	3	CDS
			locus_tag	Harvfovirus_22_1
			product	hypothetical protein
			transl_table	11
2762	669	CDS
			locus_tag	Harvfovirus_22_2
			product	desaturase
			transl_table	11
3030	3467	CDS
			locus_tag	Harvfovirus_22_3
			product	PREDICTED: vitamin K epoxide reductase complex subunit 1
			transl_table	11
4697	3456	CDS
			locus_tag	Harvfovirus_22_4
			product	hypothetical protein
			transl_table	11
4777	5733	CDS
			locus_tag	Harvfovirus_22_5
			product	hypothetical protein
			transl_table	11
5816	6751	CDS
			locus_tag	Harvfovirus_22_6
			product	hypothetical protein
			transl_table	11
6810	7271	CDS
			locus_tag	Harvfovirus_22_7
			product	hypothetical protein
			transl_table	11
7329	8420	CDS
			locus_tag	Harvfovirus_22_8
			product	hypothetical protein
			transl_table	11
8959	8417	CDS
			locus_tag	Harvfovirus_22_9
			product	hypothetical protein
			transl_table	11
9109	9852	CDS
			locus_tag	Harvfovirus_22_10
			product	hypothetical protein
			transl_table	11
10825	9845	CDS
			locus_tag	Harvfovirus_22_11
			product	hypothetical protein
			transl_table	11
10962	12137	CDS
			locus_tag	Harvfovirus_22_12
			product	chromosome condensation regulator, partial
			transl_table	11
12239	13447	CDS
			locus_tag	Harvfovirus_22_13
			product	chromosome condensation regulator
			transl_table	11
14183	13425	CDS
			locus_tag	Harvfovirus_22_14
			product	hypothetical protein
			transl_table	11
15473	14202	CDS
			locus_tag	Harvfovirus_22_15
			product	hypothetical protein
			transl_table	11
15919	15545	CDS
			locus_tag	Harvfovirus_22_16
			product	hypothetical protein
			transl_table	11
16049	16783	CDS
			locus_tag	Harvfovirus_22_17
			product	hypothetical protein
			transl_table	11
16846	17544	CDS
			locus_tag	Harvfovirus_22_18
			product	hypothetical protein
			transl_table	11
18098	17523	CDS
			locus_tag	Harvfovirus_22_19
			product	hypothetical protein
			transl_table	11
19545	18151	CDS
			locus_tag	Harvfovirus_22_20
			product	hypothetical protein
			transl_table	11
20656	19592	CDS
			locus_tag	Harvfovirus_22_21
			product	hypothetical protein
			transl_table	11
21697	20699	CDS
			locus_tag	Harvfovirus_22_22
			product	hypothetical protein
			transl_table	11
22375	21752	CDS
			locus_tag	Harvfovirus_22_23
			product	hypothetical protein
			transl_table	11
>Harvfovirus_23
1	150	CDS
			locus_tag	Harvfovirus_23_1
			product	hypothetical protein
			transl_table	11
888	595	CDS
			locus_tag	Harvfovirus_23_2
			product	hypothetical protein
			transl_table	11
1258	1139	CDS
			locus_tag	Harvfovirus_23_3
			product	hypothetical protein
			transl_table	11
1700	2053	CDS
			locus_tag	Harvfovirus_23_4
			product	hypothetical protein
			transl_table	11
2455	2976	CDS
			locus_tag	Harvfovirus_23_5
			product	hypothetical protein
			transl_table	11
3255	3635	CDS
			locus_tag	Harvfovirus_23_6
			product	ankyrin repeat domain-containing protein
			transl_table	11
4205	3954	CDS
			locus_tag	Harvfovirus_23_7
			product	hypothetical protein
			transl_table	11
5638	5114	CDS
			locus_tag	Harvfovirus_23_8
			product	DNA polymerase family X protein
			transl_table	11
6143	5658	CDS
			locus_tag	Harvfovirus_23_9
			product	DNA polymerase family X protein
			transl_table	11
6241	7575	CDS
			locus_tag	Harvfovirus_23_10
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
8294	7536	CDS
			locus_tag	Harvfovirus_23_11
			product	putative RNA methylase
			transl_table	11
8819	8328	CDS
			locus_tag	Harvfovirus_23_12
			product	hypothetical protein
			transl_table	11
8910	13334	CDS
			locus_tag	Harvfovirus_23_13
			product	DNA polymerase family B elongation subunit
			transl_table	11
14091	13336	CDS
			locus_tag	Harvfovirus_23_14
			product	hypothetical protein
			transl_table	11
14150	14740	CDS
			locus_tag	Harvfovirus_23_15
			product	hypothetical protein Catovirus_2_225
			transl_table	11
14771	21193	CDS
			locus_tag	Harvfovirus_23_16
			product	early transcription factor VETF large subunit
			transl_table	11
>Harvfovirus_24
1754	3	CDS
			locus_tag	Harvfovirus_24_1
			product	ankyrin and ring finger domain protein
			transl_table	11
1857	3791	CDS
			locus_tag	Harvfovirus_24_2
			product	arginyl-tRNA synthetase
			transl_table	11
4528	3788	CDS
			locus_tag	Harvfovirus_24_3
			product	CBN-EIF-1.A protein
			transl_table	11
5468	4578	CDS
			locus_tag	Harvfovirus_24_4
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
5587	6246	CDS
			locus_tag	Harvfovirus_24_5
			product	RNA recognition motif
			transl_table	11
7773	6220	CDS
			locus_tag	Harvfovirus_24_6
			product	putative rrp44-like exonuclease
			transl_table	11
8820	7888	CDS
			locus_tag	Harvfovirus_24_7
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
10138	8873	CDS
			locus_tag	Harvfovirus_24_8
			product	translation initiation factor 4A-III
			transl_table	11
11253	10210	CDS
			locus_tag	Harvfovirus_24_9
			product	hypothetical protein
			transl_table	11
11647	11297	CDS
			locus_tag	Harvfovirus_24_10
			product	hypothetical protein
			transl_table	11
11893	13593	CDS
			locus_tag	Harvfovirus_24_11
			product	peptidase
			transl_table	11
14495	13590	CDS
			locus_tag	Harvfovirus_24_12
			product	hypothetical protein
			transl_table	11
16129	14618	CDS
			locus_tag	Harvfovirus_24_13
			product	beta-lactamase superfamily domain
			transl_table	11
16295	17305	CDS
			locus_tag	Harvfovirus_24_14
			product	hypothetical protein Catovirus_2_122
			transl_table	11
18175	17264	CDS
			locus_tag	Harvfovirus_24_15
			product	hypothetical protein
			transl_table	11
19061	18201	CDS
			locus_tag	Harvfovirus_24_16
			product	hypothetical protein Catovirus_2_124
			transl_table	11
20933	19098	CDS
			locus_tag	Harvfovirus_24_17
			product	hypothetical protein Indivirus_2_58
			transl_table	11
>Harvfovirus_25
260	3	CDS
			locus_tag	Harvfovirus_25_1
			product	hypothetical protein
			transl_table	11
1225	320	CDS
			locus_tag	Harvfovirus_25_2
			product	hypothetical protein
			transl_table	11
10195	1277	CDS
			locus_tag	Harvfovirus_25_3
			product	hypothetical protein Hokovirus_1_255
			transl_table	11
11029	10403	CDS
			locus_tag	Harvfovirus_25_4
			product	hypothetical protein
			transl_table	11
11243	12736	CDS
			locus_tag	Harvfovirus_25_5
			product	chromosome condensation regulator
			transl_table	11
13938	12760	CDS
			locus_tag	Harvfovirus_25_6
			product	hypothetical protein
			transl_table	11
14073	14957	CDS
			locus_tag	Harvfovirus_25_7
			product	hypothetical protein
			transl_table	11
15339	16703	CDS
			locus_tag	Harvfovirus_25_8
			product	hypothetical protein Klosneuvirus_10_6
			transl_table	11
16836	16705	CDS
			locus_tag	Harvfovirus_25_9
			product	hypothetical protein
			transl_table	11
18493	17195	CDS
			locus_tag	Harvfovirus_25_10
			product	hypothetical protein
			transl_table	11
20475	20729	CDS
			locus_tag	Harvfovirus_25_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_26
3	185	CDS
			locus_tag	Harvfovirus_26_1
			product	hypothetical protein
			transl_table	11
225	1004	CDS
			locus_tag	Harvfovirus_26_2
			product	hypothetical protein
			transl_table	11
1062	1835	CDS
			locus_tag	Harvfovirus_26_3
			product	hypothetical protein
			transl_table	11
2517	1822	CDS
			locus_tag	Harvfovirus_26_4
			product	hypothetical protein
			transl_table	11
3528	2575	CDS
			locus_tag	Harvfovirus_26_5
			product	TIGR03118 family protein
			transl_table	11
4033	3767	CDS
			locus_tag	Harvfovirus_26_6
			product	hypothetical protein
			transl_table	11
5854	4043	CDS
			locus_tag	Harvfovirus_26_7
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
10375	9971	CDS
			locus_tag	Harvfovirus_26_8
			product	hypothetical protein
			transl_table	11
12296	11178	CDS
			locus_tag	Harvfovirus_26_9
			product	zinc finger protein
			transl_table	11
12443	13528	CDS
			locus_tag	Harvfovirus_26_10
			product	sulfite oxidase
			transl_table	11
13613	14395	CDS
			locus_tag	Harvfovirus_26_11
			product	histidine phosphatase domain-containing protein
			transl_table	11
14934	14425	CDS
			locus_tag	Harvfovirus_26_12
			product	hypothetical protein
			transl_table	11
16393	14993	CDS
			locus_tag	Harvfovirus_26_13
			product	hypothetical protein
			transl_table	11
16888	16466	CDS
			locus_tag	Harvfovirus_26_14
			product	hypothetical protein
			transl_table	11
16935	17096	CDS
			locus_tag	Harvfovirus_26_15
			product	hypothetical protein
			transl_table	11
17143	18657	CDS
			locus_tag	Harvfovirus_26_16
			product	hypothetical protein
			transl_table	11
19273	18611	CDS
			locus_tag	Harvfovirus_26_17
			product	hypothetical protein
			transl_table	11
19403	19837	CDS
			locus_tag	Harvfovirus_26_18
			product	hypothetical protein
			transl_table	11
20253	20417	CDS
			locus_tag	Harvfovirus_26_19
			product	hypothetical protein
			transl_table	11
>Harvfovirus_27
126	1	CDS
			locus_tag	Harvfovirus_27_1
			product	hypothetical protein
			transl_table	11
609	418	CDS
			locus_tag	Harvfovirus_27_2
			product	hypothetical protein
			transl_table	11
844	1239	CDS
			locus_tag	Harvfovirus_27_3
			product	hypothetical protein
			transl_table	11
1860	1411	CDS
			locus_tag	Harvfovirus_27_4
			product	hypothetical protein
			transl_table	11
1969	2463	CDS
			locus_tag	Harvfovirus_27_5
			product	hypothetical protein
			transl_table	11
3654	2485	CDS
			locus_tag	Harvfovirus_27_6
			product	hypothetical protein
			transl_table	11
6077	3798	CDS
			locus_tag	Harvfovirus_27_7
			product	MULTISPECIES: DUF262 domain-containing protein
			transl_table	11
6369	7013	CDS
			locus_tag	Harvfovirus_27_8
			product	hypothetical protein
			transl_table	11
7726	7076	CDS
			locus_tag	Harvfovirus_27_9
			product	hypothetical protein
			transl_table	11
8952	7795	CDS
			locus_tag	Harvfovirus_27_10
			product	hypothetical protein
			transl_table	11
10539	9007	CDS
			locus_tag	Harvfovirus_27_11
			product	hypothetical protein A2X63_04555
			transl_table	11
11508	10621	CDS
			locus_tag	Harvfovirus_27_12
			product	hypothetical protein
			transl_table	11
11873	11499	CDS
			locus_tag	Harvfovirus_27_13
			product	hypothetical protein
			transl_table	11
12133	12753	CDS
			locus_tag	Harvfovirus_27_14
			product	hypothetical protein
			transl_table	11
13390	12773	CDS
			locus_tag	Harvfovirus_27_15
			product	hypothetical protein
			transl_table	11
14956	13463	CDS
			locus_tag	Harvfovirus_27_16
			product	hypothetical protein CSB55_06140
			transl_table	11
15021	15560	CDS
			locus_tag	Harvfovirus_27_17
			product	hypothetical protein
			transl_table	11
15591	16421	CDS
			locus_tag	Harvfovirus_27_18
			product	hypothetical protein
			transl_table	11
16802	16689	CDS
			locus_tag	Harvfovirus_27_19
			product	hypothetical protein
			transl_table	11
17248	16859	CDS
			locus_tag	Harvfovirus_27_20
			product	hypothetical protein
			transl_table	11
17373	18002	CDS
			locus_tag	Harvfovirus_27_21
			product	hypothetical protein
			transl_table	11
18065	19291	CDS
			locus_tag	Harvfovirus_27_22
			product	hypothetical protein
			transl_table	11
19300	19443	CDS
			locus_tag	Harvfovirus_27_23
			product	hypothetical protein
			transl_table	11
19874	19440	CDS
			locus_tag	Harvfovirus_27_24
			product	hypothetical protein
			transl_table	11
>Harvfovirus_28
143	679	CDS
			locus_tag	Harvfovirus_28_1
			product	putative helicase/exonuclease
			transl_table	11
694	1575	CDS
			locus_tag	Harvfovirus_28_2
			product	putative helicase/exonuclease
			transl_table	11
1618	2331	CDS
			locus_tag	Harvfovirus_28_3
			product	hypothetical protein
			transl_table	11
2596	2318	CDS
			locus_tag	Harvfovirus_28_4
			product	hypothetical protein
			transl_table	11
2617	3588	CDS
			locus_tag	Harvfovirus_28_5
			product	hypothetical protein
			transl_table	11
3641	4087	CDS
			locus_tag	Harvfovirus_28_6
			product	zinc finger protein
			transl_table	11
6075	7211	CDS
			locus_tag	Harvfovirus_28_7
			product	zinc finger protein
			transl_table	11
9130	7214	CDS
			locus_tag	Harvfovirus_28_8
			product	hypothetical protein
			transl_table	11
9119	9562	CDS
			locus_tag	Harvfovirus_28_9
			product	hypothetical protein
			transl_table	11
9623	10876	CDS
			locus_tag	Harvfovirus_28_10
			product	tetratricopeptide repeat protein
			transl_table	11
10926	12194	CDS
			locus_tag	Harvfovirus_28_11
			product	tetratricopeptide repeat protein
			transl_table	11
12249	13385	CDS
			locus_tag	Harvfovirus_28_12
			product	sel1 repeat family protein
			transl_table	11
13396	14880	CDS
			locus_tag	Harvfovirus_28_13
			product	dehydrogenase
			transl_table	11
16609	16331	CDS
			locus_tag	Harvfovirus_28_14
			product	hypothetical protein
			transl_table	11
17001	17237	CDS
			locus_tag	Harvfovirus_28_15
			product	hypothetical protein
			transl_table	11
17495	17361	CDS
			locus_tag	Harvfovirus_28_16
			product	hypothetical protein
			transl_table	11
>Harvfovirus_29
1	1005	CDS
			locus_tag	Harvfovirus_29_1
			product	hypothetical protein
			transl_table	11
2167	1160	CDS
			locus_tag	Harvfovirus_29_2
			product	hypothetical protein
			transl_table	11
2360	2196	CDS
			locus_tag	Harvfovirus_29_3
			product	hypothetical protein
			transl_table	11
3907	2597	CDS
			locus_tag	Harvfovirus_29_4
			product	hypothetical protein
			transl_table	11
5230	4112	CDS
			locus_tag	Harvfovirus_29_5
			product	X-linked retinitis pigmentosa GTPase regulator isoform X1
			transl_table	11
6530	5544	CDS
			locus_tag	Harvfovirus_29_6
			product	hypothetical protein
			transl_table	11
6968	7582	CDS
			locus_tag	Harvfovirus_29_7
			product	hypothetical protein
			transl_table	11
9370	7574	CDS
			locus_tag	Harvfovirus_29_8
			product	hypothetical protein
			transl_table	11
10760	9447	CDS
			locus_tag	Harvfovirus_29_9
			product	hypothetical protein
			transl_table	11
10795	10890	CDS
			locus_tag	Harvfovirus_29_10
			product	hypothetical protein
			transl_table	11
11795	10986	CDS
			locus_tag	Harvfovirus_29_11
			product	hypothetical protein
			transl_table	11
12267	12458	CDS
			locus_tag	Harvfovirus_29_12
			product	hypothetical protein
			transl_table	11
12557	12820	CDS
			locus_tag	Harvfovirus_29_13
			product	hypothetical protein
			transl_table	11
13520	12924	CDS
			locus_tag	Harvfovirus_29_14
			product	hypothetical protein
			transl_table	11
14654	14460	CDS
			locus_tag	Harvfovirus_29_15
			product	hypothetical protein
			transl_table	11
15975	14725	CDS
			locus_tag	Harvfovirus_29_16
			product	hypothetical protein
			transl_table	11
16802	16945	CDS
			locus_tag	Harvfovirus_29_17
			product	hypothetical protein
			transl_table	11
>Harvfovirus_30
99	1	CDS
			locus_tag	Harvfovirus_30_1
			product	hypothetical protein
			transl_table	11
458	225	CDS
			locus_tag	Harvfovirus_30_2
			product	hypothetical protein
			transl_table	11
1526	606	CDS
			locus_tag	Harvfovirus_30_3
			product	hypothetical protein
			transl_table	11
1661	3367	CDS
			locus_tag	Harvfovirus_30_4
			product	hypothetical protein EMIHUDRAFT_119289
			transl_table	11
3705	4409	CDS
			locus_tag	Harvfovirus_30_5
			product	hypothetical protein
			transl_table	11
5545	4406	CDS
			locus_tag	Harvfovirus_30_6
			product	AAA family ATPase
			transl_table	11
6299	5649	CDS
			locus_tag	Harvfovirus_30_7
			product	hypothetical protein
			transl_table	11
7270	6371	CDS
			locus_tag	Harvfovirus_30_8
			product	putative ubiquitin (ribosomal protein L40)
			transl_table	11
7407	8252	CDS
			locus_tag	Harvfovirus_30_9
			product	hypothetical protein
			transl_table	11
9037	8264	CDS
			locus_tag	Harvfovirus_30_10
			product	hypothetical protein
			transl_table	11
9211	11220	CDS
			locus_tag	Harvfovirus_30_11
			product	protein of unknown function DUF4419
			transl_table	11
11310	12233	CDS
			locus_tag	Harvfovirus_30_12
			product	hypothetical protein
			transl_table	11
12291	12962	CDS
			locus_tag	Harvfovirus_30_13
			product	RNase adaptor protein
			transl_table	11
13035	15308	CDS
			locus_tag	Harvfovirus_30_14
			product	hypothetical protein F443_19289
			transl_table	11
15980	15318	CDS
			locus_tag	Harvfovirus_30_15
			product	hypothetical protein
			transl_table	11
16287	16081	CDS
			locus_tag	Harvfovirus_30_16
			product	hypothetical protein
			transl_table	11
>Harvfovirus_31
768	1	CDS
			locus_tag	Harvfovirus_31_1
			product	hypothetical protein
			transl_table	11
1500	787	CDS
			locus_tag	Harvfovirus_31_2
			product	hypothetical protein
			transl_table	11
2265	1561	CDS
			locus_tag	Harvfovirus_31_3
			product	hypothetical protein
			transl_table	11
3001	2324	CDS
			locus_tag	Harvfovirus_31_4
			product	hypothetical protein
			transl_table	11
3118	3612	CDS
			locus_tag	Harvfovirus_31_5
			product	hypothetical protein
			transl_table	11
4243	3623	CDS
			locus_tag	Harvfovirus_31_6
			product	hypothetical protein
			transl_table	11
4370	4687	CDS
			locus_tag	Harvfovirus_31_7
			product	hypothetical protein
			transl_table	11
4736	5938	CDS
			locus_tag	Harvfovirus_31_8
			product	hypothetical protein
			transl_table	11
6888	5941	CDS
			locus_tag	Harvfovirus_31_9
			product	hypothetical protein SAMN04487785_11743
			transl_table	11
7014	7943	CDS
			locus_tag	Harvfovirus_31_10
			product	hypothetical protein
			transl_table	11
7979	8959	CDS
			locus_tag	Harvfovirus_31_11
			product	hypothetical protein
			transl_table	11
9015	9788	CDS
			locus_tag	Harvfovirus_31_12
			product	hypothetical protein
			transl_table	11
9921	10859	CDS
			locus_tag	Harvfovirus_31_13
			product	hypothetical protein
			transl_table	11
10946	11362	CDS
			locus_tag	Harvfovirus_31_14
			product	hypothetical protein Catovirus_1_112
			transl_table	11
11494	12234	CDS
			locus_tag	Harvfovirus_31_15
			product	hypothetical protein
			transl_table	11
12814	12227	CDS
			locus_tag	Harvfovirus_31_16
			product	hypothetical protein
			transl_table	11
12904	13629	CDS
			locus_tag	Harvfovirus_31_17
			product	TNF receptor-associated factor 6
			transl_table	11
14160	13645	CDS
			locus_tag	Harvfovirus_31_18
			product	hypothetical protein
			transl_table	11
14727	14458	CDS
			locus_tag	Harvfovirus_31_19
			product	hypothetical protein
			transl_table	11
16281	14761	CDS
			locus_tag	Harvfovirus_31_20
			product	DNA primase
			transl_table	11
>Harvfovirus_32
1630	1821	CDS
			locus_tag	Harvfovirus_32_1
			product	hypothetical protein
			transl_table	11
4175	3999	CDS
			locus_tag	Harvfovirus_32_2
			product	hypothetical protein
			transl_table	11
6649	6230	CDS
			locus_tag	Harvfovirus_32_3
			product	hypothetical protein
			transl_table	11
6767	7099	CDS
			locus_tag	Harvfovirus_32_4
			product	hypothetical protein Catovirus_1_304
			transl_table	11
7112	7369	CDS
			locus_tag	Harvfovirus_32_5
			product	hypothetical protein
			transl_table	11
7452	8090	CDS
			locus_tag	Harvfovirus_32_6
			product	hypothetical protein
			transl_table	11
8662	8087	CDS
			locus_tag	Harvfovirus_32_7
			product	hypothetical protein
			transl_table	11
9157	8726	CDS
			locus_tag	Harvfovirus_32_8
			product	hypothetical protein
			transl_table	11
10235	9225	CDS
			locus_tag	Harvfovirus_32_9
			product	hypothetical protein
			transl_table	11
10798	10652	CDS
			locus_tag	Harvfovirus_32_10
			product	hypothetical protein
			transl_table	11
10857	11573	CDS
			locus_tag	Harvfovirus_32_11
			product	hypothetical protein
			transl_table	11
11661	12278	CDS
			locus_tag	Harvfovirus_32_12
			product	hypothetical protein
			transl_table	11
12608	12240	CDS
			locus_tag	Harvfovirus_32_13
			product	hypothetical protein
			transl_table	11
13050	12592	CDS
			locus_tag	Harvfovirus_32_14
			product	hypothetical protein
			transl_table	11
13758	13102	CDS
			locus_tag	Harvfovirus_32_15
			product	hypothetical protein
			transl_table	11
14622	13843	CDS
			locus_tag	Harvfovirus_32_16
			product	hypothetical protein
			transl_table	11
14695	14997	CDS
			locus_tag	Harvfovirus_32_17
			product	hypothetical protein
			transl_table	11
15294	16055	CDS
			locus_tag	Harvfovirus_32_18
			product	hypothetical protein
			transl_table	11
>Harvfovirus_33
1	1527	CDS
			locus_tag	Harvfovirus_33_1
			product	SWIB/MDM2 domain protein
			transl_table	11
1583	3499	CDS
			locus_tag	Harvfovirus_33_2
			product	hypothetical protein Catovirus_2_104
			transl_table	11
3564	4475	CDS
			locus_tag	Harvfovirus_33_3
			product	hypothetical protein
			transl_table	11
4767	4639	CDS
			locus_tag	Harvfovirus_33_4
			product	hypothetical protein
			transl_table	11
4924	5760	CDS
			locus_tag	Harvfovirus_33_5
			product	hypothetical protein
			transl_table	11
6487	5729	CDS
			locus_tag	Harvfovirus_33_6
			product	phosphorylcholine metabolism protein LicD
			transl_table	11
6569	9055	CDS
			locus_tag	Harvfovirus_33_7
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
9465	9052	CDS
			locus_tag	Harvfovirus_33_8
			product	translation initiation factor 2 subunit beta
			transl_table	11
9590	10873	CDS
			locus_tag	Harvfovirus_33_9
			product	DHH family phosphohydrolase
			transl_table	11
10938	11312	CDS
			locus_tag	Harvfovirus_33_10
			product	hypothetical protein
			transl_table	11
12078	11305	CDS
			locus_tag	Harvfovirus_33_11
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
12157	14544	CDS
			locus_tag	Harvfovirus_33_12
			product	DEXDc helicase
			transl_table	11
15075	14581	CDS
			locus_tag	Harvfovirus_33_13
			product	hypothetical protein
			transl_table	11
>Harvfovirus_34
657	160	CDS
			locus_tag	Harvfovirus_34_1
			product	hypothetical protein
			transl_table	11
1666	692	CDS
			locus_tag	Harvfovirus_34_2
			product	hypothetical protein SAMD00019534_038080
			transl_table	11
2901	1723	CDS
			locus_tag	Harvfovirus_34_3
			product	chromosome condensation regulator
			transl_table	11
3667	2966	CDS
			locus_tag	Harvfovirus_34_4
			product	hypothetical protein
			transl_table	11
4720	3725	CDS
			locus_tag	Harvfovirus_34_5
			product	hypothetical protein
			transl_table	11
5276	5491	CDS
			locus_tag	Harvfovirus_34_6
			product	hypothetical protein
			transl_table	11
6253	5690	CDS
			locus_tag	Harvfovirus_34_7
			product	hypothetical protein
			transl_table	11
6414	6569	CDS
			locus_tag	Harvfovirus_34_8
			product	hypothetical protein
			transl_table	11
6566	7267	CDS
			locus_tag	Harvfovirus_34_9
			product	hypothetical protein
			transl_table	11
8490	7306	CDS
			locus_tag	Harvfovirus_34_10
			product	hypothetical protein
			transl_table	11
9732	8560	CDS
			locus_tag	Harvfovirus_34_11
			product	hypothetical protein
			transl_table	11
10740	9748	CDS
			locus_tag	Harvfovirus_34_12
			product	hypothetical protein
			transl_table	11
13208	12012	CDS
			locus_tag	Harvfovirus_34_13
			product	zinc finger protein
			transl_table	11
13784	13686	CDS
			locus_tag	Harvfovirus_34_14
			product	hypothetical protein
			transl_table	11
14490	13792	CDS
			locus_tag	Harvfovirus_34_15
			product	hypothetical protein
			transl_table	11
14631	14813	CDS
			locus_tag	Harvfovirus_34_16
			product	hypothetical protein
			transl_table	11
>Harvfovirus_35
10221	10151	tRNA	Harvfovirus_tRNA_10
			product	tRNA-Ile(TAT)
3	563	CDS
			locus_tag	Harvfovirus_35_1
			product	hypothetical protein
			transl_table	11
614	1222	CDS
			locus_tag	Harvfovirus_35_2
			product	PREDICTED: dual specificity protein phosphatase 1B-like isoform X1
			transl_table	11
3223	1211	CDS
			locus_tag	Harvfovirus_35_3
			product	hypothetical protein
			transl_table	11
3485	3294	CDS
			locus_tag	Harvfovirus_35_4
			product	hypothetical protein
			transl_table	11
3613	4455	CDS
			locus_tag	Harvfovirus_35_5
			product	hypothetical protein
			transl_table	11
4942	4481	CDS
			locus_tag	Harvfovirus_35_6
			product	hypothetical protein
			transl_table	11
4976	5386	CDS
			locus_tag	Harvfovirus_35_7
			product	hypothetical protein
			transl_table	11
5441	6058	CDS
			locus_tag	Harvfovirus_35_8
			product	hypothetical protein
			transl_table	11
6112	7314	CDS
			locus_tag	Harvfovirus_35_9
			product	chromosome condensation regulator
			transl_table	11
7768	7307	CDS
			locus_tag	Harvfovirus_35_10
			product	hypothetical protein
			transl_table	11
7878	7771	CDS
			locus_tag	Harvfovirus_35_11
			product	hypothetical protein
			transl_table	11
7984	8466	CDS
			locus_tag	Harvfovirus_35_12
			product	hypothetical protein
			transl_table	11
9418	8432	CDS
			locus_tag	Harvfovirus_35_13
			product	hypothetical protein
			transl_table	11
9960	9472	CDS
			locus_tag	Harvfovirus_35_14
			product	hypothetical protein
			transl_table	11
10830	10243	CDS
			locus_tag	Harvfovirus_35_15
			product	Hypothetical protein ORPV_249
			transl_table	11
10962	12092	CDS
			locus_tag	Harvfovirus_35_16
			product	PREDICTED: heterochromatin-associated protein MENT-like
			transl_table	11
12328	12837	CDS
			locus_tag	Harvfovirus_35_17
			product	hypothetical protein
			transl_table	11
13982	12834	CDS
			locus_tag	Harvfovirus_35_18
			product	hypothetical protein COT14_01465
			transl_table	11
>Harvfovirus_36
2	1057	CDS
			locus_tag	Harvfovirus_36_1
			product	hypothetical protein
			transl_table	11
1134	2396	CDS
			locus_tag	Harvfovirus_36_2
			product	hypothetical protein
			transl_table	11
2838	2407	CDS
			locus_tag	Harvfovirus_36_3
			product	hypothetical protein
			transl_table	11
3611	2907	CDS
			locus_tag	Harvfovirus_36_4
			product	hypothetical protein
			transl_table	11
3733	4596	CDS
			locus_tag	Harvfovirus_36_5
			product	hypothetical protein
			transl_table	11
4649	5161	CDS
			locus_tag	Harvfovirus_36_6
			product	hypothetical protein
			transl_table	11
6263	5199	CDS
			locus_tag	Harvfovirus_36_7
			product	hypothetical protein
			transl_table	11
6357	8495	CDS
			locus_tag	Harvfovirus_36_8
			product	putative ORFan
			transl_table	11
8772	8497	CDS
			locus_tag	Harvfovirus_36_9
			product	hypothetical protein C5B55_09005
			transl_table	11
8858	9307	CDS
			locus_tag	Harvfovirus_36_10
			product	hypothetical protein
			transl_table	11
9377	9973	CDS
			locus_tag	Harvfovirus_36_11
			product	hypothetical protein
			transl_table	11
10101	10853	CDS
			locus_tag	Harvfovirus_36_12
			product	hypothetical protein
			transl_table	11
10870	12177	CDS
			locus_tag	Harvfovirus_36_13
			product	hypothetical protein
			transl_table	11
13313	12165	CDS
			locus_tag	Harvfovirus_36_14
			product	hypothetical protein
			transl_table	11
13810	13367	CDS
			locus_tag	Harvfovirus_36_15
			product	hypothetical protein
			transl_table	11
14508	14308	CDS
			locus_tag	Harvfovirus_36_16
			product	hypothetical protein
			transl_table	11
14582	14758	CDS
			locus_tag	Harvfovirus_36_17
			product	NAD dependent epimerase/dehydratase family protein
			transl_table	11
>Harvfovirus_37
3	803	CDS
			locus_tag	Harvfovirus_37_1
			product	chromosome condensation regulator
			transl_table	11
1490	816	CDS
			locus_tag	Harvfovirus_37_2
			product	hypothetical protein
			transl_table	11
2416	1559	CDS
			locus_tag	Harvfovirus_37_3
			product	hypothetical protein
			transl_table	11
2560	2904	CDS
			locus_tag	Harvfovirus_37_4
			product	hypothetical protein
			transl_table	11
3799	2882	CDS
			locus_tag	Harvfovirus_37_5
			product	hypothetical protein
			transl_table	11
3889	5133	CDS
			locus_tag	Harvfovirus_37_6
			product	hypothetical protein
			transl_table	11
5180	5344	CDS
			locus_tag	Harvfovirus_37_7
			product	hypothetical protein
			transl_table	11
5473	5895	CDS
			locus_tag	Harvfovirus_37_8
			product	hypothetical protein
			transl_table	11
6240	5899	CDS
			locus_tag	Harvfovirus_37_9
			product	hypothetical protein
			transl_table	11
6355	7194	CDS
			locus_tag	Harvfovirus_37_10
			product	hypothetical protein
			transl_table	11
7224	7334	CDS
			locus_tag	Harvfovirus_37_11
			product	hypothetical protein
			transl_table	11
8201	7398	CDS
			locus_tag	Harvfovirus_37_12
			product	hypothetical protein
			transl_table	11
9148	8264	CDS
			locus_tag	Harvfovirus_37_13
			product	hypothetical protein
			transl_table	11
9989	9216	CDS
			locus_tag	Harvfovirus_37_14
			product	hypothetical protein
			transl_table	11
10134	10718	CDS
			locus_tag	Harvfovirus_37_15
			product	hypothetical protein
			transl_table	11
10981	10715	CDS
			locus_tag	Harvfovirus_37_16
			product	hypothetical protein
			transl_table	11
11801	11658	CDS
			locus_tag	Harvfovirus_37_17
			product	hypothetical protein
			transl_table	11
13208	13056	CDS
			locus_tag	Harvfovirus_37_18
			product	hypothetical protein
			transl_table	11
13721	13323	CDS
			locus_tag	Harvfovirus_37_19
			product	hypothetical protein
			transl_table	11
14118	14372	CDS
			locus_tag	Harvfovirus_37_20
			product	UvrD/REP helicase family protein
			transl_table	11
>Harvfovirus_38
3	275	CDS
			locus_tag	Harvfovirus_38_1
			product	hypothetical protein
			transl_table	11
1585	272	CDS
			locus_tag	Harvfovirus_38_2
			product	hypothetical protein
			transl_table	11
2147	1644	CDS
			locus_tag	Harvfovirus_38_3
			product	hypothetical protein
			transl_table	11
2245	3396	CDS
			locus_tag	Harvfovirus_38_4
			product	hypothetical protein
			transl_table	11
4809	3424	CDS
			locus_tag	Harvfovirus_38_5
			product	PREDICTED: TNF receptor-associated factor 6 isoform X1
			transl_table	11
5380	4877	CDS
			locus_tag	Harvfovirus_38_6
			product	hypothetical protein
			transl_table	11
6722	5391	CDS
			locus_tag	Harvfovirus_38_7
			product	hypothetical protein
			transl_table	11
6833	7135	CDS
			locus_tag	Harvfovirus_38_8
			product	hypothetical protein
			transl_table	11
7194	7724	CDS
			locus_tag	Harvfovirus_38_9
			product	hypothetical protein
			transl_table	11
7726	7860	CDS
			locus_tag	Harvfovirus_38_10
			product	hypothetical protein
			transl_table	11
9045	8293	CDS
			locus_tag	Harvfovirus_38_11
			product	hypothetical protein
			transl_table	11
11579	9108	CDS
			locus_tag	Harvfovirus_38_12
			product	pre-mRNA-splicing factor ATP-dependent RNA helicase PRP16
			transl_table	11
11847	12596	CDS
			locus_tag	Harvfovirus_38_13
			product	hypothetical protein
			transl_table	11
13679	12570	CDS
			locus_tag	Harvfovirus_38_14
			product	Hypothetical protein A7982_10443
			transl_table	11
14122	13760	CDS
			locus_tag	Harvfovirus_38_15
			product	hypothetical protein
			transl_table	11
>Harvfovirus_39
1	330	CDS
			locus_tag	Harvfovirus_39_1
			product	hypothetical protein
			transl_table	11
368	775	CDS
			locus_tag	Harvfovirus_39_2
			product	hypothetical protein
			transl_table	11
792	1724	CDS
			locus_tag	Harvfovirus_39_3
			product	NADPH-dependent thioredoxin reductase
			transl_table	11
2303	1719	CDS
			locus_tag	Harvfovirus_39_4
			product	hypothetical protein
			transl_table	11
4135	2369	CDS
			locus_tag	Harvfovirus_39_5
			product	hypothetical protein
			transl_table	11
4993	4184	CDS
			locus_tag	Harvfovirus_39_6
			product	hypothetical protein
			transl_table	11
5475	5047	CDS
			locus_tag	Harvfovirus_39_7
			product	hypothetical protein
			transl_table	11
6231	5527	CDS
			locus_tag	Harvfovirus_39_8
			product	hypothetical protein
			transl_table	11
6308	7189	CDS
			locus_tag	Harvfovirus_39_9
			product	hypothetical protein
			transl_table	11
7780	8445	CDS
			locus_tag	Harvfovirus_39_10
			product	hypothetical protein
			transl_table	11
8943	8431	CDS
			locus_tag	Harvfovirus_39_11
			product	hypothetical protein
			transl_table	11
9959	8979	CDS
			locus_tag	Harvfovirus_39_12
			product	WD repeat protein
			transl_table	11
10088	10873	CDS
			locus_tag	Harvfovirus_39_13
			product	hypothetical protein
			transl_table	11
12979	10874	CDS
			locus_tag	Harvfovirus_39_14
			product	Hypothetical Protein FCC1311_031102
			transl_table	11
13042	13554	CDS
			locus_tag	Harvfovirus_39_15
			product	hypothetical protein
			transl_table	11
13766	13545	CDS
			locus_tag	Harvfovirus_39_16
			product	hypothetical protein
			transl_table	11
>Harvfovirus_40
528	1	CDS
			locus_tag	Harvfovirus_40_1
			product	hypothetical protein
			transl_table	11
658	1272	CDS
			locus_tag	Harvfovirus_40_2
			product	hypothetical protein
			transl_table	11
1338	1652	CDS
			locus_tag	Harvfovirus_40_3
			product	hypothetical protein
			transl_table	11
1662	1988	CDS
			locus_tag	Harvfovirus_40_4
			product	hypothetical protein
			transl_table	11
2443	1985	CDS
			locus_tag	Harvfovirus_40_5
			product	hypothetical protein
			transl_table	11
3600	2503	CDS
			locus_tag	Harvfovirus_40_6
			product	hypothetical protein
			transl_table	11
4741	3863	CDS
			locus_tag	Harvfovirus_40_7
			product	hypothetical protein NIES204_43580 (plasmid)
			transl_table	11
4764	5996	CDS
			locus_tag	Harvfovirus_40_8
			product	radical SAM protein
			transl_table	11
6015	6476	CDS
			locus_tag	Harvfovirus_40_9
			product	hypothetical protein
			transl_table	11
7599	6466	CDS
			locus_tag	Harvfovirus_40_10
			product	glycogen debranching enzyme alpha-1,6-glucosidase
			transl_table	11
7728	8027	CDS
			locus_tag	Harvfovirus_40_11
			product	hypothetical protein
			transl_table	11
8075	9025	CDS
			locus_tag	Harvfovirus_40_12
			product	hypothetical protein Klosneuvirus_1_331
			transl_table	11
9089	9730	CDS
			locus_tag	Harvfovirus_40_13
			product	hypothetical protein
			transl_table	11
9792	10664	CDS
			locus_tag	Harvfovirus_40_14
			product	hypothetical protein
			transl_table	11
11268	10654	CDS
			locus_tag	Harvfovirus_40_15
			product	Carbonic anhydrase 2
			transl_table	11
12225	11293	CDS
			locus_tag	Harvfovirus_40_16
			product	hypothetical protein
			transl_table	11
12485	12778	CDS
			locus_tag	Harvfovirus_40_17
			product	hypothetical protein
			transl_table	11
12898	13035	CDS
			locus_tag	Harvfovirus_40_18
			product	hypothetical protein
			transl_table	11
>Harvfovirus_41
3	137	CDS
			locus_tag	Harvfovirus_41_1
			product	hypothetical protein
			transl_table	11
234	863	CDS
			locus_tag	Harvfovirus_41_2
			product	hypothetical protein
			transl_table	11
985	2202	CDS
			locus_tag	Harvfovirus_41_3
			product	chromosome condensation regulator, partial
			transl_table	11
2263	3669	CDS
			locus_tag	Harvfovirus_41_4
			product	chromosome condensation regulator
			transl_table	11
3828	4070	CDS
			locus_tag	Harvfovirus_41_5
			product	hypothetical protein
			transl_table	11
4177	5001	CDS
			locus_tag	Harvfovirus_41_6
			product	hypothetical protein
			transl_table	11
5597	5064	CDS
			locus_tag	Harvfovirus_41_7
			product	hypothetical protein
			transl_table	11
5775	6161	CDS
			locus_tag	Harvfovirus_41_8
			product	hypothetical protein
			transl_table	11
6616	6128	CDS
			locus_tag	Harvfovirus_41_9
			product	hypothetical protein
			transl_table	11
7316	6687	CDS
			locus_tag	Harvfovirus_41_10
			product	hypothetical protein
			transl_table	11
7942	7808	CDS
			locus_tag	Harvfovirus_41_11
			product	hypothetical protein
			transl_table	11
8282	8530	CDS
			locus_tag	Harvfovirus_41_12
			product	hypothetical protein
			transl_table	11
8616	11540	CDS
			locus_tag	Harvfovirus_41_13
			product	DNA primase
			transl_table	11
12210	12067	CDS
			locus_tag	Harvfovirus_41_14
			product	hypothetical protein
			transl_table	11
12976	12236	CDS
			locus_tag	Harvfovirus_41_15
			product	hypothetical protein
			transl_table	11
>Harvfovirus_42
438	1538	CDS
			locus_tag	Harvfovirus_42_1
			product	hypothetical protein YSLV6_ORF11
			transl_table	11
3818	1539	CDS
			locus_tag	Harvfovirus_42_2
			product	N-6 DNA methylase
			transl_table	11
4196	4023	CDS
			locus_tag	Harvfovirus_42_3
			product	hypothetical protein
			transl_table	11
4484	4359	CDS
			locus_tag	Harvfovirus_42_4
			product	hypothetical protein
			transl_table	11
5470	4484	CDS
			locus_tag	Harvfovirus_42_5
			product	hypothetical protein
			transl_table	11
9654	5623	CDS
			locus_tag	Harvfovirus_42_6
			product	DNA repair exonuclease
			transl_table	11
11157	9775	CDS
			locus_tag	Harvfovirus_42_7
			product	hypothetical protein
			transl_table	11
11339	11881	CDS
			locus_tag	Harvfovirus_42_8
			product	hypothetical protein
			transl_table	11
12388	12591	CDS
			locus_tag	Harvfovirus_42_9
			product	hypothetical protein
			transl_table	11
>Harvfovirus_43
1801	1871	tRNA	Harvfovirus_tRNA_11
			product	tRNA-Pseudo(AGC)
1729	1799	tRNA	Harvfovirus_tRNA_12
			product	tRNA-Gly(GCC)
1658	1728	tRNA	Harvfovirus_tRNA_13
			product	tRNA-Gly(TCC)
1	1329	CDS
			locus_tag	Harvfovirus_43_1
			product	hypothetical protein
			transl_table	11
1596	1330	CDS
			locus_tag	Harvfovirus_43_2
			product	hypothetical protein
			transl_table	11
1777	1646	CDS
			locus_tag	Harvfovirus_43_3
			product	hypothetical protein
			transl_table	11
1944	2867	CDS
			locus_tag	Harvfovirus_43_4
			product	hypothetical protein Catovirus_1_66
			transl_table	11
4384	2879	CDS
			locus_tag	Harvfovirus_43_5
			product	putative AAA+ family ATPase
			transl_table	11
4511	6178	CDS
			locus_tag	Harvfovirus_43_6
			product	hypothetical protein Klosneuvirus_1_394
			transl_table	11
6837	6175	CDS
			locus_tag	Harvfovirus_43_7
			product	hypothetical protein DLAC_09573
			transl_table	11
6940	8052	CDS
			locus_tag	Harvfovirus_43_8
			product	hypothetical protein
			transl_table	11
8062	9390	CDS
			locus_tag	Harvfovirus_43_9
			product	hypothetical protein PENARI_c033G06413
			transl_table	11
10067	9387	CDS
			locus_tag	Harvfovirus_43_10
			product	prolipoprotein diacylglyceryl transferase
			transl_table	11
10441	10094	CDS
			locus_tag	Harvfovirus_43_11
			product	hypothetical protein
			transl_table	11
11625	10459	CDS
			locus_tag	Harvfovirus_43_12
			product	ubiquinone biosynthesis protein, partial
			transl_table	11
12212	11631	CDS
			locus_tag	Harvfovirus_43_13
			product	hypothetical protein
			transl_table	11
12285	12446	CDS
			locus_tag	Harvfovirus_43_14
			product	hypothetical protein
			transl_table	11
>Harvfovirus_44
1	234	CDS
			locus_tag	Harvfovirus_44_1
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
393	2012	CDS
			locus_tag	Harvfovirus_44_2
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
2103	2993	CDS
			locus_tag	Harvfovirus_44_3
			product	putative prophage protein
			transl_table	11
3433	5751	CDS
			locus_tag	Harvfovirus_44_4
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
6586	6891	CDS
			locus_tag	Harvfovirus_44_5
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
7000	8148	CDS
			locus_tag	Harvfovirus_44_6
			product	hypothetical protein
			transl_table	11
8367	9446	CDS
			locus_tag	Harvfovirus_44_7
			product	hypothetical protein
			transl_table	11
9605	10453	CDS
			locus_tag	Harvfovirus_44_8
			product	hypothetical protein
			transl_table	11
10570	11406	CDS
			locus_tag	Harvfovirus_44_9
			product	hypothetical protein
			transl_table	11
12356	11421	CDS
			locus_tag	Harvfovirus_44_10
			product	DHH family phosphohydrolase
			transl_table	11
12544	12434	CDS
			locus_tag	Harvfovirus_44_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_45
3	971	CDS
			locus_tag	Harvfovirus_45_1
			product	hypothetical protein
			transl_table	11
1039	2148	CDS
			locus_tag	Harvfovirus_45_2
			product	hypothetical protein
			transl_table	11
2205	3248	CDS
			locus_tag	Harvfovirus_45_3
			product	hypothetical protein
			transl_table	11
4335	3208	CDS
			locus_tag	Harvfovirus_45_4
			product	hypothetical protein
			transl_table	11
5570	4419	CDS
			locus_tag	Harvfovirus_45_5
			product	hypothetical protein
			transl_table	11
5707	5898	CDS
			locus_tag	Harvfovirus_45_6
			product	hypothetical protein
			transl_table	11
5936	6541	CDS
			locus_tag	Harvfovirus_45_7
			product	hypothetical protein
			transl_table	11
6738	6538	CDS
			locus_tag	Harvfovirus_45_8
			product	hypothetical protein
			transl_table	11
6744	7466	CDS
			locus_tag	Harvfovirus_45_9
			product	hypothetical protein
			transl_table	11
7523	9679	CDS
			locus_tag	Harvfovirus_45_10
			product	hypothetical protein UW31_C0007G0013
			transl_table	11
9733	10548	CDS
			locus_tag	Harvfovirus_45_11
			product	Esterase lipase superfamily protein
			transl_table	11
10615	11466	CDS
			locus_tag	Harvfovirus_45_12
			product	hypothetical protein
			transl_table	11
11525	12166	CDS
			locus_tag	Harvfovirus_45_13
			product	hypothetical protein
			transl_table	11
12398	12544	CDS
			locus_tag	Harvfovirus_45_14
			product	hypothetical protein
			transl_table	11
>Harvfovirus_46
1	1128	CDS
			locus_tag	Harvfovirus_46_1
			product	NADAR family protein
			transl_table	11
1313	1146	CDS
			locus_tag	Harvfovirus_46_2
			product	hypothetical protein
			transl_table	11
2231	1353	CDS
			locus_tag	Harvfovirus_46_3
			product	hypothetical protein
			transl_table	11
2851	2270	CDS
			locus_tag	Harvfovirus_46_4
			product	hypothetical protein
			transl_table	11
2889	3605	CDS
			locus_tag	Harvfovirus_46_5
			product	hypothetical protein
			transl_table	11
3841	3596	CDS
			locus_tag	Harvfovirus_46_6
			product	hypothetical protein MegaChil _gp0024
			transl_table	11
3995	5545	CDS
			locus_tag	Harvfovirus_46_7
			product	hypothetical protein
			transl_table	11
5597	6586	CDS
			locus_tag	Harvfovirus_46_8
			product	hypothetical protein
			transl_table	11
8114	6669	CDS
			locus_tag	Harvfovirus_46_9
			product	hypothetical protein
			transl_table	11
8108	8206	CDS
			locus_tag	Harvfovirus_46_10
			product	hypothetical protein
			transl_table	11
8218	10584	CDS
			locus_tag	Harvfovirus_46_11
			product	hypothetical protein PTSG_01414
			transl_table	11
11405	10833	CDS
			locus_tag	Harvfovirus_46_12
			product	hypothetical protein
			transl_table	11
11843	11929	CDS
			locus_tag	Harvfovirus_46_13
			product	hypothetical protein
			transl_table	11
>Harvfovirus_47
963	199	CDS
			locus_tag	Harvfovirus_47_1
			product	uracil-DNA glycosylase
			transl_table	11
1935	1009	CDS
			locus_tag	Harvfovirus_47_2
			product	kelch-like protein 7
			transl_table	11
2065	3525	CDS
			locus_tag	Harvfovirus_47_3
			product	transcription initiation factor IIB
			transl_table	11
6790	3536	CDS
			locus_tag	Harvfovirus_47_4
			product	ATP-dependent Lon protease
			transl_table	11
6976	7791	CDS
			locus_tag	Harvfovirus_47_5
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
7816	9003	CDS
			locus_tag	Harvfovirus_47_6
			product	hypothetical protein Catovirus_1_1049
			transl_table	11
9015	9233	CDS
			locus_tag	Harvfovirus_47_7
			product	hypothetical protein
			transl_table	11
11570	9207	CDS
			locus_tag	Harvfovirus_47_8
			product	Hsp70 protein
			transl_table	11
>Harvfovirus_48
1270	2	CDS
			locus_tag	Harvfovirus_48_1
			product	hypothetical protein RclHR1_01350014
			transl_table	11
1393	2394	CDS
			locus_tag	Harvfovirus_48_2
			product	hypothetical protein
			transl_table	11
3729	2362	CDS
			locus_tag	Harvfovirus_48_3
			product	hypothetical protein
			transl_table	11
3754	4695	CDS
			locus_tag	Harvfovirus_48_4
			product	hypothetical protein
			transl_table	11
4751	5926	CDS
			locus_tag	Harvfovirus_48_5
			product	hypothetical protein BATDEDRAFT_11439, partial
			transl_table	11
6510	5932	CDS
			locus_tag	Harvfovirus_48_6
			product	hypothetical protein
			transl_table	11
6652	8142	CDS
			locus_tag	Harvfovirus_48_7
			product	hypothetical protein
			transl_table	11
8154	9239	CDS
			locus_tag	Harvfovirus_48_8
			product	cupin domain-containing protein
			transl_table	11
9523	9236	CDS
			locus_tag	Harvfovirus_48_9
			product	hypothetical protein
			transl_table	11
9662	9862	CDS
			locus_tag	Harvfovirus_48_10
			product	hypothetical protein
			transl_table	11
10808	9870	CDS
			locus_tag	Harvfovirus_48_11
			product	L-galactose dehydrogenase
			transl_table	11
10921	11505	CDS
			locus_tag	Harvfovirus_48_12
			product	hypothetical protein
			transl_table	11
>Harvfovirus_49
3	4670	CDS
			locus_tag	Harvfovirus_49_1
			product	HrpA-like RNA helicase
			transl_table	11
4694	6097	CDS
			locus_tag	Harvfovirus_49_2
			product	hypothetical protein Klosneuvirus_2_187
			transl_table	11
6084	6464	CDS
			locus_tag	Harvfovirus_49_3
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
9142	6905	CDS
			locus_tag	Harvfovirus_49_4
			product	ankyrin repeat protein
			transl_table	11
9656	9144	CDS
			locus_tag	Harvfovirus_49_5
			product	hypothetical protein Catovirus_2_178
			transl_table	11
9751	10239	CDS
			locus_tag	Harvfovirus_49_6
			product	SNF2-like helicase
			transl_table	11
10946	11089	CDS
			locus_tag	Harvfovirus_49_7
			product	homing endonuclease
			transl_table	11
>Harvfovirus_50
419	3	CDS
			locus_tag	Harvfovirus_50_1
			product	ribose-phosphate pyrophosphokinase
			transl_table	11
643	2034	CDS
			locus_tag	Harvfovirus_50_2
			product	nicotinate phosphoribosyltransferase
			transl_table	11
2453	2031	CDS
			locus_tag	Harvfovirus_50_3
			product	hypothetical protein
			transl_table	11
2575	2880	CDS
			locus_tag	Harvfovirus_50_4
			product	hypothetical protein
			transl_table	11
2946	4583	CDS
			locus_tag	Harvfovirus_50_5
			product	hypothetical protein
			transl_table	11
4646	5908	CDS
			locus_tag	Harvfovirus_50_6
			product	DnaJ-like subfamily C member 3
			transl_table	11
7292	5868	CDS
			locus_tag	Harvfovirus_50_7
			product	hypothetical protein
			transl_table	11
8168	7308	CDS
			locus_tag	Harvfovirus_50_8
			product	hypothetical protein
			transl_table	11
8264	8908	CDS
			locus_tag	Harvfovirus_50_9
			product	hypothetical protein
			transl_table	11
9437	8925	CDS
			locus_tag	Harvfovirus_50_10
			product	hypothetical protein
			transl_table	11
10844	11041	CDS
			locus_tag	Harvfovirus_50_11
			product	ankyrin repeat protein
			transl_table	11
>Harvfovirus_51
1	564	CDS
			locus_tag	Harvfovirus_51_1
			product	RING finger and CHY zinc finger domain-containing protein 1
			transl_table	11
579	1127	CDS
			locus_tag	Harvfovirus_51_2
			product	HNH endonuclease
			transl_table	11
1480	1124	CDS
			locus_tag	Harvfovirus_51_3
			product	hypothetical protein
			transl_table	11
1546	1806	CDS
			locus_tag	Harvfovirus_51_4
			product	hypothetical protein
			transl_table	11
2432	1803	CDS
			locus_tag	Harvfovirus_51_5
			product	hypothetical protein
			transl_table	11
3465	2464	CDS
			locus_tag	Harvfovirus_51_6
			product	putative pAp phosphatase
			transl_table	11
3541	5436	CDS
			locus_tag	Harvfovirus_51_7
			product	hypothetical protein
			transl_table	11
6001	5438	CDS
			locus_tag	Harvfovirus_51_8
			product	hypothetical protein
			transl_table	11
6132	6662	CDS
			locus_tag	Harvfovirus_51_9
			product	hypothetical protein
			transl_table	11
6748	7593	CDS
			locus_tag	Harvfovirus_51_10
			product	hypothetical protein
			transl_table	11
7618	7923	CDS
			locus_tag	Harvfovirus_51_11
			product	hypothetical protein Indivirus_1_201
			transl_table	11
9356	7917	CDS
			locus_tag	Harvfovirus_51_12
			product	putative AAA+ family ATPase
			transl_table	11
9489	10037	CDS
			locus_tag	Harvfovirus_51_13
			product	hypothetical protein
			transl_table	11
10245	10424	CDS
			locus_tag	Harvfovirus_51_14
			product	hypothetical protein
			transl_table	11
>Harvfovirus_52
2	94	CDS
			locus_tag	Harvfovirus_52_1
			product	hypothetical protein
			transl_table	11
4113	1042	CDS
			locus_tag	Harvfovirus_52_2
			product	hypothetical protein Catovirus_1_78
			transl_table	11
4233	5312	CDS
			locus_tag	Harvfovirus_52_3
			product	hypothetical protein
			transl_table	11
5382	6395	CDS
			locus_tag	Harvfovirus_52_4
			product	hypothetical protein
			transl_table	11
6451	7587	CDS
			locus_tag	Harvfovirus_52_5
			product	hypothetical protein COT85_01625
			transl_table	11
7892	7584	CDS
			locus_tag	Harvfovirus_52_6
			product	hypothetical protein
			transl_table	11
9075	7945	CDS
			locus_tag	Harvfovirus_52_7
			product	hypothetical protein
			transl_table	11
10104	9124	CDS
			locus_tag	Harvfovirus_52_8
			product	hypothetical protein
			transl_table	11
>Harvfovirus_53
1	108	CDS
			locus_tag	Harvfovirus_53_1
			product	hypothetical protein
			transl_table	11
168	647	CDS
			locus_tag	Harvfovirus_53_2
			product	PREDICTED: eukaryotic translation initiation factor 5A-1
			transl_table	11
670	1089	CDS
			locus_tag	Harvfovirus_53_3
			product	hypothetical protein
			transl_table	11
1692	1084	CDS
			locus_tag	Harvfovirus_53_4
			product	hypothetical protein
			transl_table	11
2575	1751	CDS
			locus_tag	Harvfovirus_53_5
			product	predicted protein
			transl_table	11
2684	3007	CDS
			locus_tag	Harvfovirus_53_6
			product	hypothetical protein
			transl_table	11
3073	4458	CDS
			locus_tag	Harvfovirus_53_7
			product	mannose-1-phosphate guanylyltransferase 1
			transl_table	11
4526	6547	CDS
			locus_tag	Harvfovirus_53_8
			product	putative AAA+ family ATPase
			transl_table	11
6585	7604	CDS
			locus_tag	Harvfovirus_53_9
			product	hypothetical protein SAMD00019534_123960
			transl_table	11
8396	7629	CDS
			locus_tag	Harvfovirus_53_10
			product	PREDICTED: L-gulonolactone oxidase-like
			transl_table	11
8703	8452	CDS
			locus_tag	Harvfovirus_53_11
			product	hypothetical protein
			transl_table	11
8813	9325	CDS
			locus_tag	Harvfovirus_53_12
			product	hypothetical protein NTE_03442
			transl_table	11
9716	9309	CDS
			locus_tag	Harvfovirus_53_13
			product	ubiquitin family protein
			transl_table	11
>Harvfovirus_54
1	150	CDS
			locus_tag	Harvfovirus_54_1
			product	hypothetical protein
			transl_table	11
2581	284	CDS
			locus_tag	Harvfovirus_54_2
			product	putative minor capsid protein
			transl_table	11
2637	3854	CDS
			locus_tag	Harvfovirus_54_3
			product	hypothetical protein Catovirus_2_204
			transl_table	11
4773	3856	CDS
			locus_tag	Harvfovirus_54_4
			product	hypothetical protein
			transl_table	11
6355	4808	CDS
			locus_tag	Harvfovirus_54_5
			product	hypothetical protein Catovirus_2_211
			transl_table	11
6461	7105	CDS
			locus_tag	Harvfovirus_54_6
			product	transcription elongation factor TFIIS
			transl_table	11
7778	7086	CDS
			locus_tag	Harvfovirus_54_7
			product	hypothetical protein
			transl_table	11
9519	7834	CDS
			locus_tag	Harvfovirus_54_8
			product	hypothetical protein Catovirus_2_214
			transl_table	11
>Harvfovirus_55
2	193	CDS
			locus_tag	Harvfovirus_55_1
			product	hypothetical protein
			transl_table	11
199	879	CDS
			locus_tag	Harvfovirus_55_2
			product	hypothetical protein
			transl_table	11
1634	900	CDS
			locus_tag	Harvfovirus_55_3
			product	hypothetical protein Catovirus_1_263
			transl_table	11
1663	2550	CDS
			locus_tag	Harvfovirus_55_4
			product	hypothetical protein
			transl_table	11
3883	2552	CDS
			locus_tag	Harvfovirus_55_5
			product	hypothetical protein
			transl_table	11
5183	4116	CDS
			locus_tag	Harvfovirus_55_6
			product	hypothetical protein
			transl_table	11
5809	5231	CDS
			locus_tag	Harvfovirus_55_7
			product	hypothetical protein
			transl_table	11
5933	6778	CDS
			locus_tag	Harvfovirus_55_8
			product	hypothetical protein
			transl_table	11
7372	6755	CDS
			locus_tag	Harvfovirus_55_9
			product	hypothetical protein
			transl_table	11
8061	7336	CDS
			locus_tag	Harvfovirus_55_10
			product	hypothetical protein
			transl_table	11
8764	8114	CDS
			locus_tag	Harvfovirus_55_11
			product	hypothetical protein
			transl_table	11
9199	9339	CDS
			locus_tag	Harvfovirus_55_12
			product	hypothetical protein
			transl_table	11
>Harvfovirus_56
1209	13	CDS
			locus_tag	Harvfovirus_56_1
			product	hypothetical protein
			transl_table	11
1830	1252	CDS
			locus_tag	Harvfovirus_56_2
			product	hypothetical protein
			transl_table	11
3265	1859	CDS
			locus_tag	Harvfovirus_56_3
			product	hypothetical protein
			transl_table	11
5033	3333	CDS
			locus_tag	Harvfovirus_56_4
			product	hypothetical protein K443DRAFT_683561
			transl_table	11
5116	5439	CDS
			locus_tag	Harvfovirus_56_5
			product	hypothetical protein
			transl_table	11
6905	5451	CDS
			locus_tag	Harvfovirus_56_6
			product	hypothetical protein
			transl_table	11
7026	8069	CDS
			locus_tag	Harvfovirus_56_7
			product	hypothetical protein
			transl_table	11
8231	9190	CDS
			locus_tag	Harvfovirus_56_8
			product	T9SS C-terminal target domain-containing protein
			transl_table	11
>Harvfovirus_57
1	321	CDS
			locus_tag	Harvfovirus_57_1
			product	hypothetical protein
			transl_table	11
381	935	CDS
			locus_tag	Harvfovirus_57_2
			product	hypothetical protein
			transl_table	11
932	1384	CDS
			locus_tag	Harvfovirus_57_3
			product	hypothetical protein
			transl_table	11
1606	2085	CDS
			locus_tag	Harvfovirus_57_4
			product	hypothetical protein
			transl_table	11
2773	2162	CDS
			locus_tag	Harvfovirus_57_5
			product	hypothetical protein
			transl_table	11
4058	3123	CDS
			locus_tag	Harvfovirus_57_6
			product	hypothetical protein
			transl_table	11
4815	4132	CDS
			locus_tag	Harvfovirus_57_7
			product	hypothetical protein
			transl_table	11
5445	5068	CDS
			locus_tag	Harvfovirus_57_8
			product	hypothetical protein
			transl_table	11
6862	5867	CDS
			locus_tag	Harvfovirus_57_9
			product	hypothetical protein
			transl_table	11
8888	6921	CDS
			locus_tag	Harvfovirus_57_10
			product	hypothetical protein
			transl_table	11
9019	8873	CDS
			locus_tag	Harvfovirus_57_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_58
3	725	CDS
			locus_tag	Harvfovirus_58_1
			product	hypothetical protein
			transl_table	11
816	712	CDS
			locus_tag	Harvfovirus_58_2
			product	hypothetical protein
			transl_table	11
1080	1862	CDS
			locus_tag	Harvfovirus_58_3
			product	hypothetical protein Catovirus_1_1084
			transl_table	11
1918	2580	CDS
			locus_tag	Harvfovirus_58_4
			product	hypothetical protein
			transl_table	11
2638	3150	CDS
			locus_tag	Harvfovirus_58_5
			product	hypothetical protein Indivirus_1_68
			transl_table	11
3197	3757	CDS
			locus_tag	Harvfovirus_58_6
			product	hypothetical protein Catovirus_1_1082
			transl_table	11
3792	4226	CDS
			locus_tag	Harvfovirus_58_7
			product	hypothetical protein
			transl_table	11
4252	4512	CDS
			locus_tag	Harvfovirus_58_8
			product	hypothetical protein
			transl_table	11
5097	4516	CDS
			locus_tag	Harvfovirus_58_9
			product	hypothetical protein Klosneuvirus_1_190
			transl_table	11
5229	6539	CDS
			locus_tag	Harvfovirus_58_10
			product	proliferating cell nuclear antigen
			transl_table	11
6593	6907	CDS
			locus_tag	Harvfovirus_58_11
			product	hypothetical protein
			transl_table	11
7725	6904	CDS
			locus_tag	Harvfovirus_58_12
			product	translation initiation factor 4E
			transl_table	11
8836	7739	CDS
			locus_tag	Harvfovirus_58_13
			product	replication factor C small subunit
			transl_table	11
>Harvfovirus_59
2	91	CDS
			locus_tag	Harvfovirus_59_1
			product	hypothetical protein
			transl_table	11
96	446	CDS
			locus_tag	Harvfovirus_59_2
			product	hypothetical protein
			transl_table	11
782	462	CDS
			locus_tag	Harvfovirus_59_3
			product	hypothetical protein
			transl_table	11
1624	842	CDS
			locus_tag	Harvfovirus_59_4
			product	hypothetical protein
			transl_table	11
2397	1669	CDS
			locus_tag	Harvfovirus_59_5
			product	hypothetical protein
			transl_table	11
3307	2447	CDS
			locus_tag	Harvfovirus_59_6
			product	hypothetical protein
			transl_table	11
3372	3884	CDS
			locus_tag	Harvfovirus_59_7
			product	hypothetical protein
			transl_table	11
5351	4080	CDS
			locus_tag	Harvfovirus_59_8
			product	hypothetical protein
			transl_table	11
6149	5400	CDS
			locus_tag	Harvfovirus_59_9
			product	hypothetical protein
			transl_table	11
6263	7285	CDS
			locus_tag	Harvfovirus_59_10
			product	putative alpha-L-fucosidase
			transl_table	11
8038	8496	CDS
			locus_tag	Harvfovirus_59_11
			product	hypothetical protein
			transl_table	11
8695	8441	CDS
			locus_tag	Harvfovirus_59_12
			product	hypothetical protein
			transl_table	11
>Harvfovirus_60
1	927	CDS
			locus_tag	Harvfovirus_60_1
			product	hypothetical protein
			transl_table	11
1607	924	CDS
			locus_tag	Harvfovirus_60_2
			product	hypothetical protein
			transl_table	11
2338	1667	CDS
			locus_tag	Harvfovirus_60_3
			product	hypothetical protein
			transl_table	11
3629	2415	CDS
			locus_tag	Harvfovirus_60_4
			product	chromosome condensation regulator
			transl_table	11
4241	3705	CDS
			locus_tag	Harvfovirus_60_5
			product	hypothetical protein
			transl_table	11
4322	5041	CDS
			locus_tag	Harvfovirus_60_6
			product	hypothetical protein
			transl_table	11
5102	6244	CDS
			locus_tag	Harvfovirus_60_7
			product	RCC1 domain-containing protein 1
			transl_table	11
6301	7140	CDS
			locus_tag	Harvfovirus_60_8
			product	hypothetical protein
			transl_table	11
8410	7076	CDS
			locus_tag	Harvfovirus_60_9
			product	hypothetical protein
			transl_table	11
8527	8745	CDS
			locus_tag	Harvfovirus_60_10
			product	hypothetical protein
			transl_table	11
>Harvfovirus_61
2	178	CDS
			locus_tag	Harvfovirus_61_1
			product	hypothetical protein
			transl_table	11
175	432	CDS
			locus_tag	Harvfovirus_61_2
			product	hypothetical protein
			transl_table	11
1697	429	CDS
			locus_tag	Harvfovirus_61_3
			product	hypothetical protein
			transl_table	11
2804	1746	CDS
			locus_tag	Harvfovirus_61_4
			product	hypothetical protein
			transl_table	11
3996	2983	CDS
			locus_tag	Harvfovirus_61_5
			product	exodeoxyribonuclease III
			transl_table	11
4023	4664	CDS
			locus_tag	Harvfovirus_61_6
			product	hypothetical protein
			transl_table	11
4821	4651	CDS
			locus_tag	Harvfovirus_61_7
			product	hypothetical protein
			transl_table	11
4933	5439	CDS
			locus_tag	Harvfovirus_61_8
			product	hypothetical protein
			transl_table	11
5630	7951	CDS
			locus_tag	Harvfovirus_61_9
			product	putative lanosterol 14-alpha demethylase
			transl_table	11
8668	7961	CDS
			locus_tag	Harvfovirus_61_10
			product	hypothetical protein
			transl_table	11
>Harvfovirus_62
1	465	CDS
			locus_tag	Harvfovirus_62_1
			product	hybrid sensor histidine kinase/response regulator
			transl_table	11
2482	455	CDS
			locus_tag	Harvfovirus_62_2
			product	response regulator
			transl_table	11
2569	2952	CDS
			locus_tag	Harvfovirus_62_3
			product	unnamed protein product
			transl_table	11
3497	2955	CDS
			locus_tag	Harvfovirus_62_4
			product	hypothetical protein
			transl_table	11
4894	3614	CDS
			locus_tag	Harvfovirus_62_5
			product	hypothetical protein
			transl_table	11
5030	5698	CDS
			locus_tag	Harvfovirus_62_6
			product	hypothetical protein
			transl_table	11
5760	6488	CDS
			locus_tag	Harvfovirus_62_7
			product	hypothetical protein
			transl_table	11
6551	7321	CDS
			locus_tag	Harvfovirus_62_8
			product	hypothetical protein
			transl_table	11
7665	7297	CDS
			locus_tag	Harvfovirus_62_9
			product	hypothetical protein
			transl_table	11
7655	7891	CDS
			locus_tag	Harvfovirus_62_10
			product	hypothetical protein
			transl_table	11
>Harvfovirus_63
2	394	CDS
			locus_tag	Harvfovirus_63_1
			product	hypothetical protein
			transl_table	11
846	391	CDS
			locus_tag	Harvfovirus_63_2
			product	hypothetical protein D5b_00225
			transl_table	11
884	2764	CDS
			locus_tag	Harvfovirus_63_3
			product	Histidinol-phosphate aminotransferase
			transl_table	11
2807	4420	CDS
			locus_tag	Harvfovirus_63_4
			product	glycyl-tRNA synthetase
			transl_table	11
4486	4986	CDS
			locus_tag	Harvfovirus_63_5
			product	hypothetical protein
			transl_table	11
5012	6259	CDS
			locus_tag	Harvfovirus_63_6
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
6308	6919	CDS
			locus_tag	Harvfovirus_63_7
			product	hypothetical protein Klosneuvirus_1_362
			transl_table	11
6963	7409	CDS
			locus_tag	Harvfovirus_63_8
			product	hypothetical protein Catovirus_1_903
			transl_table	11
7816	8001	CDS
			locus_tag	Harvfovirus_63_9
			product	hypothetical protein
			transl_table	11
>Harvfovirus_64
3	2213	CDS
			locus_tag	Harvfovirus_64_1
			product	hypothetical protein
			transl_table	11
2189	3124	CDS
			locus_tag	Harvfovirus_64_2
			product	hypothetical protein
			transl_table	11
3160	3912	CDS
			locus_tag	Harvfovirus_64_3
			product	hypothetical protein
			transl_table	11
3933	4457	CDS
			locus_tag	Harvfovirus_64_4
			product	hypothetical protein
			transl_table	11
4479	6218	CDS
			locus_tag	Harvfovirus_64_5
			product	hypothetical protein K443DRAFT_683561
			transl_table	11
6271	7014	CDS
			locus_tag	Harvfovirus_64_6
			product	hypothetical protein
			transl_table	11
7728	7970	CDS
			locus_tag	Harvfovirus_64_7
			product	hypothetical protein
			transl_table	11
>Harvfovirus_65
610	2	CDS
			locus_tag	Harvfovirus_65_1
			product	hypothetical protein
			transl_table	11
1085	681	CDS
			locus_tag	Harvfovirus_65_2
			product	PREDICTED: mth938 domain-containing protein
			transl_table	11
3000	1126	CDS
			locus_tag	Harvfovirus_65_3
			product	hypothetical protein RO3G_15433
			transl_table	11
3126	3572	CDS
			locus_tag	Harvfovirus_65_4
			product	hypothetical protein Catovirus_2_73
			transl_table	11
4760	4023	CDS
			locus_tag	Harvfovirus_65_5
			product	PKC14209.1
			transl_table	11
7755	4951	CDS
			locus_tag	Harvfovirus_65_6
			product	DEAD-like helicases family protein
			transl_table	11
>Harvfovirus_66
1664	87	CDS
			locus_tag	Harvfovirus_66_1
			product	hypothetical protein
			transl_table	11
1731	2279	CDS
			locus_tag	Harvfovirus_66_2
			product	hypothetical protein
			transl_table	11
2991	2239	CDS
			locus_tag	Harvfovirus_66_3
			product	hypothetical protein
			transl_table	11
3101	4138	CDS
			locus_tag	Harvfovirus_66_4
			product	hypothetical protein
			transl_table	11
5275	4148	CDS
			locus_tag	Harvfovirus_66_5
			product	hypothetical protein
			transl_table	11
5717	5328	CDS
			locus_tag	Harvfovirus_66_6
			product	hypothetical protein
			transl_table	11
5837	6100	CDS
			locus_tag	Harvfovirus_66_7
			product	hypothetical protein crov077
			transl_table	11
6495	6097	CDS
			locus_tag	Harvfovirus_66_8
			product	hypothetical protein
			transl_table	11
6540	6905	CDS
			locus_tag	Harvfovirus_66_9
			product	hypothetical protein
			transl_table	11
7240	6902	CDS
			locus_tag	Harvfovirus_66_10
			product	hypothetical protein MpV1_111
			transl_table	11
7586	7314	CDS
			locus_tag	Harvfovirus_66_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_67
162	1	CDS
			locus_tag	Harvfovirus_67_1
			product	patatin
			transl_table	11
235	1074	CDS
			locus_tag	Harvfovirus_67_2
			product	hypothetical protein
			transl_table	11
1157	2443	CDS
			locus_tag	Harvfovirus_67_3
			product	hypothetical protein
			transl_table	11
2482	2709	CDS
			locus_tag	Harvfovirus_67_4
			product	hypothetical protein
			transl_table	11
2738	3817	CDS
			locus_tag	Harvfovirus_67_5
			product	PREDICTED: TNF receptor-associated factor 4-like
			transl_table	11
4714	3821	CDS
			locus_tag	Harvfovirus_67_6
			product	PREDICTED: m7GpppX diphosphatase-like
			transl_table	11
5546	4659	CDS
			locus_tag	Harvfovirus_67_7
			product	hypothetical protein CSA32_03900
			transl_table	11
5744	6610	CDS
			locus_tag	Harvfovirus_67_8
			product	hypothetical protein
			transl_table	11
6660	7355	CDS
			locus_tag	Harvfovirus_67_9
			product	hypothetical protein
			transl_table	11
>Harvfovirus_68
318	1	CDS
			locus_tag	Harvfovirus_68_1
			product	hypothetical protein
			transl_table	11
449	970	CDS
			locus_tag	Harvfovirus_68_2
			product	hypothetical protein
			transl_table	11
1038	1310	CDS
			locus_tag	Harvfovirus_68_3
			product	hypothetical protein
			transl_table	11
1489	2346	CDS
			locus_tag	Harvfovirus_68_4
			product	hypothetical protein
			transl_table	11
2393	3037	CDS
			locus_tag	Harvfovirus_68_5
			product	hypothetical protein
			transl_table	11
3088	3774	CDS
			locus_tag	Harvfovirus_68_6
			product	hypothetical protein
			transl_table	11
3814	4554	CDS
			locus_tag	Harvfovirus_68_7
			product	hypothetical protein
			transl_table	11
4594	5691	CDS
			locus_tag	Harvfovirus_68_8
			product	hypothetical protein
			transl_table	11
6864	5677	CDS
			locus_tag	Harvfovirus_68_9
			product	hypothetical protein
			transl_table	11
>Harvfovirus_69
527	3	CDS
			locus_tag	Harvfovirus_69_1
			product	hypothetical protein
			transl_table	11
1479	574	CDS
			locus_tag	Harvfovirus_69_2
			product	TATA box binding protein
			transl_table	11
1627	2697	CDS
			locus_tag	Harvfovirus_69_3
			product	hypothetical protein
			transl_table	11
2748	4484	CDS
			locus_tag	Harvfovirus_69_4
			product	NCLDV major capsid protein
			transl_table	11
6013	5543	CDS
			locus_tag	Harvfovirus_69_5
			product	hypothetical protein
			transl_table	11
6575	6784	CDS
			locus_tag	Harvfovirus_69_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_70
3	530	CDS
			locus_tag	Harvfovirus_70_1
			product	hypothetical protein
			transl_table	11
577	855	CDS
			locus_tag	Harvfovirus_70_2
			product	hypothetical protein
			transl_table	11
895	1050	CDS
			locus_tag	Harvfovirus_70_3
			product	hypothetical protein
			transl_table	11
1053	2231	CDS
			locus_tag	Harvfovirus_70_4
			product	hypothetical protein
			transl_table	11
2467	2216	CDS
			locus_tag	Harvfovirus_70_5
			product	hypothetical protein
			transl_table	11
2595	4466	CDS
			locus_tag	Harvfovirus_70_6
			product	peptidase M13
			transl_table	11
4478	4567	CDS
			locus_tag	Harvfovirus_70_7
			product	hypothetical protein
			transl_table	11
4605	5648	CDS
			locus_tag	Harvfovirus_70_8
			product	pentapeptide repeat-containing protein
			transl_table	11
6254	5655	CDS
			locus_tag	Harvfovirus_70_9
			product	hypothetical protein
			transl_table	11
6559	6296	CDS
			locus_tag	Harvfovirus_70_10
			product	hypothetical protein
			transl_table	11
6766	6566	CDS
			locus_tag	Harvfovirus_70_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_71
1	105	CDS
			locus_tag	Harvfovirus_71_1
			product	hypothetical protein
			transl_table	11
594	151	CDS
			locus_tag	Harvfovirus_71_2
			product	hypothetical protein
			transl_table	11
783	1448	CDS
			locus_tag	Harvfovirus_71_3
			product	hypothetical protein
			transl_table	11
1598	1816	CDS
			locus_tag	Harvfovirus_71_4
			product	hypothetical protein
			transl_table	11
1841	2356	CDS
			locus_tag	Harvfovirus_71_5
			product	dolichyldiphosphatase
			transl_table	11
3032	2532	CDS
			locus_tag	Harvfovirus_71_6
			product	hypothetical protein
			transl_table	11
3297	4163	CDS
			locus_tag	Harvfovirus_71_7
			product	hypothetical protein
			transl_table	11
4236	4370	CDS
			locus_tag	Harvfovirus_71_8
			product	hypothetical protein
			transl_table	11
4521	4706	CDS
			locus_tag	Harvfovirus_71_9
			product	hypothetical protein
			transl_table	11
4699	4842	CDS
			locus_tag	Harvfovirus_71_10
			product	hypothetical protein
			transl_table	11
6497	6637	CDS
			locus_tag	Harvfovirus_71_11
			product	hypothetical protein
			transl_table	11
>Harvfovirus_72
2	508	CDS
			locus_tag	Harvfovirus_72_1
			product	hypothetical protein
			transl_table	11
1591	2451	CDS
			locus_tag	Harvfovirus_72_2
			product	Clp protease
			transl_table	11
2469	4355	CDS
			locus_tag	Harvfovirus_72_3
			product	ankyrin repeat protein
			transl_table	11
4339	4485	CDS
			locus_tag	Harvfovirus_72_4
			product	hypothetical protein
			transl_table	11
4466	4969	CDS
			locus_tag	Harvfovirus_72_5
			product	hypothetical protein Catovirus_2_86
			transl_table	11
6354	4975	CDS
			locus_tag	Harvfovirus_72_6
			product	procyclic acidic repetitive protein PARP
			transl_table	11
>Harvfovirus_73
2	145	CDS
			locus_tag	Harvfovirus_73_1
			product	hypothetical protein
			transl_table	11
210	875	CDS
			locus_tag	Harvfovirus_73_2
			product	hypothetical protein
			transl_table	11
939	2603	CDS
			locus_tag	Harvfovirus_73_3
			product	DEAD/SNF2-like helicase
			transl_table	11
3690	2578	CDS
			locus_tag	Harvfovirus_73_4
			product	hypothetical protein
			transl_table	11
5702	3744	CDS
			locus_tag	Harvfovirus_73_5
			product	calcineurin-like phosphoesterase
			transl_table	11
>Harvfovirus_74
2	817	CDS
			locus_tag	Harvfovirus_74_1
			product	polyADP-ribose polymerase
			transl_table	11
1449	820	CDS
			locus_tag	Harvfovirus_74_2
			product	hypothetical protein
			transl_table	11
2241	1507	CDS
			locus_tag	Harvfovirus_74_3
			product	PREDICTED: ras-related protein Rab-13
			transl_table	11
3066	2302	CDS
			locus_tag	Harvfovirus_74_4
			product	GTP-binding protein Rab-3D
			transl_table	11
3194	3811	CDS
			locus_tag	Harvfovirus_74_5
			product	ras-related protein Rab-35
			transl_table	11
3944	5143	CDS
			locus_tag	Harvfovirus_74_6
			product	hypothetical protein
			transl_table	11
5675	5568	CDS
			locus_tag	Harvfovirus_74_7
			product	hypothetical protein
			transl_table	11
>Harvfovirus_75
397	2	CDS
			locus_tag	Harvfovirus_75_1
			product	hypothetical protein
			transl_table	11
571	1098	CDS
			locus_tag	Harvfovirus_75_2
			product	hypothetical protein
			transl_table	11
1739	1095	CDS
			locus_tag	Harvfovirus_75_3
			product	hypothetical protein
			transl_table	11
1821	2795	CDS
			locus_tag	Harvfovirus_75_4
			product	TIGR03118 family protein
			transl_table	11
2850	3506	CDS
			locus_tag	Harvfovirus_75_5
			product	PREDICTED: E3 ubiquitin-protein ligase MARCH8
			transl_table	11
3541	4317	CDS
			locus_tag	Harvfovirus_75_6
			product	hypothetical protein
			transl_table	11
4724	4596	CDS
			locus_tag	Harvfovirus_75_7
			product	hypothetical protein
			transl_table	11
5473	5613	CDS
			locus_tag	Harvfovirus_75_8
			product	hypothetical protein
			transl_table	11
>Harvfovirus_76
2	706	CDS
			locus_tag	Harvfovirus_76_1
			product	forkhead associated FHA domain and RING domain protein
			transl_table	11
2067	703	CDS
			locus_tag	Harvfovirus_76_2
			product	hypothetical protein
			transl_table	11
2656	2399	CDS
			locus_tag	Harvfovirus_76_3
			product	hypothetical protein
			transl_table	11
3033	3734	CDS
			locus_tag	Harvfovirus_76_4
			product	hypothetical protein
			transl_table	11
3801	5105	CDS
			locus_tag	Harvfovirus_76_5
			product	alanyl-tRNA synthetase
			transl_table	11
5235	5047	CDS
			locus_tag	Harvfovirus_76_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_77
1	159	CDS
			locus_tag	Harvfovirus_77_1
			product	hypothetical protein
			transl_table	11
996	166	CDS
			locus_tag	Harvfovirus_77_2
			product	hypothetical protein
			transl_table	11
2133	1015	CDS
			locus_tag	Harvfovirus_77_3
			product	hypothetical protein
			transl_table	11
2767	2162	CDS
			locus_tag	Harvfovirus_77_4
			product	hypothetical protein
			transl_table	11
4301	2793	CDS
			locus_tag	Harvfovirus_77_5
			product	threonine ammonia-lyase, biosynthetic
			transl_table	11
5067	5228	CDS
			locus_tag	Harvfovirus_77_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_78
310	783	CDS
			locus_tag	Harvfovirus_78_1
			product	hypothetical protein
			transl_table	11
1457	960	CDS
			locus_tag	Harvfovirus_78_2
			product	hypothetical protein
			transl_table	11
1630	1863	CDS
			locus_tag	Harvfovirus_78_3
			product	hypothetical protein
			transl_table	11
3360	1876	CDS
			locus_tag	Harvfovirus_78_4
			product	hypothetical protein
			transl_table	11
4021	3545	CDS
			locus_tag	Harvfovirus_78_5
			product	hypothetical protein
			transl_table	11
4218	4129	CDS
			locus_tag	Harvfovirus_78_6
			product	hypothetical protein
			transl_table	11
4981	5079	CDS
			locus_tag	Harvfovirus_78_7
			product	hypothetical protein
			transl_table	11
>Harvfovirus_79
170	3	CDS
			locus_tag	Harvfovirus_79_1
			product	hypothetical protein
			transl_table	11
288	806	CDS
			locus_tag	Harvfovirus_79_2
			product	hypothetical protein
			transl_table	11
872	1777	CDS
			locus_tag	Harvfovirus_79_3
			product	hypothetical protein
			transl_table	11
1816	3621	CDS
			locus_tag	Harvfovirus_79_4
			product	glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
3680	4237	CDS
			locus_tag	Harvfovirus_79_5
			product	hypothetical protein
			transl_table	11
4861	4223	CDS
			locus_tag	Harvfovirus_79_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_80
870	1	CDS
			locus_tag	Harvfovirus_80_1
			product	serine/threonine protein kinase
			transl_table	11
2209	920	CDS
			locus_tag	Harvfovirus_80_2
			product	hypothetical protein A2541_02530
			transl_table	11
2293	3078	CDS
			locus_tag	Harvfovirus_80_3
			product	hypothetical protein
			transl_table	11
3144	3821	CDS
			locus_tag	Harvfovirus_80_4
			product	DNA-directed RNA polymerase subunit 5
			transl_table	11
4639	3818	CDS
			locus_tag	Harvfovirus_80_5
			product	hypothetical protein
			transl_table	11
>Harvfovirus_81
2160	61	CDS
			locus_tag	Harvfovirus_81_1
			product	GTP binding translation elongation factor
			transl_table	11
2281	4143	CDS
			locus_tag	Harvfovirus_81_2
			product	YqaJ-like viral recombinase domain
			transl_table	11
4353	4448	CDS
			locus_tag	Harvfovirus_81_3
			product	hypothetical protein
			transl_table	11
>Harvfovirus_82
2	574	CDS
			locus_tag	Harvfovirus_82_1
			product	hypothetical protein
			transl_table	11
728	1312	CDS
			locus_tag	Harvfovirus_82_2
			product	hypothetical protein
			transl_table	11
2518	1301	CDS
			locus_tag	Harvfovirus_82_3
			product	hypothetical protein
			transl_table	11
4200	2941	CDS
			locus_tag	Harvfovirus_82_4
			product	hypothetical protein Indivirus_1_31
			transl_table	11
>Harvfovirus_83
2366	2286	tRNA	Harvfovirus_tRNA_14
			product	tRNA-Pseudo(ACT)
1	975	CDS
			locus_tag	Harvfovirus_83_1
			product	putative amidoligase enzyme
			transl_table	11
996	1346	CDS
			locus_tag	Harvfovirus_83_2
			product	hypothetical protein
			transl_table	11
1399	2217	CDS
			locus_tag	Harvfovirus_83_3
			product	hypothetical protein
			transl_table	11
2944	2486	CDS
			locus_tag	Harvfovirus_83_4
			product	hypothetical protein Catovirus_1_946
			transl_table	11
3458	2958	CDS
			locus_tag	Harvfovirus_83_5
			product	hypothetical protein
			transl_table	11
4073	4168	CDS
			locus_tag	Harvfovirus_83_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_84
375	1	CDS
			locus_tag	Harvfovirus_84_1
			product	hypothetical protein
			transl_table	11
460	912	CDS
			locus_tag	Harvfovirus_84_2
			product	hypothetical protein
			transl_table	11
958	1683	CDS
			locus_tag	Harvfovirus_84_3
			product	hypothetical protein
			transl_table	11
2374	1676	CDS
			locus_tag	Harvfovirus_84_4
			product	hypothetical protein
			transl_table	11
3677	2442	CDS
			locus_tag	Harvfovirus_84_5
			product	putative DNA mismatch repair protein MutS-like protein
			transl_table	11
3803	4006	CDS
			locus_tag	Harvfovirus_84_6
			product	hypothetical protein
			transl_table	11
>Harvfovirus_85
1	2241	CDS
			locus_tag	Harvfovirus_85_1
			product	tRNA 4-thiouridine(8) synthase ThiI
			transl_table	11
2899	2243	CDS
			locus_tag	Harvfovirus_85_2
			product	hypothetical protein
			transl_table	11
3322	3068	CDS
			locus_tag	Harvfovirus_85_3
			product	hypothetical protein Indivirus_1_169
			transl_table	11
3972	3415	CDS
			locus_tag	Harvfovirus_85_4
			product	XRN 5'-3' exonuclease
			transl_table	11
>Harvfovirus_86
3	1562	CDS
			locus_tag	Harvfovirus_86_1
			product	DNA mismatch repair ATPase MutS
			transl_table	11
1575	2420	CDS
			locus_tag	Harvfovirus_86_2
			product	hypothetical protein
			transl_table	11
2952	2383	CDS
			locus_tag	Harvfovirus_86_3
			product	hypothetical protein Catovirus_2_188
			transl_table	11
3586	2978	CDS
			locus_tag	Harvfovirus_86_4
			product	hypothetical protein Catovirus_2_187
			transl_table	11
>Harvfovirus_87
1103	3	CDS
			locus_tag	Harvfovirus_87_1
			product	hypothetical protein PHYSODRAFT_539927
			transl_table	11
1233	1982	CDS
			locus_tag	Harvfovirus_87_2
			product	hypothetical protein
			transl_table	11
2057	2773	CDS
			locus_tag	Harvfovirus_87_3
			product	hypothetical protein
			transl_table	11
3368	3153	CDS
			locus_tag	Harvfovirus_87_4
			product	hypothetical protein
			transl_table	11
3375	3497	CDS
			locus_tag	Harvfovirus_87_5
			product	hypothetical protein
			transl_table	11
>Harvfovirus_88
2	235	CDS
			locus_tag	Harvfovirus_88_1
			product	hypothetical protein
			transl_table	11
364	1275	CDS
			locus_tag	Harvfovirus_88_2
			product	hypothetical protein
			transl_table	11
2026	1325	CDS
			locus_tag	Harvfovirus_88_3
			product	hypothetical protein
			transl_table	11
2603	3028	CDS
			locus_tag	Harvfovirus_88_4
			product	hypothetical protein
			transl_table	11
>Harvfovirus_89
2	1093	CDS
			locus_tag	Harvfovirus_89_1
			product	hypothetical protein
			transl_table	11
2474	2620	CDS
			locus_tag	Harvfovirus_89_2
			product	hypothetical protein
			transl_table	11
