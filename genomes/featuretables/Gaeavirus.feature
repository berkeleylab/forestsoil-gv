>Gaeavirus_1
3	713	CDS
			locus_tag	Gaeavirus_1_1
			product	hypothetical protein
			transl_table	11
830	1561	CDS
			locus_tag	Gaeavirus_1_2
			product	hypothetical protein
			transl_table	11
1623	4106	CDS
			locus_tag	Gaeavirus_1_3
			product	hypothetical protein
			transl_table	11
4330	7542	CDS
			locus_tag	Gaeavirus_1_4
			product	hypothetical protein
			transl_table	11
8184	7783	CDS
			locus_tag	Gaeavirus_1_5
			product	hypothetical protein
			transl_table	11
8565	8302	CDS
			locus_tag	Gaeavirus_1_6
			product	hypothetical protein
			transl_table	11
9748	11337	CDS
			locus_tag	Gaeavirus_1_7
			product	hypothetical protein
			transl_table	11
11417	12412	CDS
			locus_tag	Gaeavirus_1_8
			product	hypothetical protein
			transl_table	11
13405	13091	CDS
			locus_tag	Gaeavirus_1_9
			product	eukaryotic translation initiation factor 5a
			transl_table	11
13488	14201	CDS
			locus_tag	Gaeavirus_1_10
			product	hypothetical protein
			transl_table	11
14479	14628	CDS
			locus_tag	Gaeavirus_1_11
			product	hypothetical protein
			transl_table	11
15179	15268	CDS
			locus_tag	Gaeavirus_1_12
			product	hypothetical protein
			transl_table	11
15268	15429	CDS
			locus_tag	Gaeavirus_1_13
			product	hypothetical protein
			transl_table	11
16430	16750	CDS
			locus_tag	Gaeavirus_1_14
			product	hypothetical protein
			transl_table	11
17404	19050	CDS
			locus_tag	Gaeavirus_1_15
			product	hypothetical protein
			transl_table	11
19231	20853	CDS
			locus_tag	Gaeavirus_1_16
			product	hypothetical protein
			transl_table	11
22667	21816	CDS
			locus_tag	Gaeavirus_1_17
			product	hypothetical protein
			transl_table	11
23187	22720	CDS
			locus_tag	Gaeavirus_1_18
			product	hypothetical protein
			transl_table	11
23300	24112	CDS
			locus_tag	Gaeavirus_1_19
			product	hypothetical protein
			transl_table	11
25417	24407	CDS
			locus_tag	Gaeavirus_1_20
			product	hypothetical protein
			transl_table	11
25561	26358	CDS
			locus_tag	Gaeavirus_1_21
			product	hypothetical protein mvi_147
			transl_table	11
26336	28000	CDS
			locus_tag	Gaeavirus_1_22
			product	hypothetical protein
			transl_table	11
28069	29259	CDS
			locus_tag	Gaeavirus_1_23
			product	hypothetical protein
			transl_table	11
29315	29632	CDS
			locus_tag	Gaeavirus_1_24
			product	hypothetical protein
			transl_table	11
30402	29878	CDS
			locus_tag	Gaeavirus_1_25
			product	hypothetical protein
			transl_table	11
30550	31197	CDS
			locus_tag	Gaeavirus_1_26
			product	hypothetical protein BGO87_09785
			transl_table	11
31781	32509	CDS
			locus_tag	Gaeavirus_1_27
			product	hypothetical protein
			transl_table	11
32568	33239	CDS
			locus_tag	Gaeavirus_1_28
			product	hypothetical protein
			transl_table	11
33574	34224	CDS
			locus_tag	Gaeavirus_1_29
			product	hypothetical protein
			transl_table	11
34351	35040	CDS
			locus_tag	Gaeavirus_1_30
			product	hypothetical protein
			transl_table	11
35167	35787	CDS
			locus_tag	Gaeavirus_1_31
			product	hypothetical protein
			transl_table	11
35859	36416	CDS
			locus_tag	Gaeavirus_1_32
			product	hypothetical protein
			transl_table	11
36659	37207	CDS
			locus_tag	Gaeavirus_1_33
			product	hypothetical protein
			transl_table	11
37238	37912	CDS
			locus_tag	Gaeavirus_1_34
			product	hypothetical protein
			transl_table	11
37978	39120	CDS
			locus_tag	Gaeavirus_1_35
			product	hypothetical protein
			transl_table	11
39214	40260	CDS
			locus_tag	Gaeavirus_1_36
			product	hypothetical protein
			transl_table	11
41106	40360	CDS
			locus_tag	Gaeavirus_1_37
			product	hypothetical protein BMW23_1062
			transl_table	11
41591	41238	CDS
			locus_tag	Gaeavirus_1_38
			product	hypothetical protein
			transl_table	11
41956	41573	CDS
			locus_tag	Gaeavirus_1_39
			product	hypothetical protein
			transl_table	11
42125	43681	CDS
			locus_tag	Gaeavirus_1_40
			product	hypothetical protein
			transl_table	11
44500	43853	CDS
			locus_tag	Gaeavirus_1_41
			product	hypothetical protein
			transl_table	11
44696	44962	CDS
			locus_tag	Gaeavirus_1_42
			product	hypothetical protein
			transl_table	11
45062	45181	CDS
			locus_tag	Gaeavirus_1_43
			product	hypothetical protein
			transl_table	11
45237	46118	CDS
			locus_tag	Gaeavirus_1_44
			product	hypothetical protein
			transl_table	11
46195	47460	CDS
			locus_tag	Gaeavirus_1_45
			product	hypothetical protein
			transl_table	11
47573	48394	CDS
			locus_tag	Gaeavirus_1_46
			product	hypothetical protein
			transl_table	11
48439	49386	CDS
			locus_tag	Gaeavirus_1_47
			product	hypothetical protein
			transl_table	11
49464	50453	CDS
			locus_tag	Gaeavirus_1_48
			product	hypothetical protein
			transl_table	11
50472	51437	CDS
			locus_tag	Gaeavirus_1_49
			product	hypothetical protein
			transl_table	11
51496	53067	CDS
			locus_tag	Gaeavirus_1_50
			product	unknown
			transl_table	11
53211	54965	CDS
			locus_tag	Gaeavirus_1_51
			product	hypothetical protein
			transl_table	11
55106	56461	CDS
			locus_tag	Gaeavirus_1_52
			product	hypothetical protein
			transl_table	11
56549	57343	CDS
			locus_tag	Gaeavirus_1_53
			product	hypothetical protein
			transl_table	11
57382	58119	CDS
			locus_tag	Gaeavirus_1_54
			product	hypothetical protein
			transl_table	11
58250	58627	CDS
			locus_tag	Gaeavirus_1_55
			product	hypothetical protein
			transl_table	11
>Gaeavirus_2
2396	3	CDS
			locus_tag	Gaeavirus_2_1
			product	restriction-modification methylase
			transl_table	11
4589	3330	CDS
			locus_tag	Gaeavirus_2_2
			product	superfamily II DNA or RNA helicase
			transl_table	11
8685	4663	CDS
			locus_tag	Gaeavirus_2_3
			product	putative site-specific DNA-methyltransferase
			transl_table	11
9739	10077	CDS
			locus_tag	Gaeavirus_2_4
			product	hypothetical protein
			transl_table	11
10082	10720	CDS
			locus_tag	Gaeavirus_2_5
			product	superoxide dismutase
			transl_table	11
11533	10715	CDS
			locus_tag	Gaeavirus_2_6
			product	hypothetical protein
			transl_table	11
12768	11584	CDS
			locus_tag	Gaeavirus_2_7
			product	hypothetical protein WICANDRAFT_27213
			transl_table	11
12914	13432	CDS
			locus_tag	Gaeavirus_2_8
			product	hypothetical protein BMW23_1062
			transl_table	11
14361	13474	CDS
			locus_tag	Gaeavirus_2_9
			product	hypothetical protein
			transl_table	11
14648	16819	CDS
			locus_tag	Gaeavirus_2_10
			product	putative type III restriction modification restriction subunit/conserved C-terminal helicase domain protein
			transl_table	11
16877	19144	CDS
			locus_tag	Gaeavirus_2_11
			product	putative type I restriction modification N-terminal methyltransferase domain protein
			transl_table	11
24594	19153	CDS
			locus_tag	Gaeavirus_2_12
			product	hypothetical protein COW71_09675
			transl_table	11
24759	26168	CDS
			locus_tag	Gaeavirus_2_13
			product	hypothetical protein CBB92_04050
			transl_table	11
28381	26282	CDS
			locus_tag	Gaeavirus_2_14
			product	outer membrane protein
			transl_table	11
30702	28426	CDS
			locus_tag	Gaeavirus_2_15
			product	hypothetical protein COW71_09675
			transl_table	11
32117	31035	CDS
			locus_tag	Gaeavirus_2_16
			product	hypothetical protein COW71_09675
			transl_table	11
33915	32131	CDS
			locus_tag	Gaeavirus_2_17
			product	hypothetical protein B6D44_10395
			transl_table	11
34548	34087	CDS
			locus_tag	Gaeavirus_2_18
			product	hypothetical protein
			transl_table	11
35626	34580	CDS
			locus_tag	Gaeavirus_2_19
			product	hypothetical protein
			transl_table	11
36450	35662	CDS
			locus_tag	Gaeavirus_2_20
			product	hypothetical protein
			transl_table	11
36911	36753	CDS
			locus_tag	Gaeavirus_2_21
			product	hypothetical protein
			transl_table	11
38049	36940	CDS
			locus_tag	Gaeavirus_2_22
			product	hypothetical protein
			transl_table	11
38264	38809	CDS
			locus_tag	Gaeavirus_2_23
			product	RING domain protein
			transl_table	11
40339	38810	CDS
			locus_tag	Gaeavirus_2_24
			product	chromodomain helicase DNA binding protein 8
			transl_table	11
40451	42172	CDS
			locus_tag	Gaeavirus_2_25
			product	hypothetical protein
			transl_table	11
42480	43577	CDS
			locus_tag	Gaeavirus_2_26
			product	hypothetical protein
			transl_table	11
43634	44455	CDS
			locus_tag	Gaeavirus_2_27
			product	hypothetical protein
			transl_table	11
45017	44520	CDS
			locus_tag	Gaeavirus_2_28
			product	hypothetical protein BMW23_1054
			transl_table	11
>Gaeavirus_3
49	888	CDS
			locus_tag	Gaeavirus_3_1
			product	isoleucyl-tRNA synthetase
			transl_table	11
3754	1124	CDS
			locus_tag	Gaeavirus_3_2
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
4867	4253	CDS
			locus_tag	Gaeavirus_3_3
			product	hypothetical protein Hokovirus_4_38
			transl_table	11
5135	4905	CDS
			locus_tag	Gaeavirus_3_4
			product	hypothetical protein
			transl_table	11
6701	5445	CDS
			locus_tag	Gaeavirus_3_5
			product	hypothetical protein
			transl_table	11
7120	6755	CDS
			locus_tag	Gaeavirus_3_6
			product	hypothetical protein
			transl_table	11
8299	7628	CDS
			locus_tag	Gaeavirus_3_7
			product	deoxynucleoside kinase
			transl_table	11
9213	8353	CDS
			locus_tag	Gaeavirus_3_8
			product	hypothetical protein
			transl_table	11
12529	9254	CDS
			locus_tag	Gaeavirus_3_9
			product	hypothetical protein Indivirus_2_103
			transl_table	11
14776	13586	CDS
			locus_tag	Gaeavirus_3_10
			product	hypothetical protein
			transl_table	11
15965	14979	CDS
			locus_tag	Gaeavirus_3_11
			product	hypothetical protein
			transl_table	11
17142	16171	CDS
			locus_tag	Gaeavirus_3_12
			product	hypothetical protein
			transl_table	11
17686	17312	CDS
			locus_tag	Gaeavirus_3_13
			product	hypothetical protein Catovirus_2_78
			transl_table	11
18195	20591	CDS
			locus_tag	Gaeavirus_3_14
			product	hypothetical protein Indivirus_1_31
			transl_table	11
20725	21711	CDS
			locus_tag	Gaeavirus_3_15
			product	XRN 5'-3' exonuclease
			transl_table	11
21740	22015	CDS
			locus_tag	Gaeavirus_3_16
			product	hypothetical protein
			transl_table	11
24672	22099	CDS
			locus_tag	Gaeavirus_3_17
			product	PREDICTED: uncharacterized protein LOC105844216
			transl_table	11
25310	24768	CDS
			locus_tag	Gaeavirus_3_18
			product	ribonuclease H
			transl_table	11
25393	25920	CDS
			locus_tag	Gaeavirus_3_19
			product	hypothetical protein
			transl_table	11
26385	27065	CDS
			locus_tag	Gaeavirus_3_20
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
27153	27857	CDS
			locus_tag	Gaeavirus_3_21
			product	hypothetical protein
			transl_table	11
29572	28196	CDS
			locus_tag	Gaeavirus_3_22
			product	restriction-modification methylase
			transl_table	11
31366	29765	CDS
			locus_tag	Gaeavirus_3_23
			product	hypothetical protein Klosneuvirus_2_50
			transl_table	11
31990	31436	CDS
			locus_tag	Gaeavirus_3_24
			product	hypothetical protein
			transl_table	11
32876	32091	CDS
			locus_tag	Gaeavirus_3_25
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
33074	33802	CDS
			locus_tag	Gaeavirus_3_26
			product	hypothetical protein
			transl_table	11
34049	33828	CDS
			locus_tag	Gaeavirus_3_27
			product	hypothetical protein
			transl_table	11
34789	34283	CDS
			locus_tag	Gaeavirus_3_28
			product	ATP-dependent RNA helicase eIF4A
			transl_table	11
35711	34854	CDS
			locus_tag	Gaeavirus_3_29
			product	ATP-dependent RNA helicase FAL1
			transl_table	11
36055	35888	CDS
			locus_tag	Gaeavirus_3_30
			product	hypothetical protein
			transl_table	11
37529	37714	CDS
			locus_tag	Gaeavirus_3_31
			product	hypothetical protein
			transl_table	11
>Gaeavirus_4
2	1156	CDS
			locus_tag	Gaeavirus_4_1
			product	eukaryotic translation initiation factor 2
			transl_table	11
1997	1137	CDS
			locus_tag	Gaeavirus_4_2
			product	PREDICTED: eukaryotic translation initiation factor 2 subunit 1
			transl_table	11
2076	2822	CDS
			locus_tag	Gaeavirus_4_3
			product	hypothetical protein
			transl_table	11
4680	3574	CDS
			locus_tag	Gaeavirus_4_4
			product	hypothetical protein BMW23_1049
			transl_table	11
5388	4924	CDS
			locus_tag	Gaeavirus_4_5
			product	hypothetical protein Indivirus_3_74
			transl_table	11
5497	6588	CDS
			locus_tag	Gaeavirus_4_6
			product	hypothetical protein
			transl_table	11
6605	7450	CDS
			locus_tag	Gaeavirus_4_7
			product	hypothetical protein
			transl_table	11
7502	7909	CDS
			locus_tag	Gaeavirus_4_8
			product	hypothetical protein
			transl_table	11
9808	7997	CDS
			locus_tag	Gaeavirus_4_9
			product	FtsJ-like methyltransferase
			transl_table	11
10118	13321	CDS
			locus_tag	Gaeavirus_4_10
			product	mRNA capping enzyme
			transl_table	11
13405	14250	CDS
			locus_tag	Gaeavirus_4_11
			product	hypothetical protein
			transl_table	11
14393	15013	CDS
			locus_tag	Gaeavirus_4_12
			product	hypothetical protein
			transl_table	11
15228	16901	CDS
			locus_tag	Gaeavirus_4_13
			product	FLAP-like endonuclease XPG
			transl_table	11
17378	16911	CDS
			locus_tag	Gaeavirus_4_14
			product	hypothetical protein
			transl_table	11
17508	18515	CDS
			locus_tag	Gaeavirus_4_15
			product	replication factor C small subunit
			transl_table	11
18969	18529	CDS
			locus_tag	Gaeavirus_4_16
			product	hypothetical protein
			transl_table	11
20129	19023	CDS
			locus_tag	Gaeavirus_4_17
			product	hypothetical protein
			transl_table	11
21146	20724	CDS
			locus_tag	Gaeavirus_4_18
			product	hypothetical protein
			transl_table	11
21173	24019	CDS
			locus_tag	Gaeavirus_4_19
			product	insulysin, insulin-degrading enzyme
			transl_table	11
24875	24087	CDS
			locus_tag	Gaeavirus_4_20
			product	hypothetical protein
			transl_table	11
25833	25054	CDS
			locus_tag	Gaeavirus_4_21
			product	hypothetical protein
			transl_table	11
25913	26041	CDS
			locus_tag	Gaeavirus_4_22
			product	hypothetical protein
			transl_table	11
26111	27757	CDS
			locus_tag	Gaeavirus_4_23
			product	hypothetical protein
			transl_table	11
29100	27808	CDS
			locus_tag	Gaeavirus_4_24
			product	hypothetical protein
			transl_table	11
30508	29162	CDS
			locus_tag	Gaeavirus_4_25
			product	hypothetical protein
			transl_table	11
30661	30801	CDS
			locus_tag	Gaeavirus_4_26
			product	hypothetical protein
			transl_table	11
>Gaeavirus_5
1644	892	CDS
			locus_tag	Gaeavirus_5_1
			product	hypothetical protein
			transl_table	11
1747	1947	CDS
			locus_tag	Gaeavirus_5_2
			product	hypothetical protein
			transl_table	11
1972	2154	CDS
			locus_tag	Gaeavirus_5_3
			product	hypothetical protein
			transl_table	11
2532	2314	CDS
			locus_tag	Gaeavirus_5_4
			product	hypothetical protein
			transl_table	11
2669	4699	CDS
			locus_tag	Gaeavirus_5_5
			product	hypothetical protein
			transl_table	11
4900	4995	CDS
			locus_tag	Gaeavirus_5_6
			product	hypothetical protein
			transl_table	11
5006	6274	CDS
			locus_tag	Gaeavirus_5_7
			product	alpha/beta hydrolase fold
			transl_table	11
6434	6769	CDS
			locus_tag	Gaeavirus_5_8
			product	hypothetical protein
			transl_table	11
7403	7065	CDS
			locus_tag	Gaeavirus_5_9
			product	hypothetical protein
			transl_table	11
7564	10551	CDS
			locus_tag	Gaeavirus_5_10
			product	hypothetical protein
			transl_table	11
10649	10948	CDS
			locus_tag	Gaeavirus_5_11
			product	hypothetical protein
			transl_table	11
11019	11786	CDS
			locus_tag	Gaeavirus_5_12
			product	hypothetical protein
			transl_table	11
12565	12017	CDS
			locus_tag	Gaeavirus_5_13
			product	hypothetical protein
			transl_table	11
13725	12772	CDS
			locus_tag	Gaeavirus_5_14
			product	hypothetical protein
			transl_table	11
15381	13987	CDS
			locus_tag	Gaeavirus_5_15
			product	restriction-modification methylase
			transl_table	11
19143	15469	CDS
			locus_tag	Gaeavirus_5_16
			product	hypothetical protein B7Z66_15020
			transl_table	11
19322	19227	CDS
			locus_tag	Gaeavirus_5_17
			product	hypothetical protein
			transl_table	11
20339	19548	CDS
			locus_tag	Gaeavirus_5_18
			product	putative pyrophosphohydrolase
			transl_table	11
20722	22404	CDS
			locus_tag	Gaeavirus_5_19
			product	hypothetical protein
			transl_table	11
22905	22612	CDS
			locus_tag	Gaeavirus_5_20
			product	hypothetical protein
			transl_table	11
23040	23339	CDS
			locus_tag	Gaeavirus_5_21
			product	restriction-modification methylase
			transl_table	11
23455	23342	CDS
			locus_tag	Gaeavirus_5_22
			product	hypothetical protein
			transl_table	11
>Gaeavirus_6
559	2	CDS
			locus_tag	Gaeavirus_6_1
			product	hypothetical protein Catovirus_1_1061
			transl_table	11
1525	686	CDS
			locus_tag	Gaeavirus_6_2
			product	hypothetical protein
			transl_table	11
2622	1588	CDS
			locus_tag	Gaeavirus_6_3
			product	hypothetical protein
			transl_table	11
3061	2828	CDS
			locus_tag	Gaeavirus_6_4
			product	hypothetical protein
			transl_table	11
3361	3137	CDS
			locus_tag	Gaeavirus_6_5
			product	hypothetical protein
			transl_table	11
4175	5257	CDS
			locus_tag	Gaeavirus_6_6
			product	hypothetical protein
			transl_table	11
5370	6317	CDS
			locus_tag	Gaeavirus_6_7
			product	hypothetical protein
			transl_table	11
7333	6608	CDS
			locus_tag	Gaeavirus_6_8
			product	hypothetical protein Catovirus_1_1058
			transl_table	11
10471	8006	CDS
			locus_tag	Gaeavirus_6_9
			product	DEAD/SNF2-like helicase
			transl_table	11
11582	10785	CDS
			locus_tag	Gaeavirus_6_10
			product	hypothetical protein DICPUDRAFT_83269
			transl_table	11
11642	12343	CDS
			locus_tag	Gaeavirus_6_11
			product	hypothetical protein Catovirus_1_1053
			transl_table	11
12433	13659	CDS
			locus_tag	Gaeavirus_6_12
			product	hypothetical protein
			transl_table	11
13721	16282	CDS
			locus_tag	Gaeavirus_6_13
			product	AAA family ATPase
			transl_table	11
17179	17721	CDS
			locus_tag	Gaeavirus_6_14
			product	hypothetical protein
			transl_table	11
19668	18784	CDS
			locus_tag	Gaeavirus_6_15
			product	hypothetical protein Hokovirus_2_185
			transl_table	11
20908	20192	CDS
			locus_tag	Gaeavirus_6_16
			product	DNA-directed RNA polymerase subunit 5
			transl_table	11
21734	20976	CDS
			locus_tag	Gaeavirus_6_17
			product	hypothetical protein
			transl_table	11
23236	22736	CDS
			locus_tag	Gaeavirus_6_18
			product	hypothetical protein
			transl_table	11
>Gaeavirus_7
2	1567	CDS
			locus_tag	Gaeavirus_7_1
			product	hypothetical protein
			transl_table	11
2181	1573	CDS
			locus_tag	Gaeavirus_7_2
			product	hypothetical protein
			transl_table	11
2323	3360	CDS
			locus_tag	Gaeavirus_7_3
			product	hypothetical protein
			transl_table	11
3490	3350	CDS
			locus_tag	Gaeavirus_7_4
			product	hypothetical protein
			transl_table	11
4054	3677	CDS
			locus_tag	Gaeavirus_7_5
			product	hypothetical protein
			transl_table	11
4157	4035	CDS
			locus_tag	Gaeavirus_7_6
			product	hypothetical protein
			transl_table	11
5426	4263	CDS
			locus_tag	Gaeavirus_7_7
			product	hypothetical protein
			transl_table	11
5608	5486	CDS
			locus_tag	Gaeavirus_7_8
			product	hypothetical protein
			transl_table	11
6943	5792	CDS
			locus_tag	Gaeavirus_7_9
			product	hypothetical protein
			transl_table	11
8192	6978	CDS
			locus_tag	Gaeavirus_7_10
			product	hypothetical protein
			transl_table	11
9374	8196	CDS
			locus_tag	Gaeavirus_7_11
			product	RNA ligase
			transl_table	11
10630	11310	CDS
			locus_tag	Gaeavirus_7_12
			product	hypothetical protein
			transl_table	11
11529	12200	CDS
			locus_tag	Gaeavirus_7_13
			product	hypothetical protein
			transl_table	11
12280	12966	CDS
			locus_tag	Gaeavirus_7_14
			product	hypothetical protein
			transl_table	11
13426	12974	CDS
			locus_tag	Gaeavirus_7_15
			product	hypothetical protein
			transl_table	11
13584	14795	CDS
			locus_tag	Gaeavirus_7_16
			product	hypothetical protein
			transl_table	11
15735	16706	CDS
			locus_tag	Gaeavirus_7_17
			product	hypothetical protein CBB97_15450
			transl_table	11
17450	16722	CDS
			locus_tag	Gaeavirus_7_18
			product	hypothetical protein
			transl_table	11
18026	17487	CDS
			locus_tag	Gaeavirus_7_19
			product	hypothetical protein
			transl_table	11
18406	18092	CDS
			locus_tag	Gaeavirus_7_20
			product	hypothetical protein
			transl_table	11
18576	18403	CDS
			locus_tag	Gaeavirus_7_21
			product	hypothetical protein
			transl_table	11
19427	18636	CDS
			locus_tag	Gaeavirus_7_22
			product	hypothetical protein
			transl_table	11
19934	19548	CDS
			locus_tag	Gaeavirus_7_23
			product	hypothetical protein
			transl_table	11
21235	19970	CDS
			locus_tag	Gaeavirus_7_24
			product	hypothetical protein
			transl_table	11
22176	21253	CDS
			locus_tag	Gaeavirus_7_25
			product	hypothetical protein
			transl_table	11
22801	22208	CDS
			locus_tag	Gaeavirus_7_26
			product	hypothetical protein
			transl_table	11
>Gaeavirus_8
2	1363	CDS
			locus_tag	Gaeavirus_8_1
			product	putative site-specific DNA-methyltransferase
			transl_table	11
1574	4312	CDS
			locus_tag	Gaeavirus_8_2
			product	ubiquitin-activating enzyme
			transl_table	11
4302	4535	CDS
			locus_tag	Gaeavirus_8_3
			product	hypothetical protein
			transl_table	11
4876	4778	CDS
			locus_tag	Gaeavirus_8_4
			product	hypothetical protein
			transl_table	11
5592	6440	CDS
			locus_tag	Gaeavirus_8_5
			product	hypothetical protein
			transl_table	11
6491	7192	CDS
			locus_tag	Gaeavirus_8_6
			product	hypothetical protein
			transl_table	11
7824	7189	CDS
			locus_tag	Gaeavirus_8_7
			product	hypothetical protein
			transl_table	11
7895	8311	CDS
			locus_tag	Gaeavirus_8_8
			product	hypothetical protein
			transl_table	11
8902	8312	CDS
			locus_tag	Gaeavirus_8_9
			product	hypothetical protein
			transl_table	11
9453	8995	CDS
			locus_tag	Gaeavirus_8_10
			product	hypothetical protein
			transl_table	11
9775	10314	CDS
			locus_tag	Gaeavirus_8_11
			product	hypothetical protein
			transl_table	11
10596	11135	CDS
			locus_tag	Gaeavirus_8_12
			product	hypothetical protein
			transl_table	11
11259	12473	CDS
			locus_tag	Gaeavirus_8_13
			product	hypothetical protein
			transl_table	11
13437	13195	CDS
			locus_tag	Gaeavirus_8_14
			product	hypothetical protein
			transl_table	11
13592	14431	CDS
			locus_tag	Gaeavirus_8_15
			product	hypothetical protein
			transl_table	11
15089	14568	CDS
			locus_tag	Gaeavirus_8_16
			product	hypothetical protein
			transl_table	11
15471	15836	CDS
			locus_tag	Gaeavirus_8_17
			product	hypothetical protein
			transl_table	11
17185	15818	CDS
			locus_tag	Gaeavirus_8_18
			product	hypothetical protein
			transl_table	11
18565	17243	CDS
			locus_tag	Gaeavirus_8_19
			product	putative orfan
			transl_table	11
18740	19339	CDS
			locus_tag	Gaeavirus_8_20
			product	Thymidine kinase
			transl_table	11
19381	19566	CDS
			locus_tag	Gaeavirus_8_21
			product	dna topoisomerase 1b
			transl_table	11
>Gaeavirus_9
661	14	CDS
			locus_tag	Gaeavirus_9_1
			product	hypothetical protein Klosneuvirus_1_187
			transl_table	11
1332	697	CDS
			locus_tag	Gaeavirus_9_2
			product	hypothetical protein
			transl_table	11
2875	1673	CDS
			locus_tag	Gaeavirus_9_3
			product	hypothetical protein
			transl_table	11
4682	3321	CDS
			locus_tag	Gaeavirus_9_4
			product	HNH endonuclease
			transl_table	11
5963	4866	CDS
			locus_tag	Gaeavirus_9_5
			product	hypothetical protein Catovirus_1_1084
			transl_table	11
6124	7833	CDS
			locus_tag	Gaeavirus_9_6
			product	hypothetical protein
			transl_table	11
7971	8228	CDS
			locus_tag	Gaeavirus_9_7
			product	hypothetical protein
			transl_table	11
8511	8765	CDS
			locus_tag	Gaeavirus_9_8
			product	hypothetical protein
			transl_table	11
8990	8862	CDS
			locus_tag	Gaeavirus_9_9
			product	hypothetical protein
			transl_table	11
9133	10410	CDS
			locus_tag	Gaeavirus_9_10
			product	7tm chemosensory receptor/ankyrin repeat domain-containing protein
			transl_table	11
10930	11919	CDS
			locus_tag	Gaeavirus_9_11
			product	hypothetical protein
			transl_table	11
12186	12602	CDS
			locus_tag	Gaeavirus_9_12
			product	hypothetical protein BMW23_0444
			transl_table	11
12697	16986	CDS
			locus_tag	Gaeavirus_9_13
			product	hypothetical protein
			transl_table	11
17029	18360	CDS
			locus_tag	Gaeavirus_9_14
			product	hypothetical protein COW71_09675
			transl_table	11
>Gaeavirus_10
412	546	CDS
			locus_tag	Gaeavirus_10_1
			product	hypothetical protein
			transl_table	11
607	1092	CDS
			locus_tag	Gaeavirus_10_2
			product	Polyubiquitin
			transl_table	11
1193	3922	CDS
			locus_tag	Gaeavirus_10_3
			product	hypothetical protein
			transl_table	11
3813	4748	CDS
			locus_tag	Gaeavirus_10_4
			product	serine/threonine protein phosphatase
			transl_table	11
6677	5202	CDS
			locus_tag	Gaeavirus_10_5
			product	hypothetical protein Indivirus_3_8
			transl_table	11
7798	6707	CDS
			locus_tag	Gaeavirus_10_6
			product	hypothetical protein
			transl_table	11
9155	8022	CDS
			locus_tag	Gaeavirus_10_7
			product	hypothetical protein
			transl_table	11
9245	11494	CDS
			locus_tag	Gaeavirus_10_8
			product	tubulin-tyrosine ligase family protein
			transl_table	11
11625	11497	CDS
			locus_tag	Gaeavirus_10_9
			product	hypothetical protein
			transl_table	11
12004	11831	CDS
			locus_tag	Gaeavirus_10_10
			product	hypothetical protein
			transl_table	11
12183	12016	CDS
			locus_tag	Gaeavirus_10_11
			product	hypothetical protein
			transl_table	11
13039	12326	CDS
			locus_tag	Gaeavirus_10_12
			product	hypothetical protein
			transl_table	11
13175	14353	CDS
			locus_tag	Gaeavirus_10_13
			product	serine/threonine protein kinase
			transl_table	11
14823	14365	CDS
			locus_tag	Gaeavirus_10_14
			product	hypothetical protein
			transl_table	11
14966	15205	CDS
			locus_tag	Gaeavirus_10_15
			product	hypothetical protein
			transl_table	11
15306	16829	CDS
			locus_tag	Gaeavirus_10_16
			product	ankyrin repeat protein
			transl_table	11
>Gaeavirus_11
2	268	CDS
			locus_tag	Gaeavirus_11_1
			product	eukaryotic translation initiation factor 5B-like protein, partial
			transl_table	11
283	486	CDS
			locus_tag	Gaeavirus_11_2
			product	hypothetical protein
			transl_table	11
528	1487	CDS
			locus_tag	Gaeavirus_11_3
			product	packaging ATPase
			transl_table	11
1493	3370	CDS
			locus_tag	Gaeavirus_11_4
			product	packaging ATPase
			transl_table	11
4249	3545	CDS
			locus_tag	Gaeavirus_11_5
			product	hypothetical protein
			transl_table	11
4691	4278	CDS
			locus_tag	Gaeavirus_11_6
			product	pentapeptide repeat-containing protein
			transl_table	11
5586	4729	CDS
			locus_tag	Gaeavirus_11_7
			product	hypothetical protein
			transl_table	11
5702	6562	CDS
			locus_tag	Gaeavirus_11_8
			product	hypothetical protein
			transl_table	11
7105	7314	CDS
			locus_tag	Gaeavirus_11_9
			product	hypothetical protein
			transl_table	11
8293	7382	CDS
			locus_tag	Gaeavirus_11_10
			product	conserved hypothetical protein
			transl_table	11
8937	8716	CDS
			locus_tag	Gaeavirus_11_11
			product	hypothetical protein
			transl_table	11
9277	9047	CDS
			locus_tag	Gaeavirus_11_12
			product	hypothetical protein
			transl_table	11
9626	9351	CDS
			locus_tag	Gaeavirus_11_13
			product	hypothetical protein
			transl_table	11
10432	9686	CDS
			locus_tag	Gaeavirus_11_14
			product	hypothetical protein
			transl_table	11
11275	10499	CDS
			locus_tag	Gaeavirus_11_15
			product	hypothetical protein
			transl_table	11
11368	11652	CDS
			locus_tag	Gaeavirus_11_16
			product	hypothetical protein
			transl_table	11
12123	11710	CDS
			locus_tag	Gaeavirus_11_17
			product	hypothetical protein
			transl_table	11
12797	12168	CDS
			locus_tag	Gaeavirus_11_18
			product	hypothetical protein
			transl_table	11
13540	12902	CDS
			locus_tag	Gaeavirus_11_19
			product	hypothetical protein
			transl_table	11
14898	13777	CDS
			locus_tag	Gaeavirus_11_20
			product	hypothetical protein
			transl_table	11
15087	15608	CDS
			locus_tag	Gaeavirus_11_21
			product	hypothetical protein
			transl_table	11
16697	15654	CDS
			locus_tag	Gaeavirus_11_22
			product	hypothetical protein
			transl_table	11
>Gaeavirus_12
2040	16	CDS
			locus_tag	Gaeavirus_12_1
			product	HrpA-like RNA helicase
			transl_table	11
3444	2077	CDS
			locus_tag	Gaeavirus_12_2
			product	hypothetical protein Klosneuvirus_2_260
			transl_table	11
3526	4269	CDS
			locus_tag	Gaeavirus_12_3
			product	hypothetical protein
			transl_table	11
4613	4855	CDS
			locus_tag	Gaeavirus_12_4
			product	hypothetical protein
			transl_table	11
4892	5788	CDS
			locus_tag	Gaeavirus_12_5
			product	hypothetical protein
			transl_table	11
5801	6598	CDS
			locus_tag	Gaeavirus_12_6
			product	hypothetical protein
			transl_table	11
7077	6595	CDS
			locus_tag	Gaeavirus_12_7
			product	hypothetical protein
			transl_table	11
7913	7110	CDS
			locus_tag	Gaeavirus_12_8
			product	hypothetical protein
			transl_table	11
8065	8598	CDS
			locus_tag	Gaeavirus_12_9
			product	hypothetical protein
			transl_table	11
9322	8735	CDS
			locus_tag	Gaeavirus_12_10
			product	transcription elongation factor TFIIS
			transl_table	11
10183	9377	CDS
			locus_tag	Gaeavirus_12_11
			product	hypothetical protein
			transl_table	11
10352	10134	CDS
			locus_tag	Gaeavirus_12_12
			product	hypothetical protein
			transl_table	11
10631	10389	CDS
			locus_tag	Gaeavirus_12_13
			product	hypothetical protein
			transl_table	11
11718	10975	CDS
			locus_tag	Gaeavirus_12_14
			product	hypothetical protein
			transl_table	11
11800	13167	CDS
			locus_tag	Gaeavirus_12_15
			product	hypothetical protein Klosneuvirus_2_260
			transl_table	11
13204	15228	CDS
			locus_tag	Gaeavirus_12_16
			product	HrpA-like RNA helicase
			transl_table	11
>Gaeavirus_13
524	120	CDS
			locus_tag	Gaeavirus_13_1
			product	hypothetical protein
			transl_table	11
598	1035	CDS
			locus_tag	Gaeavirus_13_2
			product	hypothetical protein
			transl_table	11
1400	1044	CDS
			locus_tag	Gaeavirus_13_3
			product	hypothetical protein
			transl_table	11
1408	1500	CDS
			locus_tag	Gaeavirus_13_4
			product	hypothetical protein
			transl_table	11
3800	1989	CDS
			locus_tag	Gaeavirus_13_5
			product	hypothetical protein
			transl_table	11
3930	5063	CDS
			locus_tag	Gaeavirus_13_6
			product	hypothetical protein
			transl_table	11
5189	6319	CDS
			locus_tag	Gaeavirus_13_7
			product	hypothetical protein
			transl_table	11
6428	7162	CDS
			locus_tag	Gaeavirus_13_8
			product	hypothetical protein BRD00_05895
			transl_table	11
9709	7370	CDS
			locus_tag	Gaeavirus_13_9
			product	hypothetical protein TRFO_23625
			transl_table	11
12156	9769	CDS
			locus_tag	Gaeavirus_13_10
			product	hypothetical protein TRFO_23625
			transl_table	11
12374	13834	CDS
			locus_tag	Gaeavirus_13_11
			product	hypothetical protein
			transl_table	11
13834	14163	CDS
			locus_tag	Gaeavirus_13_12
			product	hypothetical protein
			transl_table	11
>Gaeavirus_14
203	30	CDS
			locus_tag	Gaeavirus_14_1
			product	hypothetical protein
			transl_table	11
747	427	CDS
			locus_tag	Gaeavirus_14_2
			product	hypothetical protein
			transl_table	11
1364	1002	CDS
			locus_tag	Gaeavirus_14_3
			product	hypothetical protein Klosneuvirus_6_19
			transl_table	11
2357	1434	CDS
			locus_tag	Gaeavirus_14_4
			product	hypothetical protein
			transl_table	11
2468	2379	CDS
			locus_tag	Gaeavirus_14_5
			product	hypothetical protein
			transl_table	11
2467	2988	CDS
			locus_tag	Gaeavirus_14_6
			product	hypothetical protein
			transl_table	11
3114	4097	CDS
			locus_tag	Gaeavirus_14_7
			product	hypothetical protein
			transl_table	11
4187	4966	CDS
			locus_tag	Gaeavirus_14_8
			product	hypothetical protein
			transl_table	11
5078	5338	CDS
			locus_tag	Gaeavirus_14_9
			product	hypothetical protein
			transl_table	11
5428	6528	CDS
			locus_tag	Gaeavirus_14_10
			product	hypothetical protein
			transl_table	11
6739	7446	CDS
			locus_tag	Gaeavirus_14_11
			product	hypothetical protein
			transl_table	11
7398	7658	CDS
			locus_tag	Gaeavirus_14_12
			product	hypothetical protein
			transl_table	11
7726	8652	CDS
			locus_tag	Gaeavirus_14_13
			product	hypothetical protein
			transl_table	11
8727	9821	CDS
			locus_tag	Gaeavirus_14_14
			product	hypothetical protein
			transl_table	11
10010	10141	CDS
			locus_tag	Gaeavirus_14_15
			product	hypothetical protein
			transl_table	11
10178	11086	CDS
			locus_tag	Gaeavirus_14_16
			product	hypothetical protein
			transl_table	11
11108	12031	CDS
			locus_tag	Gaeavirus_14_17
			product	hypothetical protein
			transl_table	11
12101	12463	CDS
			locus_tag	Gaeavirus_14_18
			product	hypothetical protein Klosneuvirus_6_19
			transl_table	11
12718	13038	CDS
			locus_tag	Gaeavirus_14_19
			product	hypothetical protein
			transl_table	11
13262	13435	CDS
			locus_tag	Gaeavirus_14_20
			product	hypothetical protein
			transl_table	11
>Gaeavirus_15
531	1148	CDS
			locus_tag	Gaeavirus_15_1
			product	hypothetical protein
			transl_table	11
1243	1881	CDS
			locus_tag	Gaeavirus_15_2
			product	hypothetical protein Klosneuvirus_2_263
			transl_table	11
1983	2144	CDS
			locus_tag	Gaeavirus_15_3
			product	hypothetical protein
			transl_table	11
2215	2406	CDS
			locus_tag	Gaeavirus_15_4
			product	hypothetical protein
			transl_table	11
2435	3043	CDS
			locus_tag	Gaeavirus_15_5
			product	hypothetical protein Indivirus_3_26
			transl_table	11
3775	3230	CDS
			locus_tag	Gaeavirus_15_6
			product	hypothetical protein
			transl_table	11
4413	4129	CDS
			locus_tag	Gaeavirus_15_7
			product	hypothetical protein
			transl_table	11
4479	5732	CDS
			locus_tag	Gaeavirus_15_8
			product	hypothetical protein
			transl_table	11
5775	7529	CDS
			locus_tag	Gaeavirus_15_9
			product	cAMP dependent protein kinase
			transl_table	11
9165	7561	CDS
			locus_tag	Gaeavirus_15_10
			product	hypothetical protein
			transl_table	11
9528	9190	CDS
			locus_tag	Gaeavirus_15_11
			product	hypothetical protein
			transl_table	11
10816	11007	CDS
			locus_tag	Gaeavirus_15_12
			product	hypothetical protein
			transl_table	11
11220	11348	CDS
			locus_tag	Gaeavirus_15_13
			product	hypothetical protein
			transl_table	11
11674	11928	CDS
			locus_tag	Gaeavirus_15_14
			product	hypothetical protein
			transl_table	11
>Gaeavirus_16
1538	3	CDS
			locus_tag	Gaeavirus_16_1
			product	hypothetical protein
			transl_table	11
2841	4121	CDS
			locus_tag	Gaeavirus_16_2
			product	hypothetical protein Klosneuvirus_2_225
			transl_table	11
4269	4385	CDS
			locus_tag	Gaeavirus_16_3
			product	hypothetical protein
			transl_table	11
4718	5413	CDS
			locus_tag	Gaeavirus_16_4
			product	putative minor capsid protein
			transl_table	11
6875	5940	CDS
			locus_tag	Gaeavirus_16_5
			product	Ulp1 protease
			transl_table	11
7169	6939	CDS
			locus_tag	Gaeavirus_16_6
			product	hypothetical protein
			transl_table	11
7642	7824	CDS
			locus_tag	Gaeavirus_16_7
			product	hypothetical protein
			transl_table	11
7855	8772	CDS
			locus_tag	Gaeavirus_16_8
			product	hypothetical protein
			transl_table	11
8836	9573	CDS
			locus_tag	Gaeavirus_16_9
			product	hypothetical protein
			transl_table	11
11195	9636	CDS
			locus_tag	Gaeavirus_16_10
			product	YqaJ-like viral recombinase
			transl_table	11
>Gaeavirus_17
776	48	CDS
			locus_tag	Gaeavirus_17_1
			product	hypothetical protein
			transl_table	11
1405	836	CDS
			locus_tag	Gaeavirus_17_2
			product	hypothetical protein Indivirus_1_169
			transl_table	11
2817	1444	CDS
			locus_tag	Gaeavirus_17_3
			product	hypothetical protein
			transl_table	11
2912	4465	CDS
			locus_tag	Gaeavirus_17_4
			product	hypothetical protein
			transl_table	11
4496	5338	CDS
			locus_tag	Gaeavirus_17_5
			product	hypothetical protein
			transl_table	11
5383	5655	CDS
			locus_tag	Gaeavirus_17_6
			product	hypothetical protein
			transl_table	11
5869	6846	CDS
			locus_tag	Gaeavirus_17_7
			product	hypothetical protein
			transl_table	11
6982	8073	CDS
			locus_tag	Gaeavirus_17_8
			product	hypothetical protein
			transl_table	11
10372	10512	CDS
			locus_tag	Gaeavirus_17_9
			product	hypothetical protein
			transl_table	11
>Gaeavirus_18
1292	3	CDS
			locus_tag	Gaeavirus_18_1
			product	hypothetical protein
			transl_table	11
1354	2433	CDS
			locus_tag	Gaeavirus_18_2
			product	hypothetical protein LRAMOSA01193
			transl_table	11
3044	2430	CDS
			locus_tag	Gaeavirus_18_3
			product	hypothetical protein
			transl_table	11
3989	3111	CDS
			locus_tag	Gaeavirus_18_4
			product	hypothetical protein Catovirus_2_315
			transl_table	11
4697	4035	CDS
			locus_tag	Gaeavirus_18_5
			product	hypothetical protein
			transl_table	11
5913	4810	CDS
			locus_tag	Gaeavirus_18_6
			product	hypothetical protein
			transl_table	11
6432	5941	CDS
			locus_tag	Gaeavirus_18_7
			product	hypothetical protein
			transl_table	11
7411	6515	CDS
			locus_tag	Gaeavirus_18_8
			product	hypothetical protein Klosneuvirus_1_127
			transl_table	11
9271	9657	CDS
			locus_tag	Gaeavirus_18_9
			product	DNA directed RNA polymerase subunit L
			transl_table	11
>Gaeavirus_19
7063	638	CDS
			locus_tag	Gaeavirus_19_1
			product	early transcription factor VETF large subunit
			transl_table	11
9185	7131	CDS
			locus_tag	Gaeavirus_19_2
			product	DEAD/SNF2-like helicase
			transl_table	11
9587	9243	CDS
			locus_tag	Gaeavirus_19_3
			product	DnaQ-like or DEDD 3'-5' exonuclease
			transl_table	11
>Gaeavirus_20
1	273	CDS
			locus_tag	Gaeavirus_20_1
			product	PREDICTED: asparagine synthetase
			transl_table	11
766	377	CDS
			locus_tag	Gaeavirus_20_2
			product	replication factor C small subunit
			transl_table	11
820	1680	CDS
			locus_tag	Gaeavirus_20_3
			product	apurinic endonuclease
			transl_table	11
4032	1663	CDS
			locus_tag	Gaeavirus_20_4
			product	Aminoglycoside phosphotransferase
			transl_table	11
4100	5137	CDS
			locus_tag	Gaeavirus_20_5
			product	dTDP-4-oxo-6-deoxy-D-allose reductase
			transl_table	11
5193	6029	CDS
			locus_tag	Gaeavirus_20_6
			product	hypothetical protein A2673_03695
			transl_table	11
6080	7120	CDS
			locus_tag	Gaeavirus_20_7
			product	hypothetical protein
			transl_table	11
7129	7947	CDS
			locus_tag	Gaeavirus_20_8
			product	hypothetical protein
			transl_table	11
8664	7948	CDS
			locus_tag	Gaeavirus_20_9
			product	hypothetical protein NIES204_43640 (plasmid)
			transl_table	11
9138	8854	CDS
			locus_tag	Gaeavirus_20_10
			product	hypothetical protein
			transl_table	11
>Gaeavirus_21
1030	194	CDS
			locus_tag	Gaeavirus_21_1
			product	hypothetical protein
			transl_table	11
1341	2060	CDS
			locus_tag	Gaeavirus_21_2
			product	hypothetical protein
			transl_table	11
3373	2447	CDS
			locus_tag	Gaeavirus_21_3
			product	hypothetical protein
			transl_table	11
3474	4028	CDS
			locus_tag	Gaeavirus_21_4
			product	hypothetical protein MegaChil _gp0300
			transl_table	11
4306	5739	CDS
			locus_tag	Gaeavirus_21_5
			product	serine/threonine protein kinase
			transl_table	11
6241	5966	CDS
			locus_tag	Gaeavirus_21_6
			product	hypothetical protein
			transl_table	11
8909	6561	CDS
			locus_tag	Gaeavirus_21_7
			product	D5-like helicase-primase
			transl_table	11
>Gaeavirus_22
1976	405	CDS
			locus_tag	Gaeavirus_22_1
			product	hypothetical protein
			transl_table	11
2314	3024	CDS
			locus_tag	Gaeavirus_22_2
			product	hypothetical protein
			transl_table	11
3077	3643	CDS
			locus_tag	Gaeavirus_22_3
			product	hypothetical protein
			transl_table	11
3737	4708	CDS
			locus_tag	Gaeavirus_22_4
			product	hypothetical protein
			transl_table	11
4824	5597	CDS
			locus_tag	Gaeavirus_22_5
			product	hypothetical protein
			transl_table	11
5727	6797	CDS
			locus_tag	Gaeavirus_22_6
			product	hypothetical protein
			transl_table	11
7118	6858	CDS
			locus_tag	Gaeavirus_22_7
			product	hypothetical protein
			transl_table	11
8449	8688	CDS
			locus_tag	Gaeavirus_22_8
			product	hypothetical protein
			transl_table	11
8777	8863	CDS
			locus_tag	Gaeavirus_22_9
			product	hypothetical protein
			transl_table	11
>Gaeavirus_23
259	1668	CDS
			locus_tag	Gaeavirus_23_1
			product	hypothetical protein
			transl_table	11
1712	2335	CDS
			locus_tag	Gaeavirus_23_2
			product	hypothetical protein
			transl_table	11
2405	2554	CDS
			locus_tag	Gaeavirus_23_3
			product	hypothetical protein
			transl_table	11
2607	3317	CDS
			locus_tag	Gaeavirus_23_4
			product	hypothetical protein
			transl_table	11
3380	3619	CDS
			locus_tag	Gaeavirus_23_5
			product	hypothetical protein
			transl_table	11
3862	4545	CDS
			locus_tag	Gaeavirus_23_6
			product	hypothetical protein
			transl_table	11
5535	6044	CDS
			locus_tag	Gaeavirus_23_7
			product	hypothetical protein
			transl_table	11
6135	6695	CDS
			locus_tag	Gaeavirus_23_8
			product	hypothetical protein
			transl_table	11
7470	7039	CDS
			locus_tag	Gaeavirus_23_9
			product	hypothetical protein
			transl_table	11
7598	8413	CDS
			locus_tag	Gaeavirus_23_10
			product	hypothetical protein
			transl_table	11
8451	8540	CDS
			locus_tag	Gaeavirus_23_11
			product	hypothetical protein
			transl_table	11
>Gaeavirus_24
217	2	CDS
			locus_tag	Gaeavirus_24_1
			product	hypothetical protein
			transl_table	11
328	576	CDS
			locus_tag	Gaeavirus_24_2
			product	hypothetical protein
			transl_table	11
725	856	CDS
			locus_tag	Gaeavirus_24_3
			product	hypothetical protein
			transl_table	11
946	1527	CDS
			locus_tag	Gaeavirus_24_4
			product	hypothetical protein
			transl_table	11
3861	1693	CDS
			locus_tag	Gaeavirus_24_5
			product	DEXDc helicase
			transl_table	11
5291	4263	CDS
			locus_tag	Gaeavirus_24_6
			product	glutaminyl peptide cyclotransferase
			transl_table	11
5362	5703	CDS
			locus_tag	Gaeavirus_24_7
			product	hypothetical protein
			transl_table	11
5889	5725	CDS
			locus_tag	Gaeavirus_24_8
			product	hypothetical protein
			transl_table	11
8536	6500	CDS
			locus_tag	Gaeavirus_24_9
			product	superfamily II helicase
			transl_table	11
>Gaeavirus_25
3	314	CDS
			locus_tag	Gaeavirus_25_1
			product	hypothetical protein
			transl_table	11
1299	307	CDS
			locus_tag	Gaeavirus_25_2
			product	hypothetical protein
			transl_table	11
1397	2503	CDS
			locus_tag	Gaeavirus_25_3
			product	hypothetical protein Catovirus_2_292
			transl_table	11
3388	2546	CDS
			locus_tag	Gaeavirus_25_4
			product	hypothetical protein
			transl_table	11
3682	4563	CDS
			locus_tag	Gaeavirus_25_5
			product	TATA box binding protein
			transl_table	11
4856	4581	CDS
			locus_tag	Gaeavirus_25_6
			product	hypothetical protein
			transl_table	11
5475	5140	CDS
			locus_tag	Gaeavirus_25_7
			product	hypothetical protein
			transl_table	11
6386	5766	CDS
			locus_tag	Gaeavirus_25_8
			product	hypothetical protein
			transl_table	11
7010	6531	CDS
			locus_tag	Gaeavirus_25_9
			product	hypothetical protein
			transl_table	11
>Gaeavirus_26
211	2	CDS
			locus_tag	Gaeavirus_26_1
			product	IFN-gamma-induced (putative GTP-binding protein) protein Mg11 related protein
			transl_table	11
1257	235	CDS
			locus_tag	Gaeavirus_26_2
			product	replication factor C small subunit
			transl_table	11
2289	4325	CDS
			locus_tag	Gaeavirus_26_3
			product	DEAD/SNF2-like helicase
			transl_table	11
4639	6279	CDS
			locus_tag	Gaeavirus_26_4
			product	putative site-specific DNA-methyltransferase
			transl_table	11
>Gaeavirus_27
1	318	CDS
			locus_tag	Gaeavirus_27_1
			product	hypothetical protein
			transl_table	11
582	1124	CDS
			locus_tag	Gaeavirus_27_2
			product	hypothetical protein
			transl_table	11
1145	1771	CDS
			locus_tag	Gaeavirus_27_3
			product	NLI interacting factor, partial
			transl_table	11
2565	3101	CDS
			locus_tag	Gaeavirus_27_4
			product	hypothetical protein
			transl_table	11
3173	3817	CDS
			locus_tag	Gaeavirus_27_5
			product	hypothetical protein
			transl_table	11
3946	5436	CDS
			locus_tag	Gaeavirus_27_6
			product	hypothetical protein
			transl_table	11
6375	6232	CDS
			locus_tag	Gaeavirus_27_7
			product	hypothetical protein
			transl_table	11
>Gaeavirus_28
1380	1	CDS
			locus_tag	Gaeavirus_28_1
			product	hypothetical protein
			transl_table	11
2228	1515	CDS
			locus_tag	Gaeavirus_28_2
			product	hypothetical protein
			transl_table	11
2952	2266	CDS
			locus_tag	Gaeavirus_28_3
			product	hypothetical protein
			transl_table	11
4032	3586	CDS
			locus_tag	Gaeavirus_28_4
			product	hypothetical protein
			transl_table	11
6214	4835	CDS
			locus_tag	Gaeavirus_28_5
			product	AAA family ATPase
			transl_table	11
>Gaeavirus_29
1075	2	CDS
			locus_tag	Gaeavirus_29_1
			product	hypothetical protein
			transl_table	11
1786	1181	CDS
			locus_tag	Gaeavirus_29_2
			product	hypothetical protein
			transl_table	11
2080	3738	CDS
			locus_tag	Gaeavirus_29_3
			product	hypothetical protein
			transl_table	11
5939	6250	CDS
			locus_tag	Gaeavirus_29_4
			product	hypothetical protein
			transl_table	11
>Gaeavirus_30
429	1	CDS
			locus_tag	Gaeavirus_30_1
			product	nUDIX hydrolase
			transl_table	11
1141	2412	CDS
			locus_tag	Gaeavirus_30_2
			product	restriction-modification methylase
			transl_table	11
2422	2934	CDS
			locus_tag	Gaeavirus_30_3
			product	hypothetical protein
			transl_table	11
3889	2951	CDS
			locus_tag	Gaeavirus_30_4
			product	hypothetical protein
			transl_table	11
4717	3956	CDS
			locus_tag	Gaeavirus_30_5
			product	hypothetical protein
			transl_table	11
5584	5447	CDS
			locus_tag	Gaeavirus_30_6
			product	hypothetical protein
			transl_table	11
>Gaeavirus_31
1941	1	CDS
			locus_tag	Gaeavirus_31_1
			product	NAD-dependent DNA ligase
			transl_table	11
2639	2049	CDS
			locus_tag	Gaeavirus_31_2
			product	hypothetical protein
			transl_table	11
3509	2676	CDS
			locus_tag	Gaeavirus_31_3
			product	hypothetical protein
			transl_table	11
4137	3598	CDS
			locus_tag	Gaeavirus_31_4
			product	hypothetical protein
			transl_table	11
5131	4217	CDS
			locus_tag	Gaeavirus_31_5
			product	hypothetical protein
			transl_table	11
5380	5273	CDS
			locus_tag	Gaeavirus_31_6
			product	hypothetical protein
			transl_table	11
>Gaeavirus_32
434	3	CDS
			locus_tag	Gaeavirus_32_1
			product	hypothetical protein Indivirus_4_32
			transl_table	11
1144	512	CDS
			locus_tag	Gaeavirus_32_2
			product	hypothetical protein
			transl_table	11
2189	1212	CDS
			locus_tag	Gaeavirus_32_3
			product	hypothetical protein Indivirus_4_36
			transl_table	11
2674	2210	CDS
			locus_tag	Gaeavirus_32_4
			product	hypothetical protein
			transl_table	11
4133	2745	CDS
			locus_tag	Gaeavirus_32_5
			product	hypothetical protein BMW23_0528
			transl_table	11
4183	5088	CDS
			locus_tag	Gaeavirus_32_6
			product	hypothetical protein BMW23_0529
			transl_table	11
5335	5147	CDS
			locus_tag	Gaeavirus_32_7
			product	hypothetical protein
			transl_table	11
>Gaeavirus_33
847	2	CDS
			locus_tag	Gaeavirus_33_1
			product	hypothetical protein
			transl_table	11
2499	1183	CDS
			locus_tag	Gaeavirus_33_2
			product	hypothetical protein
			transl_table	11
2833	2585	CDS
			locus_tag	Gaeavirus_33_3
			product	hypothetical protein
			transl_table	11
2923	3393	CDS
			locus_tag	Gaeavirus_33_4
			product	hypothetical protein
			transl_table	11
3896	3381	CDS
			locus_tag	Gaeavirus_33_5
			product	hypothetical protein
			transl_table	11
4538	4885	CDS
			locus_tag	Gaeavirus_33_6
			product	hypothetical protein
			transl_table	11
>Gaeavirus_34
1102	95	CDS
			locus_tag	Gaeavirus_34_1
			product	hypothetical protein
			transl_table	11
2188	1745	CDS
			locus_tag	Gaeavirus_34_2
			product	hypothetical protein
			transl_table	11
2465	3442	CDS
			locus_tag	Gaeavirus_34_3
			product	hypothetical protein
			transl_table	11
3582	4118	CDS
			locus_tag	Gaeavirus_34_4
			product	hypothetical protein
			transl_table	11
4160	5104	CDS
			locus_tag	Gaeavirus_34_5
			product	hypothetical protein
			transl_table	11
>Gaeavirus_35
1	228	CDS
			locus_tag	Gaeavirus_35_1
			product	hypothetical protein
			transl_table	11
291	1088	CDS
			locus_tag	Gaeavirus_35_2
			product	hypothetical protein
			transl_table	11
1126	1230	CDS
			locus_tag	Gaeavirus_35_3
			product	hypothetical protein
			transl_table	11
1279	2115	CDS
			locus_tag	Gaeavirus_35_4
			product	hypothetical protein
			transl_table	11
2149	3069	CDS
			locus_tag	Gaeavirus_35_5
			product	hypothetical protein
			transl_table	11
3126	4094	CDS
			locus_tag	Gaeavirus_35_6
			product	hypothetical protein
			transl_table	11
4158	4358	CDS
			locus_tag	Gaeavirus_35_7
			product	hypothetical protein
			transl_table	11
>Gaeavirus_36
433	50	CDS
			locus_tag	Gaeavirus_36_1
			product	hypothetical protein
			transl_table	11
1913	483	CDS
			locus_tag	Gaeavirus_36_2
			product	hypothetical protein glt_00800
			transl_table	11
3978	1894	CDS
			locus_tag	Gaeavirus_36_3
			product	AAA family ATPase
			transl_table	11
4100	3981	CDS
			locus_tag	Gaeavirus_36_4
			product	hypothetical protein
			transl_table	11
>Gaeavirus_37
1487	3	CDS
			locus_tag	Gaeavirus_37_1
			product	hypothetical protein KFL_003890020
			transl_table	11
2091	2621	CDS
			locus_tag	Gaeavirus_37_2
			product	hypothetical protein
			transl_table	11
3666	3752	CDS
			locus_tag	Gaeavirus_37_3
			product	hypothetical protein
			transl_table	11
>Gaeavirus_38
520	2	CDS
			locus_tag	Gaeavirus_38_1
			product	hypothetical protein Klosneuvirus_2_68
			transl_table	11
2201	705	CDS
			locus_tag	Gaeavirus_38_2
			product	hypothetical protein Catovirus_2_124
			transl_table	11
3495	2431	CDS
			locus_tag	Gaeavirus_38_3
			product	lysophospholipid acyltransferase
			transl_table	11
3729	3544	CDS
			locus_tag	Gaeavirus_38_4
			product	hypothetical protein
			transl_table	11
>Gaeavirus_39
161	3	CDS
			locus_tag	Gaeavirus_39_1
			product	hypothetical protein
			transl_table	11
491	1273	CDS
			locus_tag	Gaeavirus_39_2
			product	hypothetical protein
			transl_table	11
2034	1288	CDS
			locus_tag	Gaeavirus_39_3
			product	hypothetical protein
			transl_table	11
2115	2963	CDS
			locus_tag	Gaeavirus_39_4
			product	hypothetical protein
			transl_table	11
3009	3479	CDS
			locus_tag	Gaeavirus_39_5
			product	hypothetical protein
			transl_table	11
>Gaeavirus_40
501	1907	CDS
			locus_tag	Gaeavirus_40_1
			product	replication factor C large subunit
			transl_table	11
3101	2862	CDS
			locus_tag	Gaeavirus_40_2
			product	hypothetical protein
			transl_table	11
3373	3615	CDS
			locus_tag	Gaeavirus_40_3
			product	hypothetical protein
			transl_table	11
>Gaeavirus_41
937	419	CDS
			locus_tag	Gaeavirus_41_1
			product	hypothetical protein
			transl_table	11
3315	979	CDS
			locus_tag	Gaeavirus_41_2
			product	DEAD/SNF2-like helicase
			transl_table	11
>Gaeavirus_42
115	2	CDS
			locus_tag	Gaeavirus_42_1
			product	hypothetical protein
			transl_table	11
1151	153	CDS
			locus_tag	Gaeavirus_42_2
			product	hypothetical protein
			transl_table	11
1811	1173	CDS
			locus_tag	Gaeavirus_42_3
			product	hypothetical protein
			transl_table	11
2240	1854	CDS
			locus_tag	Gaeavirus_42_4
			product	hypothetical protein
			transl_table	11
3315	2356	CDS
			locus_tag	Gaeavirus_42_5
			product	hypothetical protein
			transl_table	11
>Gaeavirus_43
3	2600	CDS
			locus_tag	Gaeavirus_43_1
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
>Gaeavirus_44
1	2397	CDS
			locus_tag	Gaeavirus_44_1
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
