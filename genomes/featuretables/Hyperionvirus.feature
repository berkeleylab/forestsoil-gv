>Hyperionvirus_1
1	600	CDS
			locus_tag	Hyperionvirus_1_1
			product	hypothetical protein
			transl_table	11
500	1924	CDS
			locus_tag	Hyperionvirus_1_2
			product	hypothetical protein
			transl_table	11
2489	1914	CDS
			locus_tag	Hyperionvirus_1_3
			product	hypothetical protein
			transl_table	11
2615	3847	CDS
			locus_tag	Hyperionvirus_1_4
			product	hypothetical protein
			transl_table	11
4288	3827	CDS
			locus_tag	Hyperionvirus_1_5
			product	hypothetical protein
			transl_table	11
5098	4316	CDS
			locus_tag	Hyperionvirus_1_6
			product	hypothetical protein
			transl_table	11
5225	6568	CDS
			locus_tag	Hyperionvirus_1_7
			product	hypothetical protein
			transl_table	11
7276	6674	CDS
			locus_tag	Hyperionvirus_1_8
			product	hypothetical protein
			transl_table	11
7505	7404	CDS
			locus_tag	Hyperionvirus_1_9
			product	hypothetical protein
			transl_table	11
8055	7483	CDS
			locus_tag	Hyperionvirus_1_10
			product	hypothetical protein
			transl_table	11
8750	8115	CDS
			locus_tag	Hyperionvirus_1_11
			product	hypothetical protein pv_1
			transl_table	11
8886	10061	CDS
			locus_tag	Hyperionvirus_1_12
			product	hypothetical protein
			transl_table	11
10114	11544	CDS
			locus_tag	Hyperionvirus_1_13
			product	chromosome condensation regulator
			transl_table	11
11544	13004	CDS
			locus_tag	Hyperionvirus_1_14
			product	chromosome condensation regulator, partial
			transl_table	11
13062	13766	CDS
			locus_tag	Hyperionvirus_1_15
			product	hypothetical protein
			transl_table	11
13842	14447	CDS
			locus_tag	Hyperionvirus_1_16
			product	hypothetical protein
			transl_table	11
15502	14444	CDS
			locus_tag	Hyperionvirus_1_17
			product	hypothetical protein CVU54_11675
			transl_table	11
15657	16742	CDS
			locus_tag	Hyperionvirus_1_18
			product	serine protease inhibitor 4, serpin-4
			transl_table	11
17384	16752	CDS
			locus_tag	Hyperionvirus_1_19
			product	hypothetical protein
			transl_table	11
18801	17446	CDS
			locus_tag	Hyperionvirus_1_20
			product	hypothetical protein AQUCO_03700025v1
			transl_table	11
19404	18880	CDS
			locus_tag	Hyperionvirus_1_21
			product	hypothetical protein
			transl_table	11
19497	20417	CDS
			locus_tag	Hyperionvirus_1_22
			product	hypothetical protein Catovirus_1_66
			transl_table	11
21345	20404	CDS
			locus_tag	Hyperionvirus_1_23
			product	tRNA:m(4)X modification enzyme TRM13 homolog
			transl_table	11
22261	21401	CDS
			locus_tag	Hyperionvirus_1_24
			product	hypothetical protein TSO221_01685
			transl_table	11
23003	22311	CDS
			locus_tag	Hyperionvirus_1_25
			product	SET domain protein
			transl_table	11
23113	23403	CDS
			locus_tag	Hyperionvirus_1_26
			product	hypothetical protein
			transl_table	11
24653	23400	CDS
			locus_tag	Hyperionvirus_1_27
			product	hypothetical protein
			transl_table	11
25722	25183	CDS
			locus_tag	Hyperionvirus_1_28
			product	hypothetical protein
			transl_table	11
25846	27030	CDS
			locus_tag	Hyperionvirus_1_29
			product	hypothetical protein
			transl_table	11
27078	27794	CDS
			locus_tag	Hyperionvirus_1_30
			product	PREDICTED: uncharacterized protein LOC105850536
			transl_table	11
27852	28541	CDS
			locus_tag	Hyperionvirus_1_31
			product	hypothetical protein
			transl_table	11
29770	28574	CDS
			locus_tag	Hyperionvirus_1_32
			product	hypothetical protein
			transl_table	11
29834	30028	CDS
			locus_tag	Hyperionvirus_1_33
			product	hypothetical protein
			transl_table	11
30818	30246	CDS
			locus_tag	Hyperionvirus_1_34
			product	hypothetical protein
			transl_table	11
32078	30873	CDS
			locus_tag	Hyperionvirus_1_35
			product	hypothetical protein GUITHDRAFT_145758
			transl_table	11
32284	33492	CDS
			locus_tag	Hyperionvirus_1_36
			product	hypothetical protein
			transl_table	11
33541	33993	CDS
			locus_tag	Hyperionvirus_1_37
			product	hypothetical protein
			transl_table	11
34468	34277	CDS
			locus_tag	Hyperionvirus_1_38
			product	hypothetical protein
			transl_table	11
35753	34596	CDS
			locus_tag	Hyperionvirus_1_39
			product	glycylpeptide N-tetradecanoyltransferase 2-like
			transl_table	11
36579	35779	CDS
			locus_tag	Hyperionvirus_1_40
			product	metallophosphatase
			transl_table	11
36756	37850	CDS
			locus_tag	Hyperionvirus_1_41
			product	serpin family protein
			transl_table	11
37904	39460	CDS
			locus_tag	Hyperionvirus_1_42
			product	hypothetical protein
			transl_table	11
40255	39467	CDS
			locus_tag	Hyperionvirus_1_43
			product	hypothetical protein
			transl_table	11
40407	40937	CDS
			locus_tag	Hyperionvirus_1_44
			product	hypothetical protein C5B50_12980
			transl_table	11
40999	41946	CDS
			locus_tag	Hyperionvirus_1_45
			product	hypothetical protein
			transl_table	11
42060	42950	CDS
			locus_tag	Hyperionvirus_1_46
			product	hypothetical protein Catovirus_1_66
			transl_table	11
43870	43196	CDS
			locus_tag	Hyperionvirus_1_47
			product	helix-hairpin-helix motif protein
			transl_table	11
44210	44299	CDS
			locus_tag	Hyperionvirus_1_48
			product	hypothetical protein
			transl_table	11
44350	44946	CDS
			locus_tag	Hyperionvirus_1_49
			product	hypothetical protein
			transl_table	11
45441	45950	CDS
			locus_tag	Hyperionvirus_1_50
			product	hypothetical protein
			transl_table	11
46009	47325	CDS
			locus_tag	Hyperionvirus_1_51
			product	hypothetical protein
			transl_table	11
47354	49945	CDS
			locus_tag	Hyperionvirus_1_52
			product	multicopper oxidase
			transl_table	11
50931	49948	CDS
			locus_tag	Hyperionvirus_1_53
			product	hypothetical protein COW78_12250
			transl_table	11
51869	50964	CDS
			locus_tag	Hyperionvirus_1_54
			product	hypothetical protein
			transl_table	11
53197	51950	CDS
			locus_tag	Hyperionvirus_1_55
			product	serpin family protein
			transl_table	11
53938	53261	CDS
			locus_tag	Hyperionvirus_1_56
			product	hypothetical protein
			transl_table	11
54209	55099	CDS
			locus_tag	Hyperionvirus_1_57
			product	hypothetical protein
			transl_table	11
55139	56716	CDS
			locus_tag	Hyperionvirus_1_58
			product	hypothetical protein
			transl_table	11
58270	56693	CDS
			locus_tag	Hyperionvirus_1_59
			product	hypothetical protein A2017_20125
			transl_table	11
58372	59043	CDS
			locus_tag	Hyperionvirus_1_60
			product	hypothetical protein M427DRAFT_63011
			transl_table	11
59119	60291	CDS
			locus_tag	Hyperionvirus_1_61
			product	hypothetical protein Klosneuvirus_2_275
			transl_table	11
60406	61266	CDS
			locus_tag	Hyperionvirus_1_62
			product	hypothetical protein
			transl_table	11
61306	62034	CDS
			locus_tag	Hyperionvirus_1_63
			product	hypothetical protein
			transl_table	11
62702	62040	CDS
			locus_tag	Hyperionvirus_1_64
			product	hypothetical protein
			transl_table	11
62851	63996	CDS
			locus_tag	Hyperionvirus_1_65
			product	glycogen debranching enzyme alpha-1,6-glucosidase
			transl_table	11
64391	64786	CDS
			locus_tag	Hyperionvirus_1_66
			product	hypothetical protein
			transl_table	11
64842	65432	CDS
			locus_tag	Hyperionvirus_1_67
			product	HD superfamily phosphohydrolase
			transl_table	11
65503	65973	CDS
			locus_tag	Hyperionvirus_1_68
			product	hypothetical protein
			transl_table	11
65961	66068	CDS
			locus_tag	Hyperionvirus_1_69
			product	hypothetical protein
			transl_table	11
66075	67145	CDS
			locus_tag	Hyperionvirus_1_70
			product	hypothetical protein
			transl_table	11
67902	67123	CDS
			locus_tag	Hyperionvirus_1_71
			product	hypothetical protein
			transl_table	11
68084	67974	CDS
			locus_tag	Hyperionvirus_1_72
			product	hypothetical protein
			transl_table	11
68344	71064	CDS
			locus_tag	Hyperionvirus_1_73
			product	hypothetical protein
			transl_table	11
72145	71900	CDS
			locus_tag	Hyperionvirus_1_74
			product	hypothetical protein
			transl_table	11
72216	72524	CDS
			locus_tag	Hyperionvirus_1_75
			product	hypothetical protein
			transl_table	11
72546	73958	CDS
			locus_tag	Hyperionvirus_1_76
			product	hypothetical protein
			transl_table	11
75748	73970	CDS
			locus_tag	Hyperionvirus_1_77
			product	Sel1 domain protein repeat-containing protein
			transl_table	11
76781	75798	CDS
			locus_tag	Hyperionvirus_1_78
			product	hypothetical protein
			transl_table	11
76865	77467	CDS
			locus_tag	Hyperionvirus_1_79
			product	hypothetical protein
			transl_table	11
77529	77876	CDS
			locus_tag	Hyperionvirus_1_80
			product	hypothetical protein
			transl_table	11
78591	77878	CDS
			locus_tag	Hyperionvirus_1_81
			product	hypothetical protein
			transl_table	11
78769	80481	CDS
			locus_tag	Hyperionvirus_1_82
			product	vacuolar protein sortingassociated protein 45, putative
			transl_table	11
82326	80482	CDS
			locus_tag	Hyperionvirus_1_83
			product	hypothetical protein
			transl_table	11
83094	82402	CDS
			locus_tag	Hyperionvirus_1_84
			product	hypothetical protein
			transl_table	11
83381	83175	CDS
			locus_tag	Hyperionvirus_1_85
			product	hypothetical protein
			transl_table	11
83899	84423	CDS
			locus_tag	Hyperionvirus_1_86
			product	AAA family ATPase
			transl_table	11
84451	85548	CDS
			locus_tag	Hyperionvirus_1_87
			product	glutamate 5-kinase
			transl_table	11
85624	86133	CDS
			locus_tag	Hyperionvirus_1_88
			product	hypothetical protein
			transl_table	11
86211	86708	CDS
			locus_tag	Hyperionvirus_1_89
			product	hypothetical protein
			transl_table	11
86817	87149	CDS
			locus_tag	Hyperionvirus_1_90
			product	hypothetical protein COU27_02685
			transl_table	11
87253	87447	CDS
			locus_tag	Hyperionvirus_1_91
			product	hypothetical protein
			transl_table	11
87448	87672	CDS
			locus_tag	Hyperionvirus_1_92
			product	hypothetical protein
			transl_table	11
89259	88201	CDS
			locus_tag	Hyperionvirus_1_93
			product	hypothetical protein
			transl_table	11
90034	89399	CDS
			locus_tag	Hyperionvirus_1_94
			product	hypothetical protein
			transl_table	11
90155	91351	CDS
			locus_tag	Hyperionvirus_1_95
			product	hypothetical protein
			transl_table	11
91419	92498	CDS
			locus_tag	Hyperionvirus_1_96
			product	hypothetical protein
			transl_table	11
93162	93740	CDS
			locus_tag	Hyperionvirus_1_97
			product	hypothetical protein
			transl_table	11
93774	94568	CDS
			locus_tag	Hyperionvirus_1_98
			product	hypothetical protein
			transl_table	11
95204	94548	CDS
			locus_tag	Hyperionvirus_1_99
			product	hypothetical protein
			transl_table	11
95416	96174	CDS
			locus_tag	Hyperionvirus_1_100
			product	hypothetical protein
			transl_table	11
96242	96847	CDS
			locus_tag	Hyperionvirus_1_101
			product	hypothetical protein
			transl_table	11
97354	96857	CDS
			locus_tag	Hyperionvirus_1_102
			product	hypothetical protein PPL_09843
			transl_table	11
97358	98899	CDS
			locus_tag	Hyperionvirus_1_103
			product	chromosome condensation regulator RCC1
			transl_table	11
99194	98880	CDS
			locus_tag	Hyperionvirus_1_104
			product	hypothetical protein
			transl_table	11
99317	99973	CDS
			locus_tag	Hyperionvirus_1_105
			product	hypothetical protein
			transl_table	11
100037	100240	CDS
			locus_tag	Hyperionvirus_1_106
			product	hypothetical protein
			transl_table	11
100304	101167	CDS
			locus_tag	Hyperionvirus_1_107
			product	serine/arginine-rich splicing factor 4-like isoform X1
			transl_table	11
101249	101383	CDS
			locus_tag	Hyperionvirus_1_108
			product	hypothetical protein
			transl_table	11
102633	101497	CDS
			locus_tag	Hyperionvirus_1_109
			product	hypothetical protein
			transl_table	11
102869	104170	CDS
			locus_tag	Hyperionvirus_1_110
			product	hypothetical protein
			transl_table	11
104257	104835	CDS
			locus_tag	Hyperionvirus_1_111
			product	hypothetical protein
			transl_table	11
105914	105084	CDS
			locus_tag	Hyperionvirus_1_112
			product	hypothetical protein
			transl_table	11
106039	106683	CDS
			locus_tag	Hyperionvirus_1_113
			product	hypothetical protein
			transl_table	11
107127	106864	CDS
			locus_tag	Hyperionvirus_1_114
			product	hypothetical protein
			transl_table	11
107940	107353	CDS
			locus_tag	Hyperionvirus_1_115
			product	hypothetical protein
			transl_table	11
108279	108031	CDS
			locus_tag	Hyperionvirus_1_116
			product	hypothetical protein
			transl_table	11
108407	108291	CDS
			locus_tag	Hyperionvirus_1_117
			product	hypothetical protein
			transl_table	11
108927	108454	CDS
			locus_tag	Hyperionvirus_1_118
			product	hypothetical protein
			transl_table	11
110389	108968	CDS
			locus_tag	Hyperionvirus_1_119
			product	chromosome condensation regulator
			transl_table	11
112378	110438	CDS
			locus_tag	Hyperionvirus_1_120
			product	glucose-methanol-choline oxidoreductase
			transl_table	11
113315	112380	CDS
			locus_tag	Hyperionvirus_1_121
			product	SDR family NAD(P)-dependent oxidoreductase
			transl_table	11
114786	113365	CDS
			locus_tag	Hyperionvirus_1_122
			product	chromosome condensation regulator, partial
			transl_table	11
116361	114844	CDS
			locus_tag	Hyperionvirus_1_123
			product	hypothetical protein
			transl_table	11
116936	116412	CDS
			locus_tag	Hyperionvirus_1_124
			product	hypothetical protein
			transl_table	11
118362	117025	CDS
			locus_tag	Hyperionvirus_1_125
			product	hypothetical protein FisN_20Hh160
			transl_table	11
118447	119592	CDS
			locus_tag	Hyperionvirus_1_126
			product	hypothetical protein
			transl_table	11
120866	119559	CDS
			locus_tag	Hyperionvirus_1_127
			product	chromosome condensation regulator, partial
			transl_table	11
120951	121538	CDS
			locus_tag	Hyperionvirus_1_128
			product	hypothetical protein
			transl_table	11
121588	123876	CDS
			locus_tag	Hyperionvirus_1_129
			product	hypothetical protein
			transl_table	11
124697	123882	CDS
			locus_tag	Hyperionvirus_1_130
			product	hypothetical protein
			transl_table	11
124816	124974	CDS
			locus_tag	Hyperionvirus_1_131
			product	hypothetical protein
			transl_table	11
125038	125739	CDS
			locus_tag	Hyperionvirus_1_132
			product	expressed protein
			transl_table	11
125788	127047	CDS
			locus_tag	Hyperionvirus_1_133
			product	chromosome condensation regulator
			transl_table	11
127607	127050	CDS
			locus_tag	Hyperionvirus_1_134
			product	nicotinamide mononucleotide transporter-domain-containing protein, partial
			transl_table	11
128113	128967	CDS
			locus_tag	Hyperionvirus_1_135
			product	hypothetical protein
			transl_table	11
129021	130256	CDS
			locus_tag	Hyperionvirus_1_136
			product	hypothetical protein
			transl_table	11
130612	130520	CDS
			locus_tag	Hyperionvirus_1_137
			product	hypothetical protein
			transl_table	11
130948	130766	CDS
			locus_tag	Hyperionvirus_1_138
			product	hypothetical protein
			transl_table	11
131233	131141	CDS
			locus_tag	Hyperionvirus_1_139
			product	hypothetical protein
			transl_table	11
131989	133191	CDS
			locus_tag	Hyperionvirus_1_140
			product	hypothetical protein
			transl_table	11
133576	133175	CDS
			locus_tag	Hyperionvirus_1_141
			product	hypothetical protein
			transl_table	11
133603	133728	CDS
			locus_tag	Hyperionvirus_1_142
			product	hypothetical protein
			transl_table	11
134351	133740	CDS
			locus_tag	Hyperionvirus_1_143
			product	hypothetical protein
			transl_table	11
134508	135404	CDS
			locus_tag	Hyperionvirus_1_144
			product	hypothetical protein
			transl_table	11
136101	135397	CDS
			locus_tag	Hyperionvirus_1_145
			product	hypothetical protein
			transl_table	11
136225	136668	CDS
			locus_tag	Hyperionvirus_1_146
			product	hypothetical protein
			transl_table	11
136754	137275	CDS
			locus_tag	Hyperionvirus_1_147
			product	hypothetical protein
			transl_table	11
138536	137280	CDS
			locus_tag	Hyperionvirus_1_148
			product	amino oxidase family protein
			transl_table	11
138561	139772	CDS
			locus_tag	Hyperionvirus_1_149
			product	hypothetical protein
			transl_table	11
139819	141051	CDS
			locus_tag	Hyperionvirus_1_150
			product	chromosome condensation regulator
			transl_table	11
142488	141058	CDS
			locus_tag	Hyperionvirus_1_151
			product	hypothetical protein
			transl_table	11
142566	143291	CDS
			locus_tag	Hyperionvirus_1_152
			product	hypothetical protein
			transl_table	11
143386	145053	CDS
			locus_tag	Hyperionvirus_1_153
			product	hypothetical protein
			transl_table	11
145506	145375	CDS
			locus_tag	Hyperionvirus_1_154
			product	hypothetical protein
			transl_table	11
145737	145519	CDS
			locus_tag	Hyperionvirus_1_155
			product	hypothetical protein
			transl_table	11
145849	146580	CDS
			locus_tag	Hyperionvirus_1_156
			product	hypothetical protein
			transl_table	11
147253	146561	CDS
			locus_tag	Hyperionvirus_1_157
			product	hypothetical protein
			transl_table	11
147692	147294	CDS
			locus_tag	Hyperionvirus_1_158
			product	hypothetical protein
			transl_table	11
148422	148589	CDS
			locus_tag	Hyperionvirus_1_159
			product	hypothetical protein
			transl_table	11
148593	148955	CDS
			locus_tag	Hyperionvirus_1_160
			product	hypothetical protein
			transl_table	11
149888	149142	CDS
			locus_tag	Hyperionvirus_1_161
			product	hypothetical protein
			transl_table	11
150562	150005	CDS
			locus_tag	Hyperionvirus_1_162
			product	hypothetical protein
			transl_table	11
150700	151992	CDS
			locus_tag	Hyperionvirus_1_163
			product	chromosome condensation regulator
			transl_table	11
152542	152003	CDS
			locus_tag	Hyperionvirus_1_164
			product	hypothetical protein
			transl_table	11
152690	153178	CDS
			locus_tag	Hyperionvirus_1_165
			product	hypothetical protein
			transl_table	11
153244	153849	CDS
			locus_tag	Hyperionvirus_1_166
			product	hypothetical protein
			transl_table	11
153894	154553	CDS
			locus_tag	Hyperionvirus_1_167
			product	hypothetical protein
			transl_table	11
154623	155630	CDS
			locus_tag	Hyperionvirus_1_168
			product	hypothetical protein
			transl_table	11
156145	155645	CDS
			locus_tag	Hyperionvirus_1_169
			product	hypothetical protein
			transl_table	11
156291	157511	CDS
			locus_tag	Hyperionvirus_1_170
			product	chromosome condensation regulator, partial
			transl_table	11
157547	158869	CDS
			locus_tag	Hyperionvirus_1_171
			product	hypothetical protein
			transl_table	11
158908	159708	CDS
			locus_tag	Hyperionvirus_1_172
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
159938	160033	CDS
			locus_tag	Hyperionvirus_1_173
			product	hypothetical protein
			transl_table	11
160323	160988	CDS
			locus_tag	Hyperionvirus_1_174
			product	hypothetical protein
			transl_table	11
161047	162003	CDS
			locus_tag	Hyperionvirus_1_175
			product	hypothetical protein
			transl_table	11
163540	161987	CDS
			locus_tag	Hyperionvirus_1_176
			product	chromosome condensation regulator
			transl_table	11
163680	164414	CDS
			locus_tag	Hyperionvirus_1_177
			product	hypothetical protein
			transl_table	11
165489	164428	CDS
			locus_tag	Hyperionvirus_1_178
			product	hypothetical protein
			transl_table	11
165798	165595	CDS
			locus_tag	Hyperionvirus_1_179
			product	hypothetical protein
			transl_table	11
166635	165910	CDS
			locus_tag	Hyperionvirus_1_180
			product	hypothetical protein
			transl_table	11
167002	166766	CDS
			locus_tag	Hyperionvirus_1_181
			product	hypothetical protein
			transl_table	11
167001	168056	CDS
			locus_tag	Hyperionvirus_1_182
			product	hypothetical protein
			transl_table	11
168117	169526	CDS
			locus_tag	Hyperionvirus_1_183
			product	hypothetical protein
			transl_table	11
169671	169528	CDS
			locus_tag	Hyperionvirus_1_184
			product	hypothetical protein
			transl_table	11
169683	170171	CDS
			locus_tag	Hyperionvirus_1_185
			product	hypothetical protein Klosneuvirus_2_292
			transl_table	11
170939	170181	CDS
			locus_tag	Hyperionvirus_1_186
			product	Hypothetical protein ORPV_249
			transl_table	11
171081	171773	CDS
			locus_tag	Hyperionvirus_1_187
			product	hypothetical protein
			transl_table	11
172232	172921	CDS
			locus_tag	Hyperionvirus_1_188
			product	hypothetical protein
			transl_table	11
173162	173025	CDS
			locus_tag	Hyperionvirus_1_189
			product	hypothetical protein
			transl_table	11
173307	174227	CDS
			locus_tag	Hyperionvirus_1_190
			product	hypothetical protein
			transl_table	11
174747	174229	CDS
			locus_tag	Hyperionvirus_1_191
			product	hypothetical protein
			transl_table	11
174899	175411	CDS
			locus_tag	Hyperionvirus_1_192
			product	hypothetical protein
			transl_table	11
175622	176215	CDS
			locus_tag	Hyperionvirus_1_193
			product	hypothetical protein
			transl_table	11
176703	176212	CDS
			locus_tag	Hyperionvirus_1_194
			product	hypothetical protein
			transl_table	11
179483	176772	CDS
			locus_tag	Hyperionvirus_1_195
			product	ankyrin repeat-containing protein
			transl_table	11
180001	179672	CDS
			locus_tag	Hyperionvirus_1_196
			product	hypothetical protein
			transl_table	11
180587	179982	CDS
			locus_tag	Hyperionvirus_1_197
			product	hypothetical protein
			transl_table	11
181983	180643	CDS
			locus_tag	Hyperionvirus_1_198
			product	hypothetical protein
			transl_table	11
182127	182732	CDS
			locus_tag	Hyperionvirus_1_199
			product	hypothetical protein
			transl_table	11
182762	182992	CDS
			locus_tag	Hyperionvirus_1_200
			product	hypothetical protein
			transl_table	11
184423	182999	CDS
			locus_tag	Hyperionvirus_1_201
			product	hypothetical protein
			transl_table	11
186038	184473	CDS
			locus_tag	Hyperionvirus_1_202
			product	hypothetical protein
			transl_table	11
186695	186129	CDS
			locus_tag	Hyperionvirus_1_203
			product	hypothetical protein
			transl_table	11
186724	187734	CDS
			locus_tag	Hyperionvirus_1_204
			product	hypothetical protein
			transl_table	11
187802	188512	CDS
			locus_tag	Hyperionvirus_1_205
			product	hypothetical protein
			transl_table	11
189254	188499	CDS
			locus_tag	Hyperionvirus_1_206
			product	hypothetical protein
			transl_table	11
189297	190184	CDS
			locus_tag	Hyperionvirus_1_207
			product	hypothetical protein
			transl_table	11
190702	190190	CDS
			locus_tag	Hyperionvirus_1_208
			product	hypothetical protein
			transl_table	11
190838	192067	CDS
			locus_tag	Hyperionvirus_1_209
			product	chromosome condensation regulator
			transl_table	11
193475	192069	CDS
			locus_tag	Hyperionvirus_1_210
			product	hypothetical protein
			transl_table	11
193893	194069	CDS
			locus_tag	Hyperionvirus_1_211
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_2
395	3	CDS
			locus_tag	Hyperionvirus_2_1
			product	fad/fmn-containing dehydrogenase
			transl_table	11
1197	421	CDS
			locus_tag	Hyperionvirus_2_2
			product	metallophosphatase
			transl_table	11
1319	2341	CDS
			locus_tag	Hyperionvirus_2_3
			product	hypothetical protein
			transl_table	11
3011	2319	CDS
			locus_tag	Hyperionvirus_2_4
			product	hypothetical protein
			transl_table	11
3143	3322	CDS
			locus_tag	Hyperionvirus_2_5
			product	hypothetical protein
			transl_table	11
3540	3352	CDS
			locus_tag	Hyperionvirus_2_6
			product	hypothetical protein
			transl_table	11
3738	3544	CDS
			locus_tag	Hyperionvirus_2_7
			product	hypothetical protein
			transl_table	11
5162	4041	CDS
			locus_tag	Hyperionvirus_2_8
			product	hypothetical protein
			transl_table	11
5684	5217	CDS
			locus_tag	Hyperionvirus_2_9
			product	hypothetical protein O9G_002264
			transl_table	11
5788	6522	CDS
			locus_tag	Hyperionvirus_2_10
			product	hypothetical protein
			transl_table	11
6603	7433	CDS
			locus_tag	Hyperionvirus_2_11
			product	hypothetical protein
			transl_table	11
7597	7430	CDS
			locus_tag	Hyperionvirus_2_12
			product	hypothetical protein
			transl_table	11
8166	7624	CDS
			locus_tag	Hyperionvirus_2_13
			product	hypothetical protein
			transl_table	11
9333	8212	CDS
			locus_tag	Hyperionvirus_2_14
			product	hypothetical protein
			transl_table	11
9434	9622	CDS
			locus_tag	Hyperionvirus_2_15
			product	hypothetical protein CHLNCDRAFT_142204
			transl_table	11
9674	11356	CDS
			locus_tag	Hyperionvirus_2_16
			product	hypothetical protein
			transl_table	11
12417	11329	CDS
			locus_tag	Hyperionvirus_2_17
			product	putative VV A32-like packaging ATPase
			transl_table	11
12744	13223	CDS
			locus_tag	Hyperionvirus_2_18
			product	hypothetical protein
			transl_table	11
13308	15752	CDS
			locus_tag	Hyperionvirus_2_19
			product	hypothetical protein
			transl_table	11
15818	16657	CDS
			locus_tag	Hyperionvirus_2_20
			product	Alginate lyase
			transl_table	11
16851	17117	CDS
			locus_tag	Hyperionvirus_2_21
			product	hypothetical protein
			transl_table	11
17213	17106	CDS
			locus_tag	Hyperionvirus_2_22
			product	hypothetical protein
			transl_table	11
17781	17653	CDS
			locus_tag	Hyperionvirus_2_23
			product	hypothetical protein
			transl_table	11
18222	18112	CDS
			locus_tag	Hyperionvirus_2_24
			product	hypothetical protein
			transl_table	11
18646	18542	CDS
			locus_tag	Hyperionvirus_2_25
			product	hypothetical protein
			transl_table	11
19120	18917	CDS
			locus_tag	Hyperionvirus_2_26
			product	hypothetical protein
			transl_table	11
20121	19204	CDS
			locus_tag	Hyperionvirus_2_27
			product	hypothetical protein PBRA_003026
			transl_table	11
20206	21279	CDS
			locus_tag	Hyperionvirus_2_28
			product	cupin domain-containing protein
			transl_table	11
21563	21276	CDS
			locus_tag	Hyperionvirus_2_29
			product	hypothetical protein
			transl_table	11
23883	21517	CDS
			locus_tag	Hyperionvirus_2_30
			product	bifunctional AAA family ATPase chaperone/translocase BCS1
			transl_table	11
24245	26446	CDS
			locus_tag	Hyperionvirus_2_31
			product	putative lanosterol 14-alpha demethylase
			transl_table	11
26500	27192	CDS
			locus_tag	Hyperionvirus_2_32
			product	carbonic anhydrase family protein
			transl_table	11
27249	28040	CDS
			locus_tag	Hyperionvirus_2_33
			product	hypothetical protein
			transl_table	11
28110	29417	CDS
			locus_tag	Hyperionvirus_2_34
			product	hypothetical protein
			transl_table	11
29435	30166	CDS
			locus_tag	Hyperionvirus_2_35
			product	hypothetical protein
			transl_table	11
30216	31319	CDS
			locus_tag	Hyperionvirus_2_36
			product	hypothetical protein
			transl_table	11
31383	32441	CDS
			locus_tag	Hyperionvirus_2_37
			product	hypothetical protein
			transl_table	11
32487	33440	CDS
			locus_tag	Hyperionvirus_2_38
			product	hypothetical protein
			transl_table	11
33495	34859	CDS
			locus_tag	Hyperionvirus_2_39
			product	hypothetical protein
			transl_table	11
34919	35404	CDS
			locus_tag	Hyperionvirus_2_40
			product	hypothetical protein
			transl_table	11
35469	36641	CDS
			locus_tag	Hyperionvirus_2_41
			product	hypothetical protein
			transl_table	11
36668	37456	CDS
			locus_tag	Hyperionvirus_2_42
			product	putative glycosyltransferase
			transl_table	11
38339	37596	CDS
			locus_tag	Hyperionvirus_2_43
			product	hypothetical protein
			transl_table	11
39132	38326	CDS
			locus_tag	Hyperionvirus_2_44
			product	glycosyltransferase
			transl_table	11
39524	39171	CDS
			locus_tag	Hyperionvirus_2_45
			product	hypothetical protein
			transl_table	11
39665	40981	CDS
			locus_tag	Hyperionvirus_2_46
			product	hypothetical protein
			transl_table	11
42378	40942	CDS
			locus_tag	Hyperionvirus_2_47
			product	hypothetical protein
			transl_table	11
42513	43808	CDS
			locus_tag	Hyperionvirus_2_48
			product	hypothetical protein
			transl_table	11
43869	45263	CDS
			locus_tag	Hyperionvirus_2_49
			product	hypothetical protein
			transl_table	11
45931	45296	CDS
			locus_tag	Hyperionvirus_2_50
			product	uncharacterized PKHD-type hydroxylase At1g22950-like
			transl_table	11
46077	46562	CDS
			locus_tag	Hyperionvirus_2_51
			product	hypothetical protein
			transl_table	11
46667	47914	CDS
			locus_tag	Hyperionvirus_2_52
			product	hypothetical protein
			transl_table	11
48198	48055	CDS
			locus_tag	Hyperionvirus_2_53
			product	hypothetical protein
			transl_table	11
49462	48251	CDS
			locus_tag	Hyperionvirus_2_54
			product	hypothetical protein
			transl_table	11
49803	50171	CDS
			locus_tag	Hyperionvirus_2_55
			product	hypothetical protein
			transl_table	11
51134	50166	CDS
			locus_tag	Hyperionvirus_2_56
			product	hypothetical protein
			transl_table	11
51288	51103	CDS
			locus_tag	Hyperionvirus_2_57
			product	hypothetical protein
			transl_table	11
51943	51347	CDS
			locus_tag	Hyperionvirus_2_58
			product	hypothetical protein
			transl_table	11
52408	51998	CDS
			locus_tag	Hyperionvirus_2_59
			product	hypothetical protein
			transl_table	11
52900	52460	CDS
			locus_tag	Hyperionvirus_2_60
			product	hypothetical protein
			transl_table	11
53038	53508	CDS
			locus_tag	Hyperionvirus_2_61
			product	hypothetical protein
			transl_table	11
53558	54799	CDS
			locus_tag	Hyperionvirus_2_62
			product	chromosome condensation regulator
			transl_table	11
54829	55107	CDS
			locus_tag	Hyperionvirus_2_63
			product	hypothetical protein
			transl_table	11
55107	56114	CDS
			locus_tag	Hyperionvirus_2_64
			product	chromosome condensation regulator
			transl_table	11
56147	58213	CDS
			locus_tag	Hyperionvirus_2_65
			product	hypothetical protein D5b_00344
			transl_table	11
58260	59708	CDS
			locus_tag	Hyperionvirus_2_66
			product	chromosome condensation regulator
			transl_table	11
59766	61028	CDS
			locus_tag	Hyperionvirus_2_67
			product	chromosome condensation regulator, partial
			transl_table	11
62064	61012	CDS
			locus_tag	Hyperionvirus_2_68
			product	RNA ligase
			transl_table	11
62508	62618	CDS
			locus_tag	Hyperionvirus_2_69
			product	hypothetical protein
			transl_table	11
62683	63831	CDS
			locus_tag	Hyperionvirus_2_70
			product	hypothetical protein
			transl_table	11
63831	64412	CDS
			locus_tag	Hyperionvirus_2_71
			product	hypothetical protein
			transl_table	11
65721	64390	CDS
			locus_tag	Hyperionvirus_2_72
			product	DnaJ-like subfamily C member 3
			transl_table	11
65845	66384	CDS
			locus_tag	Hyperionvirus_2_73
			product	hypothetical protein
			transl_table	11
67578	66439	CDS
			locus_tag	Hyperionvirus_2_74
			product	hypothetical protein
			transl_table	11
67705	68847	CDS
			locus_tag	Hyperionvirus_2_75
			product	hypothetical protein
			transl_table	11
69620	69450	CDS
			locus_tag	Hyperionvirus_2_76
			product	hypothetical protein
			transl_table	11
69815	69696	CDS
			locus_tag	Hyperionvirus_2_77
			product	hypothetical protein
			transl_table	11
69873	70226	CDS
			locus_tag	Hyperionvirus_2_78
			product	mannose-1-phosphate guanyltransferase
			transl_table	11
70280	70945	CDS
			locus_tag	Hyperionvirus_2_79
			product	hypothetical protein
			transl_table	11
71026	72162	CDS
			locus_tag	Hyperionvirus_2_80
			product	hypothetical protein, partial
			transl_table	11
72183	72983	CDS
			locus_tag	Hyperionvirus_2_81
			product	putative RNA methylase
			transl_table	11
74102	73035	CDS
			locus_tag	Hyperionvirus_2_82
			product	hypothetical protein
			transl_table	11
74248	75432	CDS
			locus_tag	Hyperionvirus_2_83
			product	hypothetical protein
			transl_table	11
75962	75744	CDS
			locus_tag	Hyperionvirus_2_84
			product	hypothetical protein
			transl_table	11
76009	76245	CDS
			locus_tag	Hyperionvirus_2_85
			product	hypothetical protein
			transl_table	11
77218	76229	CDS
			locus_tag	Hyperionvirus_2_86
			product	hypothetical protein
			transl_table	11
77296	78228	CDS
			locus_tag	Hyperionvirus_2_87
			product	chromosome condensation regulator
			transl_table	11
78324	78671	CDS
			locus_tag	Hyperionvirus_2_88
			product	regulator of chromosome condensation, RCC1
			transl_table	11
78716	80080	CDS
			locus_tag	Hyperionvirus_2_89
			product	hypothetical protein
			transl_table	11
80176	80060	CDS
			locus_tag	Hyperionvirus_2_90
			product	hypothetical protein
			transl_table	11
81757	80279	CDS
			locus_tag	Hyperionvirus_2_91
			product	hypothetical protein
			transl_table	11
81882	82745	CDS
			locus_tag	Hyperionvirus_2_92
			product	hypothetical protein
			transl_table	11
83118	82693	CDS
			locus_tag	Hyperionvirus_2_93
			product	hypothetical protein
			transl_table	11
84011	83184	CDS
			locus_tag	Hyperionvirus_2_94
			product	hypothetical protein
			transl_table	11
84720	84403	CDS
			locus_tag	Hyperionvirus_2_95
			product	hypothetical protein
			transl_table	11
85002	84745	CDS
			locus_tag	Hyperionvirus_2_96
			product	hypothetical protein
			transl_table	11
85109	85945	CDS
			locus_tag	Hyperionvirus_2_97
			product	hypothetical protein
			transl_table	11
86008	87756	CDS
			locus_tag	Hyperionvirus_2_98
			product	tryptophan--tRNA cytoplasmic
			transl_table	11
88348	87761	CDS
			locus_tag	Hyperionvirus_2_99
			product	hypothetical protein
			transl_table	11
88367	89497	CDS
			locus_tag	Hyperionvirus_2_100
			product	hypothetical protein
			transl_table	11
89531	90124	CDS
			locus_tag	Hyperionvirus_2_101
			product	hypothetical protein
			transl_table	11
90175	91464	CDS
			locus_tag	Hyperionvirus_2_102
			product	hypothetical protein
			transl_table	11
92819	91461	CDS
			locus_tag	Hyperionvirus_2_103
			product	hypothetical protein
			transl_table	11
94058	92862	CDS
			locus_tag	Hyperionvirus_2_104
			product	glutamate-5-semialdehyde dehydrogenase
			transl_table	11
94821	94087	CDS
			locus_tag	Hyperionvirus_2_105
			product	hypothetical protein Catovirus_1_263
			transl_table	11
94891	95544	CDS
			locus_tag	Hyperionvirus_2_106
			product	hypothetical protein
			transl_table	11
95572	96144	CDS
			locus_tag	Hyperionvirus_2_107
			product	hypothetical protein
			transl_table	11
96209	96715	CDS
			locus_tag	Hyperionvirus_2_108
			product	hypothetical protein
			transl_table	11
96740	97915	CDS
			locus_tag	Hyperionvirus_2_109
			product	hypothetical protein
			transl_table	11
97957	99114	CDS
			locus_tag	Hyperionvirus_2_110
			product	hypothetical protein
			transl_table	11
99162	99809	CDS
			locus_tag	Hyperionvirus_2_111
			product	hypothetical protein
			transl_table	11
100193	99831	CDS
			locus_tag	Hyperionvirus_2_112
			product	hypothetical protein
			transl_table	11
100326	101171	CDS
			locus_tag	Hyperionvirus_2_113
			product	hypothetical protein
			transl_table	11
101216	101695	CDS
			locus_tag	Hyperionvirus_2_114
			product	hypothetical protein
			transl_table	11
101745	103172	CDS
			locus_tag	Hyperionvirus_2_115
			product	chromosome condensation regulator, partial
			transl_table	11
103896	103162	CDS
			locus_tag	Hyperionvirus_2_116
			product	hypothetical protein
			transl_table	11
104725	103934	CDS
			locus_tag	Hyperionvirus_2_117
			product	hypothetical protein
			transl_table	11
105644	104865	CDS
			locus_tag	Hyperionvirus_2_118
			product	hypothetical protein
			transl_table	11
106490	105699	CDS
			locus_tag	Hyperionvirus_2_119
			product	hypothetical protein
			transl_table	11
107022	106747	CDS
			locus_tag	Hyperionvirus_2_120
			product	hypothetical protein
			transl_table	11
107167	107033	CDS
			locus_tag	Hyperionvirus_2_121
			product	hypothetical protein
			transl_table	11
107154	108299	CDS
			locus_tag	Hyperionvirus_2_122
			product	hypothetical protein
			transl_table	11
109414	108587	CDS
			locus_tag	Hyperionvirus_2_123
			product	hypothetical protein
			transl_table	11
110528	109674	CDS
			locus_tag	Hyperionvirus_2_124
			product	hypothetical protein
			transl_table	11
111592	110573	CDS
			locus_tag	Hyperionvirus_2_125
			product	PREDICTED: O-glucosyltransferase rumi homolog, partial
			transl_table	11
111648	112655	CDS
			locus_tag	Hyperionvirus_2_126
			product	Phosphorylated CTD interacting factor 1, WW domain
			transl_table	11
112609	113883	CDS
			locus_tag	Hyperionvirus_2_127
			product	hypothetical protein
			transl_table	11
113902	114900	CDS
			locus_tag	Hyperionvirus_2_128
			product	regulator of chromosome condensation (RCC1) repeat family protein
			transl_table	11
114959	116068	CDS
			locus_tag	Hyperionvirus_2_129
			product	hypothetical protein
			transl_table	11
116189	116929	CDS
			locus_tag	Hyperionvirus_2_130
			product	hypothetical protein
			transl_table	11
116994	118190	CDS
			locus_tag	Hyperionvirus_2_131
			product	hypothetical protein
			transl_table	11
118302	118406	CDS
			locus_tag	Hyperionvirus_2_132
			product	hypothetical protein
			transl_table	11
118391	118582	CDS
			locus_tag	Hyperionvirus_2_133
			product	hypothetical protein
			transl_table	11
119151	118609	CDS
			locus_tag	Hyperionvirus_2_134
			product	hypothetical protein
			transl_table	11
119629	119207	CDS
			locus_tag	Hyperionvirus_2_135
			product	hypothetical protein
			transl_table	11
120609	119719	CDS
			locus_tag	Hyperionvirus_2_136
			product	hypothetical protein
			transl_table	11
120676	121008	CDS
			locus_tag	Hyperionvirus_2_137
			product	hypothetical protein
			transl_table	11
121059	121484	CDS
			locus_tag	Hyperionvirus_2_138
			product	hypothetical protein
			transl_table	11
121531	122496	CDS
			locus_tag	Hyperionvirus_2_139
			product	chromosome condensation regulator, partial
			transl_table	11
122707	122591	CDS
			locus_tag	Hyperionvirus_2_140
			product	hypothetical protein
			transl_table	11
122718	124292	CDS
			locus_tag	Hyperionvirus_2_141
			product	RecName: Full=Probable ATP-dependent transporter ycf16 >CAA44985.1 ORF2
			transl_table	11
124337	125053	CDS
			locus_tag	Hyperionvirus_2_142
			product	hypothetical protein
			transl_table	11
125134	125244	CDS
			locus_tag	Hyperionvirus_2_143
			product	hypothetical protein
			transl_table	11
125257	125373	CDS
			locus_tag	Hyperionvirus_2_144
			product	hypothetical protein
			transl_table	11
125363	126442	CDS
			locus_tag	Hyperionvirus_2_145
			product	hypothetical protein
			transl_table	11
126933	126430	CDS
			locus_tag	Hyperionvirus_2_146
			product	hypothetical protein RvY_13488
			transl_table	11
127055	128503	CDS
			locus_tag	Hyperionvirus_2_147
			product	chromosome condensation regulator, partial
			transl_table	11
128555	129313	CDS
			locus_tag	Hyperionvirus_2_148
			product	hypothetical protein
			transl_table	11
129371	130777	CDS
			locus_tag	Hyperionvirus_2_149
			product	chromosome condensation regulator
			transl_table	11
130845	131384	CDS
			locus_tag	Hyperionvirus_2_150
			product	hypothetical protein
			transl_table	11
132628	131363	CDS
			locus_tag	Hyperionvirus_2_151
			product	hypothetical protein
			transl_table	11
133353	132676	CDS
			locus_tag	Hyperionvirus_2_152
			product	hypothetical protein Klosneuvirus_6_75
			transl_table	11
133641	133477	CDS
			locus_tag	Hyperionvirus_2_153
			product	hypothetical protein
			transl_table	11
133987	134265	CDS
			locus_tag	Hyperionvirus_2_154
			product	protein kinase
			transl_table	11
134705	134271	CDS
			locus_tag	Hyperionvirus_2_155
			product	hypothetical protein
			transl_table	11
135309	134989	CDS
			locus_tag	Hyperionvirus_2_156
			product	hypothetical protein
			transl_table	11
135512	136213	CDS
			locus_tag	Hyperionvirus_2_157
			product	hypothetical protein
			transl_table	11
136279	137535	CDS
			locus_tag	Hyperionvirus_2_158
			product	PREDICTED: TNF receptor-associated factor 4-like
			transl_table	11
138331	137537	CDS
			locus_tag	Hyperionvirus_2_159
			product	hypothetical protein
			transl_table	11
138839	138549	CDS
			locus_tag	Hyperionvirus_2_160
			product	hypothetical protein
			transl_table	11
139629	139045	CDS
			locus_tag	Hyperionvirus_2_161
			product	hypothetical protein
			transl_table	11
140074	139691	CDS
			locus_tag	Hyperionvirus_2_162
			product	hypothetical protein
			transl_table	11
140258	140082	CDS
			locus_tag	Hyperionvirus_2_163
			product	hypothetical protein
			transl_table	11
140335	141609	CDS
			locus_tag	Hyperionvirus_2_164
			product	chromosome condensation regulator, partial
			transl_table	11
141645	142367	CDS
			locus_tag	Hyperionvirus_2_165
			product	hypothetical protein
			transl_table	11
142737	142351	CDS
			locus_tag	Hyperionvirus_2_166
			product	ASCH domain-containing protein
			transl_table	11
144520	142820	CDS
			locus_tag	Hyperionvirus_2_167
			product	PREDICTED: TNF receptor-associated factor 4 isoform X1
			transl_table	11
144650	146032	CDS
			locus_tag	Hyperionvirus_2_168
			product	Arylamine N-acetyltransferase
			transl_table	11
146029	146391	CDS
			locus_tag	Hyperionvirus_2_169
			product	hypothetical protein mv_L995
			transl_table	11
146843	146388	CDS
			locus_tag	Hyperionvirus_2_170
			product	hypothetical protein
			transl_table	11
147393	146863	CDS
			locus_tag	Hyperionvirus_2_171
			product	hypothetical protein
			transl_table	11
148706	147447	CDS
			locus_tag	Hyperionvirus_2_172
			product	hypothetical protein
			transl_table	11
149555	148782	CDS
			locus_tag	Hyperionvirus_2_173
			product	hypothetical protein
			transl_table	11
150301	149558	CDS
			locus_tag	Hyperionvirus_2_174
			product	hypothetical protein M896_041060
			transl_table	11
150436	151785	CDS
			locus_tag	Hyperionvirus_2_175
			product	hypothetical protein K443DRAFT_683561
			transl_table	11
151847	153124	CDS
			locus_tag	Hyperionvirus_2_176
			product	hypothetical protein
			transl_table	11
153206	154030	CDS
			locus_tag	Hyperionvirus_2_177
			product	hypothetical protein
			transl_table	11
154111	154395	CDS
			locus_tag	Hyperionvirus_2_178
			product	hypothetical protein
			transl_table	11
154959	154768	CDS
			locus_tag	Hyperionvirus_2_179
			product	hypothetical protein
			transl_table	11
155155	155517	CDS
			locus_tag	Hyperionvirus_2_180
			product	hypothetical protein
			transl_table	11
155974	155849	CDS
			locus_tag	Hyperionvirus_2_181
			product	hypothetical protein
			transl_table	11
155973	156281	CDS
			locus_tag	Hyperionvirus_2_182
			product	hypothetical protein
			transl_table	11
157274	156276	CDS
			locus_tag	Hyperionvirus_2_183
			product	hypothetical protein
			transl_table	11
157412	157615	CDS
			locus_tag	Hyperionvirus_2_184
			product	hypothetical protein
			transl_table	11
158381	157638	CDS
			locus_tag	Hyperionvirus_2_185
			product	hypothetical protein
			transl_table	11
159357	158371	CDS
			locus_tag	Hyperionvirus_2_186
			product	hypothetical protein
			transl_table	11
160522	159422	CDS
			locus_tag	Hyperionvirus_2_187
			product	hypothetical protein KFL_001700150
			transl_table	11
161036	161341	CDS
			locus_tag	Hyperionvirus_2_188
			product	hypothetical protein
			transl_table	11
162925	161594	CDS
			locus_tag	Hyperionvirus_2_189
			product	superfamily II helicase
			transl_table	11
163143	162976	CDS
			locus_tag	Hyperionvirus_2_190
			product	hypothetical protein
			transl_table	11
163311	163970	CDS
			locus_tag	Hyperionvirus_2_191
			product	hypothetical protein
			transl_table	11
164015	165493	CDS
			locus_tag	Hyperionvirus_2_192
			product	hypothetical protein
			transl_table	11
166586	165465	CDS
			locus_tag	Hyperionvirus_2_193
			product	hypothetical protein
			transl_table	11
166717	167316	CDS
			locus_tag	Hyperionvirus_2_194
			product	hypothetical protein
			transl_table	11
167377	168030	CDS
			locus_tag	Hyperionvirus_2_195
			product	hypothetical protein
			transl_table	11
168053	168190	CDS
			locus_tag	Hyperionvirus_2_196
			product	hypothetical protein
			transl_table	11
168259	169485	CDS
			locus_tag	Hyperionvirus_2_197
			product	hypothetical protein
			transl_table	11
169915	169559	CDS
			locus_tag	Hyperionvirus_2_198
			product	hypothetical protein C5B45_03800
			transl_table	11
170094	169912	CDS
			locus_tag	Hyperionvirus_2_199
			product	hypothetical protein
			transl_table	11
170213	170362	CDS
			locus_tag	Hyperionvirus_2_200
			product	hypothetical protein
			transl_table	11
170340	170579	CDS
			locus_tag	Hyperionvirus_2_201
			product	hypothetical protein
			transl_table	11
170810	172816	CDS
			locus_tag	Hyperionvirus_2_202
			product	hypothetical protein
			transl_table	11
173070	172870	CDS
			locus_tag	Hyperionvirus_2_203
			product	hypothetical protein
			transl_table	11
173930	173160	CDS
			locus_tag	Hyperionvirus_2_204
			product	hypothetical protein
			transl_table	11
174071	173976	CDS
			locus_tag	Hyperionvirus_2_205
			product	hypothetical protein
			transl_table	11
174070	174672	CDS
			locus_tag	Hyperionvirus_2_206
			product	hypothetical protein
			transl_table	11
174716	175330	CDS
			locus_tag	Hyperionvirus_2_207
			product	hypothetical protein
			transl_table	11
175392	176618	CDS
			locus_tag	Hyperionvirus_2_208
			product	hypothetical protein
			transl_table	11
176724	177128	CDS
			locus_tag	Hyperionvirus_2_209
			product	hypothetical protein
			transl_table	11
177181	178047	CDS
			locus_tag	Hyperionvirus_2_210
			product	hypothetical protein
			transl_table	11
178508	178044	CDS
			locus_tag	Hyperionvirus_2_211
			product	hypothetical protein
			transl_table	11
178641	179306	CDS
			locus_tag	Hyperionvirus_2_212
			product	hypothetical protein
			transl_table	11
179377	181188	CDS
			locus_tag	Hyperionvirus_2_213
			product	hypothetical protein PBRA_006143
			transl_table	11
181301	182266	CDS
			locus_tag	Hyperionvirus_2_214
			product	2'-5' RNA ligase
			transl_table	11
182327	182941	CDS
			locus_tag	Hyperionvirus_2_215
			product	hypothetical protein
			transl_table	11
183095	183910	CDS
			locus_tag	Hyperionvirus_2_216
			product	hypothetical protein
			transl_table	11
183998	184585	CDS
			locus_tag	Hyperionvirus_2_217
			product	hypothetical protein
			transl_table	11
185027	184875	CDS
			locus_tag	Hyperionvirus_2_218
			product	hypothetical protein
			transl_table	11
185171	186313	CDS
			locus_tag	Hyperionvirus_2_219
			product	hypothetical protein
			transl_table	11
186364	187323	CDS
			locus_tag	Hyperionvirus_2_220
			product	hypothetical protein
			transl_table	11
188495	187320	CDS
			locus_tag	Hyperionvirus_2_221
			product	hypothetical protein
			transl_table	11
189693	188566	CDS
			locus_tag	Hyperionvirus_2_222
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_3
985	2	CDS
			locus_tag	Hyperionvirus_3_1
			product	hypothetical protein
			transl_table	11
1564	1151	CDS
			locus_tag	Hyperionvirus_3_2
			product	hypothetical protein
			transl_table	11
2191	2394	CDS
			locus_tag	Hyperionvirus_3_3
			product	hypothetical protein
			transl_table	11
2427	2597	CDS
			locus_tag	Hyperionvirus_3_4
			product	hypothetical protein
			transl_table	11
3136	4347	CDS
			locus_tag	Hyperionvirus_3_5
			product	regulator of chromosome condensation, RCC1
			transl_table	11
4970	4356	CDS
			locus_tag	Hyperionvirus_3_6
			product	hypothetical protein
			transl_table	11
6115	5033	CDS
			locus_tag	Hyperionvirus_3_7
			product	hypothetical protein
			transl_table	11
7261	6176	CDS
			locus_tag	Hyperionvirus_3_8
			product	hypothetical protein
			transl_table	11
7748	7311	CDS
			locus_tag	Hyperionvirus_3_9
			product	internalin-A precursor
			transl_table	11
7862	9211	CDS
			locus_tag	Hyperionvirus_3_10
			product	regulator of chromosome condensation, RCC1
			transl_table	11
9224	10279	CDS
			locus_tag	Hyperionvirus_3_11
			product	hypothetical protein
			transl_table	11
10805	10302	CDS
			locus_tag	Hyperionvirus_3_12
			product	Homeobox protein 4
			transl_table	11
10963	12150	CDS
			locus_tag	Hyperionvirus_3_13
			product	hypothetical protein
			transl_table	11
12203	13768	CDS
			locus_tag	Hyperionvirus_3_14
			product	hypothetical protein
			transl_table	11
14959	13808	CDS
			locus_tag	Hyperionvirus_3_15
			product	hypothetical protein
			transl_table	11
15757	14969	CDS
			locus_tag	Hyperionvirus_3_16
			product	hypothetical protein
			transl_table	11
16505	15831	CDS
			locus_tag	Hyperionvirus_3_17
			product	hypothetical protein
			transl_table	11
17197	16562	CDS
			locus_tag	Hyperionvirus_3_18
			product	hypothetical protein
			transl_table	11
17332	18612	CDS
			locus_tag	Hyperionvirus_3_19
			product	chromosome condensation regulator
			transl_table	11
18658	20124	CDS
			locus_tag	Hyperionvirus_3_20
			product	hypothetical protein
			transl_table	11
21269	20130	CDS
			locus_tag	Hyperionvirus_3_21
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
21960	21511	CDS
			locus_tag	Hyperionvirus_3_22
			product	hypothetical protein
			transl_table	11
22579	22043	CDS
			locus_tag	Hyperionvirus_3_23
			product	hypothetical protein
			transl_table	11
22938	22681	CDS
			locus_tag	Hyperionvirus_3_24
			product	hypothetical protein
			transl_table	11
23365	23042	CDS
			locus_tag	Hyperionvirus_3_25
			product	hypothetical protein
			transl_table	11
23892	23602	CDS
			locus_tag	Hyperionvirus_3_26
			product	hypothetical protein
			transl_table	11
24574	24191	CDS
			locus_tag	Hyperionvirus_3_27
			product	hypothetical protein
			transl_table	11
24764	28021	CDS
			locus_tag	Hyperionvirus_3_28
			product	hypothetical protein
			transl_table	11
28100	29434	CDS
			locus_tag	Hyperionvirus_3_29
			product	hypothetical protein
			transl_table	11
30265	29444	CDS
			locus_tag	Hyperionvirus_3_30
			product	hypothetical protein
			transl_table	11
31118	30333	CDS
			locus_tag	Hyperionvirus_3_31
			product	hypothetical protein
			transl_table	11
31403	33037	CDS
			locus_tag	Hyperionvirus_3_32
			product	hypothetical protein
			transl_table	11
33090	33515	CDS
			locus_tag	Hyperionvirus_3_33
			product	hypothetical protein
			transl_table	11
34004	33534	CDS
			locus_tag	Hyperionvirus_3_34
			product	hypothetical protein
			transl_table	11
36438	34081	CDS
			locus_tag	Hyperionvirus_3_35
			product	hypothetical protein
			transl_table	11
37779	36604	CDS
			locus_tag	Hyperionvirus_3_36
			product	hypothetical protein
			transl_table	11
37878	38984	CDS
			locus_tag	Hyperionvirus_3_37
			product	hypothetical protein
			transl_table	11
39962	39237	CDS
			locus_tag	Hyperionvirus_3_38
			product	hypothetical protein
			transl_table	11
39993	41081	CDS
			locus_tag	Hyperionvirus_3_39
			product	hypothetical protein
			transl_table	11
42130	41099	CDS
			locus_tag	Hyperionvirus_3_40
			product	DNA polymerase family X protein
			transl_table	11
42159	42857	CDS
			locus_tag	Hyperionvirus_3_41
			product	hypothetical protein
			transl_table	11
43847	42870	CDS
			locus_tag	Hyperionvirus_3_42
			product	hypothetical protein
			transl_table	11
44164	43913	CDS
			locus_tag	Hyperionvirus_3_43
			product	hypothetical protein Klosneuvirus_3_114
			transl_table	11
44341	44517	CDS
			locus_tag	Hyperionvirus_3_44
			product	hypothetical protein
			transl_table	11
45377	45228	CDS
			locus_tag	Hyperionvirus_3_45
			product	hypothetical protein
			transl_table	11
45688	45903	CDS
			locus_tag	Hyperionvirus_3_46
			product	hypothetical protein
			transl_table	11
46584	46703	CDS
			locus_tag	Hyperionvirus_3_47
			product	hypothetical protein
			transl_table	11
46925	46818	CDS
			locus_tag	Hyperionvirus_3_48
			product	hypothetical protein
			transl_table	11
48632	48543	CDS
			locus_tag	Hyperionvirus_3_49
			product	hypothetical protein
			transl_table	11
49320	49186	CDS
			locus_tag	Hyperionvirus_3_50
			product	hypothetical protein
			transl_table	11
49945	50118	CDS
			locus_tag	Hyperionvirus_3_51
			product	hypothetical protein
			transl_table	11
50236	50907	CDS
			locus_tag	Hyperionvirus_3_52
			product	hypothetical protein
			transl_table	11
51071	51202	CDS
			locus_tag	Hyperionvirus_3_53
			product	hypothetical protein
			transl_table	11
51304	51534	CDS
			locus_tag	Hyperionvirus_3_54
			product	hypothetical protein
			transl_table	11
51606	52568	CDS
			locus_tag	Hyperionvirus_3_55
			product	hypothetical protein
			transl_table	11
53913	52570	CDS
			locus_tag	Hyperionvirus_3_56
			product	hypothetical protein
			transl_table	11
54041	54472	CDS
			locus_tag	Hyperionvirus_3_57
			product	hypothetical protein
			transl_table	11
54699	55304	CDS
			locus_tag	Hyperionvirus_3_58
			product	hypothetical protein
			transl_table	11
55568	55437	CDS
			locus_tag	Hyperionvirus_3_59
			product	hypothetical protein
			transl_table	11
56307	55624	CDS
			locus_tag	Hyperionvirus_3_60
			product	hypothetical protein
			transl_table	11
56687	57010	CDS
			locus_tag	Hyperionvirus_3_61
			product	hypothetical protein
			transl_table	11
57365	57132	CDS
			locus_tag	Hyperionvirus_3_62
			product	hypothetical protein
			transl_table	11
57446	57679	CDS
			locus_tag	Hyperionvirus_3_63
			product	hypothetical protein Klosneuvirus_8_10
			transl_table	11
58075	57659	CDS
			locus_tag	Hyperionvirus_3_64
			product	hypothetical protein
			transl_table	11
58355	60319	CDS
			locus_tag	Hyperionvirus_3_65
			product	hypothetical protein
			transl_table	11
60849	60691	CDS
			locus_tag	Hyperionvirus_3_66
			product	hypothetical protein
			transl_table	11
60850	60942	CDS
			locus_tag	Hyperionvirus_3_67
			product	hypothetical protein
			transl_table	11
61621	61067	CDS
			locus_tag	Hyperionvirus_3_68
			product	hypothetical protein Hokovirus_3_254
			transl_table	11
61745	62377	CDS
			locus_tag	Hyperionvirus_3_69
			product	hypothetical protein
			transl_table	11
62857	62399	CDS
			locus_tag	Hyperionvirus_3_70
			product	hypothetical protein
			transl_table	11
63082	64113	CDS
			locus_tag	Hyperionvirus_3_71
			product	Dynamin-related protein 1A
			transl_table	11
64082	64942	CDS
			locus_tag	Hyperionvirus_3_72
			product	hypothetical protein
			transl_table	11
65050	66003	CDS
			locus_tag	Hyperionvirus_3_73
			product	hypothetical protein
			transl_table	11
67192	66056	CDS
			locus_tag	Hyperionvirus_3_74
			product	hypothetical protein
			transl_table	11
67352	67684	CDS
			locus_tag	Hyperionvirus_3_75
			product	dynamin family GTPase
			transl_table	11
67733	68671	CDS
			locus_tag	Hyperionvirus_3_76
			product	hypothetical protein
			transl_table	11
68754	70058	CDS
			locus_tag	Hyperionvirus_3_77
			product	PREDICTED: X-linked retinitis pigmentosa GTPase regulator-like, partial
			transl_table	11
70392	71225	CDS
			locus_tag	Hyperionvirus_3_78
			product	hypothetical protein
			transl_table	11
71670	72920	CDS
			locus_tag	Hyperionvirus_3_79
			product	hypothetical protein C0J52_06044
			transl_table	11
73027	74490	CDS
			locus_tag	Hyperionvirus_3_80
			product	hypothetical protein
			transl_table	11
74584	75072	CDS
			locus_tag	Hyperionvirus_3_81
			product	hypothetical protein
			transl_table	11
75276	76250	CDS
			locus_tag	Hyperionvirus_3_82
			product	glycoside hydrolase, partial
			transl_table	11
77342	76281	CDS
			locus_tag	Hyperionvirus_3_83
			product	chromatin organization modifier chromo domain protein
			transl_table	11
78365	77409	CDS
			locus_tag	Hyperionvirus_3_84
			product	hypothetical protein
			transl_table	11
78898	78536	CDS
			locus_tag	Hyperionvirus_3_85
			product	hypothetical protein
			transl_table	11
80345	79059	CDS
			locus_tag	Hyperionvirus_3_86
			product	MULTISPECIES: hypothetical protein
			transl_table	11
81065	81757	CDS
			locus_tag	Hyperionvirus_3_87
			product	hypothetical protein
			transl_table	11
82454	81960	CDS
			locus_tag	Hyperionvirus_3_88
			product	HNH endonuclease
			transl_table	11
82590	84794	CDS
			locus_tag	Hyperionvirus_3_89
			product	type III restriction enzyme, res subunit
			transl_table	11
84857	87919	CDS
			locus_tag	Hyperionvirus_3_90
			product	restriction endonuclease
			transl_table	11
89283	88114	CDS
			locus_tag	Hyperionvirus_3_91
			product	hypothetical protein
			transl_table	11
89327	90352	CDS
			locus_tag	Hyperionvirus_3_92
			product	hypothetical protein
			transl_table	11
90518	91594	CDS
			locus_tag	Hyperionvirus_3_93
			product	HNH endonuclease
			transl_table	11
93139	91733	CDS
			locus_tag	Hyperionvirus_3_94
			product	hypothetical protein
			transl_table	11
93138	93257	CDS
			locus_tag	Hyperionvirus_3_95
			product	hypothetical protein
			transl_table	11
93419	94237	CDS
			locus_tag	Hyperionvirus_3_96
			product	hypothetical protein
			transl_table	11
94378	94238	CDS
			locus_tag	Hyperionvirus_3_97
			product	hypothetical protein
			transl_table	11
95716	94829	CDS
			locus_tag	Hyperionvirus_3_98
			product	hypothetical protein
			transl_table	11
96448	95771	CDS
			locus_tag	Hyperionvirus_3_99
			product	hypothetical protein
			transl_table	11
97937	96504	CDS
			locus_tag	Hyperionvirus_3_100
			product	chromosome condensation regulator, partial
			transl_table	11
98075	98668	CDS
			locus_tag	Hyperionvirus_3_101
			product	hypothetical protein
			transl_table	11
98763	99611	CDS
			locus_tag	Hyperionvirus_3_102
			product	hypothetical protein
			transl_table	11
99605	100582	CDS
			locus_tag	Hyperionvirus_3_103
			product	hypothetical protein
			transl_table	11
100767	101666	CDS
			locus_tag	Hyperionvirus_3_104
			product	hypothetical protein
			transl_table	11
102785	101778	CDS
			locus_tag	Hyperionvirus_3_105
			product	hypothetical protein
			transl_table	11
103310	102858	CDS
			locus_tag	Hyperionvirus_3_106
			product	hypothetical protein
			transl_table	11
103835	103374	CDS
			locus_tag	Hyperionvirus_3_107
			product	hypothetical protein
			transl_table	11
103987	104967	CDS
			locus_tag	Hyperionvirus_3_108
			product	hypothetical protein Klosneuvirus_1_331
			transl_table	11
105004	106266	CDS
			locus_tag	Hyperionvirus_3_109
			product	hypothetical protein AUK34_01090
			transl_table	11
106420	107721	CDS
			locus_tag	Hyperionvirus_3_110
			product	hypothetical protein
			transl_table	11
107930	108763	CDS
			locus_tag	Hyperionvirus_3_111
			product	hypothetical protein
			transl_table	11
108926	109513	CDS
			locus_tag	Hyperionvirus_3_112
			product	hypothetical protein
			transl_table	11
109724	110041	CDS
			locus_tag	Hyperionvirus_3_113
			product	hypothetical protein
			transl_table	11
110807	110091	CDS
			locus_tag	Hyperionvirus_3_114
			product	hypothetical protein
			transl_table	11
110806	110913	CDS
			locus_tag	Hyperionvirus_3_115
			product	hypothetical protein
			transl_table	11
111103	111555	CDS
			locus_tag	Hyperionvirus_3_116
			product	hypothetical protein
			transl_table	11
114114	111676	CDS
			locus_tag	Hyperionvirus_3_117
			product	hypothetical protein
			transl_table	11
114903	114358	CDS
			locus_tag	Hyperionvirus_3_118
			product	hypothetical protein
			transl_table	11
115028	115729	CDS
			locus_tag	Hyperionvirus_3_119
			product	hypothetical protein
			transl_table	11
115815	116069	CDS
			locus_tag	Hyperionvirus_3_120
			product	hypothetical protein
			transl_table	11
116144	117160	CDS
			locus_tag	Hyperionvirus_3_121
			product	hypothetical protein
			transl_table	11
118508	117186	CDS
			locus_tag	Hyperionvirus_3_122
			product	hypothetical protein
			transl_table	11
119147	118569	CDS
			locus_tag	Hyperionvirus_3_123
			product	hypothetical protein ACA1_088890
			transl_table	11
119416	119201	CDS
			locus_tag	Hyperionvirus_3_124
			product	hypothetical protein
			transl_table	11
122954	119823	CDS
			locus_tag	Hyperionvirus_3_125
			product	hypothetical protein
			transl_table	11
123109	124413	CDS
			locus_tag	Hyperionvirus_3_126
			product	chromosome condensation regulator
			transl_table	11
124458	125333	CDS
			locus_tag	Hyperionvirus_3_127
			product	hypothetical protein
			transl_table	11
125393	126034	CDS
			locus_tag	Hyperionvirus_3_128
			product	hypothetical protein
			transl_table	11
126076	127218	CDS
			locus_tag	Hyperionvirus_3_129
			product	hypothetical protein
			transl_table	11
127307	127747	CDS
			locus_tag	Hyperionvirus_3_130
			product	hypothetical protein
			transl_table	11
127797	128807	CDS
			locus_tag	Hyperionvirus_3_131
			product	hypothetical protein PBRA_002699
			transl_table	11
128908	129006	CDS
			locus_tag	Hyperionvirus_3_132
			product	hypothetical protein
			transl_table	11
129097	129753	CDS
			locus_tag	Hyperionvirus_3_133
			product	hypothetical protein
			transl_table	11
129833	131329	CDS
			locus_tag	Hyperionvirus_3_134
			product	hypothetical protein TRIADDRAFT_26989
			transl_table	11
131355	132119	CDS
			locus_tag	Hyperionvirus_3_135
			product	hypothetical protein
			transl_table	11
132165	133436	CDS
			locus_tag	Hyperionvirus_3_136
			product	chromosome condensation regulator
			transl_table	11
133900	133433	CDS
			locus_tag	Hyperionvirus_3_137
			product	hypothetical protein
			transl_table	11
134088	135419	CDS
			locus_tag	Hyperionvirus_3_138
			product	hypothetical protein
			transl_table	11
136131	135427	CDS
			locus_tag	Hyperionvirus_3_139
			product	hypothetical protein
			transl_table	11
136488	136291	CDS
			locus_tag	Hyperionvirus_3_140
			product	hypothetical protein
			transl_table	11
137210	137010	CDS
			locus_tag	Hyperionvirus_3_141
			product	hypothetical protein
			transl_table	11
138271	138062	CDS
			locus_tag	Hyperionvirus_3_142
			product	hypothetical protein
			transl_table	11
138795	139307	CDS
			locus_tag	Hyperionvirus_3_143
			product	hypothetical protein
			transl_table	11
139618	140229	CDS
			locus_tag	Hyperionvirus_3_144
			product	hypothetical protein
			transl_table	11
140629	141690	CDS
			locus_tag	Hyperionvirus_3_145
			product	hypothetical protein Gasu_16580
			transl_table	11
142465	141692	CDS
			locus_tag	Hyperionvirus_3_146
			product	hypothetical protein
			transl_table	11
143345	142536	CDS
			locus_tag	Hyperionvirus_3_147
			product	hypothetical protein K457DRAFT_151319
			transl_table	11
143544	144581	CDS
			locus_tag	Hyperionvirus_3_148
			product	hypothetical protein
			transl_table	11
147963	144571	CDS
			locus_tag	Hyperionvirus_3_149
			product	MAC/perforin domain protein
			transl_table	11
148187	147963	CDS
			locus_tag	Hyperionvirus_3_150
			product	hypothetical protein ACD_31C00117G0003
			transl_table	11
148316	149119	CDS
			locus_tag	Hyperionvirus_3_151
			product	YchF/TatD family DNA exonuclease
			transl_table	11
149174	149476	CDS
			locus_tag	Hyperionvirus_3_152
			product	J domain-containing protein
			transl_table	11
149547	150611	CDS
			locus_tag	Hyperionvirus_3_153
			product	hypothetical protein
			transl_table	11
151521	152111	CDS
			locus_tag	Hyperionvirus_3_154
			product	hypothetical protein
			transl_table	11
152310	152477	CDS
			locus_tag	Hyperionvirus_3_155
			product	hypothetical protein
			transl_table	11
152975	152520	CDS
			locus_tag	Hyperionvirus_3_156
			product	translation initiation factor eIF-5A.2
			transl_table	11
153963	153058	CDS
			locus_tag	Hyperionvirus_3_157
			product	hypothetical protein
			transl_table	11
154168	155640	CDS
			locus_tag	Hyperionvirus_3_158
			product	hypothetical protein
			transl_table	11
156024	155647	CDS
			locus_tag	Hyperionvirus_3_159
			product	hypothetical protein
			transl_table	11
156118	156828	CDS
			locus_tag	Hyperionvirus_3_160
			product	hypothetical protein
			transl_table	11
156896	158512	CDS
			locus_tag	Hyperionvirus_3_161
			product	AAA family ATPase
			transl_table	11
158607	159302	CDS
			locus_tag	Hyperionvirus_3_162
			product	hypothetical protein
			transl_table	11
159462	160295	CDS
			locus_tag	Hyperionvirus_3_163
			product	alpha/beta hydrolase
			transl_table	11
160382	160474	CDS
			locus_tag	Hyperionvirus_3_164
			product	hypothetical protein
			transl_table	11
160528	161148	CDS
			locus_tag	Hyperionvirus_3_165
			product	hypothetical protein
			transl_table	11
162631	161657	CDS
			locus_tag	Hyperionvirus_3_166
			product	PREDICTED: uncharacterized protein LOC108790716
			transl_table	11
162716	163756	CDS
			locus_tag	Hyperionvirus_3_167
			product	hypothetical protein
			transl_table	11
163851	164294	CDS
			locus_tag	Hyperionvirus_3_168
			product	hypothetical protein
			transl_table	11
164362	164871	CDS
			locus_tag	Hyperionvirus_3_169
			product	hypothetical protein
			transl_table	11
164903	165145	CDS
			locus_tag	Hyperionvirus_3_170
			product	hypothetical protein
			transl_table	11
165574	165188	CDS
			locus_tag	Hyperionvirus_3_171
			product	hypothetical protein
			transl_table	11
165764	165898	CDS
			locus_tag	Hyperionvirus_3_172
			product	hypothetical protein
			transl_table	11
166875	166039	CDS
			locus_tag	Hyperionvirus_3_173
			product	hypothetical protein
			transl_table	11
167116	168048	CDS
			locus_tag	Hyperionvirus_3_174
			product	hypothetical protein Catovirus_1_66
			transl_table	11
168263	168096	CDS
			locus_tag	Hyperionvirus_3_175
			product	hypothetical protein
			transl_table	11
168266	169375	CDS
			locus_tag	Hyperionvirus_3_176
			product	hypothetical protein
			transl_table	11
170914	169388	CDS
			locus_tag	Hyperionvirus_3_177
			product	hypothetical protein
			transl_table	11
171049	171588	CDS
			locus_tag	Hyperionvirus_3_178
			product	hypothetical protein
			transl_table	11
171651	171842	CDS
			locus_tag	Hyperionvirus_3_179
			product	hypothetical protein
			transl_table	11
172063	171878	CDS
			locus_tag	Hyperionvirus_3_180
			product	hypothetical protein
			transl_table	11
174201	172354	CDS
			locus_tag	Hyperionvirus_3_181
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_4
3	473	CDS
			locus_tag	Hyperionvirus_4_1
			product	hypothetical protein
			transl_table	11
1298	513	CDS
			locus_tag	Hyperionvirus_4_2
			product	hypothetical protein
			transl_table	11
1557	2978	CDS
			locus_tag	Hyperionvirus_4_3
			product	chromosome condensation regulator
			transl_table	11
4654	2996	CDS
			locus_tag	Hyperionvirus_4_4
			product	hypothetical protein
			transl_table	11
5478	4723	CDS
			locus_tag	Hyperionvirus_4_5
			product	RNase adaptor protein
			transl_table	11
6593	5754	CDS
			locus_tag	Hyperionvirus_4_6
			product	hypothetical protein
			transl_table	11
7510	6827	CDS
			locus_tag	Hyperionvirus_4_7
			product	hypothetical protein
			transl_table	11
8409	7861	CDS
			locus_tag	Hyperionvirus_4_8
			product	hypothetical protein
			transl_table	11
8906	8478	CDS
			locus_tag	Hyperionvirus_4_9
			product	hypothetical protein
			transl_table	11
9355	9119	CDS
			locus_tag	Hyperionvirus_4_10
			product	hypothetical protein
			transl_table	11
9390	9593	CDS
			locus_tag	Hyperionvirus_4_11
			product	hypothetical protein
			transl_table	11
9805	9668	CDS
			locus_tag	Hyperionvirus_4_12
			product	hypothetical protein
			transl_table	11
11698	12660	CDS
			locus_tag	Hyperionvirus_4_13
			product	hypothetical protein
			transl_table	11
13203	12712	CDS
			locus_tag	Hyperionvirus_4_14
			product	hypothetical protein
			transl_table	11
14186	14079	CDS
			locus_tag	Hyperionvirus_4_15
			product	hypothetical protein
			transl_table	11
17756	14907	CDS
			locus_tag	Hyperionvirus_4_16
			product	hypothetical protein Catovirus_1_615
			transl_table	11
18109	17846	CDS
			locus_tag	Hyperionvirus_4_17
			product	hypothetical protein
			transl_table	11
18852	18199	CDS
			locus_tag	Hyperionvirus_4_18
			product	hypothetical protein
			transl_table	11
19402	19001	CDS
			locus_tag	Hyperionvirus_4_19
			product	hypothetical protein
			transl_table	11
20670	19642	CDS
			locus_tag	Hyperionvirus_4_20
			product	hypothetical protein
			transl_table	11
21284	21009	CDS
			locus_tag	Hyperionvirus_4_21
			product	hypothetical protein
			transl_table	11
21969	21358	CDS
			locus_tag	Hyperionvirus_4_22
			product	hypothetical protein
			transl_table	11
22156	21959	CDS
			locus_tag	Hyperionvirus_4_23
			product	hypothetical protein
			transl_table	11
22178	22318	CDS
			locus_tag	Hyperionvirus_4_24
			product	hypothetical protein
			transl_table	11
22341	23639	CDS
			locus_tag	Hyperionvirus_4_25
			product	chromosome condensation regulator, partial
			transl_table	11
23711	24772	CDS
			locus_tag	Hyperionvirus_4_26
			product	hypothetical protein
			transl_table	11
24855	25094	CDS
			locus_tag	Hyperionvirus_4_27
			product	hypothetical protein
			transl_table	11
26509	25112	CDS
			locus_tag	Hyperionvirus_4_28
			product	hypothetical protein
			transl_table	11
26646	26957	CDS
			locus_tag	Hyperionvirus_4_29
			product	hypothetical protein
			transl_table	11
27042	27167	CDS
			locus_tag	Hyperionvirus_4_30
			product	hypothetical protein
			transl_table	11
27335	27874	CDS
			locus_tag	Hyperionvirus_4_31
			product	hypothetical protein
			transl_table	11
27972	28694	CDS
			locus_tag	Hyperionvirus_4_32
			product	hypothetical protein
			transl_table	11
29986	28706	CDS
			locus_tag	Hyperionvirus_4_33
			product	hypothetical protein
			transl_table	11
30098	30901	CDS
			locus_tag	Hyperionvirus_4_34
			product	hypothetical protein
			transl_table	11
31802	33067	CDS
			locus_tag	Hyperionvirus_4_35
			product	hypothetical protein
			transl_table	11
33171	34196	CDS
			locus_tag	Hyperionvirus_4_36
			product	hypothetical protein
			transl_table	11
37005	34228	CDS
			locus_tag	Hyperionvirus_4_37
			product	putative ankyrin repeat protein
			transl_table	11
37431	37715	CDS
			locus_tag	Hyperionvirus_4_38
			product	BRO-N domain protein
			transl_table	11
39528	37693	CDS
			locus_tag	Hyperionvirus_4_39
			product	predicted protein
			transl_table	11
39649	40815	CDS
			locus_tag	Hyperionvirus_4_40
			product	hypothetical protein
			transl_table	11
42103	40790	CDS
			locus_tag	Hyperionvirus_4_41
			product	hypothetical protein
			transl_table	11
42170	42694	CDS
			locus_tag	Hyperionvirus_4_42
			product	hypothetical protein
			transl_table	11
42933	42760	CDS
			locus_tag	Hyperionvirus_4_43
			product	hypothetical protein
			transl_table	11
42932	43369	CDS
			locus_tag	Hyperionvirus_4_44
			product	hypothetical protein
			transl_table	11
43507	44292	CDS
			locus_tag	Hyperionvirus_4_45
			product	hypothetical protein A3J93_00600
			transl_table	11
46118	44340	CDS
			locus_tag	Hyperionvirus_4_46
			product	hypothetical protein GALMADRAFT_251438
			transl_table	11
46922	46179	CDS
			locus_tag	Hyperionvirus_4_47
			product	hypothetical protein
			transl_table	11
47082	47360	CDS
			locus_tag	Hyperionvirus_4_48
			product	hypothetical protein
			transl_table	11
47788	47405	CDS
			locus_tag	Hyperionvirus_4_49
			product	hypothetical protein
			transl_table	11
48225	48043	CDS
			locus_tag	Hyperionvirus_4_50
			product	hypothetical protein
			transl_table	11
48313	48591	CDS
			locus_tag	Hyperionvirus_4_51
			product	hypothetical protein
			transl_table	11
48676	49038	CDS
			locus_tag	Hyperionvirus_4_52
			product	hypothetical protein
			transl_table	11
49145	49597	CDS
			locus_tag	Hyperionvirus_4_53
			product	hypothetical protein
			transl_table	11
49655	50410	CDS
			locus_tag	Hyperionvirus_4_54
			product	hypothetical protein
			transl_table	11
51022	50516	CDS
			locus_tag	Hyperionvirus_4_55
			product	hypothetical protein
			transl_table	11
52108	51341	CDS
			locus_tag	Hyperionvirus_4_56
			product	hypothetical protein
			transl_table	11
52341	52204	CDS
			locus_tag	Hyperionvirus_4_57
			product	hypothetical protein
			transl_table	11
52478	53266	CDS
			locus_tag	Hyperionvirus_4_58
			product	hypothetical protein
			transl_table	11
53865	53984	CDS
			locus_tag	Hyperionvirus_4_59
			product	hypothetical protein
			transl_table	11
54013	54141	CDS
			locus_tag	Hyperionvirus_4_60
			product	hypothetical protein
			transl_table	11
54792	55715	CDS
			locus_tag	Hyperionvirus_4_61
			product	hypothetical protein
			transl_table	11
56958	55927	CDS
			locus_tag	Hyperionvirus_4_62
			product	BRO-N domain protein
			transl_table	11
57804	58586	CDS
			locus_tag	Hyperionvirus_4_63
			product	superfamily II DNA or RNA helicase
			transl_table	11
58731	59576	CDS
			locus_tag	Hyperionvirus_4_64
			product	BRO-N domain protein
			transl_table	11
60672	59761	CDS
			locus_tag	Hyperionvirus_4_65
			product	hypothetical protein
			transl_table	11
60671	60778	CDS
			locus_tag	Hyperionvirus_4_66
			product	hypothetical protein
			transl_table	11
61908	60793	CDS
			locus_tag	Hyperionvirus_4_67
			product	hypothetical protein
			transl_table	11
62691	63803	CDS
			locus_tag	Hyperionvirus_4_68
			product	GMC-type oxidoreductase
			transl_table	11
64762	64538	CDS
			locus_tag	Hyperionvirus_4_69
			product	hypothetical protein
			transl_table	11
65255	65344	CDS
			locus_tag	Hyperionvirus_4_70
			product	hypothetical protein
			transl_table	11
65482	65571	CDS
			locus_tag	Hyperionvirus_4_71
			product	hypothetical protein
			transl_table	11
66859	66743	CDS
			locus_tag	Hyperionvirus_4_72
			product	hypothetical protein
			transl_table	11
67454	67975	CDS
			locus_tag	Hyperionvirus_4_73
			product	hypothetical protein
			transl_table	11
67994	68146	CDS
			locus_tag	Hyperionvirus_4_74
			product	hypothetical protein
			transl_table	11
68748	69152	CDS
			locus_tag	Hyperionvirus_4_75
			product	hypothetical protein
			transl_table	11
70650	69271	CDS
			locus_tag	Hyperionvirus_4_76
			product	putative rep protein
			transl_table	11
70964	70722	CDS
			locus_tag	Hyperionvirus_4_77
			product	hypothetical protein
			transl_table	11
71440	71318	CDS
			locus_tag	Hyperionvirus_4_78
			product	hypothetical protein
			transl_table	11
73201	72248	CDS
			locus_tag	Hyperionvirus_4_79
			product	putative UV-damage endonuclease
			transl_table	11
73381	73788	CDS
			locus_tag	Hyperionvirus_4_80
			product	hypothetical protein
			transl_table	11
73988	75283	CDS
			locus_tag	Hyperionvirus_4_81
			product	hypothetical protein
			transl_table	11
76418	75315	CDS
			locus_tag	Hyperionvirus_4_82
			product	hypothetical protein
			transl_table	11
76510	77505	CDS
			locus_tag	Hyperionvirus_4_83
			product	TIGR03118 family protein
			transl_table	11
78376	77588	CDS
			locus_tag	Hyperionvirus_4_84
			product	hypothetical protein
			transl_table	11
78620	79654	CDS
			locus_tag	Hyperionvirus_4_85
			product	hypothetical protein
			transl_table	11
79780	80547	CDS
			locus_tag	Hyperionvirus_4_86
			product	hypothetical protein
			transl_table	11
81026	80496	CDS
			locus_tag	Hyperionvirus_4_87
			product	hypothetical protein
			transl_table	11
81647	81297	CDS
			locus_tag	Hyperionvirus_4_88
			product	hypothetical protein
			transl_table	11
81854	82813	CDS
			locus_tag	Hyperionvirus_4_89
			product	hypothetical protein
			transl_table	11
82881	83792	CDS
			locus_tag	Hyperionvirus_4_90
			product	hypothetical protein
			transl_table	11
84168	84923	CDS
			locus_tag	Hyperionvirus_4_91
			product	hypothetical protein
			transl_table	11
85376	84936	CDS
			locus_tag	Hyperionvirus_4_92
			product	hypothetical protein
			transl_table	11
85475	86275	CDS
			locus_tag	Hyperionvirus_4_93
			product	hypothetical protein
			transl_table	11
86896	86258	CDS
			locus_tag	Hyperionvirus_4_94
			product	hypothetical protein
			transl_table	11
87817	86990	CDS
			locus_tag	Hyperionvirus_4_95
			product	hypothetical protein
			transl_table	11
88510	87869	CDS
			locus_tag	Hyperionvirus_4_96
			product	DUF296 domain-containing protein
			transl_table	11
90111	88543	CDS
			locus_tag	Hyperionvirus_4_97
			product	collagen triple helix repeat containing protein
			transl_table	11
90580	90729	CDS
			locus_tag	Hyperionvirus_4_98
			product	hypothetical protein
			transl_table	11
91208	91026	CDS
			locus_tag	Hyperionvirus_4_99
			product	hypothetical protein
			transl_table	11
91911	91201	CDS
			locus_tag	Hyperionvirus_4_100
			product	IS4 family transposase
			transl_table	11
92448	92305	CDS
			locus_tag	Hyperionvirus_4_101
			product	hypothetical protein
			transl_table	11
92911	95127	CDS
			locus_tag	Hyperionvirus_4_102
			product	hypothetical protein Indivirus_1_31
			transl_table	11
95249	95890	CDS
			locus_tag	Hyperionvirus_4_103
			product	hypothetical protein
			transl_table	11
96989	96006	CDS
			locus_tag	Hyperionvirus_4_104
			product	hypothetical protein
			transl_table	11
97133	97630	CDS
			locus_tag	Hyperionvirus_4_105
			product	hypothetical protein
			transl_table	11
97702	98199	CDS
			locus_tag	Hyperionvirus_4_106
			product	hypothetical protein
			transl_table	11
98274	98942	CDS
			locus_tag	Hyperionvirus_4_107
			product	hypothetical protein
			transl_table	11
99009	99656	CDS
			locus_tag	Hyperionvirus_4_108
			product	hypothetical protein
			transl_table	11
100032	99646	CDS
			locus_tag	Hyperionvirus_4_109
			product	hypothetical protein
			transl_table	11
101053	100088	CDS
			locus_tag	Hyperionvirus_4_110
			product	hypothetical protein
			transl_table	11
101977	101141	CDS
			locus_tag	Hyperionvirus_4_111
			product	hypothetical protein
			transl_table	11
103174	102059	CDS
			locus_tag	Hyperionvirus_4_112
			product	hypothetical protein
			transl_table	11
104065	103253	CDS
			locus_tag	Hyperionvirus_4_113
			product	hypothetical protein
			transl_table	11
104319	104693	CDS
			locus_tag	Hyperionvirus_4_114
			product	hypothetical protein
			transl_table	11
106231	104753	CDS
			locus_tag	Hyperionvirus_4_115
			product	hypothetical protein
			transl_table	11
106379	106861	CDS
			locus_tag	Hyperionvirus_4_116
			product	hypothetical protein
			transl_table	11
108149	106902	CDS
			locus_tag	Hyperionvirus_4_117
			product	hypothetical protein BATDEDRAFT_11439, partial
			transl_table	11
108246	108740	CDS
			locus_tag	Hyperionvirus_4_118
			product	hypothetical protein
			transl_table	11
109562	108963	CDS
			locus_tag	Hyperionvirus_4_119
			product	hypothetical protein
			transl_table	11
110554	109820	CDS
			locus_tag	Hyperionvirus_4_120
			product	hypothetical protein
			transl_table	11
111093	110650	CDS
			locus_tag	Hyperionvirus_4_121
			product	hypothetical protein
			transl_table	11
111703	111158	CDS
			locus_tag	Hyperionvirus_4_122
			product	exported hypothetical protein
			transl_table	11
112177	112269	CDS
			locus_tag	Hyperionvirus_4_123
			product	hypothetical protein
			transl_table	11
112528	113721	CDS
			locus_tag	Hyperionvirus_4_124
			product	hypothetical protein
			transl_table	11
113770	114390	CDS
			locus_tag	Hyperionvirus_4_125
			product	hypothetical protein
			transl_table	11
114634	114515	CDS
			locus_tag	Hyperionvirus_4_126
			product	hypothetical protein
			transl_table	11
114747	115448	CDS
			locus_tag	Hyperionvirus_4_127
			product	hypothetical protein
			transl_table	11
115496	116332	CDS
			locus_tag	Hyperionvirus_4_128
			product	hypothetical protein
			transl_table	11
116389	116886	CDS
			locus_tag	Hyperionvirus_4_129
			product	hypothetical protein
			transl_table	11
117130	117369	CDS
			locus_tag	Hyperionvirus_4_130
			product	hypothetical protein
			transl_table	11
118027	117725	CDS
			locus_tag	Hyperionvirus_4_131
			product	hypothetical protein
			transl_table	11
118342	118100	CDS
			locus_tag	Hyperionvirus_4_132
			product	hypothetical protein
			transl_table	11
120045	118384	CDS
			locus_tag	Hyperionvirus_4_133
			product	hypothetical protein
			transl_table	11
122293	120158	CDS
			locus_tag	Hyperionvirus_4_134
			product	hypothetical protein
			transl_table	11
123020	122385	CDS
			locus_tag	Hyperionvirus_4_135
			product	hypothetical protein
			transl_table	11
124038	123076	CDS
			locus_tag	Hyperionvirus_4_136
			product	hypothetical protein
			transl_table	11
124409	124918	CDS
			locus_tag	Hyperionvirus_4_137
			product	hypothetical protein
			transl_table	11
125585	125022	CDS
			locus_tag	Hyperionvirus_4_138
			product	hypothetical protein
			transl_table	11
125610	126416	CDS
			locus_tag	Hyperionvirus_4_139
			product	PREDICTED: protein tumorous imaginal discs, mitochondrial
			transl_table	11
126530	127189	CDS
			locus_tag	Hyperionvirus_4_140
			product	hypothetical protein
			transl_table	11
128038	127214	CDS
			locus_tag	Hyperionvirus_4_141
			product	hypothetical protein
			transl_table	11
128268	128693	CDS
			locus_tag	Hyperionvirus_4_142
			product	hypothetical protein
			transl_table	11
128761	130017	CDS
			locus_tag	Hyperionvirus_4_143
			product	hypothetical protein
			transl_table	11
130661	131260	CDS
			locus_tag	Hyperionvirus_4_144
			product	hypothetical protein
			transl_table	11
132388	131459	CDS
			locus_tag	Hyperionvirus_4_145
			product	hypothetical protein
			transl_table	11
133686	132553	CDS
			locus_tag	Hyperionvirus_4_146
			product	hypothetical protein
			transl_table	11
135441	133729	CDS
			locus_tag	Hyperionvirus_4_147
			product	hypothetical protein
			transl_table	11
136307	135510	CDS
			locus_tag	Hyperionvirus_4_148
			product	hypothetical protein
			transl_table	11
136584	136375	CDS
			locus_tag	Hyperionvirus_4_149
			product	hypothetical protein
			transl_table	11
137860	136727	CDS
			locus_tag	Hyperionvirus_4_150
			product	hypothetical protein
			transl_table	11
138026	138895	CDS
			locus_tag	Hyperionvirus_4_151
			product	hypothetical protein
			transl_table	11
138971	139279	CDS
			locus_tag	Hyperionvirus_4_152
			product	hypothetical protein
			transl_table	11
140042	139281	CDS
			locus_tag	Hyperionvirus_4_153
			product	hypothetical protein
			transl_table	11
140255	141382	CDS
			locus_tag	Hyperionvirus_4_154
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
142770	142928	CDS
			locus_tag	Hyperionvirus_4_155
			product	hypothetical protein
			transl_table	11
144084	143968	CDS
			locus_tag	Hyperionvirus_4_156
			product	hypothetical protein
			transl_table	11
144597	144502	CDS
			locus_tag	Hyperionvirus_4_157
			product	hypothetical protein
			transl_table	11
145620	145513	CDS
			locus_tag	Hyperionvirus_4_158
			product	hypothetical protein
			transl_table	11
145916	145755	CDS
			locus_tag	Hyperionvirus_4_159
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_5
521	646	CDS
			locus_tag	Hyperionvirus_5_1
			product	hypothetical protein
			transl_table	11
1803	817	CDS
			locus_tag	Hyperionvirus_5_2
			product	hypothetical protein
			transl_table	11
2751	2164	CDS
			locus_tag	Hyperionvirus_5_3
			product	hypothetical protein
			transl_table	11
3515	2817	CDS
			locus_tag	Hyperionvirus_5_4
			product	hypothetical protein
			transl_table	11
3651	4418	CDS
			locus_tag	Hyperionvirus_5_5
			product	hypothetical protein
			transl_table	11
4890	4390	CDS
			locus_tag	Hyperionvirus_5_6
			product	hypothetical protein
			transl_table	11
5724	4954	CDS
			locus_tag	Hyperionvirus_5_7
			product	Pyruvate phosphate dikinase PEP/pyruvate-binding protein
			transl_table	11
5723	5815	CDS
			locus_tag	Hyperionvirus_5_8
			product	hypothetical protein
			transl_table	11
5843	7279	CDS
			locus_tag	Hyperionvirus_5_9
			product	hypothetical protein
			transl_table	11
7344	8390	CDS
			locus_tag	Hyperionvirus_5_10
			product	hypothetical protein
			transl_table	11
8587	8477	CDS
			locus_tag	Hyperionvirus_5_11
			product	hypothetical protein
			transl_table	11
9351	8575	CDS
			locus_tag	Hyperionvirus_5_12
			product	hypothetical protein KFL_008760020
			transl_table	11
10103	9411	CDS
			locus_tag	Hyperionvirus_5_13
			product	PREDICTED: uncharacterized protein LOC105123441
			transl_table	11
10360	10235	CDS
			locus_tag	Hyperionvirus_5_14
			product	hypothetical protein
			transl_table	11
10463	10636	CDS
			locus_tag	Hyperionvirus_5_15
			product	hypothetical protein
			transl_table	11
11535	10633	CDS
			locus_tag	Hyperionvirus_5_16
			product	hypothetical protein
			transl_table	11
11822	12142	CDS
			locus_tag	Hyperionvirus_5_17
			product	hypothetical protein
			transl_table	11
12329	12433	CDS
			locus_tag	Hyperionvirus_5_18
			product	hypothetical protein
			transl_table	11
12444	12674	CDS
			locus_tag	Hyperionvirus_5_19
			product	hypothetical protein
			transl_table	11
13797	13603	CDS
			locus_tag	Hyperionvirus_5_20
			product	hypothetical protein
			transl_table	11
14294	13848	CDS
			locus_tag	Hyperionvirus_5_21
			product	hypothetical protein
			transl_table	11
14427	14744	CDS
			locus_tag	Hyperionvirus_5_22
			product	hypothetical protein
			transl_table	11
14844	15740	CDS
			locus_tag	Hyperionvirus_5_23
			product	hypothetical protein
			transl_table	11
16103	15714	CDS
			locus_tag	Hyperionvirus_5_24
			product	hypothetical protein
			transl_table	11
16232	18007	CDS
			locus_tag	Hyperionvirus_5_25
			product	hypothetical protein
			transl_table	11
18497	18643	CDS
			locus_tag	Hyperionvirus_5_26
			product	hypothetical protein
			transl_table	11
18720	19370	CDS
			locus_tag	Hyperionvirus_5_27
			product	hypothetical protein
			transl_table	11
19419	20303	CDS
			locus_tag	Hyperionvirus_5_28
			product	hypothetical protein
			transl_table	11
20808	20293	CDS
			locus_tag	Hyperionvirus_5_29
			product	hypothetical protein
			transl_table	11
22309	20873	CDS
			locus_tag	Hyperionvirus_5_30
			product	chromosome condensation regulator
			transl_table	11
22764	22309	CDS
			locus_tag	Hyperionvirus_5_31
			product	hypothetical protein
			transl_table	11
22845	23867	CDS
			locus_tag	Hyperionvirus_5_32
			product	hypothetical protein
			transl_table	11
24288	23887	CDS
			locus_tag	Hyperionvirus_5_33
			product	hypothetical protein
			transl_table	11
27032	24963	CDS
			locus_tag	Hyperionvirus_5_34
			product	hypothetical protein Indivirus_1_31
			transl_table	11
27336	27190	CDS
			locus_tag	Hyperionvirus_5_35
			product	hypothetical protein
			transl_table	11
27700	27563	CDS
			locus_tag	Hyperionvirus_5_36
			product	hypothetical protein
			transl_table	11
28134	27748	CDS
			locus_tag	Hyperionvirus_5_37
			product	hypothetical protein
			transl_table	11
28772	28215	CDS
			locus_tag	Hyperionvirus_5_38
			product	hypothetical protein
			transl_table	11
29956	28847	CDS
			locus_tag	Hyperionvirus_5_39
			product	MULTISPECIES: hypothetical protein
			transl_table	11
30099	31490	CDS
			locus_tag	Hyperionvirus_5_40
			product	hypothetical protein
			transl_table	11
32584	31487	CDS
			locus_tag	Hyperionvirus_5_41
			product	hypothetical protein
			transl_table	11
33686	32607	CDS
			locus_tag	Hyperionvirus_5_42
			product	aaa ATPase family protein
			transl_table	11
35271	34354	CDS
			locus_tag	Hyperionvirus_5_43
			product	hypothetical protein
			transl_table	11
35412	36266	CDS
			locus_tag	Hyperionvirus_5_44
			product	hypothetical protein
			transl_table	11
37517	36294	CDS
			locus_tag	Hyperionvirus_5_45
			product	hypothetical protein
			transl_table	11
38764	37553	CDS
			locus_tag	Hyperionvirus_5_46
			product	hypothetical protein
			transl_table	11
38897	39091	CDS
			locus_tag	Hyperionvirus_5_47
			product	hypothetical protein
			transl_table	11
39091	39510	CDS
			locus_tag	Hyperionvirus_5_48
			product	hypothetical protein
			transl_table	11
39663	40979	CDS
			locus_tag	Hyperionvirus_5_49
			product	hypothetical protein
			transl_table	11
41110	41811	CDS
			locus_tag	Hyperionvirus_5_50
			product	hypothetical protein
			transl_table	11
42884	42660	CDS
			locus_tag	Hyperionvirus_5_51
			product	hypothetical protein
			transl_table	11
43002	42871	CDS
			locus_tag	Hyperionvirus_5_52
			product	hypothetical protein
			transl_table	11
43202	43071	CDS
			locus_tag	Hyperionvirus_5_53
			product	hypothetical protein
			transl_table	11
43515	43306	CDS
			locus_tag	Hyperionvirus_5_54
			product	hypothetical protein
			transl_table	11
43650	44165	CDS
			locus_tag	Hyperionvirus_5_55
			product	hypothetical protein
			transl_table	11
44208	44993	CDS
			locus_tag	Hyperionvirus_5_56
			product	hypothetical protein TSOC_014669
			transl_table	11
45067	46977	CDS
			locus_tag	Hyperionvirus_5_57
			product	hypothetical protein Klosneuvirus_5_56
			transl_table	11
47029	48096	CDS
			locus_tag	Hyperionvirus_5_58
			product	TIGR03118 family protein
			transl_table	11
48164	48670	CDS
			locus_tag	Hyperionvirus_5_59
			product	hypothetical protein
			transl_table	11
49420	48680	CDS
			locus_tag	Hyperionvirus_5_60
			product	hypothetical protein
			transl_table	11
49557	51032	CDS
			locus_tag	Hyperionvirus_5_61
			product	hypothetical protein
			transl_table	11
51579	51148	CDS
			locus_tag	Hyperionvirus_5_62
			product	PAS domain S-box protein
			transl_table	11
51685	52533	CDS
			locus_tag	Hyperionvirus_5_63
			product	hypothetical protein
			transl_table	11
53341	52520	CDS
			locus_tag	Hyperionvirus_5_64
			product	hypothetical protein
			transl_table	11
54217	53405	CDS
			locus_tag	Hyperionvirus_5_65
			product	hypothetical protein
			transl_table	11
54600	57734	CDS
			locus_tag	Hyperionvirus_5_66
			product	DEXDc helicase
			transl_table	11
58232	58113	CDS
			locus_tag	Hyperionvirus_5_67
			product	hypothetical protein
			transl_table	11
59676	58240	CDS
			locus_tag	Hyperionvirus_5_68
			product	chromosome condensation regulator RCC1
			transl_table	11
59862	60044	CDS
			locus_tag	Hyperionvirus_5_69
			product	hypothetical protein
			transl_table	11
60197	60087	CDS
			locus_tag	Hyperionvirus_5_70
			product	hypothetical protein
			transl_table	11
60227	60850	CDS
			locus_tag	Hyperionvirus_5_71
			product	hypothetical protein KFL_008760020
			transl_table	11
60923	62125	CDS
			locus_tag	Hyperionvirus_5_72
			product	hypothetical protein
			transl_table	11
62208	63032	CDS
			locus_tag	Hyperionvirus_5_73
			product	hypothetical protein
			transl_table	11
63089	63622	CDS
			locus_tag	Hyperionvirus_5_74
			product	hypothetical protein
			transl_table	11
64878	63619	CDS
			locus_tag	Hyperionvirus_5_75
			product	WD40 repeat-containing protein
			transl_table	11
65322	66467	CDS
			locus_tag	Hyperionvirus_5_76
			product	hypothetical protein
			transl_table	11
66838	66473	CDS
			locus_tag	Hyperionvirus_5_77
			product	hypothetical protein
			transl_table	11
67365	66874	CDS
			locus_tag	Hyperionvirus_5_78
			product	hypothetical protein
			transl_table	11
67485	67577	CDS
			locus_tag	Hyperionvirus_5_79
			product	hypothetical protein
			transl_table	11
67682	68101	CDS
			locus_tag	Hyperionvirus_5_80
			product	hypothetical protein
			transl_table	11
68104	68931	CDS
			locus_tag	Hyperionvirus_5_81
			product	hypothetical protein
			transl_table	11
69993	69400	CDS
			locus_tag	Hyperionvirus_5_82
			product	hypothetical protein
			transl_table	11
70065	71246	CDS
			locus_tag	Hyperionvirus_5_83
			product	hypothetical protein
			transl_table	11
71295	72023	CDS
			locus_tag	Hyperionvirus_5_84
			product	hypothetical protein
			transl_table	11
72395	72607	CDS
			locus_tag	Hyperionvirus_5_85
			product	hypothetical protein
			transl_table	11
74315	72591	CDS
			locus_tag	Hyperionvirus_5_86
			product	hypothetical protein PILCRDRAFT_826774
			transl_table	11
74422	76266	CDS
			locus_tag	Hyperionvirus_5_87
			product	hypothetical protein
			transl_table	11
76896	76273	CDS
			locus_tag	Hyperionvirus_5_88
			product	hypothetical protein
			transl_table	11
77068	76913	CDS
			locus_tag	Hyperionvirus_5_89
			product	hypothetical protein
			transl_table	11
77461	77216	CDS
			locus_tag	Hyperionvirus_5_90
			product	hypothetical protein
			transl_table	11
77540	78067	CDS
			locus_tag	Hyperionvirus_5_91
			product	hypothetical protein
			transl_table	11
78135	78377	CDS
			locus_tag	Hyperionvirus_5_92
			product	hypothetical protein
			transl_table	11
80149	78455	CDS
			locus_tag	Hyperionvirus_5_93
			product	hypothetical protein PILCRDRAFT_826774
			transl_table	11
81382	80213	CDS
			locus_tag	Hyperionvirus_5_94
			product	double-stranded RNA binding motif-containing protein
			transl_table	11
81508	82131	CDS
			locus_tag	Hyperionvirus_5_95
			product	hypothetical protein
			transl_table	11
82186	82794	CDS
			locus_tag	Hyperionvirus_5_96
			product	hypothetical protein
			transl_table	11
82860	83231	CDS
			locus_tag	Hyperionvirus_5_97
			product	hypothetical protein
			transl_table	11
84338	83199	CDS
			locus_tag	Hyperionvirus_5_98
			product	hypothetical protein
			transl_table	11
84622	84392	CDS
			locus_tag	Hyperionvirus_5_99
			product	hypothetical protein
			transl_table	11
84810	85754	CDS
			locus_tag	Hyperionvirus_5_100
			product	hypothetical protein
			transl_table	11
86747	85758	CDS
			locus_tag	Hyperionvirus_5_101
			product	hypothetical protein
			transl_table	11
87690	86812	CDS
			locus_tag	Hyperionvirus_5_102
			product	hypothetical protein
			transl_table	11
87809	89260	CDS
			locus_tag	Hyperionvirus_5_103
			product	hypothetical protein
			transl_table	11
89302	89640	CDS
			locus_tag	Hyperionvirus_5_104
			product	hypothetical protein
			transl_table	11
89676	90626	CDS
			locus_tag	Hyperionvirus_5_105
			product	DUF1152 domain-containing protein
			transl_table	11
90623	92062	CDS
			locus_tag	Hyperionvirus_5_106
			product	hypothetical protein
			transl_table	11
92118	92858	CDS
			locus_tag	Hyperionvirus_5_107
			product	hypothetical protein
			transl_table	11
93001	93777	CDS
			locus_tag	Hyperionvirus_5_108
			product	hypothetical protein
			transl_table	11
94530	93973	CDS
			locus_tag	Hyperionvirus_5_109
			product	hypothetical protein
			transl_table	11
94937	94593	CDS
			locus_tag	Hyperionvirus_5_110
			product	cysteine methyltransferase
			transl_table	11
95235	95411	CDS
			locus_tag	Hyperionvirus_5_111
			product	hypothetical protein
			transl_table	11
97130	95637	CDS
			locus_tag	Hyperionvirus_5_112
			product	hypothetical protein
			transl_table	11
97253	97771	CDS
			locus_tag	Hyperionvirus_5_113
			product	hypothetical protein
			transl_table	11
97832	98185	CDS
			locus_tag	Hyperionvirus_5_114
			product	hypothetical protein
			transl_table	11
98245	98571	CDS
			locus_tag	Hyperionvirus_5_115
			product	hypothetical protein
			transl_table	11
98846	98619	CDS
			locus_tag	Hyperionvirus_5_116
			product	hypothetical protein
			transl_table	11
100524	99037	CDS
			locus_tag	Hyperionvirus_5_117
			product	hypothetical protein
			transl_table	11
100661	101248	CDS
			locus_tag	Hyperionvirus_5_118
			product	hypothetical protein
			transl_table	11
102972	101245	CDS
			locus_tag	Hyperionvirus_5_119
			product	hypothetical protein SAMN04489710_105334
			transl_table	11
103070	104449	CDS
			locus_tag	Hyperionvirus_5_120
			product	hypothetical protein
			transl_table	11
104507	105097	CDS
			locus_tag	Hyperionvirus_5_121
			product	hypothetical protein SE17_33960
			transl_table	11
106581	105166	CDS
			locus_tag	Hyperionvirus_5_122
			product	HNH endonuclease
			transl_table	11
106815	109379	CDS
			locus_tag	Hyperionvirus_5_123
			product	hypothetical protein
			transl_table	11
109604	109494	CDS
			locus_tag	Hyperionvirus_5_124
			product	hypothetical protein
			transl_table	11
109902	109795	CDS
			locus_tag	Hyperionvirus_5_125
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_6
913	35	CDS
			locus_tag	Hyperionvirus_6_1
			product	hypothetical protein
			transl_table	11
997	2361	CDS
			locus_tag	Hyperionvirus_6_2
			product	hypothetical protein
			transl_table	11
2834	2712	CDS
			locus_tag	Hyperionvirus_6_3
			product	hypothetical protein
			transl_table	11
2966	2835	CDS
			locus_tag	Hyperionvirus_6_4
			product	hypothetical protein
			transl_table	11
3324	3001	CDS
			locus_tag	Hyperionvirus_6_5
			product	hypothetical protein
			transl_table	11
3511	3966	CDS
			locus_tag	Hyperionvirus_6_6
			product	hypothetical protein
			transl_table	11
4294	4004	CDS
			locus_tag	Hyperionvirus_6_7
			product	hypothetical protein
			transl_table	11
4628	4329	CDS
			locus_tag	Hyperionvirus_6_8
			product	hypothetical protein
			transl_table	11
6723	4822	CDS
			locus_tag	Hyperionvirus_6_9
			product	DUF262 domain-containing protein
			transl_table	11
7680	6802	CDS
			locus_tag	Hyperionvirus_6_10
			product	hypothetical protein
			transl_table	11
7809	8978	CDS
			locus_tag	Hyperionvirus_6_11
			product	hypothetical protein
			transl_table	11
9035	10477	CDS
			locus_tag	Hyperionvirus_6_12
			product	hypothetical protein
			transl_table	11
11133	10483	CDS
			locus_tag	Hyperionvirus_6_13
			product	GIY-YIG nuclease
			transl_table	11
11601	11738	CDS
			locus_tag	Hyperionvirus_6_14
			product	hypothetical protein
			transl_table	11
12512	11871	CDS
			locus_tag	Hyperionvirus_6_15
			product	hypothetical protein
			transl_table	11
12700	12557	CDS
			locus_tag	Hyperionvirus_6_16
			product	hypothetical protein
			transl_table	11
12837	13088	CDS
			locus_tag	Hyperionvirus_6_17
			product	hypothetical protein
			transl_table	11
15451	13406	CDS
			locus_tag	Hyperionvirus_6_18
			product	hypothetical protein Indivirus_1_31
			transl_table	11
15758	15630	CDS
			locus_tag	Hyperionvirus_6_19
			product	hypothetical protein
			transl_table	11
16321	15722	CDS
			locus_tag	Hyperionvirus_6_20
			product	HTH and integrase core domain protein
			transl_table	11
16620	17408	CDS
			locus_tag	Hyperionvirus_6_21
			product	hypothetical protein
			transl_table	11
17458	18612	CDS
			locus_tag	Hyperionvirus_6_22
			product	hypothetical protein
			transl_table	11
19325	18609	CDS
			locus_tag	Hyperionvirus_6_23
			product	hypothetical protein
			transl_table	11
19456	20721	CDS
			locus_tag	Hyperionvirus_6_24
			product	hypothetical protein
			transl_table	11
21643	20750	CDS
			locus_tag	Hyperionvirus_6_25
			product	hypothetical protein
			transl_table	11
21888	22454	CDS
			locus_tag	Hyperionvirus_6_26
			product	hypothetical protein
			transl_table	11
23723	22503	CDS
			locus_tag	Hyperionvirus_6_27
			product	RCC1-like G exchanging factor-like protein
			transl_table	11
23848	25296	CDS
			locus_tag	Hyperionvirus_6_28
			product	chromosome condensation regulator, partial
			transl_table	11
26678	25407	CDS
			locus_tag	Hyperionvirus_6_29
			product	regulator of chromosome condensation (RCC1)-like protein
			transl_table	11
27714	26701	CDS
			locus_tag	Hyperionvirus_6_30
			product	E3 ubiquitin-protein ligase TRIM41-like isoform X2
			transl_table	11
27842	29104	CDS
			locus_tag	Hyperionvirus_6_31
			product	hypothetical protein
			transl_table	11
29198	29359	CDS
			locus_tag	Hyperionvirus_6_32
			product	hypothetical protein
			transl_table	11
29404	29991	CDS
			locus_tag	Hyperionvirus_6_33
			product	hypothetical protein
			transl_table	11
30298	30182	CDS
			locus_tag	Hyperionvirus_6_34
			product	hypothetical protein
			transl_table	11
30535	31470	CDS
			locus_tag	Hyperionvirus_6_35
			product	hypothetical protein
			transl_table	11
32094	31471	CDS
			locus_tag	Hyperionvirus_6_36
			product	hypothetical protein
			transl_table	11
33179	32151	CDS
			locus_tag	Hyperionvirus_6_37
			product	hypothetical protein
			transl_table	11
33831	33226	CDS
			locus_tag	Hyperionvirus_6_38
			product	hypothetical protein
			transl_table	11
34529	33936	CDS
			locus_tag	Hyperionvirus_6_39
			product	hypothetical protein
			transl_table	11
34750	35115	CDS
			locus_tag	Hyperionvirus_6_40
			product	aldehyde-activating protein
			transl_table	11
35142	35792	CDS
			locus_tag	Hyperionvirus_6_41
			product	hypothetical protein
			transl_table	11
35843	36199	CDS
			locus_tag	Hyperionvirus_6_42
			product	hypothetical protein
			transl_table	11
38010	36241	CDS
			locus_tag	Hyperionvirus_6_43
			product	hypothetical protein
			transl_table	11
38555	38070	CDS
			locus_tag	Hyperionvirus_6_44
			product	hypothetical protein
			transl_table	11
39124	38621	CDS
			locus_tag	Hyperionvirus_6_45
			product	hypothetical protein
			transl_table	11
40239	39205	CDS
			locus_tag	Hyperionvirus_6_46
			product	hypothetical protein
			transl_table	11
40621	40250	CDS
			locus_tag	Hyperionvirus_6_47
			product	hypothetical protein
			transl_table	11
41416	40700	CDS
			locus_tag	Hyperionvirus_6_48
			product	hypothetical protein
			transl_table	11
41547	42809	CDS
			locus_tag	Hyperionvirus_6_49
			product	hypothetical protein
			transl_table	11
42871	43155	CDS
			locus_tag	Hyperionvirus_6_50
			product	hypothetical protein
			transl_table	11
43393	43217	CDS
			locus_tag	Hyperionvirus_6_51
			product	hypothetical protein
			transl_table	11
43399	43629	CDS
			locus_tag	Hyperionvirus_6_52
			product	hypothetical protein
			transl_table	11
43722	44744	CDS
			locus_tag	Hyperionvirus_6_53
			product	putative endonuclease
			transl_table	11
44959	44741	CDS
			locus_tag	Hyperionvirus_6_54
			product	hypothetical protein
			transl_table	11
45872	45021	CDS
			locus_tag	Hyperionvirus_6_55
			product	hypothetical protein
			transl_table	11
46016	47476	CDS
			locus_tag	Hyperionvirus_6_56
			product	hypothetical protein
			transl_table	11
47536	47916	CDS
			locus_tag	Hyperionvirus_6_57
			product	hypothetical protein
			transl_table	11
47989	49098	CDS
			locus_tag	Hyperionvirus_6_58
			product	hypothetical protein
			transl_table	11
50295	49105	CDS
			locus_tag	Hyperionvirus_6_59
			product	hypothetical protein
			transl_table	11
51308	50313	CDS
			locus_tag	Hyperionvirus_6_60
			product	hypothetical protein
			transl_table	11
51410	52318	CDS
			locus_tag	Hyperionvirus_6_61
			product	hypothetical protein
			transl_table	11
53528	52359	CDS
			locus_tag	Hyperionvirus_6_62
			product	hypothetical protein
			transl_table	11
53816	53586	CDS
			locus_tag	Hyperionvirus_6_63
			product	hypothetical protein
			transl_table	11
55216	53921	CDS
			locus_tag	Hyperionvirus_6_64
			product	restriction endonuclease
			transl_table	11
55843	55601	CDS
			locus_tag	Hyperionvirus_6_65
			product	hypothetical protein
			transl_table	11
56548	55913	CDS
			locus_tag	Hyperionvirus_6_66
			product	hypothetical protein
			transl_table	11
56882	57157	CDS
			locus_tag	Hyperionvirus_6_67
			product	hypothetical protein
			transl_table	11
57836	57246	CDS
			locus_tag	Hyperionvirus_6_68
			product	hypothetical protein
			transl_table	11
57942	58562	CDS
			locus_tag	Hyperionvirus_6_69
			product	hypothetical protein
			transl_table	11
59083	58565	CDS
			locus_tag	Hyperionvirus_6_70
			product	hypothetical protein
			transl_table	11
59829	59224	CDS
			locus_tag	Hyperionvirus_6_71
			product	hypothetical protein
			transl_table	11
60077	59886	CDS
			locus_tag	Hyperionvirus_6_72
			product	hypothetical protein
			transl_table	11
60924	60154	CDS
			locus_tag	Hyperionvirus_6_73
			product	hypothetical protein
			transl_table	11
61110	61355	CDS
			locus_tag	Hyperionvirus_6_74
			product	hypothetical protein
			transl_table	11
61352	62008	CDS
			locus_tag	Hyperionvirus_6_75
			product	hypothetical protein
			transl_table	11
62078	62410	CDS
			locus_tag	Hyperionvirus_6_76
			product	AAA domain protein
			transl_table	11
62557	62694	CDS
			locus_tag	Hyperionvirus_6_77
			product	hypothetical protein
			transl_table	11
64412	62736	CDS
			locus_tag	Hyperionvirus_6_78
			product	hypothetical protein
			transl_table	11
65950	64496	CDS
			locus_tag	Hyperionvirus_6_79
			product	hypothetical protein
			transl_table	11
66395	66024	CDS
			locus_tag	Hyperionvirus_6_80
			product	hypothetical protein
			transl_table	11
67144	66479	CDS
			locus_tag	Hyperionvirus_6_81
			product	hypothetical protein
			transl_table	11
67635	67793	CDS
			locus_tag	Hyperionvirus_6_82
			product	hypothetical protein
			transl_table	11
69975	67993	CDS
			locus_tag	Hyperionvirus_6_83
			product	dynamin family GTPase
			transl_table	11
70125	71231	CDS
			locus_tag	Hyperionvirus_6_84
			product	chromosome condensation regulator
			transl_table	11
71273	72061	CDS
			locus_tag	Hyperionvirus_6_85
			product	hypothetical protein
			transl_table	11
72657	72058	CDS
			locus_tag	Hyperionvirus_6_86
			product	hypothetical protein
			transl_table	11
73457	72711	CDS
			locus_tag	Hyperionvirus_6_87
			product	hypothetical protein
			transl_table	11
74028	73732	CDS
			locus_tag	Hyperionvirus_6_88
			product	hypothetical protein
			transl_table	11
75324	74086	CDS
			locus_tag	Hyperionvirus_6_89
			product	hypothetical protein
			transl_table	11
75457	76401	CDS
			locus_tag	Hyperionvirus_6_90
			product	hypothetical protein
			transl_table	11
76568	76398	CDS
			locus_tag	Hyperionvirus_6_91
			product	hypothetical protein
			transl_table	11
77487	76651	CDS
			locus_tag	Hyperionvirus_6_92
			product	hypothetical protein
			transl_table	11
79678	77516	CDS
			locus_tag	Hyperionvirus_6_93
			product	hypothetical protein UW31_C0007G0013
			transl_table	11
79801	80274	CDS
			locus_tag	Hyperionvirus_6_94
			product	hypothetical protein
			transl_table	11
80317	80568	CDS
			locus_tag	Hyperionvirus_6_95
			product	hypothetical protein
			transl_table	11
81209	80565	CDS
			locus_tag	Hyperionvirus_6_96
			product	hypothetical protein
			transl_table	11
81428	81949	CDS
			locus_tag	Hyperionvirus_6_97
			product	hypothetical protein
			transl_table	11
82038	83798	CDS
			locus_tag	Hyperionvirus_6_98
			product	DUF229 domain-containing protein
			transl_table	11
84464	83781	CDS
			locus_tag	Hyperionvirus_6_99
			product	hypothetical protein
			transl_table	11
84614	85744	CDS
			locus_tag	Hyperionvirus_6_100
			product	hypothetical protein
			transl_table	11
86600	85761	CDS
			locus_tag	Hyperionvirus_6_101
			product	hypothetical protein
			transl_table	11
86809	86988	CDS
			locus_tag	Hyperionvirus_6_102
			product	hypothetical protein
			transl_table	11
86994	87197	CDS
			locus_tag	Hyperionvirus_6_103
			product	hypothetical protein
			transl_table	11
87321	87620	CDS
			locus_tag	Hyperionvirus_6_104
			product	hypothetical protein
			transl_table	11
87598	89187	CDS
			locus_tag	Hyperionvirus_6_105
			product	hypothetical protein
			transl_table	11
89352	90074	CDS
			locus_tag	Hyperionvirus_6_106
			product	hypothetical protein
			transl_table	11
90546	90136	CDS
			locus_tag	Hyperionvirus_6_107
			product	hypothetical protein
			transl_table	11
93184	90776	CDS
			locus_tag	Hyperionvirus_6_108
			product	class II histone deacetylase
			transl_table	11
93521	93393	CDS
			locus_tag	Hyperionvirus_6_109
			product	hypothetical protein
			transl_table	11
94540	94725	CDS
			locus_tag	Hyperionvirus_6_110
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_7
57361	57432	tRNA	Hyperionvirus_tRNA_1
			product	tRNA-Arg(TCT)
57288	57358	tRNA	Hyperionvirus_tRNA_2
			product	tRNA-Thr(TGT)
187	2	CDS
			locus_tag	Hyperionvirus_7_1
			product	hypothetical protein
			transl_table	11
277	738	CDS
			locus_tag	Hyperionvirus_7_2
			product	PAS domain S-box protein
			transl_table	11
1669	1469	CDS
			locus_tag	Hyperionvirus_7_3
			product	hypothetical protein
			transl_table	11
1719	2351	CDS
			locus_tag	Hyperionvirus_7_4
			product	hypothetical protein
			transl_table	11
2416	3345	CDS
			locus_tag	Hyperionvirus_7_5
			product	BA75_02765T0
			transl_table	11
3400	4203	CDS
			locus_tag	Hyperionvirus_7_6
			product	hypothetical protein
			transl_table	11
4867	4190	CDS
			locus_tag	Hyperionvirus_7_7
			product	Transmembrane domain-containing protein
			transl_table	11
5001	5219	CDS
			locus_tag	Hyperionvirus_7_8
			product	hypothetical protein
			transl_table	11
5935	5216	CDS
			locus_tag	Hyperionvirus_7_9
			product	hypothetical protein BQ9231_00085
			transl_table	11
7462	5948	CDS
			locus_tag	Hyperionvirus_7_10
			product	hypothetical protein
			transl_table	11
7576	9147	CDS
			locus_tag	Hyperionvirus_7_11
			product	LL-diaminopimelate aminotransferase
			transl_table	11
10070	9150	CDS
			locus_tag	Hyperionvirus_7_12
			product	hypothetical protein
			transl_table	11
10870	10139	CDS
			locus_tag	Hyperionvirus_7_13
			product	hypothetical protein
			transl_table	11
11501	10926	CDS
			locus_tag	Hyperionvirus_7_14
			product	hypothetical protein
			transl_table	11
11558	13840	CDS
			locus_tag	Hyperionvirus_7_15
			product	hypothetical protein
			transl_table	11
13887	14129	CDS
			locus_tag	Hyperionvirus_7_16
			product	polyubiquitin, partial
			transl_table	11
14491	14126	CDS
			locus_tag	Hyperionvirus_7_17
			product	peptidyl-tRNA hydrolase
			transl_table	11
18815	14553	CDS
			locus_tag	Hyperionvirus_7_18
			product	ankyrin repeat protein
			transl_table	11
18900	19793	CDS
			locus_tag	Hyperionvirus_7_19
			product	hypothetical protein
			transl_table	11
21129	19777	CDS
			locus_tag	Hyperionvirus_7_20
			product	hypothetical protein
			transl_table	11
22262	21150	CDS
			locus_tag	Hyperionvirus_7_21
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
23134	22301	CDS
			locus_tag	Hyperionvirus_7_22
			product	hypothetical protein SORBI_3006G008200
			transl_table	11
23181	24110	CDS
			locus_tag	Hyperionvirus_7_23
			product	hypothetical protein
			transl_table	11
24171	24707	CDS
			locus_tag	Hyperionvirus_7_24
			product	hypothetical protein
			transl_table	11
26546	24708	CDS
			locus_tag	Hyperionvirus_7_25
			product	M13 family peptidase
			transl_table	11
26669	26995	CDS
			locus_tag	Hyperionvirus_7_26
			product	hypothetical protein
			transl_table	11
27651	26992	CDS
			locus_tag	Hyperionvirus_7_27
			product	hypothetical protein
			transl_table	11
28599	27667	CDS
			locus_tag	Hyperionvirus_7_28
			product	hypothetical protein
			transl_table	11
29373	28645	CDS
			locus_tag	Hyperionvirus_7_29
			product	hypothetical protein
			transl_table	11
29685	29407	CDS
			locus_tag	Hyperionvirus_7_30
			product	hypothetical protein N838_18710
			transl_table	11
29816	29932	CDS
			locus_tag	Hyperionvirus_7_31
			product	hypothetical protein
			transl_table	11
30251	31375	CDS
			locus_tag	Hyperionvirus_7_32
			product	hypothetical protein
			transl_table	11
31913	31380	CDS
			locus_tag	Hyperionvirus_7_33
			product	hypothetical protein
			transl_table	11
32203	32379	CDS
			locus_tag	Hyperionvirus_7_34
			product	hypothetical protein
			transl_table	11
33307	32369	CDS
			locus_tag	Hyperionvirus_7_35
			product	hypothetical protein
			transl_table	11
34174	33410	CDS
			locus_tag	Hyperionvirus_7_36
			product	hypothetical protein
			transl_table	11
34892	34242	CDS
			locus_tag	Hyperionvirus_7_37
			product	hypothetical protein
			transl_table	11
35017	34928	CDS
			locus_tag	Hyperionvirus_7_38
			product	hypothetical protein
			transl_table	11
35027	36316	CDS
			locus_tag	Hyperionvirus_7_39
			product	hypothetical protein
			transl_table	11
36727	36521	CDS
			locus_tag	Hyperionvirus_7_40
			product	hypothetical protein
			transl_table	11
37477	36794	CDS
			locus_tag	Hyperionvirus_7_41
			product	hypothetical protein
			transl_table	11
37923	37501	CDS
			locus_tag	Hyperionvirus_7_42
			product	E3 ubiquitin-protein ligase RNF8-like
			transl_table	11
38045	38941	CDS
			locus_tag	Hyperionvirus_7_43
			product	hypothetical protein
			transl_table	11
39269	39781	CDS
			locus_tag	Hyperionvirus_7_44
			product	uncharacterized protein LOC111124169
			transl_table	11
41622	39868	CDS
			locus_tag	Hyperionvirus_7_45
			product	hypothetical protein EMIHUDRAFT_447006
			transl_table	11
42967	41645	CDS
			locus_tag	Hyperionvirus_7_46
			product	hypothetical protein
			transl_table	11
44186	43029	CDS
			locus_tag	Hyperionvirus_7_47
			product	sel1 repeat family protein
			transl_table	11
45167	44277	CDS
			locus_tag	Hyperionvirus_7_48
			product	hypothetical protein
			transl_table	11
46201	45215	CDS
			locus_tag	Hyperionvirus_7_49
			product	hypothetical protein
			transl_table	11
46308	46832	CDS
			locus_tag	Hyperionvirus_7_50
			product	hypothetical protein
			transl_table	11
46893	47186	CDS
			locus_tag	Hyperionvirus_7_51
			product	hypothetical protein
			transl_table	11
48224	47187	CDS
			locus_tag	Hyperionvirus_7_52
			product	AAA family ATPase
			transl_table	11
49684	48272	CDS
			locus_tag	Hyperionvirus_7_53
			product	hypothetical protein
			transl_table	11
49815	50450	CDS
			locus_tag	Hyperionvirus_7_54
			product	hypothetical protein
			transl_table	11
50504	51856	CDS
			locus_tag	Hyperionvirus_7_55
			product	hypothetical protein
			transl_table	11
51870	52415	CDS
			locus_tag	Hyperionvirus_7_56
			product	hypothetical protein
			transl_table	11
53720	52407	CDS
			locus_tag	Hyperionvirus_7_57
			product	hypothetical protein crov256
			transl_table	11
55178	53742	CDS
			locus_tag	Hyperionvirus_7_58
			product	glycosyltransferase
			transl_table	11
55395	56570	CDS
			locus_tag	Hyperionvirus_7_59
			product	hypothetical protein
			transl_table	11
57198	56578	CDS
			locus_tag	Hyperionvirus_7_60
			product	hypothetical protein
			transl_table	11
57579	58619	CDS
			locus_tag	Hyperionvirus_7_61
			product	hypothetical protein BACPEC_02332
			transl_table	11
60755	58623	CDS
			locus_tag	Hyperionvirus_7_62
			product	glycosyltransferase family 2
			transl_table	11
62051	60780	CDS
			locus_tag	Hyperionvirus_7_63
			product	UDP-N-acetylglucosamine 2-epimerase
			transl_table	11
62089	63933	CDS
			locus_tag	Hyperionvirus_7_64
			product	glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
64798	63992	CDS
			locus_tag	Hyperionvirus_7_65
			product	glycosyltransferase family 25
			transl_table	11
66069	64927	CDS
			locus_tag	Hyperionvirus_7_66
			product	dTDP-6-deoxy-L-lyxo-4-hexulose reductase
			transl_table	11
67377	66400	CDS
			locus_tag	Hyperionvirus_7_67
			product	putative dTDP-d-glucose 4 6-dehydratase
			transl_table	11
67525	68634	CDS
			locus_tag	Hyperionvirus_7_68
			product	hypothetical protein
			transl_table	11
69774	68668	CDS
			locus_tag	Hyperionvirus_7_69
			product	NAD-dependent epimerase/dehydratase family protein
			transl_table	11
69898	71190	CDS
			locus_tag	Hyperionvirus_7_70
			product	dTDP-6-deoxy-L-lyxo-4-hexulose reductase
			transl_table	11
71203	71322	CDS
			locus_tag	Hyperionvirus_7_71
			product	hypothetical protein
			transl_table	11
71306	71857	CDS
			locus_tag	Hyperionvirus_7_72
			product	hypothetical protein
			transl_table	11
71964	72752	CDS
			locus_tag	Hyperionvirus_7_73
			product	hypothetical protein mc_463
			transl_table	11
72847	73815	CDS
			locus_tag	Hyperionvirus_7_74
			product	UDP-N-acetylglucosamine 4,6-dehydratase
			transl_table	11
73862	75256	CDS
			locus_tag	Hyperionvirus_7_75
			product	glycosyltransferase
			transl_table	11
75379	76665	CDS
			locus_tag	Hyperionvirus_7_76
			product	glycosyltransferase family 1 protein
			transl_table	11
76625	77878	CDS
			locus_tag	Hyperionvirus_7_77
			product	hypothetical protein
			transl_table	11
77904	79175	CDS
			locus_tag	Hyperionvirus_7_78
			product	FkbM family methyltransferase
			transl_table	11
80419	79178	CDS
			locus_tag	Hyperionvirus_7_79
			product	MULTISPECIES: glycosyltransferase
			transl_table	11
81518	80448	CDS
			locus_tag	Hyperionvirus_7_80
			product	hypothetical protein UCDDA912_g06304
			transl_table	11
82353	81595	CDS
			locus_tag	Hyperionvirus_7_81
			product	hypothetical protein
			transl_table	11
83263	82745	CDS
			locus_tag	Hyperionvirus_7_82
			product	hypothetical protein
			transl_table	11
83389	84147	CDS
			locus_tag	Hyperionvirus_7_83
			product	hypothetical protein I317_03093
			transl_table	11
84211	84942	CDS
			locus_tag	Hyperionvirus_7_84
			product	hypothetical protein
			transl_table	11
85788	84925	CDS
			locus_tag	Hyperionvirus_7_85
			product	hypothetical protein
			transl_table	11
87933	85846	CDS
			locus_tag	Hyperionvirus_7_86
			product	hypothetical protein
			transl_table	11
88219	88311	CDS
			locus_tag	Hyperionvirus_7_87
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_8
2761	2	CDS
			locus_tag	Hyperionvirus_8_1
			product	ankyrin and ring finger domain protein
			transl_table	11
2905	4806	CDS
			locus_tag	Hyperionvirus_8_2
			product	arginyl-tRNA synthetase
			transl_table	11
5522	4803	CDS
			locus_tag	Hyperionvirus_8_3
			product	translation initiation factor eIF-1A
			transl_table	11
6459	5575	CDS
			locus_tag	Hyperionvirus_8_4
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
6597	7280	CDS
			locus_tag	Hyperionvirus_8_5
			product	RNA recognition motif
			transl_table	11
8866	7277	CDS
			locus_tag	Hyperionvirus_8_6
			product	putative rrp44-like exonuclease
			transl_table	11
9854	8919	CDS
			locus_tag	Hyperionvirus_8_7
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
11194	9902	CDS
			locus_tag	Hyperionvirus_8_8
			product	translation initiation factor 4A-III
			transl_table	11
12277	11255	CDS
			locus_tag	Hyperionvirus_8_9
			product	hypothetical protein
			transl_table	11
12675	12319	CDS
			locus_tag	Hyperionvirus_8_10
			product	hypothetical protein Indivirus_2_66
			transl_table	11
12779	14458	CDS
			locus_tag	Hyperionvirus_8_11
			product	peptidase
			transl_table	11
15315	14455	CDS
			locus_tag	Hyperionvirus_8_12
			product	hypothetical protein
			transl_table	11
16895	15438	CDS
			locus_tag	Hyperionvirus_8_13
			product	beta-lactamase superfamily domain
			transl_table	11
17042	18079	CDS
			locus_tag	Hyperionvirus_8_14
			product	hypothetical protein Catovirus_2_122
			transl_table	11
19025	18069	CDS
			locus_tag	Hyperionvirus_8_15
			product	hypothetical protein
			transl_table	11
19875	19018	CDS
			locus_tag	Hyperionvirus_8_16
			product	hypothetical protein Catovirus_2_124
			transl_table	11
21800	19923	CDS
			locus_tag	Hyperionvirus_8_17
			product	hypothetical protein Indivirus_2_58
			transl_table	11
22227	21865	CDS
			locus_tag	Hyperionvirus_8_18
			product	hypothetical protein
			transl_table	11
23378	22224	CDS
			locus_tag	Hyperionvirus_8_19
			product	exodeoxyribonuclease VII large subunit
			transl_table	11
24188	23406	CDS
			locus_tag	Hyperionvirus_8_20
			product	alpha/beta hydrolase family protein
			transl_table	11
25215	24244	CDS
			locus_tag	Hyperionvirus_8_21
			product	lysophospholipid acyltransferase
			transl_table	11
25307	26392	CDS
			locus_tag	Hyperionvirus_8_22
			product	AMP-binding enzyme
			transl_table	11
26397	27314	CDS
			locus_tag	Hyperionvirus_8_23
			product	formamidopyrimidine-DNA glycosylase
			transl_table	11
27914	27318	CDS
			locus_tag	Hyperionvirus_8_24
			product	hypothetical protein Catovirus_2_132
			transl_table	11
28589	27960	CDS
			locus_tag	Hyperionvirus_8_25
			product	hypothetical protein Catovirus_2_133
			transl_table	11
30639	28615	CDS
			locus_tag	Hyperionvirus_8_26
			product	HrpA-like RNA helicase
			transl_table	11
30964	30692	CDS
			locus_tag	Hyperionvirus_8_27
			product	hypothetical protein
			transl_table	11
31250	30996	CDS
			locus_tag	Hyperionvirus_8_28
			product	acyl-CoA-binding protein
			transl_table	11
34607	31284	CDS
			locus_tag	Hyperionvirus_8_29
			product	isoleucyl-tRNA synthetase
			transl_table	11
34716	35387	CDS
			locus_tag	Hyperionvirus_8_30
			product	hypothetical protein Catovirus_2_148
			transl_table	11
35420	35800	CDS
			locus_tag	Hyperionvirus_8_31
			product	hypothetical protein
			transl_table	11
35883	36848	CDS
			locus_tag	Hyperionvirus_8_32
			product	hypothetical protein
			transl_table	11
37429	36854	CDS
			locus_tag	Hyperionvirus_8_33
			product	hypothetical protein
			transl_table	11
40241	37494	CDS
			locus_tag	Hyperionvirus_8_34
			product	Zn-dependent peptidase
			transl_table	11
40319	40765	CDS
			locus_tag	Hyperionvirus_8_35
			product	hypothetical protein Indivirus_2_29
			transl_table	11
42064	40742	CDS
			locus_tag	Hyperionvirus_8_36
			product	N-myristoyltransferase
			transl_table	11
42186	43067	CDS
			locus_tag	Hyperionvirus_8_37
			product	hypothetical protein Catovirus_2_154
			transl_table	11
43686	43033	CDS
			locus_tag	Hyperionvirus_8_38
			product	hypothetical protein
			transl_table	11
45746	43737	CDS
			locus_tag	Hyperionvirus_8_39
			product	replication factor C small subunit
			transl_table	11
45869	46459	CDS
			locus_tag	Hyperionvirus_8_40
			product	deoxynucleoside monophosphate kinase
			transl_table	11
46883	46428	CDS
			locus_tag	Hyperionvirus_8_41
			product	protein of unknown function DUF814
			transl_table	11
46886	47530	CDS
			locus_tag	Hyperionvirus_8_42
			product	hypothetical protein
			transl_table	11
48811	47537	CDS
			locus_tag	Hyperionvirus_8_43
			product	DnaJ domain protein
			transl_table	11
48899	48792	CDS
			locus_tag	Hyperionvirus_8_44
			product	hypothetical protein
			transl_table	11
48947	50485	CDS
			locus_tag	Hyperionvirus_8_45
			product	histidine--tRNA synthetase
			transl_table	11
51814	50450	CDS
			locus_tag	Hyperionvirus_8_46
			product	FLAP-like endonuclease XPG
			transl_table	11
51929	53632	CDS
			locus_tag	Hyperionvirus_8_47
			product	GTPase
			transl_table	11
55127	53757	CDS
			locus_tag	Hyperionvirus_8_48
			product	aspartate--tRNA ligase, cytoplasmic
			transl_table	11
56061	55183	CDS
			locus_tag	Hyperionvirus_8_49
			product	eukaryotic translation initiation factor 4G
			transl_table	11
59745	56149	CDS
			locus_tag	Hyperionvirus_8_50
			product	mRNA capping enzyme
			transl_table	11
59881	61434	CDS
			locus_tag	Hyperionvirus_8_51
			product	FtsJ-like methyltransferase
			transl_table	11
61896	61429	CDS
			locus_tag	Hyperionvirus_8_52
			product	hypothetical protein Catovirus_2_175
			transl_table	11
62857	61928	CDS
			locus_tag	Hyperionvirus_8_53
			product	leucine-rich repeat protein
			transl_table	11
64138	62879	CDS
			locus_tag	Hyperionvirus_8_54
			product	hypothetical protein BMW23_0903
			transl_table	11
65275	64187	CDS
			locus_tag	Hyperionvirus_8_55
			product	NUDIX hydrolase
			transl_table	11
66073	65324	CDS
			locus_tag	Hyperionvirus_8_56
			product	DNA-directed RNA polymerase, subunit E'/Rpb9
			transl_table	11
71082	66109	CDS
			locus_tag	Hyperionvirus_8_57
			product	SNF2-like helicase
			transl_table	11
71175	71675	CDS
			locus_tag	Hyperionvirus_8_58
			product	hypothetical protein Catovirus_2_178
			transl_table	11
71688	73928	CDS
			locus_tag	Hyperionvirus_8_59
			product	ankyrin repeat protein
			transl_table	11
74681	74355	CDS
			locus_tag	Hyperionvirus_8_60
			product	hypothetical protein
			transl_table	11
76090	74738	CDS
			locus_tag	Hyperionvirus_8_61
			product	hypothetical protein Klosneuvirus_2_187
			transl_table	11
81293	76119	CDS
			locus_tag	Hyperionvirus_8_62
			product	HrpA-like RNA helicase
			transl_table	11
81573	82391	CDS
			locus_tag	Hyperionvirus_8_63
			product	hypothetical protein Catovirus_2_187
			transl_table	11
82415	82963	CDS
			locus_tag	Hyperionvirus_8_64
			product	hypothetical protein Catovirus_2_188
			transl_table	11
83741	82932	CDS
			locus_tag	Hyperionvirus_8_65
			product	hypothetical protein
			transl_table	11
84388	83756	CDS
			locus_tag	Hyperionvirus_8_66
			product	hypothetical protein Catovirus_2_189
			transl_table	11
85665	85754	CDS
			locus_tag	Hyperionvirus_8_67
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_9
39984	39913	tRNA	Hyperionvirus_tRNA_3
			product	tRNA-Pseudo(TAC)
40131	40059	tRNA	Hyperionvirus_tRNA_4
			product	tRNA-Glu(TTC)
40355	40281	tRNA	Hyperionvirus_tRNA_5
			product	tRNA-Lys(TTT)
40501	40431	tRNA	Hyperionvirus_tRNA_6
			product	tRNA-Pseudo(GTG)
40574	40503	tRNA	Hyperionvirus_tRNA_7
			product	tRNA-Cys(GCA)
47976	47903	tRNA	Hyperionvirus_tRNA_8
			product	tRNA-Ile(TAT)
48287	48216	tRNA	Hyperionvirus_tRNA_9
			product	tRNA-Arg(ACG)
48435	48364	tRNA	Hyperionvirus_tRNA_10
			product	tRNA-Pseudo(CTT)
48510	48438	tRNA	Hyperionvirus_tRNA_11
			product	tRNA-Pseudo(GTA)
48623	48554	tRNA	Hyperionvirus_tRNA_12
			product	tRNA-Pseudo(TGC)
48699	48626	tRNA	Hyperionvirus_tRNA_13
			product	tRNA-Pseudo(CCA)
137	3	CDS
			locus_tag	Hyperionvirus_9_1
			product	hypothetical protein
			transl_table	11
784	182	CDS
			locus_tag	Hyperionvirus_9_2
			product	hypothetical protein
			transl_table	11
1956	823	CDS
			locus_tag	Hyperionvirus_9_3
			product	glycosyltransferase
			transl_table	11
2607	2002	CDS
			locus_tag	Hyperionvirus_9_4
			product	CDP-alcohol phosphatidyltransferase
			transl_table	11
3185	2940	CDS
			locus_tag	Hyperionvirus_9_5
			product	hypothetical protein
			transl_table	11
3184	4320	CDS
			locus_tag	Hyperionvirus_9_6
			product	glycyl-tRNA synthetase
			transl_table	11
4386	4799	CDS
			locus_tag	Hyperionvirus_9_7
			product	hypothetical protein
			transl_table	11
4853	5533	CDS
			locus_tag	Hyperionvirus_9_8
			product	hypothetical protein Klosneuvirus_1_362
			transl_table	11
5593	5501	CDS
			locus_tag	Hyperionvirus_9_9
			product	hypothetical protein
			transl_table	11
5592	6038	CDS
			locus_tag	Hyperionvirus_9_10
			product	hypothetical protein Catovirus_1_903
			transl_table	11
6094	6693	CDS
			locus_tag	Hyperionvirus_9_11
			product	RING finger and CHY zinc finger domain-containing protein 1 isoform X1
			transl_table	11
6713	7234	CDS
			locus_tag	Hyperionvirus_9_12
			product	HNH endonuclease
			transl_table	11
7599	7231	CDS
			locus_tag	Hyperionvirus_9_13
			product	hypothetical protein
			transl_table	11
7686	7952	CDS
			locus_tag	Hyperionvirus_9_14
			product	hypothetical protein
			transl_table	11
8616	7954	CDS
			locus_tag	Hyperionvirus_9_15
			product	hypothetical protein
			transl_table	11
9667	8654	CDS
			locus_tag	Hyperionvirus_9_16
			product	hypothetical protein Catovirus_1_898
			transl_table	11
9745	11622	CDS
			locus_tag	Hyperionvirus_9_17
			product	hypothetical protein Catovirus_1_897
			transl_table	11
12183	11623	CDS
			locus_tag	Hyperionvirus_9_18
			product	hypothetical protein
			transl_table	11
12323	12673	CDS
			locus_tag	Hyperionvirus_9_19
			product	hypothetical protein
			transl_table	11
12743	13492	CDS
			locus_tag	Hyperionvirus_9_20
			product	hypothetical protein
			transl_table	11
13539	13856	CDS
			locus_tag	Hyperionvirus_9_21
			product	hypothetical protein Indivirus_1_201
			transl_table	11
15331	13841	CDS
			locus_tag	Hyperionvirus_9_22
			product	putative AAA+ family ATPase
			transl_table	11
15436	15963	CDS
			locus_tag	Hyperionvirus_9_23
			product	hypothetical protein
			transl_table	11
16397	16218	CDS
			locus_tag	Hyperionvirus_9_24
			product	hypothetical protein
			transl_table	11
17782	16394	CDS
			locus_tag	Hyperionvirus_9_25
			product	DEAD/SNF2-like helicase
			transl_table	11
18576	17794	CDS
			locus_tag	Hyperionvirus_9_26
			product	Phosphoribosyl-ATP pyrophosphohydrolase-domain containing protein (two domains)
			transl_table	11
18820	18626	CDS
			locus_tag	Hyperionvirus_9_27
			product	hypothetical protein
			transl_table	11
19264	18884	CDS
			locus_tag	Hyperionvirus_9_28
			product	hypothetical protein
			transl_table	11
19684	19304	CDS
			locus_tag	Hyperionvirus_9_29
			product	hypothetical protein
			transl_table	11
19806	20597	CDS
			locus_tag	Hyperionvirus_9_30
			product	serine dehydrogenase proteinase
			transl_table	11
20651	21379	CDS
			locus_tag	Hyperionvirus_9_31
			product	hypothetical protein
			transl_table	11
21395	22078	CDS
			locus_tag	Hyperionvirus_9_32
			product	hypothetical protein Catovirus_1_886
			transl_table	11
22144	22641	CDS
			locus_tag	Hyperionvirus_9_33
			product	ubiquitin-conjugating enzyme
			transl_table	11
23158	22634	CDS
			locus_tag	Hyperionvirus_9_34
			product	hypothetical protein
			transl_table	11
23202	23351	CDS
			locus_tag	Hyperionvirus_9_35
			product	hypothetical protein
			transl_table	11
23400	26270	CDS
			locus_tag	Hyperionvirus_9_36
			product	ubiquitin-like modifier-activating enzyme 1 isoform X2
			transl_table	11
26330	29590	CDS
			locus_tag	Hyperionvirus_9_37
			product	hypothetical protein Klosneuvirus_1_394
			transl_table	11
30684	29587	CDS
			locus_tag	Hyperionvirus_9_38
			product	hypothetical protein
			transl_table	11
30763	31140	CDS
			locus_tag	Hyperionvirus_9_39
			product	protein disulfide isomerase
			transl_table	11
31186	31605	CDS
			locus_tag	Hyperionvirus_9_40
			product	hypothetical protein
			transl_table	11
31665	32180	CDS
			locus_tag	Hyperionvirus_9_41
			product	hybrid sensor histidine kinase/response regulator
			transl_table	11
34080	32167	CDS
			locus_tag	Hyperionvirus_9_42
			product	hybrid sensor histidine kinase/response regulator
			transl_table	11
34128	34508	CDS
			locus_tag	Hyperionvirus_9_43
			product	related to TRX2-thioredoxin II
			transl_table	11
35156	34509	CDS
			locus_tag	Hyperionvirus_9_44
			product	hypothetical protein
			transl_table	11
36540	35197	CDS
			locus_tag	Hyperionvirus_9_45
			product	hypothetical protein
			transl_table	11
36680	37330	CDS
			locus_tag	Hyperionvirus_9_46
			product	hypothetical protein
			transl_table	11
37434	38183	CDS
			locus_tag	Hyperionvirus_9_47
			product	hypothetical protein
			transl_table	11
38979	38173	CDS
			locus_tag	Hyperionvirus_9_48
			product	hypothetical protein ACA1_332300
			transl_table	11
39792	39100	CDS
			locus_tag	Hyperionvirus_9_49
			product	ubiquitin fusion degradation protein, putative
			transl_table	11
42043	40619	CDS
			locus_tag	Hyperionvirus_9_50
			product	hypothetical protein Klosneuvirus_1_400
			transl_table	11
42582	42788	CDS
			locus_tag	Hyperionvirus_9_51
			product	hypothetical protein
			transl_table	11
42917	43552	CDS
			locus_tag	Hyperionvirus_9_52
			product	hypothetical protein MegaChil _gp0136
			transl_table	11
43606	45357	CDS
			locus_tag	Hyperionvirus_9_53
			product	prolyl 4-hydroxylase alpha subunit
			transl_table	11
46866	45469	CDS
			locus_tag	Hyperionvirus_9_54
			product	hypothetical protein Klosneuvirus_1_400
			transl_table	11
47698	47444	CDS
			locus_tag	Hyperionvirus_9_55
			product	putative RNA-binding protein C17H9.04c
			transl_table	11
49096	49974	CDS
			locus_tag	Hyperionvirus_9_56
			product	hypothetical protein
			transl_table	11
50116	50748	CDS
			locus_tag	Hyperionvirus_9_57
			product	hypothetical protein
			transl_table	11
50887	53622	CDS
			locus_tag	Hyperionvirus_9_58
			product	hypothetical protein
			transl_table	11
54082	53957	CDS
			locus_tag	Hyperionvirus_9_59
			product	hypothetical protein
			transl_table	11
54481	54371	CDS
			locus_tag	Hyperionvirus_9_60
			product	hypothetical protein
			transl_table	11
54782	54633	CDS
			locus_tag	Hyperionvirus_9_61
			product	hypothetical protein
			transl_table	11
55403	55296	CDS
			locus_tag	Hyperionvirus_9_62
			product	hypothetical protein
			transl_table	11
55586	55410	CDS
			locus_tag	Hyperionvirus_9_63
			product	hypothetical protein
			transl_table	11
55777	56607	CDS
			locus_tag	Hyperionvirus_9_64
			product	polyADP-ribose polymerase
			transl_table	11
57249	56602	CDS
			locus_tag	Hyperionvirus_9_65
			product	hypothetical protein H311_03002
			transl_table	11
58049	57309	CDS
			locus_tag	Hyperionvirus_9_66
			product	Ras-related protein Rab-13
			transl_table	11
58184	58777	CDS
			locus_tag	Hyperionvirus_9_67
			product	Ras-like GTP-binding protein YPT1
			transl_table	11
58846	60036	CDS
			locus_tag	Hyperionvirus_9_68
			product	hypothetical protein
			transl_table	11
60408	60268	CDS
			locus_tag	Hyperionvirus_9_69
			product	hypothetical protein
			transl_table	11
60464	61321	CDS
			locus_tag	Hyperionvirus_9_70
			product	hypothetical protein
			transl_table	11
61376	61933	CDS
			locus_tag	Hyperionvirus_9_71
			product	hypothetical protein Catovirus_1_847
			transl_table	11
62364	61930	CDS
			locus_tag	Hyperionvirus_9_72
			product	I cysteine dioxygenase
			transl_table	11
62910	62410	CDS
			locus_tag	Hyperionvirus_9_73
			product	hypothetical protein
			transl_table	11
63512	62967	CDS
			locus_tag	Hyperionvirus_9_74
			product	small GTP-binding protein Rab28, putative
			transl_table	11
63588	64637	CDS
			locus_tag	Hyperionvirus_9_75
			product	Tpt1/KptA family RNA 2'-phosphotransferase
			transl_table	11
64741	65586	CDS
			locus_tag	Hyperionvirus_9_76
			product	hypothetical protein
			transl_table	11
65643	67145	CDS
			locus_tag	Hyperionvirus_9_77
			product	cysteinyl-tRNA synthetase
			transl_table	11
67206	69221	CDS
			locus_tag	Hyperionvirus_9_78
			product	hypothetical protein
			transl_table	11
69284	70864	CDS
			locus_tag	Hyperionvirus_9_79
			product	MULTISPECIES: valine--tRNA ligase
			transl_table	11
70827	71366	CDS
			locus_tag	Hyperionvirus_9_80
			product	hypothetical protein B6D56_07890
			transl_table	11
72204	71347	CDS
			locus_tag	Hyperionvirus_9_81
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_10
3	428	CDS
			locus_tag	Hyperionvirus_10_1
			product	hypothetical protein
			transl_table	11
465	851	CDS
			locus_tag	Hyperionvirus_10_2
			product	hypothetical protein
			transl_table	11
926	1918	CDS
			locus_tag	Hyperionvirus_10_3
			product	MULTISPECIES: TIGR03118 family protein
			transl_table	11
1964	3361	CDS
			locus_tag	Hyperionvirus_10_4
			product	hypothetical protein
			transl_table	11
3419	4669	CDS
			locus_tag	Hyperionvirus_10_5
			product	hypothetical protein
			transl_table	11
5452	4715	CDS
			locus_tag	Hyperionvirus_10_6
			product	hypothetical protein
			transl_table	11
6403	5591	CDS
			locus_tag	Hyperionvirus_10_7
			product	hypothetical protein
			transl_table	11
6868	6479	CDS
			locus_tag	Hyperionvirus_10_8
			product	chromosome condensation regulator
			transl_table	11
7864	6974	CDS
			locus_tag	Hyperionvirus_10_9
			product	chromosome condensation regulator
			transl_table	11
8092	8922	CDS
			locus_tag	Hyperionvirus_10_10
			product	hypothetical protein
			transl_table	11
9010	10212	CDS
			locus_tag	Hyperionvirus_10_11
			product	hypothetical protein
			transl_table	11
10266	10991	CDS
			locus_tag	Hyperionvirus_10_12
			product	hypothetical protein
			transl_table	11
11712	11005	CDS
			locus_tag	Hyperionvirus_10_13
			product	hypothetical protein
			transl_table	11
11825	12178	CDS
			locus_tag	Hyperionvirus_10_14
			product	hypothetical protein
			transl_table	11
13008	12673	CDS
			locus_tag	Hyperionvirus_10_15
			product	hypothetical protein
			transl_table	11
14087	17017	CDS
			locus_tag	Hyperionvirus_10_16
			product	superfamily II helicase
			transl_table	11
17248	18348	CDS
			locus_tag	Hyperionvirus_10_17
			product	hypothetical protein
			transl_table	11
19957	18317	CDS
			locus_tag	Hyperionvirus_10_18
			product	FAD-binding oxidoreductase
			transl_table	11
20634	20005	CDS
			locus_tag	Hyperionvirus_10_19
			product	hypothetical protein
			transl_table	11
21128	20724	CDS
			locus_tag	Hyperionvirus_10_20
			product	hypothetical protein
			transl_table	11
21338	22693	CDS
			locus_tag	Hyperionvirus_10_21
			product	hypothetical protein
			transl_table	11
22996	23328	CDS
			locus_tag	Hyperionvirus_10_22
			product	hypothetical protein
			transl_table	11
25732	25920	CDS
			locus_tag	Hyperionvirus_10_23
			product	hypothetical protein
			transl_table	11
26307	26456	CDS
			locus_tag	Hyperionvirus_10_24
			product	hypothetical protein
			transl_table	11
27021	27191	CDS
			locus_tag	Hyperionvirus_10_25
			product	hypothetical protein
			transl_table	11
29218	29048	CDS
			locus_tag	Hyperionvirus_10_26
			product	hypothetical protein
			transl_table	11
31159	34134	CDS
			locus_tag	Hyperionvirus_10_27
			product	UvrD/REP helicase
			transl_table	11
34321	35031	CDS
			locus_tag	Hyperionvirus_10_28
			product	hypothetical protein
			transl_table	11
35572	35192	CDS
			locus_tag	Hyperionvirus_10_29
			product	hypothetical protein
			transl_table	11
37073	37219	CDS
			locus_tag	Hyperionvirus_10_30
			product	hypothetical protein
			transl_table	11
38570	38734	CDS
			locus_tag	Hyperionvirus_10_31
			product	hypothetical protein
			transl_table	11
39068	38820	CDS
			locus_tag	Hyperionvirus_10_32
			product	hypothetical protein
			transl_table	11
39176	40222	CDS
			locus_tag	Hyperionvirus_10_33
			product	hypothetical protein SAMD00019534_123960
			transl_table	11
40671	40994	CDS
			locus_tag	Hyperionvirus_10_34
			product	hypothetical protein
			transl_table	11
41054	41812	CDS
			locus_tag	Hyperionvirus_10_35
			product	hypothetical protein
			transl_table	11
42342	41770	CDS
			locus_tag	Hyperionvirus_10_36
			product	hypothetical protein
			transl_table	11
43652	42477	CDS
			locus_tag	Hyperionvirus_10_37
			product	hypothetical protein
			transl_table	11
44341	43742	CDS
			locus_tag	Hyperionvirus_10_38
			product	hypothetical protein
			transl_table	11
45236	44418	CDS
			locus_tag	Hyperionvirus_10_39
			product	hypothetical protein
			transl_table	11
46642	45317	CDS
			locus_tag	Hyperionvirus_10_40
			product	hypothetical protein
			transl_table	11
47187	46699	CDS
			locus_tag	Hyperionvirus_10_41
			product	MULTISPECIES: nucleoside deaminase
			transl_table	11
47580	48077	CDS
			locus_tag	Hyperionvirus_10_42
			product	molecular chaperone DnaJ
			transl_table	11
48128	48898	CDS
			locus_tag	Hyperionvirus_10_43
			product	hypothetical protein
			transl_table	11
49064	49981	CDS
			locus_tag	Hyperionvirus_10_44
			product	hypothetical protein
			transl_table	11
50094	51593	CDS
			locus_tag	Hyperionvirus_10_45
			product	hypothetical protein RMCBS344292_00028
			transl_table	11
52366	51539	CDS
			locus_tag	Hyperionvirus_10_46
			product	hypothetical protein
			transl_table	11
52512	53366	CDS
			locus_tag	Hyperionvirus_10_47
			product	histidine phosphatase domain-containing protein
			transl_table	11
53450	54664	CDS
			locus_tag	Hyperionvirus_10_48
			product	hypothetical protein
			transl_table	11
54723	55796	CDS
			locus_tag	Hyperionvirus_10_49
			product	hypothetical protein
			transl_table	11
58273	55763	CDS
			locus_tag	Hyperionvirus_10_50
			product	Aminopeptidase N
			transl_table	11
58926	58339	CDS
			locus_tag	Hyperionvirus_10_51
			product	hypothetical protein
			transl_table	11
59041	60270	CDS
			locus_tag	Hyperionvirus_10_52
			product	hypothetical protein
			transl_table	11
60345	63296	CDS
			locus_tag	Hyperionvirus_10_53
			product	hypothetical protein
			transl_table	11
63439	65052	CDS
			locus_tag	Hyperionvirus_10_54
			product	hypothetical protein
			transl_table	11
65269	65054	CDS
			locus_tag	Hyperionvirus_10_55
			product	hypothetical protein
			transl_table	11
65413	65643	CDS
			locus_tag	Hyperionvirus_10_56
			product	hypothetical protein
			transl_table	11
65673	65918	CDS
			locus_tag	Hyperionvirus_10_57
			product	hypothetical protein
			transl_table	11
66519	65911	CDS
			locus_tag	Hyperionvirus_10_58
			product	hypothetical protein
			transl_table	11
66671	67381	CDS
			locus_tag	Hyperionvirus_10_59
			product	hypothetical protein
			transl_table	11
68327	67398	CDS
			locus_tag	Hyperionvirus_10_60
			product	hypothetical protein
			transl_table	11
68466	69155	CDS
			locus_tag	Hyperionvirus_10_61
			product	hypothetical protein
			transl_table	11
69209	69604	CDS
			locus_tag	Hyperionvirus_10_62
			product	hypothetical protein
			transl_table	11
69712	69816	CDS
			locus_tag	Hyperionvirus_10_63
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_11
1	699	CDS
			locus_tag	Hyperionvirus_11_1
			product	hypothetical protein
			transl_table	11
1606	2406	CDS
			locus_tag	Hyperionvirus_11_2
			product	hypothetical protein
			transl_table	11
2765	2430	CDS
			locus_tag	Hyperionvirus_11_3
			product	hypothetical protein
			transl_table	11
3201	3049	CDS
			locus_tag	Hyperionvirus_11_4
			product	hypothetical protein
			transl_table	11
3335	3982	CDS
			locus_tag	Hyperionvirus_11_5
			product	hypothetical protein
			transl_table	11
4047	5504	CDS
			locus_tag	Hyperionvirus_11_6
			product	hypothetical protein
			transl_table	11
5562	7013	CDS
			locus_tag	Hyperionvirus_11_7
			product	chromosome condensation regulator
			transl_table	11
8017	7022	CDS
			locus_tag	Hyperionvirus_11_8
			product	poly-gamma-glutamate synthesis protein (capsule biosynthesis protein)
			transl_table	11
8065	8433	CDS
			locus_tag	Hyperionvirus_11_9
			product	Glyoxylase, beta-lactamase superfamily II
			transl_table	11
8962	8660	CDS
			locus_tag	Hyperionvirus_11_10
			product	hypothetical protein
			transl_table	11
9192	9284	CDS
			locus_tag	Hyperionvirus_11_11
			product	hypothetical protein
			transl_table	11
9426	9647	CDS
			locus_tag	Hyperionvirus_11_12
			product	hypothetical protein
			transl_table	11
9953	10378	CDS
			locus_tag	Hyperionvirus_11_13
			product	hypothetical protein
			transl_table	11
10371	11135	CDS
			locus_tag	Hyperionvirus_11_14
			product	hypothetical protein
			transl_table	11
11829	11200	CDS
			locus_tag	Hyperionvirus_11_15
			product	hypothetical protein
			transl_table	11
11957	11829	CDS
			locus_tag	Hyperionvirus_11_16
			product	hypothetical protein
			transl_table	11
12083	13825	CDS
			locus_tag	Hyperionvirus_11_17
			product	hypothetical protein
			transl_table	11
13840	14562	CDS
			locus_tag	Hyperionvirus_11_18
			product	hypothetical protein
			transl_table	11
14897	15103	CDS
			locus_tag	Hyperionvirus_11_19
			product	hypothetical protein
			transl_table	11
16570	15977	CDS
			locus_tag	Hyperionvirus_11_20
			product	hypothetical protein
			transl_table	11
16700	18067	CDS
			locus_tag	Hyperionvirus_11_21
			product	WD40 repeat-like protein
			transl_table	11
18141	18941	CDS
			locus_tag	Hyperionvirus_11_22
			product	hypothetical protein
			transl_table	11
19285	21567	CDS
			locus_tag	Hyperionvirus_11_23
			product	predicted protein
			transl_table	11
22225	22073	CDS
			locus_tag	Hyperionvirus_11_24
			product	hypothetical protein
			transl_table	11
22348	23049	CDS
			locus_tag	Hyperionvirus_11_25
			product	hypothetical protein
			transl_table	11
23719	23006	CDS
			locus_tag	Hyperionvirus_11_26
			product	hypothetical protein
			transl_table	11
24564	23740	CDS
			locus_tag	Hyperionvirus_11_27
			product	uncharacterized protein
			transl_table	11
24715	26637	CDS
			locus_tag	Hyperionvirus_11_28
			product	hypothetical protein
			transl_table	11
26886	27992	CDS
			locus_tag	Hyperionvirus_11_29
			product	chromosome condensation regulator
			transl_table	11
29092	27998	CDS
			locus_tag	Hyperionvirus_11_30
			product	peptide chain release factor eRF1/aRF1
			transl_table	11
29251	29099	CDS
			locus_tag	Hyperionvirus_11_31
			product	hypothetical protein
			transl_table	11
29449	29282	CDS
			locus_tag	Hyperionvirus_11_32
			product	hypothetical protein
			transl_table	11
29720	29511	CDS
			locus_tag	Hyperionvirus_11_33
			product	hypothetical protein
			transl_table	11
30060	29902	CDS
			locus_tag	Hyperionvirus_11_34
			product	hypothetical protein
			transl_table	11
30047	30754	CDS
			locus_tag	Hyperionvirus_11_35
			product	hypothetical protein
			transl_table	11
30805	31659	CDS
			locus_tag	Hyperionvirus_11_36
			product	hypothetical protein
			transl_table	11
32424	31654	CDS
			locus_tag	Hyperionvirus_11_37
			product	hypothetical protein
			transl_table	11
32752	33153	CDS
			locus_tag	Hyperionvirus_11_38
			product	hypothetical protein
			transl_table	11
33215	35008	CDS
			locus_tag	Hyperionvirus_11_39
			product	hypothetical protein
			transl_table	11
35883	35005	CDS
			locus_tag	Hyperionvirus_11_40
			product	hypothetical protein
			transl_table	11
35968	35870	CDS
			locus_tag	Hyperionvirus_11_41
			product	hypothetical protein
			transl_table	11
36090	36749	CDS
			locus_tag	Hyperionvirus_11_42
			product	hypothetical protein EMIHUDRAFT_237699
			transl_table	11
37373	36990	CDS
			locus_tag	Hyperionvirus_11_43
			product	hypothetical protein
			transl_table	11
37540	37385	CDS
			locus_tag	Hyperionvirus_11_44
			product	hypothetical protein
			transl_table	11
38382	37741	CDS
			locus_tag	Hyperionvirus_11_45
			product	hypothetical protein
			transl_table	11
38566	38997	CDS
			locus_tag	Hyperionvirus_11_46
			product	hypothetical protein Klosneuvirus_3_141
			transl_table	11
39100	42003	CDS
			locus_tag	Hyperionvirus_11_47
			product	DNA primase
			transl_table	11
44287	42824	CDS
			locus_tag	Hyperionvirus_11_48
			product	Protein sel-1
			transl_table	11
45721	44288	CDS
			locus_tag	Hyperionvirus_11_49
			product	hypothetical protein
			transl_table	11
46056	46286	CDS
			locus_tag	Hyperionvirus_11_50
			product	hypothetical protein
			transl_table	11
46667	46299	CDS
			locus_tag	Hyperionvirus_11_51
			product	hypothetical protein
			transl_table	11
47825	46749	CDS
			locus_tag	Hyperionvirus_11_52
			product	hypothetical protein
			transl_table	11
47952	47863	CDS
			locus_tag	Hyperionvirus_11_53
			product	hypothetical protein
			transl_table	11
49241	47985	CDS
			locus_tag	Hyperionvirus_11_54
			product	chromosome condensation regulator
			transl_table	11
49668	49570	CDS
			locus_tag	Hyperionvirus_11_55
			product	hypothetical protein
			transl_table	11
49944	49810	CDS
			locus_tag	Hyperionvirus_11_56
			product	hypothetical protein
			transl_table	11
50109	49957	CDS
			locus_tag	Hyperionvirus_11_57
			product	hypothetical protein
			transl_table	11
50313	50113	CDS
			locus_tag	Hyperionvirus_11_58
			product	hypothetical protein
			transl_table	11
50573	51049	CDS
			locus_tag	Hyperionvirus_11_59
			product	hypothetical protein
			transl_table	11
51099	52169	CDS
			locus_tag	Hyperionvirus_11_60
			product	hypothetical protein
			transl_table	11
52596	52150	CDS
			locus_tag	Hyperionvirus_11_61
			product	hypothetical protein AK812_SmicGene6231
			transl_table	11
52944	54236	CDS
			locus_tag	Hyperionvirus_11_62
			product	hypothetical protein
			transl_table	11
54898	54239	CDS
			locus_tag	Hyperionvirus_11_63
			product	hypothetical protein
			transl_table	11
55929	54970	CDS
			locus_tag	Hyperionvirus_11_64
			product	hypothetical protein
			transl_table	11
55956	56489	CDS
			locus_tag	Hyperionvirus_11_65
			product	hypothetical protein
			transl_table	11
56675	57523	CDS
			locus_tag	Hyperionvirus_11_66
			product	hypothetical protein
			transl_table	11
57614	58930	CDS
			locus_tag	Hyperionvirus_11_67
			product	hypothetical protein
			transl_table	11
60132	58948	CDS
			locus_tag	Hyperionvirus_11_68
			product	hypothetical protein
			transl_table	11
60263	60982	CDS
			locus_tag	Hyperionvirus_11_69
			product	hypothetical protein
			transl_table	11
61121	61372	CDS
			locus_tag	Hyperionvirus_11_70
			product	hypothetical protein
			transl_table	11
61550	63934	CDS
			locus_tag	Hyperionvirus_11_71
			product	hypothetical protein BMW23_0613
			transl_table	11
64094	64876	CDS
			locus_tag	Hyperionvirus_11_72
			product	hypothetical protein
			transl_table	11
66251	64860	CDS
			locus_tag	Hyperionvirus_11_73
			product	chromosome condensation regulator
			transl_table	11
66398	67132	CDS
			locus_tag	Hyperionvirus_11_74
			product	hypothetical protein
			transl_table	11
68268	67165	CDS
			locus_tag	Hyperionvirus_11_75
			product	m7GpppX diphosphatase, partial
			transl_table	11
68841	69005	CDS
			locus_tag	Hyperionvirus_11_76
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_12
1	708	CDS
			locus_tag	Hyperionvirus_12_1
			product	hypothetical protein
			transl_table	11
853	2010	CDS
			locus_tag	Hyperionvirus_12_2
			product	hypothetical protein
			transl_table	11
2036	2857	CDS
			locus_tag	Hyperionvirus_12_3
			product	hypothetical protein
			transl_table	11
2921	3487	CDS
			locus_tag	Hyperionvirus_12_4
			product	hypothetical protein
			transl_table	11
3526	6045	CDS
			locus_tag	Hyperionvirus_12_5
			product	hypothetical protein
			transl_table	11
6094	7374	CDS
			locus_tag	Hyperionvirus_12_6
			product	hypothetical protein BVG19_g1396
			transl_table	11
7462	8517	CDS
			locus_tag	Hyperionvirus_12_7
			product	acetoin utilization deacetylase
			transl_table	11
9532	8510	CDS
			locus_tag	Hyperionvirus_12_8
			product	lipase
			transl_table	11
10123	9950	CDS
			locus_tag	Hyperionvirus_12_9
			product	hypothetical protein
			transl_table	11
10122	10559	CDS
			locus_tag	Hyperionvirus_12_10
			product	tRNase Z TRZ1
			transl_table	11
11472	10546	CDS
			locus_tag	Hyperionvirus_12_11
			product	hypothetical protein
			transl_table	11
12652	11519	CDS
			locus_tag	Hyperionvirus_12_12
			product	MULTISPECIES: glycosyltransferase
			transl_table	11
13915	12656	CDS
			locus_tag	Hyperionvirus_12_13
			product	predicted protein
			transl_table	11
14531	15274	CDS
			locus_tag	Hyperionvirus_12_14
			product	CHY zinc finger family protein
			transl_table	11
16092	15235	CDS
			locus_tag	Hyperionvirus_12_15
			product	putative alpha_beta hydrolase
			transl_table	11
16225	17400	CDS
			locus_tag	Hyperionvirus_12_16
			product	E3 ubiquitin ligase triad3, putative
			transl_table	11
17459	17851	CDS
			locus_tag	Hyperionvirus_12_17
			product	hypothetical protein
			transl_table	11
19156	17837	CDS
			locus_tag	Hyperionvirus_12_18
			product	DEAD/SNF2-like helicase
			transl_table	11
19234	20727	CDS
			locus_tag	Hyperionvirus_12_19
			product	cytochrome P450 704B1
			transl_table	11
20777	21616	CDS
			locus_tag	Hyperionvirus_12_20
			product	hypothetical protein
			transl_table	11
21664	22290	CDS
			locus_tag	Hyperionvirus_12_21
			product	hypothetical protein
			transl_table	11
22339	22845	CDS
			locus_tag	Hyperionvirus_12_22
			product	hypothetical protein
			transl_table	11
22875	23132	CDS
			locus_tag	Hyperionvirus_12_23
			product	hypothetical protein
			transl_table	11
23476	24111	CDS
			locus_tag	Hyperionvirus_12_24
			product	hypothetical protein
			transl_table	11
24131	24985	CDS
			locus_tag	Hyperionvirus_12_25
			product	PREDICTED: ubiquitin carboxyl-terminal hydrolase 2 isoform X1
			transl_table	11
25042	26028	CDS
			locus_tag	Hyperionvirus_12_26
			product	SAM-dependent methyltransferase
			transl_table	11
26069	27001	CDS
			locus_tag	Hyperionvirus_12_27
			product	hypothetical protein
			transl_table	11
27070	27657	CDS
			locus_tag	Hyperionvirus_12_28
			product	hypothetical protein
			transl_table	11
27735	31307	CDS
			locus_tag	Hyperionvirus_12_29
			product	hypothetical protein PHYSODRAFT_294653
			transl_table	11
31630	31262	CDS
			locus_tag	Hyperionvirus_12_30
			product	hypothetical protein
			transl_table	11
32438	31668	CDS
			locus_tag	Hyperionvirus_12_31
			product	exonuclease
			transl_table	11
32517	33533	CDS
			locus_tag	Hyperionvirus_12_32
			product	glutamine synthetase
			transl_table	11
33600	34052	CDS
			locus_tag	Hyperionvirus_12_33
			product	hypothetical protein
			transl_table	11
35020	34055	CDS
			locus_tag	Hyperionvirus_12_34
			product	hypothetical protein
			transl_table	11
35205	35065	CDS
			locus_tag	Hyperionvirus_12_35
			product	hypothetical protein
			transl_table	11
36649	35234	CDS
			locus_tag	Hyperionvirus_12_36
			product	chromosome condensation regulator, partial
			transl_table	11
36774	38063	CDS
			locus_tag	Hyperionvirus_12_37
			product	hypothetical protein
			transl_table	11
38143	41178	CDS
			locus_tag	Hyperionvirus_12_38
			product	hypothetical protein
			transl_table	11
42155	41175	CDS
			locus_tag	Hyperionvirus_12_39
			product	GDP-mannose 6-dehydrogenase
			transl_table	11
42242	42361	CDS
			locus_tag	Hyperionvirus_12_40
			product	hypothetical protein
			transl_table	11
42345	43529	CDS
			locus_tag	Hyperionvirus_12_41
			product	hypothetical protein
			transl_table	11
44581	43532	CDS
			locus_tag	Hyperionvirus_12_42
			product	2OG-FeII oxygenase
			transl_table	11
45112	44645	CDS
			locus_tag	Hyperionvirus_12_43
			product	hypothetical protein
			transl_table	11
45245	46204	CDS
			locus_tag	Hyperionvirus_12_44
			product	stomatin family protein
			transl_table	11
46994	46182	CDS
			locus_tag	Hyperionvirus_12_45
			product	hypothetical protein
			transl_table	11
47815	47051	CDS
			locus_tag	Hyperionvirus_12_46
			product	hypothetical protein
			transl_table	11
49279	48041	CDS
			locus_tag	Hyperionvirus_12_47
			product	hypothetical protein Klosneuvirus_1_232
			transl_table	11
50034	49315	CDS
			locus_tag	Hyperionvirus_12_48
			product	hypothetical protein
			transl_table	11
50129	50959	CDS
			locus_tag	Hyperionvirus_12_49
			product	bifunctional methylenetetrahydrofolate dehydrogenase/methenyltetrahydrofolate cyclohydrolase FolD
			transl_table	11
51008	51709	CDS
			locus_tag	Hyperionvirus_12_50
			product	hypothetical protein
			transl_table	11
51710	52357	CDS
			locus_tag	Hyperionvirus_12_51
			product	hypothetical protein A2161_04525
			transl_table	11
52418	53473	CDS
			locus_tag	Hyperionvirus_12_52
			product	hypothetical protein
			transl_table	11
55943	53436	CDS
			locus_tag	Hyperionvirus_12_53
			product	hypothetical protein
			transl_table	11
56477	56007	CDS
			locus_tag	Hyperionvirus_12_54
			product	hypothetical protein
			transl_table	11
58046	56601	CDS
			locus_tag	Hyperionvirus_12_55
			product	homospermidine synthase
			transl_table	11
>Hyperionvirus_13
7844	7774	tRNA	Hyperionvirus_tRNA_14
			product	tRNA-Gly(TCC)
10581	10511	tRNA	Hyperionvirus_tRNA_15
			product	tRNA-Glu(CTC)
10654	10584	tRNA	Hyperionvirus_tRNA_16
			product	tRNA-Pseudo(GTT)
10727	10656	tRNA	Hyperionvirus_tRNA_17
			product	tRNA-Pseudo(CAA)
10801	10730	tRNA	Hyperionvirus_tRNA_18
			product	tRNA-Leu(TAG)
10877	10806	tRNA	Hyperionvirus_tRNA_19
			product	tRNA-Pseudo(TAA)
10953	10883	tRNA	Hyperionvirus_tRNA_20
			product	tRNA-Phe(GAA)
11027	10957	tRNA	Hyperionvirus_tRNA_21
			product	tRNA-Pro(TGG)
13373	13302	tRNA	Hyperionvirus_tRNA_22
			product	tRNA-Arg(TCG)
13518	13448	tRNA	Hyperionvirus_tRNA_23
			product	tRNA-Gln(TTG)
13592	13521	tRNA	Hyperionvirus_tRNA_24
			product	tRNA-Arg(TCT)
16545	16462	tRNA	Hyperionvirus_tRNA_25
			product	tRNA-Ser(GCT)
16627	16548	tRNA	Hyperionvirus_tRNA_26
			product	tRNA-Pseudo(TGA)
1	426	CDS
			locus_tag	Hyperionvirus_13_1
			product	hypothetical protein
			transl_table	11
426	1583	CDS
			locus_tag	Hyperionvirus_13_2
			product	AarF/ABC1/UbiB kinase family protein
			transl_table	11
1601	1939	CDS
			locus_tag	Hyperionvirus_13_3
			product	hypothetical protein
			transl_table	11
1992	2645	CDS
			locus_tag	Hyperionvirus_13_4
			product	prolipoprotein diacylglyceryl transferase
			transl_table	11
3215	2610	CDS
			locus_tag	Hyperionvirus_13_5
			product	IPT/TIG domain-containing protein
			transl_table	11
4343	3243	CDS
			locus_tag	Hyperionvirus_13_6
			product	hypothetical protein
			transl_table	11
4420	5091	CDS
			locus_tag	Hyperionvirus_13_7
			product	hypothetical protein BBK36DRAFT_23835
			transl_table	11
6730	5051	CDS
			locus_tag	Hyperionvirus_13_8
			product	hypothetical protein Klosneuvirus_1_394
			transl_table	11
7396	6962	CDS
			locus_tag	Hyperionvirus_13_9
			product	hypothetical protein
			transl_table	11
8068	8340	CDS
			locus_tag	Hyperionvirus_13_10
			product	hypothetical protein
			transl_table	11
8486	8866	CDS
			locus_tag	Hyperionvirus_13_11
			product	PREDICTED: vesicle transport protein SFT2A isoform X2
			transl_table	11
8937	9152	CDS
			locus_tag	Hyperionvirus_13_12
			product	hypothetical protein
			transl_table	11
9176	9280	CDS
			locus_tag	Hyperionvirus_13_13
			product	hypothetical protein
			transl_table	11
9323	9457	CDS
			locus_tag	Hyperionvirus_13_14
			product	hypothetical protein
			transl_table	11
9575	9724	CDS
			locus_tag	Hyperionvirus_13_15
			product	hypothetical protein
			transl_table	11
10327	10473	CDS
			locus_tag	Hyperionvirus_13_16
			product	hypothetical protein
			transl_table	11
10996	12159	CDS
			locus_tag	Hyperionvirus_13_17
			product	hypothetical protein
			transl_table	11
12527	13216	CDS
			locus_tag	Hyperionvirus_13_18
			product	hypothetical protein
			transl_table	11
14044	13778	CDS
			locus_tag	Hyperionvirus_13_19
			product	hypothetical protein
			transl_table	11
14264	15334	CDS
			locus_tag	Hyperionvirus_13_20
			product	alpha-L-fucosidase
			transl_table	11
15387	15863	CDS
			locus_tag	Hyperionvirus_13_21
			product	hypothetical protein
			transl_table	11
15936	16403	CDS
			locus_tag	Hyperionvirus_13_22
			product	hypothetical protein
			transl_table	11
17120	16662	CDS
			locus_tag	Hyperionvirus_13_23
			product	hypothetical protein
			transl_table	11
17180	18043	CDS
			locus_tag	Hyperionvirus_13_24
			product	PREDICTED: alpha-soluble NSF attachment protein-like
			transl_table	11
18085	18549	CDS
			locus_tag	Hyperionvirus_13_25
			product	hypothetical protein
			transl_table	11
18604	20724	CDS
			locus_tag	Hyperionvirus_13_26
			product	Hypothetical Protein FCC1311_031102
			transl_table	11
21013	21993	CDS
			locus_tag	Hyperionvirus_13_27
			product	WD repeat protein
			transl_table	11
22035	22559	CDS
			locus_tag	Hyperionvirus_13_28
			product	hypothetical protein
			transl_table	11
25273	22556	CDS
			locus_tag	Hyperionvirus_13_29
			product	hypothetical protein
			transl_table	11
26574	25321	CDS
			locus_tag	Hyperionvirus_13_30
			product	Thg1-like protein 1
			transl_table	11
26698	27198	CDS
			locus_tag	Hyperionvirus_13_31
			product	hypothetical protein
			transl_table	11
27254	27670	CDS
			locus_tag	Hyperionvirus_13_32
			product	hypothetical protein
			transl_table	11
27731	28525	CDS
			locus_tag	Hyperionvirus_13_33
			product	hypothetical protein
			transl_table	11
28581	29072	CDS
			locus_tag	Hyperionvirus_13_34
			product	hypothetical protein
			transl_table	11
29085	31715	CDS
			locus_tag	Hyperionvirus_13_35
			product	hypothetical protein
			transl_table	11
31786	32043	CDS
			locus_tag	Hyperionvirus_13_36
			product	hypothetical protein
			transl_table	11
33612	32014	CDS
			locus_tag	Hyperionvirus_13_37
			product	Nucleolar GTP-binding protein 1
			transl_table	11
33837	33670	CDS
			locus_tag	Hyperionvirus_13_38
			product	hypothetical protein
			transl_table	11
34706	34425	CDS
			locus_tag	Hyperionvirus_13_39
			product	hypothetical protein
			transl_table	11
34947	36146	CDS
			locus_tag	Hyperionvirus_13_40
			product	hypothetical protein Klosneuvirus_3_298
			transl_table	11
37034	36396	CDS
			locus_tag	Hyperionvirus_13_41
			product	putative orfan
			transl_table	11
38899	37061	CDS
			locus_tag	Hyperionvirus_13_42
			product	hypothetical protein
			transl_table	11
39976	38975	CDS
			locus_tag	Hyperionvirus_13_43
			product	GTP binding translation elongation factor
			transl_table	11
41693	40038	CDS
			locus_tag	Hyperionvirus_13_44
			product	dna topoisomerase 1b
			transl_table	11
42009	41749	CDS
			locus_tag	Hyperionvirus_13_45
			product	hypothetical protein
			transl_table	11
42497	42072	CDS
			locus_tag	Hyperionvirus_13_46
			product	hypothetical protein
			transl_table	11
42595	43410	CDS
			locus_tag	Hyperionvirus_13_47
			product	hypothetical protein
			transl_table	11
43960	43505	CDS
			locus_tag	Hyperionvirus_13_48
			product	hypothetical protein
			transl_table	11
45668	44121	CDS
			locus_tag	Hyperionvirus_13_49
			product	hypothetical protein
			transl_table	11
46242	45724	CDS
			locus_tag	Hyperionvirus_13_50
			product	hypothetical protein
			transl_table	11
46925	46410	CDS
			locus_tag	Hyperionvirus_13_51
			product	hypothetical protein
			transl_table	11
47214	47354	CDS
			locus_tag	Hyperionvirus_13_52
			product	hypothetical protein
			transl_table	11
48702	47464	CDS
			locus_tag	Hyperionvirus_13_53
			product	hypothetical protein
			transl_table	11
49793	48765	CDS
			locus_tag	Hyperionvirus_13_54
			product	hypothetical protein CVU91_02440
			transl_table	11
>Hyperionvirus_14
644	3	CDS
			locus_tag	Hyperionvirus_14_1
			product	TRAF
			transl_table	11
777	1871	CDS
			locus_tag	Hyperionvirus_14_2
			product	hypothetical protein
			transl_table	11
1937	2029	CDS
			locus_tag	Hyperionvirus_14_3
			product	hypothetical protein
			transl_table	11
2432	2722	CDS
			locus_tag	Hyperionvirus_14_4
			product	hypothetical protein
			transl_table	11
3511	2798	CDS
			locus_tag	Hyperionvirus_14_5
			product	hypothetical protein
			transl_table	11
3652	4653	CDS
			locus_tag	Hyperionvirus_14_6
			product	serpin family protein
			transl_table	11
4695	5564	CDS
			locus_tag	Hyperionvirus_14_7
			product	hypothetical protein
			transl_table	11
7654	5561	CDS
			locus_tag	Hyperionvirus_14_8
			product	dynamin family protein
			transl_table	11
7759	8391	CDS
			locus_tag	Hyperionvirus_14_9
			product	hypothetical protein
			transl_table	11
8803	8357	CDS
			locus_tag	Hyperionvirus_14_10
			product	hypothetical protein
			transl_table	11
9842	8856	CDS
			locus_tag	Hyperionvirus_14_11
			product	hypothetical protein
			transl_table	11
10507	9890	CDS
			locus_tag	Hyperionvirus_14_12
			product	hypothetical protein
			transl_table	11
11191	10547	CDS
			locus_tag	Hyperionvirus_14_13
			product	hypothetical protein
			transl_table	11
11329	11904	CDS
			locus_tag	Hyperionvirus_14_14
			product	hypothetical protein
			transl_table	11
12341	11901	CDS
			locus_tag	Hyperionvirus_14_15
			product	hypothetical protein
			transl_table	11
12436	14865	CDS
			locus_tag	Hyperionvirus_14_16
			product	poly(beta-D-mannuronate) C5 epimerase 1
			transl_table	11
14910	15830	CDS
			locus_tag	Hyperionvirus_14_17
			product	hypothetical protein
			transl_table	11
16134	15883	CDS
			locus_tag	Hyperionvirus_14_18
			product	hypothetical protein
			transl_table	11
17834	17079	CDS
			locus_tag	Hyperionvirus_14_19
			product	NUDIX hydrolase
			transl_table	11
19265	17871	CDS
			locus_tag	Hyperionvirus_14_20
			product	chromosome condensation regulator, partial
			transl_table	11
20424	19369	CDS
			locus_tag	Hyperionvirus_14_21
			product	hypothetical protein
			transl_table	11
20531	21298	CDS
			locus_tag	Hyperionvirus_14_22
			product	hypothetical protein
			transl_table	11
21954	21301	CDS
			locus_tag	Hyperionvirus_14_23
			product	hypothetical protein
			transl_table	11
22070	22687	CDS
			locus_tag	Hyperionvirus_14_24
			product	hypothetical protein Hokovirus_3_84
			transl_table	11
23049	22684	CDS
			locus_tag	Hyperionvirus_14_25
			product	hypothetical protein
			transl_table	11
23692	23060	CDS
			locus_tag	Hyperionvirus_14_26
			product	hypothetical protein
			transl_table	11
24216	23740	CDS
			locus_tag	Hyperionvirus_14_27
			product	hypothetical protein Catovirus_1_1
			transl_table	11
24279	25880	CDS
			locus_tag	Hyperionvirus_14_28
			product	hypothetical protein
			transl_table	11
25935	26942	CDS
			locus_tag	Hyperionvirus_14_29
			product	hypothetical protein
			transl_table	11
26995	27771	CDS
			locus_tag	Hyperionvirus_14_30
			product	hypothetical protein
			transl_table	11
27791	28354	CDS
			locus_tag	Hyperionvirus_14_31
			product	PREDICTED: RING finger protein 151-like
			transl_table	11
28474	28665	CDS
			locus_tag	Hyperionvirus_14_32
			product	hypothetical protein
			transl_table	11
31168	28652	CDS
			locus_tag	Hyperionvirus_14_33
			product	hypothetical protein Hokovirus_1_183
			transl_table	11
31155	31265	CDS
			locus_tag	Hyperionvirus_14_34
			product	hypothetical protein
			transl_table	11
31954	32712	CDS
			locus_tag	Hyperionvirus_14_35
			product	hypothetical protein PBRA_002699
			transl_table	11
33624	32707	CDS
			locus_tag	Hyperionvirus_14_36
			product	hypothetical protein PBRA_002699
			transl_table	11
34396	33686	CDS
			locus_tag	Hyperionvirus_14_37
			product	hypothetical protein DyAD56_06310
			transl_table	11
35174	34452	CDS
			locus_tag	Hyperionvirus_14_38
			product	hypothetical protein
			transl_table	11
35319	35840	CDS
			locus_tag	Hyperionvirus_14_39
			product	hypothetical protein
			transl_table	11
35897	36304	CDS
			locus_tag	Hyperionvirus_14_40
			product	hypothetical protein
			transl_table	11
36370	36690	CDS
			locus_tag	Hyperionvirus_14_41
			product	hypothetical protein
			transl_table	11
36717	38108	CDS
			locus_tag	Hyperionvirus_14_42
			product	mannose-1-phosphate guanylyltransferase/mannose-6-phosphate isomerase
			transl_table	11
38199	38351	CDS
			locus_tag	Hyperionvirus_14_43
			product	hypothetical protein
			transl_table	11
38634	39779	CDS
			locus_tag	Hyperionvirus_14_44
			product	FAD-binding protein
			transl_table	11
39800	39985	CDS
			locus_tag	Hyperionvirus_14_45
			product	hypothetical protein
			transl_table	11
39985	40179	CDS
			locus_tag	Hyperionvirus_14_46
			product	hypothetical protein
			transl_table	11
40207	40455	CDS
			locus_tag	Hyperionvirus_14_47
			product	hypothetical protein
			transl_table	11
40687	41805	CDS
			locus_tag	Hyperionvirus_14_48
			product	hypothetical protein
			transl_table	11
42342	41848	CDS
			locus_tag	Hyperionvirus_14_49
			product	hypothetical protein
			transl_table	11
43789	42461	CDS
			locus_tag	Hyperionvirus_14_50
			product	hypothetical protein Indivirus_7_20
			transl_table	11
45068	45229	CDS
			locus_tag	Hyperionvirus_14_51
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_15
409	263	CDS
			locus_tag	Hyperionvirus_15_1
			product	hypothetical protein
			transl_table	11
2375	435	CDS
			locus_tag	Hyperionvirus_15_2
			product	DEAD/SNF2-like helicase
			transl_table	11
2412	3371	CDS
			locus_tag	Hyperionvirus_15_3
			product	L-galactose dehydrogenase
			transl_table	11
4435	3368	CDS
			locus_tag	Hyperionvirus_15_4
			product	hypothetical protein
			transl_table	11
4740	6128	CDS
			locus_tag	Hyperionvirus_15_5
			product	hypothetical protein Klosneuvirus_10_6
			transl_table	11
6290	6445	CDS
			locus_tag	Hyperionvirus_15_6
			product	hypothetical protein
			transl_table	11
6957	6760	CDS
			locus_tag	Hyperionvirus_15_7
			product	hypothetical protein
			transl_table	11
8699	7023	CDS
			locus_tag	Hyperionvirus_15_8
			product	hypothetical protein
			transl_table	11
9104	8754	CDS
			locus_tag	Hyperionvirus_15_9
			product	putative GIY-YIG endonuclease
			transl_table	11
9760	9125	CDS
			locus_tag	Hyperionvirus_15_10
			product	hypothetical protein
			transl_table	11
10230	9829	CDS
			locus_tag	Hyperionvirus_15_11
			product	hypothetical protein
			transl_table	11
10910	10287	CDS
			locus_tag	Hyperionvirus_15_12
			product	PREDICTED: tripartite motif-containing protein 40-like
			transl_table	11
11635	10961	CDS
			locus_tag	Hyperionvirus_15_13
			product	hypothetical protein
			transl_table	11
11826	11575	CDS
			locus_tag	Hyperionvirus_15_14
			product	hypothetical protein
			transl_table	11
12037	11894	CDS
			locus_tag	Hyperionvirus_15_15
			product	hypothetical protein
			transl_table	11
12250	12131	CDS
			locus_tag	Hyperionvirus_15_16
			product	hypothetical protein
			transl_table	11
12536	12426	CDS
			locus_tag	Hyperionvirus_15_17
			product	hypothetical protein
			transl_table	11
12806	12627	CDS
			locus_tag	Hyperionvirus_15_18
			product	hypothetical protein
			transl_table	11
13677	13132	CDS
			locus_tag	Hyperionvirus_15_19
			product	hypothetical protein
			transl_table	11
14569	13691	CDS
			locus_tag	Hyperionvirus_15_20
			product	hypothetical protein
			transl_table	11
15387	14719	CDS
			locus_tag	Hyperionvirus_15_21
			product	hypothetical protein
			transl_table	11
16247	15438	CDS
			locus_tag	Hyperionvirus_15_22
			product	phosphonoacetaldehyde hydrolase
			transl_table	11
16360	16671	CDS
			locus_tag	Hyperionvirus_15_23
			product	hypothetical protein
			transl_table	11
17067	16654	CDS
			locus_tag	Hyperionvirus_15_24
			product	hypothetical protein
			transl_table	11
17168	18028	CDS
			locus_tag	Hyperionvirus_15_25
			product	hypothetical protein
			transl_table	11
18750	18031	CDS
			locus_tag	Hyperionvirus_15_26
			product	RING/U-box superfamily protein
			transl_table	11
19042	18911	CDS
			locus_tag	Hyperionvirus_15_27
			product	hypothetical protein
			transl_table	11
19195	20262	CDS
			locus_tag	Hyperionvirus_15_28
			product	hypothetical protein
			transl_table	11
20333	21514	CDS
			locus_tag	Hyperionvirus_15_29
			product	hypothetical protein
			transl_table	11
22182	21511	CDS
			locus_tag	Hyperionvirus_15_30
			product	hypothetical protein SDRG_10562
			transl_table	11
22385	22251	CDS
			locus_tag	Hyperionvirus_15_31
			product	hypothetical protein
			transl_table	11
22600	22391	CDS
			locus_tag	Hyperionvirus_15_32
			product	hypothetical protein
			transl_table	11
22691	23359	CDS
			locus_tag	Hyperionvirus_15_33
			product	hypothetical protein
			transl_table	11
23340	24131	CDS
			locus_tag	Hyperionvirus_15_34
			product	BAX inhibitor BI-1
			transl_table	11
24418	24128	CDS
			locus_tag	Hyperionvirus_15_35
			product	hypothetical protein
			transl_table	11
25444	24473	CDS
			locus_tag	Hyperionvirus_15_36
			product	acid ceramidase
			transl_table	11
25512	26276	CDS
			locus_tag	Hyperionvirus_15_37
			product	glycosyltransferase family 25
			transl_table	11
26363	27091	CDS
			locus_tag	Hyperionvirus_15_38
			product	hypothetical protein
			transl_table	11
27199	27891	CDS
			locus_tag	Hyperionvirus_15_39
			product	hypothetical protein
			transl_table	11
28194	27898	CDS
			locus_tag	Hyperionvirus_15_40
			product	vesicle-associated membrane protein 7B
			transl_table	11
28681	28247	CDS
			locus_tag	Hyperionvirus_15_41
			product	hypothetical protein
			transl_table	11
29493	28738	CDS
			locus_tag	Hyperionvirus_15_42
			product	hypothetical protein B6240_02170
			transl_table	11
29763	29494	CDS
			locus_tag	Hyperionvirus_15_43
			product	hypothetical protein VOLCADRAFT_106432
			transl_table	11
31420	29795	CDS
			locus_tag	Hyperionvirus_15_44
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
31558	31439	CDS
			locus_tag	Hyperionvirus_15_45
			product	hypothetical protein
			transl_table	11
31827	31576	CDS
			locus_tag	Hyperionvirus_15_46
			product	hypothetical protein
			transl_table	11
32497	31892	CDS
			locus_tag	Hyperionvirus_15_47
			product	hypothetical protein Indivirus_1_41
			transl_table	11
32496	32636	CDS
			locus_tag	Hyperionvirus_15_48
			product	hypothetical protein
			transl_table	11
32907	33020	CDS
			locus_tag	Hyperionvirus_15_49
			product	hypothetical protein
			transl_table	11
34926	33454	CDS
			locus_tag	Hyperionvirus_15_50
			product	hypothetical protein
			transl_table	11
35580	35011	CDS
			locus_tag	Hyperionvirus_15_51
			product	hypothetical protein
			transl_table	11
38108	35598	CDS
			locus_tag	Hyperionvirus_15_52
			product	Aminopeptidase 1
			transl_table	11
38942	38175	CDS
			locus_tag	Hyperionvirus_15_53
			product	hypothetical protein
			transl_table	11
39039	40346	CDS
			locus_tag	Hyperionvirus_15_54
			product	hypothetical protein
			transl_table	11
40393	40899	CDS
			locus_tag	Hyperionvirus_15_55
			product	hypothetical protein TPHA_0D04300
			transl_table	11
42290	40896	CDS
			locus_tag	Hyperionvirus_15_56
			product	seryl-tRNA synthetase
			transl_table	11
42376	43800	CDS
			locus_tag	Hyperionvirus_15_57
			product	glutamine tRNA-synthetase
			transl_table	11
43855	44355	CDS
			locus_tag	Hyperionvirus_15_58
			product	hypothetical protein
			transl_table	11
45032	44352	CDS
			locus_tag	Hyperionvirus_15_59
			product	hypothetical protein RFI_18233, partial
			transl_table	11
45022	46008	CDS
			locus_tag	Hyperionvirus_15_60
			product	glycosyl transferase group 1 family protein
			transl_table	11
47209	45989	CDS
			locus_tag	Hyperionvirus_15_61
			product	hypothetical protein THRCLA_20764
			transl_table	11
47480	47289	CDS
			locus_tag	Hyperionvirus_15_62
			product	hypothetical protein
			transl_table	11
47631	47963	CDS
			locus_tag	Hyperionvirus_15_63
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_16
162	1	CDS
			locus_tag	Hyperionvirus_16_1
			product	hypothetical protein
			transl_table	11
390	193	CDS
			locus_tag	Hyperionvirus_16_2
			product	hypothetical protein
			transl_table	11
613	449	CDS
			locus_tag	Hyperionvirus_16_3
			product	hypothetical protein
			transl_table	11
776	1897	CDS
			locus_tag	Hyperionvirus_16_4
			product	hypothetical protein
			transl_table	11
1965	4241	CDS
			locus_tag	Hyperionvirus_16_5
			product	hypothetical protein F443_19289
			transl_table	11
5814	4234	CDS
			locus_tag	Hyperionvirus_16_6
			product	hypothetical protein
			transl_table	11
6487	5855	CDS
			locus_tag	Hyperionvirus_16_7
			product	hypothetical protein
			transl_table	11
8648	6573	CDS
			locus_tag	Hyperionvirus_16_8
			product	hypothetical protein
			transl_table	11
8972	8832	CDS
			locus_tag	Hyperionvirus_16_9
			product	hypothetical protein
			transl_table	11
8971	10374	CDS
			locus_tag	Hyperionvirus_16_10
			product	hypothetical protein
			transl_table	11
10947	10420	CDS
			locus_tag	Hyperionvirus_16_11
			product	hypothetical protein
			transl_table	11
11752	11042	CDS
			locus_tag	Hyperionvirus_16_12
			product	hypothetical protein
			transl_table	11
12491	11838	CDS
			locus_tag	Hyperionvirus_16_13
			product	hypothetical protein
			transl_table	11
12880	14331	CDS
			locus_tag	Hyperionvirus_16_14
			product	prolyl-tRNA synthetase
			transl_table	11
14592	14455	CDS
			locus_tag	Hyperionvirus_16_15
			product	hypothetical protein
			transl_table	11
14727	15500	CDS
			locus_tag	Hyperionvirus_16_16
			product	hypothetical protein
			transl_table	11
15923	15585	CDS
			locus_tag	Hyperionvirus_16_17
			product	hypothetical protein
			transl_table	11
16042	17235	CDS
			locus_tag	Hyperionvirus_16_18
			product	hypothetical protein
			transl_table	11
17294	17752	CDS
			locus_tag	Hyperionvirus_16_19
			product	hypothetical protein OlV2_214
			transl_table	11
17822	18973	CDS
			locus_tag	Hyperionvirus_16_20
			product	hypothetical protein AMS18_00165
			transl_table	11
20187	19015	CDS
			locus_tag	Hyperionvirus_16_21
			product	hypothetical protein
			transl_table	11
20381	20920	CDS
			locus_tag	Hyperionvirus_16_22
			product	hypothetical protein
			transl_table	11
21071	21625	CDS
			locus_tag	Hyperionvirus_16_23
			product	hypothetical protein
			transl_table	11
21691	21930	CDS
			locus_tag	Hyperionvirus_16_24
			product	hypothetical protein
			transl_table	11
22353	21919	CDS
			locus_tag	Hyperionvirus_16_25
			product	hypothetical protein
			transl_table	11
22951	22436	CDS
			locus_tag	Hyperionvirus_16_26
			product	hypothetical protein
			transl_table	11
23910	23026	CDS
			locus_tag	Hyperionvirus_16_27
			product	hypothetical protein
			transl_table	11
23957	24970	CDS
			locus_tag	Hyperionvirus_16_28
			product	hypothetical protein
			transl_table	11
25344	25835	CDS
			locus_tag	Hyperionvirus_16_29
			product	hypothetical protein
			transl_table	11
26358	25837	CDS
			locus_tag	Hyperionvirus_16_30
			product	DEHA2D06424p
			transl_table	11
26947	26423	CDS
			locus_tag	Hyperionvirus_16_31
			product	hypothetical protein Catovirus_1_50
			transl_table	11
27044	27700	CDS
			locus_tag	Hyperionvirus_16_32
			product	hypothetical protein
			transl_table	11
27748	28698	CDS
			locus_tag	Hyperionvirus_16_33
			product	hypothetical protein SAMD00019534_038080
			transl_table	11
29973	28693	CDS
			locus_tag	Hyperionvirus_16_34
			product	hypothetical protein
			transl_table	11
30481	30603	CDS
			locus_tag	Hyperionvirus_16_35
			product	hypothetical protein
			transl_table	11
31816	30908	CDS
			locus_tag	Hyperionvirus_16_36
			product	hypothetical protein
			transl_table	11
31800	32159	CDS
			locus_tag	Hyperionvirus_16_37
			product	hypothetical protein
			transl_table	11
32181	32402	CDS
			locus_tag	Hyperionvirus_16_38
			product	hypothetical protein
			transl_table	11
32567	33925	CDS
			locus_tag	Hyperionvirus_16_39
			product	chromosome condensation regulator
			transl_table	11
33993	34373	CDS
			locus_tag	Hyperionvirus_16_40
			product	hypothetical protein
			transl_table	11
34466	35620	CDS
			locus_tag	Hyperionvirus_16_41
			product	hypothetical protein
			transl_table	11
36771	35623	CDS
			locus_tag	Hyperionvirus_16_42
			product	hypothetical protein
			transl_table	11
36892	37716	CDS
			locus_tag	Hyperionvirus_16_43
			product	hypothetical protein
			transl_table	11
37788	38351	CDS
			locus_tag	Hyperionvirus_16_44
			product	hypothetical protein
			transl_table	11
38993	38364	CDS
			locus_tag	Hyperionvirus_16_45
			product	hypothetical protein
			transl_table	11
39150	40043	CDS
			locus_tag	Hyperionvirus_16_46
			product	hypothetical protein
			transl_table	11
40122	42257	CDS
			locus_tag	Hyperionvirus_16_47
			product	putative ORFan
			transl_table	11
42943	42260	CDS
			locus_tag	Hyperionvirus_16_48
			product	hypothetical protein
			transl_table	11
43354	43169	CDS
			locus_tag	Hyperionvirus_16_49
			product	hypothetical protein
			transl_table	11
43726	43466	CDS
			locus_tag	Hyperionvirus_16_50
			product	hypothetical protein
			transl_table	11
44035	43826	CDS
			locus_tag	Hyperionvirus_16_51
			product	hypothetical protein
			transl_table	11
44196	44083	CDS
			locus_tag	Hyperionvirus_16_52
			product	hypothetical protein
			transl_table	11
44647	44429	CDS
			locus_tag	Hyperionvirus_16_53
			product	hypothetical protein
			transl_table	11
44915	45010	CDS
			locus_tag	Hyperionvirus_16_54
			product	hypothetical protein
			transl_table	11
45166	45261	CDS
			locus_tag	Hyperionvirus_16_55
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_17
1	576	CDS
			locus_tag	Hyperionvirus_17_1
			product	glycosyltransferase
			transl_table	11
1834	548	CDS
			locus_tag	Hyperionvirus_17_2
			product	hypothetical protein
			transl_table	11
2539	1859	CDS
			locus_tag	Hyperionvirus_17_3
			product	serine/threonine protein kinase
			transl_table	11
2538	2651	CDS
			locus_tag	Hyperionvirus_17_4
			product	hypothetical protein
			transl_table	11
2688	2834	CDS
			locus_tag	Hyperionvirus_17_5
			product	hypothetical protein
			transl_table	11
3042	4727	CDS
			locus_tag	Hyperionvirus_17_6
			product	ATPase, AAA domain containing protein
			transl_table	11
5963	4704	CDS
			locus_tag	Hyperionvirus_17_7
			product	hypothetical protein
			transl_table	11
6515	5994	CDS
			locus_tag	Hyperionvirus_17_8
			product	hypothetical protein Catovirus_1_911
			transl_table	11
7965	6571	CDS
			locus_tag	Hyperionvirus_17_9
			product	hypothetical protein
			transl_table	11
8050	8847	CDS
			locus_tag	Hyperionvirus_17_10
			product	hypothetical protein Catovirus_1_919
			transl_table	11
10127	8814	CDS
			locus_tag	Hyperionvirus_17_11
			product	hypothetical protein COV43_06850
			transl_table	11
11553	10171	CDS
			locus_tag	Hyperionvirus_17_12
			product	hypothetical protein
			transl_table	11
15762	11578	CDS
			locus_tag	Hyperionvirus_17_13
			product	hypothetical protein
			transl_table	11
17916	15790	CDS
			locus_tag	Hyperionvirus_17_14
			product	hypothetical protein
			transl_table	11
18481	17942	CDS
			locus_tag	Hyperionvirus_17_15
			product	hypothetical protein
			transl_table	11
19336	18527	CDS
			locus_tag	Hyperionvirus_17_16
			product	hypothetical protein
			transl_table	11
19372	19746	CDS
			locus_tag	Hyperionvirus_17_17
			product	hypothetical protein
			transl_table	11
20506	19748	CDS
			locus_tag	Hyperionvirus_17_18
			product	hypothetical protein Catovirus_1_922
			transl_table	11
20560	20958	CDS
			locus_tag	Hyperionvirus_17_19
			product	hypothetical protein Indivirus_1_171
			transl_table	11
21006	23798	CDS
			locus_tag	Hyperionvirus_17_20
			product	tRNA 4-thiouridine(8) synthase ThiI
			transl_table	11
24444	23800	CDS
			locus_tag	Hyperionvirus_17_21
			product	hypothetical protein
			transl_table	11
24937	24470	CDS
			locus_tag	Hyperionvirus_17_22
			product	hypothetical protein Indivirus_1_169
			transl_table	11
25284	24913	CDS
			locus_tag	Hyperionvirus_17_23
			product	hypothetical protein
			transl_table	11
27816	25375	CDS
			locus_tag	Hyperionvirus_17_24
			product	XRN 5'-3' exonuclease
			transl_table	11
28676	27819	CDS
			locus_tag	Hyperionvirus_17_25
			product	apurinic endonuclease
			transl_table	11
29072	28725	CDS
			locus_tag	Hyperionvirus_17_26
			product	hypothetical protein
			transl_table	11
30098	29085	CDS
			locus_tag	Hyperionvirus_17_27
			product	hypothetical protein Catovirus_1_928
			transl_table	11
30191	31543	CDS
			locus_tag	Hyperionvirus_17_28
			product	hypothetical protein Catovirus_1_929
			transl_table	11
31578	31694	CDS
			locus_tag	Hyperionvirus_17_29
			product	hypothetical protein
			transl_table	11
33800	31689	CDS
			locus_tag	Hyperionvirus_17_30
			product	UvrD-family helicase
			transl_table	11
34972	33854	CDS
			locus_tag	Hyperionvirus_17_31
			product	hypothetical protein Indivirus_1_153
			transl_table	11
35718	35041	CDS
			locus_tag	Hyperionvirus_17_32
			product	hypothetical protein
			transl_table	11
37557	35758	CDS
			locus_tag	Hyperionvirus_17_33
			product	hypothetical protein NCER_100390
			transl_table	11
37979	37614	CDS
			locus_tag	Hyperionvirus_17_34
			product	hypothetical protein
			transl_table	11
38286	38035	CDS
			locus_tag	Hyperionvirus_17_35
			product	hypothetical protein
			transl_table	11
40066	38318	CDS
			locus_tag	Hyperionvirus_17_36
			product	SNF2-like helicase
			transl_table	11
40963	40091	CDS
			locus_tag	Hyperionvirus_17_37
			product	hypothetical protein
			transl_table	11
41801	41028	CDS
			locus_tag	Hyperionvirus_17_38
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
42296	41850	CDS
			locus_tag	Hyperionvirus_17_39
			product	NADAR family protein
			transl_table	11
42706	42347	CDS
			locus_tag	Hyperionvirus_17_40
			product	hypothetical protein Catovirus_1_948
			transl_table	11
42711	42827	CDS
			locus_tag	Hyperionvirus_17_41
			product	hypothetical protein
			transl_table	11
43098	43220	CDS
			locus_tag	Hyperionvirus_17_42
			product	hypothetical protein
			transl_table	11
43517	44590	CDS
			locus_tag	Hyperionvirus_17_43
			product	metallopeptidase family M24
			transl_table	11
45027	44587	CDS
			locus_tag	Hyperionvirus_17_44
			product	hypothetical protein CBC78_07235
			transl_table	11
>Hyperionvirus_18
3	113	CDS
			locus_tag	Hyperionvirus_18_1
			product	hypothetical protein
			transl_table	11
195	977	CDS
			locus_tag	Hyperionvirus_18_2
			product	hypothetical protein
			transl_table	11
1822	827	CDS
			locus_tag	Hyperionvirus_18_3
			product	sel1 repeat family protein
			transl_table	11
3219	1915	CDS
			locus_tag	Hyperionvirus_18_4
			product	chromosome condensation regulator
			transl_table	11
3252	3551	CDS
			locus_tag	Hyperionvirus_18_5
			product	hypothetical protein
			transl_table	11
4610	3573	CDS
			locus_tag	Hyperionvirus_18_6
			product	exodeoxyribonuclease III
			transl_table	11
5679	5293	CDS
			locus_tag	Hyperionvirus_18_7
			product	hypothetical protein
			transl_table	11
5898	5716	CDS
			locus_tag	Hyperionvirus_18_8
			product	hypothetical protein
			transl_table	11
7067	5982	CDS
			locus_tag	Hyperionvirus_18_9
			product	predicted homing endonuclease
			transl_table	11
8085	7168	CDS
			locus_tag	Hyperionvirus_18_10
			product	hypothetical protein Catovirus_2_144
			transl_table	11
8354	8133	CDS
			locus_tag	Hyperionvirus_18_11
			product	hypothetical protein
			transl_table	11
8404	9351	CDS
			locus_tag	Hyperionvirus_18_12
			product	hypothetical protein AMSG_06688
			transl_table	11
9835	9431	CDS
			locus_tag	Hyperionvirus_18_13
			product	uvr/rep helicase: PROVISIONAL
			transl_table	11
14031	9985	CDS
			locus_tag	Hyperionvirus_18_14
			product	DEAD/SNF2-like helicase
			transl_table	11
14592	14056	CDS
			locus_tag	Hyperionvirus_18_15
			product	hypothetical protein
			transl_table	11
15613	14648	CDS
			locus_tag	Hyperionvirus_18_16
			product	hypothetical protein
			transl_table	11
16231	15653	CDS
			locus_tag	Hyperionvirus_18_17
			product	hypothetical protein
			transl_table	11
16304	17563	CDS
			locus_tag	Hyperionvirus_18_18
			product	hypothetical protein
			transl_table	11
19454	17553	CDS
			locus_tag	Hyperionvirus_18_19
			product	hypothetical protein
			transl_table	11
19566	20861	CDS
			locus_tag	Hyperionvirus_18_20
			product	hypothetical protein
			transl_table	11
21450	20863	CDS
			locus_tag	Hyperionvirus_18_21
			product	hypothetical protein HELRODRAFT_156074
			transl_table	11
21642	22358	CDS
			locus_tag	Hyperionvirus_18_22
			product	hypothetical protein
			transl_table	11
22998	22348	CDS
			locus_tag	Hyperionvirus_18_23
			product	hypothetical protein
			transl_table	11
23701	22952	CDS
			locus_tag	Hyperionvirus_18_24
			product	ring finger domain protein
			transl_table	11
24331	23768	CDS
			locus_tag	Hyperionvirus_18_25
			product	hypothetical protein
			transl_table	11
24614	25315	CDS
			locus_tag	Hyperionvirus_18_26
			product	hypothetical protein
			transl_table	11
25438	25602	CDS
			locus_tag	Hyperionvirus_18_27
			product	hypothetical protein
			transl_table	11
25628	25777	CDS
			locus_tag	Hyperionvirus_18_28
			product	hypothetical protein
			transl_table	11
26636	25887	CDS
			locus_tag	Hyperionvirus_18_29
			product	hypothetical protein
			transl_table	11
27319	26681	CDS
			locus_tag	Hyperionvirus_18_30
			product	hypothetical protein
			transl_table	11
27403	28452	CDS
			locus_tag	Hyperionvirus_18_31
			product	hypothetical protein
			transl_table	11
28500	29513	CDS
			locus_tag	Hyperionvirus_18_32
			product	hypothetical protein I308_06674
			transl_table	11
30865	29498	CDS
			locus_tag	Hyperionvirus_18_33
			product	hypothetical protein
			transl_table	11
31857	30916	CDS
			locus_tag	Hyperionvirus_18_34
			product	peptide-methionine (R)-S-oxide reductase
			transl_table	11
33292	32144	CDS
			locus_tag	Hyperionvirus_18_35
			product	hypothetical protein A2X20_06890
			transl_table	11
34599	33292	CDS
			locus_tag	Hyperionvirus_18_36
			product	hypothetical protein
			transl_table	11
35348	34674	CDS
			locus_tag	Hyperionvirus_18_37
			product	hypothetical protein AYK21_04925
			transl_table	11
36403	35351	CDS
			locus_tag	Hyperionvirus_18_38
			product	hypothetical protein
			transl_table	11
36748	36404	CDS
			locus_tag	Hyperionvirus_18_39
			product	hypothetical protein
			transl_table	11
37047	36937	CDS
			locus_tag	Hyperionvirus_18_40
			product	hypothetical protein
			transl_table	11
37334	37696	CDS
			locus_tag	Hyperionvirus_18_41
			product	hypothetical protein
			transl_table	11
38158	37673	CDS
			locus_tag	Hyperionvirus_18_42
			product	hypothetical protein
			transl_table	11
38337	38206	CDS
			locus_tag	Hyperionvirus_18_43
			product	hypothetical protein
			transl_table	11
38843	38382	CDS
			locus_tag	Hyperionvirus_18_44
			product	MULTISPECIES: ribosomal-protein-alanine N-acetyltransferase
			transl_table	11
38979	39620	CDS
			locus_tag	Hyperionvirus_18_45
			product	hypothetical protein
			transl_table	11
40115	39576	CDS
			locus_tag	Hyperionvirus_18_46
			product	hypothetical protein
			transl_table	11
40556	40167	CDS
			locus_tag	Hyperionvirus_18_47
			product	hypothetical protein
			transl_table	11
41569	40616	CDS
			locus_tag	Hyperionvirus_18_48
			product	hypothetical protein
			transl_table	11
42633	41656	CDS
			locus_tag	Hyperionvirus_18_49
			product	hypothetical protein
			transl_table	11
43279	42683	CDS
			locus_tag	Hyperionvirus_18_50
			product	Ras-related protein Rab-35
			transl_table	11
43378	44724	CDS
			locus_tag	Hyperionvirus_18_51
			product	hypothetical protein
			transl_table	11
44898	44716	CDS
			locus_tag	Hyperionvirus_18_52
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_19
2	97	CDS
			locus_tag	Hyperionvirus_19_1
			product	hypothetical protein
			transl_table	11
322	2046	CDS
			locus_tag	Hyperionvirus_19_2
			product	hypothetical protein Catovirus_2_294
			transl_table	11
2088	3164	CDS
			locus_tag	Hyperionvirus_19_3
			product	hypothetical protein Catovirus_2_292
			transl_table	11
3238	3768	CDS
			locus_tag	Hyperionvirus_19_4
			product	hypothetical protein
			transl_table	11
3796	5460	CDS
			locus_tag	Hyperionvirus_19_5
			product	hypothetical protein
			transl_table	11
5497	5802	CDS
			locus_tag	Hyperionvirus_19_6
			product	hypothetical protein
			transl_table	11
6772	5783	CDS
			locus_tag	Hyperionvirus_19_7
			product	hypothetical protein Catovirus_2_291
			transl_table	11
7125	6805	CDS
			locus_tag	Hyperionvirus_19_8
			product	hypothetical protein
			transl_table	11
7268	7543	CDS
			locus_tag	Hyperionvirus_19_9
			product	hypothetical protein
			transl_table	11
7581	7739	CDS
			locus_tag	Hyperionvirus_19_10
			product	hypothetical protein
			transl_table	11
7841	7951	CDS
			locus_tag	Hyperionvirus_19_11
			product	hypothetical protein
			transl_table	11
8739	8074	CDS
			locus_tag	Hyperionvirus_19_12
			product	hypothetical protein Klosneuvirus_4_137
			transl_table	11
9124	8990	CDS
			locus_tag	Hyperionvirus_19_13
			product	hypothetical protein
			transl_table	11
9123	11186	CDS
			locus_tag	Hyperionvirus_19_14
			product	divergent HNH endonuclease
			transl_table	11
11292	11468	CDS
			locus_tag	Hyperionvirus_19_15
			product	hypothetical protein
			transl_table	11
11739	13550	CDS
			locus_tag	Hyperionvirus_19_16
			product	prolyl 4-hydroxylase alpha subunit
			transl_table	11
14187	13672	CDS
			locus_tag	Hyperionvirus_19_17
			product	hypothetical protein Klosneuvirus_1_39
			transl_table	11
15085	15285	CDS
			locus_tag	Hyperionvirus_19_18
			product	hypothetical protein
			transl_table	11
15768	17165	CDS
			locus_tag	Hyperionvirus_19_19
			product	hypothetical protein Indivirus_4_15
			transl_table	11
17105	18364	CDS
			locus_tag	Hyperionvirus_19_20
			product	divergent HNH endonuclease
			transl_table	11
20880	18997	CDS
			locus_tag	Hyperionvirus_19_21
			product	NCLDV major capsid protein
			transl_table	11
21157	22662	CDS
			locus_tag	Hyperionvirus_19_22
			product	hypothetical protein BNJ_00111
			transl_table	11
22954	22763	CDS
			locus_tag	Hyperionvirus_19_23
			product	hypothetical protein
			transl_table	11
23194	24216	CDS
			locus_tag	Hyperionvirus_19_24
			product	late transcription factor VLTF3-like protein
			transl_table	11
24327	25901	CDS
			locus_tag	Hyperionvirus_19_25
			product	hypothetical protein BNJ_00233
			transl_table	11
26401	26799	CDS
			locus_tag	Hyperionvirus_19_26
			product	late transcription factor VLTF3-like protein
			transl_table	11
27026	28690	CDS
			locus_tag	Hyperionvirus_19_27
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
28800	29189	CDS
			locus_tag	Hyperionvirus_19_28
			product	hypothetical protein
			transl_table	11
29196	29471	CDS
			locus_tag	Hyperionvirus_19_29
			product	hypothetical protein
			transl_table	11
29556	29660	CDS
			locus_tag	Hyperionvirus_19_30
			product	hypothetical protein
			transl_table	11
30181	29657	CDS
			locus_tag	Hyperionvirus_19_31
			product	hypothetical protein
			transl_table	11
30503	30736	CDS
			locus_tag	Hyperionvirus_19_32
			product	hypothetical protein
			transl_table	11
30901	32313	CDS
			locus_tag	Hyperionvirus_19_33
			product	hypothetical protein
			transl_table	11
32825	32310	CDS
			locus_tag	Hyperionvirus_19_34
			product	hypothetical protein
			transl_table	11
34979	33216	CDS
			locus_tag	Hyperionvirus_19_35
			product	MBL fold metallo-hydrolase
			transl_table	11
35020	36519	CDS
			locus_tag	Hyperionvirus_19_36
			product	collagen triple helix repeat containing protein
			transl_table	11
37494	36529	CDS
			locus_tag	Hyperionvirus_19_37
			product	chromosome condensation regulator
			transl_table	11
37623	38471	CDS
			locus_tag	Hyperionvirus_19_38
			product	hypothetical protein GUITHDRAFT_67770
			transl_table	11
39389	38451	CDS
			locus_tag	Hyperionvirus_19_39
			product	hypothetical protein Catovirus_1_66
			transl_table	11
40426	39419	CDS
			locus_tag	Hyperionvirus_19_40
			product	hypothetical protein BGO67_08165
			transl_table	11
40479	40649	CDS
			locus_tag	Hyperionvirus_19_41
			product	hypothetical protein
			transl_table	11
40694	41455	CDS
			locus_tag	Hyperionvirus_19_42
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
42378	41458	CDS
			locus_tag	Hyperionvirus_19_43
			product	packaging ATPase
			transl_table	11
42493	42714	CDS
			locus_tag	Hyperionvirus_19_44
			product	hypothetical protein
			transl_table	11
44053	42830	CDS
			locus_tag	Hyperionvirus_19_45
			product	hypothetical protein Catovirus_2_274
			transl_table	11
44401	44508	CDS
			locus_tag	Hyperionvirus_19_46
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_20
97	2	CDS
			locus_tag	Hyperionvirus_20_1
			product	hypothetical protein
			transl_table	11
566	423	CDS
			locus_tag	Hyperionvirus_20_2
			product	hypothetical protein
			transl_table	11
1073	726	CDS
			locus_tag	Hyperionvirus_20_3
			product	hypothetical protein
			transl_table	11
1329	1213	CDS
			locus_tag	Hyperionvirus_20_4
			product	hypothetical protein
			transl_table	11
2126	1413	CDS
			locus_tag	Hyperionvirus_20_5
			product	putative RNA methylase
			transl_table	11
2607	2167	CDS
			locus_tag	Hyperionvirus_20_6
			product	hypothetical protein Catovirus_2_228
			transl_table	11
2702	7129	CDS
			locus_tag	Hyperionvirus_20_7
			product	DNA polymerase family B elongation subunit
			transl_table	11
7877	7131	CDS
			locus_tag	Hyperionvirus_20_8
			product	hypothetical protein
			transl_table	11
7961	8551	CDS
			locus_tag	Hyperionvirus_20_9
			product	hypothetical protein Catovirus_2_225
			transl_table	11
8552	15025	CDS
			locus_tag	Hyperionvirus_20_10
			product	early transcription factor VETF large subunit
			transl_table	11
15088	15612	CDS
			locus_tag	Hyperionvirus_20_11
			product	hypothetical protein MegaChil _gp0576
			transl_table	11
15636	15926	CDS
			locus_tag	Hyperionvirus_20_12
			product	hypothetical protein
			transl_table	11
15949	16566	CDS
			locus_tag	Hyperionvirus_20_13
			product	hypothetical protein Catovirus_2_221
			transl_table	11
17441	16563	CDS
			locus_tag	Hyperionvirus_20_14
			product	hypothetical protein
			transl_table	11
17501	18394	CDS
			locus_tag	Hyperionvirus_20_15
			product	hypothetical protein Catovirus_2_218
			transl_table	11
18432	19028	CDS
			locus_tag	Hyperionvirus_20_16
			product	hypothetical protein
			transl_table	11
19523	19017	CDS
			locus_tag	Hyperionvirus_20_17
			product	hypothetical protein
			transl_table	11
19634	19536	CDS
			locus_tag	Hyperionvirus_20_18
			product	hypothetical protein
			transl_table	11
19646	21637	CDS
			locus_tag	Hyperionvirus_20_19
			product	cullin family protein
			transl_table	11
22201	21683	CDS
			locus_tag	Hyperionvirus_20_20
			product	hypothetical protein
			transl_table	11
22371	24665	CDS
			locus_tag	Hyperionvirus_20_21
			product	hypothetical protein Catovirus_2_214
			transl_table	11
25311	24667	CDS
			locus_tag	Hyperionvirus_20_22
			product	transcription elongation factor TFIIS
			transl_table	11
25411	26970	CDS
			locus_tag	Hyperionvirus_20_23
			product	hypothetical protein Catovirus_2_211
			transl_table	11
27010	27942	CDS
			locus_tag	Hyperionvirus_20_24
			product	hypothetical protein
			transl_table	11
29144	27939	CDS
			locus_tag	Hyperionvirus_20_25
			product	hypothetical protein Catovirus_2_204
			transl_table	11
29221	31593	CDS
			locus_tag	Hyperionvirus_20_26
			product	putative minor capsid protein
			transl_table	11
31993	31577	CDS
			locus_tag	Hyperionvirus_20_27
			product	hypothetical protein Catovirus_2_202
			transl_table	11
32853	32005	CDS
			locus_tag	Hyperionvirus_20_28
			product	Ulp1 protease
			transl_table	11
33207	32884	CDS
			locus_tag	Hyperionvirus_20_29
			product	hypothetical protein
			transl_table	11
35593	33266	CDS
			locus_tag	Hyperionvirus_20_30
			product	DEAD/SNF2-like helicase
			transl_table	11
35648	36097	CDS
			locus_tag	Hyperionvirus_20_31
			product	response regulator
			transl_table	11
36126	37055	CDS
			locus_tag	Hyperionvirus_20_32
			product	hypothetical protein BMW23_0306
			transl_table	11
37113	38129	CDS
			locus_tag	Hyperionvirus_20_33
			product	hypothetical protein
			transl_table	11
39007	38126	CDS
			locus_tag	Hyperionvirus_20_34
			product	ribonuclease III
			transl_table	11
40868	39066	CDS
			locus_tag	Hyperionvirus_20_35
			product	YqaJ-like viral recombinase domain
			transl_table	11
41224	40910	CDS
			locus_tag	Hyperionvirus_20_36
			product	hypothetical protein
			transl_table	11
41671	41534	CDS
			locus_tag	Hyperionvirus_20_37
			product	hypothetical protein
			transl_table	11
41905	41774	CDS
			locus_tag	Hyperionvirus_20_38
			product	hypothetical protein
			transl_table	11
42118	41957	CDS
			locus_tag	Hyperionvirus_20_39
			product	hypothetical protein
			transl_table	11
42271	42128	CDS
			locus_tag	Hyperionvirus_20_40
			product	hypothetical protein
			transl_table	11
42660	42436	CDS
			locus_tag	Hyperionvirus_20_41
			product	hypothetical protein Catovirus_2_197
			transl_table	11
42801	43304	CDS
			locus_tag	Hyperionvirus_20_42
			product	hypothetical protein Indivirus_3_56
			transl_table	11
43329	43895	CDS
			locus_tag	Hyperionvirus_20_43
			product	hypothetical protein Catovirus_2_192
			transl_table	11
43934	44407	CDS
			locus_tag	Hyperionvirus_20_44
			product	eukaryotic translation initiation factor eIF-2 alpha
			transl_table	11
>Hyperionvirus_21
1429	464	CDS
			locus_tag	Hyperionvirus_21_1
			product	GIY-YIG nuclease
			transl_table	11
1610	3676	CDS
			locus_tag	Hyperionvirus_21_2
			product	hypothetical protein
			transl_table	11
3800	5896	CDS
			locus_tag	Hyperionvirus_21_3
			product	hypothetical protein
			transl_table	11
6505	6329	CDS
			locus_tag	Hyperionvirus_21_4
			product	hypothetical protein
			transl_table	11
6657	6526	CDS
			locus_tag	Hyperionvirus_21_5
			product	hypothetical protein
			transl_table	11
6659	7165	CDS
			locus_tag	Hyperionvirus_21_6
			product	hypothetical protein
			transl_table	11
7446	7147	CDS
			locus_tag	Hyperionvirus_21_7
			product	hypothetical protein
			transl_table	11
7568	7479	CDS
			locus_tag	Hyperionvirus_21_8
			product	hypothetical protein
			transl_table	11
8054	7938	CDS
			locus_tag	Hyperionvirus_21_9
			product	hypothetical protein
			transl_table	11
8396	8175	CDS
			locus_tag	Hyperionvirus_21_10
			product	hypothetical protein
			transl_table	11
9005	8418	CDS
			locus_tag	Hyperionvirus_21_11
			product	hypothetical protein
			transl_table	11
10061	9063	CDS
			locus_tag	Hyperionvirus_21_12
			product	hypothetical protein MegaChil _gp0096
			transl_table	11
12651	10114	CDS
			locus_tag	Hyperionvirus_21_13
			product	endonuclease/exonuclease/phosphatase family
			transl_table	11
12791	13402	CDS
			locus_tag	Hyperionvirus_21_14
			product	hypothetical protein M427DRAFT_52489
			transl_table	11
13467	14528	CDS
			locus_tag	Hyperionvirus_21_15
			product	hypothetical protein CHLRE_02g105350v5
			transl_table	11
15421	14525	CDS
			locus_tag	Hyperionvirus_21_16
			product	hypothetical protein
			transl_table	11
17362	15491	CDS
			locus_tag	Hyperionvirus_21_17
			product	hypothetical protein mv_L924
			transl_table	11
18606	17437	CDS
			locus_tag	Hyperionvirus_21_18
			product	DUF3494 domain-containing protein
			transl_table	11
21827	18675	CDS
			locus_tag	Hyperionvirus_21_19
			product	hypothetical protein A2X86_10845
			transl_table	11
22106	23347	CDS
			locus_tag	Hyperionvirus_21_20
			product	hypothetical protein
			transl_table	11
23378	25891	CDS
			locus_tag	Hyperionvirus_21_21
			product	putative family 25 glycosyltransferase
			transl_table	11
26811	25843	CDS
			locus_tag	Hyperionvirus_21_22
			product	hypothetical protein CBB97_21810
			transl_table	11
27665	26826	CDS
			locus_tag	Hyperionvirus_21_23
			product	hypothetical protein CBD38_00845
			transl_table	11
27750	28913	CDS
			locus_tag	Hyperionvirus_21_24
			product	predicted protein, partial
			transl_table	11
28966	30099	CDS
			locus_tag	Hyperionvirus_21_25
			product	PREDICTED: probable galacturonosyltransferase 6-like
			transl_table	11
30145	31500	CDS
			locus_tag	Hyperionvirus_21_26
			product	hypothetical protein CBD58_01790
			transl_table	11
31518	34058	CDS
			locus_tag	Hyperionvirus_21_27
			product	hypothetical protein CBD58_01790
			transl_table	11
35324	34065	CDS
			locus_tag	Hyperionvirus_21_28
			product	class I SAM-dependent methyltransferase
			transl_table	11
37093	35348	CDS
			locus_tag	Hyperionvirus_21_29
			product	hypothetical protein
			transl_table	11
38220	37129	CDS
			locus_tag	Hyperionvirus_21_30
			product	hypothetical protein
			transl_table	11
39403	38306	CDS
			locus_tag	Hyperionvirus_21_31
			product	hypothetical protein
			transl_table	11
39498	42023	CDS
			locus_tag	Hyperionvirus_21_32
			product	hypothetical protein CBE49_07015
			transl_table	11
42958	42020	CDS
			locus_tag	Hyperionvirus_21_33
			product	hypothetical protein CBC78_05365
			transl_table	11
>Hyperionvirus_22
1187	1017	CDS
			locus_tag	Hyperionvirus_22_1
			product	hypothetical protein
			transl_table	11
1570	2601	CDS
			locus_tag	Hyperionvirus_22_2
			product	hypothetical protein
			transl_table	11
2742	3377	CDS
			locus_tag	Hyperionvirus_22_3
			product	dual specificity phosphatase domain protein
			transl_table	11
3544	3816	CDS
			locus_tag	Hyperionvirus_22_4
			product	hypothetical protein
			transl_table	11
3891	4646	CDS
			locus_tag	Hyperionvirus_22_5
			product	hypothetical protein
			transl_table	11
10729	4643	CDS
			locus_tag	Hyperionvirus_22_6
			product	hypothetical protein Hokovirus_2_216
			transl_table	11
10689	10838	CDS
			locus_tag	Hyperionvirus_22_7
			product	hypothetical protein
			transl_table	11
11820	10897	CDS
			locus_tag	Hyperionvirus_22_8
			product	hypothetical protein Hokovirus_2_216
			transl_table	11
12591	11899	CDS
			locus_tag	Hyperionvirus_22_9
			product	hypothetical protein
			transl_table	11
14310	12652	CDS
			locus_tag	Hyperionvirus_22_10
			product	hypothetical protein Catovirus_2_104
			transl_table	11
15235	14933	CDS
			locus_tag	Hyperionvirus_22_11
			product	hypothetical protein
			transl_table	11
16959	15310	CDS
			locus_tag	Hyperionvirus_22_12
			product	hypothetical protein Catovirus_2_106
			transl_table	11
22282	17003	CDS
			locus_tag	Hyperionvirus_22_13
			product	hypothetical protein
			transl_table	11
23423	22275	CDS
			locus_tag	Hyperionvirus_22_14
			product	hypothetical protein
			transl_table	11
23508	26738	CDS
			locus_tag	Hyperionvirus_22_15
			product	calcineurin-like phosphoesterase
			transl_table	11
27432	26713	CDS
			locus_tag	Hyperionvirus_22_16
			product	hypothetical protein
			transl_table	11
27648	27508	CDS
			locus_tag	Hyperionvirus_22_17
			product	hypothetical protein
			transl_table	11
27979	27743	CDS
			locus_tag	Hyperionvirus_22_18
			product	hypothetical protein
			transl_table	11
28119	28259	CDS
			locus_tag	Hyperionvirus_22_19
			product	hypothetical protein
			transl_table	11
30000	28522	CDS
			locus_tag	Hyperionvirus_22_20
			product	hypothetical protein
			transl_table	11
30704	30051	CDS
			locus_tag	Hyperionvirus_22_21
			product	hypothetical protein
			transl_table	11
31371	30760	CDS
			locus_tag	Hyperionvirus_22_22
			product	hypothetical protein
			transl_table	11
33151	31424	CDS
			locus_tag	Hyperionvirus_22_23
			product	kinase-like protein
			transl_table	11
33894	33739	CDS
			locus_tag	Hyperionvirus_22_24
			product	hypothetical protein
			transl_table	11
34995	34150	CDS
			locus_tag	Hyperionvirus_22_25
			product	hypothetical protein
			transl_table	11
35114	36103	CDS
			locus_tag	Hyperionvirus_22_26
			product	hypothetical protein Klosneuvirus_3_188
			transl_table	11
36820	36098	CDS
			locus_tag	Hyperionvirus_22_27
			product	hypothetical protein
			transl_table	11
36955	37317	CDS
			locus_tag	Hyperionvirus_22_28
			product	hypothetical protein
			transl_table	11
37936	37805	CDS
			locus_tag	Hyperionvirus_22_29
			product	hypothetical protein
			transl_table	11
38534	39427	CDS
			locus_tag	Hyperionvirus_22_30
			product	hypothetical protein
			transl_table	11
39928	39464	CDS
			locus_tag	Hyperionvirus_22_31
			product	hypothetical protein
			transl_table	11
40073	40945	CDS
			locus_tag	Hyperionvirus_22_32
			product	Hypothetical protein ORPV_249
			transl_table	11
41612	40947	CDS
			locus_tag	Hyperionvirus_22_33
			product	hypothetical protein
			transl_table	11
41871	41677	CDS
			locus_tag	Hyperionvirus_22_34
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_23
2	793	CDS
			locus_tag	Hyperionvirus_23_1
			product	MULTISPECIES: FAD-binding oxidoreductase
			transl_table	11
843	1331	CDS
			locus_tag	Hyperionvirus_23_2
			product	hypothetical protein
			transl_table	11
1360	2544	CDS
			locus_tag	Hyperionvirus_23_3
			product	hypothetical protein AR158_C264L
			transl_table	11
2851	3009	CDS
			locus_tag	Hyperionvirus_23_4
			product	hypothetical protein
			transl_table	11
3132	3266	CDS
			locus_tag	Hyperionvirus_23_5
			product	hypothetical protein
			transl_table	11
3235	5454	CDS
			locus_tag	Hyperionvirus_23_6
			product	hypothetical protein
			transl_table	11
5519	6355	CDS
			locus_tag	Hyperionvirus_23_7
			product	hypothetical protein
			transl_table	11
6818	6342	CDS
			locus_tag	Hyperionvirus_23_8
			product	hypothetical protein
			transl_table	11
7884	6958	CDS
			locus_tag	Hyperionvirus_23_9
			product	hypothetical protein
			transl_table	11
8228	8434	CDS
			locus_tag	Hyperionvirus_23_10
			product	hypothetical protein
			transl_table	11
9882	8470	CDS
			locus_tag	Hyperionvirus_23_11
			product	hypothetical protein
			transl_table	11
9965	12352	CDS
			locus_tag	Hyperionvirus_23_12
			product	hypothetical protein PTSG_01414
			transl_table	11
12402	13367	CDS
			locus_tag	Hyperionvirus_23_13
			product	hypothetical protein
			transl_table	11
13419	13769	CDS
			locus_tag	Hyperionvirus_23_14
			product	hypothetical protein
			transl_table	11
14310	13771	CDS
			locus_tag	Hyperionvirus_23_15
			product	hypothetical protein
			transl_table	11
14495	15379	CDS
			locus_tag	Hyperionvirus_23_16
			product	TNF receptor-associated factor 2
			transl_table	11
16879	15368	CDS
			locus_tag	Hyperionvirus_23_17
			product	threonine ammonia-lyase, biosynthetic
			transl_table	11
17819	16953	CDS
			locus_tag	Hyperionvirus_23_18
			product	hypothetical protein
			transl_table	11
18380	17883	CDS
			locus_tag	Hyperionvirus_23_19
			product	hypothetical protein
			transl_table	11
18397	18543	CDS
			locus_tag	Hyperionvirus_23_20
			product	hypothetical protein
			transl_table	11
18748	19752	CDS
			locus_tag	Hyperionvirus_23_21
			product	hypothetical protein
			transl_table	11
20226	19753	CDS
			locus_tag	Hyperionvirus_23_22
			product	hypothetical protein
			transl_table	11
20561	20770	CDS
			locus_tag	Hyperionvirus_23_23
			product	hypothetical protein
			transl_table	11
20972	21121	CDS
			locus_tag	Hyperionvirus_23_24
			product	hypothetical protein
			transl_table	11
21228	21908	CDS
			locus_tag	Hyperionvirus_23_25
			product	hypothetical protein
			transl_table	11
22540	21911	CDS
			locus_tag	Hyperionvirus_23_26
			product	hypothetical protein
			transl_table	11
23029	23445	CDS
			locus_tag	Hyperionvirus_23_27
			product	hypothetical protein
			transl_table	11
23467	24645	CDS
			locus_tag	Hyperionvirus_23_28
			product	PREDICTED: E3 ubiquitin-protein ligase parkin
			transl_table	11
24669	26120	CDS
			locus_tag	Hyperionvirus_23_29
			product	hypothetical protein
			transl_table	11
26654	26746	CDS
			locus_tag	Hyperionvirus_23_30
			product	hypothetical protein
			transl_table	11
26870	26989	CDS
			locus_tag	Hyperionvirus_23_31
			product	hypothetical protein
			transl_table	11
27674	27120	CDS
			locus_tag	Hyperionvirus_23_32
			product	NADPH-dependent FMN reductase
			transl_table	11
27799	27915	CDS
			locus_tag	Hyperionvirus_23_33
			product	hypothetical protein
			transl_table	11
28269	28982	CDS
			locus_tag	Hyperionvirus_23_34
			product	hypothetical protein
			transl_table	11
29884	28979	CDS
			locus_tag	Hyperionvirus_23_35
			product	hypothetical protein
			transl_table	11
31146	29959	CDS
			locus_tag	Hyperionvirus_23_36
			product	hypothetical protein Indivirus_7_20
			transl_table	11
31175	31558	CDS
			locus_tag	Hyperionvirus_23_37
			product	hypothetical protein
			transl_table	11
32713	32868	CDS
			locus_tag	Hyperionvirus_23_38
			product	hypothetical protein
			transl_table	11
33579	34436	CDS
			locus_tag	Hyperionvirus_23_39
			product	hypothetical protein
			transl_table	11
35264	34896	CDS
			locus_tag	Hyperionvirus_23_40
			product	hypothetical protein
			transl_table	11
35626	35375	CDS
			locus_tag	Hyperionvirus_23_41
			product	hypothetical protein
			transl_table	11
35964	35743	CDS
			locus_tag	Hyperionvirus_23_42
			product	hypothetical protein
			transl_table	11
36249	36866	CDS
			locus_tag	Hyperionvirus_23_43
			product	hypothetical protein
			transl_table	11
36851	37024	CDS
			locus_tag	Hyperionvirus_23_44
			product	hypothetical protein
			transl_table	11
37156	37314	CDS
			locus_tag	Hyperionvirus_23_45
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_24
311	3	CDS
			locus_tag	Hyperionvirus_24_1
			product	hypothetical protein
			transl_table	11
399	1622	CDS
			locus_tag	Hyperionvirus_24_2
			product	hypothetical protein
			transl_table	11
2737	1604	CDS
			locus_tag	Hyperionvirus_24_3
			product	hypothetical protein
			transl_table	11
2801	3385	CDS
			locus_tag	Hyperionvirus_24_4
			product	exonuclease
			transl_table	11
3573	4706	CDS
			locus_tag	Hyperionvirus_24_5
			product	hypothetical protein
			transl_table	11
5268	4747	CDS
			locus_tag	Hyperionvirus_24_6
			product	hypothetical protein
			transl_table	11
6302	6766	CDS
			locus_tag	Hyperionvirus_24_7
			product	hypothetical protein
			transl_table	11
12139	8351	CDS
			locus_tag	Hyperionvirus_24_8
			product	hypothetical protein Catovirus_1_615
			transl_table	11
13944	12364	CDS
			locus_tag	Hyperionvirus_24_9
			product	hypothetical protein Catovirus_1_78
			transl_table	11
14264	14175	CDS
			locus_tag	Hyperionvirus_24_10
			product	hypothetical protein
			transl_table	11
15460	14261	CDS
			locus_tag	Hyperionvirus_24_11
			product	hypothetical protein
			transl_table	11
16217	15537	CDS
			locus_tag	Hyperionvirus_24_12
			product	hypothetical protein
			transl_table	11
17016	16282	CDS
			locus_tag	Hyperionvirus_24_13
			product	hypothetical protein
			transl_table	11
17139	18200	CDS
			locus_tag	Hyperionvirus_24_14
			product	hypothetical protein A1O9_01352
			transl_table	11
18256	19374	CDS
			locus_tag	Hyperionvirus_24_15
			product	hypothetical protein
			transl_table	11
19962	20165	CDS
			locus_tag	Hyperionvirus_24_16
			product	hypothetical protein
			transl_table	11
20628	20146	CDS
			locus_tag	Hyperionvirus_24_17
			product	hypothetical protein
			transl_table	11
21582	20698	CDS
			locus_tag	Hyperionvirus_24_18
			product	hypothetical protein
			transl_table	11
21829	22875	CDS
			locus_tag	Hyperionvirus_24_19
			product	hypothetical protein
			transl_table	11
23537	22872	CDS
			locus_tag	Hyperionvirus_24_20
			product	hypothetical protein
			transl_table	11
24302	23628	CDS
			locus_tag	Hyperionvirus_24_21
			product	hypothetical protein
			transl_table	11
25481	24360	CDS
			locus_tag	Hyperionvirus_24_22
			product	hypothetical protein
			transl_table	11
26040	25552	CDS
			locus_tag	Hyperionvirus_24_23
			product	hypothetical protein
			transl_table	11
26629	26075	CDS
			locus_tag	Hyperionvirus_24_24
			product	hypothetical protein
			transl_table	11
26989	26696	CDS
			locus_tag	Hyperionvirus_24_25
			product	hypothetical protein
			transl_table	11
27130	27738	CDS
			locus_tag	Hyperionvirus_24_26
			product	hypothetical protein
			transl_table	11
28607	27735	CDS
			locus_tag	Hyperionvirus_24_27
			product	hypothetical protein
			transl_table	11
28730	29623	CDS
			locus_tag	Hyperionvirus_24_28
			product	hypothetical protein
			transl_table	11
29693	30868	CDS
			locus_tag	Hyperionvirus_24_29
			product	hypothetical protein
			transl_table	11
30906	31847	CDS
			locus_tag	Hyperionvirus_24_30
			product	hypothetical protein BMW23_0564
			transl_table	11
31913	32287	CDS
			locus_tag	Hyperionvirus_24_31
			product	hypothetical protein
			transl_table	11
32346	32930	CDS
			locus_tag	Hyperionvirus_24_32
			product	PREDICTED: RING finger protein 151
			transl_table	11
33956	32931	CDS
			locus_tag	Hyperionvirus_24_33
			product	hypothetical protein
			transl_table	11
33989	34735	CDS
			locus_tag	Hyperionvirus_24_34
			product	hypothetical protein
			transl_table	11
34817	36319	CDS
			locus_tag	Hyperionvirus_24_35
			product	hypothetical protein
			transl_table	11
36881	36321	CDS
			locus_tag	Hyperionvirus_24_36
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_25
2274	118	CDS
			locus_tag	Hyperionvirus_25_1
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
7144	3077	CDS
			locus_tag	Hyperionvirus_25_2
			product	intein-containing DNA-dependent RNA polymerase 2 subunit Rpb2/subunit Rpb4 precursor
			transl_table	11
7991	7218	CDS
			locus_tag	Hyperionvirus_25_3
			product	hypothetical protein
			transl_table	11
8082	8534	CDS
			locus_tag	Hyperionvirus_25_4
			product	hypothetical protein
			transl_table	11
9296	8529	CDS
			locus_tag	Hyperionvirus_25_5
			product	uracil-DNA glycosylase
			transl_table	11
9431	10936	CDS
			locus_tag	Hyperionvirus_25_6
			product	transcription initiation factor IIB
			transl_table	11
11788	10910	CDS
			locus_tag	Hyperionvirus_25_7
			product	hypothetical protein
			transl_table	11
11857	13131	CDS
			locus_tag	Hyperionvirus_25_8
			product	hypothetical protein
			transl_table	11
13167	13643	CDS
			locus_tag	Hyperionvirus_25_9
			product	hypothetical protein
			transl_table	11
14690	13647	CDS
			locus_tag	Hyperionvirus_25_10
			product	hypothetical protein
			transl_table	11
18180	14737	CDS
			locus_tag	Hyperionvirus_25_11
			product	ATP-dependent Lon protease
			transl_table	11
18196	18984	CDS
			locus_tag	Hyperionvirus_25_12
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
19292	19107	CDS
			locus_tag	Hyperionvirus_25_13
			product	hypothetical protein
			transl_table	11
19291	20160	CDS
			locus_tag	Hyperionvirus_25_14
			product	hypothetical protein Catovirus_1_1049
			transl_table	11
20169	20381	CDS
			locus_tag	Hyperionvirus_25_15
			product	hypothetical protein
			transl_table	11
22724	20370	CDS
			locus_tag	Hyperionvirus_25_16
			product	Hsp70 protein
			transl_table	11
23073	22702	CDS
			locus_tag	Hyperionvirus_25_17
			product	Hsp70 protein
			transl_table	11
23489	23118	CDS
			locus_tag	Hyperionvirus_25_18
			product	hypothetical protein
			transl_table	11
24444	23521	CDS
			locus_tag	Hyperionvirus_25_19
			product	hypothetical protein Catovirus_1_1053
			transl_table	11
24529	25482	CDS
			locus_tag	Hyperionvirus_25_20
			product	DnaJ domain protein
			transl_table	11
25501	26751	CDS
			locus_tag	Hyperionvirus_25_21
			product	hypothetical protein crov314
			transl_table	11
26877	28004	CDS
			locus_tag	Hyperionvirus_25_22
			product	PREDICTED: flap endonuclease 1-like
			transl_table	11
28608	27997	CDS
			locus_tag	Hyperionvirus_25_23
			product	hypothetical protein Catovirus_1_1055
			transl_table	11
28749	29270	CDS
			locus_tag	Hyperionvirus_25_24
			product	hypothetical protein BCR33DRAFT_711812
			transl_table	11
32306	29265	CDS
			locus_tag	Hyperionvirus_25_25
			product	DEAD/SNF2-like helicase
			transl_table	11
32838	32383	CDS
			locus_tag	Hyperionvirus_25_26
			product	hypothetical protein
			transl_table	11
33332	32898	CDS
			locus_tag	Hyperionvirus_25_27
			product	hypothetical protein
			transl_table	11
33423	34142	CDS
			locus_tag	Hyperionvirus_25_28
			product	hypothetical protein Catovirus_1_1058
			transl_table	11
34882	34145	CDS
			locus_tag	Hyperionvirus_25_29
			product	hypothetical protein Catovirus_1_1059
			transl_table	11
36026	34947	CDS
			locus_tag	Hyperionvirus_25_30
			product	hypothetical protein Catovirus_1_1060
			transl_table	11
36546	36728	CDS
			locus_tag	Hyperionvirus_25_31
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_26
2	895	CDS
			locus_tag	Hyperionvirus_26_1
			product	hypothetical protein
			transl_table	11
1201	887	CDS
			locus_tag	Hyperionvirus_26_2
			product	hypothetical protein
			transl_table	11
2012	1236	CDS
			locus_tag	Hyperionvirus_26_3
			product	hypothetical protein
			transl_table	11
2105	2215	CDS
			locus_tag	Hyperionvirus_26_4
			product	hypothetical protein
			transl_table	11
3089	3949	CDS
			locus_tag	Hyperionvirus_26_5
			product	hypothetical protein
			transl_table	11
4017	4811	CDS
			locus_tag	Hyperionvirus_26_6
			product	CS-domain-containing protein
			transl_table	11
4864	7341	CDS
			locus_tag	Hyperionvirus_26_7
			product	poly(beta-D-mannuronate) C5 epimerase 7
			transl_table	11
7746	7360	CDS
			locus_tag	Hyperionvirus_26_8
			product	hypothetical protein
			transl_table	11
7966	9147	CDS
			locus_tag	Hyperionvirus_26_9
			product	hypothetical protein
			transl_table	11
9203	10909	CDS
			locus_tag	Hyperionvirus_26_10
			product	hypothetical protein BOTBODRAFT_37317
			transl_table	11
13957	10901	CDS
			locus_tag	Hyperionvirus_26_11
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
14801	14001	CDS
			locus_tag	Hyperionvirus_26_12
			product	hypothetical protein
			transl_table	11
16141	14852	CDS
			locus_tag	Hyperionvirus_26_13
			product	hypothetical protein
			transl_table	11
17538	16147	CDS
			locus_tag	Hyperionvirus_26_14
			product	sel1 repeat family protein
			transl_table	11
17663	18676	CDS
			locus_tag	Hyperionvirus_26_15
			product	hypothetical protein
			transl_table	11
18740	19228	CDS
			locus_tag	Hyperionvirus_26_16
			product	hypothetical protein
			transl_table	11
19281	19991	CDS
			locus_tag	Hyperionvirus_26_17
			product	hypothetical protein
			transl_table	11
20062	20895	CDS
			locus_tag	Hyperionvirus_26_18
			product	alpha/beta hydrolase family protein
			transl_table	11
20967	21824	CDS
			locus_tag	Hyperionvirus_26_19
			product	sel1 repeat family protein
			transl_table	11
23504	21849	CDS
			locus_tag	Hyperionvirus_26_20
			product	hypothetical protein
			transl_table	11
23636	24115	CDS
			locus_tag	Hyperionvirus_26_21
			product	hypothetical protein
			transl_table	11
25051	24110	CDS
			locus_tag	Hyperionvirus_26_22
			product	histidine phosphatase superfamily branch 1
			transl_table	11
25713	25051	CDS
			locus_tag	Hyperionvirus_26_23
			product	hypothetical protein
			transl_table	11
25853	26143	CDS
			locus_tag	Hyperionvirus_26_24
			product	hypothetical protein
			transl_table	11
26841	26134	CDS
			locus_tag	Hyperionvirus_26_25
			product	hypothetical protein
			transl_table	11
26981	28138	CDS
			locus_tag	Hyperionvirus_26_26
			product	hypothetical protein
			transl_table	11
28243	28824	CDS
			locus_tag	Hyperionvirus_26_27
			product	Alkylated DNA repair dioxygenase AlkB
			transl_table	11
29218	28808	CDS
			locus_tag	Hyperionvirus_26_28
			product	hypothetical protein AK88_03458
			transl_table	11
29354	29698	CDS
			locus_tag	Hyperionvirus_26_29
			product	hypothetical protein
			transl_table	11
29740	30339	CDS
			locus_tag	Hyperionvirus_26_30
			product	hypothetical protein COT80_02035
			transl_table	11
31505	30330	CDS
			locus_tag	Hyperionvirus_26_31
			product	hypothetical protein
			transl_table	11
31577	32530	CDS
			locus_tag	Hyperionvirus_26_32
			product	hypothetical protein Indivirus_7_20
			transl_table	11
34610	34783	CDS
			locus_tag	Hyperionvirus_26_33
			product	hypothetical protein
			transl_table	11
35798	35691	CDS
			locus_tag	Hyperionvirus_26_34
			product	hypothetical protein
			transl_table	11
36183	35941	CDS
			locus_tag	Hyperionvirus_26_35
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_27
216	1	CDS
			locus_tag	Hyperionvirus_27_1
			product	hypothetical protein
			transl_table	11
853	263	CDS
			locus_tag	Hyperionvirus_27_2
			product	DUF4291 domain-containing protein
			transl_table	11
2353	923	CDS
			locus_tag	Hyperionvirus_27_3
			product	hypothetical protein Klosneuvirus_10_6
			transl_table	11
2835	3485	CDS
			locus_tag	Hyperionvirus_27_4
			product	hypothetical protein
			transl_table	11
4501	3503	CDS
			locus_tag	Hyperionvirus_27_5
			product	Glycosyltransferase sugar-binding region containing DXD motif-containing protein
			transl_table	11
4669	4511	CDS
			locus_tag	Hyperionvirus_27_6
			product	hypothetical protein
			transl_table	11
4719	5108	CDS
			locus_tag	Hyperionvirus_27_7
			product	hypothetical protein
			transl_table	11
5696	5244	CDS
			locus_tag	Hyperionvirus_27_8
			product	hypothetical protein
			transl_table	11
6058	6705	CDS
			locus_tag	Hyperionvirus_27_9
			product	hypothetical protein
			transl_table	11
7644	6742	CDS
			locus_tag	Hyperionvirus_27_10
			product	hypothetical protein
			transl_table	11
7858	7757	CDS
			locus_tag	Hyperionvirus_27_11
			product	hypothetical protein
			transl_table	11
7938	9275	CDS
			locus_tag	Hyperionvirus_27_12
			product	hypothetical protein
			transl_table	11
9344	10888	CDS
			locus_tag	Hyperionvirus_27_13
			product	hypothetical protein
			transl_table	11
14264	11031	CDS
			locus_tag	Hyperionvirus_27_14
			product	superfamily II helicase
			transl_table	11
15476	14676	CDS
			locus_tag	Hyperionvirus_27_15
			product	hypothetical protein
			transl_table	11
15631	16461	CDS
			locus_tag	Hyperionvirus_27_16
			product	hypothetical protein
			transl_table	11
16530	17996	CDS
			locus_tag	Hyperionvirus_27_17
			product	hypothetical protein
			transl_table	11
18061	18696	CDS
			locus_tag	Hyperionvirus_27_18
			product	hypothetical protein
			transl_table	11
18969	18691	CDS
			locus_tag	Hyperionvirus_27_19
			product	hypothetical protein
			transl_table	11
19433	19570	CDS
			locus_tag	Hyperionvirus_27_20
			product	hypothetical protein
			transl_table	11
22317	22481	CDS
			locus_tag	Hyperionvirus_27_21
			product	hypothetical protein
			transl_table	11
25478	25338	CDS
			locus_tag	Hyperionvirus_27_22
			product	hypothetical protein
			transl_table	11
26933	27142	CDS
			locus_tag	Hyperionvirus_27_23
			product	hypothetical protein
			transl_table	11
27264	28112	CDS
			locus_tag	Hyperionvirus_27_24
			product	PREDICTED: TNF receptor-associated factor 4-like
			transl_table	11
28239	28589	CDS
			locus_tag	Hyperionvirus_27_25
			product	hypothetical protein
			transl_table	11
28967	29785	CDS
			locus_tag	Hyperionvirus_27_26
			product	hypothetical protein
			transl_table	11
30097	30858	CDS
			locus_tag	Hyperionvirus_27_27
			product	hypothetical protein
			transl_table	11
30922	32262	CDS
			locus_tag	Hyperionvirus_27_28
			product	WD domain-containing protein
			transl_table	11
32395	32252	CDS
			locus_tag	Hyperionvirus_27_29
			product	hypothetical protein
			transl_table	11
32651	34012	CDS
			locus_tag	Hyperionvirus_27_30
			product	F-box/WD repeat-containing protein 7
			transl_table	11
34890	34759	CDS
			locus_tag	Hyperionvirus_27_31
			product	hypothetical protein
			transl_table	11
35188	34964	CDS
			locus_tag	Hyperionvirus_27_32
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_28
119	3	CDS
			locus_tag	Hyperionvirus_28_1
			product	hypothetical protein
			transl_table	11
197	2017	CDS
			locus_tag	Hyperionvirus_28_2
			product	acetolactate synthase large subunit
			transl_table	11
2107	3273	CDS
			locus_tag	Hyperionvirus_28_3
			product	hypothetical protein
			transl_table	11
3391	4251	CDS
			locus_tag	Hyperionvirus_28_4
			product	hypothetical protein
			transl_table	11
4539	5420	CDS
			locus_tag	Hyperionvirus_28_5
			product	putative methyltransferase TRM13-like
			transl_table	11
5485	6093	CDS
			locus_tag	Hyperionvirus_28_6
			product	hypothetical protein
			transl_table	11
6885	6106	CDS
			locus_tag	Hyperionvirus_28_7
			product	hypothetical protein
			transl_table	11
7615	7004	CDS
			locus_tag	Hyperionvirus_28_8
			product	hypothetical protein
			transl_table	11
8660	7710	CDS
			locus_tag	Hyperionvirus_28_9
			product	hypothetical protein
			transl_table	11
8936	10738	CDS
			locus_tag	Hyperionvirus_28_10
			product	alpha-L-rhamnosidase
			transl_table	11
11323	10745	CDS
			locus_tag	Hyperionvirus_28_11
			product	hypothetical protein
			transl_table	11
11399	12778	CDS
			locus_tag	Hyperionvirus_28_12
			product	zinc finger, C3HC4 type (RING finger) domain containing protein
			transl_table	11
13966	12785	CDS
			locus_tag	Hyperionvirus_28_13
			product	putative E3 ubiquitin ligase complex SCF subunit sconB
			transl_table	11
14122	14994	CDS
			locus_tag	Hyperionvirus_28_14
			product	hypothetical protein
			transl_table	11
15126	15019	CDS
			locus_tag	Hyperionvirus_28_15
			product	hypothetical protein
			transl_table	11
15129	16235	CDS
			locus_tag	Hyperionvirus_28_16
			product	hypothetical protein AMJ90_01545
			transl_table	11
16296	16922	CDS
			locus_tag	Hyperionvirus_28_17
			product	hypothetical protein
			transl_table	11
17018	17296	CDS
			locus_tag	Hyperionvirus_28_18
			product	hypothetical protein
			transl_table	11
17509	19287	CDS
			locus_tag	Hyperionvirus_28_19
			product	hypothetical protein BOTBODRAFT_37317
			transl_table	11
19343	19807	CDS
			locus_tag	Hyperionvirus_28_20
			product	hypothetical protein
			transl_table	11
21209	19818	CDS
			locus_tag	Hyperionvirus_28_21
			product	chromosome condensation regulator
			transl_table	11
21285	21527	CDS
			locus_tag	Hyperionvirus_28_22
			product	hypothetical protein
			transl_table	11
21613	24384	CDS
			locus_tag	Hyperionvirus_28_23
			product	multicopper oxidase
			transl_table	11
24523	25743	CDS
			locus_tag	Hyperionvirus_28_24
			product	DUF3494 domain-containing protein
			transl_table	11
25780	27615	CDS
			locus_tag	Hyperionvirus_28_25
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
28265	27621	CDS
			locus_tag	Hyperionvirus_28_26
			product	hypothetical protein
			transl_table	11
28403	28633	CDS
			locus_tag	Hyperionvirus_28_27
			product	hypothetical protein
			transl_table	11
28667	29053	CDS
			locus_tag	Hyperionvirus_28_28
			product	hypothetical protein
			transl_table	11
29330	29196	CDS
			locus_tag	Hyperionvirus_28_29
			product	hypothetical protein
			transl_table	11
30861	29572	CDS
			locus_tag	Hyperionvirus_28_30
			product	chromosome condensation regulator
			transl_table	11
30999	31637	CDS
			locus_tag	Hyperionvirus_28_31
			product	hypothetical protein BVRB_038540, partial
			transl_table	11
31708	32220	CDS
			locus_tag	Hyperionvirus_28_32
			product	hypothetical protein
			transl_table	11
32293	32493	CDS
			locus_tag	Hyperionvirus_28_33
			product	hypothetical protein
			transl_table	11
32715	33938	CDS
			locus_tag	Hyperionvirus_28_34
			product	hypothetical protein
			transl_table	11
34661	33969	CDS
			locus_tag	Hyperionvirus_28_35
			product	cupin domain-containing protein
			transl_table	11
>Hyperionvirus_29
1	732	CDS
			locus_tag	Hyperionvirus_29_1
			product	NAD-dependent protein deacetylase sirtuin-1 isoform X1
			transl_table	11
914	729	CDS
			locus_tag	Hyperionvirus_29_2
			product	hypothetical protein
			transl_table	11
1041	1685	CDS
			locus_tag	Hyperionvirus_29_3
			product	HD superfamily phosphohydrolase
			transl_table	11
2138	1704	CDS
			locus_tag	Hyperionvirus_29_4
			product	hypothetical protein
			transl_table	11
2306	4639	CDS
			locus_tag	Hyperionvirus_29_5
			product	hypothetical protein
			transl_table	11
4909	5166	CDS
			locus_tag	Hyperionvirus_29_6
			product	hypothetical protein
			transl_table	11
5619	6398	CDS
			locus_tag	Hyperionvirus_29_7
			product	PREDICTED: E3 ubiquitin-protein ligase MARCH3-like
			transl_table	11
6535	7398	CDS
			locus_tag	Hyperionvirus_29_8
			product	hypothetical protein RirG_255130
			transl_table	11
7591	8115	CDS
			locus_tag	Hyperionvirus_29_9
			product	hypothetical protein
			transl_table	11
8284	8964	CDS
			locus_tag	Hyperionvirus_29_10
			product	hypothetical protein
			transl_table	11
9057	9794	CDS
			locus_tag	Hyperionvirus_29_11
			product	putative orfan
			transl_table	11
11129	9783	CDS
			locus_tag	Hyperionvirus_29_12
			product	hypothetical protein
			transl_table	11
11718	11284	CDS
			locus_tag	Hyperionvirus_29_13
			product	hypothetical protein
			transl_table	11
12173	11730	CDS
			locus_tag	Hyperionvirus_29_14
			product	hypothetical protein
			transl_table	11
12964	12434	CDS
			locus_tag	Hyperionvirus_29_15
			product	hypothetical protein
			transl_table	11
13268	13056	CDS
			locus_tag	Hyperionvirus_29_16
			product	HNH endonuclease
			transl_table	11
13371	13598	CDS
			locus_tag	Hyperionvirus_29_17
			product	hypothetical protein
			transl_table	11
14198	18502	CDS
			locus_tag	Hyperionvirus_29_18
			product	DNA repair exonuclease
			transl_table	11
19253	19501	CDS
			locus_tag	Hyperionvirus_29_19
			product	hypothetical protein
			transl_table	11
19783	19676	CDS
			locus_tag	Hyperionvirus_29_20
			product	hypothetical protein
			transl_table	11
20002	19889	CDS
			locus_tag	Hyperionvirus_29_21
			product	hypothetical protein
			transl_table	11
20260	20108	CDS
			locus_tag	Hyperionvirus_29_22
			product	hypothetical protein
			transl_table	11
21196	20576	CDS
			locus_tag	Hyperionvirus_29_23
			product	hypothetical protein
			transl_table	11
21976	23340	CDS
			locus_tag	Hyperionvirus_29_24
			product	hypothetical protein
			transl_table	11
24088	24453	CDS
			locus_tag	Hyperionvirus_29_25
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
24493	24753	CDS
			locus_tag	Hyperionvirus_29_26
			product	hypothetical protein
			transl_table	11
25716	24838	CDS
			locus_tag	Hyperionvirus_29_27
			product	hypothetical protein
			transl_table	11
26073	25981	CDS
			locus_tag	Hyperionvirus_29_28
			product	hypothetical protein
			transl_table	11
26472	26582	CDS
			locus_tag	Hyperionvirus_29_29
			product	hypothetical protein
			transl_table	11
26892	26797	CDS
			locus_tag	Hyperionvirus_29_30
			product	hypothetical protein
			transl_table	11
28429	29748	CDS
			locus_tag	Hyperionvirus_29_31
			product	ATP-binding protein
			transl_table	11
29726	30016	CDS
			locus_tag	Hyperionvirus_29_32
			product	hypothetical protein
			transl_table	11
30210	30079	CDS
			locus_tag	Hyperionvirus_29_33
			product	hypothetical protein
			transl_table	11
31132	30191	CDS
			locus_tag	Hyperionvirus_29_34
			product	hypothetical protein YSLV5_ORF15
			transl_table	11
31644	31246	CDS
			locus_tag	Hyperionvirus_29_35
			product	hypothetical protein
			transl_table	11
32075	31719	CDS
			locus_tag	Hyperionvirus_29_36
			product	hypothetical protein
			transl_table	11
32615	32484	CDS
			locus_tag	Hyperionvirus_29_37
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_30
3	1733	CDS
			locus_tag	Hyperionvirus_30_1
			product	ankyrin and ring finger domain protein
			transl_table	11
1792	3249	CDS
			locus_tag	Hyperionvirus_30_2
			product	lysyl-tRNA synthetase
			transl_table	11
3312	5057	CDS
			locus_tag	Hyperionvirus_30_3
			product	SWIB/MDM2 domain protein
			transl_table	11
5107	6843	CDS
			locus_tag	Hyperionvirus_30_4
			product	hypothetical protein
			transl_table	11
6890	7813	CDS
			locus_tag	Hyperionvirus_30_5
			product	hypothetical protein
			transl_table	11
8123	7851	CDS
			locus_tag	Hyperionvirus_30_6
			product	hypothetical protein
			transl_table	11
8279	9070	CDS
			locus_tag	Hyperionvirus_30_7
			product	glycosyl transferase, family 25
			transl_table	11
9716	9054	CDS
			locus_tag	Hyperionvirus_30_8
			product	phosphorylcholine metabolism protein LicD
			transl_table	11
9798	12218	CDS
			locus_tag	Hyperionvirus_30_9
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
12629	12213	CDS
			locus_tag	Hyperionvirus_30_10
			product	translation initiation factor 2 subunit beta
			transl_table	11
12718	13641	CDS
			locus_tag	Hyperionvirus_30_11
			product	hypothetical protein
			transl_table	11
13698	14987	CDS
			locus_tag	Hyperionvirus_30_12
			product	DHH family phosphohydrolase
			transl_table	11
15051	15404	CDS
			locus_tag	Hyperionvirus_30_13
			product	hypothetical protein
			transl_table	11
15894	15397	CDS
			locus_tag	Hyperionvirus_30_14
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
15979	18324	CDS
			locus_tag	Hyperionvirus_30_15
			product	DEXDc helicase
			transl_table	11
18345	19403	CDS
			locus_tag	Hyperionvirus_30_16
			product	hypothetical protein
			transl_table	11
19467	20030	CDS
			locus_tag	Hyperionvirus_30_17
			product	hypothetical protein
			transl_table	11
20422	20027	CDS
			locus_tag	Hyperionvirus_30_18
			product	helix-turn-helix protein
			transl_table	11
20559	21692	CDS
			locus_tag	Hyperionvirus_30_19
			product	hypothetical protein
			transl_table	11
21745	23667	CDS
			locus_tag	Hyperionvirus_30_20
			product	PREDICTED: ABC transporter E family member 1-like
			transl_table	11
23733	24557	CDS
			locus_tag	Hyperionvirus_30_21
			product	Clp protease
			transl_table	11
24584	26383	CDS
			locus_tag	Hyperionvirus_30_22
			product	ankyrin repeat protein
			transl_table	11
26389	26967	CDS
			locus_tag	Hyperionvirus_30_23
			product	hypothetical protein Catovirus_2_86
			transl_table	11
26970	27485	CDS
			locus_tag	Hyperionvirus_30_24
			product	ribonuclease H
			transl_table	11
27528	29018	CDS
			locus_tag	Hyperionvirus_30_25
			product	procyclic acidic repetitive protein PARP
			transl_table	11
29040	30005	CDS
			locus_tag	Hyperionvirus_30_26
			product	serine/threonine protein kinase
			transl_table	11
>Hyperionvirus_31
2815	1697	CDS
			locus_tag	Hyperionvirus_31_1
			product	HNH endonuclease
			transl_table	11
3742	3212	CDS
			locus_tag	Hyperionvirus_31_2
			product	hypothetical protein
			transl_table	11
4536	3964	CDS
			locus_tag	Hyperionvirus_31_3
			product	hypothetical protein
			transl_table	11
11252	4557	CDS
			locus_tag	Hyperionvirus_31_4
			product	putative amidoligase enzyme
			transl_table	11
11295	13166	CDS
			locus_tag	Hyperionvirus_31_5
			product	hypothetical protein Klosneuvirus_4_5
			transl_table	11
13447	13169	CDS
			locus_tag	Hyperionvirus_31_6
			product	hypothetical protein
			transl_table	11
14921	13449	CDS
			locus_tag	Hyperionvirus_31_7
			product	choline dehydrogenase
			transl_table	11
15196	14990	CDS
			locus_tag	Hyperionvirus_31_8
			product	hypothetical protein
			transl_table	11
15225	16202	CDS
			locus_tag	Hyperionvirus_31_9
			product	hypothetical protein
			transl_table	11
16463	16173	CDS
			locus_tag	Hyperionvirus_31_10
			product	hypothetical protein
			transl_table	11
16917	16642	CDS
			locus_tag	Hyperionvirus_31_11
			product	hypothetical protein
			transl_table	11
17123	16965	CDS
			locus_tag	Hyperionvirus_31_12
			product	hypothetical protein
			transl_table	11
19575	17377	CDS
			locus_tag	Hyperionvirus_31_13
			product	putative helicase/exonuclease
			transl_table	11
20021	21766	CDS
			locus_tag	Hyperionvirus_31_14
			product	restriction endonuclease
			transl_table	11
24338	21771	CDS
			locus_tag	Hyperionvirus_31_15
			product	mimivirus elongation factor aef-2
			transl_table	11
24494	25210	CDS
			locus_tag	Hyperionvirus_31_16
			product	hypothetical protein
			transl_table	11
25714	25199	CDS
			locus_tag	Hyperionvirus_31_17
			product	hypothetical protein
			transl_table	11
25789	26517	CDS
			locus_tag	Hyperionvirus_31_18
			product	hypothetical protein
			transl_table	11
26814	26668	CDS
			locus_tag	Hyperionvirus_31_19
			product	hypothetical protein
			transl_table	11
26813	27382	CDS
			locus_tag	Hyperionvirus_31_20
			product	hypothetical protein
			transl_table	11
27464	28741	CDS
			locus_tag	Hyperionvirus_31_21
			product	hypothetical protein
			transl_table	11
29077	28958	CDS
			locus_tag	Hyperionvirus_31_22
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_32
92	3	CDS
			locus_tag	Hyperionvirus_32_1
			product	hypothetical protein
			transl_table	11
1174	1076	CDS
			locus_tag	Hyperionvirus_32_2
			product	hypothetical protein
			transl_table	11
1984	1844	CDS
			locus_tag	Hyperionvirus_32_3
			product	hypothetical protein
			transl_table	11
2019	2543	CDS
			locus_tag	Hyperionvirus_32_4
			product	hypothetical protein
			transl_table	11
3302	2709	CDS
			locus_tag	Hyperionvirus_32_5
			product	hypothetical protein
			transl_table	11
3888	4916	CDS
			locus_tag	Hyperionvirus_32_6
			product	hypothetical protein
			transl_table	11
5261	4920	CDS
			locus_tag	Hyperionvirus_32_7
			product	hypothetical protein
			transl_table	11
5955	5311	CDS
			locus_tag	Hyperionvirus_32_8
			product	hypothetical protein
			transl_table	11
8620	6098	CDS
			locus_tag	Hyperionvirus_32_9
			product	UvrD/REP helicase
			transl_table	11
10230	8746	CDS
			locus_tag	Hyperionvirus_32_10
			product	hypothetical protein CVU50_02400
			transl_table	11
11102	10971	CDS
			locus_tag	Hyperionvirus_32_11
			product	hypothetical protein
			transl_table	11
12287	11898	CDS
			locus_tag	Hyperionvirus_32_12
			product	hypothetical protein
			transl_table	11
13188	12379	CDS
			locus_tag	Hyperionvirus_32_13
			product	hypothetical protein
			transl_table	11
13362	14939	CDS
			locus_tag	Hyperionvirus_32_14
			product	hypothetical protein
			transl_table	11
15640	14960	CDS
			locus_tag	Hyperionvirus_32_15
			product	hypothetical protein
			transl_table	11
15868	16554	CDS
			locus_tag	Hyperionvirus_32_16
			product	hypothetical protein
			transl_table	11
16630	17151	CDS
			locus_tag	Hyperionvirus_32_17
			product	hypothetical protein
			transl_table	11
17771	18961	CDS
			locus_tag	Hyperionvirus_32_18
			product	hypothetical protein
			transl_table	11
19791	19021	CDS
			locus_tag	Hyperionvirus_32_19
			product	hypothetical protein Catovirus_1_263
			transl_table	11
20554	19949	CDS
			locus_tag	Hyperionvirus_32_20
			product	hypothetical protein
			transl_table	11
20725	21726	CDS
			locus_tag	Hyperionvirus_32_21
			product	cathepsin L1 isoform X1
			transl_table	11
21978	21730	CDS
			locus_tag	Hyperionvirus_32_22
			product	alpha/beta hydrolase
			transl_table	11
22816	22031	CDS
			locus_tag	Hyperionvirus_32_23
			product	Lipase
			transl_table	11
22955	23779	CDS
			locus_tag	Hyperionvirus_32_24
			product	store-operated calcium entry-associated regulatory factor
			transl_table	11
23924	24307	CDS
			locus_tag	Hyperionvirus_32_25
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_33
598	2	CDS
			locus_tag	Hyperionvirus_33_1
			product	hypothetical protein Indivirus_1_9
			transl_table	11
654	1268	CDS
			locus_tag	Hyperionvirus_33_2
			product	patatin-like phospholipase
			transl_table	11
2379	1270	CDS
			locus_tag	Hyperionvirus_33_3
			product	DnaJ domain protein
			transl_table	11
2458	3552	CDS
			locus_tag	Hyperionvirus_33_4
			product	ADP-ribosylglycohydrolase
			transl_table	11
3905	3549	CDS
			locus_tag	Hyperionvirus_33_5
			product	thioredoxin, putative
			transl_table	11
3972	6254	CDS
			locus_tag	Hyperionvirus_33_6
			product	hypothetical protein Indivirus_1_4
			transl_table	11
6301	7812	CDS
			locus_tag	Hyperionvirus_33_7
			product	NCLDV major capsid protein
			transl_table	11
7872	8513	CDS
			locus_tag	Hyperionvirus_33_8
			product	hypothetical protein
			transl_table	11
8980	8510	CDS
			locus_tag	Hyperionvirus_33_9
			product	polyubiquitin
			transl_table	11
13396	9041	CDS
			locus_tag	Hyperionvirus_33_10
			product	NCLDV major capsid protein
			transl_table	11
14284	13424	CDS
			locus_tag	Hyperionvirus_33_11
			product	NCLDV major capsid protein
			transl_table	11
14709	14876	CDS
			locus_tag	Hyperionvirus_33_12
			product	hypothetical protein
			transl_table	11
15315	15884	CDS
			locus_tag	Hyperionvirus_33_13
			product	hypothetical protein Klosneuvirus_1_59
			transl_table	11
16957	15896	CDS
			locus_tag	Hyperionvirus_33_14
			product	hypothetical protein
			transl_table	11
17132	17983	CDS
			locus_tag	Hyperionvirus_33_15
			product	TATA box binding protein
			transl_table	11
18009	21605	CDS
			locus_tag	Hyperionvirus_33_16
			product	hypothetical protein Catovirus_2_307
			transl_table	11
22219	21602	CDS
			locus_tag	Hyperionvirus_33_17
			product	hypothetical protein Catovirus_2_308
			transl_table	11
22742	22251	CDS
			locus_tag	Hyperionvirus_33_18
			product	PREDICTED: ubiquitin-conjugating enzyme E2 D1
			transl_table	11
23272	22808	CDS
			locus_tag	Hyperionvirus_33_19
			product	hypothetical protein Catovirus_2_310
			transl_table	11
>Hyperionvirus_34
563	3	CDS
			locus_tag	Hyperionvirus_34_1
			product	hypothetical protein
			transl_table	11
1881	625	CDS
			locus_tag	Hyperionvirus_34_2
			product	chromosome condensation regulator
			transl_table	11
2015	2593	CDS
			locus_tag	Hyperionvirus_34_3
			product	hypothetical protein
			transl_table	11
3970	2957	CDS
			locus_tag	Hyperionvirus_34_4
			product	hypothetical protein
			transl_table	11
4092	4970	CDS
			locus_tag	Hyperionvirus_34_5
			product	hypothetical protein
			transl_table	11
5887	4967	CDS
			locus_tag	Hyperionvirus_34_6
			product	hypothetical protein
			transl_table	11
7225	5948	CDS
			locus_tag	Hyperionvirus_34_7
			product	hypothetical protein
			transl_table	11
7289	7723	CDS
			locus_tag	Hyperionvirus_34_8
			product	hypothetical protein
			transl_table	11
7774	8232	CDS
			locus_tag	Hyperionvirus_34_9
			product	hypothetical protein
			transl_table	11
8284	9552	CDS
			locus_tag	Hyperionvirus_34_10
			product	hypothetical protein
			transl_table	11
9545	9982	CDS
			locus_tag	Hyperionvirus_34_11
			product	hypothetical protein
			transl_table	11
11138	9987	CDS
			locus_tag	Hyperionvirus_34_12
			product	unnamed protein product
			transl_table	11
11934	11200	CDS
			locus_tag	Hyperionvirus_34_13
			product	hypothetical protein K493DRAFT_277676
			transl_table	11
13493	12147	CDS
			locus_tag	Hyperionvirus_34_14
			product	hypothetical protein
			transl_table	11
13495	13692	CDS
			locus_tag	Hyperionvirus_34_15
			product	hypothetical protein
			transl_table	11
13739	13906	CDS
			locus_tag	Hyperionvirus_34_16
			product	hypothetical protein
			transl_table	11
13884	14363	CDS
			locus_tag	Hyperionvirus_34_17
			product	PREDICTED: 26S proteasome non-ATPase regulatory subunit 8 homolog A-like
			transl_table	11
14439	15611	CDS
			locus_tag	Hyperionvirus_34_18
			product	uncharacterized protein
			transl_table	11
16365	15622	CDS
			locus_tag	Hyperionvirus_34_19
			product	hypothetical protein
			transl_table	11
16693	17382	CDS
			locus_tag	Hyperionvirus_34_20
			product	hypothetical protein
			transl_table	11
18168	17395	CDS
			locus_tag	Hyperionvirus_34_21
			product	hypothetical protein Ecym_4629
			transl_table	11
19083	18250	CDS
			locus_tag	Hyperionvirus_34_22
			product	Scavenger mRNA-decapping enzyme DcpS, partial
			transl_table	11
19139	19333	CDS
			locus_tag	Hyperionvirus_34_23
			product	hypothetical protein
			transl_table	11
19315	19464	CDS
			locus_tag	Hyperionvirus_34_24
			product	hypothetical protein
			transl_table	11
20074	19439	CDS
			locus_tag	Hyperionvirus_34_25
			product	hypothetical protein
			transl_table	11
20335	20099	CDS
			locus_tag	Hyperionvirus_34_26
			product	hypothetical protein
			transl_table	11
21618	20416	CDS
			locus_tag	Hyperionvirus_34_27
			product	hypothetical protein
			transl_table	11
21756	22058	CDS
			locus_tag	Hyperionvirus_34_28
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_35
3393	1	CDS
			locus_tag	Hyperionvirus_35_1
			product	hypothetical protein Hokovirus_2_216
			transl_table	11
4494	3460	CDS
			locus_tag	Hyperionvirus_35_2
			product	glycoside hydrolase
			transl_table	11
4567	6009	CDS
			locus_tag	Hyperionvirus_35_3
			product	hypothetical protein CSB55_06140
			transl_table	11
6094	6480	CDS
			locus_tag	Hyperionvirus_35_4
			product	hypothetical protein
			transl_table	11
7880	6477	CDS
			locus_tag	Hyperionvirus_35_5
			product	hypothetical protein
			transl_table	11
7996	9345	CDS
			locus_tag	Hyperionvirus_35_6
			product	hypothetical protein
			transl_table	11
9491	9342	CDS
			locus_tag	Hyperionvirus_35_7
			product	hypothetical protein
			transl_table	11
9570	10277	CDS
			locus_tag	Hyperionvirus_35_8
			product	antibiotic biosynthesis monooxygenase
			transl_table	11
10355	11749	CDS
			locus_tag	Hyperionvirus_35_9
			product	hypothetical protein
			transl_table	11
12774	11746	CDS
			locus_tag	Hyperionvirus_35_10
			product	TIGR03118 family protein
			transl_table	11
12874	13827	CDS
			locus_tag	Hyperionvirus_35_11
			product	hypothetical protein CE11_00132
			transl_table	11
13897	14733	CDS
			locus_tag	Hyperionvirus_35_12
			product	hypothetical protein
			transl_table	11
15903	14728	CDS
			locus_tag	Hyperionvirus_35_13
			product	chromosome condensation regulator
			transl_table	11
17916	16093	CDS
			locus_tag	Hyperionvirus_35_14
			product	hypothetical protein
			transl_table	11
17977	18270	CDS
			locus_tag	Hyperionvirus_35_15
			product	hypothetical protein
			transl_table	11
18299	18589	CDS
			locus_tag	Hyperionvirus_35_16
			product	hypothetical protein
			transl_table	11
19514	19813	CDS
			locus_tag	Hyperionvirus_35_17
			product	DNA polymerase family B elongation subunit
			transl_table	11
21378	19831	CDS
			locus_tag	Hyperionvirus_35_18
			product	hypothetical protein
			transl_table	11
21502	21765	CDS
			locus_tag	Hyperionvirus_35_19
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_36
2522	2388	CDS
			locus_tag	Hyperionvirus_36_1
			product	hypothetical protein
			transl_table	11
2572	3222	CDS
			locus_tag	Hyperionvirus_36_2
			product	hypothetical protein
			transl_table	11
3406	3573	CDS
			locus_tag	Hyperionvirus_36_3
			product	hypothetical protein
			transl_table	11
4048	4236	CDS
			locus_tag	Hyperionvirus_36_4
			product	hypothetical protein
			transl_table	11
4245	4865	CDS
			locus_tag	Hyperionvirus_36_5
			product	hypothetical protein
			transl_table	11
5059	4862	CDS
			locus_tag	Hyperionvirus_36_6
			product	hypothetical protein
			transl_table	11
5207	5311	CDS
			locus_tag	Hyperionvirus_36_7
			product	hypothetical protein
			transl_table	11
5308	5904	CDS
			locus_tag	Hyperionvirus_36_8
			product	hypothetical protein B5M49_02375
			transl_table	11
7646	5928	CDS
			locus_tag	Hyperionvirus_36_9
			product	prolyl 4-hydroxylase alpha subunit
			transl_table	11
8458	7700	CDS
			locus_tag	Hyperionvirus_36_10
			product	hypothetical protein
			transl_table	11
9487	8507	CDS
			locus_tag	Hyperionvirus_36_11
			product	hypothetical protein
			transl_table	11
9590	9775	CDS
			locus_tag	Hyperionvirus_36_12
			product	hypothetical protein
			transl_table	11
9835	9966	CDS
			locus_tag	Hyperionvirus_36_13
			product	hypothetical protein
			transl_table	11
10315	9950	CDS
			locus_tag	Hyperionvirus_36_14
			product	hypothetical protein
			transl_table	11
10407	10667	CDS
			locus_tag	Hyperionvirus_36_15
			product	hypothetical protein
			transl_table	11
10705	11664	CDS
			locus_tag	Hyperionvirus_36_16
			product	hypothetical protein
			transl_table	11
11755	11973	CDS
			locus_tag	Hyperionvirus_36_17
			product	hypothetical protein
			transl_table	11
12064	12750	CDS
			locus_tag	Hyperionvirus_36_18
			product	hypothetical protein
			transl_table	11
13348	14637	CDS
			locus_tag	Hyperionvirus_36_19
			product	hypothetical protein
			transl_table	11
15388	14612	CDS
			locus_tag	Hyperionvirus_36_20
			product	hypothetical protein
			transl_table	11
15531	15809	CDS
			locus_tag	Hyperionvirus_36_21
			product	hypothetical protein
			transl_table	11
16661	15786	CDS
			locus_tag	Hyperionvirus_36_22
			product	hypothetical protein CBC91_00690
			transl_table	11
18154	16685	CDS
			locus_tag	Hyperionvirus_36_23
			product	hypothetical protein
			transl_table	11
18429	18340	CDS
			locus_tag	Hyperionvirus_36_24
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_37
565	80	CDS
			locus_tag	Hyperionvirus_37_1
			product	hypothetical protein Hokovirus_2_164
			transl_table	11
1754	591	CDS
			locus_tag	Hyperionvirus_37_2
			product	hypothetical protein Catovirus_2_272
			transl_table	11
1806	2303	CDS
			locus_tag	Hyperionvirus_37_3
			product	hypothetical protein Klosneuvirus_1_13
			transl_table	11
2335	3093	CDS
			locus_tag	Hyperionvirus_37_4
			product	hypothetical protein Catovirus_2_270
			transl_table	11
3122	4705	CDS
			locus_tag	Hyperionvirus_37_5
			product	replication factor C large subunit
			transl_table	11
4763	6514	CDS
			locus_tag	Hyperionvirus_37_6
			product	P4B major core protein
			transl_table	11
7086	6511	CDS
			locus_tag	Hyperionvirus_37_7
			product	hypothetical protein
			transl_table	11
7660	7154	CDS
			locus_tag	Hyperionvirus_37_8
			product	hypothetical protein Catovirus_2_265
			transl_table	11
9333	7687	CDS
			locus_tag	Hyperionvirus_37_9
			product	hypothetical protein Catovirus_2_262
			transl_table	11
9439	9585	CDS
			locus_tag	Hyperionvirus_37_10
			product	hypothetical protein
			transl_table	11
12302	9879	CDS
			locus_tag	Hyperionvirus_37_11
			product	divergent protein kinase
			transl_table	11
13637	12330	CDS
			locus_tag	Hyperionvirus_37_12
			product	serine/threonine protein kinase
			transl_table	11
15442	13688	CDS
			locus_tag	Hyperionvirus_37_13
			product	XRN 5'-3' exonuclease
			transl_table	11
15526	16515	CDS
			locus_tag	Hyperionvirus_37_14
			product	hypothetical protein
			transl_table	11
17630	16509	CDS
			locus_tag	Hyperionvirus_37_15
			product	metallophosphatase/phosphoesterase
			transl_table	11
>Hyperionvirus_38
1	294	CDS
			locus_tag	Hyperionvirus_38_1
			product	hypothetical protein
			transl_table	11
337	2823	CDS
			locus_tag	Hyperionvirus_38_2
			product	leucyl-tRNA synthetase
			transl_table	11
2810	6058	CDS
			locus_tag	Hyperionvirus_38_3
			product	DEAD-like helicases family protein
			transl_table	11
6125	6790	CDS
			locus_tag	Hyperionvirus_38_4
			product	ubiquitin family protein
			transl_table	11
7614	7195	CDS
			locus_tag	Hyperionvirus_38_5
			product	hypothetical protein Catovirus_2_73
			transl_table	11
7745	9652	CDS
			locus_tag	Hyperionvirus_38_6
			product	AFG3-like protein 1 isoform X2
			transl_table	11
9706	10071	CDS
			locus_tag	Hyperionvirus_38_7
			product	hypothetical protein BRD49_03370
			transl_table	11
10150	11640	CDS
			locus_tag	Hyperionvirus_38_8
			product	PREDICTED: E3 ubiquitin-protein ligase RNF216, partial
			transl_table	11
11671	12516	CDS
			locus_tag	Hyperionvirus_38_9
			product	histidine phosphatase domain-containing protein
			transl_table	11
12524	12946	CDS
			locus_tag	Hyperionvirus_38_10
			product	protein D2-like isoform X1
			transl_table	11
13407	12943	CDS
			locus_tag	Hyperionvirus_38_11
			product	hypothetical protein
			transl_table	11
14147	13476	CDS
			locus_tag	Hyperionvirus_38_12
			product	hypothetical protein SteCoe_21819
			transl_table	11
14859	14203	CDS
			locus_tag	Hyperionvirus_38_13
			product	hypothetical protein BGO43_09925
			transl_table	11
14916	15314	CDS
			locus_tag	Hyperionvirus_38_14
			product	hypothetical protein
			transl_table	11
15733	15272	CDS
			locus_tag	Hyperionvirus_38_15
			product	hypothetical protein
			transl_table	11
15723	15896	CDS
			locus_tag	Hyperionvirus_38_16
			product	hypothetical protein
			transl_table	11
15942	16184	CDS
			locus_tag	Hyperionvirus_38_17
			product	hypothetical protein
			transl_table	11
16676	16930	CDS
			locus_tag	Hyperionvirus_38_18
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_39
3	131	CDS
			locus_tag	Hyperionvirus_39_1
			product	hypothetical protein
			transl_table	11
1575	133	CDS
			locus_tag	Hyperionvirus_39_2
			product	hypothetical protein
			transl_table	11
2515	1814	CDS
			locus_tag	Hyperionvirus_39_3
			product	hypothetical protein
			transl_table	11
2955	2608	CDS
			locus_tag	Hyperionvirus_39_4
			product	hypothetical protein
			transl_table	11
3141	3863	CDS
			locus_tag	Hyperionvirus_39_5
			product	hypothetical protein
			transl_table	11
4365	4231	CDS
			locus_tag	Hyperionvirus_39_6
			product	hypothetical protein
			transl_table	11
7625	4593	CDS
			locus_tag	Hyperionvirus_39_7
			product	DNA primase
			transl_table	11
8112	7696	CDS
			locus_tag	Hyperionvirus_39_8
			product	hypothetical protein Catovirus_1_617
			transl_table	11
10749	10639	CDS
			locus_tag	Hyperionvirus_39_9
			product	hypothetical protein
			transl_table	11
11190	11074	CDS
			locus_tag	Hyperionvirus_39_10
			product	hypothetical protein
			transl_table	11
11623	11730	CDS
			locus_tag	Hyperionvirus_39_11
			product	hypothetical protein
			transl_table	11
13015	12869	CDS
			locus_tag	Hyperionvirus_39_12
			product	hypothetical protein
			transl_table	11
13193	13327	CDS
			locus_tag	Hyperionvirus_39_13
			product	hypothetical protein
			transl_table	11
13701	13793	CDS
			locus_tag	Hyperionvirus_39_14
			product	hypothetical protein
			transl_table	11
14337	14173	CDS
			locus_tag	Hyperionvirus_39_15
			product	hypothetical protein
			transl_table	11
15899	15258	CDS
			locus_tag	Hyperionvirus_39_16
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_40
1965	2054	tRNA	Hyperionvirus_tRNA_27
			product	tRNA-Undet(???)
2	187	CDS
			locus_tag	Hyperionvirus_40_1
			product	hypothetical protein
			transl_table	11
1084	167	CDS
			locus_tag	Hyperionvirus_40_2
			product	hypothetical protein
			transl_table	11
1839	1066	CDS
			locus_tag	Hyperionvirus_40_3
			product	hypothetical protein
			transl_table	11
2982	2272	CDS
			locus_tag	Hyperionvirus_40_4
			product	hypothetical protein
			transl_table	11
3529	3050	CDS
			locus_tag	Hyperionvirus_40_5
			product	hypothetical protein
			transl_table	11
4592	3600	CDS
			locus_tag	Hyperionvirus_40_6
			product	hypothetical protein SAMN05428972_3696
			transl_table	11
5894	4626	CDS
			locus_tag	Hyperionvirus_40_7
			product	hypothetical protein
			transl_table	11
6227	5919	CDS
			locus_tag	Hyperionvirus_40_8
			product	hypothetical protein
			transl_table	11
6923	6258	CDS
			locus_tag	Hyperionvirus_40_9
			product	hypothetical protein
			transl_table	11
7240	6986	CDS
			locus_tag	Hyperionvirus_40_10
			product	hypothetical protein
			transl_table	11
7993	7331	CDS
			locus_tag	Hyperionvirus_40_11
			product	hypothetical protein
			transl_table	11
8530	8330	CDS
			locus_tag	Hyperionvirus_40_12
			product	hypothetical protein
			transl_table	11
9493	9774	CDS
			locus_tag	Hyperionvirus_40_13
			product	hypothetical protein
			transl_table	11
10116	10361	CDS
			locus_tag	Hyperionvirus_40_14
			product	hypothetical protein
			transl_table	11
10435	11223	CDS
			locus_tag	Hyperionvirus_40_15
			product	hypothetical protein
			transl_table	11
12622	11228	CDS
			locus_tag	Hyperionvirus_40_16
			product	hypothetical protein
			transl_table	11
12793	13071	CDS
			locus_tag	Hyperionvirus_40_17
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_41
2	2512	CDS
			locus_tag	Hyperionvirus_41_1
			product	DNA topoisomerase IA
			transl_table	11
2574	3005	CDS
			locus_tag	Hyperionvirus_41_2
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
3070	3681	CDS
			locus_tag	Hyperionvirus_41_3
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
3750	6482	CDS
			locus_tag	Hyperionvirus_41_4
			product	D5-like helicase-primase
			transl_table	11
6554	7027	CDS
			locus_tag	Hyperionvirus_41_5
			product	hypothetical protein
			transl_table	11
7482	7369	CDS
			locus_tag	Hyperionvirus_41_6
			product	hypothetical protein
			transl_table	11
7481	9379	CDS
			locus_tag	Hyperionvirus_41_7
			product	ADP-ribosyltransferase exoenzyme domain protein
			transl_table	11
10292	9372	CDS
			locus_tag	Hyperionvirus_41_8
			product	hypothetical protein
			transl_table	11
10377	10637	CDS
			locus_tag	Hyperionvirus_41_9
			product	hypothetical protein
			transl_table	11
12031	10646	CDS
			locus_tag	Hyperionvirus_41_10
			product	serine/threonine protein kinase
			transl_table	11
12439	12732	CDS
			locus_tag	Hyperionvirus_41_11
			product	hypothetical protein Indivirus_1_125
			transl_table	11
>Hyperionvirus_42
621	508	CDS
			locus_tag	Hyperionvirus_42_1
			product	hypothetical protein
			transl_table	11
3491	3345	CDS
			locus_tag	Hyperionvirus_42_2
			product	hypothetical protein
			transl_table	11
3646	4197	CDS
			locus_tag	Hyperionvirus_42_3
			product	hypothetical protein
			transl_table	11
4252	4962	CDS
			locus_tag	Hyperionvirus_42_4
			product	hypothetical protein
			transl_table	11
5042	5665	CDS
			locus_tag	Hyperionvirus_42_5
			product	hypothetical protein
			transl_table	11
6227	5907	CDS
			locus_tag	Hyperionvirus_42_6
			product	hypothetical protein
			transl_table	11
6983	6369	CDS
			locus_tag	Hyperionvirus_42_7
			product	hypothetical protein
			transl_table	11
7312	7166	CDS
			locus_tag	Hyperionvirus_42_8
			product	hypothetical protein
			transl_table	11
7322	8740	CDS
			locus_tag	Hyperionvirus_42_9
			product	hypothetical protein
			transl_table	11
9360	9178	CDS
			locus_tag	Hyperionvirus_42_10
			product	hypothetical protein
			transl_table	11
9425	10594	CDS
			locus_tag	Hyperionvirus_42_11
			product	hypothetical protein
			transl_table	11
10794	10615	CDS
			locus_tag	Hyperionvirus_42_12
			product	hypothetical protein
			transl_table	11
11401	11174	CDS
			locus_tag	Hyperionvirus_42_13
			product	hypothetical protein Indivirus_1_31
			transl_table	11
>Hyperionvirus_43
2	3559	CDS
			locus_tag	Hyperionvirus_43_1
			product	hypothetical protein Catovirus_2_323
			transl_table	11
3582	4499	CDS
			locus_tag	Hyperionvirus_43_2
			product	hypothetical protein Catovirus_2_325
			transl_table	11
6188	4485	CDS
			locus_tag	Hyperionvirus_43_3
			product	asparagine synthase (glutamine-hydrolysing)
			transl_table	11
7434	6238	CDS
			locus_tag	Hyperionvirus_43_4
			product	replication factor C small subunit
			transl_table	11
7800	7922	CDS
			locus_tag	Hyperionvirus_43_5
			product	apurinic endonuclease
			transl_table	11
>Hyperionvirus_44
890	84	CDS
			locus_tag	Hyperionvirus_44_1
			product	hypothetical protein Catovirus_2_311
			transl_table	11
1063	890	CDS
			locus_tag	Hyperionvirus_44_2
			product	hypothetical protein
			transl_table	11
1535	1434	CDS
			locus_tag	Hyperionvirus_44_3
			product	hypothetical protein
			transl_table	11
2869	1574	CDS
			locus_tag	Hyperionvirus_44_4
			product	serine proteinase inhibitor
			transl_table	11
2931	3998	CDS
			locus_tag	Hyperionvirus_44_5
			product	polynucleotide phosphatase/kinase
			transl_table	11
5036	3990	CDS
			locus_tag	Hyperionvirus_44_6
			product	tyrosine--tRNA ligase, cytoplasmic
			transl_table	11
5543	5085	CDS
			locus_tag	Hyperionvirus_44_7
			product	hypothetical protein Catovirus_2_315
			transl_table	11
6241	5567	CDS
			locus_tag	Hyperionvirus_44_8
			product	hypothetical protein
			transl_table	11
7592	6294	CDS
			locus_tag	Hyperionvirus_44_9
			product	hypothetical protein Catovirus_2_317
			transl_table	11
>Hyperionvirus_45
424	224	CDS
			locus_tag	Hyperionvirus_45_1
			product	hypothetical protein
			transl_table	11
635	1897	CDS
			locus_tag	Hyperionvirus_45_2
			product	alanyl-tRNA synthetase, putative
			transl_table	11
2097	1894	CDS
			locus_tag	Hyperionvirus_45_3
			product	hypothetical protein
			transl_table	11
2199	3182	CDS
			locus_tag	Hyperionvirus_45_4
			product	hypothetical protein THAOC_37898, partial
			transl_table	11
3244	3357	CDS
			locus_tag	Hyperionvirus_45_5
			product	hypothetical protein
			transl_table	11
3529	3425	CDS
			locus_tag	Hyperionvirus_45_6
			product	hypothetical protein
			transl_table	11
3814	4407	CDS
			locus_tag	Hyperionvirus_45_7
			product	hypothetical protein
			transl_table	11
4680	4564	CDS
			locus_tag	Hyperionvirus_45_8
			product	hypothetical protein
			transl_table	11
4770	4681	CDS
			locus_tag	Hyperionvirus_45_9
			product	hypothetical protein
			transl_table	11
4974	4879	CDS
			locus_tag	Hyperionvirus_45_10
			product	hypothetical protein
			transl_table	11
5069	4977	CDS
			locus_tag	Hyperionvirus_45_11
			product	hypothetical protein
			transl_table	11
5381	5262	CDS
			locus_tag	Hyperionvirus_45_12
			product	hypothetical protein
			transl_table	11
6515	6952	CDS
			locus_tag	Hyperionvirus_45_13
			product	uvrD/REp helicase family protein
			transl_table	11
>Hyperionvirus_46
2177	228	CDS
			locus_tag	Hyperionvirus_46_1
			product	PREDICTED: heat shock 70 kDa protein
			transl_table	11
2315	3409	CDS
			locus_tag	Hyperionvirus_46_2
			product	replication factor C small subunit
			transl_table	11
4783	3374	CDS
			locus_tag	Hyperionvirus_46_3
			product	HD phosphohydrolase
			transl_table	11
5877	4837	CDS
			locus_tag	Hyperionvirus_46_4
			product	superfamily II DNA or RNA helicase
			transl_table	11
>Hyperionvirus_47
3	179	CDS
			locus_tag	Hyperionvirus_47_1
			product	hypothetical protein
			transl_table	11
1594	176	CDS
			locus_tag	Hyperionvirus_47_2
			product	thymidylate synthase
			transl_table	11
1631	2524	CDS
			locus_tag	Hyperionvirus_47_3
			product	alkaline phosphatase
			transl_table	11
2722	2504	CDS
			locus_tag	Hyperionvirus_47_4
			product	hypothetical protein
			transl_table	11
3569	2751	CDS
			locus_tag	Hyperionvirus_47_5
			product	FLAP-like endonuclease XPG
			transl_table	11
3992	3774	CDS
			locus_tag	Hyperionvirus_47_6
			product	hypothetical protein
			transl_table	11
4290	4054	CDS
			locus_tag	Hyperionvirus_47_7
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_48
1	117	CDS
			locus_tag	Hyperionvirus_48_1
			product	hypothetical protein
			transl_table	11
502	597	CDS
			locus_tag	Hyperionvirus_48_2
			product	hypothetical protein
			transl_table	11
625	855	CDS
			locus_tag	Hyperionvirus_48_3
			product	hypothetical protein
			transl_table	11
1908	1477	CDS
			locus_tag	Hyperionvirus_48_4
			product	hypothetical protein
			transl_table	11
1986	2090	CDS
			locus_tag	Hyperionvirus_48_5
			product	hypothetical protein
			transl_table	11
2103	2972	CDS
			locus_tag	Hyperionvirus_48_6
			product	hypothetical protein
			transl_table	11
4219	3509	CDS
			locus_tag	Hyperionvirus_48_7
			product	hypothetical protein
			transl_table	11
4441	4590	CDS
			locus_tag	Hyperionvirus_48_8
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_49
1178	3	CDS
			locus_tag	Hyperionvirus_49_1
			product	hypothetical protein
			transl_table	11
1719	1252	CDS
			locus_tag	Hyperionvirus_49_2
			product	hypothetical protein
			transl_table	11
1847	3106	CDS
			locus_tag	Hyperionvirus_49_3
			product	chromosome condensation regulator, partial
			transl_table	11
3167	4363	CDS
			locus_tag	Hyperionvirus_49_4
			product	hypothetical protein
			transl_table	11
4547	4341	CDS
			locus_tag	Hyperionvirus_49_5
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_50
628	5	CDS
			locus_tag	Hyperionvirus_50_1
			product	hypothetical protein Indivirus_1_43
			transl_table	11
732	1775	CDS
			locus_tag	Hyperionvirus_50_2
			product	DNA directed RNA polymerase subunit L
			transl_table	11
3069	1768	CDS
			locus_tag	Hyperionvirus_50_3
			product	RNA ligase
			transl_table	11
4126	3956	CDS
			locus_tag	Hyperionvirus_50_4
			product	hypothetical protein
			transl_table	11
4118	4315	CDS
			locus_tag	Hyperionvirus_50_5
			product	hypothetical protein Catovirus_2_323
			transl_table	11
>Hyperionvirus_51
394	2	CDS
			locus_tag	Hyperionvirus_51_1
			product	hypothetical protein
			transl_table	11
796	479	CDS
			locus_tag	Hyperionvirus_51_2
			product	hypothetical protein
			transl_table	11
946	1668	CDS
			locus_tag	Hyperionvirus_51_3
			product	hypothetical protein
			transl_table	11
2263	1658	CDS
			locus_tag	Hyperionvirus_51_4
			product	hypothetical protein
			transl_table	11
3157	2963	CDS
			locus_tag	Hyperionvirus_51_5
			product	hypothetical protein
			transl_table	11
3156	3746	CDS
			locus_tag	Hyperionvirus_51_6
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_52
589	2	CDS
			locus_tag	Hyperionvirus_52_1
			product	hypothetical protein Indivirus_1_31
			transl_table	11
968	636	CDS
			locus_tag	Hyperionvirus_52_2
			product	transposase
			transl_table	11
1629	1267	CDS
			locus_tag	Hyperionvirus_52_3
			product	hypothetical protein
			transl_table	11
2169	1741	CDS
			locus_tag	Hyperionvirus_52_4
			product	hypothetical protein
			transl_table	11
3293	2286	CDS
			locus_tag	Hyperionvirus_52_5
			product	uncharacterized HNH endonuclease
			transl_table	11
3710	3336	CDS
			locus_tag	Hyperionvirus_52_6
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_53
1	108	CDS
			locus_tag	Hyperionvirus_53_1
			product	hypothetical protein
			transl_table	11
138	980	CDS
			locus_tag	Hyperionvirus_53_2
			product	hypothetical protein Catovirus_2_243
			transl_table	11
1795	977	CDS
			locus_tag	Hyperionvirus_53_3
			product	serine/threonine protein phosphatase
			transl_table	11
3116	1827	CDS
			locus_tag	Hyperionvirus_53_4
			product	eukaryotic translation initiation factor 2 gamma subunit
			transl_table	11
3592	3167	CDS
			locus_tag	Hyperionvirus_53_5
			product	hypothetical protein
			transl_table	11
>Hyperionvirus_54
468	1	CDS
			locus_tag	Hyperionvirus_54_1
			product	hypothetical protein DOTSEDRAFT_67521
			transl_table	11
594	1445	CDS
			locus_tag	Hyperionvirus_54_2
			product	hypothetical protein MegaChil _gp0690
			transl_table	11
1494	2216	CDS
			locus_tag	Hyperionvirus_54_3
			product	hypothetical protein
			transl_table	11
2263	3156	CDS
			locus_tag	Hyperionvirus_54_4
			product	hypothetical protein
			transl_table	11
