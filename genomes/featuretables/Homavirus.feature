>Homavirus_1
1	1719	CDS
			locus_tag	Homavirus_1_1
			product	dehydrogenase/oxidoreductase
			transl_table	11
1809	2180	CDS
			locus_tag	Homavirus_1_2
			product	hypothetical protein
			transl_table	11
2360	3271	CDS
			locus_tag	Homavirus_1_3
			product	hypothetical protein
			transl_table	11
3337	5892	CDS
			locus_tag	Homavirus_1_4
			product	phosphoenolpyruvate synthase
			transl_table	11
5927	6712	CDS
			locus_tag	Homavirus_1_5
			product	hypothetical protein Indivirus_2_102
			transl_table	11
7987	6770	CDS
			locus_tag	Homavirus_1_6
			product	hypothetical protein Hokovirus_1_151
			transl_table	11
8115	10841	CDS
			locus_tag	Homavirus_1_7
			product	leucyl-tRNA synthetase
			transl_table	11
11113	14580	CDS
			locus_tag	Homavirus_1_8
			product	hypothetical protein Indivirus_2_103
			transl_table	11
15391	14591	CDS
			locus_tag	Homavirus_1_9
			product	hypothetical protein Klosneuvirus_6_3
			transl_table	11
15864	15553	CDS
			locus_tag	Homavirus_1_10
			product	hypothetical protein
			transl_table	11
16020	16514	CDS
			locus_tag	Homavirus_1_11
			product	hypothetical protein Klosneuvirus_6_6
			transl_table	11
16777	18915	CDS
			locus_tag	Homavirus_1_12
			product	dynamin family protein
			transl_table	11
19006	19860	CDS
			locus_tag	Homavirus_1_13
			product	hypothetical protein
			transl_table	11
19995	20723	CDS
			locus_tag	Homavirus_1_14
			product	hypothetical protein
			transl_table	11
20782	21381	CDS
			locus_tag	Homavirus_1_15
			product	HNH endonuclease
			transl_table	11
21459	22583	CDS
			locus_tag	Homavirus_1_16
			product	hypothetical protein
			transl_table	11
22762	22598	CDS
			locus_tag	Homavirus_1_17
			product	hypothetical protein
			transl_table	11
23808	22801	CDS
			locus_tag	Homavirus_1_18
			product	dihydroorotate oxidase
			transl_table	11
23919	24527	CDS
			locus_tag	Homavirus_1_19
			product	superoxide dismutase
			transl_table	11
25084	24524	CDS
			locus_tag	Homavirus_1_20
			product	GIY-YIG nuclease
			transl_table	11
25619	25116	CDS
			locus_tag	Homavirus_1_21
			product	hypothetical protein
			transl_table	11
25756	26049	CDS
			locus_tag	Homavirus_1_22
			product	hypothetical protein
			transl_table	11
27562	26042	CDS
			locus_tag	Homavirus_1_23
			product	flavin containing amine oxidoreductase
			transl_table	11
27592	27930	CDS
			locus_tag	Homavirus_1_24
			product	hypothetical protein Klosneuvirus_3_295
			transl_table	11
29141	35815	CDS
			locus_tag	Homavirus_1_25
			product	PREDICTED: CAD protein isoform X1
			transl_table	11
36691	36029	CDS
			locus_tag	Homavirus_1_26
			product	hypothetical protein
			transl_table	11
>Homavirus_2
915	619	CDS
			locus_tag	Homavirus_2_1
			product	NCLDV major capsid protein
			transl_table	11
1888	908	CDS
			locus_tag	Homavirus_2_2
			product	NCLDV major capsid protein
			transl_table	11
3742	2099	CDS
			locus_tag	Homavirus_2_3
			product	NCLDV major capsid protein
			transl_table	11
4058	3861	CDS
			locus_tag	Homavirus_2_4
			product	hypothetical protein
			transl_table	11
4139	5605	CDS
			locus_tag	Homavirus_2_5
			product	late transcription factor VLTF3-like protein
			transl_table	11
6065	5631	CDS
			locus_tag	Homavirus_2_6
			product	hypothetical protein Klosneuvirus_1_117
			transl_table	11
6326	6096	CDS
			locus_tag	Homavirus_2_7
			product	hypothetical protein
			transl_table	11
6911	6387	CDS
			locus_tag	Homavirus_2_8
			product	hypothetical protein Klosneuvirus_1_117
			transl_table	11
7011	7496	CDS
			locus_tag	Homavirus_2_9
			product	hypothetical protein Klosneuvirus_1_116
			transl_table	11
7533	7910	CDS
			locus_tag	Homavirus_2_10
			product	hypothetical protein Klosneuvirus_1_115
			transl_table	11
8062	8277	CDS
			locus_tag	Homavirus_2_11
			product	hypothetical protein
			transl_table	11
8337	9008	CDS
			locus_tag	Homavirus_2_12
			product	Ras family GTPase
			transl_table	11
9093	9797	CDS
			locus_tag	Homavirus_2_13
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
9940	10215	CDS
			locus_tag	Homavirus_2_14
			product	hypothetical protein Klosneuvirus_1_23
			transl_table	11
10263	11561	CDS
			locus_tag	Homavirus_2_15
			product	HNH endonuclease
			transl_table	11
11864	11568	CDS
			locus_tag	Homavirus_2_16
			product	hypothetical protein
			transl_table	11
12949	11939	CDS
			locus_tag	Homavirus_2_17
			product	packaging ATPase
			transl_table	11
13400	12987	CDS
			locus_tag	Homavirus_2_18
			product	hypothetical protein Klosneuvirus_1_21
			transl_table	11
14796	13426	CDS
			locus_tag	Homavirus_2_19
			product	hypothetical protein Klosneuvirus_1_20
			transl_table	11
14875	15621	CDS
			locus_tag	Homavirus_2_20
			product	hypothetical protein
			transl_table	11
15985	15689	CDS
			locus_tag	Homavirus_2_21
			product	hypothetical protein
			transl_table	11
20409	16153	CDS
			locus_tag	Homavirus_2_22
			product	HNH endonuclease
			transl_table	11
21144	20536	CDS
			locus_tag	Homavirus_2_23
			product	hypothetical protein Indivirus_4_35
			transl_table	11
22369	21203	CDS
			locus_tag	Homavirus_2_24
			product	hypothetical protein Indivirus_4_36
			transl_table	11
22946	22407	CDS
			locus_tag	Homavirus_2_25
			product	hypothetical protein Klosneuvirus_1_13
			transl_table	11
23492	23812	CDS
			locus_tag	Homavirus_2_26
			product	hypothetical protein
			transl_table	11
>Homavirus_3
246	1	CDS
			locus_tag	Homavirus_3_1
			product	hypothetical protein
			transl_table	11
2227	392	CDS
			locus_tag	Homavirus_3_2
			product	methyltransferase domain-containing protein
			transl_table	11
2309	4201	CDS
			locus_tag	Homavirus_3_3
			product	collagen-like protein
			transl_table	11
5114	4230	CDS
			locus_tag	Homavirus_3_4
			product	hypothetical protein Klosneuvirus_3_318
			transl_table	11
5258	6250	CDS
			locus_tag	Homavirus_3_5
			product	glycosyltransferase family 25
			transl_table	11
8292	6265	CDS
			locus_tag	Homavirus_3_6
			product	hypothetical protein CBE49_06195
			transl_table	11
10612	8354	CDS
			locus_tag	Homavirus_3_7
			product	glycosyltransferase
			transl_table	11
11735	10677	CDS
			locus_tag	Homavirus_3_8
			product	hypothetical protein
			transl_table	11
13855	13217	CDS
			locus_tag	Homavirus_3_9
			product	Ras-like protein Rab-11A
			transl_table	11
14463	13921	CDS
			locus_tag	Homavirus_3_10
			product	hypothetical protein SAMD00019534_023660
			transl_table	11
14916	14644	CDS
			locus_tag	Homavirus_3_11
			product	hypothetical protein
			transl_table	11
15034	16689	CDS
			locus_tag	Homavirus_3_12
			product	glutaminyl-tRNA synthetase
			transl_table	11
16857	17147	CDS
			locus_tag	Homavirus_3_13
			product	hypothetical protein
			transl_table	11
17574	17272	CDS
			locus_tag	Homavirus_3_14
			product	hypothetical protein
			transl_table	11
19086	17653	CDS
			locus_tag	Homavirus_3_15
			product	hypothetical protein Catovirus_1_1060
			transl_table	11
>Homavirus_4
1	471	CDS
			locus_tag	Homavirus_4_1
			product	hypothetical protein B5V51_3331
			transl_table	11
549	1088	CDS
			locus_tag	Homavirus_4_2
			product	alpha/beta hydrolase family protein
			transl_table	11
1138	1425	CDS
			locus_tag	Homavirus_4_3
			product	hypothetical protein
			transl_table	11
2107	1472	CDS
			locus_tag	Homavirus_4_4
			product	hypothetical protein
			transl_table	11
2424	2167	CDS
			locus_tag	Homavirus_4_5
			product	hypothetical protein
			transl_table	11
3522	2491	CDS
			locus_tag	Homavirus_4_6
			product	lysophospholipid acyltransferase
			transl_table	11
3528	5546	CDS
			locus_tag	Homavirus_4_7
			product	AMP-binding enzyme
			transl_table	11
5683	6636	CDS
			locus_tag	Homavirus_4_8
			product	formamidopyrimidine-DNA glycosylase
			transl_table	11
7333	6638	CDS
			locus_tag	Homavirus_4_9
			product	hypothetical protein
			transl_table	11
7560	8690	CDS
			locus_tag	Homavirus_4_10
			product	hypothetical protein
			transl_table	11
9298	8687	CDS
			locus_tag	Homavirus_4_11
			product	hypothetical protein Indivirus_2_45
			transl_table	11
9834	9442	CDS
			locus_tag	Homavirus_4_12
			product	autophagy protein Atg8 ubiquitin-like protein
			transl_table	11
11309	9915	CDS
			locus_tag	Homavirus_4_13
			product	seryl-tRNA synthetase
			transl_table	11
11422	11805	CDS
			locus_tag	Homavirus_4_14
			product	inositol oxygenase, putative
			transl_table	11
12584	11802	CDS
			locus_tag	Homavirus_4_15
			product	hypothetical protein
			transl_table	11
14747	12591	CDS
			locus_tag	Homavirus_4_16
			product	HrpA-like RNA helicase
			transl_table	11
15317	14805	CDS
			locus_tag	Homavirus_4_17
			product	Chain A, Crystal Structure Of Ancestral Thioredoxin Relative To Last Animal And Fungi Common Ancestor (lafca) From The Precambrian Period
			transl_table	11
15885	15400	CDS
			locus_tag	Homavirus_4_18
			product	hypothetical protein
			transl_table	11
16224	15934	CDS
			locus_tag	Homavirus_4_19
			product	hypothetical protein
			transl_table	11
17820	16279	CDS
			locus_tag	Homavirus_4_20
			product	polyADP-ribose polymerase
			transl_table	11
18965	17916	CDS
			locus_tag	Homavirus_4_21
			product	isoleucyl-tRNA synthetase
			transl_table	11
>Homavirus_5
762	1	CDS
			locus_tag	Homavirus_5_1
			product	hypothetical protein Klosneuvirus_1_176
			transl_table	11
1218	808	CDS
			locus_tag	Homavirus_5_2
			product	hypothetical protein BMW23_0448
			transl_table	11
1570	1349	CDS
			locus_tag	Homavirus_5_3
			product	hypothetical protein
			transl_table	11
1631	2215	CDS
			locus_tag	Homavirus_5_4
			product	hypothetical protein Catovirus_1_1086
			transl_table	11
4361	2271	CDS
			locus_tag	Homavirus_5_5
			product	ankyrin repeat protein
			transl_table	11
4567	5175	CDS
			locus_tag	Homavirus_5_6
			product	hypothetical protein Indivirus_1_65
			transl_table	11
6063	5200	CDS
			locus_tag	Homavirus_5_7
			product	hypothetical protein
			transl_table	11
6035	6763	CDS
			locus_tag	Homavirus_5_8
			product	putative papain-like cysteine peptidase
			transl_table	11
6849	7694	CDS
			locus_tag	Homavirus_5_9
			product	hypothetical protein Klosneuvirus_1_180
			transl_table	11
7767	8354	CDS
			locus_tag	Homavirus_5_10
			product	hypothetical protein Indivirus_1_68
			transl_table	11
8392	8934	CDS
			locus_tag	Homavirus_5_11
			product	hypothetical protein Klosneuvirus_1_187
			transl_table	11
9024	9545	CDS
			locus_tag	Homavirus_5_12
			product	hypothetical protein Indivirus_1_70
			transl_table	11
9599	9904	CDS
			locus_tag	Homavirus_5_13
			product	hypothetical protein
			transl_table	11
10446	9901	CDS
			locus_tag	Homavirus_5_14
			product	hypothetical protein Indivirus_1_72
			transl_table	11
11472	10531	CDS
			locus_tag	Homavirus_5_15
			product	alpha-soluble NSF attachment protein 2-like
			transl_table	11
11631	12980	CDS
			locus_tag	Homavirus_5_16
			product	proliferating cell nuclear antigen
			transl_table	11
14213	13038	CDS
			locus_tag	Homavirus_5_17
			product	translation initiation factor 4E
			transl_table	11
15384	14290	CDS
			locus_tag	Homavirus_5_18
			product	replication factor C small subunit
			transl_table	11
15469	16158	CDS
			locus_tag	Homavirus_5_19
			product	hypothetical protein Indivirus_1_77
			transl_table	11
16246	17766	CDS
			locus_tag	Homavirus_5_20
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
17834	18475	CDS
			locus_tag	Homavirus_5_21
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
>Homavirus_6
539	2128	CDS
			locus_tag	Homavirus_6_1
			product	hypothetical protein
			transl_table	11
2189	2593	CDS
			locus_tag	Homavirus_6_2
			product	hypothetical protein
			transl_table	11
2575	2745	CDS
			locus_tag	Homavirus_6_3
			product	hypothetical protein
			transl_table	11
3225	2764	CDS
			locus_tag	Homavirus_6_4
			product	hypothetical protein
			transl_table	11
3358	4188	CDS
			locus_tag	Homavirus_6_5
			product	hypothetical protein
			transl_table	11
4454	7642	CDS
			locus_tag	Homavirus_6_6
			product	hypothetical protein
			transl_table	11
7776	9194	CDS
			locus_tag	Homavirus_6_7
			product	hypothetical protein Indivirus_5_51
			transl_table	11
9340	9795	CDS
			locus_tag	Homavirus_6_8
			product	hypothetical protein
			transl_table	11
9849	10418	CDS
			locus_tag	Homavirus_6_9
			product	hypothetical protein
			transl_table	11
10633	11076	CDS
			locus_tag	Homavirus_6_10
			product	hypothetical protein
			transl_table	11
11146	11982	CDS
			locus_tag	Homavirus_6_11
			product	putative ORFan
			transl_table	11
12021	12866	CDS
			locus_tag	Homavirus_6_12
			product	putative orfan
			transl_table	11
12914	13966	CDS
			locus_tag	Homavirus_6_13
			product	tRNAHis guanylyltransferase
			transl_table	11
14043	14777	CDS
			locus_tag	Homavirus_6_14
			product	hypothetical protein
			transl_table	11
15435	15653	CDS
			locus_tag	Homavirus_6_15
			product	hypothetical protein
			transl_table	11
>Homavirus_7
1	285	CDS
			locus_tag	Homavirus_7_1
			product	hypothetical protein
			transl_table	11
1170	313	CDS
			locus_tag	Homavirus_7_2
			product	hypothetical protein
			transl_table	11
1345	1845	CDS
			locus_tag	Homavirus_7_3
			product	hypothetical protein
			transl_table	11
2135	1884	CDS
			locus_tag	Homavirus_7_4
			product	hypothetical protein
			transl_table	11
3328	2207	CDS
			locus_tag	Homavirus_7_5
			product	PREDICTED: synaptosomal-associated protein 29
			transl_table	11
5384	3486	CDS
			locus_tag	Homavirus_7_6
			product	transposase
			transl_table	11
5883	6665	CDS
			locus_tag	Homavirus_7_7
			product	hypothetical protein
			transl_table	11
7806	6790	CDS
			locus_tag	Homavirus_7_8
			product	hypothetical protein Hokovirus_1_173
			transl_table	11
7901	7803	CDS
			locus_tag	Homavirus_7_9
			product	hypothetical protein
			transl_table	11
7951	9351	CDS
			locus_tag	Homavirus_7_10
			product	chitinase, GH18 family
			transl_table	11
10495	9401	CDS
			locus_tag	Homavirus_7_11
			product	TKL protein kinase
			transl_table	11
13647	10567	CDS
			locus_tag	Homavirus_7_12
			product	hypothetical protein
			transl_table	11
14855	13788	CDS
			locus_tag	Homavirus_7_13
			product	chitinase, GH18 family
			transl_table	11
>Homavirus_8
2	1513	CDS
			locus_tag	Homavirus_8_1
			product	hypothetical protein
			transl_table	11
1633	3792	CDS
			locus_tag	Homavirus_8_2
			product	hypothetical protein
			transl_table	11
3858	5303	CDS
			locus_tag	Homavirus_8_3
			product	hypothetical protein
			transl_table	11
5369	7294	CDS
			locus_tag	Homavirus_8_4
			product	hypothetical protein
			transl_table	11
7360	8508	CDS
			locus_tag	Homavirus_8_5
			product	hypothetical protein
			transl_table	11
8612	10006	CDS
			locus_tag	Homavirus_8_6
			product	hypothetical protein
			transl_table	11
10121	10537	CDS
			locus_tag	Homavirus_8_7
			product	hypothetical protein
			transl_table	11
11523	10534	CDS
			locus_tag	Homavirus_8_8
			product	hypothetical protein
			transl_table	11
12507	11590	CDS
			locus_tag	Homavirus_8_9
			product	hypothetical protein
			transl_table	11
12746	13417	CDS
			locus_tag	Homavirus_8_10
			product	hypothetical protein
			transl_table	11
13593	13895	CDS
			locus_tag	Homavirus_8_11
			product	hypothetical protein
			transl_table	11
>Homavirus_9
2	1291	CDS
			locus_tag	Homavirus_9_1
			product	DnaJ domain protein
			transl_table	11
2341	1316	CDS
			locus_tag	Homavirus_9_2
			product	hypothetical protein
			transl_table	11
2466	2597	CDS
			locus_tag	Homavirus_9_3
			product	hypothetical protein
			transl_table	11
3116	2565	CDS
			locus_tag	Homavirus_9_4
			product	deoxynucleoside monophosphate kinase
			transl_table	11
3249	5696	CDS
			locus_tag	Homavirus_9_5
			product	FtsJ-like methyltransferase
			transl_table	11
5794	6900	CDS
			locus_tag	Homavirus_9_6
			product	replication factor C small subunit
			transl_table	11
7021	7725	CDS
			locus_tag	Homavirus_9_7
			product	hypothetical protein Klosneuvirus_2_146
			transl_table	11
8954	7722	CDS
			locus_tag	Homavirus_9_8
			product	hypothetical protein Klosneuvirus_3_258
			transl_table	11
9037	10338	CDS
			locus_tag	Homavirus_9_9
			product	N-myristoyltransferase
			transl_table	11
10998	10381	CDS
			locus_tag	Homavirus_9_10
			product	hypothetical protein Indivirus_2_29
			transl_table	11
11058	13853	CDS
			locus_tag	Homavirus_9_11
			product	Zn-dependent peptidase
			transl_table	11
13951	13850	CDS
			locus_tag	Homavirus_9_12
			product	hypothetical protein
			transl_table	11
13950	14126	CDS
			locus_tag	Homavirus_9_13
			product	hypothetical protein
			transl_table	11
>Homavirus_10
367	44	CDS
			locus_tag	Homavirus_10_1
			product	hypothetical protein
			transl_table	11
643	2865	CDS
			locus_tag	Homavirus_10_2
			product	UvrD-family helicase
			transl_table	11
3213	2881	CDS
			locus_tag	Homavirus_10_3
			product	hypothetical protein
			transl_table	11
3321	3641	CDS
			locus_tag	Homavirus_10_4
			product	hypothetical protein Klosneuvirus_1_304
			transl_table	11
3703	3840	CDS
			locus_tag	Homavirus_10_5
			product	hypothetical protein
			transl_table	11
5461	3905	CDS
			locus_tag	Homavirus_10_6
			product	hypothetical protein Indivirus_1_157
			transl_table	11
5545	6621	CDS
			locus_tag	Homavirus_10_7
			product	hypothetical protein Klosneuvirus_1_308
			transl_table	11
6661	7632	CDS
			locus_tag	Homavirus_10_8
			product	hypothetical protein
			transl_table	11
7660	8670	CDS
			locus_tag	Homavirus_10_9
			product	hypothetical protein
			transl_table	11
11635	8717	CDS
			locus_tag	Homavirus_10_10
			product	subtilase family serine protease
			transl_table	11
12067	11687	CDS
			locus_tag	Homavirus_10_11
			product	hypothetical protein
			transl_table	11
12164	12298	CDS
			locus_tag	Homavirus_10_12
			product	hypothetical protein
			transl_table	11
13012	12572	CDS
			locus_tag	Homavirus_10_13
			product	hypothetical protein
			transl_table	11
>Homavirus_11
1	339	CDS
			locus_tag	Homavirus_11_1
			product	replication factor C large subunit
			transl_table	11
411	2213	CDS
			locus_tag	Homavirus_11_2
			product	P4B major core protein
			transl_table	11
2678	2232	CDS
			locus_tag	Homavirus_11_3
			product	hypothetical protein Indivirus_9_11
			transl_table	11
3184	3516	CDS
			locus_tag	Homavirus_11_4
			product	hypothetical protein
			transl_table	11
3594	4265	CDS
			locus_tag	Homavirus_11_5
			product	hypothetical protein Klosneuvirus_4_137
			transl_table	11
4721	4410	CDS
			locus_tag	Homavirus_11_6
			product	hypothetical protein
			transl_table	11
5410	4814	CDS
			locus_tag	Homavirus_11_7
			product	hypothetical protein Indivirus_4_42
			transl_table	11
6045	5437	CDS
			locus_tag	Homavirus_11_8
			product	hypothetical protein
			transl_table	11
8444	6078	CDS
			locus_tag	Homavirus_11_9
			product	hypothetical protein Indivirus_4_43
			transl_table	11
10933	8450	CDS
			locus_tag	Homavirus_11_10
			product	hypothetical protein Indivirus_4_44
			transl_table	11
11034	11285	CDS
			locus_tag	Homavirus_11_11
			product	hypothetical protein
			transl_table	11
12526	11282	CDS
			locus_tag	Homavirus_11_12
			product	serine/threonine protein kinase
			transl_table	11
>Homavirus_12
1	387	CDS
			locus_tag	Homavirus_12_1
			product	hypothetical protein
			transl_table	11
491	384	CDS
			locus_tag	Homavirus_12_2
			product	hypothetical protein
			transl_table	11
508	912	CDS
			locus_tag	Homavirus_12_3
			product	hypothetical protein
			transl_table	11
1251	925	CDS
			locus_tag	Homavirus_12_4
			product	hypothetical protein
			transl_table	11
1407	2042	CDS
			locus_tag	Homavirus_12_5
			product	hypothetical protein
			transl_table	11
2454	3932	CDS
			locus_tag	Homavirus_12_6
			product	putative RtcB-like RNA-splicing ligase
			transl_table	11
4740	4000	CDS
			locus_tag	Homavirus_12_7
			product	hypothetical protein
			transl_table	11
4976	5464	CDS
			locus_tag	Homavirus_12_8
			product	hypothetical protein
			transl_table	11
5573	7105	CDS
			locus_tag	Homavirus_12_9
			product	hypothetical protein
			transl_table	11
9027	7165	CDS
			locus_tag	Homavirus_12_10
			product	hypothetical protein Klosneuvirus_3_123
			transl_table	11
9153	9614	CDS
			locus_tag	Homavirus_12_11
			product	hypothetical protein
			transl_table	11
10055	9726	CDS
			locus_tag	Homavirus_12_12
			product	hypothetical protein
			transl_table	11
10336	10791	CDS
			locus_tag	Homavirus_12_13
			product	hypothetical protein
			transl_table	11
11009	11959	CDS
			locus_tag	Homavirus_12_14
			product	hypothetical protein
			transl_table	11
12441	12016	CDS
			locus_tag	Homavirus_12_15
			product	hypothetical protein
			transl_table	11
>Homavirus_13
2	1525	CDS
			locus_tag	Homavirus_13_1
			product	RNA ligase
			transl_table	11
2567	1584	CDS
			locus_tag	Homavirus_13_2
			product	DNA directed RNA polymerase subunit L
			transl_table	11
2756	3364	CDS
			locus_tag	Homavirus_13_3
			product	hypothetical protein Indivirus_1_43
			transl_table	11
3836	3438	CDS
			locus_tag	Homavirus_13_4
			product	hypothetical protein Indivirus_1_42
			transl_table	11
4007	5332	CDS
			locus_tag	Homavirus_13_5
			product	hypothetical protein Klosneuvirus_1_125
			transl_table	11
5388	6491	CDS
			locus_tag	Homavirus_13_6
			product	thioredoxin
			transl_table	11
6527	7405	CDS
			locus_tag	Homavirus_13_7
			product	hypothetical protein Klosneuvirus_1_123
			transl_table	11
7454	7618	CDS
			locus_tag	Homavirus_13_8
			product	hypothetical protein
			transl_table	11
7674	8453	CDS
			locus_tag	Homavirus_13_9
			product	uridyltransferase
			transl_table	11
8517	9434	CDS
			locus_tag	Homavirus_13_10
			product	Collagen triple helix repeat-containing protein
			transl_table	11
9934	9497	CDS
			locus_tag	Homavirus_13_11
			product	acetyltransferase GNAT family protein
			transl_table	11
10055	11257	CDS
			locus_tag	Homavirus_13_12
			product	tyrosyl-tRNA synthetase
			transl_table	11
12182	11262	CDS
			locus_tag	Homavirus_13_13
			product	hypothetical protein
			transl_table	11
>Homavirus_14
3	104	CDS
			locus_tag	Homavirus_14_1
			product	hypothetical protein
			transl_table	11
5208	151	CDS
			locus_tag	Homavirus_14_2
			product	hypothetical protein
			transl_table	11
5589	5269	CDS
			locus_tag	Homavirus_14_3
			product	hypothetical protein Klosneuvirus_5_83
			transl_table	11
6287	8029	CDS
			locus_tag	Homavirus_14_4
			product	predicted protein
			transl_table	11
8878	8021	CDS
			locus_tag	Homavirus_14_5
			product	hypothetical protein Klosneuvirus_4_11
			transl_table	11
9691	8951	CDS
			locus_tag	Homavirus_14_6
			product	phosphoribulokinase / Uridine kinase family protein
			transl_table	11
11208	9712	CDS
			locus_tag	Homavirus_14_7
			product	V21
			transl_table	11
>Homavirus_15
621	1	CDS
			locus_tag	Homavirus_15_1
			product	transcription initiation factor IIB
			transl_table	11
861	1667	CDS
			locus_tag	Homavirus_15_2
			product	uracil-DNA glycosylase
			transl_table	11
1717	2799	CDS
			locus_tag	Homavirus_15_3
			product	hypothetical protein
			transl_table	11
2946	5798	CDS
			locus_tag	Homavirus_15_4
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
5927	6106	CDS
			locus_tag	Homavirus_15_5
			product	hypothetical protein
			transl_table	11
6142	6600	CDS
			locus_tag	Homavirus_15_6
			product	hypothetical protein
			transl_table	11
6717	7157	CDS
			locus_tag	Homavirus_15_7
			product	hypothetical protein
			transl_table	11
8813	7200	CDS
			locus_tag	Homavirus_15_8
			product	hypothetical protein
			transl_table	11
8918	9691	CDS
			locus_tag	Homavirus_15_9
			product	ribonuclease H
			transl_table	11
10159	10341	CDS
			locus_tag	Homavirus_15_10
			product	hypothetical protein
			transl_table	11
>Homavirus_16
737	180	CDS
			locus_tag	Homavirus_16_1
			product	hypothetical protein
			transl_table	11
1940	819	CDS
			locus_tag	Homavirus_16_2
			product	hypothetical protein
			transl_table	11
2518	2177	CDS
			locus_tag	Homavirus_16_3
			product	hypothetical protein
			transl_table	11
3557	2622	CDS
			locus_tag	Homavirus_16_4
			product	hypothetical protein
			transl_table	11
4606	3734	CDS
			locus_tag	Homavirus_16_5
			product	hypothetical protein
			transl_table	11
5661	4726	CDS
			locus_tag	Homavirus_16_6
			product	hypothetical protein
			transl_table	11
6636	5752	CDS
			locus_tag	Homavirus_16_7
			product	hypothetical protein
			transl_table	11
7670	6756	CDS
			locus_tag	Homavirus_16_8
			product	hypothetical protein
			transl_table	11
8146	7733	CDS
			locus_tag	Homavirus_16_9
			product	hypothetical protein
			transl_table	11
8591	8352	CDS
			locus_tag	Homavirus_16_10
			product	hypothetical protein
			transl_table	11
9376	9843	CDS
			locus_tag	Homavirus_16_11
			product	hypothetical protein
			transl_table	11
>Homavirus_17
216	1	CDS
			locus_tag	Homavirus_17_1
			product	hypothetical protein Indivirus_1_201
			transl_table	11
246	1169	CDS
			locus_tag	Homavirus_17_2
			product	hypothetical protein
			transl_table	11
1385	2764	CDS
			locus_tag	Homavirus_17_3
			product	AAA family ATPase
			transl_table	11
4804	2780	CDS
			locus_tag	Homavirus_17_4
			product	hypothetical protein
			transl_table	11
6366	4903	CDS
			locus_tag	Homavirus_17_5
			product	alpha/beta hydrolase fold
			transl_table	11
6521	8509	CDS
			locus_tag	Homavirus_17_6
			product	hypothetical protein
			transl_table	11
8602	9534	CDS
			locus_tag	Homavirus_17_7
			product	hypothetical protein Klosneuvirus_1_374
			transl_table	11
9773	9678	CDS
			locus_tag	Homavirus_17_8
			product	hypothetical protein
			transl_table	11
>Homavirus_18
3	107	CDS
			locus_tag	Homavirus_18_1
			product	hypothetical protein
			transl_table	11
215	394	CDS
			locus_tag	Homavirus_18_2
			product	hypothetical protein
			transl_table	11
1121	405	CDS
			locus_tag	Homavirus_18_3
			product	hypothetical protein
			transl_table	11
1176	4097	CDS
			locus_tag	Homavirus_18_4
			product	magnesium-transporting ATPase
			transl_table	11
4411	4100	CDS
			locus_tag	Homavirus_18_5
			product	hypothetical protein
			transl_table	11
4490	4963	CDS
			locus_tag	Homavirus_18_6
			product	hypothetical protein
			transl_table	11
4980	5366	CDS
			locus_tag	Homavirus_18_7
			product	hypothetical protein
			transl_table	11
6514	5378	CDS
			locus_tag	Homavirus_18_8
			product	TIGR03032 family protein
			transl_table	11
6596	6913	CDS
			locus_tag	Homavirus_18_9
			product	hypothetical protein
			transl_table	11
7012	8652	CDS
			locus_tag	Homavirus_18_10
			product	hypothetical protein A3J41_02450
			transl_table	11
9524	9700	CDS
			locus_tag	Homavirus_18_11
			product	hypothetical protein
			transl_table	11
>Homavirus_19
135	1	CDS
			locus_tag	Homavirus_19_1
			product	hypothetical protein
			transl_table	11
194	1396	CDS
			locus_tag	Homavirus_19_2
			product	hypothetical protein
			transl_table	11
1757	1428	CDS
			locus_tag	Homavirus_19_3
			product	hypothetical protein
			transl_table	11
2971	1859	CDS
			locus_tag	Homavirus_19_4
			product	Putative ADP-ribosyl glycohydrolase
			transl_table	11
3230	3691	CDS
			locus_tag	Homavirus_19_5
			product	hypothetical protein UR74_C0002G0240
			transl_table	11
3780	4955	CDS
			locus_tag	Homavirus_19_6
			product	hypothetical protein
			transl_table	11
5024	5770	CDS
			locus_tag	Homavirus_19_7
			product	DUF72 domain-containing protein
			transl_table	11
6976	5879	CDS
			locus_tag	Homavirus_19_8
			product	gamma-butyrobetaine dioxygenase
			transl_table	11
8504	7050	CDS
			locus_tag	Homavirus_19_9
			product	atp-dependent rna helicase
			transl_table	11
9206	8535	CDS
			locus_tag	Homavirus_19_10
			product	hypothetical protein
			transl_table	11
9342	9470	CDS
			locus_tag	Homavirus_19_11
			product	hypothetical protein
			transl_table	11
>Homavirus_20
1341	10	CDS
			locus_tag	Homavirus_20_1
			product	hypothetical protein Klosneuvirus_3_120
			transl_table	11
1491	1694	CDS
			locus_tag	Homavirus_20_2
			product	hypothetical protein
			transl_table	11
2194	1721	CDS
			locus_tag	Homavirus_20_3
			product	hypothetical protein
			transl_table	11
3382	2420	CDS
			locus_tag	Homavirus_20_4
			product	hypothetical protein ACA1_098010
			transl_table	11
3532	3846	CDS
			locus_tag	Homavirus_20_5
			product	hypothetical protein
			transl_table	11
3919	4461	CDS
			locus_tag	Homavirus_20_6
			product	hypothetical protein Catovirus_1_29
			transl_table	11
4978	4625	CDS
			locus_tag	Homavirus_20_7
			product	hypothetical protein
			transl_table	11
5176	5493	CDS
			locus_tag	Homavirus_20_8
			product	hypothetical protein
			transl_table	11
6754	5606	CDS
			locus_tag	Homavirus_20_9
			product	hypothetical protein
			transl_table	11
8552	6924	CDS
			locus_tag	Homavirus_20_10
			product	ankyrin-1-like isoform X1
			transl_table	11
9070	8699	CDS
			locus_tag	Homavirus_20_11
			product	hypothetical protein
			transl_table	11
>Homavirus_21
1	579	CDS
			locus_tag	Homavirus_21_1
			product	hypothetical protein
			transl_table	11
2885	672	CDS
			locus_tag	Homavirus_21_2
			product	hypothetical protein SARC_01664
			transl_table	11
3320	2985	CDS
			locus_tag	Homavirus_21_3
			product	hypothetical protein
			transl_table	11
3384	3274	CDS
			locus_tag	Homavirus_21_4
			product	hypothetical protein
			transl_table	11
3451	4140	CDS
			locus_tag	Homavirus_21_5
			product	dCTP deaminase
			transl_table	11
4287	5363	CDS
			locus_tag	Homavirus_21_6
			product	hypothetical protein
			transl_table	11
5367	5459	CDS
			locus_tag	Homavirus_21_7
			product	hypothetical protein
			transl_table	11
5834	5577	CDS
			locus_tag	Homavirus_21_8
			product	hypothetical protein
			transl_table	11
5943	6356	CDS
			locus_tag	Homavirus_21_9
			product	hypothetical protein
			transl_table	11
8835	6358	CDS
			locus_tag	Homavirus_21_10
			product	glycosyltransferase family 2
			transl_table	11
>Homavirus_22
2	1318	CDS
			locus_tag	Homavirus_22_1
			product	DNA topoisomerase IA
			transl_table	11
1389	1823	CDS
			locus_tag	Homavirus_22_2
			product	hypothetical protein RvY_04668
			transl_table	11
2688	1837	CDS
			locus_tag	Homavirus_22_3
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
3973	3233	CDS
			locus_tag	Homavirus_22_4
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
4313	8059	CDS
			locus_tag	Homavirus_22_5
			product	D5-like helicase-primase
			transl_table	11
8134	8346	CDS
			locus_tag	Homavirus_22_6
			product	hypothetical protein
			transl_table	11
>Homavirus_23
1923	1	CDS
			locus_tag	Homavirus_23_1
			product	predicted protein
			transl_table	11
2056	2979	CDS
			locus_tag	Homavirus_23_2
			product	proliferating cell nuclear antigen
			transl_table	11
3044	4201	CDS
			locus_tag	Homavirus_23_3
			product	PREDICTED: putative ankyrin repeat protein RF_0381
			transl_table	11
4685	4203	CDS
			locus_tag	Homavirus_23_4
			product	hypothetical protein
			transl_table	11
4829	7168	CDS
			locus_tag	Homavirus_23_5
			product	hypothetical protein
			transl_table	11
7240	7470	CDS
			locus_tag	Homavirus_23_6
			product	hypothetical protein
			transl_table	11
8042	7620	CDS
			locus_tag	Homavirus_23_7
			product	hypothetical protein
			transl_table	11
8469	8104	CDS
			locus_tag	Homavirus_23_8
			product	superfamily II helicase
			transl_table	11
>Homavirus_24
95	3	CDS
			locus_tag	Homavirus_24_1
			product	hypothetical protein
			transl_table	11
284	1336	CDS
			locus_tag	Homavirus_24_2
			product	hypothetical protein
			transl_table	11
2995	1358	CDS
			locus_tag	Homavirus_24_3
			product	hypothetical protein
			transl_table	11
4444	3056	CDS
			locus_tag	Homavirus_24_4
			product	hypothetical protein
			transl_table	11
6488	4506	CDS
			locus_tag	Homavirus_24_5
			product	hypothetical protein
			transl_table	11
6887	6588	CDS
			locus_tag	Homavirus_24_6
			product	hypothetical protein
			transl_table	11
7401	7102	CDS
			locus_tag	Homavirus_24_7
			product	hypothetical protein
			transl_table	11
8011	7781	CDS
			locus_tag	Homavirus_24_8
			product	hypothetical protein
			transl_table	11
8181	8321	CDS
			locus_tag	Homavirus_24_9
			product	hypothetical protein
			transl_table	11
>Homavirus_25
2	5017	CDS
			locus_tag	Homavirus_25_1
			product	Histidine kinase-like ATPase
			transl_table	11
5638	5183	CDS
			locus_tag	Homavirus_25_2
			product	hypothetical protein
			transl_table	11
5943	6719	CDS
			locus_tag	Homavirus_25_3
			product	hypothetical protein
			transl_table	11
7309	7178	CDS
			locus_tag	Homavirus_25_4
			product	hypothetical protein
			transl_table	11
7730	7602	CDS
			locus_tag	Homavirus_25_5
			product	hypothetical protein
			transl_table	11
7955	8116	CDS
			locus_tag	Homavirus_25_6
			product	hypothetical protein
			transl_table	11
>Homavirus_26
944	63	CDS
			locus_tag	Homavirus_26_1
			product	dTDP-6-deoxy-L-lyxo-4-hexulose reductase
			transl_table	11
1114	2085	CDS
			locus_tag	Homavirus_26_2
			product	UDP-N-acetylglucosamine 4,6-dehydratase
			transl_table	11
3599	2082	CDS
			locus_tag	Homavirus_26_3
			product	hypothetical protein B6D46_10620
			transl_table	11
3796	4776	CDS
			locus_tag	Homavirus_26_4
			product	UDP-N-acetylglucosamine 2-epimerase (non-hydrolyzing)
			transl_table	11
4992	4762	CDS
			locus_tag	Homavirus_26_5
			product	hypothetical protein
			transl_table	11
4983	5333	CDS
			locus_tag	Homavirus_26_6
			product	hypothetical protein
			transl_table	11
5438	6289	CDS
			locus_tag	Homavirus_26_7
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
>Homavirus_27
1	675	CDS
			locus_tag	Homavirus_27_1
			product	phosphoribosylformylglycinamidine synthase
			transl_table	11
867	3500	CDS
			locus_tag	Homavirus_27_2
			product	PREDICTED: phosphoribosylformylglycinamidine synthase-like
			transl_table	11
3542	5320	CDS
			locus_tag	Homavirus_27_3
			product	uncharacterized protein
			transl_table	11
6853	5342	CDS
			locus_tag	Homavirus_27_4
			product	hypothetical protein A2725_03790
			transl_table	11
7223	6831	CDS
			locus_tag	Homavirus_27_5
			product	phosphoribosylglycinamide formyltransferase
			transl_table	11
>Homavirus_28
716	3	CDS
			locus_tag	Homavirus_28_1
			product	hypothetical protein Indivirus_2_90
			transl_table	11
1960	740	CDS
			locus_tag	Homavirus_28_2
			product	putative histone-lysine N-methyltransferase, partial
			transl_table	11
2241	2086	CDS
			locus_tag	Homavirus_28_3
			product	hypothetical protein
			transl_table	11
2421	3104	CDS
			locus_tag	Homavirus_28_4
			product	hypothetical protein
			transl_table	11
3139	4818	CDS
			locus_tag	Homavirus_28_5
			product	procyclic acidic repetitive protein PARP
			transl_table	11
5118	4831	CDS
			locus_tag	Homavirus_28_6
			product	hypothetical protein
			transl_table	11
6383	5112	CDS
			locus_tag	Homavirus_28_7
			product	serine/threonine protein kinase
			transl_table	11
6576	6764	CDS
			locus_tag	Homavirus_28_8
			product	hypothetical protein
			transl_table	11
>Homavirus_29
1207	278	CDS
			locus_tag	Homavirus_29_1
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
2540	1329	CDS
			locus_tag	Homavirus_29_2
			product	translation initiation factor 4A-III
			transl_table	11
2713	3417	CDS
			locus_tag	Homavirus_29_3
			product	hypothetical protein
			transl_table	11
4448	3567	CDS
			locus_tag	Homavirus_29_4
			product	hypothetical protein
			transl_table	11
4722	5858	CDS
			locus_tag	Homavirus_29_5
			product	hypothetical protein
			transl_table	11
6233	6589	CDS
			locus_tag	Homavirus_29_6
			product	hypothetical protein
			transl_table	11
>Homavirus_30
1	90	CDS
			locus_tag	Homavirus_30_1
			product	hypothetical protein
			transl_table	11
147	974	CDS
			locus_tag	Homavirus_30_2
			product	hypothetical protein
			transl_table	11
1039	1545	CDS
			locus_tag	Homavirus_30_3
			product	hypothetical protein
			transl_table	11
2535	1579	CDS
			locus_tag	Homavirus_30_4
			product	hypothetical protein
			transl_table	11
2730	3254	CDS
			locus_tag	Homavirus_30_5
			product	hypothetical protein
			transl_table	11
3349	3726	CDS
			locus_tag	Homavirus_30_6
			product	hypothetical protein
			transl_table	11
3816	4481	CDS
			locus_tag	Homavirus_30_7
			product	hypothetical protein
			transl_table	11
5827	4514	CDS
			locus_tag	Homavirus_30_8
			product	hypothetical protein
			transl_table	11
>Homavirus_31
3	1649	CDS
			locus_tag	Homavirus_31_1
			product	glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
2432	1734	CDS
			locus_tag	Homavirus_31_2
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
3426	2461	CDS
			locus_tag	Homavirus_31_3
			product	fructose-1,6-bisphosphatase, cytosolic-like
			transl_table	11
4335	3463	CDS
			locus_tag	Homavirus_31_4
			product	hypothetical protein Klosneuvirus_4_88
			transl_table	11
4787	4455	CDS
			locus_tag	Homavirus_31_5
			product	protein of unknown function DUF814
			transl_table	11
5963	5748	CDS
			locus_tag	Homavirus_31_6
			product	hypothetical protein
			transl_table	11
>Homavirus_32
1465	2	CDS
			locus_tag	Homavirus_32_1
			product	serine/threonine protein kinase
			transl_table	11
1562	2209	CDS
			locus_tag	Homavirus_32_2
			product	hypothetical protein
			transl_table	11
2354	4048	CDS
			locus_tag	Homavirus_32_3
			product	ubiquitin-conjugating enzyme
			transl_table	11
4738	4064	CDS
			locus_tag	Homavirus_32_4
			product	hypothetical protein
			transl_table	11
5778	4801	CDS
			locus_tag	Homavirus_32_5
			product	hypothetical protein
			transl_table	11
5809	5925	CDS
			locus_tag	Homavirus_32_6
			product	hypothetical protein
			transl_table	11
>Homavirus_33
1	1737	CDS
			locus_tag	Homavirus_33_1
			product	glycosyltransferase
			transl_table	11
1781	2608	CDS
			locus_tag	Homavirus_33_2
			product	N-acetylneuraminate synthase
			transl_table	11
3416	2610	CDS
			locus_tag	Homavirus_33_3
			product	cytidyltransferase
			transl_table	11
4206	3541	CDS
			locus_tag	Homavirus_33_4
			product	hypothetical protein
			transl_table	11
4973	4227	CDS
			locus_tag	Homavirus_33_5
			product	hypothetical protein A2289_04340
			transl_table	11
5517	5621	CDS
			locus_tag	Homavirus_33_6
			product	hypothetical protein
			transl_table	11
>Homavirus_34
3	1712	CDS
			locus_tag	Homavirus_34_1
			product	RING finger domain protein
			transl_table	11
1782	2219	CDS
			locus_tag	Homavirus_34_2
			product	hypothetical protein
			transl_table	11
2279	4138	CDS
			locus_tag	Homavirus_34_3
			product	Sphingosine-1-phosphate lyase 1
			transl_table	11
4899	4159	CDS
			locus_tag	Homavirus_34_4
			product	esterase/lipase
			transl_table	11
5151	4978	CDS
			locus_tag	Homavirus_34_5
			product	hypothetical protein
			transl_table	11
>Homavirus_35
47	499	CDS
			locus_tag	Homavirus_35_1
			product	hypothetical protein NDAI_0J01540
			transl_table	11
799	578	CDS
			locus_tag	Homavirus_35_2
			product	hypothetical protein
			transl_table	11
902	2548	CDS
			locus_tag	Homavirus_35_3
			product	AAA family ATPase
			transl_table	11
3384	2605	CDS
			locus_tag	Homavirus_35_4
			product	hypothetical protein
			transl_table	11
4438	3491	CDS
			locus_tag	Homavirus_35_5
			product	hypothetical protein
			transl_table	11
4975	5079	CDS
			locus_tag	Homavirus_35_6
			product	hypothetical protein
			transl_table	11
>Homavirus_36
2079	1997	tRNA	Homavirus_tRNA_1
			product	tRNA-Ser(AGA)
2262	2179	tRNA	Homavirus_tRNA_2
			product	tRNA-Ser(CGA)
2411	2327	tRNA	Homavirus_tRNA_3
			product	tRNA-Pseudo(TGA)
1	138	CDS
			locus_tag	Homavirus_36_1
			product	hypothetical protein
			transl_table	11
441	722	CDS
			locus_tag	Homavirus_36_2
			product	hypothetical protein
			transl_table	11
791	1795	CDS
			locus_tag	Homavirus_36_3
			product	zinc-finger and zinc-ribbon domain protein
			transl_table	11
3462	2641	CDS
			locus_tag	Homavirus_36_4
			product	hypothetical protein
			transl_table	11
3569	4525	CDS
			locus_tag	Homavirus_36_5
			product	hypothetical protein
			transl_table	11
4809	4922	CDS
			locus_tag	Homavirus_36_6
			product	hypothetical protein
			transl_table	11
>Homavirus_37
159	1859	CDS
			locus_tag	Homavirus_37_1
			product	asparagine synthetase
			transl_table	11
1951	2904	CDS
			locus_tag	Homavirus_37_2
			product	hypothetical protein Hokovirus_2_86
			transl_table	11
3184	2936	CDS
			locus_tag	Homavirus_37_3
			product	hypothetical protein Indivirus_1_49
			transl_table	11
4468	3317	CDS
			locus_tag	Homavirus_37_4
			product	hypothetical protein Indivirus_1_48
			transl_table	11
4839	4552	CDS
			locus_tag	Homavirus_37_5
			product	hypothetical protein
			transl_table	11
>Homavirus_38
2	157	CDS
			locus_tag	Homavirus_38_1
			product	hypothetical protein
			transl_table	11
431	171	CDS
			locus_tag	Homavirus_38_2
			product	hypothetical protein Klosneuvirus_2_57
			transl_table	11
1306	509	CDS
			locus_tag	Homavirus_38_3
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
1470	2300	CDS
			locus_tag	Homavirus_38_4
			product	hypothetical protein
			transl_table	11
4159	2330	CDS
			locus_tag	Homavirus_38_5
			product	exosome complex exonuclease RRP44-like
			transl_table	11
4640	4230	CDS
			locus_tag	Homavirus_38_6
			product	hypothetical protein
			transl_table	11
>Homavirus_39
1	684	CDS
			locus_tag	Homavirus_39_1
			product	hypothetical protein
			transl_table	11
724	1674	CDS
			locus_tag	Homavirus_39_2
			product	hypothetical protein
			transl_table	11
1687	2799	CDS
			locus_tag	Homavirus_39_3
			product	hypothetical protein
			transl_table	11
2907	3341	CDS
			locus_tag	Homavirus_39_4
			product	hypothetical protein
			transl_table	11
3977	4225	CDS
			locus_tag	Homavirus_39_5
			product	hypothetical protein
			transl_table	11
>Homavirus_40
3	935	CDS
			locus_tag	Homavirus_40_1
			product	hypothetical protein CAOG_05959
			transl_table	11
1390	932	CDS
			locus_tag	Homavirus_40_2
			product	hypothetical protein
			transl_table	11
3032	1464	CDS
			locus_tag	Homavirus_40_3
			product	hypothetical protein ATCVMN08101_226L
			transl_table	11
3737	3306	CDS
			locus_tag	Homavirus_40_4
			product	hypothetical protein
			transl_table	11
>Homavirus_41
3658	2	CDS
			locus_tag	Homavirus_41_1
			product	hypothetical protein Klosneuvirus_1_130
			transl_table	11
>Homavirus_42
3	173	CDS
			locus_tag	Homavirus_42_1
			product	hypothetical protein
			transl_table	11
163	399	CDS
			locus_tag	Homavirus_42_2
			product	hypothetical protein
			transl_table	11
497	781	CDS
			locus_tag	Homavirus_42_3
			product	hypothetical protein
			transl_table	11
820	1017	CDS
			locus_tag	Homavirus_42_4
			product	hypothetical protein
			transl_table	11
1177	2766	CDS
			locus_tag	Homavirus_42_5
			product	hypothetical protein
			transl_table	11
3403	2789	CDS
			locus_tag	Homavirus_42_6
			product	hypothetical protein COY27_01515
			transl_table	11
>Homavirus_43
3	539	CDS
			locus_tag	Homavirus_43_1
			product	hypothetical protein
			transl_table	11
817	1497	CDS
			locus_tag	Homavirus_43_2
			product	hypothetical protein
			transl_table	11
1565	1807	CDS
			locus_tag	Homavirus_43_3
			product	hypothetical protein
			transl_table	11
1867	2406	CDS
			locus_tag	Homavirus_43_4
			product	hypothetical protein
			transl_table	11
3239	2415	CDS
			locus_tag	Homavirus_43_5
			product	hypothetical protein
			transl_table	11
>Homavirus_44
88	423	CDS
			locus_tag	Homavirus_44_1
			product	hypothetical protein
			transl_table	11
577	1158	CDS
			locus_tag	Homavirus_44_2
			product	PREDICTED: ras-related protein Rab-35
			transl_table	11
1282	2187	CDS
			locus_tag	Homavirus_44_3
			product	hypothetical protein
			transl_table	11
2789	2184	CDS
			locus_tag	Homavirus_44_4
			product	hypothetical protein
			transl_table	11
3165	2839	CDS
			locus_tag	Homavirus_44_5
			product	hypothetical protein
			transl_table	11
>Homavirus_45
3	338	CDS
			locus_tag	Homavirus_45_1
			product	hypothetical protein
			transl_table	11
2863	3075	CDS
			locus_tag	Homavirus_45_2
			product	hypothetical protein
			transl_table	11
>Homavirus_46
2	1336	CDS
			locus_tag	Homavirus_46_1
			product	cell division protein FtsH
			transl_table	11
1362	1817	CDS
			locus_tag	Homavirus_46_2
			product	hypothetical protein
			transl_table	11
1917	2948	CDS
			locus_tag	Homavirus_46_3
			product	XRN 5'-3' exonuclease
			transl_table	11
>Homavirus_47
3	182	CDS
			locus_tag	Homavirus_47_1
			product	hypothetical protein
			transl_table	11
97	2034	CDS
			locus_tag	Homavirus_47_2
			product	HrpA-like RNA helicase
			transl_table	11
2489	2968	CDS
			locus_tag	Homavirus_47_3
			product	HrpA-like RNA helicase
			transl_table	11
>Homavirus_48
230	1084	CDS
			locus_tag	Homavirus_48_1
			product	acetyltransferase
			transl_table	11
2201	1089	CDS
			locus_tag	Homavirus_48_2
			product	hypothetical protein ACA1_287570
			transl_table	11
2508	2894	CDS
			locus_tag	Homavirus_48_3
			product	rab family gtpase
			transl_table	11
>Homavirus_49
443	3	CDS
			locus_tag	Homavirus_49_1
			product	hypothetical protein
			transl_table	11
537	1739	CDS
			locus_tag	Homavirus_49_2
			product	patatin-like phospholipase
			transl_table	11
2054	1932	CDS
			locus_tag	Homavirus_49_3
			product	hypothetical protein
			transl_table	11
2336	2106	CDS
			locus_tag	Homavirus_49_4
			product	hypothetical protein
			transl_table	11
>Homavirus_50
2	109	CDS
			locus_tag	Homavirus_50_1
			product	hypothetical protein
			transl_table	11
207	473	CDS
			locus_tag	Homavirus_50_2
			product	hypothetical protein
			transl_table	11
1470	445	CDS
			locus_tag	Homavirus_50_3
			product	hypothetical protein KFL_005560135
			transl_table	11
2405	1620	CDS
			locus_tag	Homavirus_50_4
			product	hypothetical protein Klosneuvirus_1_399
			transl_table	11
>Homavirus_51
2	232	CDS
			locus_tag	Homavirus_51_1
			product	hypothetical protein
			transl_table	11
1186	200	CDS
			locus_tag	Homavirus_51_2
			product	elongation factor 1-alpha-like protein, partial
			transl_table	11
1224	1691	CDS
			locus_tag	Homavirus_51_3
			product	Ras-like protein ORAB-1
			transl_table	11
2309	2533	CDS
			locus_tag	Homavirus_51_4
			product	hypothetical protein
			transl_table	11
