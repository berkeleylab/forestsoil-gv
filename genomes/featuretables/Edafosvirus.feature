>Edafosvirus_1
371	3	CDS
			locus_tag	Edafosvirus_1_1
			product	hypothetical protein
			transl_table	11
904	683	CDS
			locus_tag	Edafosvirus_1_2
			product	hypothetical protein
			transl_table	11
1256	2974	CDS
			locus_tag	Edafosvirus_1_3
			product	putative transposase
			transl_table	11
3854	3111	CDS
			locus_tag	Edafosvirus_1_4
			product	hypothetical protein
			transl_table	11
4696	3998	CDS
			locus_tag	Edafosvirus_1_5
			product	hypothetical protein
			transl_table	11
5885	4776	CDS
			locus_tag	Edafosvirus_1_6
			product	hypothetical protein
			transl_table	11
6019	6567	CDS
			locus_tag	Edafosvirus_1_7
			product	hypothetical protein
			transl_table	11
6622	7047	CDS
			locus_tag	Edafosvirus_1_8
			product	hypothetical protein
			transl_table	11
7684	7013	CDS
			locus_tag	Edafosvirus_1_9
			product	hypothetical protein
			transl_table	11
8348	7707	CDS
			locus_tag	Edafosvirus_1_10
			product	hypothetical protein
			transl_table	11
8487	9323	CDS
			locus_tag	Edafosvirus_1_11
			product	hypothetical protein
			transl_table	11
9689	9423	CDS
			locus_tag	Edafosvirus_1_12
			product	hypothetical protein
			transl_table	11
9892	9743	CDS
			locus_tag	Edafosvirus_1_13
			product	hypothetical protein
			transl_table	11
10397	11260	CDS
			locus_tag	Edafosvirus_1_14
			product	hypothetical protein
			transl_table	11
11978	11271	CDS
			locus_tag	Edafosvirus_1_15
			product	hypothetical protein
			transl_table	11
12533	12033	CDS
			locus_tag	Edafosvirus_1_16
			product	hypothetical protein
			transl_table	11
13991	12657	CDS
			locus_tag	Edafosvirus_1_17
			product	hypothetical protein
			transl_table	11
14133	14360	CDS
			locus_tag	Edafosvirus_1_18
			product	hypothetical protein
			transl_table	11
15790	14489	CDS
			locus_tag	Edafosvirus_1_19
			product	hypothetical protein
			transl_table	11
16523	15822	CDS
			locus_tag	Edafosvirus_1_20
			product	hypothetical protein
			transl_table	11
16902	18281	CDS
			locus_tag	Edafosvirus_1_21
			product	hypothetical protein
			transl_table	11
18343	18957	CDS
			locus_tag	Edafosvirus_1_22
			product	hypothetical protein glt_00139
			transl_table	11
20545	18947	CDS
			locus_tag	Edafosvirus_1_23
			product	Arylamine N-acetyltransferase
			transl_table	11
21694	20744	CDS
			locus_tag	Edafosvirus_1_24
			product	hypothetical protein
			transl_table	11
21853	22323	CDS
			locus_tag	Edafosvirus_1_25
			product	hypothetical protein
			transl_table	11
22478	23881	CDS
			locus_tag	Edafosvirus_1_26
			product	hypothetical protein
			transl_table	11
24300	23890	CDS
			locus_tag	Edafosvirus_1_27
			product	hypothetical protein
			transl_table	11
24331	26763	CDS
			locus_tag	Edafosvirus_1_28
			product	DUF3160 domain-containing protein
			transl_table	11
26936	29203	CDS
			locus_tag	Edafosvirus_1_29
			product	DUF3160 domain-containing protein
			transl_table	11
29273	30226	CDS
			locus_tag	Edafosvirus_1_30
			product	hypothetical protein
			transl_table	11
30564	30241	CDS
			locus_tag	Edafosvirus_1_31
			product	hypothetical protein
			transl_table	11
32162	30642	CDS
			locus_tag	Edafosvirus_1_32
			product	hypothetical protein
			transl_table	11
32503	32204	CDS
			locus_tag	Edafosvirus_1_33
			product	hypothetical protein
			transl_table	11
32817	32539	CDS
			locus_tag	Edafosvirus_1_34
			product	hypothetical protein
			transl_table	11
33607	34482	CDS
			locus_tag	Edafosvirus_1_35
			product	hypothetical protein
			transl_table	11
35025	34492	CDS
			locus_tag	Edafosvirus_1_36
			product	hypothetical protein
			transl_table	11
35626	35129	CDS
			locus_tag	Edafosvirus_1_37
			product	hypothetical protein
			transl_table	11
35757	36725	CDS
			locus_tag	Edafosvirus_1_38
			product	hypothetical protein
			transl_table	11
37195	36728	CDS
			locus_tag	Edafosvirus_1_39
			product	hypothetical protein
			transl_table	11
37339	38343	CDS
			locus_tag	Edafosvirus_1_40
			product	hypothetical protein
			transl_table	11
39407	38340	CDS
			locus_tag	Edafosvirus_1_41
			product	hypothetical protein
			transl_table	11
40117	39530	CDS
			locus_tag	Edafosvirus_1_42
			product	GNAT family N-acetyltransferase
			transl_table	11
40275	40775	CDS
			locus_tag	Edafosvirus_1_43
			product	hypothetical protein
			transl_table	11
41700	40786	CDS
			locus_tag	Edafosvirus_1_44
			product	hypothetical protein
			transl_table	11
44132	41952	CDS
			locus_tag	Edafosvirus_1_45
			product	hypothetical protein Indivirus_1_31
			transl_table	11
44428	45048	CDS
			locus_tag	Edafosvirus_1_46
			product	hypothetical protein
			transl_table	11
46561	45041	CDS
			locus_tag	Edafosvirus_1_47
			product	LL-diaminopimelate aminotransferase
			transl_table	11
47212	46631	CDS
			locus_tag	Edafosvirus_1_48
			product	hypothetical protein
			transl_table	11
47983	47366	CDS
			locus_tag	Edafosvirus_1_49
			product	hypothetical protein
			transl_table	11
48463	48017	CDS
			locus_tag	Edafosvirus_1_50
			product	hypothetical protein
			transl_table	11
49976	48594	CDS
			locus_tag	Edafosvirus_1_51
			product	ABC transporter
			transl_table	11
50392	51384	CDS
			locus_tag	Edafosvirus_1_52
			product	hypothetical protein
			transl_table	11
52104	51391	CDS
			locus_tag	Edafosvirus_1_53
			product	hypothetical protein
			transl_table	11
52771	52172	CDS
			locus_tag	Edafosvirus_1_54
			product	hypothetical protein
			transl_table	11
52899	53693	CDS
			locus_tag	Edafosvirus_1_55
			product	hypothetical protein
			transl_table	11
54095	54871	CDS
			locus_tag	Edafosvirus_1_56
			product	hypothetical protein
			transl_table	11
54938	55702	CDS
			locus_tag	Edafosvirus_1_57
			product	predicted protein
			transl_table	11
56324	55680	CDS
			locus_tag	Edafosvirus_1_58
			product	hypothetical protein
			transl_table	11
56447	57382	CDS
			locus_tag	Edafosvirus_1_59
			product	hypothetical protein AK812_SmicGene647
			transl_table	11
57409	57618	CDS
			locus_tag	Edafosvirus_1_60
			product	hypothetical protein
			transl_table	11
58267	57626	CDS
			locus_tag	Edafosvirus_1_61
			product	hypothetical protein
			transl_table	11
58402	59433	CDS
			locus_tag	Edafosvirus_1_62
			product	hypothetical protein
			transl_table	11
62291	60048	CDS
			locus_tag	Edafosvirus_1_63
			product	hypothetical protein
			transl_table	11
63815	62361	CDS
			locus_tag	Edafosvirus_1_64
			product	CYP86A7
			transl_table	11
64619	63873	CDS
			locus_tag	Edafosvirus_1_65
			product	hypothetical protein
			transl_table	11
64771	65454	CDS
			locus_tag	Edafosvirus_1_66
			product	hypothetical protein
			transl_table	11
65509	66348	CDS
			locus_tag	Edafosvirus_1_67
			product	hypothetical protein
			transl_table	11
67340	66345	CDS
			locus_tag	Edafosvirus_1_68
			product	hypothetical protein
			transl_table	11
70610	67425	CDS
			locus_tag	Edafosvirus_1_69
			product	oxidoreductase FAD-binding
			transl_table	11
71619	70735	CDS
			locus_tag	Edafosvirus_1_70
			product	peptide chain release factor 1 eRF1
			transl_table	11
71863	71756	CDS
			locus_tag	Edafosvirus_1_71
			product	hypothetical protein
			transl_table	11
72801	71917	CDS
			locus_tag	Edafosvirus_1_72
			product	hypothetical protein
			transl_table	11
74089	72860	CDS
			locus_tag	Edafosvirus_1_73
			product	hypothetical protein C5B51_26365
			transl_table	11
74564	74109	CDS
			locus_tag	Edafosvirus_1_74
			product	hypothetical protein
			transl_table	11
74961	74626	CDS
			locus_tag	Edafosvirus_1_75
			product	hypothetical protein
			transl_table	11
75374	75063	CDS
			locus_tag	Edafosvirus_1_76
			product	hypothetical protein
			transl_table	11
75404	75742	CDS
			locus_tag	Edafosvirus_1_77
			product	hypothetical protein
			transl_table	11
77288	75732	CDS
			locus_tag	Edafosvirus_1_78
			product	linear amide C-N hydrolase
			transl_table	11
77535	78278	CDS
			locus_tag	Edafosvirus_1_79
			product	hypothetical protein
			transl_table	11
78559	78296	CDS
			locus_tag	Edafosvirus_1_80
			product	hypothetical protein
			transl_table	11
78736	79692	CDS
			locus_tag	Edafosvirus_1_81
			product	hypothetical protein
			transl_table	11
80903	79698	CDS
			locus_tag	Edafosvirus_1_82
			product	histidine phosphatase domain-containing protein
			transl_table	11
81123	81830	CDS
			locus_tag	Edafosvirus_1_83
			product	hypothetical protein
			transl_table	11
81974	82351	CDS
			locus_tag	Edafosvirus_1_84
			product	hypothetical protein
			transl_table	11
82980	82510	CDS
			locus_tag	Edafosvirus_1_85
			product	hypothetical protein
			transl_table	11
83433	83107	CDS
			locus_tag	Edafosvirus_1_86
			product	hypothetical protein
			transl_table	11
83687	83418	CDS
			locus_tag	Edafosvirus_1_87
			product	hypothetical protein
			transl_table	11
84609	83743	CDS
			locus_tag	Edafosvirus_1_88
			product	hypothetical protein A2270_01305
			transl_table	11
84756	84652	CDS
			locus_tag	Edafosvirus_1_89
			product	hypothetical protein
			transl_table	11
85901	85095	CDS
			locus_tag	Edafosvirus_1_90
			product	hypothetical protein A2Y53_07335
			transl_table	11
86865	86080	CDS
			locus_tag	Edafosvirus_1_91
			product	hypothetical protein
			transl_table	11
87258	86944	CDS
			locus_tag	Edafosvirus_1_92
			product	PREDICTED: dihydrolipoyllysine-residue succinyltransferase component of 2-oxoglutarate dehydrogenase complex, mitochondrial
			transl_table	11
87409	88074	CDS
			locus_tag	Edafosvirus_1_93
			product	hypothetical protein
			transl_table	11
88705	88076	CDS
			locus_tag	Edafosvirus_1_94
			product	hypothetical protein
			transl_table	11
89998	88760	CDS
			locus_tag	Edafosvirus_1_95
			product	hypothetical protein
			transl_table	11
90899	90051	CDS
			locus_tag	Edafosvirus_1_96
			product	hypothetical protein
			transl_table	11
90977	91513	CDS
			locus_tag	Edafosvirus_1_97
			product	hypothetical protein
			transl_table	11
91889	91524	CDS
			locus_tag	Edafosvirus_1_98
			product	hypothetical protein
			transl_table	11
92287	91973	CDS
			locus_tag	Edafosvirus_1_99
			product	hypothetical protein
			transl_table	11
92420	96706	CDS
			locus_tag	Edafosvirus_1_100
			product	ankyrin repeat protein
			transl_table	11
97726	96713	CDS
			locus_tag	Edafosvirus_1_101
			product	pentapeptide repeat-containing protein
			transl_table	11
98634	97792	CDS
			locus_tag	Edafosvirus_1_102
			product	hypothetical protein
			transl_table	11
98766	99137	CDS
			locus_tag	Edafosvirus_1_103
			product	peptidyl-tRNA hydrolase
			transl_table	11
100482	99142	CDS
			locus_tag	Edafosvirus_1_104
			product	hypothetical protein
			transl_table	11
100598	101515	CDS
			locus_tag	Edafosvirus_1_105
			product	SprT-like protein
			transl_table	11
101540	102016	CDS
			locus_tag	Edafosvirus_1_106
			product	hypothetical protein
			transl_table	11
102575	102006	CDS
			locus_tag	Edafosvirus_1_107
			product	hypothetical protein Hokovirus_1_66
			transl_table	11
102733	103695	CDS
			locus_tag	Edafosvirus_1_108
			product	hypothetical protein
			transl_table	11
104802	103711	CDS
			locus_tag	Edafosvirus_1_109
			product	hypothetical protein
			transl_table	11
104979	105161	CDS
			locus_tag	Edafosvirus_1_110
			product	hypothetical protein
			transl_table	11
106861	105317	CDS
			locus_tag	Edafosvirus_1_111
			product	ISSoc2, transposase
			transl_table	11
107019	107372	CDS
			locus_tag	Edafosvirus_1_112
			product	hypothetical protein
			transl_table	11
108068	107397	CDS
			locus_tag	Edafosvirus_1_113
			product	hypothetical protein
			transl_table	11
109163	108132	CDS
			locus_tag	Edafosvirus_1_114
			product	hypothetical protein
			transl_table	11
109265	109951	CDS
			locus_tag	Edafosvirus_1_115
			product	hypothetical protein
			transl_table	11
110797	110114	CDS
			locus_tag	Edafosvirus_1_116
			product	hypothetical protein
			transl_table	11
111853	110864	CDS
			locus_tag	Edafosvirus_1_117
			product	PREDICTED: uncharacterized protein LOC101235891
			transl_table	11
112160	111972	CDS
			locus_tag	Edafosvirus_1_118
			product	hypothetical protein
			transl_table	11
112828	112289	CDS
			locus_tag	Edafosvirus_1_119
			product	hypothetical protein
			transl_table	11
113861	112905	CDS
			locus_tag	Edafosvirus_1_120
			product	hypothetical protein
			transl_table	11
114964	113927	CDS
			locus_tag	Edafosvirus_1_121
			product	GMP reductase
			transl_table	11
115529	115122	CDS
			locus_tag	Edafosvirus_1_122
			product	hypothetical protein
			transl_table	11
116124	115735	CDS
			locus_tag	Edafosvirus_1_123
			product	hypothetical protein
			transl_table	11
116249	117445	CDS
			locus_tag	Edafosvirus_1_124
			product	hypothetical protein
			transl_table	11
118381	117467	CDS
			locus_tag	Edafosvirus_1_125
			product	hypothetical protein
			transl_table	11
119560	118862	CDS
			locus_tag	Edafosvirus_1_126
			product	hypothetical protein
			transl_table	11
120012	119743	CDS
			locus_tag	Edafosvirus_1_127
			product	hypothetical protein
			transl_table	11
120254	120853	CDS
			locus_tag	Edafosvirus_1_128
			product	hypothetical protein
			transl_table	11
121875	120850	CDS
			locus_tag	Edafosvirus_1_129
			product	hypothetical protein CBC91_00715
			transl_table	11
123964	122630	CDS
			locus_tag	Edafosvirus_1_130
			product	hypothetical protein
			transl_table	11
124249	124731	CDS
			locus_tag	Edafosvirus_1_131
			product	hypothetical protein
			transl_table	11
125349	124732	CDS
			locus_tag	Edafosvirus_1_132
			product	hypothetical protein mc_770
			transl_table	11
125535	126530	CDS
			locus_tag	Edafosvirus_1_133
			product	hypothetical protein
			transl_table	11
126662	128101	CDS
			locus_tag	Edafosvirus_1_134
			product	hypothetical protein
			transl_table	11
128296	129708	CDS
			locus_tag	Edafosvirus_1_135
			product	E3 ubiquitin-protein ligase ATL41-like
			transl_table	11
129835	130443	CDS
			locus_tag	Edafosvirus_1_136
			product	hypothetical protein
			transl_table	11
131010	130447	CDS
			locus_tag	Edafosvirus_1_137
			product	hypothetical protein
			transl_table	11
131802	131056	CDS
			locus_tag	Edafosvirus_1_138
			product	hypothetical protein
			transl_table	11
131938	132411	CDS
			locus_tag	Edafosvirus_1_139
			product	hypothetical protein
			transl_table	11
132752	132489	CDS
			locus_tag	Edafosvirus_1_140
			product	hypothetical protein
			transl_table	11
133282	132944	CDS
			locus_tag	Edafosvirus_1_141
			product	hypothetical protein
			transl_table	11
134200	133502	CDS
			locus_tag	Edafosvirus_1_142
			product	hypothetical protein
			transl_table	11
134658	134260	CDS
			locus_tag	Edafosvirus_1_143
			product	hypothetical protein
			transl_table	11
134804	135886	CDS
			locus_tag	Edafosvirus_1_144
			product	hypothetical protein
			transl_table	11
135906	136226	CDS
			locus_tag	Edafosvirus_1_145
			product	hypothetical protein
			transl_table	11
136238	137251	CDS
			locus_tag	Edafosvirus_1_146
			product	Carboxylesterase NlhH
			transl_table	11
137307	138161	CDS
			locus_tag	Edafosvirus_1_147
			product	hypothetical protein
			transl_table	11
138654	138151	CDS
			locus_tag	Edafosvirus_1_148
			product	hypothetical protein SAMD00019534_012320
			transl_table	11
138987	138709	CDS
			locus_tag	Edafosvirus_1_149
			product	hypothetical protein
			transl_table	11
140012	139401	CDS
			locus_tag	Edafosvirus_1_150
			product	hypothetical protein
			transl_table	11
141816	140077	CDS
			locus_tag	Edafosvirus_1_151
			product	predicted protein
			transl_table	11
142703	142810	CDS
			locus_tag	Edafosvirus_1_152
			product	hypothetical protein
			transl_table	11
>Edafosvirus_2
325	1530	CDS
			locus_tag	Edafosvirus_2_1
			product	hypothetical protein
			transl_table	11
1594	1980	CDS
			locus_tag	Edafosvirus_2_2
			product	hypothetical protein
			transl_table	11
2086	3342	CDS
			locus_tag	Edafosvirus_2_3
			product	hypothetical protein
			transl_table	11
4338	3523	CDS
			locus_tag	Edafosvirus_2_4
			product	hypothetical protein
			transl_table	11
5215	4598	CDS
			locus_tag	Edafosvirus_2_5
			product	hypothetical protein
			transl_table	11
5613	5317	CDS
			locus_tag	Edafosvirus_2_6
			product	hypothetical protein
			transl_table	11
5853	6812	CDS
			locus_tag	Edafosvirus_2_7
			product	hypothetical protein
			transl_table	11
7358	6807	CDS
			locus_tag	Edafosvirus_2_8
			product	hypothetical protein
			transl_table	11
7477	8238	CDS
			locus_tag	Edafosvirus_2_9
			product	hypothetical protein
			transl_table	11
8443	9261	CDS
			locus_tag	Edafosvirus_2_10
			product	metallophosphatase
			transl_table	11
9773	9315	CDS
			locus_tag	Edafosvirus_2_11
			product	hypothetical protein Catovirus_1_725
			transl_table	11
9866	10462	CDS
			locus_tag	Edafosvirus_2_12
			product	hypothetical protein
			transl_table	11
11111	10452	CDS
			locus_tag	Edafosvirus_2_13
			product	E3 ubiquitin-protein ligase Mdm2
			transl_table	11
11260	11700	CDS
			locus_tag	Edafosvirus_2_14
			product	hypothetical protein
			transl_table	11
12389	13243	CDS
			locus_tag	Edafosvirus_2_15
			product	hypothetical protein
			transl_table	11
13254	13793	CDS
			locus_tag	Edafosvirus_2_16
			product	hypothetical protein
			transl_table	11
13935	14051	CDS
			locus_tag	Edafosvirus_2_17
			product	hypothetical protein
			transl_table	11
14123	14443	CDS
			locus_tag	Edafosvirus_2_18
			product	hypothetical protein
			transl_table	11
14509	14727	CDS
			locus_tag	Edafosvirus_2_19
			product	hypothetical protein
			transl_table	11
14800	15072	CDS
			locus_tag	Edafosvirus_2_20
			product	hypothetical protein
			transl_table	11
15166	15447	CDS
			locus_tag	Edafosvirus_2_21
			product	hypothetical protein
			transl_table	11
15527	18064	CDS
			locus_tag	Edafosvirus_2_22
			product	mimivirus elongation factor aef-2
			transl_table	11
18171	18833	CDS
			locus_tag	Edafosvirus_2_23
			product	hypothetical protein
			transl_table	11
19087	18977	CDS
			locus_tag	Edafosvirus_2_24
			product	hypothetical protein
			transl_table	11
19110	20366	CDS
			locus_tag	Edafosvirus_2_25
			product	hypothetical protein RO3G_04913
			transl_table	11
21668	20373	CDS
			locus_tag	Edafosvirus_2_26
			product	hypothetical protein
			transl_table	11
21806	22345	CDS
			locus_tag	Edafosvirus_2_27
			product	peptide-methionine (S)-S-oxide reductase
			transl_table	11
22786	22346	CDS
			locus_tag	Edafosvirus_2_28
			product	vitamin K epoxide reductase complex subunit 1
			transl_table	11
24025	22850	CDS
			locus_tag	Edafosvirus_2_29
			product	hypothetical protein
			transl_table	11
24201	24410	CDS
			locus_tag	Edafosvirus_2_30
			product	hypothetical protein
			transl_table	11
25570	24407	CDS
			locus_tag	Edafosvirus_2_31
			product	hypothetical protein
			transl_table	11
26003	25608	CDS
			locus_tag	Edafosvirus_2_32
			product	hypothetical protein
			transl_table	11
26314	26096	CDS
			locus_tag	Edafosvirus_2_33
			product	hypothetical protein
			transl_table	11
26371	26490	CDS
			locus_tag	Edafosvirus_2_34
			product	hypothetical protein
			transl_table	11
26435	28312	CDS
			locus_tag	Edafosvirus_2_35
			product	hypothetical protein
			transl_table	11
28646	28383	CDS
			locus_tag	Edafosvirus_2_36
			product	hypothetical protein
			transl_table	11
28833	30203	CDS
			locus_tag	Edafosvirus_2_37
			product	hypothetical protein glt_00063
			transl_table	11
30318	31916	CDS
			locus_tag	Edafosvirus_2_38
			product	hypothetical protein
			transl_table	11
32469	31891	CDS
			locus_tag	Edafosvirus_2_39
			product	hypothetical protein
			transl_table	11
33376	32525	CDS
			locus_tag	Edafosvirus_2_40
			product	PREDICTED: m7GpppX diphosphatase isoform X2
			transl_table	11
33517	34263	CDS
			locus_tag	Edafosvirus_2_41
			product	hypothetical protein
			transl_table	11
35550	34396	CDS
			locus_tag	Edafosvirus_2_42
			product	hypothetical protein
			transl_table	11
35619	36680	CDS
			locus_tag	Edafosvirus_2_43
			product	hypothetical protein Klosneuvirus_3_188
			transl_table	11
37623	36649	CDS
			locus_tag	Edafosvirus_2_44
			product	tetratricopeptide repeat protein
			transl_table	11
37755	37859	CDS
			locus_tag	Edafosvirus_2_45
			product	hypothetical protein
			transl_table	11
38203	39120	CDS
			locus_tag	Edafosvirus_2_46
			product	hypothetical protein
			transl_table	11
39181	40161	CDS
			locus_tag	Edafosvirus_2_47
			product	hypothetical protein
			transl_table	11
40158	40691	CDS
			locus_tag	Edafosvirus_2_48
			product	nucleoside deaminase
			transl_table	11
40853	42019	CDS
			locus_tag	Edafosvirus_2_49
			product	hypothetical protein
			transl_table	11
42077	42814	CDS
			locus_tag	Edafosvirus_2_50
			product	hypothetical protein
			transl_table	11
42948	44282	CDS
			locus_tag	Edafosvirus_2_51
			product	hypothetical protein
			transl_table	11
44469	44654	CDS
			locus_tag	Edafosvirus_2_52
			product	hypothetical protein
			transl_table	11
44680	44850	CDS
			locus_tag	Edafosvirus_2_53
			product	hypothetical protein
			transl_table	11
45408	44851	CDS
			locus_tag	Edafosvirus_2_54
			product	hypothetical protein
			transl_table	11
46482	45478	CDS
			locus_tag	Edafosvirus_2_55
			product	hypothetical protein
			transl_table	11
47342	46611	CDS
			locus_tag	Edafosvirus_2_56
			product	hypothetical protein
			transl_table	11
47516	48748	CDS
			locus_tag	Edafosvirus_2_57
			product	predicted protein
			transl_table	11
48812	49828	CDS
			locus_tag	Edafosvirus_2_58
			product	hypothetical protein
			transl_table	11
49844	50782	CDS
			locus_tag	Edafosvirus_2_59
			product	hypothetical protein
			transl_table	11
52055	50784	CDS
			locus_tag	Edafosvirus_2_60
			product	hypothetical protein
			transl_table	11
52175	52885	CDS
			locus_tag	Edafosvirus_2_61
			product	exonuclease
			transl_table	11
53360	52857	CDS
			locus_tag	Edafosvirus_2_62
			product	hypothetical protein
			transl_table	11
54027	53458	CDS
			locus_tag	Edafosvirus_2_63
			product	hypothetical protein
			transl_table	11
54763	54188	CDS
			locus_tag	Edafosvirus_2_64
			product	hypothetical protein
			transl_table	11
54898	55422	CDS
			locus_tag	Edafosvirus_2_65
			product	hypothetical protein
			transl_table	11
55503	56051	CDS
			locus_tag	Edafosvirus_2_66
			product	hypothetical protein
			transl_table	11
57015	56044	CDS
			locus_tag	Edafosvirus_2_67
			product	hypothetical protein
			transl_table	11
57156	58424	CDS
			locus_tag	Edafosvirus_2_68
			product	hypothetical protein
			transl_table	11
58517	58693	CDS
			locus_tag	Edafosvirus_2_69
			product	hypothetical protein
			transl_table	11
59359	58700	CDS
			locus_tag	Edafosvirus_2_70
			product	hypothetical protein
			transl_table	11
59564	60778	CDS
			locus_tag	Edafosvirus_2_71
			product	hypothetical protein
			transl_table	11
60988	61188	CDS
			locus_tag	Edafosvirus_2_72
			product	hypothetical protein
			transl_table	11
61242	61958	CDS
			locus_tag	Edafosvirus_2_73
			product	hypothetical protein
			transl_table	11
63273	61960	CDS
			locus_tag	Edafosvirus_2_74
			product	alanine--tRNA ligase
			transl_table	11
64536	63346	CDS
			locus_tag	Edafosvirus_2_75
			product	hypothetical protein
			transl_table	11
65543	64602	CDS
			locus_tag	Edafosvirus_2_76
			product	hypothetical protein
			transl_table	11
65705	66922	CDS
			locus_tag	Edafosvirus_2_77
			product	peptidase S8 and S53 subtilisin kexin sedolisin
			transl_table	11
68251	66947	CDS
			locus_tag	Edafosvirus_2_78
			product	hypothetical protein
			transl_table	11
69661	68369	CDS
			locus_tag	Edafosvirus_2_79
			product	hypothetical protein
			transl_table	11
69812	72940	CDS
			locus_tag	Edafosvirus_2_80
			product	hypothetical protein
			transl_table	11
73774	72947	CDS
			locus_tag	Edafosvirus_2_81
			product	hypothetical protein
			transl_table	11
74877	73846	CDS
			locus_tag	Edafosvirus_2_82
			product	hypothetical protein
			transl_table	11
75568	74945	CDS
			locus_tag	Edafosvirus_2_83
			product	Alkylated DNA repair dioxygenase AlkB
			transl_table	11
75903	77078	CDS
			locus_tag	Edafosvirus_2_84
			product	Tryptophanyl-tRNA synthetase, putative
			transl_table	11
77378	77082	CDS
			locus_tag	Edafosvirus_2_85
			product	hypothetical protein
			transl_table	11
78616	77447	CDS
			locus_tag	Edafosvirus_2_86
			product	hypothetical protein
			transl_table	11
78733	79269	CDS
			locus_tag	Edafosvirus_2_87
			product	hypothetical protein
			transl_table	11
79371	79928	CDS
			locus_tag	Edafosvirus_2_88
			product	hypothetical protein
			transl_table	11
79986	81278	CDS
			locus_tag	Edafosvirus_2_89
			product	hypothetical protein
			transl_table	11
81871	81281	CDS
			locus_tag	Edafosvirus_2_90
			product	GTP-binding protein ypt1
			transl_table	11
82046	82963	CDS
			locus_tag	Edafosvirus_2_91
			product	hypothetical protein
			transl_table	11
83027	83821	CDS
			locus_tag	Edafosvirus_2_92
			product	GTP-binding protein YPTC1
			transl_table	11
83898	84551	CDS
			locus_tag	Edafosvirus_2_93
			product	hypothetical protein, conserved
			transl_table	11
84533	84637	CDS
			locus_tag	Edafosvirus_2_94
			product	hypothetical protein
			transl_table	11
84634	85401	CDS
			locus_tag	Edafosvirus_2_95
			product	hypothetical protein
			transl_table	11
85508	86767	CDS
			locus_tag	Edafosvirus_2_96
			product	hypothetical protein
			transl_table	11
86832	88079	CDS
			locus_tag	Edafosvirus_2_97
			product	hypothetical protein
			transl_table	11
88156	89130	CDS
			locus_tag	Edafosvirus_2_98
			product	hypothetical protein glt_00909
			transl_table	11
89310	89119	CDS
			locus_tag	Edafosvirus_2_99
			product	hypothetical protein
			transl_table	11
90630	89386	CDS
			locus_tag	Edafosvirus_2_100
			product	PREDICTED: uncharacterized protein LOC105844216
			transl_table	11
91106	91252	CDS
			locus_tag	Edafosvirus_2_101
			product	hypothetical protein
			transl_table	11
>Edafosvirus_3
1	177	CDS
			locus_tag	Edafosvirus_3_1
			product	hypothetical protein
			transl_table	11
1539	223	CDS
			locus_tag	Edafosvirus_3_2
			product	hypothetical protein
			transl_table	11
1676	1942	CDS
			locus_tag	Edafosvirus_3_3
			product	hypothetical protein
			transl_table	11
2523	4040	CDS
			locus_tag	Edafosvirus_3_4
			product	asparaginyl-tRNA synthetase
			transl_table	11
4539	4132	CDS
			locus_tag	Edafosvirus_3_5
			product	PREDICTED: RING finger protein 122-like
			transl_table	11
4678	5283	CDS
			locus_tag	Edafosvirus_3_6
			product	hypothetical protein
			transl_table	11
5329	8886	CDS
			locus_tag	Edafosvirus_3_7
			product	hypothetical protein
			transl_table	11
9293	9493	CDS
			locus_tag	Edafosvirus_3_8
			product	hypothetical protein
			transl_table	11
9564	9869	CDS
			locus_tag	Edafosvirus_3_9
			product	hypothetical protein
			transl_table	11
10421	9864	CDS
			locus_tag	Edafosvirus_3_10
			product	hypothetical protein RclHR1_11750003
			transl_table	11
10547	11713	CDS
			locus_tag	Edafosvirus_3_11
			product	PREDICTED: TNF receptor-associated factor 5
			transl_table	11
11774	13066	CDS
			locus_tag	Edafosvirus_3_12
			product	tnf receptor-associated factor 4b
			transl_table	11
13150	13665	CDS
			locus_tag	Edafosvirus_3_13
			product	hypothetical protein
			transl_table	11
13942	14967	CDS
			locus_tag	Edafosvirus_3_14
			product	GTP binding translation elongation factor
			transl_table	11
15547	14969	CDS
			locus_tag	Edafosvirus_3_15
			product	predicted protein
			transl_table	11
15651	17075	CDS
			locus_tag	Edafosvirus_3_16
			product	hypothetical protein Catovirus_1_853
			transl_table	11
17787	17077	CDS
			locus_tag	Edafosvirus_3_17
			product	hypothetical protein
			transl_table	11
17912	18250	CDS
			locus_tag	Edafosvirus_3_18
			product	hypothetical protein
			transl_table	11
19033	18269	CDS
			locus_tag	Edafosvirus_3_19
			product	hypothetical protein K457DRAFT_151319
			transl_table	11
19080	19832	CDS
			locus_tag	Edafosvirus_3_20
			product	hypothetical protein
			transl_table	11
20604	19834	CDS
			locus_tag	Edafosvirus_3_21
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
21185	20679	CDS
			locus_tag	Edafosvirus_3_22
			product	hypothetical protein
			transl_table	11
21576	21250	CDS
			locus_tag	Edafosvirus_3_23
			product	hypothetical protein
			transl_table	11
22180	21713	CDS
			locus_tag	Edafosvirus_3_24
			product	hypothetical protein BpV1_157
			transl_table	11
23399	22221	CDS
			locus_tag	Edafosvirus_3_25
			product	autophagy-related protein 18a-like
			transl_table	11
23738	24505	CDS
			locus_tag	Edafosvirus_3_26
			product	hypothetical protein
			transl_table	11
24556	25173	CDS
			locus_tag	Edafosvirus_3_27
			product	hypothetical protein
			transl_table	11
25236	25856	CDS
			locus_tag	Edafosvirus_3_28
			product	hypothetical protein
			transl_table	11
25941	28004	CDS
			locus_tag	Edafosvirus_3_29
			product	DEAD/SNF2-like helicase
			transl_table	11
29410	28001	CDS
			locus_tag	Edafosvirus_3_30
			product	hypothetical protein
			transl_table	11
29559	30374	CDS
			locus_tag	Edafosvirus_3_31
			product	hypothetical protein
			transl_table	11
30473	30787	CDS
			locus_tag	Edafosvirus_3_32
			product	hypothetical protein
			transl_table	11
32224	30791	CDS
			locus_tag	Edafosvirus_3_33
			product	Cysteine--trna ligase
			transl_table	11
32327	32782	CDS
			locus_tag	Edafosvirus_3_34
			product	NCLDV major capsid protein
			transl_table	11
33325	32783	CDS
			locus_tag	Edafosvirus_3_35
			product	hypothetical protein
			transl_table	11
34251	33382	CDS
			locus_tag	Edafosvirus_3_36
			product	hypothetical protein
			transl_table	11
34343	35992	CDS
			locus_tag	Edafosvirus_3_37
			product	dna topoisomerase 1b
			transl_table	11
36085	36585	CDS
			locus_tag	Edafosvirus_3_38
			product	hypothetical protein
			transl_table	11
36642	37802	CDS
			locus_tag	Edafosvirus_3_39
			product	Glycosyl transferase, group 2
			transl_table	11
37820	38992	CDS
			locus_tag	Edafosvirus_3_40
			product	FAD-binding protein
			transl_table	11
39059	39520	CDS
			locus_tag	Edafosvirus_3_41
			product	hypothetical protein
			transl_table	11
39575	40675	CDS
			locus_tag	Edafosvirus_3_42
			product	hypothetical protein
			transl_table	11
41147	41491	CDS
			locus_tag	Edafosvirus_3_43
			product	hypothetical protein
			transl_table	11
41526	42191	CDS
			locus_tag	Edafosvirus_3_44
			product	hypothetical protein
			transl_table	11
42259	42858	CDS
			locus_tag	Edafosvirus_3_45
			product	hypothetical protein
			transl_table	11
42991	43476	CDS
			locus_tag	Edafosvirus_3_46
			product	hypothetical protein
			transl_table	11
43570	44163	CDS
			locus_tag	Edafosvirus_3_47
			product	hypothetical protein
			transl_table	11
44189	44767	CDS
			locus_tag	Edafosvirus_3_48
			product	hypothetical protein
			transl_table	11
44823	45539	CDS
			locus_tag	Edafosvirus_3_49
			product	uncharacterized protein LOC110103309 isoform X1
			transl_table	11
45590	45982	CDS
			locus_tag	Edafosvirus_3_50
			product	hypothetical protein
			transl_table	11
46098	46634	CDS
			locus_tag	Edafosvirus_3_51
			product	hypothetical protein
			transl_table	11
49401	46759	CDS
			locus_tag	Edafosvirus_3_52
			product	glycosyltransferase
			transl_table	11
49917	49447	CDS
			locus_tag	Edafosvirus_3_53
			product	Anamorsin -like protein
			transl_table	11
50619	49987	CDS
			locus_tag	Edafosvirus_3_54
			product	hypothetical protein
			transl_table	11
50691	51080	CDS
			locus_tag	Edafosvirus_3_55
			product	hypothetical protein
			transl_table	11
51942	51085	CDS
			locus_tag	Edafosvirus_3_56
			product	glycosyltransferase family 25
			transl_table	11
51994	52533	CDS
			locus_tag	Edafosvirus_3_57
			product	hypothetical protein COU58_00925
			transl_table	11
52974	52525	CDS
			locus_tag	Edafosvirus_3_58
			product	hypothetical protein
			transl_table	11
53776	52979	CDS
			locus_tag	Edafosvirus_3_59
			product	putative glycosyltransferase
			transl_table	11
53829	54434	CDS
			locus_tag	Edafosvirus_3_60
			product	CDP-alcohol phosphatidyltransferase
			transl_table	11
54463	55065	CDS
			locus_tag	Edafosvirus_3_61
			product	CDP-alcohol phosphatidyltransferase
			transl_table	11
55870	55067	CDS
			locus_tag	Edafosvirus_3_62
			product	hypothetical protein
			transl_table	11
56452	56045	CDS
			locus_tag	Edafosvirus_3_63
			product	hypothetical protein
			transl_table	11
56591	59068	CDS
			locus_tag	Edafosvirus_3_64
			product	hypothetical protein
			transl_table	11
59193	59092	CDS
			locus_tag	Edafosvirus_3_65
			product	hypothetical protein
			transl_table	11
59842	59159	CDS
			locus_tag	Edafosvirus_3_66
			product	E3 ubiquitin-protein ligase SIAH1B-like
			transl_table	11
61229	59847	CDS
			locus_tag	Edafosvirus_3_67
			product	hypothetical protein
			transl_table	11
61375	62217	CDS
			locus_tag	Edafosvirus_3_68
			product	hypothetical protein
			transl_table	11
62372	63385	CDS
			locus_tag	Edafosvirus_3_69
			product	glutamine synthetase
			transl_table	11
63439	65103	CDS
			locus_tag	Edafosvirus_3_70
			product	hypothetical protein
			transl_table	11
65979	65098	CDS
			locus_tag	Edafosvirus_3_71
			product	hypothetical protein
			transl_table	11
66658	66047	CDS
			locus_tag	Edafosvirus_3_72
			product	hypothetical protein
			transl_table	11
66841	67749	CDS
			locus_tag	Edafosvirus_3_73
			product	hypothetical protein
			transl_table	11
67820	68284	CDS
			locus_tag	Edafosvirus_3_74
			product	hypothetical protein
			transl_table	11
68372	69046	CDS
			locus_tag	Edafosvirus_3_75
			product	hypothetical protein
			transl_table	11
69154	69717	CDS
			locus_tag	Edafosvirus_3_76
			product	hypothetical protein
			transl_table	11
70507	69710	CDS
			locus_tag	Edafosvirus_3_77
			product	hypothetical protein
			transl_table	11
70607	72370	CDS
			locus_tag	Edafosvirus_3_78
			product	hypothetical protein Catovirus_2_65
			transl_table	11
72406	74064	CDS
			locus_tag	Edafosvirus_3_79
			product	hypothetical protein Catovirus_1_68
			transl_table	11
74118	75086	CDS
			locus_tag	Edafosvirus_3_80
			product	hypothetical protein
			transl_table	11
75157	75915	CDS
			locus_tag	Edafosvirus_3_81
			product	hypothetical protein
			transl_table	11
75988	76485	CDS
			locus_tag	Edafosvirus_3_82
			product	hypothetical protein
			transl_table	11
76624	76935	CDS
			locus_tag	Edafosvirus_3_83
			product	hypothetical protein
			transl_table	11
77729	76932	CDS
			locus_tag	Edafosvirus_3_84
			product	hypothetical protein
			transl_table	11
78912	77875	CDS
			locus_tag	Edafosvirus_3_85
			product	DnaJ-like subfamily C member 3
			transl_table	11
79317	78967	CDS
			locus_tag	Edafosvirus_3_86
			product	aldehyde-activating protein
			transl_table	11
79406	79909	CDS
			locus_tag	Edafosvirus_3_87
			product	hypothetical protein
			transl_table	11
80415	79906	CDS
			locus_tag	Edafosvirus_3_88
			product	predicted protein
			transl_table	11
80833	80480	CDS
			locus_tag	Edafosvirus_3_89
			product	hypothetical protein
			transl_table	11
81246	80854	CDS
			locus_tag	Edafosvirus_3_90
			product	hypothetical protein
			transl_table	11
81339	82364	CDS
			locus_tag	Edafosvirus_3_91
			product	HNH endonuclease
			transl_table	11
82674	84107	CDS
			locus_tag	Edafosvirus_3_92
			product	elongation factor 1 alpha long form
			transl_table	11
84233	85975	CDS
			locus_tag	Edafosvirus_3_93
			product	hypothetical protein Catovirus_1_1084
			transl_table	11
86568	86002	CDS
			locus_tag	Edafosvirus_3_94
			product	hypothetical protein
			transl_table	11
>Edafosvirus_4
3	920	CDS
			locus_tag	Edafosvirus_4_1
			product	hypothetical protein
			transl_table	11
1661	936	CDS
			locus_tag	Edafosvirus_4_2
			product	PREDICTED: uncharacterized protein LOC101235891
			transl_table	11
2339	1740	CDS
			locus_tag	Edafosvirus_4_3
			product	hypothetical protein
			transl_table	11
3014	2403	CDS
			locus_tag	Edafosvirus_4_4
			product	hypothetical protein
			transl_table	11
3647	3231	CDS
			locus_tag	Edafosvirus_4_5
			product	hypothetical protein
			transl_table	11
3944	4720	CDS
			locus_tag	Edafosvirus_4_6
			product	NUDIX hydrolase
			transl_table	11
4742	5422	CDS
			locus_tag	Edafosvirus_4_7
			product	hypothetical protein
			transl_table	11
5517	6509	CDS
			locus_tag	Edafosvirus_4_8
			product	hypothetical protein
			transl_table	11
6555	7169	CDS
			locus_tag	Edafosvirus_4_9
			product	hypothetical protein
			transl_table	11
8776	7187	CDS
			locus_tag	Edafosvirus_4_10
			product	hypothetical protein
			transl_table	11
8875	9477	CDS
			locus_tag	Edafosvirus_4_11
			product	dolichyldiphosphatase
			transl_table	11
11004	9481	CDS
			locus_tag	Edafosvirus_4_12
			product	hypothetical protein Klosneuvirus_1_218
			transl_table	11
11140	12117	CDS
			locus_tag	Edafosvirus_4_13
			product	GDP-mannose 6-dehydrogenase
			transl_table	11
12262	13545	CDS
			locus_tag	Edafosvirus_4_14
			product	hypothetical protein
			transl_table	11
13674	14522	CDS
			locus_tag	Edafosvirus_4_15
			product	hypothetical protein
			transl_table	11
14636	15451	CDS
			locus_tag	Edafosvirus_4_16
			product	hypothetical protein
			transl_table	11
15837	15457	CDS
			locus_tag	Edafosvirus_4_17
			product	hypothetical protein
			transl_table	11
15963	18983	CDS
			locus_tag	Edafosvirus_4_18
			product	UvrD/REP helicase family protein
			transl_table	11
20252	18984	CDS
			locus_tag	Edafosvirus_4_19
			product	histidine phosphatase superfamily branch 1
			transl_table	11
20284	20907	CDS
			locus_tag	Edafosvirus_4_20
			product	hypothetical protein
			transl_table	11
21529	20933	CDS
			locus_tag	Edafosvirus_4_21
			product	hypothetical protein
			transl_table	11
21988	21614	CDS
			locus_tag	Edafosvirus_4_22
			product	hypothetical protein
			transl_table	11
23009	22059	CDS
			locus_tag	Edafosvirus_4_23
			product	hypothetical protein
			transl_table	11
24115	23069	CDS
			locus_tag	Edafosvirus_4_24
			product	hypothetical protein
			transl_table	11
24245	24862	CDS
			locus_tag	Edafosvirus_4_25
			product	P-loop containing nucleoside triphosphate hydrolase
			transl_table	11
26067	24859	CDS
			locus_tag	Edafosvirus_4_26
			product	hypothetical protein
			transl_table	11
27393	26137	CDS
			locus_tag	Edafosvirus_4_27
			product	glycosyltransferase
			transl_table	11
27530	28237	CDS
			locus_tag	Edafosvirus_4_28
			product	hypothetical protein
			transl_table	11
28451	28332	CDS
			locus_tag	Edafosvirus_4_29
			product	hypothetical protein
			transl_table	11
28595	28807	CDS
			locus_tag	Edafosvirus_4_30
			product	hypothetical protein
			transl_table	11
29107	28823	CDS
			locus_tag	Edafosvirus_4_31
			product	hypothetical protein
			transl_table	11
29833	29159	CDS
			locus_tag	Edafosvirus_4_32
			product	hypothetical protein pdul_cds_452
			transl_table	11
30599	29907	CDS
			locus_tag	Edafosvirus_4_33
			product	hypothetical protein
			transl_table	11
31168	30650	CDS
			locus_tag	Edafosvirus_4_34
			product	protein-ADP-ribose hydrolase
			transl_table	11
31895	31197	CDS
			locus_tag	Edafosvirus_4_35
			product	hypothetical protein
			transl_table	11
32052	34334	CDS
			locus_tag	Edafosvirus_4_36
			product	hypothetical protein
			transl_table	11
35124	34348	CDS
			locus_tag	Edafosvirus_4_37
			product	hypothetical protein
			transl_table	11
35288	35992	CDS
			locus_tag	Edafosvirus_4_38
			product	hypothetical protein
			transl_table	11
36678	35995	CDS
			locus_tag	Edafosvirus_4_39
			product	hypothetical protein
			transl_table	11
37330	36719	CDS
			locus_tag	Edafosvirus_4_40
			product	hypothetical protein
			transl_table	11
37597	37397	CDS
			locus_tag	Edafosvirus_4_41
			product	hypothetical protein
			transl_table	11
39195	37627	CDS
			locus_tag	Edafosvirus_4_42
			product	hypothetical protein
			transl_table	11
40095	39244	CDS
			locus_tag	Edafosvirus_4_43
			product	hypothetical protein
			transl_table	11
40155	40283	CDS
			locus_tag	Edafosvirus_4_44
			product	hypothetical protein
			transl_table	11
40635	40507	CDS
			locus_tag	Edafosvirus_4_45
			product	hypothetical protein
			transl_table	11
41830	43965	CDS
			locus_tag	Edafosvirus_4_46
			product	N-ethylmaleimide-sensitive fusion protein
			transl_table	11
44377	44006	CDS
			locus_tag	Edafosvirus_4_47
			product	hypothetical protein
			transl_table	11
44482	44934	CDS
			locus_tag	Edafosvirus_4_48
			product	hypothetical protein
			transl_table	11
45420	44995	CDS
			locus_tag	Edafosvirus_4_49
			product	hypothetical protein
			transl_table	11
46635	45547	CDS
			locus_tag	Edafosvirus_4_50
			product	hypothetical protein
			transl_table	11
47376	46747	CDS
			locus_tag	Edafosvirus_4_51
			product	putative orfan
			transl_table	11
47536	48597	CDS
			locus_tag	Edafosvirus_4_52
			product	hypothetical protein
			transl_table	11
49190	48594	CDS
			locus_tag	Edafosvirus_4_53
			product	hypothetical protein
			transl_table	11
49297	49953	CDS
			locus_tag	Edafosvirus_4_54
			product	hypothetical protein
			transl_table	11
50030	50683	CDS
			locus_tag	Edafosvirus_4_55
			product	hypothetical protein
			transl_table	11
51375	52823	CDS
			locus_tag	Edafosvirus_4_56
			product	prolyl-tRNA synthetase
			transl_table	11
54177	52825	CDS
			locus_tag	Edafosvirus_4_57
			product	chromosome condensation regulator
			transl_table	11
54399	55604	CDS
			locus_tag	Edafosvirus_4_58
			product	hypothetical protein
			transl_table	11
55820	56089	CDS
			locus_tag	Edafosvirus_4_59
			product	hypothetical protein
			transl_table	11
58933	56354	CDS
			locus_tag	Edafosvirus_4_60
			product	hypothetical protein
			transl_table	11
59292	58975	CDS
			locus_tag	Edafosvirus_4_61
			product	hypothetical protein
			transl_table	11
60649	59345	CDS
			locus_tag	Edafosvirus_4_62
			product	hect e3 ubiquitin
			transl_table	11
62039	60744	CDS
			locus_tag	Edafosvirus_4_63
			product	RCC1 and BTB domaincontaining protein putative
			transl_table	11
63465	62131	CDS
			locus_tag	Edafosvirus_4_64
			product	hypothetical protein A3K10_14420
			transl_table	11
64014	63574	CDS
			locus_tag	Edafosvirus_4_65
			product	hypothetical protein
			transl_table	11
65645	64320	CDS
			locus_tag	Edafosvirus_4_66
			product	chromosome condensation regulator, partial
			transl_table	11
>Edafosvirus_5
3	302	CDS
			locus_tag	Edafosvirus_5_1
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
471	1085	CDS
			locus_tag	Edafosvirus_5_2
			product	hypothetical protein
			transl_table	11
2009	1104	CDS
			locus_tag	Edafosvirus_5_3
			product	hypothetical protein
			transl_table	11
2171	3226	CDS
			locus_tag	Edafosvirus_5_4
			product	hypothetical protein
			transl_table	11
3327	3962	CDS
			locus_tag	Edafosvirus_5_5
			product	hypothetical protein
			transl_table	11
4053	4679	CDS
			locus_tag	Edafosvirus_5_6
			product	hypothetical protein
			transl_table	11
5139	4693	CDS
			locus_tag	Edafosvirus_5_7
			product	hypothetical protein
			transl_table	11
5708	5190	CDS
			locus_tag	Edafosvirus_5_8
			product	hypothetical protein
			transl_table	11
6869	5850	CDS
			locus_tag	Edafosvirus_5_9
			product	hypothetical protein
			transl_table	11
7129	6908	CDS
			locus_tag	Edafosvirus_5_10
			product	hypothetical protein
			transl_table	11
7556	7786	CDS
			locus_tag	Edafosvirus_5_11
			product	hypothetical protein
			transl_table	11
8365	7781	CDS
			locus_tag	Edafosvirus_5_12
			product	hypothetical protein
			transl_table	11
8596	9273	CDS
			locus_tag	Edafosvirus_5_13
			product	hypothetical protein
			transl_table	11
11012	9282	CDS
			locus_tag	Edafosvirus_5_14
			product	hypothetical protein
			transl_table	11
11137	13716	CDS
			locus_tag	Edafosvirus_5_15
			product	ankyrin repeat-containing protein
			transl_table	11
13872	14195	CDS
			locus_tag	Edafosvirus_5_16
			product	hypothetical protein
			transl_table	11
14399	14202	CDS
			locus_tag	Edafosvirus_5_17
			product	hypothetical protein
			transl_table	11
14614	14811	CDS
			locus_tag	Edafosvirus_5_18
			product	hypothetical protein
			transl_table	11
15977	14793	CDS
			locus_tag	Edafosvirus_5_19
			product	amino oxidase
			transl_table	11
16570	16001	CDS
			locus_tag	Edafosvirus_5_20
			product	hypothetical protein
			transl_table	11
16950	16639	CDS
			locus_tag	Edafosvirus_5_21
			product	hypothetical protein
			transl_table	11
17447	17028	CDS
			locus_tag	Edafosvirus_5_22
			product	hypothetical protein
			transl_table	11
18521	17898	CDS
			locus_tag	Edafosvirus_5_23
			product	Rab1
			transl_table	11
18602	19714	CDS
			locus_tag	Edafosvirus_5_24
			product	patatin
			transl_table	11
20617	20036	CDS
			locus_tag	Edafosvirus_5_25
			product	RING domain protein
			transl_table	11
20695	21405	CDS
			locus_tag	Edafosvirus_5_26
			product	hypothetical protein AD998_04925
			transl_table	11
21662	21408	CDS
			locus_tag	Edafosvirus_5_27
			product	hypothetical protein
			transl_table	11
22172	21762	CDS
			locus_tag	Edafosvirus_5_28
			product	mannose-6-phosphate isomerase
			transl_table	11
22393	22229	CDS
			locus_tag	Edafosvirus_5_29
			product	hypothetical protein
			transl_table	11
23793	23488	CDS
			locus_tag	Edafosvirus_5_30
			product	hypothetical protein
			transl_table	11
25613	25801	CDS
			locus_tag	Edafosvirus_5_31
			product	hypothetical protein
			transl_table	11
26091	25798	CDS
			locus_tag	Edafosvirus_5_32
			product	hypothetical protein
			transl_table	11
26207	26509	CDS
			locus_tag	Edafosvirus_5_33
			product	hypothetical protein
			transl_table	11
26982	26506	CDS
			locus_tag	Edafosvirus_5_34
			product	hypothetical protein
			transl_table	11
27095	27808	CDS
			locus_tag	Edafosvirus_5_35
			product	hypothetical protein
			transl_table	11
28005	28394	CDS
			locus_tag	Edafosvirus_5_36
			product	hypothetical protein
			transl_table	11
28867	28391	CDS
			locus_tag	Edafosvirus_5_37
			product	hypothetical protein
			transl_table	11
29018	30070	CDS
			locus_tag	Edafosvirus_5_38
			product	Cys/Met metabolism, pyridoxal phosphate-dependent enzyme
			transl_table	11
30342	30067	CDS
			locus_tag	Edafosvirus_5_39
			product	hypothetical protein
			transl_table	11
30827	30411	CDS
			locus_tag	Edafosvirus_5_40
			product	hypothetical protein
			transl_table	11
30971	31486	CDS
			locus_tag	Edafosvirus_5_41
			product	hypothetical protein ACA1_037450
			transl_table	11
32275	31472	CDS
			locus_tag	Edafosvirus_5_42
			product	hypothetical protein
			transl_table	11
34551	32362	CDS
			locus_tag	Edafosvirus_5_43
			product	hypothetical protein
			transl_table	11
35825	34971	CDS
			locus_tag	Edafosvirus_5_44
			product	ligase 2
			transl_table	11
36020	36349	CDS
			locus_tag	Edafosvirus_5_45
			product	hypothetical protein
			transl_table	11
36930	36352	CDS
			locus_tag	Edafosvirus_5_46
			product	hypothetical protein COT80_02035
			transl_table	11
37800	36991	CDS
			locus_tag	Edafosvirus_5_47
			product	Hypothetical protein FSTVST1_107
			transl_table	11
38526	37864	CDS
			locus_tag	Edafosvirus_5_48
			product	hypothetical protein
			transl_table	11
38666	39190	CDS
			locus_tag	Edafosvirus_5_49
			product	NADPH-dependent oxidoreductase
			transl_table	11
39731	39273	CDS
			locus_tag	Edafosvirus_5_50
			product	predicted protein
			transl_table	11
40938	39826	CDS
			locus_tag	Edafosvirus_5_51
			product	hypothetical protein
			transl_table	11
41495	41001	CDS
			locus_tag	Edafosvirus_5_52
			product	hypothetical protein
			transl_table	11
42284	41625	CDS
			locus_tag	Edafosvirus_5_53
			product	hypothetical protein
			transl_table	11
42999	42349	CDS
			locus_tag	Edafosvirus_5_54
			product	hypothetical protein
			transl_table	11
43057	43182	CDS
			locus_tag	Edafosvirus_5_55
			product	hypothetical protein
			transl_table	11
44012	43731	CDS
			locus_tag	Edafosvirus_5_56
			product	hypothetical protein
			transl_table	11
44087	45031	CDS
			locus_tag	Edafosvirus_5_57
			product	polyketide cyclase
			transl_table	11
45815	45051	CDS
			locus_tag	Edafosvirus_5_58
			product	hypothetical protein
			transl_table	11
45984	47060	CDS
			locus_tag	Edafosvirus_5_59
			product	GIY-YIG nuclease
			transl_table	11
47173	47057	CDS
			locus_tag	Edafosvirus_5_60
			product	hypothetical protein
			transl_table	11
47763	47632	CDS
			locus_tag	Edafosvirus_5_61
			product	hypothetical protein
			transl_table	11
47942	48862	CDS
			locus_tag	Edafosvirus_5_62
			product	hypothetical protein
			transl_table	11
50110	48938	CDS
			locus_tag	Edafosvirus_5_63
			product	hypothetical protein
			transl_table	11
50246	51343	CDS
			locus_tag	Edafosvirus_5_64
			product	hypothetical protein
			transl_table	11
51397	52536	CDS
			locus_tag	Edafosvirus_5_65
			product	hypothetical protein
			transl_table	11
54400	52565	CDS
			locus_tag	Edafosvirus_5_66
			product	hypothetical protein Catovirus_1_860
			transl_table	11
55236	54505	CDS
			locus_tag	Edafosvirus_5_67
			product	hypothetical protein
			transl_table	11
56151	55327	CDS
			locus_tag	Edafosvirus_5_68
			product	hypothetical protein
			transl_table	11
56791	56315	CDS
			locus_tag	Edafosvirus_5_69
			product	hypothetical protein
			transl_table	11
>Edafosvirus_6
3	860	CDS
			locus_tag	Edafosvirus_6_1
			product	putative uncharacterized protein
			transl_table	11
1421	867	CDS
			locus_tag	Edafosvirus_6_2
			product	hypothetical protein
			transl_table	11
2142	1447	CDS
			locus_tag	Edafosvirus_6_3
			product	hypothetical protein
			transl_table	11
3021	2212	CDS
			locus_tag	Edafosvirus_6_4
			product	hypothetical protein
			transl_table	11
3110	3967	CDS
			locus_tag	Edafosvirus_6_5
			product	PREDICTED: mRNA cap guanine-N7 methyltransferase isoform X1
			transl_table	11
4119	4457	CDS
			locus_tag	Edafosvirus_6_6
			product	hypothetical protein
			transl_table	11
5421	4447	CDS
			locus_tag	Edafosvirus_6_7
			product	hypothetical protein Hokovirus_1_173
			transl_table	11
5496	6491	CDS
			locus_tag	Edafosvirus_6_8
			product	UDP-glucose 4-epimerase GalE
			transl_table	11
7154	6480	CDS
			locus_tag	Edafosvirus_6_9
			product	metal dependent phosphohydrolase
			transl_table	11
7627	7211	CDS
			locus_tag	Edafosvirus_6_10
			product	hypothetical protein
			transl_table	11
8333	7683	CDS
			locus_tag	Edafosvirus_6_11
			product	hypothetical protein
			transl_table	11
9214	8393	CDS
			locus_tag	Edafosvirus_6_12
			product	hypothetical protein
			transl_table	11
9656	9258	CDS
			locus_tag	Edafosvirus_6_13
			product	hypothetical protein
			transl_table	11
9736	10326	CDS
			locus_tag	Edafosvirus_6_14
			product	hypothetical protein
			transl_table	11
11114	10323	CDS
			locus_tag	Edafosvirus_6_15
			product	hypothetical protein Catovirus_1_1044
			transl_table	11
11159	11587	CDS
			locus_tag	Edafosvirus_6_16
			product	hypothetical protein
			transl_table	11
11622	11948	CDS
			locus_tag	Edafosvirus_6_17
			product	hypothetical protein
			transl_table	11
11935	12240	CDS
			locus_tag	Edafosvirus_6_18
			product	hypothetical protein
			transl_table	11
14517	12229	CDS
			locus_tag	Edafosvirus_6_19
			product	Hypothetical Protein FCC1311_031102
			transl_table	11
14563	14751	CDS
			locus_tag	Edafosvirus_6_20
			product	hypothetical protein
			transl_table	11
14776	14952	CDS
			locus_tag	Edafosvirus_6_21
			product	hypothetical protein
			transl_table	11
15437	15006	CDS
			locus_tag	Edafosvirus_6_22
			product	hypothetical protein
			transl_table	11
16312	15479	CDS
			locus_tag	Edafosvirus_6_23
			product	hypothetical protein
			transl_table	11
16807	16505	CDS
			locus_tag	Edafosvirus_6_24
			product	hypothetical protein
			transl_table	11
17531	16830	CDS
			locus_tag	Edafosvirus_6_25
			product	Protein phosphatase Slingshot, partial
			transl_table	11
18595	17897	CDS
			locus_tag	Edafosvirus_6_26
			product	hypothetical protein
			transl_table	11
18935	18684	CDS
			locus_tag	Edafosvirus_6_27
			product	hypothetical protein
			transl_table	11
19293	19012	CDS
			locus_tag	Edafosvirus_6_28
			product	hypothetical protein
			transl_table	11
19686	19378	CDS
			locus_tag	Edafosvirus_6_29
			product	hypothetical protein
			transl_table	11
20535	19726	CDS
			locus_tag	Edafosvirus_6_30
			product	hypothetical protein COW24_02445
			transl_table	11
20668	22767	CDS
			locus_tag	Edafosvirus_6_31
			product	valyl-tRNA synthetase
			transl_table	11
24711	22843	CDS
			locus_tag	Edafosvirus_6_32
			product	hypothetical protein
			transl_table	11
25385	24795	CDS
			locus_tag	Edafosvirus_6_33
			product	hypothetical protein
			transl_table	11
25546	26847	CDS
			locus_tag	Edafosvirus_6_34
			product	hypothetical protein
			transl_table	11
28065	26839	CDS
			locus_tag	Edafosvirus_6_35
			product	hypothetical protein
			transl_table	11
28874	28161	CDS
			locus_tag	Edafosvirus_6_36
			product	hypothetical protein
			transl_table	11
30116	28923	CDS
			locus_tag	Edafosvirus_6_37
			product	hypothetical protein
			transl_table	11
30971	30219	CDS
			locus_tag	Edafosvirus_6_38
			product	hypothetical protein
			transl_table	11
32407	31040	CDS
			locus_tag	Edafosvirus_6_39
			product	hypothetical protein
			transl_table	11
32380	32505	CDS
			locus_tag	Edafosvirus_6_40
			product	hypothetical protein
			transl_table	11
33893	32502	CDS
			locus_tag	Edafosvirus_6_41
			product	hypothetical protein
			transl_table	11
34845	33955	CDS
			locus_tag	Edafosvirus_6_42
			product	hypothetical protein
			transl_table	11
34963	36333	CDS
			locus_tag	Edafosvirus_6_43
			product	hypothetical protein
			transl_table	11
37098	36325	CDS
			locus_tag	Edafosvirus_6_44
			product	hypothetical protein
			transl_table	11
38323	37151	CDS
			locus_tag	Edafosvirus_6_45
			product	hypothetical protein
			transl_table	11
39646	38378	CDS
			locus_tag	Edafosvirus_6_46
			product	hypothetical protein
			transl_table	11
39801	40949	CDS
			locus_tag	Edafosvirus_6_47
			product	hypothetical protein
			transl_table	11
42241	41096	CDS
			locus_tag	Edafosvirus_6_48
			product	hypothetical protein
			transl_table	11
43256	42384	CDS
			locus_tag	Edafosvirus_6_49
			product	hypothetical protein
			transl_table	11
44498	43281	CDS
			locus_tag	Edafosvirus_6_50
			product	hypothetical protein
			transl_table	11
44766	45614	CDS
			locus_tag	Edafosvirus_6_51
			product	uncharacterized protein Dvir_GJ23890
			transl_table	11
46831	45659	CDS
			locus_tag	Edafosvirus_6_52
			product	hypothetical protein
			transl_table	11
46820	46921	CDS
			locus_tag	Edafosvirus_6_53
			product	hypothetical protein
			transl_table	11
48187	46913	CDS
			locus_tag	Edafosvirus_6_54
			product	hypothetical protein
			transl_table	11
49513	48278	CDS
			locus_tag	Edafosvirus_6_55
			product	hypothetical protein
			transl_table	11
50992	49679	CDS
			locus_tag	Edafosvirus_6_56
			product	hypothetical protein
			transl_table	11
51633	51905	CDS
			locus_tag	Edafosvirus_6_57
			product	hypothetical protein
			transl_table	11
>Edafosvirus_7
281	790	CDS
			locus_tag	Edafosvirus_7_1
			product	hypothetical protein
			transl_table	11
863	1699	CDS
			locus_tag	Edafosvirus_7_2
			product	hypothetical protein
			transl_table	11
2103	3224	CDS
			locus_tag	Edafosvirus_7_3
			product	thymidylate synthase
			transl_table	11
3573	3830	CDS
			locus_tag	Edafosvirus_7_4
			product	Chain A, Crystal Structure Of The Chts-dhfr F207a Non-active Site Mutant >3HJ3_B Chain B, Crystal Structure Of The Chts-dhfr F207a Non-active Site Mutant >3HJ3_C Chain C, Crystal Structure Of The Chts-dhfr F207a Non-active Site Mutant >3HJ3_D Chain D, Crystal Structure Of The Chts-dhfr F207a Non-active Site Mutant
			transl_table	11
3867	4742	CDS
			locus_tag	Edafosvirus_7_5
			product	hypothetical protein
			transl_table	11
4812	5681	CDS
			locus_tag	Edafosvirus_7_6
			product	eukaryotic translation initiation factor 4G
			transl_table	11
5843	6469	CDS
			locus_tag	Edafosvirus_7_7
			product	hypothetical protein
			transl_table	11
6535	7935	CDS
			locus_tag	Edafosvirus_7_8
			product	aspartyl-tRNA synthetase
			transl_table	11
9562	7916	CDS
			locus_tag	Edafosvirus_7_9
			product	GTPase
			transl_table	11
9991	11334	CDS
			locus_tag	Edafosvirus_7_10
			product	FLAP-like endonuclease XPG
			transl_table	11
11747	12649	CDS
			locus_tag	Edafosvirus_7_11
			product	hypothetical protein ACD_46C00039G0008
			transl_table	11
14087	12747	CDS
			locus_tag	Edafosvirus_7_12
			product	histidine tRNA synthetase
			transl_table	11
16077	14143	CDS
			locus_tag	Edafosvirus_7_13
			product	hypothetical protein AUG52_01025
			transl_table	11
16178	17530	CDS
			locus_tag	Edafosvirus_7_14
			product	DnaJ domain protein
			transl_table	11
18119	17553	CDS
			locus_tag	Edafosvirus_7_15
			product	hypothetical protein
			transl_table	11
18258	18599	CDS
			locus_tag	Edafosvirus_7_16
			product	protein of unknown function DUF814
			transl_table	11
19175	18561	CDS
			locus_tag	Edafosvirus_7_17
			product	deoxynucleoside monophosphate kinase
			transl_table	11
20539	19301	CDS
			locus_tag	Edafosvirus_7_18
			product	cyclopropane-fatty-acyl-phospholipid synthase
			transl_table	11
21421	20576	CDS
			locus_tag	Edafosvirus_7_19
			product	hypothetical protein
			transl_table	11
21567	21776	CDS
			locus_tag	Edafosvirus_7_20
			product	hypothetical protein
			transl_table	11
21718	23742	CDS
			locus_tag	Edafosvirus_7_21
			product	FtsJ-like methyltransferase
			transl_table	11
24625	23735	CDS
			locus_tag	Edafosvirus_7_22
			product	hypothetical protein
			transl_table	11
24782	25768	CDS
			locus_tag	Edafosvirus_7_23
			product	WD repeat protein
			transl_table	11
25842	26954	CDS
			locus_tag	Edafosvirus_7_24
			product	replication factor C small subunit
			transl_table	11
28488	26944	CDS
			locus_tag	Edafosvirus_7_25
			product	L-threonine dehydratase biosynthetic IlvA
			transl_table	11
28631	28813	CDS
			locus_tag	Edafosvirus_7_26
			product	hypothetical protein
			transl_table	11
28882	29691	CDS
			locus_tag	Edafosvirus_7_27
			product	alpha/beta hydrolase family protein
			transl_table	11
29712	29939	CDS
			locus_tag	Edafosvirus_7_28
			product	hypothetical protein
			transl_table	11
30115	30288	CDS
			locus_tag	Edafosvirus_7_29
			product	hypothetical protein
			transl_table	11
30849	30295	CDS
			locus_tag	Edafosvirus_7_30
			product	hypothetical protein
			transl_table	11
30993	31664	CDS
			locus_tag	Edafosvirus_7_31
			product	hypothetical protein Catovirus_2_155
			transl_table	11
32704	31631	CDS
			locus_tag	Edafosvirus_7_32
			product	hypothetical protein Klosneuvirus_3_258
			transl_table	11
32825	34093	CDS
			locus_tag	Edafosvirus_7_33
			product	N-myristoyltransferase
			transl_table	11
35028	34558	CDS
			locus_tag	Edafosvirus_7_34
			product	hypothetical protein Catovirus_2_152
			transl_table	11
35102	37882	CDS
			locus_tag	Edafosvirus_7_35
			product	Zn-dependent peptidase
			transl_table	11
38091	38753	CDS
			locus_tag	Edafosvirus_7_36
			product	hypothetical protein
			transl_table	11
38826	39251	CDS
			locus_tag	Edafosvirus_7_37
			product	hypothetical protein
			transl_table	11
41570	39327	CDS
			locus_tag	Edafosvirus_7_38
			product	protein of unknown function DUF4419
			transl_table	11
41707	41570	CDS
			locus_tag	Edafosvirus_7_39
			product	hypothetical protein
			transl_table	11
41697	43199	CDS
			locus_tag	Edafosvirus_7_40
			product	hypothetical protein
			transl_table	11
43257	43913	CDS
			locus_tag	Edafosvirus_7_41
			product	hypothetical protein Catovirus_2_148
			transl_table	11
44003	44356	CDS
			locus_tag	Edafosvirus_7_42
			product	hypothetical protein
			transl_table	11
>Edafosvirus_8
1	978	CDS
			locus_tag	Edafosvirus_8_1
			product	hypothetical protein
			transl_table	11
981	1175	CDS
			locus_tag	Edafosvirus_8_2
			product	hypothetical protein
			transl_table	11
1181	2149	CDS
			locus_tag	Edafosvirus_8_3
			product	putative dTDP-D-glucose 46-dehydratase
			transl_table	11
2214	3029	CDS
			locus_tag	Edafosvirus_8_4
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
3156	4448	CDS
			locus_tag	Edafosvirus_8_5
			product	hypothetical protein
			transl_table	11
5326	4445	CDS
			locus_tag	Edafosvirus_8_6
			product	packaging ATPase
			transl_table	11
5770	5393	CDS
			locus_tag	Edafosvirus_8_7
			product	hypothetical protein Catovirus_2_275
			transl_table	11
6636	5914	CDS
			locus_tag	Edafosvirus_8_8
			product	hypothetical protein
			transl_table	11
8178	6778	CDS
			locus_tag	Edafosvirus_8_9
			product	hypothetical protein Catovirus_2_274
			transl_table	11
8534	9547	CDS
			locus_tag	Edafosvirus_8_10
			product	serine recombinase
			transl_table	11
9588	9905	CDS
			locus_tag	Edafosvirus_8_11
			product	hypothetical protein
			transl_table	11
10542	9907	CDS
			locus_tag	Edafosvirus_8_12
			product	hypothetical protein Indivirus_4_35
			transl_table	11
12236	10656	CDS
			locus_tag	Edafosvirus_8_13
			product	hypothetical protein B6I17_03770
			transl_table	11
13464	12319	CDS
			locus_tag	Edafosvirus_8_14
			product	hypothetical protein Catovirus_2_272
			transl_table	11
14041	13502	CDS
			locus_tag	Edafosvirus_8_15
			product	hypothetical protein Klosneuvirus_1_13
			transl_table	11
14123	15046	CDS
			locus_tag	Edafosvirus_8_16
			product	hypothetical protein Klosneuvirus_1_12
			transl_table	11
15131	16708	CDS
			locus_tag	Edafosvirus_8_17
			product	replication factor C large subunit
			transl_table	11
16831	18603	CDS
			locus_tag	Edafosvirus_8_18
			product	P4B major core protein
			transl_table	11
19222	18680	CDS
			locus_tag	Edafosvirus_8_19
			product	hypothetical protein Catovirus_2_265
			transl_table	11
19849	19364	CDS
			locus_tag	Edafosvirus_8_20
			product	hypothetical protein
			transl_table	11
22033	19988	CDS
			locus_tag	Edafosvirus_8_21
			product	hypothetical protein Catovirus_2_262
			transl_table	11
24864	22249	CDS
			locus_tag	Edafosvirus_8_22
			product	divergent protein kinase
			transl_table	11
26399	24972	CDS
			locus_tag	Edafosvirus_8_23
			product	serine/threonine protein kinase
			transl_table	11
28244	26475	CDS
			locus_tag	Edafosvirus_8_24
			product	XRN 5'-3' exonuclease
			transl_table	11
28377	29159	CDS
			locus_tag	Edafosvirus_8_25
			product	hypothetical protein
			transl_table	11
29421	29218	CDS
			locus_tag	Edafosvirus_8_26
			product	hypothetical protein
			transl_table	11
29699	29472	CDS
			locus_tag	Edafosvirus_8_27
			product	hypothetical protein
			transl_table	11
29972	29763	CDS
			locus_tag	Edafosvirus_8_28
			product	hypothetical protein
			transl_table	11
31148	30054	CDS
			locus_tag	Edafosvirus_8_29
			product	metallophosphatase/phosphoesterase
			transl_table	11
31342	31166	CDS
			locus_tag	Edafosvirus_8_30
			product	hypothetical protein
			transl_table	11
32731	32096	CDS
			locus_tag	Edafosvirus_8_31
			product	hypothetical protein
			transl_table	11
33454	32765	CDS
			locus_tag	Edafosvirus_8_32
			product	hypothetical protein Klosneuvirus_9_8
			transl_table	11
33572	36136	CDS
			locus_tag	Edafosvirus_8_33
			product	superfamily II DNA or RNA helicase
			transl_table	11
36428	36147	CDS
			locus_tag	Edafosvirus_8_34
			product	cytosine-specific methyltransferase
			transl_table	11
36619	37704	CDS
			locus_tag	Edafosvirus_8_35
			product	replication factor C small subunit
			transl_table	11
37900	37697	CDS
			locus_tag	Edafosvirus_8_36
			product	hypothetical protein
			transl_table	11
>Edafosvirus_9
1	3204	CDS
			locus_tag	Edafosvirus_9_1
			product	DEAD/SNF2-like helicase
			transl_table	11
3213	5057	CDS
			locus_tag	Edafosvirus_9_2
			product	hypothetical protein Klosneuvirus_1_286
			transl_table	11
5718	5044	CDS
			locus_tag	Edafosvirus_9_3
			product	hypothetical protein
			transl_table	11
5791	6225	CDS
			locus_tag	Edafosvirus_9_4
			product	hypothetical protein Catovirus_1_665
			transl_table	11
6282	6755	CDS
			locus_tag	Edafosvirus_9_5
			product	hypothetical protein
			transl_table	11
6805	7368	CDS
			locus_tag	Edafosvirus_9_6
			product	hypothetical protein
			transl_table	11
8141	7365	CDS
			locus_tag	Edafosvirus_9_7
			product	hypothetical protein
			transl_table	11
9755	8316	CDS
			locus_tag	Edafosvirus_9_8
			product	nicotinate phosphoribosyltransferase
			transl_table	11
10706	11629	CDS
			locus_tag	Edafosvirus_9_9
			product	ribose-phosphate pyrophosphokinase
			transl_table	11
13537	11663	CDS
			locus_tag	Edafosvirus_9_10
			product	hypothetical protein Klosneuvirus_5_93
			transl_table	11
13705	16149	CDS
			locus_tag	Edafosvirus_9_11
			product	hypothetical protein Klosneuvirus_3_140
			transl_table	11
16228	17247	CDS
			locus_tag	Edafosvirus_9_12
			product	hypothetical protein Klosneuvirus_3_244
			transl_table	11
18221	17265	CDS
			locus_tag	Edafosvirus_9_13
			product	putative glycosyltransferase
			transl_table	11
18256	18462	CDS
			locus_tag	Edafosvirus_9_14
			product	hypothetical protein
			transl_table	11
19732	19595	CDS
			locus_tag	Edafosvirus_9_15
			product	hypothetical protein
			transl_table	11
20006	19869	CDS
			locus_tag	Edafosvirus_9_16
			product	hypothetical protein
			transl_table	11
20143	20006	CDS
			locus_tag	Edafosvirus_9_17
			product	hypothetical protein
			transl_table	11
20522	20385	CDS
			locus_tag	Edafosvirus_9_18
			product	hypothetical protein
			transl_table	11
21070	20933	CDS
			locus_tag	Edafosvirus_9_19
			product	hypothetical protein
			transl_table	11
21207	21070	CDS
			locus_tag	Edafosvirus_9_20
			product	hypothetical protein
			transl_table	11
21422	22480	CDS
			locus_tag	Edafosvirus_9_21
			product	hypothetical protein Klosneuvirus_3_311
			transl_table	11
23404	22487	CDS
			locus_tag	Edafosvirus_9_22
			product	hypothetical protein
			transl_table	11
23377	23493	CDS
			locus_tag	Edafosvirus_9_23
			product	hypothetical protein
			transl_table	11
24058	23465	CDS
			locus_tag	Edafosvirus_9_24
			product	hemerythrin
			transl_table	11
24579	24064	CDS
			locus_tag	Edafosvirus_9_25
			product	hypothetical protein
			transl_table	11
24800	25375	CDS
			locus_tag	Edafosvirus_9_26
			product	hypothetical protein
			transl_table	11
26034	25372	CDS
			locus_tag	Edafosvirus_9_27
			product	hypothetical protein
			transl_table	11
26861	26085	CDS
			locus_tag	Edafosvirus_9_28
			product	hypothetical protein PPL_02155
			transl_table	11
27415	26897	CDS
			locus_tag	Edafosvirus_9_29
			product	hypothetical protein
			transl_table	11
27678	27971	CDS
			locus_tag	Edafosvirus_9_30
			product	hypothetical protein
			transl_table	11
28249	28608	CDS
			locus_tag	Edafosvirus_9_31
			product	hypothetical protein
			transl_table	11
28879	28619	CDS
			locus_tag	Edafosvirus_9_32
			product	hypothetical protein
			transl_table	11
29534	29004	CDS
			locus_tag	Edafosvirus_9_33
			product	hypothetical protein
			transl_table	11
29599	31944	CDS
			locus_tag	Edafosvirus_9_34
			product	hypothetical protein Catovirus_1_69
			transl_table	11
32112	32858	CDS
			locus_tag	Edafosvirus_9_35
			product	hypothetical protein
			transl_table	11
32946	33962	CDS
			locus_tag	Edafosvirus_9_36
			product	hypothetical protein A3F72_02525
			transl_table	11
33993	34742	CDS
			locus_tag	Edafosvirus_9_37
			product	mg19 protein
			transl_table	11
34951	36468	CDS
			locus_tag	Edafosvirus_9_38
			product	transposase
			transl_table	11
37253	36993	CDS
			locus_tag	Edafosvirus_9_39
			product	macrocin o-methyltransferase
			transl_table	11
>Edafosvirus_10
26788	26709	tRNA	Edafosvirus_tRNA_1
			product	tRNA-Ser(ACT)
26877	26794	tRNA	Edafosvirus_tRNA_2
			product	tRNA-Ser(AGA)
26963	26882	tRNA	Edafosvirus_tRNA_3
			product	tRNA-Ser(CGA)
973	1944	CDS
			locus_tag	Edafosvirus_10_1
			product	hypothetical protein Klosneuvirus_1_132
			transl_table	11
2726	1950	CDS
			locus_tag	Edafosvirus_10_2
			product	peptidase C1
			transl_table	11
3729	2935	CDS
			locus_tag	Edafosvirus_10_3
			product	hypothetical protein Klosneuvirus_1_405
			transl_table	11
3842	4144	CDS
			locus_tag	Edafosvirus_10_4
			product	hypothetical protein
			transl_table	11
4387	4758	CDS
			locus_tag	Edafosvirus_10_5
			product	hypothetical protein
			transl_table	11
5168	4764	CDS
			locus_tag	Edafosvirus_10_6
			product	hypothetical protein
			transl_table	11
5639	5181	CDS
			locus_tag	Edafosvirus_10_7
			product	dCMP deaminase
			transl_table	11
5955	5689	CDS
			locus_tag	Edafosvirus_10_8
			product	hypothetical protein Klosneuvirus_1_391
			transl_table	11
6083	5952	CDS
			locus_tag	Edafosvirus_10_9
			product	hypothetical protein
			transl_table	11
6119	6655	CDS
			locus_tag	Edafosvirus_10_10
			product	phosphoribosyl-ATP pyrophosphohydrolase
			transl_table	11
6961	6638	CDS
			locus_tag	Edafosvirus_10_11
			product	hypothetical protein
			transl_table	11
7046	8425	CDS
			locus_tag	Edafosvirus_10_12
			product	DEAD/SNF2-like helicase
			transl_table	11
8470	8625	CDS
			locus_tag	Edafosvirus_10_13
			product	hypothetical protein
			transl_table	11
9338	9484	CDS
			locus_tag	Edafosvirus_10_14
			product	hypothetical protein Klosneuvirus_2_125
			transl_table	11
9528	10016	CDS
			locus_tag	Edafosvirus_10_15
			product	hypothetical protein Klosneuvirus_1_379
			transl_table	11
10071	10763	CDS
			locus_tag	Edafosvirus_10_16
			product	hypothetical protein BMW23_0249
			transl_table	11
11107	10760	CDS
			locus_tag	Edafosvirus_10_17
			product	hypothetical protein Indivirus_1_201
			transl_table	11
11708	11154	CDS
			locus_tag	Edafosvirus_10_18
			product	hypothetical protein Indivirus_1_196
			transl_table	11
13818	11779	CDS
			locus_tag	Edafosvirus_10_19
			product	S9 family peptidase
			transl_table	11
13949	14863	CDS
			locus_tag	Edafosvirus_10_20
			product	hypothetical protein
			transl_table	11
17027	14868	CDS
			locus_tag	Edafosvirus_10_21
			product	hypothetical protein
			transl_table	11
17132	18109	CDS
			locus_tag	Edafosvirus_10_22
			product	hypothetical protein Catovirus_1_898
			transl_table	11
19460	18090	CDS
			locus_tag	Edafosvirus_10_23
			product	alpha/beta hydrolase fold
			transl_table	11
20266	19574	CDS
			locus_tag	Edafosvirus_10_24
			product	hypothetical protein
			transl_table	11
20392	20544	CDS
			locus_tag	Edafosvirus_10_25
			product	hypothetical protein
			transl_table	11
21347	20997	CDS
			locus_tag	Edafosvirus_10_26
			product	hypothetical protein
			transl_table	11
21515	23086	CDS
			locus_tag	Edafosvirus_10_27
			product	uncharacterized protein
			transl_table	11
23152	24255	CDS
			locus_tag	Edafosvirus_10_28
			product	unnamed protein product
			transl_table	11
24953	24264	CDS
			locus_tag	Edafosvirus_10_29
			product	hypothetical protein
			transl_table	11
25063	25302	CDS
			locus_tag	Edafosvirus_10_30
			product	hypothetical protein
			transl_table	11
26184	25366	CDS
			locus_tag	Edafosvirus_10_31
			product	E3 ubiquitin-protein ligase RGLG1
			transl_table	11
26547	26254	CDS
			locus_tag	Edafosvirus_10_32
			product	hypothetical protein
			transl_table	11
27094	27558	CDS
			locus_tag	Edafosvirus_10_33
			product	hypothetical protein
			transl_table	11
29548	27548	CDS
			locus_tag	Edafosvirus_10_34
			product	hypothetical protein SDRG_01183
			transl_table	11
30112	29627	CDS
			locus_tag	Edafosvirus_10_35
			product	hypothetical protein
			transl_table	11
30883	30227	CDS
			locus_tag	Edafosvirus_10_36
			product	hypothetical protein
			transl_table	11
31710	30952	CDS
			locus_tag	Edafosvirus_10_37
			product	hypothetical protein Klosneuvirus_1_369
			transl_table	11
32602	34149	CDS
			locus_tag	Edafosvirus_10_38
			product	hypothetical protein
			transl_table	11
34622	34146	CDS
			locus_tag	Edafosvirus_10_39
			product	hypothetical protein
			transl_table	11
35081	34710	CDS
			locus_tag	Edafosvirus_10_40
			product	hypothetical protein
			transl_table	11
>Edafosvirus_11
345	106	CDS
			locus_tag	Edafosvirus_11_1
			product	hypothetical protein
			transl_table	11
1191	457	CDS
			locus_tag	Edafosvirus_11_2
			product	hypothetical protein
			transl_table	11
2736	1312	CDS
			locus_tag	Edafosvirus_11_3
			product	hypothetical protein
			transl_table	11
4017	2953	CDS
			locus_tag	Edafosvirus_11_4
			product	hypothetical protein
			transl_table	11
4691	4143	CDS
			locus_tag	Edafosvirus_11_5
			product	hypothetical protein
			transl_table	11
6970	5249	CDS
			locus_tag	Edafosvirus_11_6
			product	transposase
			transl_table	11
7522	6974	CDS
			locus_tag	Edafosvirus_11_7
			product	GIY-YIG nuclease
			transl_table	11
7851	9041	CDS
			locus_tag	Edafosvirus_11_8
			product	hypothetical protein
			transl_table	11
9990	9031	CDS
			locus_tag	Edafosvirus_11_9
			product	hypothetical protein
			transl_table	11
10523	9960	CDS
			locus_tag	Edafosvirus_11_10
			product	hypothetical protein
			transl_table	11
11081	10599	CDS
			locus_tag	Edafosvirus_11_11
			product	hypothetical protein
			transl_table	11
12121	11282	CDS
			locus_tag	Edafosvirus_11_12
			product	hypothetical protein
			transl_table	11
12228	13079	CDS
			locus_tag	Edafosvirus_11_13
			product	hypothetical protein
			transl_table	11
13254	13102	CDS
			locus_tag	Edafosvirus_11_14
			product	hypothetical protein
			transl_table	11
14378	13335	CDS
			locus_tag	Edafosvirus_11_15
			product	hypothetical protein SAMD00019534_000090, partial
			transl_table	11
14584	14456	CDS
			locus_tag	Edafosvirus_11_16
			product	hypothetical protein
			transl_table	11
14893	14738	CDS
			locus_tag	Edafosvirus_11_17
			product	hypothetical protein
			transl_table	11
15816	14944	CDS
			locus_tag	Edafosvirus_11_18
			product	hypothetical protein lpa_00292
			transl_table	11
16042	15797	CDS
			locus_tag	Edafosvirus_11_19
			product	hypothetical protein
			transl_table	11
16916	16113	CDS
			locus_tag	Edafosvirus_11_20
			product	hypothetical protein
			transl_table	11
17121	17705	CDS
			locus_tag	Edafosvirus_11_21
			product	serine/threonine/tyrosine-interacting-like protein 1
			transl_table	11
17831	18490	CDS
			locus_tag	Edafosvirus_11_22
			product	hypothetical protein
			transl_table	11
19067	18477	CDS
			locus_tag	Edafosvirus_11_23
			product	hypothetical protein
			transl_table	11
19850	20401	CDS
			locus_tag	Edafosvirus_11_24
			product	hypothetical protein
			transl_table	11
20446	22320	CDS
			locus_tag	Edafosvirus_11_25
			product	hypothetical protein
			transl_table	11
22402	23625	CDS
			locus_tag	Edafosvirus_11_26
			product	hypothetical protein DDB_G0279743
			transl_table	11
24083	23694	CDS
			locus_tag	Edafosvirus_11_27
			product	hypothetical protein
			transl_table	11
24197	24736	CDS
			locus_tag	Edafosvirus_11_28
			product	conserved hypothetical protein
			transl_table	11
25692	24742	CDS
			locus_tag	Edafosvirus_11_29
			product	hypothetical protein
			transl_table	11
25770	26555	CDS
			locus_tag	Edafosvirus_11_30
			product	alpha/beta hydrolase family protein
			transl_table	11
26953	26546	CDS
			locus_tag	Edafosvirus_11_31
			product	hypothetical protein
			transl_table	11
27109	28500	CDS
			locus_tag	Edafosvirus_11_32
			product	AGAP006730-PA-like protein
			transl_table	11
28556	29725	CDS
			locus_tag	Edafosvirus_11_33
			product	hypothetical protein
			transl_table	11
29788	31083	CDS
			locus_tag	Edafosvirus_11_34
			product	hypothetical protein
			transl_table	11
31759	31097	CDS
			locus_tag	Edafosvirus_11_35
			product	hypothetical protein
			transl_table	11
32572	32718	CDS
			locus_tag	Edafosvirus_11_36
			product	hypothetical protein
			transl_table	11
>Edafosvirus_12
3	398	CDS
			locus_tag	Edafosvirus_12_1
			product	hypothetical protein
			transl_table	11
468	842	CDS
			locus_tag	Edafosvirus_12_2
			product	hypothetical protein
			transl_table	11
996	1442	CDS
			locus_tag	Edafosvirus_12_3
			product	hypothetical protein
			transl_table	11
1833	1423	CDS
			locus_tag	Edafosvirus_12_4
			product	hypothetical protein Catovirus_1_927
			transl_table	11
1949	2494	CDS
			locus_tag	Edafosvirus_12_5
			product	hypothetical protein
			transl_table	11
3475	2498	CDS
			locus_tag	Edafosvirus_12_6
			product	hypothetical protein Catovirus_1_928
			transl_table	11
3596	4663	CDS
			locus_tag	Edafosvirus_12_7
			product	acetoin utilization deacetylase
			transl_table	11
4707	6137	CDS
			locus_tag	Edafosvirus_12_8
			product	hypothetical protein Indivirus_1_157
			transl_table	11
8276	6129	CDS
			locus_tag	Edafosvirus_12_9
			product	UvrD-family helicase
			transl_table	11
8524	8375	CDS
			locus_tag	Edafosvirus_12_10
			product	hypothetical protein
			transl_table	11
9520	8612	CDS
			locus_tag	Edafosvirus_12_11
			product	collagen triple helix repeat containing protein
			transl_table	11
9701	10774	CDS
			locus_tag	Edafosvirus_12_12
			product	Monooxygenase, FAD-binding domain-containing protein
			transl_table	11
11424	10804	CDS
			locus_tag	Edafosvirus_12_13
			product	PREDICTED: ras-related protein Rab-10-like
			transl_table	11
11871	11515	CDS
			locus_tag	Edafosvirus_12_14
			product	hypothetical protein
			transl_table	11
13706	11928	CDS
			locus_tag	Edafosvirus_12_15
			product	DEAD/SNF2-like helicase
			transl_table	11
14878	13817	CDS
			locus_tag	Edafosvirus_12_16
			product	hypothetical protein Catovirus_1_933
			transl_table	11
15056	14931	CDS
			locus_tag	Edafosvirus_12_17
			product	hypothetical protein
			transl_table	11
16341	15049	CDS
			locus_tag	Edafosvirus_12_18
			product	hypothetical protein
			transl_table	11
16825	16409	CDS
			locus_tag	Edafosvirus_12_19
			product	hypothetical protein
			transl_table	11
18106	16874	CDS
			locus_tag	Edafosvirus_12_20
			product	hypothetical protein BMW23_0153
			transl_table	11
19323	19439	CDS
			locus_tag	Edafosvirus_12_21
			product	hypothetical protein
			transl_table	11
21178	19436	CDS
			locus_tag	Edafosvirus_12_22
			product	SNF2-like helicase
			transl_table	11
22429	21245	CDS
			locus_tag	Edafosvirus_12_23
			product	hypothetical protein
			transl_table	11
24118	22505	CDS
			locus_tag	Edafosvirus_12_24
			product	glutamine--tRNA ligase-like isoform X1
			transl_table	11
24275	25099	CDS
			locus_tag	Edafosvirus_12_25
			product	hypothetical protein
			transl_table	11
26226	25108	CDS
			locus_tag	Edafosvirus_12_26
			product	hypothetical protein
			transl_table	11
26833	26294	CDS
			locus_tag	Edafosvirus_12_27
			product	hypothetical protein
			transl_table	11
27669	26905	CDS
			locus_tag	Edafosvirus_12_28
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
28103	27849	CDS
			locus_tag	Edafosvirus_12_29
			product	hypothetical protein
			transl_table	11
28740	28183	CDS
			locus_tag	Edafosvirus_12_30
			product	hypothetical protein Catovirus_1_948
			transl_table	11
29106	28771	CDS
			locus_tag	Edafosvirus_12_31
			product	hypothetical protein
			transl_table	11
30265	29141	CDS
			locus_tag	Edafosvirus_12_32
			product	metallopeptidase family M24
			transl_table	11
30779	30630	CDS
			locus_tag	Edafosvirus_12_33
			product	hypothetical protein
			transl_table	11
30949	31035	CDS
			locus_tag	Edafosvirus_12_34
			product	hypothetical protein
			transl_table	11
>Edafosvirus_13
1	624	CDS
			locus_tag	Edafosvirus_13_1
			product	serine/threonine protein kinase
			transl_table	11
1791	607	CDS
			locus_tag	Edafosvirus_13_2
			product	ribonucleoside diphosphate reductase beta subunit
			transl_table	11
2022	5618	CDS
			locus_tag	Edafosvirus_13_3
			product	ribonucleoside diphosphate reductase large subunit
			transl_table	11
5699	7789	CDS
			locus_tag	Edafosvirus_13_4
			product	ankyrin repeat protein
			transl_table	11
8849	7806	CDS
			locus_tag	Edafosvirus_13_5
			product	DNA polymerase family X protein
			transl_table	11
8977	10497	CDS
			locus_tag	Edafosvirus_13_6
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
11144	10494	CDS
			locus_tag	Edafosvirus_13_7
			product	putative RNA methylase
			transl_table	11
12264	11188	CDS
			locus_tag	Edafosvirus_13_8
			product	hypothetical protein
			transl_table	11
13283	12330	CDS
			locus_tag	Edafosvirus_13_9
			product	hypothetical protein
			transl_table	11
13772	13867	CDS
			locus_tag	Edafosvirus_13_10
			product	hypothetical protein
			transl_table	11
14101	18627	CDS
			locus_tag	Edafosvirus_13_11
			product	DNA polymerase family B elongation subunit
			transl_table	11
19395	18622	CDS
			locus_tag	Edafosvirus_13_12
			product	hypothetical protein
			transl_table	11
19522	20631	CDS
			locus_tag	Edafosvirus_13_13
			product	hypothetical protein
			transl_table	11
21550	20681	CDS
			locus_tag	Edafosvirus_13_14
			product	hypothetical protein
			transl_table	11
21648	22235	CDS
			locus_tag	Edafosvirus_13_15
			product	hypothetical protein Catovirus_2_225
			transl_table	11
22310	23542	CDS
			locus_tag	Edafosvirus_13_16
			product	hypothetical protein
			transl_table	11
24222	24362	CDS
			locus_tag	Edafosvirus_13_17
			product	hypothetical protein
			transl_table	11
26711	26896	CDS
			locus_tag	Edafosvirus_13_18
			product	early transcription factor VETF large subunit
			transl_table	11
>Edafosvirus_14
4353	4282	tRNA	Edafosvirus_tRNA_4
			product	tRNA-Tyr(GTA)
4441	4358	tRNA	Edafosvirus_tRNA_5
			product	tRNA-Pseudo(AAG)
260	3	CDS
			locus_tag	Edafosvirus_14_1
			product	hypothetical protein
			transl_table	11
635	3094	CDS
			locus_tag	Edafosvirus_14_2
			product	XRN 5'-3' exonuclease
			transl_table	11
3170	3427	CDS
			locus_tag	Edafosvirus_14_3
			product	hypothetical protein
			transl_table	11
3458	4255	CDS
			locus_tag	Edafosvirus_14_4
			product	uracil-DNA glycosylase
			transl_table	11
5035	4475	CDS
			locus_tag	Edafosvirus_14_5
			product	hypothetical protein
			transl_table	11
5134	5742	CDS
			locus_tag	Edafosvirus_14_6
			product	hypothetical protein Klosneuvirus_1_350
			transl_table	11
5783	6253	CDS
			locus_tag	Edafosvirus_14_7
			product	I cysteine dioxygenase
			transl_table	11
6624	7472	CDS
			locus_tag	Edafosvirus_14_8
			product	hypothetical protein
			transl_table	11
7528	8427	CDS
			locus_tag	Edafosvirus_14_9
			product	hypothetical protein
			transl_table	11
11357	8430	CDS
			locus_tag	Edafosvirus_14_10
			product	hypothetical protein HMPREF9333_00975
			transl_table	11
11466	11864	CDS
			locus_tag	Edafosvirus_14_11
			product	hypothetical protein Indivirus_1_171
			transl_table	11
12230	12958	CDS
			locus_tag	Edafosvirus_14_12
			product	hypothetical protein Klosneuvirus_3_180
			transl_table	11
13016	13888	CDS
			locus_tag	Edafosvirus_14_13
			product	uridine kinase
			transl_table	11
13939	14538	CDS
			locus_tag	Edafosvirus_14_14
			product	hypothetical protein
			transl_table	11
14635	15147	CDS
			locus_tag	Edafosvirus_14_15
			product	hypothetical protein
			transl_table	11
15160	15609	CDS
			locus_tag	Edafosvirus_14_16
			product	hypothetical protein
			transl_table	11
15670	16293	CDS
			locus_tag	Edafosvirus_14_17
			product	hypothetical protein
			transl_table	11
17714	16290	CDS
			locus_tag	Edafosvirus_14_18
			product	release factor h-coupled family protein
			transl_table	11
17863	18993	CDS
			locus_tag	Edafosvirus_14_19
			product	hypothetical protein
			transl_table	11
19148	20227	CDS
			locus_tag	Edafosvirus_14_20
			product	hypothetical protein
			transl_table	11
20455	21810	CDS
			locus_tag	Edafosvirus_14_21
			product	hypothetical protein A2053_03845
			transl_table	11
22615	21776	CDS
			locus_tag	Edafosvirus_14_22
			product	hypothetical protein Catovirus_1_919
			transl_table	11
22704	23780	CDS
			locus_tag	Edafosvirus_14_23
			product	hypothetical protein
			transl_table	11
23787	24800	CDS
			locus_tag	Edafosvirus_14_24
			product	hypothetical protein
			transl_table	11
24871	25647	CDS
			locus_tag	Edafosvirus_14_25
			product	hypothetical protein
			transl_table	11
25671	26225	CDS
			locus_tag	Edafosvirus_14_26
			product	hypothetical protein TRIADDRAFT_57559
			transl_table	11
>Edafosvirus_15
536	3	CDS
			locus_tag	Edafosvirus_15_1
			product	hypothetical protein
			transl_table	11
2740	608	CDS
			locus_tag	Edafosvirus_15_2
			product	phosphoinositide 3-kinase
			transl_table	11
2986	4293	CDS
			locus_tag	Edafosvirus_15_3
			product	hypothetical protein
			transl_table	11
4396	4295	CDS
			locus_tag	Edafosvirus_15_4
			product	hypothetical protein
			transl_table	11
4924	4430	CDS
			locus_tag	Edafosvirus_15_5
			product	alpha-ribazole phosphatase
			transl_table	11
5694	5197	CDS
			locus_tag	Edafosvirus_15_6
			product	hypothetical protein
			transl_table	11
5807	6499	CDS
			locus_tag	Edafosvirus_15_7
			product	hypothetical protein
			transl_table	11
6723	8360	CDS
			locus_tag	Edafosvirus_15_8
			product	putative AAA family ATPase
			transl_table	11
8666	8361	CDS
			locus_tag	Edafosvirus_15_9
			product	hypothetical protein
			transl_table	11
9917	8739	CDS
			locus_tag	Edafosvirus_15_10
			product	hypothetical protein
			transl_table	11
10055	11404	CDS
			locus_tag	Edafosvirus_15_11
			product	serine/threonine protein kinase
			transl_table	11
11470	12099	CDS
			locus_tag	Edafosvirus_15_12
			product	hypothetical protein
			transl_table	11
12239	13324	CDS
			locus_tag	Edafosvirus_15_13
			product	hypothetical protein
			transl_table	11
13435	15120	CDS
			locus_tag	Edafosvirus_15_14
			product	hypothetical protein
			transl_table	11
18090	15115	CDS
			locus_tag	Edafosvirus_15_15
			product	soluble P-type ATPase
			transl_table	11
18761	18183	CDS
			locus_tag	Edafosvirus_15_16
			product	hypothetical protein
			transl_table	11
18814	19620	CDS
			locus_tag	Edafosvirus_15_17
			product	hypothetical protein
			transl_table	11
19749	22274	CDS
			locus_tag	Edafosvirus_15_18
			product	hypothetical protein
			transl_table	11
22374	23807	CDS
			locus_tag	Edafosvirus_15_19
			product	hypothetical protein
			transl_table	11
23949	24068	CDS
			locus_tag	Edafosvirus_15_20
			product	hypothetical protein
			transl_table	11
24250	24429	CDS
			locus_tag	Edafosvirus_15_21
			product	hypothetical protein
			transl_table	11
24889	25572	CDS
			locus_tag	Edafosvirus_15_22
			product	hypothetical protein
			transl_table	11
>Edafosvirus_16
127	837	CDS
			locus_tag	Edafosvirus_16_1
			product	hypothetical protein
			transl_table	11
1544	1026	CDS
			locus_tag	Edafosvirus_16_2
			product	hypothetical protein
			transl_table	11
3282	1591	CDS
			locus_tag	Edafosvirus_16_3
			product	hypothetical protein
			transl_table	11
3403	3960	CDS
			locus_tag	Edafosvirus_16_4
			product	globin, partial
			transl_table	11
4810	3962	CDS
			locus_tag	Edafosvirus_16_5
			product	hypothetical protein
			transl_table	11
4960	5589	CDS
			locus_tag	Edafosvirus_16_6
			product	hypothetical protein
			transl_table	11
5691	5990	CDS
			locus_tag	Edafosvirus_16_7
			product	hypothetical protein
			transl_table	11
8391	5995	CDS
			locus_tag	Edafosvirus_16_8
			product	hypothetical protein PTSG_01414
			transl_table	11
10695	8473	CDS
			locus_tag	Edafosvirus_16_9
			product	hypothetical protein PTSG_01414
			transl_table	11
13306	10763	CDS
			locus_tag	Edafosvirus_16_10
			product	protein of unknown function DUF4419
			transl_table	11
13421	14131	CDS
			locus_tag	Edafosvirus_16_11
			product	hypothetical protein
			transl_table	11
14385	15107	CDS
			locus_tag	Edafosvirus_16_12
			product	hypothetical protein
			transl_table	11
15218	15913	CDS
			locus_tag	Edafosvirus_16_13
			product	hypothetical protein
			transl_table	11
15984	16586	CDS
			locus_tag	Edafosvirus_16_14
			product	hypothetical protein
			transl_table	11
17367	16657	CDS
			locus_tag	Edafosvirus_16_15
			product	hypothetical protein
			transl_table	11
18030	17458	CDS
			locus_tag	Edafosvirus_16_16
			product	hypothetical protein
			transl_table	11
19827	18115	CDS
			locus_tag	Edafosvirus_16_17
			product	glycosyltransferase family 2 protein
			transl_table	11
21013	19991	CDS
			locus_tag	Edafosvirus_16_18
			product	mannose-1-phosphate guanylyltransferase/mannose-6-phosphate isomerase
			transl_table	11
21147	22193	CDS
			locus_tag	Edafosvirus_16_19
			product	uncharacterized conserved protein
			transl_table	11
22292	23149	CDS
			locus_tag	Edafosvirus_16_20
			product	hypothetical protein
			transl_table	11
23182	23844	CDS
			locus_tag	Edafosvirus_16_21
			product	fatty acid hydroxylase
			transl_table	11
24151	24462	CDS
			locus_tag	Edafosvirus_16_22
			product	hypothetical protein
			transl_table	11
25126	24890	CDS
			locus_tag	Edafosvirus_16_23
			product	hypothetical protein
			transl_table	11
>Edafosvirus_17
1634	684	CDS
			locus_tag	Edafosvirus_17_1
			product	hypothetical protein
			transl_table	11
1962	1681	CDS
			locus_tag	Edafosvirus_17_2
			product	hypothetical protein
			transl_table	11
2076	1981	CDS
			locus_tag	Edafosvirus_17_3
			product	hypothetical protein
			transl_table	11
2101	2787	CDS
			locus_tag	Edafosvirus_17_4
			product	phosphatases II
			transl_table	11
3111	2794	CDS
			locus_tag	Edafosvirus_17_5
			product	hypothetical protein
			transl_table	11
3832	3197	CDS
			locus_tag	Edafosvirus_17_6
			product	hypothetical protein
			transl_table	11
4386	3958	CDS
			locus_tag	Edafosvirus_17_7
			product	hypothetical protein
			transl_table	11
5534	4548	CDS
			locus_tag	Edafosvirus_17_8
			product	hypothetical protein
			transl_table	11
5714	6097	CDS
			locus_tag	Edafosvirus_17_9
			product	hypothetical protein
			transl_table	11
6104	6400	CDS
			locus_tag	Edafosvirus_17_10
			product	hypothetical protein
			transl_table	11
7442	7002	CDS
			locus_tag	Edafosvirus_17_11
			product	hypothetical protein
			transl_table	11
7581	9461	CDS
			locus_tag	Edafosvirus_17_12
			product	glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
9867	9472	CDS
			locus_tag	Edafosvirus_17_13
			product	hypothetical protein DFA_02833
			transl_table	11
10639	9962	CDS
			locus_tag	Edafosvirus_17_14
			product	hypothetical protein MIMI_L798
			transl_table	11
10784	11347	CDS
			locus_tag	Edafosvirus_17_15
			product	hypothetical protein
			transl_table	11
12418	11351	CDS
			locus_tag	Edafosvirus_17_16
			product	hypothetical protein
			transl_table	11
12560	12874	CDS
			locus_tag	Edafosvirus_17_17
			product	branched-chain alpha-keto acid dehydrogenase subunit E2
			transl_table	11
14029	12878	CDS
			locus_tag	Edafosvirus_17_18
			product	hypothetical protein
			transl_table	11
14594	14115	CDS
			locus_tag	Edafosvirus_17_19
			product	hypothetical protein
			transl_table	11
15297	14908	CDS
			locus_tag	Edafosvirus_17_20
			product	hypothetical protein
			transl_table	11
16896	15697	CDS
			locus_tag	Edafosvirus_17_21
			product	hypothetical protein
			transl_table	11
17426	17118	CDS
			locus_tag	Edafosvirus_17_22
			product	hypothetical protein
			transl_table	11
18089	18952	CDS
			locus_tag	Edafosvirus_17_23
			product	hypothetical protein
			transl_table	11
19030	19230	CDS
			locus_tag	Edafosvirus_17_24
			product	hypothetical protein
			transl_table	11
19256	19567	CDS
			locus_tag	Edafosvirus_17_25
			product	hypothetical protein
			transl_table	11
23477	19632	CDS
			locus_tag	Edafosvirus_17_26
			product	hypothetical protein
			transl_table	11
23638	24501	CDS
			locus_tag	Edafosvirus_17_27
			product	hypothetical protein
			transl_table	11
>Edafosvirus_18
1691	3	CDS
			locus_tag	Edafosvirus_18_1
			product	DEAD/SNF2-like helicase
			transl_table	11
1767	2480	CDS
			locus_tag	Edafosvirus_18_2
			product	hypothetical protein
			transl_table	11
3136	2489	CDS
			locus_tag	Edafosvirus_18_3
			product	hypothetical protein
			transl_table	11
3277	4029	CDS
			locus_tag	Edafosvirus_18_4
			product	hypothetical protein
			transl_table	11
4457	4026	CDS
			locus_tag	Edafosvirus_18_5
			product	PREDICTED: thioredoxin
			transl_table	11
4625	5167	CDS
			locus_tag	Edafosvirus_18_6
			product	hypothetical protein
			transl_table	11
5813	5175	CDS
			locus_tag	Edafosvirus_18_7
			product	hypothetical protein A9Q86_11095
			transl_table	11
6619	6221	CDS
			locus_tag	Edafosvirus_18_8
			product	hypothetical protein
			transl_table	11
6735	8630	CDS
			locus_tag	Edafosvirus_18_9
			product	TROVE domain protein
			transl_table	11
9161	8751	CDS
			locus_tag	Edafosvirus_18_10
			product	thioredoxin
			transl_table	11
9586	9209	CDS
			locus_tag	Edafosvirus_18_11
			product	hypothetical protein
			transl_table	11
10545	9622	CDS
			locus_tag	Edafosvirus_18_12
			product	hypothetical protein Klosneuvirus_5_32
			transl_table	11
10651	11622	CDS
			locus_tag	Edafosvirus_18_13
			product	protein of unknown function DUF3419
			transl_table	11
12272	11583	CDS
			locus_tag	Edafosvirus_18_14
			product	heme oxygenase
			transl_table	11
12386	12904	CDS
			locus_tag	Edafosvirus_18_15
			product	hypothetical protein ACD_45C00345G0003
			transl_table	11
14938	12896	CDS
			locus_tag	Edafosvirus_18_16
			product	glycosyltransferase
			transl_table	11
15022	15381	CDS
			locus_tag	Edafosvirus_18_17
			product	hypothetical protein Indivirus_1_218
			transl_table	11
15468	15974	CDS
			locus_tag	Edafosvirus_18_18
			product	hypothetical protein
			transl_table	11
19328	15981	CDS
			locus_tag	Edafosvirus_18_19
			product	hypothetical protein Klosneuvirus_1_394
			transl_table	11
22439	19503	CDS
			locus_tag	Edafosvirus_18_20
			product	ubiquitin-activating enzyme E1
			transl_table	11
23779	22469	CDS
			locus_tag	Edafosvirus_18_21
			product	hypothetical protein
			transl_table	11
24270	24175	CDS
			locus_tag	Edafosvirus_18_22
			product	hypothetical protein
			transl_table	11
>Edafosvirus_19
639	1	CDS
			locus_tag	Edafosvirus_19_1
			product	DEAD/SNF2-like helicase
			transl_table	11
995	678	CDS
			locus_tag	Edafosvirus_19_2
			product	hypothetical protein
			transl_table	11
2195	1053	CDS
			locus_tag	Edafosvirus_19_3
			product	hypothetical protein
			transl_table	11
2288	2974	CDS
			locus_tag	Edafosvirus_19_4
			product	hypothetical protein
			transl_table	11
4966	2966	CDS
			locus_tag	Edafosvirus_19_5
			product	hypothetical protein
			transl_table	11
5959	5024	CDS
			locus_tag	Edafosvirus_19_6
			product	hypothetical protein Klosneuvirus_4_123
			transl_table	11
6094	6726	CDS
			locus_tag	Edafosvirus_19_7
			product	putative papain-like cysteine peptidase
			transl_table	11
7947	6712	CDS
			locus_tag	Edafosvirus_19_8
			product	hypothetical protein
			transl_table	11
9173	8004	CDS
			locus_tag	Edafosvirus_19_9
			product	hypothetical protein
			transl_table	11
9924	9406	CDS
			locus_tag	Edafosvirus_19_10
			product	hypoxanthine phosphoribosyltransferase
			transl_table	11
10369	11412	CDS
			locus_tag	Edafosvirus_19_11
			product	hypothetical protein
			transl_table	11
11651	11409	CDS
			locus_tag	Edafosvirus_19_12
			product	hypothetical protein
			transl_table	11
12078	11926	CDS
			locus_tag	Edafosvirus_19_13
			product	hypothetical protein
			transl_table	11
12356	12138	CDS
			locus_tag	Edafosvirus_19_14
			product	hypothetical protein
			transl_table	11
12992	12438	CDS
			locus_tag	Edafosvirus_19_15
			product	hypothetical protein
			transl_table	11
13777	13007	CDS
			locus_tag	Edafosvirus_19_16
			product	hypothetical protein
			transl_table	11
15273	13807	CDS
			locus_tag	Edafosvirus_19_17
			product	hypothetical protein
			transl_table	11
15423	16511	CDS
			locus_tag	Edafosvirus_19_18
			product	hypothetical protein
			transl_table	11
17514	16513	CDS
			locus_tag	Edafosvirus_19_19
			product	hypothetical protein
			transl_table	11
17849	17688	CDS
			locus_tag	Edafosvirus_19_20
			product	hypothetical protein
			transl_table	11
17927	19642	CDS
			locus_tag	Edafosvirus_19_21
			product	hypothetical protein A2145_05275
			transl_table	11
19996	19619	CDS
			locus_tag	Edafosvirus_19_22
			product	hypothetical protein A2385_05345
			transl_table	11
21054	20065	CDS
			locus_tag	Edafosvirus_19_23
			product	hypothetical protein
			transl_table	11
21587	21159	CDS
			locus_tag	Edafosvirus_19_24
			product	hypothetical protein
			transl_table	11
22280	21657	CDS
			locus_tag	Edafosvirus_19_25
			product	UvrD/REP helicase family protein
			transl_table	11
23215	22406	CDS
			locus_tag	Edafosvirus_19_26
			product	unnamed protein product
			transl_table	11
23868	23290	CDS
			locus_tag	Edafosvirus_19_27
			product	hypothetical protein
			transl_table	11
>Edafosvirus_20
3	122	CDS
			locus_tag	Edafosvirus_20_1
			product	hypothetical protein
			transl_table	11
171	1436	CDS
			locus_tag	Edafosvirus_20_2
			product	hypothetical protein
			transl_table	11
1916	1428	CDS
			locus_tag	Edafosvirus_20_3
			product	hypothetical protein
			transl_table	11
2039	3295	CDS
			locus_tag	Edafosvirus_20_4
			product	hypothetical protein
			transl_table	11
3353	4336	CDS
			locus_tag	Edafosvirus_20_5
			product	hypothetical protein
			transl_table	11
5228	4422	CDS
			locus_tag	Edafosvirus_20_6
			product	hypothetical protein
			transl_table	11
5420	6400	CDS
			locus_tag	Edafosvirus_20_7
			product	hypothetical protein
			transl_table	11
6614	8116	CDS
			locus_tag	Edafosvirus_20_8
			product	hypothetical protein Indivirus_4_15
			transl_table	11
8201	9115	CDS
			locus_tag	Edafosvirus_20_9
			product	hypothetical protein
			transl_table	11
11038	9221	CDS
			locus_tag	Edafosvirus_20_10
			product	NCLDV major capsid protein
			transl_table	11
11535	11323	CDS
			locus_tag	Edafosvirus_20_11
			product	hypothetical protein
			transl_table	11
11678	12760	CDS
			locus_tag	Edafosvirus_20_12
			product	late transcription factor VLTF3-like protein
			transl_table	11
13137	14465	CDS
			locus_tag	Edafosvirus_20_13
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
15243	15710	CDS
			locus_tag	Edafosvirus_20_14
			product	late transcription factor VLTF3-like protein
			transl_table	11
15848	15711	CDS
			locus_tag	Edafosvirus_20_15
			product	hypothetical protein
			transl_table	11
15892	16740	CDS
			locus_tag	Edafosvirus_20_16
			product	hypothetical protein A2834_02655
			transl_table	11
16901	17443	CDS
			locus_tag	Edafosvirus_20_17
			product	hypothetical protein
			transl_table	11
19064	17481	CDS
			locus_tag	Edafosvirus_20_18
			product	hypothetical protein mc_388
			transl_table	11
19740	19333	CDS
			locus_tag	Edafosvirus_20_19
			product	hypothetical protein
			transl_table	11
19868	20674	CDS
			locus_tag	Edafosvirus_20_20
			product	hypothetical protein
			transl_table	11
20731	21582	CDS
			locus_tag	Edafosvirus_20_21
			product	late transcription factor VLTF3-like protein
			transl_table	11
>Edafosvirus_21
3	593	CDS
			locus_tag	Edafosvirus_21_1
			product	Isochorismatase hydrolase
			transl_table	11
652	1506	CDS
			locus_tag	Edafosvirus_21_2
			product	hypothetical protein
			transl_table	11
1998	1510	CDS
			locus_tag	Edafosvirus_21_3
			product	hypothetical protein
			transl_table	11
2944	2102	CDS
			locus_tag	Edafosvirus_21_4
			product	hypothetical protein UT24_C0018G0031
			transl_table	11
3599	3009	CDS
			locus_tag	Edafosvirus_21_5
			product	hypothetical protein
			transl_table	11
3748	4374	CDS
			locus_tag	Edafosvirus_21_6
			product	hypothetical protein PBRA_001806
			transl_table	11
4882	4376	CDS
			locus_tag	Edafosvirus_21_7
			product	hypothetical protein
			transl_table	11
4987	5526	CDS
			locus_tag	Edafosvirus_21_8
			product	hypothetical protein
			transl_table	11
5604	5984	CDS
			locus_tag	Edafosvirus_21_9
			product	hypothetical protein
			transl_table	11
6560	5985	CDS
			locus_tag	Edafosvirus_21_10
			product	macro domain containing protein
			transl_table	11
6666	8165	CDS
			locus_tag	Edafosvirus_21_11
			product	LMBR1-domain-containing protein
			transl_table	11
8581	9030	CDS
			locus_tag	Edafosvirus_21_12
			product	hypothetical protein
			transl_table	11
10014	9016	CDS
			locus_tag	Edafosvirus_21_13
			product	hypothetical protein
			transl_table	11
10885	10076	CDS
			locus_tag	Edafosvirus_21_14
			product	hypothetical protein
			transl_table	11
11694	10954	CDS
			locus_tag	Edafosvirus_21_15
			product	phage/plasmid primase, P4 family
			transl_table	11
12693	11737	CDS
			locus_tag	Edafosvirus_21_16
			product	alpha/beta hydrolase
			transl_table	11
13325	12747	CDS
			locus_tag	Edafosvirus_21_17
			product	hypothetical protein
			transl_table	11
13455	14138	CDS
			locus_tag	Edafosvirus_21_18
			product	hypothetical protein
			transl_table	11
17074	18135	CDS
			locus_tag	Edafosvirus_21_19
			product	hypothetical protein Klosneuvirus_1_132
			transl_table	11
18261	18908	CDS
			locus_tag	Edafosvirus_21_20
			product	hypothetical protein
			transl_table	11
18974	19612	CDS
			locus_tag	Edafosvirus_21_21
			product	hypothetical protein
			transl_table	11
19683	19799	CDS
			locus_tag	Edafosvirus_21_22
			product	hypothetical protein
			transl_table	11
>Edafosvirus_22
703	2	CDS
			locus_tag	Edafosvirus_22_1
			product	hypothetical protein
			transl_table	11
2026	770	CDS
			locus_tag	Edafosvirus_22_2
			product	hypothetical protein
			transl_table	11
2441	2091	CDS
			locus_tag	Edafosvirus_22_3
			product	hypothetical protein
			transl_table	11
3914	2535	CDS
			locus_tag	Edafosvirus_22_4
			product	phosphodiester glycosidase family protein
			transl_table	11
3994	6135	CDS
			locus_tag	Edafosvirus_22_5
			product	putative orfan
			transl_table	11
6660	6130	CDS
			locus_tag	Edafosvirus_22_6
			product	YdcF family protein
			transl_table	11
7387	6839	CDS
			locus_tag	Edafosvirus_22_7
			product	hypothetical protein
			transl_table	11
8671	7391	CDS
			locus_tag	Edafosvirus_22_8
			product	hypothetical protein C0J52_06044
			transl_table	11
8761	9018	CDS
			locus_tag	Edafosvirus_22_9
			product	hypothetical protein
			transl_table	11
9980	9009	CDS
			locus_tag	Edafosvirus_22_10
			product	hypothetical protein Indivirus_4_9
			transl_table	11
10237	11277	CDS
			locus_tag	Edafosvirus_22_11
			product	hypothetical protein
			transl_table	11
11992	11339	CDS
			locus_tag	Edafosvirus_22_12
			product	hypothetical protein Klosneuvirus_1_59
			transl_table	11
14555	12093	CDS
			locus_tag	Edafosvirus_22_13
			product	hypothetical protein Catovirus_2_301
			transl_table	11
14700	15185	CDS
			locus_tag	Edafosvirus_22_14
			product	putative protein disulfide isomerase PDIa
			transl_table	11
16348	15239	CDS
			locus_tag	Edafosvirus_22_15
			product	hypothetical protein
			transl_table	11
17585	16458	CDS
			locus_tag	Edafosvirus_22_16
			product	ADP-ribosylglycohydrolase
			transl_table	11
17658	18662	CDS
			locus_tag	Edafosvirus_22_17
			product	DnaJ domain protein
			transl_table	11
19518	18814	CDS
			locus_tag	Edafosvirus_22_18
			product	patatin-like phospholipase
			transl_table	11
>Edafosvirus_23
563	3	CDS
			locus_tag	Edafosvirus_23_1
			product	hypothetical protein
			transl_table	11
1626	667	CDS
			locus_tag	Edafosvirus_23_2
			product	hypothetical protein
			transl_table	11
3797	1740	CDS
			locus_tag	Edafosvirus_23_3
			product	hypothetical protein
			transl_table	11
3937	5862	CDS
			locus_tag	Edafosvirus_23_4
			product	hypothetical protein
			transl_table	11
5963	6445	CDS
			locus_tag	Edafosvirus_23_5
			product	hypothetical protein
			transl_table	11
6587	7585	CDS
			locus_tag	Edafosvirus_23_6
			product	hypothetical protein
			transl_table	11
8717	7560	CDS
			locus_tag	Edafosvirus_23_7
			product	RNA ligase, T4 RnlA-like protein
			transl_table	11
10819	8768	CDS
			locus_tag	Edafosvirus_23_8
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
11467	11255	CDS
			locus_tag	Edafosvirus_23_9
			product	hypothetical protein
			transl_table	11
11836	11540	CDS
			locus_tag	Edafosvirus_23_10
			product	hypothetical protein
			transl_table	11
12587	11907	CDS
			locus_tag	Edafosvirus_23_11
			product	hypothetical protein
			transl_table	11
12717	13760	CDS
			locus_tag	Edafosvirus_23_12
			product	hypothetical protein
			transl_table	11
14443	13766	CDS
			locus_tag	Edafosvirus_23_13
			product	hypothetical protein
			transl_table	11
>Edafosvirus_24
1192	2	CDS
			locus_tag	Edafosvirus_24_1
			product	hypothetical protein Catovirus_1_1060
			transl_table	11
1388	2203	CDS
			locus_tag	Edafosvirus_24_2
			product	hypothetical protein Klosneuvirus_1_202
			transl_table	11
2563	2183	CDS
			locus_tag	Edafosvirus_24_3
			product	hypothetical protein
			transl_table	11
3283	4335	CDS
			locus_tag	Edafosvirus_24_4
			product	hypothetical protein
			transl_table	11
8793	4336	CDS
			locus_tag	Edafosvirus_24_5
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
9852	9052	CDS
			locus_tag	Edafosvirus_24_6
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
10574	10077	CDS
			locus_tag	Edafosvirus_24_7
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
11624	10842	CDS
			locus_tag	Edafosvirus_24_8
			product	GIY-YIG-like endonuclease
			transl_table	11
12955	11897	CDS
			locus_tag	Edafosvirus_24_9
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
13069	13656	CDS
			locus_tag	Edafosvirus_24_10
			product	exported hypothetical protein
			transl_table	11
13724	14365	CDS
			locus_tag	Edafosvirus_24_11
			product	hypothetical protein
			transl_table	11
14886	14572	CDS
			locus_tag	Edafosvirus_24_12
			product	hypothetical protein
			transl_table	11
>Edafosvirus_25
1	1236	CDS
			locus_tag	Edafosvirus_25_1
			product	hypothetical protein
			transl_table	11
2562	1405	CDS
			locus_tag	Edafosvirus_25_2
			product	hypothetical protein
			transl_table	11
2824	3750	CDS
			locus_tag	Edafosvirus_25_3
			product	TATA box binding protein
			transl_table	11
5138	3780	CDS
			locus_tag	Edafosvirus_25_4
			product	hypothetical protein
			transl_table	11
8958	5305	CDS
			locus_tag	Edafosvirus_25_5
			product	hypothetical protein Catovirus_2_307
			transl_table	11
9826	9029	CDS
			locus_tag	Edafosvirus_25_6
			product	hypothetical protein
			transl_table	11
9911	10609	CDS
			locus_tag	Edafosvirus_25_7
			product	hypothetical protein
			transl_table	11
11275	10685	CDS
			locus_tag	Edafosvirus_25_8
			product	hypothetical protein Indivirus_1_24
			transl_table	11
11804	11337	CDS
			locus_tag	Edafosvirus_25_9
			product	hypothetical protein, partial
			transl_table	11
12322	11888	CDS
			locus_tag	Edafosvirus_25_10
			product	hypothetical protein Catovirus_2_310
			transl_table	11
13439	12399	CDS
			locus_tag	Edafosvirus_25_11
			product	hypothetical protein Klosneuvirus_1_103
			transl_table	11
13522	14058	CDS
			locus_tag	Edafosvirus_25_12
			product	hypothetical protein Klosneuvirus_1_104
			transl_table	11
>Edafosvirus_26
3	2234	CDS
			locus_tag	Edafosvirus_26_1
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
2215	2307	CDS
			locus_tag	Edafosvirus_26_2
			product	hypothetical protein
			transl_table	11
2351	3184	CDS
			locus_tag	Edafosvirus_26_3
			product	HNH Endonuclease
			transl_table	11
3638	3549	CDS
			locus_tag	Edafosvirus_26_4
			product	hypothetical protein
			transl_table	11
4160	5704	CDS
			locus_tag	Edafosvirus_26_5
			product	intein-containing DNA-directed RNA polymerase subunit 2
			transl_table	11
6077	6544	CDS
			locus_tag	Edafosvirus_26_6
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
6637	7437	CDS
			locus_tag	Edafosvirus_26_7
			product	uncharacterized HNH endonuclease
			transl_table	11
7810	8115	CDS
			locus_tag	Edafosvirus_26_8
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
8137	8331	CDS
			locus_tag	Edafosvirus_26_9
			product	hypothetical protein
			transl_table	11
9848	8571	CDS
			locus_tag	Edafosvirus_26_10
			product	hypothetical protein
			transl_table	11
9922	12546	CDS
			locus_tag	Edafosvirus_26_11
			product	hypothetical protein Indivirus_1_109
			transl_table	11
12601	13008	CDS
			locus_tag	Edafosvirus_26_12
			product	hypothetical protein
			transl_table	11
13573	13010	CDS
			locus_tag	Edafosvirus_26_13
			product	phosphotransferase KptA/Tpt1
			transl_table	11
>Edafosvirus_27
1	390	CDS
			locus_tag	Edafosvirus_27_1
			product	ankyrin
			transl_table	11
420	596	CDS
			locus_tag	Edafosvirus_27_2
			product	hypothetical protein
			transl_table	11
623	1492	CDS
			locus_tag	Edafosvirus_27_3
			product	hypothetical protein
			transl_table	11
1613	4786	CDS
			locus_tag	Edafosvirus_27_4
			product	isoleucyl-tRNA synthetase
			transl_table	11
5548	4781	CDS
			locus_tag	Edafosvirus_27_5
			product	hypothetical protein
			transl_table	11
5640	5521	CDS
			locus_tag	Edafosvirus_27_6
			product	hypothetical protein
			transl_table	11
6958	5621	CDS
			locus_tag	Edafosvirus_27_7
			product	hypothetical protein Klosneuvirus_2_130
			transl_table	11
7027	7872	CDS
			locus_tag	Edafosvirus_27_8
			product	hypothetical protein
			transl_table	11
9836	7902	CDS
			locus_tag	Edafosvirus_27_9
			product	bifunctional phosphoribosyl-AMP cyclohydrolase/phosphoribosyl-ATP pyrophosphatase protein
			transl_table	11
10924	9899	CDS
			locus_tag	Edafosvirus_27_10
			product	hypothetical protein
			transl_table	11
11022	12026	CDS
			locus_tag	Edafosvirus_27_11
			product	2OG-FeII oxygenase
			transl_table	11
12450	12602	CDS
			locus_tag	Edafosvirus_27_12
			product	hypothetical protein
			transl_table	11
12732	12592	CDS
			locus_tag	Edafosvirus_27_13
			product	hypothetical protein
			transl_table	11
12869	12729	CDS
			locus_tag	Edafosvirus_27_14
			product	hypothetical protein
			transl_table	11
13006	12866	CDS
			locus_tag	Edafosvirus_27_15
			product	hypothetical protein
			transl_table	11
13385	13296	CDS
			locus_tag	Edafosvirus_27_16
			product	hypothetical protein
			transl_table	11
>Edafosvirus_28
43	1026	CDS
			locus_tag	Edafosvirus_28_1
			product	hypothetical protein Indivirus_1_88
			transl_table	11
1139	3823	CDS
			locus_tag	Edafosvirus_28_2
			product	Hsp70 protein
			transl_table	11
3946	5079	CDS
			locus_tag	Edafosvirus_28_3
			product	hypothetical protein
			transl_table	11
5363	5076	CDS
			locus_tag	Edafosvirus_28_4
			product	hypothetical protein
			transl_table	11
6528	5341	CDS
			locus_tag	Edafosvirus_28_5
			product	hypothetical protein Catovirus_1_1049
			transl_table	11
7369	6629	CDS
			locus_tag	Edafosvirus_28_6
			product	hypothetical protein Klosneuvirus_1_216
			transl_table	11
8560	7544	CDS
			locus_tag	Edafosvirus_28_7
			product	DNA repair protein
			transl_table	11
8656	11637	CDS
			locus_tag	Edafosvirus_28_8
			product	ATP-dependent Lon protease
			transl_table	11
13013	11634	CDS
			locus_tag	Edafosvirus_28_9
			product	transcription initiation factor IIB
			transl_table	11
>Edafosvirus_29
470	3	CDS
			locus_tag	Edafosvirus_29_1
			product	unnamed protein product
			transl_table	11
695	1591	CDS
			locus_tag	Edafosvirus_29_2
			product	hypothetical protein Klosneuvirus_4_55
			transl_table	11
1652	2188	CDS
			locus_tag	Edafosvirus_29_3
			product	putative sensor histidine kinase
			transl_table	11
2316	2666	CDS
			locus_tag	Edafosvirus_29_4
			product	putative sensor histidine kinase
			transl_table	11
2722	4656	CDS
			locus_tag	Edafosvirus_29_5
			product	histidine kinase
			transl_table	11
4712	5188	CDS
			locus_tag	Edafosvirus_29_6
			product	histidine protein kinase SLN1
			transl_table	11
5219	5572	CDS
			locus_tag	Edafosvirus_29_7
			product	hypothetical protein
			transl_table	11
5605	7410	CDS
			locus_tag	Edafosvirus_29_8
			product	glucosamine-fructose-6-phosphate aminotransferase
			transl_table	11
7748	7416	CDS
			locus_tag	Edafosvirus_29_9
			product	protein of unknown function DUF4326
			transl_table	11
8089	8193	CDS
			locus_tag	Edafosvirus_29_10
			product	hypothetical protein
			transl_table	11
8178	11609	CDS
			locus_tag	Edafosvirus_29_11
			product	hypothetical protein BCR33DRAFT_848053
			transl_table	11
11671	12021	CDS
			locus_tag	Edafosvirus_29_12
			product	hypothetical protein
			transl_table	11
12437	12018	CDS
			locus_tag	Edafosvirus_29_13
			product	metallophosphatase
			transl_table	11
>Edafosvirus_30
3	2246	CDS
			locus_tag	Edafosvirus_30_1
			product	early transcription factor VETF large subunit
			transl_table	11
2310	2594	CDS
			locus_tag	Edafosvirus_30_2
			product	hypothetical protein Klosneuvirus_2_262
			transl_table	11
2673	3227	CDS
			locus_tag	Edafosvirus_30_3
			product	hypothetical protein Catovirus_2_221
			transl_table	11
4260	3442	CDS
			locus_tag	Edafosvirus_30_4
			product	hypothetical protein Klosneuvirus_2_260
			transl_table	11
4304	4504	CDS
			locus_tag	Edafosvirus_30_5
			product	hypothetical protein
			transl_table	11
4556	5518	CDS
			locus_tag	Edafosvirus_30_6
			product	hypothetical protein Klosneuvirus_2_259
			transl_table	11
5575	6222	CDS
			locus_tag	Edafosvirus_30_7
			product	hypothetical protein Klosneuvirus_2_258
			transl_table	11
6768	6226	CDS
			locus_tag	Edafosvirus_30_8
			product	hypothetical protein
			transl_table	11
6926	8998	CDS
			locus_tag	Edafosvirus_30_9
			product	cullin family protein
			transl_table	11
9077	11824	CDS
			locus_tag	Edafosvirus_30_10
			product	hypothetical protein Catovirus_2_214
			transl_table	11
12198	11836	CDS
			locus_tag	Edafosvirus_30_11
			product	transcription elongation factor TFIIS
			transl_table	11
>Edafosvirus_31
96	1	CDS
			locus_tag	Edafosvirus_31_1
			product	hypothetical protein
			transl_table	11
624	1778	CDS
			locus_tag	Edafosvirus_31_2
			product	hypothetical protein Catovirus_1_1020
			transl_table	11
1971	1798	CDS
			locus_tag	Edafosvirus_31_3
			product	hypothetical protein
			transl_table	11
2030	3019	CDS
			locus_tag	Edafosvirus_31_4
			product	hypothetical protein
			transl_table	11
6812	3096	CDS
			locus_tag	Edafosvirus_31_5
			product	mRNA capping enzyme
			transl_table	11
6991	8601	CDS
			locus_tag	Edafosvirus_31_6
			product	hypothetical protein
			transl_table	11
8675	10249	CDS
			locus_tag	Edafosvirus_31_7
			product	FtsJ-like methyltransferase
			transl_table	11
10590	10255	CDS
			locus_tag	Edafosvirus_31_8
			product	hypothetical protein
			transl_table	11
11082	10714	CDS
			locus_tag	Edafosvirus_31_9
			product	hypothetical protein
			transl_table	11
>Edafosvirus_32
1	159	CDS
			locus_tag	Edafosvirus_32_1
			product	hypothetical protein
			transl_table	11
239	1327	CDS
			locus_tag	Edafosvirus_32_2
			product	polynucleotide phosphatase/kinase
			transl_table	11
2396	1329	CDS
			locus_tag	Edafosvirus_32_3
			product	tyrosine-tRNA synthetase
			transl_table	11
3393	2476	CDS
			locus_tag	Edafosvirus_32_4
			product	hypothetical protein BMW23_0408
			transl_table	11
4538	3453	CDS
			locus_tag	Edafosvirus_32_5
			product	thioredoxin
			transl_table	11
5826	4651	CDS
			locus_tag	Edafosvirus_32_6
			product	hypothetical protein Klosneuvirus_1_125
			transl_table	11
6286	5891	CDS
			locus_tag	Edafosvirus_32_7
			product	hypothetical protein Klosneuvirus_1_126
			transl_table	11
7229	6621	CDS
			locus_tag	Edafosvirus_32_8
			product	hypothetical protein Indivirus_1_43
			transl_table	11
7326	8408	CDS
			locus_tag	Edafosvirus_32_9
			product	DNA directed RNA polymerase subunit L
			transl_table	11
8880	8425	CDS
			locus_tag	Edafosvirus_32_10
			product	hypothetical protein
			transl_table	11
10379	8952	CDS
			locus_tag	Edafosvirus_32_11
			product	RNA ligase
			transl_table	11
>Edafosvirus_33
2	241	CDS
			locus_tag	Edafosvirus_33_1
			product	hypothetical protein
			transl_table	11
335	2056	CDS
			locus_tag	Edafosvirus_33_2
			product	Flotillin domain-containing protein
			transl_table	11
3240	2107	CDS
			locus_tag	Edafosvirus_33_3
			product	Hypothetical protein ORPV_338
			transl_table	11
4361	3324	CDS
			locus_tag	Edafosvirus_33_4
			product	hypothetical protein
			transl_table	11
4330	5784	CDS
			locus_tag	Edafosvirus_33_5
			product	DHH family phosphohydrolase
			transl_table	11
5848	7062	CDS
			locus_tag	Edafosvirus_33_6
			product	hypothetical protein
			transl_table	11
7846	7085	CDS
			locus_tag	Edafosvirus_33_7
			product	hypothetical protein
			transl_table	11
8248	8916	CDS
			locus_tag	Edafosvirus_33_8
			product	RING-H2 zinc finger
			transl_table	11
9718	8909	CDS
			locus_tag	Edafosvirus_33_9
			product	Erv1/Alr family disulfide thiol oxidoreductase
			transl_table	11
10163	10312	CDS
			locus_tag	Edafosvirus_33_10
			product	hypothetical protein
			transl_table	11
>Edafosvirus_34
2	682	CDS
			locus_tag	Edafosvirus_34_1
			product	hypothetical protein
			transl_table	11
762	1745	CDS
			locus_tag	Edafosvirus_34_2
			product	hypothetical protein
			transl_table	11
1831	2811	CDS
			locus_tag	Edafosvirus_34_3
			product	putative Fe-S-cluster redox enzyme
			transl_table	11
3219	2806	CDS
			locus_tag	Edafosvirus_34_4
			product	hypothetical protein
			transl_table	11
3679	3269	CDS
			locus_tag	Edafosvirus_34_5
			product	hypothetical protein
			transl_table	11
3813	4430	CDS
			locus_tag	Edafosvirus_34_6
			product	hypothetical protein
			transl_table	11
5409	4441	CDS
			locus_tag	Edafosvirus_34_7
			product	hypothetical protein LBA_00929
			transl_table	11
6294	5500	CDS
			locus_tag	Edafosvirus_34_8
			product	hypothetical protein Indivirus_6_13
			transl_table	11
6453	7460	CDS
			locus_tag	Edafosvirus_34_9
			product	hypothetical protein
			transl_table	11
7559	8107	CDS
			locus_tag	Edafosvirus_34_10
			product	hypothetical protein
			transl_table	11
8172	8780	CDS
			locus_tag	Edafosvirus_34_11
			product	hypothetical protein
			transl_table	11
9658	8819	CDS
			locus_tag	Edafosvirus_34_12
			product	bifunctional protein FolD
			transl_table	11
10252	9719	CDS
			locus_tag	Edafosvirus_34_13
			product	hypothetical protein
			transl_table	11
>Edafosvirus_35
3763	59	CDS
			locus_tag	Edafosvirus_35_1
			product	putative DNA repair protein
			transl_table	11
3931	4860	CDS
			locus_tag	Edafosvirus_35_2
			product	hypothetical protein
			transl_table	11
5789	4911	CDS
			locus_tag	Edafosvirus_35_3
			product	hypothetical protein NIES4075_18460
			transl_table	11
5680	6468	CDS
			locus_tag	Edafosvirus_35_4
			product	hypothetical protein PALB_13110
			transl_table	11
7554	6460	CDS
			locus_tag	Edafosvirus_35_5
			product	hypothetical protein DDB_G0282767
			transl_table	11
8123	9076	CDS
			locus_tag	Edafosvirus_35_6
			product	hypothetical protein
			transl_table	11
10080	9274	CDS
			locus_tag	Edafosvirus_35_7
			product	hypothetical protein
			transl_table	11
>Edafosvirus_36
767	3	CDS
			locus_tag	Edafosvirus_36_1
			product	7tm chemosensory receptor/ankyrin repeat domain-containing protein
			transl_table	11
1184	951	CDS
			locus_tag	Edafosvirus_36_2
			product	hypothetical protein Catovirus_1_1087
			transl_table	11
1239	1856	CDS
			locus_tag	Edafosvirus_36_3
			product	hypothetical protein Catovirus_1_1086
			transl_table	11
3831	2374	CDS
			locus_tag	Edafosvirus_36_4
			product	ankyrin repeat protein
			transl_table	11
4002	4892	CDS
			locus_tag	Edafosvirus_36_5
			product	hypothetical protein Catovirus_1_1084
			transl_table	11
4963	5508	CDS
			locus_tag	Edafosvirus_36_6
			product	hypothetical protein Klosneuvirus_1_186
			transl_table	11
5598	6272	CDS
			locus_tag	Edafosvirus_36_7
			product	hypothetical protein
			transl_table	11
6523	7101	CDS
			locus_tag	Edafosvirus_36_8
			product	hypothetical protein Catovirus_1_1082
			transl_table	11
7139	7654	CDS
			locus_tag	Edafosvirus_36_9
			product	hypothetical protein
			transl_table	11
7791	7928	CDS
			locus_tag	Edafosvirus_36_10
			product	hypothetical protein
			transl_table	11
7928	8065	CDS
			locus_tag	Edafosvirus_36_11
			product	hypothetical protein
			transl_table	11
8065	8202	CDS
			locus_tag	Edafosvirus_36_12
			product	hypothetical protein
			transl_table	11
8202	8339	CDS
			locus_tag	Edafosvirus_36_13
			product	hypothetical protein
			transl_table	11
8339	8476	CDS
			locus_tag	Edafosvirus_36_14
			product	hypothetical protein
			transl_table	11
8476	8613	CDS
			locus_tag	Edafosvirus_36_15
			product	hypothetical protein
			transl_table	11
8743	8603	CDS
			locus_tag	Edafosvirus_36_16
			product	hypothetical protein
			transl_table	11
>Edafosvirus_37
764	651	CDS
			locus_tag	Edafosvirus_37_1
			product	hypothetical protein
			transl_table	11
1657	1785	CDS
			locus_tag	Edafosvirus_37_2
			product	hypothetical protein
			transl_table	11
1782	1910	CDS
			locus_tag	Edafosvirus_37_3
			product	hypothetical protein
			transl_table	11
1907	2047	CDS
			locus_tag	Edafosvirus_37_4
			product	hypothetical protein
			transl_table	11
2044	2172	CDS
			locus_tag	Edafosvirus_37_5
			product	hypothetical protein
			transl_table	11
2169	2414	CDS
			locus_tag	Edafosvirus_37_6
			product	hypothetical protein
			transl_table	11
2411	2551	CDS
			locus_tag	Edafosvirus_37_7
			product	hypothetical protein
			transl_table	11
2744	2541	CDS
			locus_tag	Edafosvirus_37_8
			product	hypothetical protein
			transl_table	11
2911	3945	CDS
			locus_tag	Edafosvirus_37_9
			product	hypothetical protein Klosneuvirus_1_132
			transl_table	11
4878	3937	CDS
			locus_tag	Edafosvirus_37_10
			product	Alpha-soluble NSF attachment protein, partial
			transl_table	11
6757	4946	CDS
			locus_tag	Edafosvirus_37_11
			product	hypothetical protein Klosneuvirus_1_99
			transl_table	11
6949	6827	CDS
			locus_tag	Edafosvirus_37_12
			product	hypothetical protein
			transl_table	11
7444	7289	CDS
			locus_tag	Edafosvirus_37_13
			product	hypothetical protein
			transl_table	11
8001	8156	CDS
			locus_tag	Edafosvirus_37_14
			product	hypothetical protein
			transl_table	11
>Edafosvirus_38
2	1237	CDS
			locus_tag	Edafosvirus_38_1
			product	HD phosphohydrolase
			transl_table	11
1779	1270	CDS
			locus_tag	Edafosvirus_38_2
			product	NADAR family protein
			transl_table	11
1956	2522	CDS
			locus_tag	Edafosvirus_38_3
			product	hypothetical protein
			transl_table	11
2621	4582	CDS
			locus_tag	Edafosvirus_38_4
			product	heat shock cognate 71 kDa protein
			transl_table	11
5465	4659	CDS
			locus_tag	Edafosvirus_38_5
			product	hypothetical protein
			transl_table	11
6280	5498	CDS
			locus_tag	Edafosvirus_38_6
			product	hypothetical protein PBRA_002631
			transl_table	11
7048	7230	CDS
			locus_tag	Edafosvirus_38_7
			product	hypothetical protein
			transl_table	11
>Edafosvirus_39
2	907	CDS
			locus_tag	Edafosvirus_39_1
			product	hypothetical protein
			transl_table	11
947	1252	CDS
			locus_tag	Edafosvirus_39_2
			product	hypothetical protein
			transl_table	11
1327	2202	CDS
			locus_tag	Edafosvirus_39_3
			product	hypothetical protein Catovirus_1_742
			transl_table	11
3239	2199	CDS
			locus_tag	Edafosvirus_39_4
			product	HNH endonuclease
			transl_table	11
3325	4209	CDS
			locus_tag	Edafosvirus_39_5
			product	hypothetical protein
			transl_table	11
4919	4224	CDS
			locus_tag	Edafosvirus_39_6
			product	hypothetical protein
			transl_table	11
5069	5578	CDS
			locus_tag	Edafosvirus_39_7
			product	hypothetical protein AAP_00955
			transl_table	11
6807	6968	CDS
			locus_tag	Edafosvirus_39_8
			product	hypothetical protein
			transl_table	11
>Edafosvirus_40
369	1	CDS
			locus_tag	Edafosvirus_40_1
			product	hypothetical protein
			transl_table	11
680	456	CDS
			locus_tag	Edafosvirus_40_2
			product	hypothetical protein
			transl_table	11
701	2455	CDS
			locus_tag	Edafosvirus_40_3
			product	GTP binding translation elongation factor
			transl_table	11
2663	2448	CDS
			locus_tag	Edafosvirus_40_4
			product	hypothetical protein Indivirus_3_53
			transl_table	11
3139	2732	CDS
			locus_tag	Edafosvirus_40_5
			product	hypothetical protein
			transl_table	11
3283	4398	CDS
			locus_tag	Edafosvirus_40_6
			product	glycogen debranching enzyme alpha-1,6-glucosidase
			transl_table	11
5949	4390	CDS
			locus_tag	Edafosvirus_40_7
			product	dehydrogenase
			transl_table	11
6341	5973	CDS
			locus_tag	Edafosvirus_40_8
			product	hypothetical protein Catovirus_2_195
			transl_table	11
6791	6456	CDS
			locus_tag	Edafosvirus_40_9
			product	hypothetical protein
			transl_table	11
>Edafosvirus_41
3	620	CDS
			locus_tag	Edafosvirus_41_1
			product	hypothetical protein
			transl_table	11
1672	521	CDS
			locus_tag	Edafosvirus_41_2
			product	glycosyltransferase
			transl_table	11
1775	2299	CDS
			locus_tag	Edafosvirus_41_3
			product	hypothetical protein LSAT_5X104520
			transl_table	11
3746	2301	CDS
			locus_tag	Edafosvirus_41_4
			product	glycyl-tRNA synthetase
			transl_table	11
3871	4947	CDS
			locus_tag	Edafosvirus_41_5
			product	TIGR03118 family protein
			transl_table	11
4965	6077	CDS
			locus_tag	Edafosvirus_41_6
			product	TIGR03118 family protein
			transl_table	11
6692	6066	CDS
			locus_tag	Edafosvirus_41_7
			product	hypothetical protein
			transl_table	11
>Edafosvirus_42
1	291	CDS
			locus_tag	Edafosvirus_42_1
			product	DNA gyrase/topoisomerase IV
			transl_table	11
477	608	CDS
			locus_tag	Edafosvirus_42_2
			product	hypothetical protein
			transl_table	11
689	940	CDS
			locus_tag	Edafosvirus_42_3
			product	hypothetical protein
			transl_table	11
1036	1395	CDS
			locus_tag	Edafosvirus_42_4
			product	hypothetical protein Klosneuvirus_1_167
			transl_table	11
1598	4918	CDS
			locus_tag	Edafosvirus_42_5
			product	ankyrin repeat protein
			transl_table	11
4890	5129	CDS
			locus_tag	Edafosvirus_42_6
			product	hypothetical protein
			transl_table	11
5155	6111	CDS
			locus_tag	Edafosvirus_42_7
			product	hypothetical protein
			transl_table	11
6529	6657	CDS
			locus_tag	Edafosvirus_42_8
			product	hypothetical protein
			transl_table	11
>Edafosvirus_43
894	1	CDS
			locus_tag	Edafosvirus_43_1
			product	HNH endonuclease
			transl_table	11
964	3288	CDS
			locus_tag	Edafosvirus_43_2
			product	DEAD/SNF2-like helicase
			transl_table	11
3786	3295	CDS
			locus_tag	Edafosvirus_43_3
			product	hypothetical protein
			transl_table	11
5119	4367	CDS
			locus_tag	Edafosvirus_43_4
			product	hypothetical protein
			transl_table	11
5242	6129	CDS
			locus_tag	Edafosvirus_43_5
			product	hypothetical protein Klosneuvirus_2_228
			transl_table	11
6170	6427	CDS
			locus_tag	Edafosvirus_43_6
			product	hypothetical protein
			transl_table	11
>Edafosvirus_44
2	1177	CDS
			locus_tag	Edafosvirus_44_1
			product	DEAD/SNF2-like helicase
			transl_table	11
1249	1935	CDS
			locus_tag	Edafosvirus_44_2
			product	hypothetical protein Catovirus_1_1058
			transl_table	11
2817	2002	CDS
			locus_tag	Edafosvirus_44_3
			product	hypothetical protein Catovirus_1_1059
			transl_table	11
4618	2984	CDS
			locus_tag	Edafosvirus_44_4
			product	hypothetical protein
			transl_table	11
4794	5414	CDS
			locus_tag	Edafosvirus_44_5
			product	hypothetical protein
			transl_table	11
5629	5453	CDS
			locus_tag	Edafosvirus_44_6
			product	hypothetical protein
			transl_table	11
>Edafosvirus_45
3	272	CDS
			locus_tag	Edafosvirus_45_1
			product	hypothetical protein Catovirus_2_296
			transl_table	11
1073	237	CDS
			locus_tag	Edafosvirus_45_2
			product	hypothetical protein
			transl_table	11
1190	1522	CDS
			locus_tag	Edafosvirus_45_3
			product	hypothetical protein Klosneuvirus_1_74
			transl_table	11
2898	1549	CDS
			locus_tag	Edafosvirus_45_4
			product	hypothetical protein
			transl_table	11
3020	4780	CDS
			locus_tag	Edafosvirus_45_5
			product	hypothetical protein Catovirus_2_294
			transl_table	11
5103	5336	CDS
			locus_tag	Edafosvirus_45_6
			product	hypothetical protein
			transl_table	11
5290	5424	CDS
			locus_tag	Edafosvirus_45_7
			product	hypothetical protein
			transl_table	11
>Edafosvirus_46
534	1	CDS
			locus_tag	Edafosvirus_46_1
			product	hypothetical protein
			transl_table	11
1540	602	CDS
			locus_tag	Edafosvirus_46_2
			product	hypothetical protein
			transl_table	11
1964	1599	CDS
			locus_tag	Edafosvirus_46_3
			product	protein of unknown function DUF2493
			transl_table	11
2564	2076	CDS
			locus_tag	Edafosvirus_46_4
			product	hypothetical protein Klosneuvirus_2_136
			transl_table	11
3502	2597	CDS
			locus_tag	Edafosvirus_46_5
			product	hypothetical protein
			transl_table	11
4644	3643	CDS
			locus_tag	Edafosvirus_46_6
			product	hypothetical protein Klosneuvirus_1_132
			transl_table	11
>Edafosvirus_47
2	1192	CDS
			locus_tag	Edafosvirus_47_1
			product	hypothetical protein Catovirus_2_323
			transl_table	11
1179	1766	CDS
			locus_tag	Edafosvirus_47_2
			product	hypothetical protein
			transl_table	11
2001	1723	CDS
			locus_tag	Edafosvirus_47_3
			product	hypothetical protein Indivirus_1_49
			transl_table	11
3923	2781	CDS
			locus_tag	Edafosvirus_47_4
			product	hypothetical protein Indivirus_1_48
			transl_table	11
4491	4093	CDS
			locus_tag	Edafosvirus_47_5
			product	hypothetical protein
			transl_table	11
>Edafosvirus_48
937	2	CDS
			locus_tag	Edafosvirus_48_1
			product	hypothetical protein
			transl_table	11
1651	1013	CDS
			locus_tag	Edafosvirus_48_2
			product	hypothetical protein Indivirus_1_184
			transl_table	11
3683	1737	CDS
			locus_tag	Edafosvirus_48_3
			product	dynamin family protein
			transl_table	11
3778	3963	CDS
			locus_tag	Edafosvirus_48_4
			product	hypothetical protein
			transl_table	11
4301	4095	CDS
			locus_tag	Edafosvirus_48_5
			product	hypothetical protein
			transl_table	11
>Edafosvirus_49
3	3461	CDS
			locus_tag	Edafosvirus_49_1
			product	DNA mismatch repair ATPase MutS
			transl_table	11
3933	3463	CDS
			locus_tag	Edafosvirus_49_2
			product	hypothetical protein
			transl_table	11
>Edafosvirus_50
1	522	CDS
			locus_tag	Edafosvirus_50_1
			product	alpha/beta hydrolase
			transl_table	11
2574	508	CDS
			locus_tag	Edafosvirus_50_2
			product	hypothetical protein
			transl_table	11
2739	2864	CDS
			locus_tag	Edafosvirus_50_3
			product	hypothetical protein
			transl_table	11
3751	3320	CDS
			locus_tag	Edafosvirus_50_4
			product	hypothetical protein
			transl_table	11
3815	3928	CDS
			locus_tag	Edafosvirus_50_5
			product	hypothetical protein
			transl_table	11
>Edafosvirus_51
648	1	CDS
			locus_tag	Edafosvirus_51_1
			product	methionyl-tRNA synthetase
			transl_table	11
1595	825	CDS
			locus_tag	Edafosvirus_51_2
			product	PREDICTED: E3 ubiquitin-protein ligase RNF115 isoform X1
			transl_table	11
2087	1659	CDS
			locus_tag	Edafosvirus_51_3
			product	hypothetical protein
			transl_table	11
2737	2258	CDS
			locus_tag	Edafosvirus_51_4
			product	polyubiquitin
			transl_table	11
3824	2787	CDS
			locus_tag	Edafosvirus_51_5
			product	hypothetical protein AK812_SmicGene1814
			transl_table	11
>Edafosvirus_52
303	73	CDS
			locus_tag	Edafosvirus_52_1
			product	hypothetical protein
			transl_table	11
437	1381	CDS
			locus_tag	Edafosvirus_52_2
			product	hypothetical protein
			transl_table	11
1426	2106	CDS
			locus_tag	Edafosvirus_52_3
			product	PREDICTED: lathosterol oxidase-like
			transl_table	11
2632	2123	CDS
			locus_tag	Edafosvirus_52_4
			product	hypothetical protein
			transl_table	11
3387	3166	CDS
			locus_tag	Edafosvirus_52_5
			product	hypothetical protein
			transl_table	11
3431	3778	CDS
			locus_tag	Edafosvirus_52_6
			product	hypothetical protein
			transl_table	11
>Edafosvirus_53
1465	158	CDS
			locus_tag	Edafosvirus_53_1
			product	glutamyl-tRNA synthetase
			transl_table	11
2935	1970	CDS
			locus_tag	Edafosvirus_53_2
			product	PREDICTED: dnaJ homolog subfamily B member 1
			transl_table	11
3686	3054	CDS
			locus_tag	Edafosvirus_53_3
			product	Clp protease
			transl_table	11
>Edafosvirus_54
964	179	CDS
			locus_tag	Edafosvirus_54_1
			product	hypothetical protein
			transl_table	11
1079	1960	CDS
			locus_tag	Edafosvirus_54_2
			product	DNA-3-methyladenine glycosylase 2 family protein
			transl_table	11
2047	2949	CDS
			locus_tag	Edafosvirus_54_3
			product	hypothetical protein PBRA_003026
			transl_table	11
3185	3358	CDS
			locus_tag	Edafosvirus_54_4
			product	hypothetical protein
			transl_table	11
3334	3645	CDS
			locus_tag	Edafosvirus_54_5
			product	hypothetical protein
			transl_table	11
>Edafosvirus_55
1	666	CDS
			locus_tag	Edafosvirus_55_1
			product	hypothetical protein
			transl_table	11
741	1577	CDS
			locus_tag	Edafosvirus_55_2
			product	hypothetical protein
			transl_table	11
1735	3012	CDS
			locus_tag	Edafosvirus_55_3
			product	TNF receptor-associated factor 2
			transl_table	11
3599	3021	CDS
			locus_tag	Edafosvirus_55_4
			product	Ras-related protein Rab-1A
			transl_table	11
>Edafosvirus_56
694	1740	CDS
			locus_tag	Edafosvirus_56_1
			product	hypothetical protein Klosneuvirus_2_97
			transl_table	11
1942	2820	CDS
			locus_tag	Edafosvirus_56_2
			product	hypothetical protein BMW23_0890
			transl_table	11
2891	3520	CDS
			locus_tag	Edafosvirus_56_3
			product	UvrD/REP helicase family protein
			transl_table	11
>Edafosvirus_57
1	705	CDS
			locus_tag	Edafosvirus_57_1
			product	DnaJ domain protein
			transl_table	11
3494	720	CDS
			locus_tag	Edafosvirus_57_2
			product	hypothetical protein Hokovirus_1_183
			transl_table	11
>Edafosvirus_58
2	376	CDS
			locus_tag	Edafosvirus_58_1
			product	hypothetical protein Catovirus_2_211
			transl_table	11
813	1787	CDS
			locus_tag	Edafosvirus_58_2
			product	putative ankyrin repeat protein
			transl_table	11
1735	1929	CDS
			locus_tag	Edafosvirus_58_3
			product	hypothetical protein
			transl_table	11
2764	1910	CDS
			locus_tag	Edafosvirus_58_4
			product	hypothetical protein
			transl_table	11
3285	3422	CDS
			locus_tag	Edafosvirus_58_5
			product	ribonuclease III
			transl_table	11
>Edafosvirus_59
2	1519	CDS
			locus_tag	Edafosvirus_59_1
			product	ankyrin repeat protein
			transl_table	11
1937	1512	CDS
			locus_tag	Edafosvirus_59_2
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
3288	1981	CDS
			locus_tag	Edafosvirus_59_3
			product	hypothetical protein Indivirus_3_65
			transl_table	11
>Edafosvirus_60
50	859	CDS
			locus_tag	Edafosvirus_60_1
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
1119	907	CDS
			locus_tag	Edafosvirus_60_2
			product	conserved hypothetical protein
			transl_table	11
3053	1176	CDS
			locus_tag	Edafosvirus_60_3
			product	DNA topoisomerase IA
			transl_table	11
>Edafosvirus_61
295	2	CDS
			locus_tag	Edafosvirus_61_1
			product	hypothetical protein
			transl_table	11
459	1586	CDS
			locus_tag	Edafosvirus_61_2
			product	putative orfan
			transl_table	11
2292	1579	CDS
			locus_tag	Edafosvirus_61_3
			product	RNase adaptor protein
			transl_table	11
2929	3036	CDS
			locus_tag	Edafosvirus_61_4
			product	hypothetical protein
			transl_table	11
>Edafosvirus_62
3	1772	CDS
			locus_tag	Edafosvirus_62_1
			product	DEAD/SNF2-like helicase
			transl_table	11
2433	1774	CDS
			locus_tag	Edafosvirus_62_2
			product	hypothetical protein
			transl_table	11
2859	2548	CDS
			locus_tag	Edafosvirus_62_3
			product	hypothetical protein
			transl_table	11
>Edafosvirus_63
315	31	CDS
			locus_tag	Edafosvirus_63_1
			product	hypothetical protein
			transl_table	11
960	409	CDS
			locus_tag	Edafosvirus_63_2
			product	molecular chaperone DnaJ
			transl_table	11
1730	987	CDS
			locus_tag	Edafosvirus_63_3
			product	dual specificity phosphatase
			transl_table	11
2841	1813	CDS
			locus_tag	Edafosvirus_63_4
			product	transmembrane protein 184C-like
			transl_table	11
>Edafosvirus_64
2	832	CDS
			locus_tag	Edafosvirus_64_1
			product	replication factor C small subunit
			transl_table	11
1003	1800	CDS
			locus_tag	Edafosvirus_64_2
			product	translation initiation factor 4E
			transl_table	11
2725	1898	CDS
			locus_tag	Edafosvirus_64_3
			product	proliferating cell nuclear antigen
			transl_table	11
>Edafosvirus_65
1434	1	CDS
			locus_tag	Edafosvirus_65_1
			product	hypothetical protein BSLG_06661
			transl_table	11
1783	1562	CDS
			locus_tag	Edafosvirus_65_2
			product	hypothetical protein
			transl_table	11
2385	2549	CDS
			locus_tag	Edafosvirus_65_3
			product	hypothetical protein
			transl_table	11
>Edafosvirus_66
1	480	CDS
			locus_tag	Edafosvirus_66_1
			product	hypothetical protein
			transl_table	11
2188	488	CDS
			locus_tag	Edafosvirus_66_2
			product	hypothetical protein
			transl_table	11
2526	2257	CDS
			locus_tag	Edafosvirus_66_3
			product	hypothetical protein
			transl_table	11
