>Satyrvirus_1
9297	9226	tRNA	Satyrvirus_tRNA_1
			product	tRNA-Trp(CCA)
9369	9299	tRNA	Satyrvirus_tRNA_2
			product	tRNA-Cys(GCA)
9443	9371	tRNA	Satyrvirus_tRNA_3
			product	tRNA-Arg(TCT)
22538	22466	tRNA	Satyrvirus_tRNA_4
			product	tRNA-Tyr(GTA)
721	2	CDS
			locus_tag	Satyrvirus_1_1
			product	putative virion-associated membrane protein
			transl_table	11
1376	759	CDS
			locus_tag	Satyrvirus_1_2
			product	hypothetical protein
			transl_table	11
1746	2819	CDS
			locus_tag	Satyrvirus_1_3
			product	alpha/beta hydrolase
			transl_table	11
3346	2822	CDS
			locus_tag	Satyrvirus_1_4
			product	hypothetical protein
			transl_table	11
3374	6682	CDS
			locus_tag	Satyrvirus_1_5
			product	copper oxidase
			transl_table	11
6727	8187	CDS
			locus_tag	Satyrvirus_1_6
			product	AAA family ATPase
			transl_table	11
9050	8205	CDS
			locus_tag	Satyrvirus_1_7
			product	caspase domain protein
			transl_table	11
9510	11858	CDS
			locus_tag	Satyrvirus_1_8
			product	hypothetical protein COA99_15150, partial
			transl_table	11
12367	11855	CDS
			locus_tag	Satyrvirus_1_9
			product	hypothetical protein Catovirus_1_791
			transl_table	11
12507	12926	CDS
			locus_tag	Satyrvirus_1_10
			product	hypothetical protein
			transl_table	11
13006	13992	CDS
			locus_tag	Satyrvirus_1_11
			product	hypothetical protein
			transl_table	11
14144	14629	CDS
			locus_tag	Satyrvirus_1_12
			product	hypothetical protein
			transl_table	11
16502	14616	CDS
			locus_tag	Satyrvirus_1_13
			product	hypothetical protein CBB75_08945
			transl_table	11
16843	17724	CDS
			locus_tag	Satyrvirus_1_14
			product	hypothetical protein
			transl_table	11
17813	19075	CDS
			locus_tag	Satyrvirus_1_15
			product	hypothetical protein ARMSODRAFT_953639
			transl_table	11
19136	19531	CDS
			locus_tag	Satyrvirus_1_16
			product	hypothetical protein
			transl_table	11
19728	20186	CDS
			locus_tag	Satyrvirus_1_17
			product	mg735 protein
			transl_table	11
20176	20901	CDS
			locus_tag	Satyrvirus_1_18
			product	glycosyltransferase family 25
			transl_table	11
20942	21784	CDS
			locus_tag	Satyrvirus_1_19
			product	stomatin family protein/prohibitin-family membrane protease subunit ybbk
			transl_table	11
22371	21781	CDS
			locus_tag	Satyrvirus_1_20
			product	hypothetical protein BMW23_0385
			transl_table	11
22634	22888	CDS
			locus_tag	Satyrvirus_1_21
			product	hypothetical protein
			transl_table	11
23922	22906	CDS
			locus_tag	Satyrvirus_1_22
			product	hypothetical protein
			transl_table	11
25962	23962	CDS
			locus_tag	Satyrvirus_1_23
			product	phosphoinositide 3-kinase
			transl_table	11
26215	27921	CDS
			locus_tag	Satyrvirus_1_24
			product	hypothetical protein
			transl_table	11
29330	28014	CDS
			locus_tag	Satyrvirus_1_25
			product	subtilase family serine protease
			transl_table	11
29649	31598	CDS
			locus_tag	Satyrvirus_1_26
			product	hypothetical protein
			transl_table	11
31631	31846	CDS
			locus_tag	Satyrvirus_1_27
			product	hypothetical protein
			transl_table	11
33589	31880	CDS
			locus_tag	Satyrvirus_1_28
			product	hypothetical protein AUG87_02985
			transl_table	11
34515	33610	CDS
			locus_tag	Satyrvirus_1_29
			product	putative orfan
			transl_table	11
34596	34886	CDS
			locus_tag	Satyrvirus_1_30
			product	hypothetical protein
			transl_table	11
36020	34893	CDS
			locus_tag	Satyrvirus_1_31
			product	phospholipase
			transl_table	11
36468	36073	CDS
			locus_tag	Satyrvirus_1_32
			product	hypothetical protein
			transl_table	11
37614	36628	CDS
			locus_tag	Satyrvirus_1_33
			product	mg747 protein
			transl_table	11
37704	38327	CDS
			locus_tag	Satyrvirus_1_34
			product	tyrosine-protein phosphatase
			transl_table	11
38383	39264	CDS
			locus_tag	Satyrvirus_1_35
			product	mg749 protein
			transl_table	11
39582	40532	CDS
			locus_tag	Satyrvirus_1_36
			product	hypothetical protein
			transl_table	11
40618	42045	CDS
			locus_tag	Satyrvirus_1_37
			product	putative GTP-binding translation elongation/initiation factor
			transl_table	11
42391	42059	CDS
			locus_tag	Satyrvirus_1_38
			product	hypothetical protein
			transl_table	11
42535	42993	CDS
			locus_tag	Satyrvirus_1_39
			product	putative GTP-binding protein
			transl_table	11
43066	43782	CDS
			locus_tag	Satyrvirus_1_40
			product	hypothetical protein
			transl_table	11
43871	45148	CDS
			locus_tag	Satyrvirus_1_41
			product	mimivirus translation initiation factor 2 gamma subunit
			transl_table	11
45200	47065	CDS
			locus_tag	Satyrvirus_1_42
			product	hypothetical protein
			transl_table	11
47112	47606	CDS
			locus_tag	Satyrvirus_1_43
			product	hypothetical protein
			transl_table	11
47678	48145	CDS
			locus_tag	Satyrvirus_1_44
			product	hypothetical protein
			transl_table	11
48105	48950	CDS
			locus_tag	Satyrvirus_1_45
			product	hypothetical protein
			transl_table	11
49183	48953	CDS
			locus_tag	Satyrvirus_1_46
			product	hypothetical protein
			transl_table	11
50231	49296	CDS
			locus_tag	Satyrvirus_1_47
			product	hypothetical protein
			transl_table	11
50884	50279	CDS
			locus_tag	Satyrvirus_1_48
			product	putative transmembrane protein
			transl_table	11
52280	50964	CDS
			locus_tag	Satyrvirus_1_49
			product	hypothetical protein
			transl_table	11
53036	52500	CDS
			locus_tag	Satyrvirus_1_50
			product	nucleic acid-binding protein
			transl_table	11
53362	53180	CDS
			locus_tag	Satyrvirus_1_51
			product	hypothetical protein
			transl_table	11
53552	53394	CDS
			locus_tag	Satyrvirus_1_52
			product	hypothetical protein
			transl_table	11
53812	54126	CDS
			locus_tag	Satyrvirus_1_53
			product	hypothetical protein
			transl_table	11
54314	54204	CDS
			locus_tag	Satyrvirus_1_54
			product	hypothetical protein
			transl_table	11
58685	54435	CDS
			locus_tag	Satyrvirus_1_55
			product	putative FtsJ-like methyltransferase
			transl_table	11
58710	60023	CDS
			locus_tag	Satyrvirus_1_56
			product	hypothetical protein
			transl_table	11
60123	60752	CDS
			locus_tag	Satyrvirus_1_57
			product	hypothetical protein
			transl_table	11
60840	62057	CDS
			locus_tag	Satyrvirus_1_58
			product	hypothetical protein
			transl_table	11
62737	62060	CDS
			locus_tag	Satyrvirus_1_59
			product	hypothetical protein
			transl_table	11
62853	64352	CDS
			locus_tag	Satyrvirus_1_60
			product	hypothetical protein
			transl_table	11
65626	67551	CDS
			locus_tag	Satyrvirus_1_61
			product	bifunctional phosphoribosyl-AMP cyclohydrolase/phosphoribosyl-ATP pyrophosphatase protein
			transl_table	11
67620	68231	CDS
			locus_tag	Satyrvirus_1_62
			product	hypothetical protein
			transl_table	11
68303	69559	CDS
			locus_tag	Satyrvirus_1_63
			product	hypothetical protein Klosneuvirus_2_130
			transl_table	11
70050	69541	CDS
			locus_tag	Satyrvirus_1_64
			product	hypothetical protein A3C46_03730
			transl_table	11
70129	70560	CDS
			locus_tag	Satyrvirus_1_65
			product	hypothetical protein
			transl_table	11
71042	70563	CDS
			locus_tag	Satyrvirus_1_66
			product	hypothetical protein
			transl_table	11
71081	72247	CDS
			locus_tag	Satyrvirus_1_67
			product	hypothetical protein
			transl_table	11
72276	72896	CDS
			locus_tag	Satyrvirus_1_68
			product	hypothetical protein
			transl_table	11
73517	72993	CDS
			locus_tag	Satyrvirus_1_69
			product	hypothetical protein
			transl_table	11
74338	73613	CDS
			locus_tag	Satyrvirus_1_70
			product	hypothetical protein
			transl_table	11
74507	75619	CDS
			locus_tag	Satyrvirus_1_71
			product	hypothetical protein
			transl_table	11
75752	76465	CDS
			locus_tag	Satyrvirus_1_72
			product	hypothetical protein MegaChil _gp0239
			transl_table	11
76473	76817	CDS
			locus_tag	Satyrvirus_1_73
			product	hypothetical protein
			transl_table	11
77540	76821	CDS
			locus_tag	Satyrvirus_1_74
			product	glycerophosphodiester phosphodiesterase
			transl_table	11
78379	77591	CDS
			locus_tag	Satyrvirus_1_75
			product	hypothetical protein
			transl_table	11
>Satyrvirus_2
3	821	CDS
			locus_tag	Satyrvirus_2_1
			product	A32-like packaging ATPase
			transl_table	11
1171	863	CDS
			locus_tag	Satyrvirus_2_2
			product	putative ORFan
			transl_table	11
2379	1213	CDS
			locus_tag	Satyrvirus_2_3
			product	hypothetical protein
			transl_table	11
2425	2997	CDS
			locus_tag	Satyrvirus_2_4
			product	HD phosphohydrolase
			transl_table	11
4265	2982	CDS
			locus_tag	Satyrvirus_2_5
			product	hypothetical protein
			transl_table	11
5004	4519	CDS
			locus_tag	Satyrvirus_2_6
			product	hypothetical protein
			transl_table	11
5707	5087	CDS
			locus_tag	Satyrvirus_2_7
			product	hypothetical protein
			transl_table	11
5941	7713	CDS
			locus_tag	Satyrvirus_2_8
			product	capsid protein 1
			transl_table	11
9683	7794	CDS
			locus_tag	Satyrvirus_2_9
			product	hypothetical protein
			transl_table	11
10304	9816	CDS
			locus_tag	Satyrvirus_2_10
			product	DUF4241 domain-containing protein
			transl_table	11
10401	10655	CDS
			locus_tag	Satyrvirus_2_11
			product	hypothetical protein
			transl_table	11
10755	11549	CDS
			locus_tag	Satyrvirus_2_12
			product	putative orfan
			transl_table	11
11599	12051	CDS
			locus_tag	Satyrvirus_2_13
			product	hypothetical protein HIRU_S677
			transl_table	11
12103	13287	CDS
			locus_tag	Satyrvirus_2_14
			product	hypothetical protein
			transl_table	11
13320	13652	CDS
			locus_tag	Satyrvirus_2_15
			product	hypothetical protein
			transl_table	11
14404	13649	CDS
			locus_tag	Satyrvirus_2_16
			product	hypothetical protein
			transl_table	11
14525	14782	CDS
			locus_tag	Satyrvirus_2_17
			product	hypothetical protein
			transl_table	11
14821	15135	CDS
			locus_tag	Satyrvirus_2_18
			product	hypothetical protein
			transl_table	11
15201	15407	CDS
			locus_tag	Satyrvirus_2_19
			product	hypothetical protein
			transl_table	11
15915	15706	CDS
			locus_tag	Satyrvirus_2_20
			product	hypothetical protein
			transl_table	11
17022	15985	CDS
			locus_tag	Satyrvirus_2_21
			product	hypothetical protein
			transl_table	11
17109	17960	CDS
			locus_tag	Satyrvirus_2_22
			product	hypothetical protein
			transl_table	11
19474	17972	CDS
			locus_tag	Satyrvirus_2_23
			product	mg482 protein
			transl_table	11
19725	19588	CDS
			locus_tag	Satyrvirus_2_24
			product	hypothetical protein
			transl_table	11
19806	21707	CDS
			locus_tag	Satyrvirus_2_25
			product	putative core protein
			transl_table	11
22280	21744	CDS
			locus_tag	Satyrvirus_2_26
			product	hypothetical protein
			transl_table	11
22912	22328	CDS
			locus_tag	Satyrvirus_2_27
			product	hypothetical protein
			transl_table	11
23007	22900	CDS
			locus_tag	Satyrvirus_2_28
			product	hypothetical protein
			transl_table	11
23521	22991	CDS
			locus_tag	Satyrvirus_2_29
			product	hypothetical protein
			transl_table	11
23601	24020	CDS
			locus_tag	Satyrvirus_2_30
			product	YALI0E31691p
			transl_table	11
24077	25315	CDS
			locus_tag	Satyrvirus_2_31
			product	hypothetical protein
			transl_table	11
25410	25321	CDS
			locus_tag	Satyrvirus_2_32
			product	hypothetical protein
			transl_table	11
25491	26174	CDS
			locus_tag	Satyrvirus_2_33
			product	hypothetical protein
			transl_table	11
26226	27059	CDS
			locus_tag	Satyrvirus_2_34
			product	hydrolase family protein
			transl_table	11
27168	27965	CDS
			locus_tag	Satyrvirus_2_35
			product	hydrolase family protein
			transl_table	11
29958	27997	CDS
			locus_tag	Satyrvirus_2_36
			product	hypothetical protein
			transl_table	11
32465	30126	CDS
			locus_tag	Satyrvirus_2_37
			product	hypothetical protein
			transl_table	11
32558	33193	CDS
			locus_tag	Satyrvirus_2_38
			product	hypothetical protein
			transl_table	11
33261	33935	CDS
			locus_tag	Satyrvirus_2_39
			product	putative ORFan
			transl_table	11
35725	33977	CDS
			locus_tag	Satyrvirus_2_40
			product	putative serine/threonine-protein kinase
			transl_table	11
35834	36700	CDS
			locus_tag	Satyrvirus_2_41
			product	hypothetical protein
			transl_table	11
37891	36713	CDS
			locus_tag	Satyrvirus_2_42
			product	hypothetical protein
			transl_table	11
37970	39283	CDS
			locus_tag	Satyrvirus_2_43
			product	hypothetical protein
			transl_table	11
39376	40929	CDS
			locus_tag	Satyrvirus_2_44
			product	putative ATP-dependent RNA helicase
			transl_table	11
41493	41023	CDS
			locus_tag	Satyrvirus_2_45
			product	hypothetical protein
			transl_table	11
41769	41629	CDS
			locus_tag	Satyrvirus_2_46
			product	hypothetical protein
			transl_table	11
42497	42018	CDS
			locus_tag	Satyrvirus_2_47
			product	hypothetical protein
			transl_table	11
43223	42588	CDS
			locus_tag	Satyrvirus_2_48
			product	hypothetical protein
			transl_table	11
43310	43741	CDS
			locus_tag	Satyrvirus_2_49
			product	hypothetical protein
			transl_table	11
44688	43738	CDS
			locus_tag	Satyrvirus_2_50
			product	putative replication factor C small subunit
			transl_table	11
44819	45283	CDS
			locus_tag	Satyrvirus_2_51
			product	hypothetical protein
			transl_table	11
45292	45468	CDS
			locus_tag	Satyrvirus_2_52
			product	hypothetical protein
			transl_table	11
46492	45437	CDS
			locus_tag	Satyrvirus_2_53
			product	hypothetical protein
			transl_table	11
46622	47989	CDS
			locus_tag	Satyrvirus_2_54
			product	putative HD domain-containing protein
			transl_table	11
48248	47967	CDS
			locus_tag	Satyrvirus_2_55
			product	Cystathionine beta-synthase, core
			transl_table	11
48725	48303	CDS
			locus_tag	Satyrvirus_2_56
			product	hypothetical protein
			transl_table	11
48959	49597	CDS
			locus_tag	Satyrvirus_2_57
			product	putative heat shock 70 kDa protein
			transl_table	11
49921	51234	CDS
			locus_tag	Satyrvirus_2_58
			product	HSP70-like protein
			transl_table	11
51299	51634	CDS
			locus_tag	Satyrvirus_2_59
			product	hypothetical protein
			transl_table	11
51682	51918	CDS
			locus_tag	Satyrvirus_2_60
			product	hypothetical protein
			transl_table	11
51980	52195	CDS
			locus_tag	Satyrvirus_2_61
			product	hypothetical protein
			transl_table	11
52233	53009	CDS
			locus_tag	Satyrvirus_2_62
			product	uncharacterized protein LOC110916189 isoform X2
			transl_table	11
53276	53178	CDS
			locus_tag	Satyrvirus_2_63
			product	hypothetical protein
			transl_table	11
53296	54147	CDS
			locus_tag	Satyrvirus_2_64
			product	hypothetical protein
			transl_table	11
54484	54630	CDS
			locus_tag	Satyrvirus_2_65
			product	hypothetical protein
			transl_table	11
56194	54593	CDS
			locus_tag	Satyrvirus_2_66
			product	HNH endonuclease
			transl_table	11
56763	56515	CDS
			locus_tag	Satyrvirus_2_67
			product	ubiquitin-domain-containing protein
			transl_table	11
56859	57005	CDS
			locus_tag	Satyrvirus_2_68
			product	hypothetical protein
			transl_table	11
57106	57549	CDS
			locus_tag	Satyrvirus_2_69
			product	hypothetical protein
			transl_table	11
57626	58519	CDS
			locus_tag	Satyrvirus_2_70
			product	hypothetical protein
			transl_table	11
58644	59702	CDS
			locus_tag	Satyrvirus_2_71
			product	putative ankyrin repeat protein
			transl_table	11
59723	60295	CDS
			locus_tag	Satyrvirus_2_72
			product	hypothetical protein
			transl_table	11
60690	60301	CDS
			locus_tag	Satyrvirus_2_73
			product	hypothetical protein
			transl_table	11
60858	61550	CDS
			locus_tag	Satyrvirus_2_74
			product	hypothetical protein
			transl_table	11
61618	62316	CDS
			locus_tag	Satyrvirus_2_75
			product	hypothetical protein
			transl_table	11
64065	63610	CDS
			locus_tag	Satyrvirus_2_76
			product	putative ORFan
			transl_table	11
64255	65559	CDS
			locus_tag	Satyrvirus_2_77
			product	endonuclease of the XPG family
			transl_table	11
65962	65573	CDS
			locus_tag	Satyrvirus_2_78
			product	hypothetical protein
			transl_table	11
67056	66004	CDS
			locus_tag	Satyrvirus_2_79
			product	methyltransferase
			transl_table	11
67465	67115	CDS
			locus_tag	Satyrvirus_2_80
			product	mRNA capping enzyme
			transl_table	11
>Satyrvirus_3
322	2	CDS
			locus_tag	Satyrvirus_3_1
			product	hypothetical protein
			transl_table	11
2314	392	CDS
			locus_tag	Satyrvirus_3_2
			product	NAD-dependent DNA ligase
			transl_table	11
2444	3670	CDS
			locus_tag	Satyrvirus_3_3
			product	hypothetical protein
			transl_table	11
4190	3687	CDS
			locus_tag	Satyrvirus_3_4
			product	putative ORFan
			transl_table	11
5443	4214	CDS
			locus_tag	Satyrvirus_3_5
			product	putative serine threonine protein kinase
			transl_table	11
5473	6000	CDS
			locus_tag	Satyrvirus_3_6
			product	putative endo/excinuclease amino terminal domain protein
			transl_table	11
6362	5961	CDS
			locus_tag	Satyrvirus_3_7
			product	hypothetical protein mc_146
			transl_table	11
7926	6409	CDS
			locus_tag	Satyrvirus_3_8
			product	poly(ADP-ribose) polymerase
			transl_table	11
8801	7992	CDS
			locus_tag	Satyrvirus_3_9
			product	ribonuclease H
			transl_table	11
9692	8916	CDS
			locus_tag	Satyrvirus_3_10
			product	putative low complexity protein
			transl_table	11
10643	9729	CDS
			locus_tag	Satyrvirus_3_11
			product	putative endonuclease 4
			transl_table	11
10735	11220	CDS
			locus_tag	Satyrvirus_3_12
			product	putative ORFan
			transl_table	11
11289	20402	CDS
			locus_tag	Satyrvirus_3_13
			product	kinesin-like protein
			transl_table	11
20390	21787	CDS
			locus_tag	Satyrvirus_3_14
			product	peptidase C19 subfamily protein
			transl_table	11
22286	21825	CDS
			locus_tag	Satyrvirus_3_15
			product	hypothetical protein
			transl_table	11
24123	22339	CDS
			locus_tag	Satyrvirus_3_16
			product	hypothetical protein
			transl_table	11
26559	24172	CDS
			locus_tag	Satyrvirus_3_17
			product	hypothetical protein
			transl_table	11
26691	27755	CDS
			locus_tag	Satyrvirus_3_18
			product	putative orfan
			transl_table	11
29446	27773	CDS
			locus_tag	Satyrvirus_3_19
			product	putative ATP-dependent RNA helicase
			transl_table	11
30360	29518	CDS
			locus_tag	Satyrvirus_3_20
			product	metallophosphoesterase-like protein
			transl_table	11
30482	32170	CDS
			locus_tag	Satyrvirus_3_21
			product	hypothetical protein
			transl_table	11
32218	33333	CDS
			locus_tag	Satyrvirus_3_22
			product	putative replication factor c small subunit
			transl_table	11
33437	33742	CDS
			locus_tag	Satyrvirus_3_23
			product	hypothetical protein
			transl_table	11
33929	34198	CDS
			locus_tag	Satyrvirus_3_24
			product	hypothetical protein
			transl_table	11
36640	34193	CDS
			locus_tag	Satyrvirus_3_25
			product	putative FtsJ-like methyltransferase
			transl_table	11
36785	37315	CDS
			locus_tag	Satyrvirus_3_26
			product	hypothetical protein glt_00374
			transl_table	11
37730	37320	CDS
			locus_tag	Satyrvirus_3_27
			product	hypothetical protein
			transl_table	11
38202	37789	CDS
			locus_tag	Satyrvirus_3_28
			product	hypothetical protein
			transl_table	11
38325	38591	CDS
			locus_tag	Satyrvirus_3_29
			product	hypothetical protein
			transl_table	11
38759	38980	CDS
			locus_tag	Satyrvirus_3_30
			product	hypothetical protein
			transl_table	11
39041	39562	CDS
			locus_tag	Satyrvirus_3_31
			product	hypothetical protein
			transl_table	11
39810	41384	CDS
			locus_tag	Satyrvirus_3_32
			product	putative serine threonine-protein kinase
			transl_table	11
42206	41388	CDS
			locus_tag	Satyrvirus_3_33
			product	hypothetical protein
			transl_table	11
42339	43295	CDS
			locus_tag	Satyrvirus_3_34
			product	putative zinc metalloproteinase
			transl_table	11
>Satyrvirus_4
1571	1897	CDS
			locus_tag	Satyrvirus_4_1
			product	hypothetical protein
			transl_table	11
1948	2484	CDS
			locus_tag	Satyrvirus_4_2
			product	hypothetical protein
			transl_table	11
2873	2481	CDS
			locus_tag	Satyrvirus_4_3
			product	hypothetical protein
			transl_table	11
3028	3603	CDS
			locus_tag	Satyrvirus_4_4
			product	hypothetical protein
			transl_table	11
3651	4574	CDS
			locus_tag	Satyrvirus_4_5
			product	putative ankyrin repeat protein
			transl_table	11
5369	4584	CDS
			locus_tag	Satyrvirus_4_6
			product	hypothetical protein
			transl_table	11
5499	6125	CDS
			locus_tag	Satyrvirus_4_7
			product	hypothetical protein
			transl_table	11
6308	6958	CDS
			locus_tag	Satyrvirus_4_8
			product	putative orfan
			transl_table	11
7042	7707	CDS
			locus_tag	Satyrvirus_4_9
			product	putative orfan
			transl_table	11
7742	8242	CDS
			locus_tag	Satyrvirus_4_10
			product	mg583 protein
			transl_table	11
8307	13589	CDS
			locus_tag	Satyrvirus_4_11
			product	B-family DNA polymerase, partial
			transl_table	11
14603	13614	CDS
			locus_tag	Satyrvirus_4_12
			product	IMV envelope protein
			transl_table	11
14687	15643	CDS
			locus_tag	Satyrvirus_4_13
			product	lipase family protein
			transl_table	11
16843	15650	CDS
			locus_tag	Satyrvirus_4_14
			product	hypothetical protein
			transl_table	11
16939	17523	CDS
			locus_tag	Satyrvirus_4_15
			product	putative metallopeptidase WLM domain protein
			transl_table	11
18233	17520	CDS
			locus_tag	Satyrvirus_4_16
			product	putative orfan
			transl_table	11
18338	24397	CDS
			locus_tag	Satyrvirus_4_17
			product	hypothetical protein
			transl_table	11
24505	25293	CDS
			locus_tag	Satyrvirus_4_18
			product	hypothetical protein
			transl_table	11
25321	25518	CDS
			locus_tag	Satyrvirus_4_19
			product	hypothetical protein
			transl_table	11
25562	26077	CDS
			locus_tag	Satyrvirus_4_20
			product	hypothetical protein
			transl_table	11
26131	26613	CDS
			locus_tag	Satyrvirus_4_21
			product	hypothetical protein
			transl_table	11
27383	26616	CDS
			locus_tag	Satyrvirus_4_22
			product	hypothetical protein
			transl_table	11
27403	27630	CDS
			locus_tag	Satyrvirus_4_23
			product	putative ORFan
			transl_table	11
27915	28016	CDS
			locus_tag	Satyrvirus_4_24
			product	hypothetical protein
			transl_table	11
27995	28681	CDS
			locus_tag	Satyrvirus_4_25
			product	putative ORFan
			transl_table	11
28686	29714	CDS
			locus_tag	Satyrvirus_4_26
			product	hypothetical protein
			transl_table	11
29751	30893	CDS
			locus_tag	Satyrvirus_4_27
			product	hypothetical protein
			transl_table	11
30949	31716	CDS
			locus_tag	Satyrvirus_4_28
			product	hypothetical protein
			transl_table	11
31787	34534	CDS
			locus_tag	Satyrvirus_4_29
			product	hypothetical protein
			transl_table	11
34623	37475	CDS
			locus_tag	Satyrvirus_4_30
			product	hypothetical protein
			transl_table	11
38140	37532	CDS
			locus_tag	Satyrvirus_4_31
			product	hypothetical protein
			transl_table	11
39103	38231	CDS
			locus_tag	Satyrvirus_4_32
			product	protein phosphatase 2c-like protein
			transl_table	11
>Satyrvirus_5
127	2	CDS
			locus_tag	Satyrvirus_5_1
			product	repeat protein
			transl_table	11
1045	173	CDS
			locus_tag	Satyrvirus_5_2
			product	ankyrin repeat protein
			transl_table	11
1849	1091	CDS
			locus_tag	Satyrvirus_5_3
			product	repeat protein
			transl_table	11
2924	1896	CDS
			locus_tag	Satyrvirus_5_4
			product	putative ankyrin repeat protein
			transl_table	11
3902	2970	CDS
			locus_tag	Satyrvirus_5_5
			product	putative ankyrin repeat protein
			transl_table	11
5070	3949	CDS
			locus_tag	Satyrvirus_5_6
			product	ankyrin repeat protein
			transl_table	11
6194	5091	CDS
			locus_tag	Satyrvirus_5_7
			product	ankyrin repeat protein
			transl_table	11
7277	6240	CDS
			locus_tag	Satyrvirus_5_8
			product	ankyrin repeat protein
			transl_table	11
8257	7298	CDS
			locus_tag	Satyrvirus_5_9
			product	ankyrin repeat protein
			transl_table	11
9328	8303	CDS
			locus_tag	Satyrvirus_5_10
			product	ankyrin repeat protein
			transl_table	11
10488	9349	CDS
			locus_tag	Satyrvirus_5_11
			product	repeat protein
			transl_table	11
10637	10530	CDS
			locus_tag	Satyrvirus_5_12
			product	hypothetical protein
			transl_table	11
10999	10637	CDS
			locus_tag	Satyrvirus_5_13
			product	hypothetical protein
			transl_table	11
11888	11133	CDS
			locus_tag	Satyrvirus_5_14
			product	hypothetical protein
			transl_table	11
13552	13367	CDS
			locus_tag	Satyrvirus_5_15
			product	hypothetical protein
			transl_table	11
15657	15746	CDS
			locus_tag	Satyrvirus_5_16
			product	hypothetical protein
			transl_table	11
16646	16735	CDS
			locus_tag	Satyrvirus_5_17
			product	hypothetical protein
			transl_table	11
18154	17354	CDS
			locus_tag	Satyrvirus_5_18
			product	hypothetical protein CE11_01208
			transl_table	11
18347	18685	CDS
			locus_tag	Satyrvirus_5_19
			product	hypothetical protein
			transl_table	11
19286	18714	CDS
			locus_tag	Satyrvirus_5_20
			product	hypothetical protein
			transl_table	11
19415	20077	CDS
			locus_tag	Satyrvirus_5_21
			product	hypothetical protein
			transl_table	11
22397	20097	CDS
			locus_tag	Satyrvirus_5_22
			product	hypothetical protein Indivirus_2_53
			transl_table	11
22803	22492	CDS
			locus_tag	Satyrvirus_5_23
			product	hypothetical protein
			transl_table	11
24452	22869	CDS
			locus_tag	Satyrvirus_5_24
			product	D5-ATPase-helicase
			transl_table	11
25596	24535	CDS
			locus_tag	Satyrvirus_5_25
			product	hypothetical protein
			transl_table	11
26844	25666	CDS
			locus_tag	Satyrvirus_5_26
			product	D5-ATPase-helicase
			transl_table	11
26967	27545	CDS
			locus_tag	Satyrvirus_5_27
			product	hypothetical protein
			transl_table	11
27632	28009	CDS
			locus_tag	Satyrvirus_5_28
			product	hypothetical protein
			transl_table	11
28115	29620	CDS
			locus_tag	Satyrvirus_5_29
			product	hypothetical protein HMPREF1093_00415
			transl_table	11
29883	30863	CDS
			locus_tag	Satyrvirus_5_30
			product	methyltransferase domain
			transl_table	11
30902	33001	CDS
			locus_tag	Satyrvirus_5_31
			product	glutathione synthase
			transl_table	11
33007	33876	CDS
			locus_tag	Satyrvirus_5_32
			product	hypothetical protein Catovirus_1_263
			transl_table	11
34422	33922	CDS
			locus_tag	Satyrvirus_5_33
			product	hypothetical protein
			transl_table	11
>Satyrvirus_6
263	3	CDS
			locus_tag	Satyrvirus_6_1
			product	hypothetical protein
			transl_table	11
378	569	CDS
			locus_tag	Satyrvirus_6_2
			product	hypothetical protein
			transl_table	11
632	1096	CDS
			locus_tag	Satyrvirus_6_3
			product	hypothetical protein
			transl_table	11
1506	1348	CDS
			locus_tag	Satyrvirus_6_4
			product	hypothetical protein
			transl_table	11
1490	2314	CDS
			locus_tag	Satyrvirus_6_5
			product	hypothetical protein BQ9231_00352
			transl_table	11
2409	2867	CDS
			locus_tag	Satyrvirus_6_6
			product	hypothetical protein
			transl_table	11
2885	3748	CDS
			locus_tag	Satyrvirus_6_7
			product	hypothetical protein
			transl_table	11
5099	5326	CDS
			locus_tag	Satyrvirus_6_8
			product	hypothetical protein
			transl_table	11
5310	5840	CDS
			locus_tag	Satyrvirus_6_9
			product	FkbM family methyltransferase
			transl_table	11
5912	6445	CDS
			locus_tag	Satyrvirus_6_10
			product	hypothetical protein
			transl_table	11
6498	6911	CDS
			locus_tag	Satyrvirus_6_11
			product	hypothetical protein
			transl_table	11
7196	6915	CDS
			locus_tag	Satyrvirus_6_12
			product	hypothetical protein
			transl_table	11
8551	7250	CDS
			locus_tag	Satyrvirus_6_13
			product	putative serine/threonine-protein kinase
			transl_table	11
9860	8595	CDS
			locus_tag	Satyrvirus_6_14
			product	protein Shroom2-like
			transl_table	11
10433	9882	CDS
			locus_tag	Satyrvirus_6_15
			product	hypothetical protein
			transl_table	11
10528	11385	CDS
			locus_tag	Satyrvirus_6_16
			product	hypothetical protein
			transl_table	11
11440	12219	CDS
			locus_tag	Satyrvirus_6_17
			product	hypothetical protein
			transl_table	11
13074	12328	CDS
			locus_tag	Satyrvirus_6_18
			product	PREDICTED: uncharacterized protein LOC109487136 isoform X1
			transl_table	11
13155	14618	CDS
			locus_tag	Satyrvirus_6_19
			product	serine/threonine protein kinase
			transl_table	11
15873	15514	CDS
			locus_tag	Satyrvirus_6_20
			product	hypothetical protein
			transl_table	11
17112	15979	CDS
			locus_tag	Satyrvirus_6_21
			product	gamma-butyrobetaine dioxygenase
			transl_table	11
18662	17181	CDS
			locus_tag	Satyrvirus_6_22
			product	dehydrogenase
			transl_table	11
20686	18671	CDS
			locus_tag	Satyrvirus_6_23
			product	mRNA capping enzyme
			transl_table	11
20778	21395	CDS
			locus_tag	Satyrvirus_6_24
			product	hypothetical protein
			transl_table	11
21450	22319	CDS
			locus_tag	Satyrvirus_6_25
			product	hypothetical protein
			transl_table	11
22539	22324	CDS
			locus_tag	Satyrvirus_6_26
			product	hypothetical protein
			transl_table	11
23401	22841	CDS
			locus_tag	Satyrvirus_6_27
			product	hypothetical protein
			transl_table	11
23594	24469	CDS
			locus_tag	Satyrvirus_6_28
			product	hypothetical protein
			transl_table	11
25171	24431	CDS
			locus_tag	Satyrvirus_6_29
			product	hypothetical protein
			transl_table	11
26214	25282	CDS
			locus_tag	Satyrvirus_6_30
			product	hypothetical protein
			transl_table	11
26996	26292	CDS
			locus_tag	Satyrvirus_6_31
			product	hypothetical protein
			transl_table	11
27229	27513	CDS
			locus_tag	Satyrvirus_6_32
			product	hypothetical protein
			transl_table	11
28670	27510	CDS
			locus_tag	Satyrvirus_6_33
			product	hypothetical protein
			transl_table	11
28785	29597	CDS
			locus_tag	Satyrvirus_6_34
			product	hypothetical protein
			transl_table	11
30275	29592	CDS
			locus_tag	Satyrvirus_6_35
			product	hypothetical protein
			transl_table	11
31332	30325	CDS
			locus_tag	Satyrvirus_6_36
			product	hypothetical protein
			transl_table	11
31463	31900	CDS
			locus_tag	Satyrvirus_6_37
			product	hypothetical protein glt_00881
			transl_table	11
32056	32169	CDS
			locus_tag	Satyrvirus_6_38
			product	hypothetical protein
			transl_table	11
>Satyrvirus_7
97	513	CDS
			locus_tag	Satyrvirus_7_1
			product	hypothetical protein
			transl_table	11
577	2916	CDS
			locus_tag	Satyrvirus_7_2
			product	PREDICTED: poly
			transl_table	11
5370	5750	CDS
			locus_tag	Satyrvirus_7_3
			product	hypothetical protein
			transl_table	11
6054	7640	CDS
			locus_tag	Satyrvirus_7_4
			product	serpin
			transl_table	11
8539	7667	CDS
			locus_tag	Satyrvirus_7_5
			product	antirepressor
			transl_table	11
8950	8588	CDS
			locus_tag	Satyrvirus_7_6
			product	hypothetical protein
			transl_table	11
9028	9738	CDS
			locus_tag	Satyrvirus_7_7
			product	hypothetical protein
			transl_table	11
9795	10130	CDS
			locus_tag	Satyrvirus_7_8
			product	hypothetical protein
			transl_table	11
10747	10139	CDS
			locus_tag	Satyrvirus_7_9
			product	START domain containing protein
			transl_table	11
10867	11577	CDS
			locus_tag	Satyrvirus_7_10
			product	hypothetical protein Catovirus_1_259
			transl_table	11
11824	12000	CDS
			locus_tag	Satyrvirus_7_11
			product	hypothetical protein
			transl_table	11
12267	12049	CDS
			locus_tag	Satyrvirus_7_12
			product	hypothetical protein
			transl_table	11
12399	12875	CDS
			locus_tag	Satyrvirus_7_13
			product	hypothetical protein
			transl_table	11
13617	12877	CDS
			locus_tag	Satyrvirus_7_14
			product	hypothetical protein
			transl_table	11
14185	13787	CDS
			locus_tag	Satyrvirus_7_15
			product	hypothetical protein
			transl_table	11
15059	14304	CDS
			locus_tag	Satyrvirus_7_16
			product	hypothetical protein
			transl_table	11
15939	15097	CDS
			locus_tag	Satyrvirus_7_17
			product	hypothetical protein
			transl_table	11
16150	16680	CDS
			locus_tag	Satyrvirus_7_18
			product	uncharacterized protein LOC111117857
			transl_table	11
16928	16740	CDS
			locus_tag	Satyrvirus_7_19
			product	hypothetical protein
			transl_table	11
17061	17864	CDS
			locus_tag	Satyrvirus_7_20
			product	hypothetical protein
			transl_table	11
20589	18997	CDS
			locus_tag	Satyrvirus_7_21
			product	FAD-binding oxidoreductase
			transl_table	11
21255	20734	CDS
			locus_tag	Satyrvirus_7_22
			product	hypothetical protein
			transl_table	11
22632	21343	CDS
			locus_tag	Satyrvirus_7_23
			product	PREDICTED: protein unc-93 homolog A-like
			transl_table	11
22777	22968	CDS
			locus_tag	Satyrvirus_7_24
			product	hypothetical protein
			transl_table	11
23094	24329	CDS
			locus_tag	Satyrvirus_7_25
			product	hypothetical protein
			transl_table	11
24388	25260	CDS
			locus_tag	Satyrvirus_7_26
			product	hypothetical protein
			transl_table	11
25307	25822	CDS
			locus_tag	Satyrvirus_7_27
			product	hypothetical protein
			transl_table	11
25891	27135	CDS
			locus_tag	Satyrvirus_7_28
			product	hypothetical protein
			transl_table	11
27277	27714	CDS
			locus_tag	Satyrvirus_7_29
			product	hypothetical protein
			transl_table	11
28138	27719	CDS
			locus_tag	Satyrvirus_7_30
			product	mannose-6P isomerase
			transl_table	11
28190	28810	CDS
			locus_tag	Satyrvirus_7_31
			product	DUF1254 domain-containing protein
			transl_table	11
28887	29702	CDS
			locus_tag	Satyrvirus_7_32
			product	Hypothetical protein ZAZAV_284
			transl_table	11
29753	30400	CDS
			locus_tag	Satyrvirus_7_33
			product	putative ORFan
			transl_table	11
30862	30389	CDS
			locus_tag	Satyrvirus_7_34
			product	hypothetical protein
			transl_table	11
31357	31214	CDS
			locus_tag	Satyrvirus_7_35
			product	hypothetical protein
			transl_table	11
31948	31406	CDS
			locus_tag	Satyrvirus_7_36
			product	putative ORFan
			transl_table	11
>Satyrvirus_8
1	123	CDS
			locus_tag	Satyrvirus_8_1
			product	hypothetical protein
			transl_table	11
2721	2161	CDS
			locus_tag	Satyrvirus_8_2
			product	hypothetical protein
			transl_table	11
3158	2763	CDS
			locus_tag	Satyrvirus_8_3
			product	hypothetical protein
			transl_table	11
3279	3950	CDS
			locus_tag	Satyrvirus_8_4
			product	hypothetical protein
			transl_table	11
4058	4819	CDS
			locus_tag	Satyrvirus_8_5
			product	hypothetical protein Catovirus_1_263
			transl_table	11
5467	5805	CDS
			locus_tag	Satyrvirus_8_6
			product	hypothetical protein
			transl_table	11
6640	5870	CDS
			locus_tag	Satyrvirus_8_7
			product	hypothetical protein
			transl_table	11
7746	6766	CDS
			locus_tag	Satyrvirus_8_8
			product	hypothetical protein CE11_00132
			transl_table	11
8580	8296	CDS
			locus_tag	Satyrvirus_8_9
			product	hypothetical protein
			transl_table	11
8763	8608	CDS
			locus_tag	Satyrvirus_8_10
			product	hypothetical protein
			transl_table	11
9149	8871	CDS
			locus_tag	Satyrvirus_8_11
			product	hypothetical protein
			transl_table	11
9455	10462	CDS
			locus_tag	Satyrvirus_8_12
			product	hypothetical protein CE11_00132
			transl_table	11
10983	10600	CDS
			locus_tag	Satyrvirus_8_13
			product	hypothetical protein
			transl_table	11
11551	11234	CDS
			locus_tag	Satyrvirus_8_14
			product	hypothetical protein
			transl_table	11
11918	12133	CDS
			locus_tag	Satyrvirus_8_15
			product	hypothetical protein
			transl_table	11
12218	13282	CDS
			locus_tag	Satyrvirus_8_16
			product	hypothetical protein CE11_00132
			transl_table	11
13876	13406	CDS
			locus_tag	Satyrvirus_8_17
			product	hypothetical protein
			transl_table	11
15198	14047	CDS
			locus_tag	Satyrvirus_8_18
			product	phosphoribosyl transferase
			transl_table	11
15982	15419	CDS
			locus_tag	Satyrvirus_8_19
			product	hypothetical protein
			transl_table	11
16181	16074	CDS
			locus_tag	Satyrvirus_8_20
			product	hypothetical protein
			transl_table	11
16890	16396	CDS
			locus_tag	Satyrvirus_8_21
			product	hypothetical protein
			transl_table	11
17481	16966	CDS
			locus_tag	Satyrvirus_8_22
			product	hypothetical protein
			transl_table	11
17496	19478	CDS
			locus_tag	Satyrvirus_8_23
			product	hypothetical protein
			transl_table	11
19576	19995	CDS
			locus_tag	Satyrvirus_8_24
			product	mg989 protein
			transl_table	11
20114	22048	CDS
			locus_tag	Satyrvirus_8_25
			product	putative transposase
			transl_table	11
23098	22619	CDS
			locus_tag	Satyrvirus_8_26
			product	hypothetical protein
			transl_table	11
23505	23386	CDS
			locus_tag	Satyrvirus_8_27
			product	hypothetical protein
			transl_table	11
25470	25006	CDS
			locus_tag	Satyrvirus_8_28
			product	hypothetical protein
			transl_table	11
26004	25531	CDS
			locus_tag	Satyrvirus_8_29
			product	hypothetical protein
			transl_table	11
26424	27068	CDS
			locus_tag	Satyrvirus_8_30
			product	hypothetical protein MegaChil _gp0137
			transl_table	11
27257	27538	CDS
			locus_tag	Satyrvirus_8_31
			product	hypothetical protein
			transl_table	11
27636	27734	CDS
			locus_tag	Satyrvirus_8_32
			product	hypothetical protein
			transl_table	11
>Satyrvirus_9
4199	3	CDS
			locus_tag	Satyrvirus_9_1
			product	hypothetical protein
			transl_table	11
4323	5597	CDS
			locus_tag	Satyrvirus_9_2
			product	hypothetical protein
			transl_table	11
6570	5611	CDS
			locus_tag	Satyrvirus_9_3
			product	hypothetical protein
			transl_table	11
7719	6622	CDS
			locus_tag	Satyrvirus_9_4
			product	hypothetical protein
			transl_table	11
7852	9072	CDS
			locus_tag	Satyrvirus_9_5
			product	hypothetical protein
			transl_table	11
9639	9067	CDS
			locus_tag	Satyrvirus_9_6
			product	hypothetical protein
			transl_table	11
10652	9681	CDS
			locus_tag	Satyrvirus_9_7
			product	hypothetical protein
			transl_table	11
11889	10708	CDS
			locus_tag	Satyrvirus_9_8
			product	hypothetical protein
			transl_table	11
12734	12183	CDS
			locus_tag	Satyrvirus_9_9
			product	hypothetical protein
			transl_table	11
13131	12793	CDS
			locus_tag	Satyrvirus_9_10
			product	mimivirus translation factor SUI1-like 2
			transl_table	11
13810	13220	CDS
			locus_tag	Satyrvirus_9_11
			product	hypothetical protein Moumou_00346
			transl_table	11
15485	13836	CDS
			locus_tag	Satyrvirus_9_12
			product	guanine nucleotide exchange factor
			transl_table	11
15867	15517	CDS
			locus_tag	Satyrvirus_9_13
			product	hypothetical protein
			transl_table	11
15994	16611	CDS
			locus_tag	Satyrvirus_9_14
			product	mg424 protein
			transl_table	11
16668	17279	CDS
			locus_tag	Satyrvirus_9_15
			product	hypothetical protein
			transl_table	11
17329	17802	CDS
			locus_tag	Satyrvirus_9_16
			product	putative ubiquitin-conjugating enzyme E2
			transl_table	11
18347	17853	CDS
			locus_tag	Satyrvirus_9_17
			product	hypothetical protein
			transl_table	11
19974	18418	CDS
			locus_tag	Satyrvirus_9_18
			product	mimivirus translation initiation factor 4a
			transl_table	11
20609	20082	CDS
			locus_tag	Satyrvirus_9_19
			product	hypothetical protein
			transl_table	11
21311	20637	CDS
			locus_tag	Satyrvirus_9_20
			product	hypothetical protein
			transl_table	11
21465	24752	CDS
			locus_tag	Satyrvirus_9_21
			product	hypothetical protein
			transl_table	11
25942	24788	CDS
			locus_tag	Satyrvirus_9_22
			product	TATA-box-binding protein-like protein
			transl_table	11
26040	26369	CDS
			locus_tag	Satyrvirus_9_23
			product	hypothetical protein
			transl_table	11
26416	27456	CDS
			locus_tag	Satyrvirus_9_24
			product	mg437 protein
			transl_table	11
27689	27453	CDS
			locus_tag	Satyrvirus_9_25
			product	hypothetical protein
			transl_table	11
>Satyrvirus_10
739	230	CDS
			locus_tag	Satyrvirus_10_1
			product	hypothetical protein
			transl_table	11
904	2787	CDS
			locus_tag	Satyrvirus_10_2
			product	hypothetical protein
			transl_table	11
2852	3121	CDS
			locus_tag	Satyrvirus_10_3
			product	hypothetical protein
			transl_table	11
3188	3952	CDS
			locus_tag	Satyrvirus_10_4
			product	hypothetical protein
			transl_table	11
5144	3957	CDS
			locus_tag	Satyrvirus_10_5
			product	hypothetical protein
			transl_table	11
5251	5745	CDS
			locus_tag	Satyrvirus_10_6
			product	hypothetical protein
			transl_table	11
6601	5750	CDS
			locus_tag	Satyrvirus_10_7
			product	putative ORFan
			transl_table	11
6693	12419	CDS
			locus_tag	Satyrvirus_10_8
			product	putative ankyrin repeat protein
			transl_table	11
12446	13291	CDS
			locus_tag	Satyrvirus_10_9
			product	hypothetical protein
			transl_table	11
16003	13268	CDS
			locus_tag	Satyrvirus_10_10
			product	hypothetical protein
			transl_table	11
16111	17364	CDS
			locus_tag	Satyrvirus_10_11
			product	hypothetical protein
			transl_table	11
17389	17655	CDS
			locus_tag	Satyrvirus_10_12
			product	hypothetical protein
			transl_table	11
17773	19047	CDS
			locus_tag	Satyrvirus_10_13
			product	hypothetical protein
			transl_table	11
19171	20175	CDS
			locus_tag	Satyrvirus_10_14
			product	hypothetical protein US03_C0002G0079
			transl_table	11
20975	20259	CDS
			locus_tag	Satyrvirus_10_15
			product	hypothetical protein
			transl_table	11
24944	21117	CDS
			locus_tag	Satyrvirus_10_16
			product	hypothetical protein
			transl_table	11
26525	26409	CDS
			locus_tag	Satyrvirus_10_17
			product	hypothetical protein
			transl_table	11
26837	26649	CDS
			locus_tag	Satyrvirus_10_18
			product	hypothetical protein
			transl_table	11
27407	27291	CDS
			locus_tag	Satyrvirus_10_19
			product	hypothetical protein
			transl_table	11
>Satyrvirus_11
214	2	CDS
			locus_tag	Satyrvirus_11_1
			product	hypothetical protein
			transl_table	11
354	247	CDS
			locus_tag	Satyrvirus_11_2
			product	hypothetical protein
			transl_table	11
618	716	CDS
			locus_tag	Satyrvirus_11_3
			product	hypothetical protein
			transl_table	11
728	1063	CDS
			locus_tag	Satyrvirus_11_4
			product	hypothetical protein
			transl_table	11
1206	1592	CDS
			locus_tag	Satyrvirus_11_5
			product	hypothetical protein
			transl_table	11
2965	2405	CDS
			locus_tag	Satyrvirus_11_6
			product	putative resolvase
			transl_table	11
3087	5084	CDS
			locus_tag	Satyrvirus_11_7
			product	putative transposase/mobile element protein
			transl_table	11
5462	5614	CDS
			locus_tag	Satyrvirus_11_8
			product	hypothetical protein
			transl_table	11
6067	6516	CDS
			locus_tag	Satyrvirus_11_9
			product	hypothetical protein
			transl_table	11
8351	8650	CDS
			locus_tag	Satyrvirus_11_10
			product	hypothetical protein
			transl_table	11
8674	8820	CDS
			locus_tag	Satyrvirus_11_11
			product	hypothetical protein
			transl_table	11
8862	9872	CDS
			locus_tag	Satyrvirus_11_12
			product	T5orf172 domain-containing protein
			transl_table	11
10781	9912	CDS
			locus_tag	Satyrvirus_11_13
			product	hypothetical protein
			transl_table	11
11625	11155	CDS
			locus_tag	Satyrvirus_11_14
			product	hypothetical protein LBA_01178
			transl_table	11
11768	14311	CDS
			locus_tag	Satyrvirus_11_15
			product	DNA primase
			transl_table	11
15753	16208	CDS
			locus_tag	Satyrvirus_11_16
			product	hypothetical protein
			transl_table	11
16509	17627	CDS
			locus_tag	Satyrvirus_11_17
			product	hypothetical protein
			transl_table	11
17871	17737	CDS
			locus_tag	Satyrvirus_11_18
			product	hypothetical protein
			transl_table	11
18569	17928	CDS
			locus_tag	Satyrvirus_11_19
			product	hypothetical protein
			transl_table	11
19213	19692	CDS
			locus_tag	Satyrvirus_11_20
			product	hypothetical protein
			transl_table	11
20069	20449	CDS
			locus_tag	Satyrvirus_11_21
			product	hypothetical protein
			transl_table	11
21191	20772	CDS
			locus_tag	Satyrvirus_11_22
			product	hypothetical protein
			transl_table	11
22141	21704	CDS
			locus_tag	Satyrvirus_11_23
			product	hypothetical protein
			transl_table	11
22824	23543	CDS
			locus_tag	Satyrvirus_11_24
			product	hypothetical protein
			transl_table	11
23663	23878	CDS
			locus_tag	Satyrvirus_11_25
			product	ankyrin repeat protein
			transl_table	11
23987	24169	CDS
			locus_tag	Satyrvirus_11_26
			product	hypothetical protein
			transl_table	11
24296	24550	CDS
			locus_tag	Satyrvirus_11_27
			product	hypothetical protein
			transl_table	11
24735	26939	CDS
			locus_tag	Satyrvirus_11_28
			product	highly derived d5-like helicase-primase: PROVISIONAL
			transl_table	11
>Satyrvirus_12
1216	2	CDS
			locus_tag	Satyrvirus_12_1
			product	putative amidoligase enzyme
			transl_table	11
1340	1741	CDS
			locus_tag	Satyrvirus_12_2
			product	hypothetical protein
			transl_table	11
1859	2587	CDS
			locus_tag	Satyrvirus_12_3
			product	DNA/RNA endonuclease
			transl_table	11
2646	5876	CDS
			locus_tag	Satyrvirus_12_4
			product	superfamily II helicase
			transl_table	11
5898	6149	CDS
			locus_tag	Satyrvirus_12_5
			product	hypothetical protein
			transl_table	11
7330	6869	CDS
			locus_tag	Satyrvirus_12_6
			product	hypothetical protein
			transl_table	11
7474	11154	CDS
			locus_tag	Satyrvirus_12_7
			product	superfamily II helicase
			transl_table	11
12439	12275	CDS
			locus_tag	Satyrvirus_12_8
			product	hypothetical protein
			transl_table	11
13341	12691	CDS
			locus_tag	Satyrvirus_12_9
			product	hypothetical protein
			transl_table	11
13480	15279	CDS
			locus_tag	Satyrvirus_12_10
			product	hypothetical protein Catovirus_1_860
			transl_table	11
15982	15668	CDS
			locus_tag	Satyrvirus_12_11
			product	hypothetical protein
			transl_table	11
16538	16155	CDS
			locus_tag	Satyrvirus_12_12
			product	hypothetical protein
			transl_table	11
16784	16993	CDS
			locus_tag	Satyrvirus_12_13
			product	hypothetical protein
			transl_table	11
17047	17679	CDS
			locus_tag	Satyrvirus_12_14
			product	hypothetical protein
			transl_table	11
17977	18144	CDS
			locus_tag	Satyrvirus_12_15
			product	hypothetical protein
			transl_table	11
18611	18706	CDS
			locus_tag	Satyrvirus_12_16
			product	hypothetical protein
			transl_table	11
19885	20226	CDS
			locus_tag	Satyrvirus_12_17
			product	hypothetical protein
			transl_table	11
21034	23451	CDS
			locus_tag	Satyrvirus_12_18
			product	hypothetical protein
			transl_table	11
23628	24071	CDS
			locus_tag	Satyrvirus_12_19
			product	hypothetical protein
			transl_table	11
25603	26133	CDS
			locus_tag	Satyrvirus_12_20
			product	hypothetical protein
			transl_table	11
27026	26424	CDS
			locus_tag	Satyrvirus_12_21
			product	hypothetical protein
			transl_table	11
>Satyrvirus_13
252	16	CDS
			locus_tag	Satyrvirus_13_1
			product	hypothetical protein
			transl_table	11
1514	321	CDS
			locus_tag	Satyrvirus_13_2
			product	putative viral transcription factor
			transl_table	11
3412	1559	CDS
			locus_tag	Satyrvirus_13_3
			product	hypothetical protein glt_00578
			transl_table	11
3586	4158	CDS
			locus_tag	Satyrvirus_13_4
			product	hypothetical protein
			transl_table	11
4463	4149	CDS
			locus_tag	Satyrvirus_13_5
			product	mg441 protein
			transl_table	11
4565	5512	CDS
			locus_tag	Satyrvirus_13_6
			product	putative patatin-like phospholipase
			transl_table	11
6672	5575	CDS
			locus_tag	Satyrvirus_13_7
			product	putative J domain-containing protein
			transl_table	11
6772	8211	CDS
			locus_tag	Satyrvirus_13_8
			product	ADP-ribosylglycohydrolase
			transl_table	11
8720	8235	CDS
			locus_tag	Satyrvirus_13_9
			product	hypothetical protein
			transl_table	11
8855	12814	CDS
			locus_tag	Satyrvirus_13_10
			product	hypothetical protein CE11_00470
			transl_table	11
12875	13561	CDS
			locus_tag	Satyrvirus_13_11
			product	Deoxyuridine 5'-triphosphate nucleotidohydrolase
			transl_table	11
15137	13602	CDS
			locus_tag	Satyrvirus_13_12
			product	hypothetical protein
			transl_table	11
15791	15180	CDS
			locus_tag	Satyrvirus_13_13
			product	hypothetical protein
			transl_table	11
22511	15837	CDS
			locus_tag	Satyrvirus_13_14
			product	capsid protein
			transl_table	11
23149	22556	CDS
			locus_tag	Satyrvirus_13_15
			product	RNA 2 - tpt1 family protein
			transl_table	11
25017	23179	CDS
			locus_tag	Satyrvirus_13_16
			product	capsid protein
			transl_table	11
25237	25052	CDS
			locus_tag	Satyrvirus_13_17
			product	hypothetical protein
			transl_table	11
>Satyrvirus_14
216	61	CDS
			locus_tag	Satyrvirus_14_1
			product	hypothetical protein
			transl_table	11
2782	2072	CDS
			locus_tag	Satyrvirus_14_2
			product	DNA ligase
			transl_table	11
2905	3978	CDS
			locus_tag	Satyrvirus_14_3
			product	NUDIX hydrolase
			transl_table	11
4400	4053	CDS
			locus_tag	Satyrvirus_14_4
			product	hypothetical protein
			transl_table	11
4926	4591	CDS
			locus_tag	Satyrvirus_14_5
			product	putative ORFan
			transl_table	11
4975	5751	CDS
			locus_tag	Satyrvirus_14_6
			product	glycosyltransferase
			transl_table	11
5844	8102	CDS
			locus_tag	Satyrvirus_14_7
			product	ankyrin repeat-containing protein
			transl_table	11
8853	8107	CDS
			locus_tag	Satyrvirus_14_8
			product	putative ORFan
			transl_table	11
10692	8887	CDS
			locus_tag	Satyrvirus_14_9
			product	putative orfan
			transl_table	11
11161	10754	CDS
			locus_tag	Satyrvirus_14_10
			product	hypothetical protein
			transl_table	11
12386	11136	CDS
			locus_tag	Satyrvirus_14_11
			product	hypothetical protein
			transl_table	11
12890	12450	CDS
			locus_tag	Satyrvirus_14_12
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
14526	12940	CDS
			locus_tag	Satyrvirus_14_13
			product	hypothetical protein
			transl_table	11
19796	14601	CDS
			locus_tag	Satyrvirus_14_14
			product	putative ATP-dependent RNA helicase
			transl_table	11
20430	19822	CDS
			locus_tag	Satyrvirus_14_15
			product	hypothetical protein A3Q56_01498
			transl_table	11
21618	20524	CDS
			locus_tag	Satyrvirus_14_16
			product	acetylpolyamine aminohydrolase
			transl_table	11
22337	22056	CDS
			locus_tag	Satyrvirus_14_17
			product	hypothetical protein
			transl_table	11
22546	22671	CDS
			locus_tag	Satyrvirus_14_18
			product	hypothetical protein
			transl_table	11
23529	23290	CDS
			locus_tag	Satyrvirus_14_19
			product	hypothetical protein
			transl_table	11
>Satyrvirus_15
1057	2073	CDS
			locus_tag	Satyrvirus_15_1
			product	hypothetical protein
			transl_table	11
2281	3681	CDS
			locus_tag	Satyrvirus_15_2
			product	Flotillin domain-containing protein
			transl_table	11
3774	4178	CDS
			locus_tag	Satyrvirus_15_3
			product	hypothetical protein
			transl_table	11
4554	4183	CDS
			locus_tag	Satyrvirus_15_4
			product	hypothetical protein
			transl_table	11
5086	4652	CDS
			locus_tag	Satyrvirus_15_5
			product	hypothetical protein
			transl_table	11
5281	7614	CDS
			locus_tag	Satyrvirus_15_6
			product	hypothetical protein
			transl_table	11
7834	8922	CDS
			locus_tag	Satyrvirus_15_7
			product	hypothetical protein
			transl_table	11
10295	9105	CDS
			locus_tag	Satyrvirus_15_8
			product	putative ankyrin repeat protein
			transl_table	11
10375	11454	CDS
			locus_tag	Satyrvirus_15_9
			product	nucleoside hydrolase
			transl_table	11
11553	12374	CDS
			locus_tag	Satyrvirus_15_10
			product	hypothetical protein mv_R6
			transl_table	11
12367	12519	CDS
			locus_tag	Satyrvirus_15_11
			product	hypothetical protein
			transl_table	11
15564	16109	CDS
			locus_tag	Satyrvirus_15_12
			product	hypothetical protein
			transl_table	11
16606	16151	CDS
			locus_tag	Satyrvirus_15_13
			product	hypothetical protein
			transl_table	11
16715	17749	CDS
			locus_tag	Satyrvirus_15_14
			product	putative ankyrin repeat protein
			transl_table	11
18597	17758	CDS
			locus_tag	Satyrvirus_15_15
			product	hypothetical protein
			transl_table	11
18658	18792	CDS
			locus_tag	Satyrvirus_15_16
			product	hypothetical protein
			transl_table	11
18935	19072	CDS
			locus_tag	Satyrvirus_15_17
			product	hypothetical protein
			transl_table	11
19139	19702	CDS
			locus_tag	Satyrvirus_15_18
			product	hypothetical protein D931_01648
			transl_table	11
20232	19729	CDS
			locus_tag	Satyrvirus_15_19
			product	hypothetical protein
			transl_table	11
21389	20295	CDS
			locus_tag	Satyrvirus_15_20
			product	hypothetical protein
			transl_table	11
21484	21627	CDS
			locus_tag	Satyrvirus_15_21
			product	hypothetical protein
			transl_table	11
>Satyrvirus_16
4723	3212	CDS
			locus_tag	Satyrvirus_16_1
			product	putative DNA helicase
			transl_table	11
4891	5010	CDS
			locus_tag	Satyrvirus_16_2
			product	hypothetical protein
			transl_table	11
6514	5705	CDS
			locus_tag	Satyrvirus_16_3
			product	hypothetical protein
			transl_table	11
6533	6706	CDS
			locus_tag	Satyrvirus_16_4
			product	hypothetical protein
			transl_table	11
7585	6755	CDS
			locus_tag	Satyrvirus_16_5
			product	hypothetical protein
			transl_table	11
7750	8358	CDS
			locus_tag	Satyrvirus_16_6
			product	hypothetical protein
			transl_table	11
10697	8361	CDS
			locus_tag	Satyrvirus_16_7
			product	putative 5'-3'exonuclease20
			transl_table	11
12975	10795	CDS
			locus_tag	Satyrvirus_16_8
			product	putative 5'-3' exonuclease 20
			transl_table	11
14015	13020	CDS
			locus_tag	Satyrvirus_16_9
			product	hypothetical protein
			transl_table	11
14772	14050	CDS
			locus_tag	Satyrvirus_16_10
			product	RING zinc finger-containing protein
			transl_table	11
16304	14859	CDS
			locus_tag	Satyrvirus_16_11
			product	hypothetical protein
			transl_table	11
16513	17442	CDS
			locus_tag	Satyrvirus_16_12
			product	putative divergent NUDIX hydrolase
			transl_table	11
18135	17449	CDS
			locus_tag	Satyrvirus_16_13
			product	2Og-Fe(II) oxygenase
			transl_table	11
18216	18731	CDS
			locus_tag	Satyrvirus_16_14
			product	putative rhomboid protein
			transl_table	11
20141	18762	CDS
			locus_tag	Satyrvirus_16_15
			product	DHH family phosphohydrolase-like protein
			transl_table	11
20574	20825	CDS
			locus_tag	Satyrvirus_16_16
			product	hypothetical protein
			transl_table	11
>Satyrvirus_17
16359	16276	tRNA	Satyrvirus_tRNA_5
			product	tRNA-Pseudo(GCT)
1567	161	CDS
			locus_tag	Satyrvirus_17_1
			product	cytochrome 450-like protein
			transl_table	11
1983	1621	CDS
			locus_tag	Satyrvirus_17_2
			product	hypothetical protein
			transl_table	11
2100	2921	CDS
			locus_tag	Satyrvirus_17_3
			product	hypothetical protein Catovirus_1_259
			transl_table	11
4422	2902	CDS
			locus_tag	Satyrvirus_17_4
			product	hypothetical protein
			transl_table	11
4948	4475	CDS
			locus_tag	Satyrvirus_17_5
			product	hydrolase
			transl_table	11
5298	4918	CDS
			locus_tag	Satyrvirus_17_6
			product	NUDIX hydrolase
			transl_table	11
5852	5349	CDS
			locus_tag	Satyrvirus_17_7
			product	hypothetical protein
			transl_table	11
7046	5886	CDS
			locus_tag	Satyrvirus_17_8
			product	hypothetical protein
			transl_table	11
8521	7073	CDS
			locus_tag	Satyrvirus_17_9
			product	putative ATP-dependent RNA helicase
			transl_table	11
8598	9152	CDS
			locus_tag	Satyrvirus_17_10
			product	putative orfan
			transl_table	11
9211	9840	CDS
			locus_tag	Satyrvirus_17_11
			product	ras-related protein RIC1 isoform X2
			transl_table	11
10229	9837	CDS
			locus_tag	Satyrvirus_17_12
			product	hypothetical protein
			transl_table	11
10580	10308	CDS
			locus_tag	Satyrvirus_17_13
			product	hypothetical protein
			transl_table	11
12660	10654	CDS
			locus_tag	Satyrvirus_17_14
			product	HrpA-like helicase
			transl_table	11
12766	13707	CDS
			locus_tag	Satyrvirus_17_15
			product	protein phosphatase 2C domain containing protein
			transl_table	11
14736	14026	CDS
			locus_tag	Satyrvirus_17_16
			product	hypothetical protein
			transl_table	11
14868	15662	CDS
			locus_tag	Satyrvirus_17_17
			product	putative low complexity protein
			transl_table	11
15746	16186	CDS
			locus_tag	Satyrvirus_17_18
			product	hypothetical protein
			transl_table	11
16422	16868	CDS
			locus_tag	Satyrvirus_17_19
			product	thioredoxin-like protein
			transl_table	11
17921	16857	CDS
			locus_tag	Satyrvirus_17_20
			product	histone deacetylase
			transl_table	11
18054	18404	CDS
			locus_tag	Satyrvirus_17_21
			product	hypothetical protein
			transl_table	11
18467	19468	CDS
			locus_tag	Satyrvirus_17_22
			product	putative ORFan
			transl_table	11
19777	19956	CDS
			locus_tag	Satyrvirus_17_23
			product	hypothetical protein
			transl_table	11
>Satyrvirus_18
3362	3283	tRNA	Satyrvirus_tRNA_6
			product	tRNA-Leu(AAG)
1	873	CDS
			locus_tag	Satyrvirus_18_1
			product	hypothetical protein
			transl_table	11
1527	859	CDS
			locus_tag	Satyrvirus_18_2
			product	hypothetical protein
			transl_table	11
2222	1590	CDS
			locus_tag	Satyrvirus_18_3
			product	hypothetical protein
			transl_table	11
2366	3199	CDS
			locus_tag	Satyrvirus_18_4
			product	hypothetical protein
			transl_table	11
4388	3399	CDS
			locus_tag	Satyrvirus_18_5
			product	Phytanoyl-CoA dioxygenase family protein
			transl_table	11
4515	6299	CDS
			locus_tag	Satyrvirus_18_6
			product	hypothetical protein Catovirus_1_352
			transl_table	11
6354	6671	CDS
			locus_tag	Satyrvirus_18_7
			product	hypothetical protein
			transl_table	11
6734	8500	CDS
			locus_tag	Satyrvirus_18_8
			product	hypothetical protein Catovirus_1_352
			transl_table	11
8574	10289	CDS
			locus_tag	Satyrvirus_18_9
			product	hypothetical protein Catovirus_1_352
			transl_table	11
10392	10703	CDS
			locus_tag	Satyrvirus_18_10
			product	hypothetical protein
			transl_table	11
10812	12317	CDS
			locus_tag	Satyrvirus_18_11
			product	hypothetical protein
			transl_table	11
12602	12327	CDS
			locus_tag	Satyrvirus_18_12
			product	hypothetical protein
			transl_table	11
14424	12667	CDS
			locus_tag	Satyrvirus_18_13
			product	hypothetical protein
			transl_table	11
16710	14476	CDS
			locus_tag	Satyrvirus_18_14
			product	hypothetical protein CVU91_13340
			transl_table	11
17327	16968	CDS
			locus_tag	Satyrvirus_18_15
			product	hypothetical protein
			transl_table	11
>Satyrvirus_19
256	2	CDS
			locus_tag	Satyrvirus_19_1
			product	hypothetical protein
			transl_table	11
3867	298	CDS
			locus_tag	Satyrvirus_19_2
			product	hypothetical protein
			transl_table	11
4868	4044	CDS
			locus_tag	Satyrvirus_19_3
			product	hypothetical protein
			transl_table	11
5177	4932	CDS
			locus_tag	Satyrvirus_19_4
			product	hypothetical protein
			transl_table	11
5345	5917	CDS
			locus_tag	Satyrvirus_19_5
			product	flap endonuclease 1-like
			transl_table	11
6004	6534	CDS
			locus_tag	Satyrvirus_19_6
			product	flap endonuclease 1-like
			transl_table	11
7155	6535	CDS
			locus_tag	Satyrvirus_19_7
			product	putative swib domain-containing protein
			transl_table	11
7320	8192	CDS
			locus_tag	Satyrvirus_19_8
			product	histidine phosphatase superfamily branch 1
			transl_table	11
8407	9189	CDS
			locus_tag	Satyrvirus_19_9
			product	hypothetical protein
			transl_table	11
10199	9198	CDS
			locus_tag	Satyrvirus_19_10
			product	hypothetical protein
			transl_table	11
11979	10312	CDS
			locus_tag	Satyrvirus_19_11
			product	hypothetical protein
			transl_table	11
16055	14529	CDS
			locus_tag	Satyrvirus_19_12
			product	hypothetical protein
			transl_table	11
16820	16314	CDS
			locus_tag	Satyrvirus_19_13
			product	hypothetical protein
			transl_table	11
16886	17017	CDS
			locus_tag	Satyrvirus_19_14
			product	hypothetical protein
			transl_table	11
17020	17115	CDS
			locus_tag	Satyrvirus_19_15
			product	hypothetical protein
			transl_table	11
>Satyrvirus_20
1	99	CDS
			locus_tag	Satyrvirus_20_1
			product	hypothetical protein
			transl_table	11
419	108	CDS
			locus_tag	Satyrvirus_20_2
			product	hypothetical protein
			transl_table	11
727	1365	CDS
			locus_tag	Satyrvirus_20_3
			product	BAx inhibitor (BI)-1/yccA-like protein family
			transl_table	11
4305	1408	CDS
			locus_tag	Satyrvirus_20_4
			product	hypothetical protein
			transl_table	11
4431	5345	CDS
			locus_tag	Satyrvirus_20_5
			product	NTPase family protein
			transl_table	11
5491	6024	CDS
			locus_tag	Satyrvirus_20_6
			product	hypothetical protein
			transl_table	11
6095	7297	CDS
			locus_tag	Satyrvirus_20_7
			product	hypothetical protein
			transl_table	11
7757	7290	CDS
			locus_tag	Satyrvirus_20_8
			product	hypothetical protein
			transl_table	11
8178	8870	CDS
			locus_tag	Satyrvirus_20_9
			product	hypothetical protein
			transl_table	11
8917	9138	CDS
			locus_tag	Satyrvirus_20_10
			product	hypothetical protein
			transl_table	11
9184	9945	CDS
			locus_tag	Satyrvirus_20_11
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
10853	9966	CDS
			locus_tag	Satyrvirus_20_12
			product	putative translation factor
			transl_table	11
10934	11659	CDS
			locus_tag	Satyrvirus_20_13
			product	hypothetical protein
			transl_table	11
11729	11971	CDS
			locus_tag	Satyrvirus_20_14
			product	hypothetical protein
			transl_table	11
12082	12291	CDS
			locus_tag	Satyrvirus_20_15
			product	hypothetical protein
			transl_table	11
13134	12478	CDS
			locus_tag	Satyrvirus_20_16
			product	hypothetical protein
			transl_table	11
13128	13259	CDS
			locus_tag	Satyrvirus_20_17
			product	hypothetical protein
			transl_table	11
13234	14721	CDS
			locus_tag	Satyrvirus_20_18
			product	cysteinyl-tRNA synthetase
			transl_table	11
15147	14944	CDS
			locus_tag	Satyrvirus_20_19
			product	putative phosphoglycerate mutase family protein
			transl_table	11
15373	15218	CDS
			locus_tag	Satyrvirus_20_20
			product	putative phosphoglycerate mutase family protein
			transl_table	11
15511	16269	CDS
			locus_tag	Satyrvirus_20_21
			product	structural ppiase-like protein
			transl_table	11
16307	16510	CDS
			locus_tag	Satyrvirus_20_22
			product	hypothetical protein
			transl_table	11
17023	16598	CDS
			locus_tag	Satyrvirus_20_23
			product	structural ppiase-like protein
			transl_table	11
>Satyrvirus_21
2	1012	CDS
			locus_tag	Satyrvirus_21_1
			product	hypothetical protein PROFUN_04195
			transl_table	11
1069	1695	CDS
			locus_tag	Satyrvirus_21_2
			product	hypothetical protein MegaChil _gp0239
			transl_table	11
1726	3285	CDS
			locus_tag	Satyrvirus_21_3
			product	hypothetical protein mv_R926
			transl_table	11
3863	3318	CDS
			locus_tag	Satyrvirus_21_4
			product	hypothetical protein
			transl_table	11
3993	4376	CDS
			locus_tag	Satyrvirus_21_5
			product	hypothetical protein
			transl_table	11
4474	4914	CDS
			locus_tag	Satyrvirus_21_6
			product	hypothetical protein
			transl_table	11
5750	4911	CDS
			locus_tag	Satyrvirus_21_7
			product	mg321 protein
			transl_table	11
5790	6437	CDS
			locus_tag	Satyrvirus_21_8
			product	putative ORFan
			transl_table	11
6522	8711	CDS
			locus_tag	Satyrvirus_21_9
			product	glycosyl hydrolase family 18
			transl_table	11
8800	9012	CDS
			locus_tag	Satyrvirus_21_10
			product	hypothetical protein
			transl_table	11
9095	10084	CDS
			locus_tag	Satyrvirus_21_11
			product	putative UV-damage endonuclease
			transl_table	11
10680	10045	CDS
			locus_tag	Satyrvirus_21_12
			product	phosphoribosyl transferase domain protein
			transl_table	11
11554	10769	CDS
			locus_tag	Satyrvirus_21_13
			product	hypothetical protein
			transl_table	11
11649	13211	CDS
			locus_tag	Satyrvirus_21_14
			product	asparaginyl-tRNA synthetase
			transl_table	11
13438	14109	CDS
			locus_tag	Satyrvirus_21_15
			product	hypothetical protein
			transl_table	11
14866	14132	CDS
			locus_tag	Satyrvirus_21_16
			product	hypothetical protein
			transl_table	11
15426	15076	CDS
			locus_tag	Satyrvirus_21_17
			product	hypothetical protein
			transl_table	11
15571	16527	CDS
			locus_tag	Satyrvirus_21_18
			product	hypothetical protein mv_R911
			transl_table	11
16786	16574	CDS
			locus_tag	Satyrvirus_21_19
			product	hypothetical protein
			transl_table	11
16936	17022	CDS
			locus_tag	Satyrvirus_21_20
			product	hypothetical protein
			transl_table	11
>Satyrvirus_22
1	150	CDS
			locus_tag	Satyrvirus_22_1
			product	hypothetical protein
			transl_table	11
656	195	CDS
			locus_tag	Satyrvirus_22_2
			product	hypothetical protein
			transl_table	11
727	1161	CDS
			locus_tag	Satyrvirus_22_3
			product	hypothetical protein
			transl_table	11
1205	1687	CDS
			locus_tag	Satyrvirus_22_4
			product	hypothetical protein
			transl_table	11
2200	1667	CDS
			locus_tag	Satyrvirus_22_5
			product	putative ORFan
			transl_table	11
2339	2500	CDS
			locus_tag	Satyrvirus_22_6
			product	hypothetical protein
			transl_table	11
2743	2549	CDS
			locus_tag	Satyrvirus_22_7
			product	hypothetical protein
			transl_table	11
5429	3009	CDS
			locus_tag	Satyrvirus_22_8
			product	putative WcaK-like polysaccharide pyruvyl transferase
			transl_table	11
5510	5812	CDS
			locus_tag	Satyrvirus_22_9
			product	hypothetical protein
			transl_table	11
5888	7099	CDS
			locus_tag	Satyrvirus_22_10
			product	putative helicase
			transl_table	11
8158	7184	CDS
			locus_tag	Satyrvirus_22_11
			product	VV A32 virion packaging ATPase
			transl_table	11
8188	8286	CDS
			locus_tag	Satyrvirus_22_12
			product	hypothetical protein
			transl_table	11
8300	8512	CDS
			locus_tag	Satyrvirus_22_13
			product	hypothetical protein
			transl_table	11
8633	8851	CDS
			locus_tag	Satyrvirus_22_14
			product	hypothetical protein
			transl_table	11
8929	9927	CDS
			locus_tag	Satyrvirus_22_15
			product	AAA family ATPase
			transl_table	11
9996	11624	CDS
			locus_tag	Satyrvirus_22_16
			product	hypothetical protein Indivirus_3_59
			transl_table	11
12994	11828	CDS
			locus_tag	Satyrvirus_22_17
			product	mg15 protein
			transl_table	11
13765	13869	CDS
			locus_tag	Satyrvirus_22_18
			product	hypothetical protein
			transl_table	11
13924	14028	CDS
			locus_tag	Satyrvirus_22_19
			product	hypothetical protein
			transl_table	11
14455	14631	CDS
			locus_tag	Satyrvirus_22_20
			product	hypothetical protein
			transl_table	11
14740	14841	CDS
			locus_tag	Satyrvirus_22_21
			product	hypothetical protein
			transl_table	11
>Satyrvirus_23
1	732	CDS
			locus_tag	Satyrvirus_23_1
			product	hypothetical protein
			transl_table	11
785	1846	CDS
			locus_tag	Satyrvirus_23_2
			product	hypothetical protein PBRA_008189
			transl_table	11
1891	2259	CDS
			locus_tag	Satyrvirus_23_3
			product	hypothetical protein C2E21_7769
			transl_table	11
2317	3000	CDS
			locus_tag	Satyrvirus_23_4
			product	exonuclease
			transl_table	11
3063	3335	CDS
			locus_tag	Satyrvirus_23_5
			product	hypothetical protein
			transl_table	11
3450	5027	CDS
			locus_tag	Satyrvirus_23_6
			product	intein-containing DNA-directed RNA polymerase subunit 2
			transl_table	11
5080	5223	CDS
			locus_tag	Satyrvirus_23_7
			product	hypothetical protein
			transl_table	11
5186	6343	CDS
			locus_tag	Satyrvirus_23_8
			product	DNA-directed RNA polymerase subunit 2
			transl_table	11
6758	7591	CDS
			locus_tag	Satyrvirus_23_9
			product	intein-containing DNA-directed RNA polymerase subunit 2
			transl_table	11
7599	7706	CDS
			locus_tag	Satyrvirus_23_10
			product	hypothetical protein
			transl_table	11
7717	8409	CDS
			locus_tag	Satyrvirus_23_11
			product	putative nuclease
			transl_table	11
8838	10676	CDS
			locus_tag	Satyrvirus_23_12
			product	intein-containing dna-directed rna polymerase subunit 2
			transl_table	11
10828	11478	CDS
			locus_tag	Satyrvirus_23_13
			product	putative intron HNH endonuclease
			transl_table	11
11438	11893	CDS
			locus_tag	Satyrvirus_23_14
			product	hypothetical protein
			transl_table	11
12102	12596	CDS
			locus_tag	Satyrvirus_23_15
			product	intein-containing dna-directed rna polymerase subunit 2
			transl_table	11
12663	13202	CDS
			locus_tag	Satyrvirus_23_16
			product	putative ORFan
			transl_table	11
13270	13785	CDS
			locus_tag	Satyrvirus_23_17
			product	hypothetical protein
			transl_table	11
13820	14254	CDS
			locus_tag	Satyrvirus_23_18
			product	hypothetical protein
			transl_table	11
14850	14251	CDS
			locus_tag	Satyrvirus_23_19
			product	hypothetical protein
			transl_table	11
>Satyrvirus_24
389	3	CDS
			locus_tag	Satyrvirus_24_1
			product	hypothetical protein c7_L232
			transl_table	11
1243	482	CDS
			locus_tag	Satyrvirus_24_2
			product	hypothetical protein
			transl_table	11
2392	1769	CDS
			locus_tag	Satyrvirus_24_3
			product	hypothetical protein
			transl_table	11
2949	2677	CDS
			locus_tag	Satyrvirus_24_4
			product	hypothetical protein
			transl_table	11
3302	3174	CDS
			locus_tag	Satyrvirus_24_5
			product	hypothetical protein
			transl_table	11
3893	3387	CDS
			locus_tag	Satyrvirus_24_6
			product	hypothetical protein
			transl_table	11
3886	3996	CDS
			locus_tag	Satyrvirus_24_7
			product	hypothetical protein
			transl_table	11
4045	7200	CDS
			locus_tag	Satyrvirus_24_8
			product	DEAD-like helicase
			transl_table	11
7675	7442	CDS
			locus_tag	Satyrvirus_24_9
			product	hypothetical protein
			transl_table	11
7864	7679	CDS
			locus_tag	Satyrvirus_24_10
			product	hypothetical protein
			transl_table	11
8018	8311	CDS
			locus_tag	Satyrvirus_24_11
			product	hypothetical protein
			transl_table	11
8363	8680	CDS
			locus_tag	Satyrvirus_24_12
			product	hypothetical protein
			transl_table	11
9003	9443	CDS
			locus_tag	Satyrvirus_24_13
			product	hypothetical protein
			transl_table	11
9603	9463	CDS
			locus_tag	Satyrvirus_24_14
			product	hypothetical protein
			transl_table	11
10321	9662	CDS
			locus_tag	Satyrvirus_24_15
			product	hypothetical protein
			transl_table	11
10495	10358	CDS
			locus_tag	Satyrvirus_24_16
			product	hypothetical protein
			transl_table	11
10720	12648	CDS
			locus_tag	Satyrvirus_24_17
			product	hypothetical protein CBB70_03635
			transl_table	11
12715	13137	CDS
			locus_tag	Satyrvirus_24_18
			product	hypothetical protein
			transl_table	11
13578	13138	CDS
			locus_tag	Satyrvirus_24_19
			product	hypothetical protein
			transl_table	11
>Satyrvirus_25
204	16	CDS
			locus_tag	Satyrvirus_25_1
			product	hypothetical protein
			transl_table	11
1755	253	CDS
			locus_tag	Satyrvirus_25_2
			product	hypothetical protein
			transl_table	11
1724	1918	CDS
			locus_tag	Satyrvirus_25_3
			product	hypothetical protein
			transl_table	11
5199	2200	CDS
			locus_tag	Satyrvirus_25_4
			product	D5-ATPase-helicase
			transl_table	11
5858	5277	CDS
			locus_tag	Satyrvirus_25_5
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
5926	6954	CDS
			locus_tag	Satyrvirus_25_6
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
6974	8113	CDS
			locus_tag	Satyrvirus_25_7
			product	hypothetical protein
			transl_table	11
8917	8138	CDS
			locus_tag	Satyrvirus_25_8
			product	putative etoposide-induced protein 2.4
			transl_table	11
9353	9141	CDS
			locus_tag	Satyrvirus_25_9
			product	hypothetical protein
			transl_table	11
9595	9350	CDS
			locus_tag	Satyrvirus_25_10
			product	hypothetical protein
			transl_table	11
9899	9609	CDS
			locus_tag	Satyrvirus_25_11
			product	hypothetical protein
			transl_table	11
9960	10106	CDS
			locus_tag	Satyrvirus_25_12
			product	hypothetical protein
			transl_table	11
10203	10385	CDS
			locus_tag	Satyrvirus_25_13
			product	hypothetical protein
			transl_table	11
10649	12178	CDS
			locus_tag	Satyrvirus_25_14
			product	hypothetical protein
			transl_table	11
13414	12836	CDS
			locus_tag	Satyrvirus_25_15
			product	hypothetical protein
			transl_table	11
13646	13491	CDS
			locus_tag	Satyrvirus_25_16
			product	hypothetical protein
			transl_table	11
>Satyrvirus_26
695	138	CDS
			locus_tag	Satyrvirus_26_1
			product	HD superfamily hydrolase
			transl_table	11
1419	862	CDS
			locus_tag	Satyrvirus_26_2
			product	hypothetical protein
			transl_table	11
2928	1549	CDS
			locus_tag	Satyrvirus_26_3
			product	hypothetical protein Moumou_00622
			transl_table	11
4028	3000	CDS
			locus_tag	Satyrvirus_26_4
			product	DEHA2D17908p
			transl_table	11
4155	6809	CDS
			locus_tag	Satyrvirus_26_5
			product	putative bifunctional metalloprotease/ubiquitin-protein ligase
			transl_table	11
8096	6822	CDS
			locus_tag	Satyrvirus_26_6
			product	putative ORFan
			transl_table	11
8182	8382	CDS
			locus_tag	Satyrvirus_26_7
			product	hypothetical protein
			transl_table	11
9064	8384	CDS
			locus_tag	Satyrvirus_26_8
			product	hypothetical protein
			transl_table	11
9664	9122	CDS
			locus_tag	Satyrvirus_26_9
			product	putative orfan
			transl_table	11
11539	9713	CDS
			locus_tag	Satyrvirus_26_10
			product	glycyl tRNA-synthetase
			transl_table	11
12971	11601	CDS
			locus_tag	Satyrvirus_26_11
			product	hypothetical protein
			transl_table	11
13465	12995	CDS
			locus_tag	Satyrvirus_26_12
			product	hypothetical protein
			transl_table	11
>Satyrvirus_27
1070	510	CDS
			locus_tag	Satyrvirus_27_1
			product	hypothetical protein
			transl_table	11
1916	1530	CDS
			locus_tag	Satyrvirus_27_2
			product	hypothetical protein
			transl_table	11
2520	2143	CDS
			locus_tag	Satyrvirus_27_3
			product	hypothetical protein
			transl_table	11
3085	2735	CDS
			locus_tag	Satyrvirus_27_4
			product	hypothetical protein
			transl_table	11
3231	3923	CDS
			locus_tag	Satyrvirus_27_5
			product	hypothetical protein
			transl_table	11
5444	3858	CDS
			locus_tag	Satyrvirus_27_6
			product	hypothetical protein
			transl_table	11
5574	6668	CDS
			locus_tag	Satyrvirus_27_7
			product	Stk1 family PASTA domain-containing Ser/Thr kinase
			transl_table	11
7706	6783	CDS
			locus_tag	Satyrvirus_27_8
			product	hypothetical protein
			transl_table	11
7938	7780	CDS
			locus_tag	Satyrvirus_27_9
			product	hypothetical protein
			transl_table	11
7917	8012	CDS
			locus_tag	Satyrvirus_27_10
			product	hypothetical protein
			transl_table	11
8067	8399	CDS
			locus_tag	Satyrvirus_27_11
			product	hypothetical protein
			transl_table	11
8446	8781	CDS
			locus_tag	Satyrvirus_27_12
			product	putative orfan
			transl_table	11
9435	8803	CDS
			locus_tag	Satyrvirus_27_13
			product	putative ankyrin repeat protein L25
			transl_table	11
10139	9498	CDS
			locus_tag	Satyrvirus_27_14
			product	hypothetical protein
			transl_table	11
10378	10653	CDS
			locus_tag	Satyrvirus_27_15
			product	hypothetical protein
			transl_table	11
10800	11126	CDS
			locus_tag	Satyrvirus_27_16
			product	hypothetical protein
			transl_table	11
11256	12680	CDS
			locus_tag	Satyrvirus_27_17
			product	hypothetical protein
			transl_table	11
12919	12764	CDS
			locus_tag	Satyrvirus_27_18
			product	hypothetical protein
			transl_table	11
12970	13296	CDS
			locus_tag	Satyrvirus_27_19
			product	hypothetical protein
			transl_table	11
>Satyrvirus_28
150	920	CDS
			locus_tag	Satyrvirus_28_1
			product	hypothetical protein
			transl_table	11
1464	1102	CDS
			locus_tag	Satyrvirus_28_2
			product	hypothetical protein
			transl_table	11
2441	1608	CDS
			locus_tag	Satyrvirus_28_3
			product	pathogenesis-related protein 5-like
			transl_table	11
2573	2956	CDS
			locus_tag	Satyrvirus_28_4
			product	hypothetical protein
			transl_table	11
3094	3756	CDS
			locus_tag	Satyrvirus_28_5
			product	ankyrin repeat-containing protein
			transl_table	11
4368	3820	CDS
			locus_tag	Satyrvirus_28_6
			product	hypothetical protein
			transl_table	11
4507	5148	CDS
			locus_tag	Satyrvirus_28_7
			product	hypothetical protein
			transl_table	11
5897	5448	CDS
			locus_tag	Satyrvirus_28_8
			product	hypothetical protein
			transl_table	11
6036	6488	CDS
			locus_tag	Satyrvirus_28_9
			product	hypothetical protein
			transl_table	11
6659	7597	CDS
			locus_tag	Satyrvirus_28_10
			product	putative ORFan
			transl_table	11
7655	8662	CDS
			locus_tag	Satyrvirus_28_11
			product	ankyrin repeat protein
			transl_table	11
8736	9143	CDS
			locus_tag	Satyrvirus_28_12
			product	hypothetical protein
			transl_table	11
9721	9194	CDS
			locus_tag	Satyrvirus_28_13
			product	UNKNOWN
			transl_table	11
9950	11158	CDS
			locus_tag	Satyrvirus_28_14
			product	gfo/Idh/MocA family oxidoreductase
			transl_table	11
11373	11167	CDS
			locus_tag	Satyrvirus_28_15
			product	hypothetical protein
			transl_table	11
11723	11379	CDS
			locus_tag	Satyrvirus_28_16
			product	hypothetical protein
			transl_table	11
12223	12399	CDS
			locus_tag	Satyrvirus_28_17
			product	hypothetical protein
			transl_table	11
>Satyrvirus_29
724	161	CDS
			locus_tag	Satyrvirus_29_1
			product	hypothetical protein
			transl_table	11
1215	805	CDS
			locus_tag	Satyrvirus_29_2
			product	hypothetical protein
			transl_table	11
1336	1830	CDS
			locus_tag	Satyrvirus_29_3
			product	hypothetical protein
			transl_table	11
1928	2398	CDS
			locus_tag	Satyrvirus_29_4
			product	hypothetical protein
			transl_table	11
2587	3003	CDS
			locus_tag	Satyrvirus_29_5
			product	hypothetical protein
			transl_table	11
3452	3027	CDS
			locus_tag	Satyrvirus_29_6
			product	hypothetical protein
			transl_table	11
3520	4257	CDS
			locus_tag	Satyrvirus_29_7
			product	putative ankyrin repeat protein
			transl_table	11
4454	4263	CDS
			locus_tag	Satyrvirus_29_8
			product	hypothetical protein
			transl_table	11
5219	4509	CDS
			locus_tag	Satyrvirus_29_9
			product	hypothetical protein
			transl_table	11
5909	5277	CDS
			locus_tag	Satyrvirus_29_10
			product	hypothetical protein
			transl_table	11
6053	6385	CDS
			locus_tag	Satyrvirus_29_11
			product	hypothetical protein
			transl_table	11
7960	6500	CDS
			locus_tag	Satyrvirus_29_12
			product	SPFH domain / Band 7 domain containing protein
			transl_table	11
9148	9258	CDS
			locus_tag	Satyrvirus_29_13
			product	hypothetical protein
			transl_table	11
9236	9634	CDS
			locus_tag	Satyrvirus_29_14
			product	hypothetical protein
			transl_table	11
11354	10020	CDS
			locus_tag	Satyrvirus_29_15
			product	hypothetical protein
			transl_table	11
12050	11496	CDS
			locus_tag	Satyrvirus_29_16
			product	glycosyltransferase family 1 protein
			transl_table	11
>Satyrvirus_30
1100	636	CDS
			locus_tag	Satyrvirus_30_1
			product	hypothetical protein
			transl_table	11
1219	3012	CDS
			locus_tag	Satyrvirus_30_2
			product	hypothetical protein
			transl_table	11
3773	3015	CDS
			locus_tag	Satyrvirus_30_3
			product	hypothetical protein
			transl_table	11
4982	3816	CDS
			locus_tag	Satyrvirus_30_4
			product	hypothetical protein
			transl_table	11
6897	4936	CDS
			locus_tag	Satyrvirus_30_5
			product	ankyrin repeat protein
			transl_table	11
6976	7668	CDS
			locus_tag	Satyrvirus_30_6
			product	double-stranded RNA binding motif protein
			transl_table	11
9865	7655	CDS
			locus_tag	Satyrvirus_30_7
			product	hypothetical protein
			transl_table	11
10529	9921	CDS
			locus_tag	Satyrvirus_30_8
			product	Rab family GTPase
			transl_table	11
10612	10731	CDS
			locus_tag	Satyrvirus_30_9
			product	hypothetical protein
			transl_table	11
11243	11010	CDS
			locus_tag	Satyrvirus_30_10
			product	hypothetical protein
			transl_table	11
11419	11243	CDS
			locus_tag	Satyrvirus_30_11
			product	hypothetical protein
			transl_table	11
>Satyrvirus_31
247	2	CDS
			locus_tag	Satyrvirus_31_1
			product	hypothetical protein
			transl_table	11
2111	309	CDS
			locus_tag	Satyrvirus_31_2
			product	dehydrogenase/oxidoreductase
			transl_table	11
3011	2169	CDS
			locus_tag	Satyrvirus_31_3
			product	RING finger domain protein
			transl_table	11
3943	3437	CDS
			locus_tag	Satyrvirus_31_4
			product	hypothetical protein
			transl_table	11
4935	4105	CDS
			locus_tag	Satyrvirus_31_5
			product	uncharacterized protein
			transl_table	11
5011	5481	CDS
			locus_tag	Satyrvirus_31_6
			product	hypothetical protein
			transl_table	11
5541	5831	CDS
			locus_tag	Satyrvirus_31_7
			product	hypothetical protein
			transl_table	11
6368	5850	CDS
			locus_tag	Satyrvirus_31_8
			product	hypothetical protein
			transl_table	11
7311	6970	CDS
			locus_tag	Satyrvirus_31_9
			product	hypothetical protein
			transl_table	11
8251	7382	CDS
			locus_tag	Satyrvirus_31_10
			product	glycosyl transferase
			transl_table	11
9287	8313	CDS
			locus_tag	Satyrvirus_31_11
			product	Hypothetical protein BRZCDTV_405
			transl_table	11
9435	9659	CDS
			locus_tag	Satyrvirus_31_12
			product	hypothetical protein
			transl_table	11
10683	9721	CDS
			locus_tag	Satyrvirus_31_13
			product	collagen triple helix repeat containing protein
			transl_table	11
10830	11150	CDS
			locus_tag	Satyrvirus_31_14
			product	hypothetical protein
			transl_table	11
>Satyrvirus_32
1512	1060	CDS
			locus_tag	Satyrvirus_32_1
			product	Topoisomerase I
			transl_table	11
3574	1664	CDS
			locus_tag	Satyrvirus_32_2
			product	topoisomerase I
			transl_table	11
6359	3648	CDS
			locus_tag	Satyrvirus_32_3
			product	putative ATP-dependent helicase
			transl_table	11
6489	6385	CDS
			locus_tag	Satyrvirus_32_4
			product	hypothetical protein
			transl_table	11
6537	6878	CDS
			locus_tag	Satyrvirus_32_5
			product	hypothetical protein
			transl_table	11
7730	6846	CDS
			locus_tag	Satyrvirus_32_6
			product	hypothetical protein
			transl_table	11
8590	7805	CDS
			locus_tag	Satyrvirus_32_7
			product	hypothetical protein
			transl_table	11
9401	8664	CDS
			locus_tag	Satyrvirus_32_8
			product	hypothetical protein
			transl_table	11
10720	9416	CDS
			locus_tag	Satyrvirus_32_9
			product	amino oxidase family protein
			transl_table	11
10917	10717	CDS
			locus_tag	Satyrvirus_32_10
			product	hypothetical protein
			transl_table	11
>Satyrvirus_33
1	444	CDS
			locus_tag	Satyrvirus_33_1
			product	hypothetical protein ceV_147
			transl_table	11
498	803	CDS
			locus_tag	Satyrvirus_33_2
			product	hypothetical protein
			transl_table	11
2211	2381	CDS
			locus_tag	Satyrvirus_33_3
			product	hypothetical protein
			transl_table	11
2624	3997	CDS
			locus_tag	Satyrvirus_33_4
			product	hypothetical protein
			transl_table	11
4870	4103	CDS
			locus_tag	Satyrvirus_33_5
			product	ankyrin repeat protein
			transl_table	11
5887	5120	CDS
			locus_tag	Satyrvirus_33_6
			product	hypothetical protein Catovirus_1_785
			transl_table	11
6860	5910	CDS
			locus_tag	Satyrvirus_33_7
			product	hypothetical protein Catovirus_1_784
			transl_table	11
6884	7822	CDS
			locus_tag	Satyrvirus_33_8
			product	hypothetical protein
			transl_table	11
8758	7811	CDS
			locus_tag	Satyrvirus_33_9
			product	hypothetical protein Catovirus_1_783
			transl_table	11
10729	8804	CDS
			locus_tag	Satyrvirus_33_10
			product	hypothetical protein
			transl_table	11
>Satyrvirus_34
85	234	CDS
			locus_tag	Satyrvirus_34_1
			product	hypothetical protein
			transl_table	11
227	388	CDS
			locus_tag	Satyrvirus_34_2
			product	hypothetical protein
			transl_table	11
381	494	CDS
			locus_tag	Satyrvirus_34_3
			product	hypothetical protein
			transl_table	11
536	682	CDS
			locus_tag	Satyrvirus_34_4
			product	hypothetical protein
			transl_table	11
1269	1364	CDS
			locus_tag	Satyrvirus_34_5
			product	hypothetical protein
			transl_table	11
1446	2585	CDS
			locus_tag	Satyrvirus_34_6
			product	mg15 protein
			transl_table	11
3107	2604	CDS
			locus_tag	Satyrvirus_34_7
			product	hypothetical protein
			transl_table	11
5618	3261	CDS
			locus_tag	Satyrvirus_34_8
			product	hypothetical protein
			transl_table	11
5725	6483	CDS
			locus_tag	Satyrvirus_34_9
			product	putative RNA methylase
			transl_table	11
6470	6604	CDS
			locus_tag	Satyrvirus_34_10
			product	hypothetical protein
			transl_table	11
8119	6632	CDS
			locus_tag	Satyrvirus_34_11
			product	putative ubiquitin-specific protease
			transl_table	11
9269	8190	CDS
			locus_tag	Satyrvirus_34_12
			product	aminotransferase
			transl_table	11
10238	9291	CDS
			locus_tag	Satyrvirus_34_13
			product	dTDP-D-glucose 4,6-dehydratase/N-acetylglucosamine-1-phosphate urydiltransferase fusion protein
			transl_table	11
10428	10712	CDS
			locus_tag	Satyrvirus_34_14
			product	hypothetical protein
			transl_table	11
>Satyrvirus_35
1	120	CDS
			locus_tag	Satyrvirus_35_1
			product	hypothetical protein
			transl_table	11
74	1348	CDS
			locus_tag	Satyrvirus_35_2
			product	bifunctional dihydrofolate reductase-thymidylate synthase
			transl_table	11
2391	1345	CDS
			locus_tag	Satyrvirus_35_3
			product	putative replication factor c small subuni
			transl_table	11
2531	3058	CDS
			locus_tag	Satyrvirus_35_4
			product	hypothetical protein
			transl_table	11
3148	5640	CDS
			locus_tag	Satyrvirus_35_5
			product	dna-directed rna polymerase subunit 1
			transl_table	11
6301	8298	CDS
			locus_tag	Satyrvirus_35_6
			product	dna-directed rna polymerase subunit 1
			transl_table	11
8373	9071	CDS
			locus_tag	Satyrvirus_35_7
			product	hypothetical protein
			transl_table	11
9747	9884	CDS
			locus_tag	Satyrvirus_35_8
			product	hypothetical protein
			transl_table	11
>Satyrvirus_36
1	357	CDS
			locus_tag	Satyrvirus_36_1
			product	hypothetical protein
			transl_table	11
1031	387	CDS
			locus_tag	Satyrvirus_36_2
			product	hypothetical protein
			transl_table	11
1186	1914	CDS
			locus_tag	Satyrvirus_36_3
			product	hypothetical protein
			transl_table	11
2032	1925	CDS
			locus_tag	Satyrvirus_36_4
			product	hypothetical protein
			transl_table	11
3434	2034	CDS
			locus_tag	Satyrvirus_36_5
			product	E3 ubiquitin-protein ligase
			transl_table	11
3876	3508	CDS
			locus_tag	Satyrvirus_36_6
			product	hypothetical protein
			transl_table	11
4352	4032	CDS
			locus_tag	Satyrvirus_36_7
			product	hypothetical protein
			transl_table	11
4475	4849	CDS
			locus_tag	Satyrvirus_36_8
			product	DNA-directed RNA polymerase II subunit RPB2
			transl_table	11
5406	4822	CDS
			locus_tag	Satyrvirus_36_9
			product	hypothetical protein
			transl_table	11
5933	7342	CDS
			locus_tag	Satyrvirus_36_10
			product	hypothetical protein
			transl_table	11
7467	7907	CDS
			locus_tag	Satyrvirus_36_11
			product	hypothetical protein
			transl_table	11
7974	8939	CDS
			locus_tag	Satyrvirus_36_12
			product	Hypothetical protein ORPV_1015
			transl_table	11
9292	8945	CDS
			locus_tag	Satyrvirus_36_13
			product	hypothetical protein
			transl_table	11
>Satyrvirus_37
2	1105	CDS
			locus_tag	Satyrvirus_37_1
			product	hypothetical protein BGO67_05460
			transl_table	11
1142	1771	CDS
			locus_tag	Satyrvirus_37_2
			product	hypothetical protein mc_298
			transl_table	11
2326	1772	CDS
			locus_tag	Satyrvirus_37_3
			product	YdcF family protein
			transl_table	11
2928	2368	CDS
			locus_tag	Satyrvirus_37_4
			product	hypothetical protein
			transl_table	11
4225	3005	CDS
			locus_tag	Satyrvirus_37_5
			product	hypothetical protein
			transl_table	11
5557	4295	CDS
			locus_tag	Satyrvirus_37_6
			product	hypothetical protein
			transl_table	11
5657	5538	CDS
			locus_tag	Satyrvirus_37_7
			product	hypothetical protein
			transl_table	11
8721	5632	CDS
			locus_tag	Satyrvirus_37_8
			product	isoleucyl-tRNA synthetase
			transl_table	11
>Satyrvirus_38
334	2	CDS
			locus_tag	Satyrvirus_38_1
			product	hypothetical protein
			transl_table	11
652	1596	CDS
			locus_tag	Satyrvirus_38_2
			product	hypothetical protein
			transl_table	11
1657	2298	CDS
			locus_tag	Satyrvirus_38_3
			product	hypothetical protein
			transl_table	11
2629	2501	CDS
			locus_tag	Satyrvirus_38_4
			product	hypothetical protein
			transl_table	11
2883	3686	CDS
			locus_tag	Satyrvirus_38_5
			product	hypothetical protein
			transl_table	11
3771	4481	CDS
			locus_tag	Satyrvirus_38_6
			product	hypothetical protein
			transl_table	11
4551	5636	CDS
			locus_tag	Satyrvirus_38_7
			product	hypothetical protein Klosneuvirus_4_141
			transl_table	11
6752	5640	CDS
			locus_tag	Satyrvirus_38_8
			product	glycogen debranching enzyme alpha-1,6-glucosidase
			transl_table	11
7709	6831	CDS
			locus_tag	Satyrvirus_38_9
			product	hypothetical protein
			transl_table	11
>Satyrvirus_39
1077	76	CDS
			locus_tag	Satyrvirus_39_1
			product	putative ankyrin repeat protein L25
			transl_table	11
1238	1387	CDS
			locus_tag	Satyrvirus_39_2
			product	hypothetical protein
			transl_table	11
5113	1439	CDS
			locus_tag	Satyrvirus_39_3
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
5995	5867	CDS
			locus_tag	Satyrvirus_39_4
			product	hypothetical protein
			transl_table	11
6352	6197	CDS
			locus_tag	Satyrvirus_39_5
			product	hypothetical protein
			transl_table	11
>Satyrvirus_40
391	47	CDS
			locus_tag	Satyrvirus_40_1
			product	rab family small GTPase
			transl_table	11
478	1395	CDS
			locus_tag	Satyrvirus_40_2
			product	tRNA guanylyltransferase
			transl_table	11
3240	1387	CDS
			locus_tag	Satyrvirus_40_3
			product	vesicle-fusion atpase
			transl_table	11
4349	3288	CDS
			locus_tag	Satyrvirus_40_4
			product	putative orfan
			transl_table	11
4460	5026	CDS
			locus_tag	Satyrvirus_40_5
			product	hypothetical protein
			transl_table	11
5129	5845	CDS
			locus_tag	Satyrvirus_40_6
			product	Predicted O-linked N-acetylglucosamine transferase, SPINDLY family
			transl_table	11
5993	5856	CDS
			locus_tag	Satyrvirus_40_7
			product	hypothetical protein
			transl_table	11
6811	6698	CDS
			locus_tag	Satyrvirus_40_8
			product	hypothetical protein
			transl_table	11
>Satyrvirus_41
1394	3	CDS
			locus_tag	Satyrvirus_41_1
			product	hypothetical protein
			transl_table	11
1644	2495	CDS
			locus_tag	Satyrvirus_41_2
			product	PREDICTED: uncharacterized protein LOC101860027
			transl_table	11
3596	2664	CDS
			locus_tag	Satyrvirus_41_3
			product	hypothetical protein Catovirus_1_431
			transl_table	11
4351	3737	CDS
			locus_tag	Satyrvirus_41_4
			product	hypothetical protein Klosneuvirus_16_1
			transl_table	11
4480	5109	CDS
			locus_tag	Satyrvirus_41_5
			product	hypothetical protein
			transl_table	11
5748	5251	CDS
			locus_tag	Satyrvirus_41_6
			product	hypothetical protein
			transl_table	11
>Satyrvirus_42
31	1677	CDS
			locus_tag	Satyrvirus_42_1
			product	polyA polymerase catalitic subunit
			transl_table	11
1741	2568	CDS
			locus_tag	Satyrvirus_42_2
			product	hypothetical protein
			transl_table	11
2629	4248	CDS
			locus_tag	Satyrvirus_42_3
			product	putative ribonuclease 3
			transl_table	11
4311	5333	CDS
			locus_tag	Satyrvirus_42_4
			product	hypothetical protein
			transl_table	11
5630	5716	CDS
			locus_tag	Satyrvirus_42_5
			product	hypothetical protein
			transl_table	11
>Satyrvirus_43
2	145	CDS
			locus_tag	Satyrvirus_43_1
			product	hypothetical protein
			transl_table	11
687	295	CDS
			locus_tag	Satyrvirus_43_2
			product	hypothetical protein
			transl_table	11
916	1278	CDS
			locus_tag	Satyrvirus_43_3
			product	hypothetical protein
			transl_table	11
1479	2267	CDS
			locus_tag	Satyrvirus_43_4
			product	hypothetical protein
			transl_table	11
2416	3006	CDS
			locus_tag	Satyrvirus_43_5
			product	PREDICTED: guanosine-3',5'-bis(diphosphate) 3'-pyrophosphohydrolase MESH1 isoform X1
			transl_table	11
3791	3078	CDS
			locus_tag	Satyrvirus_43_6
			product	hypothetical protein
			transl_table	11
4386	3901	CDS
			locus_tag	Satyrvirus_43_7
			product	hypothetical protein
			transl_table	11
5050	4451	CDS
			locus_tag	Satyrvirus_43_8
			product	hypothetical protein DICPUDRAFT_32515
			transl_table	11
5165	5608	CDS
			locus_tag	Satyrvirus_43_9
			product	hypothetical protein
			transl_table	11
>Satyrvirus_44
3	1001	CDS
			locus_tag	Satyrvirus_44_1
			product	hypothetical protein Indivirus_3_59
			transl_table	11
1799	1323	CDS
			locus_tag	Satyrvirus_44_2
			product	hypothetical protein
			transl_table	11
2019	1771	CDS
			locus_tag	Satyrvirus_44_3
			product	hypothetical protein
			transl_table	11
2102	2230	CDS
			locus_tag	Satyrvirus_44_4
			product	hypothetical protein
			transl_table	11
2353	3081	CDS
			locus_tag	Satyrvirus_44_5
			product	ran, putative
			transl_table	11
3184	3660	CDS
			locus_tag	Satyrvirus_44_6
			product	hypothetical protein
			transl_table	11
4129	3692	CDS
			locus_tag	Satyrvirus_44_7
			product	hypothetical protein
			transl_table	11
>Satyrvirus_45
345	1310	CDS
			locus_tag	Satyrvirus_45_1
			product	NAD-dependent epimerase/dehydratase
			transl_table	11
1374	1946	CDS
			locus_tag	Satyrvirus_45_2
			product	4 epimerase
			transl_table	11
2223	2891	CDS
			locus_tag	Satyrvirus_45_3
			product	acetyltransferase
			transl_table	11
3700	3933	CDS
			locus_tag	Satyrvirus_45_4
			product	aminotransferase
			transl_table	11
>Satyrvirus_46
719	3	CDS
			locus_tag	Satyrvirus_46_1
			product	hypothetical protein
			transl_table	11
794	1681	CDS
			locus_tag	Satyrvirus_46_2
			product	hypothetical protein
			transl_table	11
1710	2510	CDS
			locus_tag	Satyrvirus_46_3
			product	heme oxygenase
			transl_table	11
3022	2507	CDS
			locus_tag	Satyrvirus_46_4
			product	transcription factor S-II-related protein
			transl_table	11
3328	3053	CDS
			locus_tag	Satyrvirus_46_5
			product	hypothetical protein
			transl_table	11
3537	3385	CDS
			locus_tag	Satyrvirus_46_6
			product	hypothetical protein
			transl_table	11
>Satyrvirus_47
142	624	CDS
			locus_tag	Satyrvirus_47_1
			product	putative orfan
			transl_table	11
678	875	CDS
			locus_tag	Satyrvirus_47_2
			product	putative ORFan
			transl_table	11
927	2999	CDS
			locus_tag	Satyrvirus_47_3
			product	NAD-dependent DNA ligase
			transl_table	11
3004	3093	CDS
			locus_tag	Satyrvirus_47_4
			product	hypothetical protein
			transl_table	11
>Satyrvirus_48
2	625	CDS
			locus_tag	Satyrvirus_48_1
			product	mg362 protein
			transl_table	11
1056	646	CDS
			locus_tag	Satyrvirus_48_2
			product	hypothetical protein
			transl_table	11
2371	1169	CDS
			locus_tag	Satyrvirus_48_3
			product	Adenylosuccinate synthetase
			transl_table	11
2614	2489	CDS
			locus_tag	Satyrvirus_48_4
			product	hypothetical protein
			transl_table	11
3008	2817	CDS
			locus_tag	Satyrvirus_48_5
			product	hypothetical protein
			transl_table	11
>Satyrvirus_49
1967	3	CDS
			locus_tag	Satyrvirus_49_1
			product	heat shock protein 70-like protein
			transl_table	11
2408	2166	CDS
			locus_tag	Satyrvirus_49_2
			product	hypothetical protein
			transl_table	11
2617	2901	CDS
			locus_tag	Satyrvirus_49_3
			product	hypothetical protein
			transl_table	11
>Satyrvirus_50
350	108	CDS
			locus_tag	Satyrvirus_50_1
			product	hypothetical protein
			transl_table	11
1069	566	CDS
			locus_tag	Satyrvirus_50_2
			product	hypothetical protein Klosneuvirus_6_44
			transl_table	11
1405	2367	CDS
			locus_tag	Satyrvirus_50_3
			product	hypothetical protein glt_00110
			transl_table	11
2850	2680	CDS
			locus_tag	Satyrvirus_50_4
			product	hypothetical protein
			transl_table	11
>Satyrvirus_51
3	512	CDS
			locus_tag	Satyrvirus_51_1
			product	hypothetical protein
			transl_table	11
581	2221	CDS
			locus_tag	Satyrvirus_51_2
			product	glutamine-dependent asparagine synthetase
			transl_table	11
2516	2638	CDS
			locus_tag	Satyrvirus_51_3
			product	hypothetical protein
			transl_table	11
>Satyrvirus_52
10	678	CDS
			locus_tag	Satyrvirus_52_1
			product	hypothetical protein
			transl_table	11
2631	703	CDS
			locus_tag	Satyrvirus_52_2
			product	putative ORFan
			transl_table	11
