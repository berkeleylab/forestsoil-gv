>Faunusvirus_1
3	1394	CDS
			locus_tag	Faunusvirus_1_1
			product	hypothetical protein Catovirus_1_792
			transl_table	11
1442	3157	CDS
			locus_tag	Faunusvirus_1_2
			product	hypothetical protein A3F91_08400
			transl_table	11
3743	3144	CDS
			locus_tag	Faunusvirus_1_3
			product	hypothetical protein
			transl_table	11
4822	3791	CDS
			locus_tag	Faunusvirus_1_4
			product	hypothetical protein Klosneuvirus_2_228
			transl_table	11
4907	5467	CDS
			locus_tag	Faunusvirus_1_5
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
5576	5445	CDS
			locus_tag	Faunusvirus_1_6
			product	hypothetical protein
			transl_table	11
5647	6411	CDS
			locus_tag	Faunusvirus_1_7
			product	hypothetical protein
			transl_table	11
7103	6510	CDS
			locus_tag	Faunusvirus_1_8
			product	hypothetical protein COA94_02040
			transl_table	11
7188	7496	CDS
			locus_tag	Faunusvirus_1_9
			product	hypothetical protein
			transl_table	11
7547	7747	CDS
			locus_tag	Faunusvirus_1_10
			product	hypothetical protein
			transl_table	11
7815	8384	CDS
			locus_tag	Faunusvirus_1_11
			product	hypothetical protein
			transl_table	11
8406	8732	CDS
			locus_tag	Faunusvirus_1_12
			product	hypothetical protein
			transl_table	11
8787	9464	CDS
			locus_tag	Faunusvirus_1_13
			product	hypothetical protein
			transl_table	11
9533	10339	CDS
			locus_tag	Faunusvirus_1_14
			product	hypothetical protein
			transl_table	11
11261	10386	CDS
			locus_tag	Faunusvirus_1_15
			product	hypothetical protein
			transl_table	11
11919	11320	CDS
			locus_tag	Faunusvirus_1_16
			product	hypothetical protein
			transl_table	11
12044	12889	CDS
			locus_tag	Faunusvirus_1_17
			product	hypothetical protein
			transl_table	11
12939	13760	CDS
			locus_tag	Faunusvirus_1_18
			product	hypothetical protein
			transl_table	11
13829	14647	CDS
			locus_tag	Faunusvirus_1_19
			product	hypothetical protein
			transl_table	11
15279	14644	CDS
			locus_tag	Faunusvirus_1_20
			product	PREDICTED: LOW QUALITY PROTEIN: uncharacterized protein LOC106050137
			transl_table	11
15424	16710	CDS
			locus_tag	Faunusvirus_1_21
			product	hypothetical protein
			transl_table	11
16963	16721	CDS
			locus_tag	Faunusvirus_1_22
			product	hypothetical protein
			transl_table	11
17071	17937	CDS
			locus_tag	Faunusvirus_1_23
			product	fibronectin type 3 and ankyrin repeat domains protein 1-like
			transl_table	11
18000	18881	CDS
			locus_tag	Faunusvirus_1_24
			product	ankyrin-3-like
			transl_table	11
18945	19559	CDS
			locus_tag	Faunusvirus_1_25
			product	hypothetical protein
			transl_table	11
19600	20241	CDS
			locus_tag	Faunusvirus_1_26
			product	hypothetical protein CAPTEDRAFT_86050, partial
			transl_table	11
20509	20252	CDS
			locus_tag	Faunusvirus_1_27
			product	hypothetical protein RhiirA4_510386
			transl_table	11
21779	20667	CDS
			locus_tag	Faunusvirus_1_28
			product	hypothetical protein
			transl_table	11
23531	21849	CDS
			locus_tag	Faunusvirus_1_29
			product	topoisomerase 1b
			transl_table	11
23646	24116	CDS
			locus_tag	Faunusvirus_1_30
			product	hypothetical protein
			transl_table	11
25252	24113	CDS
			locus_tag	Faunusvirus_1_31
			product	hypothetical protein
			transl_table	11
25548	25820	CDS
			locus_tag	Faunusvirus_1_32
			product	hypothetical protein Catovirus_1_796
			transl_table	11
25913	27112	CDS
			locus_tag	Faunusvirus_1_33
			product	hypothetical protein
			transl_table	11
27281	27940	CDS
			locus_tag	Faunusvirus_1_34
			product	hypothetical protein
			transl_table	11
28121	28267	CDS
			locus_tag	Faunusvirus_1_35
			product	hypothetical protein
			transl_table	11
29637	28330	CDS
			locus_tag	Faunusvirus_1_36
			product	ankyrin
			transl_table	11
30677	29682	CDS
			locus_tag	Faunusvirus_1_37
			product	putative UV-damage endonuclease
			transl_table	11
30783	31478	CDS
			locus_tag	Faunusvirus_1_38
			product	tRNA U55 pseudouridine synthase TruB
			transl_table	11
31545	31778	CDS
			locus_tag	Faunusvirus_1_39
			product	ubiquitin
			transl_table	11
33113	31767	CDS
			locus_tag	Faunusvirus_1_40
			product	flavin containing amine oxidoreductase
			transl_table	11
33348	33118	CDS
			locus_tag	Faunusvirus_1_41
			product	hypothetical protein
			transl_table	11
33342	33683	CDS
			locus_tag	Faunusvirus_1_42
			product	hypothetical protein
			transl_table	11
33789	34547	CDS
			locus_tag	Faunusvirus_1_43
			product	hypothetical protein
			transl_table	11
35534	34557	CDS
			locus_tag	Faunusvirus_1_44
			product	hypothetical protein
			transl_table	11
35684	36694	CDS
			locus_tag	Faunusvirus_1_45
			product	hypothetical protein
			transl_table	11
36812	38062	CDS
			locus_tag	Faunusvirus_1_46
			product	hypothetical protein
			transl_table	11
38178	38855	CDS
			locus_tag	Faunusvirus_1_47
			product	hypothetical protein
			transl_table	11
38949	39329	CDS
			locus_tag	Faunusvirus_1_48
			product	hypothetical protein
			transl_table	11
39429	40307	CDS
			locus_tag	Faunusvirus_1_49
			product	hypothetical protein
			transl_table	11
41361	40417	CDS
			locus_tag	Faunusvirus_1_50
			product	hypothetical protein
			transl_table	11
41429	42226	CDS
			locus_tag	Faunusvirus_1_51
			product	hypothetical protein
			transl_table	11
42909	42256	CDS
			locus_tag	Faunusvirus_1_52
			product	putative papain-like cysteine peptidase
			transl_table	11
43042	44220	CDS
			locus_tag	Faunusvirus_1_53
			product	23S rRNA (uracil(1939)-C(5))-methyltransferase RlmD
			transl_table	11
44520	44221	CDS
			locus_tag	Faunusvirus_1_54
			product	hypothetical protein
			transl_table	11
44683	46308	CDS
			locus_tag	Faunusvirus_1_55
			product	hypothetical protein
			transl_table	11
46345	46638	CDS
			locus_tag	Faunusvirus_1_56
			product	hypothetical protein
			transl_table	11
46766	47179	CDS
			locus_tag	Faunusvirus_1_57
			product	hypothetical protein
			transl_table	11
47189	47383	CDS
			locus_tag	Faunusvirus_1_58
			product	hypothetical protein
			transl_table	11
47789	47427	CDS
			locus_tag	Faunusvirus_1_59
			product	hypothetical protein
			transl_table	11
47909	48166	CDS
			locus_tag	Faunusvirus_1_60
			product	hypothetical protein
			transl_table	11
48340	49221	CDS
			locus_tag	Faunusvirus_1_61
			product	hypothetical protein
			transl_table	11
49444	49349	CDS
			locus_tag	Faunusvirus_1_62
			product	hypothetical protein
			transl_table	11
49506	50006	CDS
			locus_tag	Faunusvirus_1_63
			product	hypothetical protein
			transl_table	11
50125	50382	CDS
			locus_tag	Faunusvirus_1_64
			product	hypothetical protein
			transl_table	11
51208	50498	CDS
			locus_tag	Faunusvirus_1_65
			product	hypothetical protein
			transl_table	11
51948	51268	CDS
			locus_tag	Faunusvirus_1_66
			product	hypothetical protein VICG_00908
			transl_table	11
51954	52997	CDS
			locus_tag	Faunusvirus_1_67
			product	hypothetical protein
			transl_table	11
53106	53537	CDS
			locus_tag	Faunusvirus_1_68
			product	hypothetical protein
			transl_table	11
53615	53818	CDS
			locus_tag	Faunusvirus_1_69
			product	hypothetical protein
			transl_table	11
53893	54594	CDS
			locus_tag	Faunusvirus_1_70
			product	hypothetical protein
			transl_table	11
54733	55290	CDS
			locus_tag	Faunusvirus_1_71
			product	hypothetical protein
			transl_table	11
55411	55788	CDS
			locus_tag	Faunusvirus_1_72
			product	hypothetical protein
			transl_table	11
56369	56461	CDS
			locus_tag	Faunusvirus_1_73
			product	hypothetical protein
			transl_table	11
>Faunusvirus_2
2	469	CDS
			locus_tag	Faunusvirus_2_1
			product	hypothetical protein
			transl_table	11
563	1153	CDS
			locus_tag	Faunusvirus_2_2
			product	hypothetical protein
			transl_table	11
1220	1837	CDS
			locus_tag	Faunusvirus_2_3
			product	hypothetical protein
			transl_table	11
1897	2472	CDS
			locus_tag	Faunusvirus_2_4
			product	hypothetical protein
			transl_table	11
2585	2977	CDS
			locus_tag	Faunusvirus_2_5
			product	hypothetical protein
			transl_table	11
3131	3958	CDS
			locus_tag	Faunusvirus_2_6
			product	hypothetical protein
			transl_table	11
4190	4564	CDS
			locus_tag	Faunusvirus_2_7
			product	hypothetical protein
			transl_table	11
5156	4749	CDS
			locus_tag	Faunusvirus_2_8
			product	hypothetical protein
			transl_table	11
5284	5967	CDS
			locus_tag	Faunusvirus_2_9
			product	hypothetical protein
			transl_table	11
6359	5973	CDS
			locus_tag	Faunusvirus_2_10
			product	hypothetical protein
			transl_table	11
6450	7319	CDS
			locus_tag	Faunusvirus_2_11
			product	ankyrin
			transl_table	11
7411	8166	CDS
			locus_tag	Faunusvirus_2_12
			product	hypothetical protein
			transl_table	11
8877	8176	CDS
			locus_tag	Faunusvirus_2_13
			product	hypothetical protein Hokovirus_2_45
			transl_table	11
8948	9544	CDS
			locus_tag	Faunusvirus_2_14
			product	hypothetical protein
			transl_table	11
9620	9961	CDS
			locus_tag	Faunusvirus_2_15
			product	hypothetical protein
			transl_table	11
10582	10031	CDS
			locus_tag	Faunusvirus_2_16
			product	hypothetical protein
			transl_table	11
10727	11323	CDS
			locus_tag	Faunusvirus_2_17
			product	hypothetical protein
			transl_table	11
11675	11391	CDS
			locus_tag	Faunusvirus_2_18
			product	hypothetical protein
			transl_table	11
12604	11753	CDS
			locus_tag	Faunusvirus_2_19
			product	hypothetical protein
			transl_table	11
13830	12688	CDS
			locus_tag	Faunusvirus_2_20
			product	hypothetical protein UV38_C0002G0300
			transl_table	11
13881	14216	CDS
			locus_tag	Faunusvirus_2_21
			product	hypothetical protein
			transl_table	11
14291	14962	CDS
			locus_tag	Faunusvirus_2_22
			product	hypothetical protein
			transl_table	11
15022	15693	CDS
			locus_tag	Faunusvirus_2_23
			product	hypothetical protein
			transl_table	11
15761	16408	CDS
			locus_tag	Faunusvirus_2_24
			product	hypothetical protein
			transl_table	11
16455	17114	CDS
			locus_tag	Faunusvirus_2_25
			product	hypothetical protein
			transl_table	11
17151	18203	CDS
			locus_tag	Faunusvirus_2_26
			product	hypothetical protein
			transl_table	11
18273	18860	CDS
			locus_tag	Faunusvirus_2_27
			product	hypothetical protein
			transl_table	11
18915	19496	CDS
			locus_tag	Faunusvirus_2_28
			product	hypothetical protein
			transl_table	11
19550	20125	CDS
			locus_tag	Faunusvirus_2_29
			product	hypothetical protein
			transl_table	11
20180	20791	CDS
			locus_tag	Faunusvirus_2_30
			product	hypothetical protein
			transl_table	11
20865	21473	CDS
			locus_tag	Faunusvirus_2_31
			product	hypothetical protein
			transl_table	11
21823	21479	CDS
			locus_tag	Faunusvirus_2_32
			product	hypothetical protein
			transl_table	11
21935	22546	CDS
			locus_tag	Faunusvirus_2_33
			product	hypothetical protein
			transl_table	11
22561	23277	CDS
			locus_tag	Faunusvirus_2_34
			product	hypothetical protein
			transl_table	11
23375	23674	CDS
			locus_tag	Faunusvirus_2_35
			product	hypothetical protein
			transl_table	11
23765	24481	CDS
			locus_tag	Faunusvirus_2_36
			product	hypothetical protein
			transl_table	11
24552	24695	CDS
			locus_tag	Faunusvirus_2_37
			product	hypothetical protein
			transl_table	11
24746	25096	CDS
			locus_tag	Faunusvirus_2_38
			product	hypothetical protein
			transl_table	11
25153	25647	CDS
			locus_tag	Faunusvirus_2_39
			product	hypothetical protein
			transl_table	11
25701	26285	CDS
			locus_tag	Faunusvirus_2_40
			product	hypothetical protein
			transl_table	11
26425	27003	CDS
			locus_tag	Faunusvirus_2_41
			product	hypothetical protein
			transl_table	11
27061	27546	CDS
			locus_tag	Faunusvirus_2_42
			product	hypothetical protein
			transl_table	11
27623	28156	CDS
			locus_tag	Faunusvirus_2_43
			product	hypothetical protein
			transl_table	11
28722	28168	CDS
			locus_tag	Faunusvirus_2_44
			product	AAA domain protein
			transl_table	11
29587	28748	CDS
			locus_tag	Faunusvirus_2_45
			product	hypothetical protein
			transl_table	11
29679	30155	CDS
			locus_tag	Faunusvirus_2_46
			product	hypothetical protein
			transl_table	11
30284	30859	CDS
			locus_tag	Faunusvirus_2_47
			product	hypothetical protein
			transl_table	11
30912	31487	CDS
			locus_tag	Faunusvirus_2_48
			product	hypothetical protein
			transl_table	11
32675	31527	CDS
			locus_tag	Faunusvirus_2_49
			product	hypothetical protein
			transl_table	11
32829	33323	CDS
			locus_tag	Faunusvirus_2_50
			product	hypothetical protein
			transl_table	11
33474	33962	CDS
			locus_tag	Faunusvirus_2_51
			product	hypothetical protein
			transl_table	11
34396	33974	CDS
			locus_tag	Faunusvirus_2_52
			product	hypothetical protein
			transl_table	11
34633	35457	CDS
			locus_tag	Faunusvirus_2_53
			product	hypothetical protein
			transl_table	11
35696	35499	CDS
			locus_tag	Faunusvirus_2_54
			product	hypothetical protein
			transl_table	11
36380	35697	CDS
			locus_tag	Faunusvirus_2_55
			product	hypothetical protein
			transl_table	11
36830	37681	CDS
			locus_tag	Faunusvirus_2_56
			product	hypothetical protein
			transl_table	11
37914	38576	CDS
			locus_tag	Faunusvirus_2_57
			product	hypothetical protein
			transl_table	11
39012	38665	CDS
			locus_tag	Faunusvirus_2_58
			product	hypothetical protein
			transl_table	11
40531	39131	CDS
			locus_tag	Faunusvirus_2_59
			product	HNH endonuclease
			transl_table	11
40660	42114	CDS
			locus_tag	Faunusvirus_2_60
			product	hypothetical protein
			transl_table	11
42234	42133	CDS
			locus_tag	Faunusvirus_2_61
			product	hypothetical protein
			transl_table	11
44843	42234	CDS
			locus_tag	Faunusvirus_2_62
			product	superfamily II helicase
			transl_table	11
45001	46860	CDS
			locus_tag	Faunusvirus_2_63
			product	dynamin family GTPase
			transl_table	11
47803	46958	CDS
			locus_tag	Faunusvirus_2_64
			product	hypothetical protein
			transl_table	11
47964	48554	CDS
			locus_tag	Faunusvirus_2_65
			product	hypothetical protein
			transl_table	11
48648	50786	CDS
			locus_tag	Faunusvirus_2_66
			product	putative helicase/exonuclease
			transl_table	11
51912	50980	CDS
			locus_tag	Faunusvirus_2_67
			product	hypothetical protein
			transl_table	11
>Faunusvirus_3
104	673	CDS
			locus_tag	Faunusvirus_3_1
			product	hypothetical protein crov296
			transl_table	11
1228	689	CDS
			locus_tag	Faunusvirus_3_2
			product	hypothetical protein Catovirus_2_221
			transl_table	11
1977	1279	CDS
			locus_tag	Faunusvirus_3_3
			product	hypothetical protein
			transl_table	11
2053	2397	CDS
			locus_tag	Faunusvirus_3_4
			product	hypothetical protein
			transl_table	11
2962	2387	CDS
			locus_tag	Faunusvirus_3_5
			product	hypothetical protein A3E87_03900
			transl_table	11
3443	3018	CDS
			locus_tag	Faunusvirus_3_6
			product	hypothetical protein
			transl_table	11
9237	3424	CDS
			locus_tag	Faunusvirus_3_7
			product	early transcription factor VETF large subunit
			transl_table	11
9903	9301	CDS
			locus_tag	Faunusvirus_3_8
			product	protein of unknown function DUF45
			transl_table	11
9960	10955	CDS
			locus_tag	Faunusvirus_3_9
			product	hypothetical protein
			transl_table	11
11337	10957	CDS
			locus_tag	Faunusvirus_3_10
			product	hypothetical protein
			transl_table	11
12743	11493	CDS
			locus_tag	Faunusvirus_3_11
			product	eukaryotic translation initiation factor 2-alpha kinase 1-like
			transl_table	11
13505	12957	CDS
			locus_tag	Faunusvirus_3_12
			product	hypothetical protein
			transl_table	11
13583	14773	CDS
			locus_tag	Faunusvirus_3_13
			product	exodeoxyribonuclease VII large subunit
			transl_table	11
14817	15158	CDS
			locus_tag	Faunusvirus_3_14
			product	hypothetical protein
			transl_table	11
15172	15789	CDS
			locus_tag	Faunusvirus_3_15
			product	Ras-related protein Rab-2
			transl_table	11
15823	16395	CDS
			locus_tag	Faunusvirus_3_16
			product	hypothetical protein
			transl_table	11
17586	16399	CDS
			locus_tag	Faunusvirus_3_17
			product	serine/threonine protein kinase
			transl_table	11
17705	18472	CDS
			locus_tag	Faunusvirus_3_18
			product	hypothetical protein TSOC_013020
			transl_table	11
18558	19940	CDS
			locus_tag	Faunusvirus_3_19
			product	hypothetical protein Klosneuvirus_1_181
			transl_table	11
20004	20519	CDS
			locus_tag	Faunusvirus_3_20
			product	hypothetical protein
			transl_table	11
20677	20516	CDS
			locus_tag	Faunusvirus_3_21
			product	hypothetical protein
			transl_table	11
20800	20702	CDS
			locus_tag	Faunusvirus_3_22
			product	hypothetical protein
			transl_table	11
20962	21480	CDS
			locus_tag	Faunusvirus_3_23
			product	hypothetical protein
			transl_table	11
21558	22184	CDS
			locus_tag	Faunusvirus_3_24
			product	hypothetical protein
			transl_table	11
22212	22745	CDS
			locus_tag	Faunusvirus_3_25
			product	hypothetical protein Moumou_00182
			transl_table	11
24196	22730	CDS
			locus_tag	Faunusvirus_3_26
			product	bifunctional dihydrofolate reductase/thymidylate synthase
			transl_table	11
25648	24374	CDS
			locus_tag	Faunusvirus_3_27
			product	hypothetical protein
			transl_table	11
26166	26035	CDS
			locus_tag	Faunusvirus_3_28
			product	hypothetical protein
			transl_table	11
26564	27241	CDS
			locus_tag	Faunusvirus_3_29
			product	hypothetical protein
			transl_table	11
27463	31014	CDS
			locus_tag	Faunusvirus_3_30
			product	mRNA capping enzyme
			transl_table	11
31966	31016	CDS
			locus_tag	Faunusvirus_3_31
			product	phosphonoacetaldehyde hydrolase
			transl_table	11
32083	33483	CDS
			locus_tag	Faunusvirus_3_32
			product	FtsJ-like methyltransferase
			transl_table	11
33965	33555	CDS
			locus_tag	Faunusvirus_3_33
			product	hypothetical protein
			transl_table	11
35065	34007	CDS
			locus_tag	Faunusvirus_3_34
			product	hypothetical protein
			transl_table	11
35744	35112	CDS
			locus_tag	Faunusvirus_3_35
			product	hypothetical protein
			transl_table	11
35837	36232	CDS
			locus_tag	Faunusvirus_3_36
			product	hypothetical protein CML47_07175
			transl_table	11
36279	36734	CDS
			locus_tag	Faunusvirus_3_37
			product	hypothetical protein
			transl_table	11
36736	36837	CDS
			locus_tag	Faunusvirus_3_38
			product	hypothetical protein
			transl_table	11
37992	36856	CDS
			locus_tag	Faunusvirus_3_39
			product	hypothetical protein Catovirus_1_469
			transl_table	11
>Faunusvirus_4
193	735	CDS
			locus_tag	Faunusvirus_4_1
			product	putative ribonuclease H1
			transl_table	11
791	1336	CDS
			locus_tag	Faunusvirus_4_2
			product	hypothetical protein
			transl_table	11
1398	1925	CDS
			locus_tag	Faunusvirus_4_3
			product	hypothetical protein
			transl_table	11
1992	2558	CDS
			locus_tag	Faunusvirus_4_4
			product	hypothetical protein
			transl_table	11
2618	3061	CDS
			locus_tag	Faunusvirus_4_5
			product	hypothetical protein
			transl_table	11
3164	3640	CDS
			locus_tag	Faunusvirus_4_6
			product	hypothetical protein
			transl_table	11
3888	3643	CDS
			locus_tag	Faunusvirus_4_7
			product	hypothetical protein
			transl_table	11
4011	4760	CDS
			locus_tag	Faunusvirus_4_8
			product	hypothetical protein
			transl_table	11
4837	5571	CDS
			locus_tag	Faunusvirus_4_9
			product	hypothetical protein
			transl_table	11
5633	6421	CDS
			locus_tag	Faunusvirus_4_10
			product	hypothetical protein
			transl_table	11
6491	7102	CDS
			locus_tag	Faunusvirus_4_11
			product	hypothetical protein Catovirus_1_826
			transl_table	11
8107	7289	CDS
			locus_tag	Faunusvirus_4_12
			product	hypothetical protein
			transl_table	11
8426	8199	CDS
			locus_tag	Faunusvirus_4_13
			product	hypothetical protein
			transl_table	11
11681	8481	CDS
			locus_tag	Faunusvirus_4_14
			product	DNA mismatch repair ATPase MutS
			transl_table	11
12488	11751	CDS
			locus_tag	Faunusvirus_4_15
			product	hypothetical protein Klosneuvirus_2_193
			transl_table	11
12535	13155	CDS
			locus_tag	Faunusvirus_4_16
			product	hypothetical protein SDRG_05719
			transl_table	11
13198	14049	CDS
			locus_tag	Faunusvirus_4_17
			product	apurinic endonuclease
			transl_table	11
16666	14063	CDS
			locus_tag	Faunusvirus_4_18
			product	superfamily II helicase
			transl_table	11
16795	17409	CDS
			locus_tag	Faunusvirus_4_19
			product	hypothetical protein
			transl_table	11
17438	18019	CDS
			locus_tag	Faunusvirus_4_20
			product	hypothetical protein Klosneuvirus_4_144
			transl_table	11
18087	19136	CDS
			locus_tag	Faunusvirus_4_21
			product	Flap Endonuclease 1
			transl_table	11
21405	19126	CDS
			locus_tag	Faunusvirus_4_22
			product	putative 5'-3'exonuclease20
			transl_table	11
22713	21424	CDS
			locus_tag	Faunusvirus_4_23
			product	hypothetical protein
			transl_table	11
23465	22740	CDS
			locus_tag	Faunusvirus_4_24
			product	hypothetical protein PENSTE_c002G01918
			transl_table	11
24161	23508	CDS
			locus_tag	Faunusvirus_4_25
			product	hypothetical protein
			transl_table	11
25065	24244	CDS
			locus_tag	Faunusvirus_4_26
			product	hypothetical protein
			transl_table	11
26152	25121	CDS
			locus_tag	Faunusvirus_4_27
			product	hypothetical protein
			transl_table	11
27369	26437	CDS
			locus_tag	Faunusvirus_4_28
			product	hypothetical protein
			transl_table	11
28540	27506	CDS
			locus_tag	Faunusvirus_4_29
			product	Ankyrin repeat domain-containing protein 50
			transl_table	11
28679	29173	CDS
			locus_tag	Faunusvirus_4_30
			product	hypothetical protein
			transl_table	11
29826	29179	CDS
			locus_tag	Faunusvirus_4_31
			product	hypothetical protein
			transl_table	11
30530	30631	CDS
			locus_tag	Faunusvirus_4_32
			product	hypothetical protein
			transl_table	11
>Faunusvirus_5
2	121	CDS
			locus_tag	Faunusvirus_5_1
			product	hypothetical protein
			transl_table	11
2304	136	CDS
			locus_tag	Faunusvirus_5_2
			product	putative helicase/exonuclease
			transl_table	11
2420	3082	CDS
			locus_tag	Faunusvirus_5_3
			product	hypothetical protein
			transl_table	11
3158	3940	CDS
			locus_tag	Faunusvirus_5_4
			product	hypothetical protein
			transl_table	11
4003	4839	CDS
			locus_tag	Faunusvirus_5_5
			product	hypothetical protein
			transl_table	11
4902	5777	CDS
			locus_tag	Faunusvirus_5_6
			product	hypothetical protein
			transl_table	11
6000	5779	CDS
			locus_tag	Faunusvirus_5_7
			product	hypothetical protein
			transl_table	11
6433	8220	CDS
			locus_tag	Faunusvirus_5_8
			product	hypothetical protein
			transl_table	11
8327	9985	CDS
			locus_tag	Faunusvirus_5_9
			product	ankyrin repeat domain-containing protein
			transl_table	11
10059	11366	CDS
			locus_tag	Faunusvirus_5_10
			product	hypothetical protein
			transl_table	11
11433	11921	CDS
			locus_tag	Faunusvirus_5_11
			product	hypothetical protein
			transl_table	11
12476	11928	CDS
			locus_tag	Faunusvirus_5_12
			product	hypothetical protein
			transl_table	11
12621	13481	CDS
			locus_tag	Faunusvirus_5_13
			product	hypothetical protein
			transl_table	11
13518	14456	CDS
			locus_tag	Faunusvirus_5_14
			product	hypothetical protein
			transl_table	11
14525	15235	CDS
			locus_tag	Faunusvirus_5_15
			product	hypothetical protein
			transl_table	11
15305	16000	CDS
			locus_tag	Faunusvirus_5_16
			product	hypothetical protein
			transl_table	11
16111	16806	CDS
			locus_tag	Faunusvirus_5_17
			product	hypothetical protein
			transl_table	11
17463	16807	CDS
			locus_tag	Faunusvirus_5_18
			product	hypothetical protein
			transl_table	11
17598	18542	CDS
			locus_tag	Faunusvirus_5_19
			product	hypothetical protein
			transl_table	11
18801	19571	CDS
			locus_tag	Faunusvirus_5_20
			product	hypothetical protein
			transl_table	11
19693	20577	CDS
			locus_tag	Faunusvirus_5_21
			product	hypothetical protein
			transl_table	11
20601	21578	CDS
			locus_tag	Faunusvirus_5_22
			product	hypothetical protein
			transl_table	11
21529	22350	CDS
			locus_tag	Faunusvirus_5_23
			product	hypothetical protein
			transl_table	11
22877	22488	CDS
			locus_tag	Faunusvirus_5_24
			product	hypothetical protein
			transl_table	11
25531	23036	CDS
			locus_tag	Faunusvirus_5_25
			product	hypothetical protein Klosneuvirus_1_181
			transl_table	11
25606	26094	CDS
			locus_tag	Faunusvirus_5_26
			product	hypothetical protein Klosneuvirus_1_332
			transl_table	11
27427	26114	CDS
			locus_tag	Faunusvirus_5_27
			product	hypothetical protein DICPUDRAFT_52947
			transl_table	11
28182	28042	CDS
			locus_tag	Faunusvirus_5_28
			product	hypothetical protein
			transl_table	11
28682	28578	CDS
			locus_tag	Faunusvirus_5_29
			product	hypothetical protein
			transl_table	11
28822	28962	CDS
			locus_tag	Faunusvirus_5_30
			product	hypothetical protein
			transl_table	11
29344	29141	CDS
			locus_tag	Faunusvirus_5_31
			product	hypothetical protein
			transl_table	11
>Faunusvirus_6
1787	3	CDS
			locus_tag	Faunusvirus_6_1
			product	putative 5'-3' exonuclease
			transl_table	11
1906	2796	CDS
			locus_tag	Faunusvirus_6_2
			product	hypothetical protein
			transl_table	11
4204	3167	CDS
			locus_tag	Faunusvirus_6_3
			product	metallophosphatase/phosphoesterase
			transl_table	11
4876	4247	CDS
			locus_tag	Faunusvirus_6_4
			product	hypothetical protein
			transl_table	11
5756	4902	CDS
			locus_tag	Faunusvirus_6_5
			product	hypothetical protein
			transl_table	11
6783	5830	CDS
			locus_tag	Faunusvirus_6_6
			product	hypothetical protein
			transl_table	11
6879	8387	CDS
			locus_tag	Faunusvirus_6_7
			product	superfamily II DNA or RNA helicase
			transl_table	11
9192	8392	CDS
			locus_tag	Faunusvirus_6_8
			product	hypothetical protein
			transl_table	11
10265	9249	CDS
			locus_tag	Faunusvirus_6_9
			product	replication factor C small subunit
			transl_table	11
12150	10348	CDS
			locus_tag	Faunusvirus_6_10
			product	eukaryotic translation initiation factor 4G-like
			transl_table	11
12247	12438	CDS
			locus_tag	Faunusvirus_6_11
			product	hypothetical protein
			transl_table	11
12429	13202	CDS
			locus_tag	Faunusvirus_6_12
			product	hypothetical protein
			transl_table	11
13459	14409	CDS
			locus_tag	Faunusvirus_6_13
			product	hypothetical protein Indivirus_3_8
			transl_table	11
15187	14402	CDS
			locus_tag	Faunusvirus_6_14
			product	serine/threonine protein phosphatase
			transl_table	11
15950	15291	CDS
			locus_tag	Faunusvirus_6_15
			product	hypothetical protein
			transl_table	11
16430	16098	CDS
			locus_tag	Faunusvirus_6_16
			product	hypothetical protein
			transl_table	11
17055	16630	CDS
			locus_tag	Faunusvirus_6_17
			product	hypothetical protein
			transl_table	11
17327	17157	CDS
			locus_tag	Faunusvirus_6_18
			product	hypothetical protein
			transl_table	11
17447	17584	CDS
			locus_tag	Faunusvirus_6_19
			product	hypothetical protein
			transl_table	11
19559	17631	CDS
			locus_tag	Faunusvirus_6_20
			product	heat shock protein
			transl_table	11
19736	20767	CDS
			locus_tag	Faunusvirus_6_21
			product	hypothetical protein
			transl_table	11
20886	21902	CDS
			locus_tag	Faunusvirus_6_22
			product	hypothetical protein
			transl_table	11
21992	22978	CDS
			locus_tag	Faunusvirus_6_23
			product	hypothetical protein
			transl_table	11
23006	24262	CDS
			locus_tag	Faunusvirus_6_24
			product	hypothetical protein
			transl_table	11
24312	25439	CDS
			locus_tag	Faunusvirus_6_25
			product	hypothetical protein
			transl_table	11
25591	26274	CDS
			locus_tag	Faunusvirus_6_26
			product	metal dependent phosphohydrolase
			transl_table	11
26651	26292	CDS
			locus_tag	Faunusvirus_6_27
			product	hypothetical protein
			transl_table	11
27226	26633	CDS
			locus_tag	Faunusvirus_6_28
			product	hypothetical protein
			transl_table	11
27858	27283	CDS
			locus_tag	Faunusvirus_6_29
			product	hypothetical protein
			transl_table	11
28555	27914	CDS
			locus_tag	Faunusvirus_6_30
			product	hypothetical protein
			transl_table	11
>Faunusvirus_7
2	373	CDS
			locus_tag	Faunusvirus_7_1
			product	hypothetical protein
			transl_table	11
1261	347	CDS
			locus_tag	Faunusvirus_7_2
			product	serine hydrolase
			transl_table	11
2945	1548	CDS
			locus_tag	Faunusvirus_7_3
			product	hypothetical protein
			transl_table	11
3311	3120	CDS
			locus_tag	Faunusvirus_7_4
			product	hypothetical protein
			transl_table	11
4232	3354	CDS
			locus_tag	Faunusvirus_7_5
			product	hypothetical protein
			transl_table	11
4355	4702	CDS
			locus_tag	Faunusvirus_7_6
			product	hypothetical protein
			transl_table	11
5433	4690	CDS
			locus_tag	Faunusvirus_7_7
			product	tumor necrosis factor receptor-associated factor 6-like protein
			transl_table	11
5530	10806	CDS
			locus_tag	Faunusvirus_7_8
			product	kinesin-like protein
			transl_table	11
10873	11568	CDS
			locus_tag	Faunusvirus_7_9
			product	hypothetical protein
			transl_table	11
11740	11862	CDS
			locus_tag	Faunusvirus_7_10
			product	hypothetical protein
			transl_table	11
11916	12533	CDS
			locus_tag	Faunusvirus_7_11
			product	hypothetical protein
			transl_table	11
14305	12557	CDS
			locus_tag	Faunusvirus_7_12
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
15501	14380	CDS
			locus_tag	Faunusvirus_7_13
			product	hypothetical protein
			transl_table	11
16920	15616	CDS
			locus_tag	Faunusvirus_7_14
			product	hypothetical protein
			transl_table	11
17860	16985	CDS
			locus_tag	Faunusvirus_7_15
			product	hypothetical protein
			transl_table	11
17987	18184	CDS
			locus_tag	Faunusvirus_7_16
			product	hypothetical protein
			transl_table	11
18287	19642	CDS
			locus_tag	Faunusvirus_7_17
			product	hypothetical protein
			transl_table	11
20181	19669	CDS
			locus_tag	Faunusvirus_7_18
			product	hypothetical protein
			transl_table	11
20551	20252	CDS
			locus_tag	Faunusvirus_7_19
			product	vesicle-associated membrane protein 7
			transl_table	11
21837	20614	CDS
			locus_tag	Faunusvirus_7_20
			product	hypothetical protein Hokovirus_2_225
			transl_table	11
21891	22364	CDS
			locus_tag	Faunusvirus_7_21
			product	hypothetical protein TRFO_14805
			transl_table	11
22456	22731	CDS
			locus_tag	Faunusvirus_7_22
			product	hypothetical protein
			transl_table	11
22784	23182	CDS
			locus_tag	Faunusvirus_7_23
			product	hypothetical protein
			transl_table	11
23264	23893	CDS
			locus_tag	Faunusvirus_7_24
			product	hypothetical protein
			transl_table	11
24179	23916	CDS
			locus_tag	Faunusvirus_7_25
			product	E3 ubiquitin-protein ligase RBX1
			transl_table	11
24318	25373	CDS
			locus_tag	Faunusvirus_7_26
			product	ATPase, AAA family protein (macronuclear)
			transl_table	11
25984	25391	CDS
			locus_tag	Faunusvirus_7_27
			product	hypothetical protein
			transl_table	11
26682	26071	CDS
			locus_tag	Faunusvirus_7_28
			product	hypothetical protein
			transl_table	11
27228	26806	CDS
			locus_tag	Faunusvirus_7_29
			product	hypothetical protein
			transl_table	11
27345	27809	CDS
			locus_tag	Faunusvirus_7_30
			product	hypothetical protein
			transl_table	11
27854	27967	CDS
			locus_tag	Faunusvirus_7_31
			product	hypothetical protein
			transl_table	11
>Faunusvirus_8
3	3638	CDS
			locus_tag	Faunusvirus_8_1
			product	hypothetical protein Indivirus_4_43
			transl_table	11
3669	4166	CDS
			locus_tag	Faunusvirus_8_2
			product	hypothetical protein Moumou_00410
			transl_table	11
6050	4227	CDS
			locus_tag	Faunusvirus_8_3
			product	putative core protein
			transl_table	11
6787	6125	CDS
			locus_tag	Faunusvirus_8_4
			product	hypothetical protein
			transl_table	11
6924	8288	CDS
			locus_tag	Faunusvirus_8_5
			product	replication factor C large subunit
			transl_table	11
8906	8316	CDS
			locus_tag	Faunusvirus_8_6
			product	hypothetical protein
			transl_table	11
9033	9416	CDS
			locus_tag	Faunusvirus_8_7
			product	hypothetical protein BMW23_0534
			transl_table	11
9461	10414	CDS
			locus_tag	Faunusvirus_8_8
			product	hypothetical protein Klosneuvirus_1_20
			transl_table	11
10443	10703	CDS
			locus_tag	Faunusvirus_8_9
			product	hypothetical protein
			transl_table	11
10756	11604	CDS
			locus_tag	Faunusvirus_8_10
			product	packaging ATPase
			transl_table	11
11870	11610	CDS
			locus_tag	Faunusvirus_8_11
			product	hypothetical protein
			transl_table	11
12752	11907	CDS
			locus_tag	Faunusvirus_8_12
			product	hypothetical protein Klosneuvirus_1_24
			transl_table	11
13635	12832	CDS
			locus_tag	Faunusvirus_8_13
			product	hypothetical protein
			transl_table	11
14551	13736	CDS
			locus_tag	Faunusvirus_8_14
			product	hypothetical protein
			transl_table	11
14654	14785	CDS
			locus_tag	Faunusvirus_8_15
			product	hypothetical protein
			transl_table	11
14827	15648	CDS
			locus_tag	Faunusvirus_8_16
			product	hypothetical protein
			transl_table	11
15723	16178	CDS
			locus_tag	Faunusvirus_8_17
			product	hypothetical protein
			transl_table	11
16587	17036	CDS
			locus_tag	Faunusvirus_8_18
			product	hypothetical protein
			transl_table	11
17097	17921	CDS
			locus_tag	Faunusvirus_8_19
			product	hypothetical protein
			transl_table	11
18030	18602	CDS
			locus_tag	Faunusvirus_8_20
			product	hypothetical protein
			transl_table	11
18708	19322	CDS
			locus_tag	Faunusvirus_8_21
			product	hypothetical protein
			transl_table	11
19435	19986	CDS
			locus_tag	Faunusvirus_8_22
			product	hypothetical protein
			transl_table	11
20096	20794	CDS
			locus_tag	Faunusvirus_8_23
			product	hypothetical protein
			transl_table	11
20873	21577	CDS
			locus_tag	Faunusvirus_8_24
			product	hypothetical protein
			transl_table	11
21669	21580	CDS
			locus_tag	Faunusvirus_8_25
			product	hypothetical protein
			transl_table	11
21660	22259	CDS
			locus_tag	Faunusvirus_8_26
			product	hypothetical protein
			transl_table	11
22394	23011	CDS
			locus_tag	Faunusvirus_8_27
			product	hypothetical protein
			transl_table	11
23139	23498	CDS
			locus_tag	Faunusvirus_8_28
			product	hypothetical protein
			transl_table	11
23555	23872	CDS
			locus_tag	Faunusvirus_8_29
			product	hypothetical protein
			transl_table	11
23920	24264	CDS
			locus_tag	Faunusvirus_8_30
			product	hypothetical protein
			transl_table	11
24335	24679	CDS
			locus_tag	Faunusvirus_8_31
			product	hypothetical protein
			transl_table	11
24734	25069	CDS
			locus_tag	Faunusvirus_8_32
			product	hypothetical protein
			transl_table	11
25125	25472	CDS
			locus_tag	Faunusvirus_8_33
			product	hypothetical protein
			transl_table	11
25536	25718	CDS
			locus_tag	Faunusvirus_8_34
			product	hypothetical protein
			transl_table	11
25950	26294	CDS
			locus_tag	Faunusvirus_8_35
			product	hypothetical protein
			transl_table	11
26363	26707	CDS
			locus_tag	Faunusvirus_8_36
			product	hypothetical protein
			transl_table	11
26784	27149	CDS
			locus_tag	Faunusvirus_8_37
			product	hypothetical protein
			transl_table	11
27199	27576	CDS
			locus_tag	Faunusvirus_8_38
			product	hypothetical protein
			transl_table	11
27625	27984	CDS
			locus_tag	Faunusvirus_8_39
			product	hypothetical protein
			transl_table	11
>Faunusvirus_9
747	1	CDS
			locus_tag	Faunusvirus_9_1
			product	hypothetical protein
			transl_table	11
902	4564	CDS
			locus_tag	Faunusvirus_9_2
			product	DNA polymerase family B elongation subunit
			transl_table	11
5266	4559	CDS
			locus_tag	Faunusvirus_9_3
			product	hypothetical protein
			transl_table	11
6308	5385	CDS
			locus_tag	Faunusvirus_9_4
			product	hypothetical protein
			transl_table	11
7476	6370	CDS
			locus_tag	Faunusvirus_9_5
			product	hypothetical protein B6D44_09080
			transl_table	11
7812	7711	CDS
			locus_tag	Faunusvirus_9_6
			product	hypothetical protein
			transl_table	11
8196	10649	CDS
			locus_tag	Faunusvirus_9_7
			product	Hypothetical protein PACV_40
			transl_table	11
11089	10646	CDS
			locus_tag	Faunusvirus_9_8
			product	Eukaryotic translation initiation factor 1A, X-chromosomal, partial
			transl_table	11
11425	14064	CDS
			locus_tag	Faunusvirus_9_9
			product	Serine/threonine-protein phosphatase 6 regulatory ankyrin repeat subunit B
			transl_table	11
14178	16718	CDS
			locus_tag	Faunusvirus_9_10
			product	hypothetical protein Indivirus_2_58
			transl_table	11
17364	16750	CDS
			locus_tag	Faunusvirus_9_11
			product	hypothetical protein
			transl_table	11
17861	17421	CDS
			locus_tag	Faunusvirus_9_12
			product	hypothetical protein
			transl_table	11
17989	20391	CDS
			locus_tag	Faunusvirus_9_13
			product	RING finger domain protein
			transl_table	11
21980	20427	CDS
			locus_tag	Faunusvirus_9_14
			product	hypothetical protein
			transl_table	11
23786	22038	CDS
			locus_tag	Faunusvirus_9_15
			product	hypothetical protein
			transl_table	11
>Faunusvirus_10
1390	2	CDS
			locus_tag	Faunusvirus_10_1
			product	ankyrin repeat protein
			transl_table	11
3929	1434	CDS
			locus_tag	Faunusvirus_10_2
			product	ribonucleoside-diphosphate reductase subunit alpha
			transl_table	11
3995	4225	CDS
			locus_tag	Faunusvirus_10_3
			product	hypothetical protein
			transl_table	11
4230	5531	CDS
			locus_tag	Faunusvirus_10_4
			product	hypothetical protein
			transl_table	11
5645	6583	CDS
			locus_tag	Faunusvirus_10_5
			product	hypothetical protein
			transl_table	11
6629	7609	CDS
			locus_tag	Faunusvirus_10_6
			product	hypothetical protein
			transl_table	11
7702	8685	CDS
			locus_tag	Faunusvirus_10_7
			product	MULTISPECIES: ribonucleoside-diphosphate reductase
			transl_table	11
8723	9352	CDS
			locus_tag	Faunusvirus_10_8
			product	hypothetical protein
			transl_table	11
12164	9582	CDS
			locus_tag	Faunusvirus_10_9
			product	hypothetical protein M427DRAFT_147307
			transl_table	11
12251	12736	CDS
			locus_tag	Faunusvirus_10_10
			product	hypothetical protein
			transl_table	11
13780	12776	CDS
			locus_tag	Faunusvirus_10_11
			product	hypothetical protein
			transl_table	11
14062	13856	CDS
			locus_tag	Faunusvirus_10_12
			product	hypothetical protein
			transl_table	11
14153	14548	CDS
			locus_tag	Faunusvirus_10_13
			product	hypothetical protein
			transl_table	11
14637	15710	CDS
			locus_tag	Faunusvirus_10_14
			product	hypothetical protein
			transl_table	11
16760	15723	CDS
			locus_tag	Faunusvirus_10_15
			product	hypothetical protein PBRA_006783
			transl_table	11
16905	17357	CDS
			locus_tag	Faunusvirus_10_16
			product	hypothetical protein
			transl_table	11
17417	17866	CDS
			locus_tag	Faunusvirus_10_17
			product	hypothetical protein
			transl_table	11
17966	19492	CDS
			locus_tag	Faunusvirus_10_18
			product	hypothetical protein
			transl_table	11
20052	19546	CDS
			locus_tag	Faunusvirus_10_19
			product	hypothetical protein
			transl_table	11
20143	20607	CDS
			locus_tag	Faunusvirus_10_20
			product	hypothetical protein
			transl_table	11
20679	21152	CDS
			locus_tag	Faunusvirus_10_21
			product	hypothetical protein
			transl_table	11
21225	22103	CDS
			locus_tag	Faunusvirus_10_22
			product	hypothetical protein
			transl_table	11
22182	23423	CDS
			locus_tag	Faunusvirus_10_23
			product	hypothetical protein
			transl_table	11
23496	23744	CDS
			locus_tag	Faunusvirus_10_24
			product	hypothetical protein
			transl_table	11
>Faunusvirus_11
741	1	CDS
			locus_tag	Faunusvirus_11_1
			product	hypothetical protein
			transl_table	11
891	2468	CDS
			locus_tag	Faunusvirus_11_2
			product	hypothetical protein
			transl_table	11
3230	2484	CDS
			locus_tag	Faunusvirus_11_3
			product	hypothetical protein
			transl_table	11
3264	3440	CDS
			locus_tag	Faunusvirus_11_4
			product	hypothetical protein
			transl_table	11
4197	3433	CDS
			locus_tag	Faunusvirus_11_5
			product	hypothetical protein
			transl_table	11
4321	4989	CDS
			locus_tag	Faunusvirus_11_6
			product	hypothetical protein
			transl_table	11
5743	5009	CDS
			locus_tag	Faunusvirus_11_7
			product	hypothetical protein
			transl_table	11
6331	5762	CDS
			locus_tag	Faunusvirus_11_8
			product	hypothetical protein
			transl_table	11
6756	6430	CDS
			locus_tag	Faunusvirus_11_9
			product	hypothetical protein
			transl_table	11
7192	6842	CDS
			locus_tag	Faunusvirus_11_10
			product	hypothetical protein
			transl_table	11
7928	7302	CDS
			locus_tag	Faunusvirus_11_11
			product	hypothetical protein
			transl_table	11
9301	8012	CDS
			locus_tag	Faunusvirus_11_12
			product	hypothetical protein
			transl_table	11
10881	10126	CDS
			locus_tag	Faunusvirus_11_13
			product	hypothetical protein
			transl_table	11
11979	10954	CDS
			locus_tag	Faunusvirus_11_14
			product	hypothetical protein
			transl_table	11
13021	12053	CDS
			locus_tag	Faunusvirus_11_15
			product	hypothetical protein
			transl_table	11
13740	13132	CDS
			locus_tag	Faunusvirus_11_16
			product	hypothetical protein
			transl_table	11
13853	14470	CDS
			locus_tag	Faunusvirus_11_17
			product	hypothetical protein
			transl_table	11
15112	14474	CDS
			locus_tag	Faunusvirus_11_18
			product	hypothetical protein
			transl_table	11
15843	15232	CDS
			locus_tag	Faunusvirus_11_19
			product	hypothetical protein
			transl_table	11
16490	15897	CDS
			locus_tag	Faunusvirus_11_20
			product	PREDICTED: serine/threonine-protein phosphatase 6 regulatory ankyrin repeat subunit B-like
			transl_table	11
17996	16572	CDS
			locus_tag	Faunusvirus_11_21
			product	hypothetical protein
			transl_table	11
18082	19020	CDS
			locus_tag	Faunusvirus_11_22
			product	hypothetical protein
			transl_table	11
19866	19021	CDS
			locus_tag	Faunusvirus_11_23
			product	hypothetical protein
			transl_table	11
20514	20017	CDS
			locus_tag	Faunusvirus_11_24
			product	hypothetical protein
			transl_table	11
21487	20606	CDS
			locus_tag	Faunusvirus_11_25
			product	hypothetical protein
			transl_table	11
22089	21484	CDS
			locus_tag	Faunusvirus_11_26
			product	hypothetical protein
			transl_table	11
>Faunusvirus_12
2	796	CDS
			locus_tag	Faunusvirus_12_1
			product	hypothetical protein
			transl_table	11
875	1708	CDS
			locus_tag	Faunusvirus_12_2
			product	putative TATA-box binding protein
			transl_table	11
4641	2053	CDS
			locus_tag	Faunusvirus_12_3
			product	hypothetical protein Indivirus_1_22
			transl_table	11
5324	4707	CDS
			locus_tag	Faunusvirus_12_4
			product	hypothetical protein crov186
			transl_table	11
5755	5363	CDS
			locus_tag	Faunusvirus_12_5
			product	hypothetical protein
			transl_table	11
6383	5817	CDS
			locus_tag	Faunusvirus_12_6
			product	hypothetical protein
			transl_table	11
6448	6963	CDS
			locus_tag	Faunusvirus_12_7
			product	hypothetical protein
			transl_table	11
7571	6951	CDS
			locus_tag	Faunusvirus_12_8
			product	hypothetical protein
			transl_table	11
7994	7608	CDS
			locus_tag	Faunusvirus_12_9
			product	thioredoxin-like protein
			transl_table	11
9163	8102	CDS
			locus_tag	Faunusvirus_12_10
			product	hypothetical protein
			transl_table	11
9481	10419	CDS
			locus_tag	Faunusvirus_12_11
			product	hypothetical protein
			transl_table	11
10968	10567	CDS
			locus_tag	Faunusvirus_12_12
			product	hypothetical protein
			transl_table	11
12652	11015	CDS
			locus_tag	Faunusvirus_12_13
			product	zinc finger protein
			transl_table	11
12846	12944	CDS
			locus_tag	Faunusvirus_12_14
			product	hypothetical protein
			transl_table	11
13990	14094	CDS
			locus_tag	Faunusvirus_12_15
			product	hypothetical protein
			transl_table	11
15072	15221	CDS
			locus_tag	Faunusvirus_12_16
			product	hypothetical protein
			transl_table	11
15307	15483	CDS
			locus_tag	Faunusvirus_12_17
			product	hypothetical protein
			transl_table	11
15467	16222	CDS
			locus_tag	Faunusvirus_12_18
			product	ankyrin repeat protein
			transl_table	11
16351	17571	CDS
			locus_tag	Faunusvirus_12_19
			product	dnaJ homolog subfamily A member 2
			transl_table	11
19688	17577	CDS
			locus_tag	Faunusvirus_12_20
			product	ADP-ribosyltransferase exoenzyme domain protein
			transl_table	11
>Faunusvirus_13
883	2	CDS
			locus_tag	Faunusvirus_13_1
			product	hypothetical protein
			transl_table	11
1730	993	CDS
			locus_tag	Faunusvirus_13_2
			product	hypothetical protein
			transl_table	11
2551	1826	CDS
			locus_tag	Faunusvirus_13_3
			product	hypothetical protein
			transl_table	11
2869	3069	CDS
			locus_tag	Faunusvirus_13_4
			product	hypothetical protein
			transl_table	11
3454	3107	CDS
			locus_tag	Faunusvirus_13_5
			product	hypothetical protein
			transl_table	11
3879	3532	CDS
			locus_tag	Faunusvirus_13_6
			product	hypothetical protein
			transl_table	11
4653	3922	CDS
			locus_tag	Faunusvirus_13_7
			product	hypothetical protein
			transl_table	11
5496	4708	CDS
			locus_tag	Faunusvirus_13_8
			product	hypothetical protein
			transl_table	11
6350	5550	CDS
			locus_tag	Faunusvirus_13_9
			product	hypothetical protein
			transl_table	11
7169	6405	CDS
			locus_tag	Faunusvirus_13_10
			product	hypothetical protein
			transl_table	11
7972	7217	CDS
			locus_tag	Faunusvirus_13_11
			product	hypothetical protein
			transl_table	11
8838	8029	CDS
			locus_tag	Faunusvirus_13_12
			product	hypothetical protein
			transl_table	11
9647	8886	CDS
			locus_tag	Faunusvirus_13_13
			product	hypothetical protein
			transl_table	11
10435	9695	CDS
			locus_tag	Faunusvirus_13_14
			product	hypothetical protein
			transl_table	11
10612	11289	CDS
			locus_tag	Faunusvirus_13_15
			product	hypothetical protein
			transl_table	11
12255	11356	CDS
			locus_tag	Faunusvirus_13_16
			product	hypothetical protein
			transl_table	11
13339	12593	CDS
			locus_tag	Faunusvirus_13_17
			product	hypothetical protein
			transl_table	11
13426	13536	CDS
			locus_tag	Faunusvirus_13_18
			product	hypothetical protein
			transl_table	11
13581	14354	CDS
			locus_tag	Faunusvirus_13_19
			product	hypothetical protein
			transl_table	11
14659	14360	CDS
			locus_tag	Faunusvirus_13_20
			product	hypothetical protein
			transl_table	11
15101	14979	CDS
			locus_tag	Faunusvirus_13_21
			product	hypothetical protein
			transl_table	11
15698	15162	CDS
			locus_tag	Faunusvirus_13_22
			product	hypothetical protein
			transl_table	11
15873	16334	CDS
			locus_tag	Faunusvirus_13_23
			product	hypothetical protein
			transl_table	11
17097	16372	CDS
			locus_tag	Faunusvirus_13_24
			product	hypothetical protein
			transl_table	11
17903	17154	CDS
			locus_tag	Faunusvirus_13_25
			product	hypothetical protein
			transl_table	11
18718	17960	CDS
			locus_tag	Faunusvirus_13_26
			product	hypothetical protein
			transl_table	11
>Faunusvirus_14
669	1	CDS
			locus_tag	Faunusvirus_14_1
			product	poxvirus poly(A) polymerase catalytic subunit-like protein
			transl_table	11
1327	704	CDS
			locus_tag	Faunusvirus_14_2
			product	hypothetical protein
			transl_table	11
1450	1977	CDS
			locus_tag	Faunusvirus_14_3
			product	hypothetical protein
			transl_table	11
2355	3068	CDS
			locus_tag	Faunusvirus_14_4
			product	hypothetical protein Klosneuvirus_4_72
			transl_table	11
5177	3255	CDS
			locus_tag	Faunusvirus_14_5
			product	NAD-dependent DNA ligase
			transl_table	11
7973	5253	CDS
			locus_tag	Faunusvirus_14_6
			product	hypothetical protein
			transl_table	11
10548	8137	CDS
			locus_tag	Faunusvirus_14_7
			product	hypothetical protein
			transl_table	11
11481	10639	CDS
			locus_tag	Faunusvirus_14_8
			product	hypothetical protein
			transl_table	11
12878	11661	CDS
			locus_tag	Faunusvirus_14_9
			product	hypothetical protein
			transl_table	11
13579	12980	CDS
			locus_tag	Faunusvirus_14_10
			product	hypothetical protein
			transl_table	11
13727	14974	CDS
			locus_tag	Faunusvirus_14_11
			product	hypothetical protein BB559_004070
			transl_table	11
15081	16283	CDS
			locus_tag	Faunusvirus_14_12
			product	hypothetical protein
			transl_table	11
16368	16970	CDS
			locus_tag	Faunusvirus_14_13
			product	hypothetical protein
			transl_table	11
17158	17718	CDS
			locus_tag	Faunusvirus_14_14
			product	hypothetical protein
			transl_table	11
17816	18340	CDS
			locus_tag	Faunusvirus_14_15
			product	hypothetical protein
			transl_table	11
18667	18347	CDS
			locus_tag	Faunusvirus_14_16
			product	hypothetical protein
			transl_table	11
>Faunusvirus_15
778	53	CDS
			locus_tag	Faunusvirus_15_1
			product	hypothetical protein
			transl_table	11
1578	865	CDS
			locus_tag	Faunusvirus_15_2
			product	hypothetical protein CBC78_07235
			transl_table	11
2522	1611	CDS
			locus_tag	Faunusvirus_15_3
			product	hypothetical protein SAGO17_0077
			transl_table	11
2610	3074	CDS
			locus_tag	Faunusvirus_15_4
			product	hypothetical protein
			transl_table	11
3107	3328	CDS
			locus_tag	Faunusvirus_15_5
			product	hypothetical protein
			transl_table	11
3424	4236	CDS
			locus_tag	Faunusvirus_15_6
			product	hypothetical protein
			transl_table	11
5289	4276	CDS
			locus_tag	Faunusvirus_15_7
			product	UV excision repair protein RAD23 homolog A-like isoform X1
			transl_table	11
6049	5363	CDS
			locus_tag	Faunusvirus_15_8
			product	DNA-directed RNA polymerase II complex subunit Rpb2
			transl_table	11
6766	6332	CDS
			locus_tag	Faunusvirus_15_9
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
10165	7277	CDS
			locus_tag	Faunusvirus_15_10
			product	intein-containing DNA-dependent RNA polymerase 2 subunit Rpb2/subunit Rpb4 precursor
			transl_table	11
10294	11019	CDS
			locus_tag	Faunusvirus_15_11
			product	uracil-DNA glycosylase
			transl_table	11
12364	11003	CDS
			locus_tag	Faunusvirus_15_12
			product	uncharacterized HNH endonuclease
			transl_table	11
12514	13239	CDS
			locus_tag	Faunusvirus_15_13
			product	hypothetical protein
			transl_table	11
13298	14023	CDS
			locus_tag	Faunusvirus_15_14
			product	hypothetical protein
			transl_table	11
15076	14051	CDS
			locus_tag	Faunusvirus_15_15
			product	hypothetical protein
			transl_table	11
15239	16321	CDS
			locus_tag	Faunusvirus_15_16
			product	transcription initiation factor IIB
			transl_table	11
16802	16308	CDS
			locus_tag	Faunusvirus_15_17
			product	hypothetical protein
			transl_table	11
>Faunusvirus_16
698	3	CDS
			locus_tag	Faunusvirus_16_1
			product	hypothetical protein
			transl_table	11
2100	910	CDS
			locus_tag	Faunusvirus_16_2
			product	hypothetical protein
			transl_table	11
3031	2318	CDS
			locus_tag	Faunusvirus_16_3
			product	hypothetical protein
			transl_table	11
3151	3321	CDS
			locus_tag	Faunusvirus_16_4
			product	hypothetical protein
			transl_table	11
3558	3719	CDS
			locus_tag	Faunusvirus_16_5
			product	hypothetical protein
			transl_table	11
4033	4647	CDS
			locus_tag	Faunusvirus_16_6
			product	hypothetical protein
			transl_table	11
4753	4628	CDS
			locus_tag	Faunusvirus_16_7
			product	hypothetical protein
			transl_table	11
5937	4969	CDS
			locus_tag	Faunusvirus_16_8
			product	hypothetical protein
			transl_table	11
7148	6273	CDS
			locus_tag	Faunusvirus_16_9
			product	hypothetical protein
			transl_table	11
8603	7203	CDS
			locus_tag	Faunusvirus_16_10
			product	hypothetical protein
			transl_table	11
10243	8975	CDS
			locus_tag	Faunusvirus_16_11
			product	hypothetical protein
			transl_table	11
10824	10348	CDS
			locus_tag	Faunusvirus_16_12
			product	hypothetical protein
			transl_table	11
12119	10923	CDS
			locus_tag	Faunusvirus_16_13
			product	hypothetical protein
			transl_table	11
12814	12191	CDS
			locus_tag	Faunusvirus_16_14
			product	hypothetical protein
			transl_table	11
14007	12898	CDS
			locus_tag	Faunusvirus_16_15
			product	hypothetical protein
			transl_table	11
>Faunusvirus_17
117	542	CDS
			locus_tag	Faunusvirus_17_1
			product	putative N-acetyltransferase
			transl_table	11
611	2209	CDS
			locus_tag	Faunusvirus_17_2
			product	hypothetical protein
			transl_table	11
2273	2635	CDS
			locus_tag	Faunusvirus_17_3
			product	hypothetical protein
			transl_table	11
2610	2933	CDS
			locus_tag	Faunusvirus_17_4
			product	hypothetical protein
			transl_table	11
3455	2961	CDS
			locus_tag	Faunusvirus_17_5
			product	hypothetical protein
			transl_table	11
3564	6284	CDS
			locus_tag	Faunusvirus_17_6
			product	Phosphatidylinositol kinase
			transl_table	11
6379	7083	CDS
			locus_tag	Faunusvirus_17_7
			product	hypothetical protein
			transl_table	11
7537	7130	CDS
			locus_tag	Faunusvirus_17_8
			product	hypothetical protein
			transl_table	11
8227	7589	CDS
			locus_tag	Faunusvirus_17_9
			product	hypothetical protein
			transl_table	11
8689	8312	CDS
			locus_tag	Faunusvirus_17_10
			product	hypothetical protein
			transl_table	11
9098	8805	CDS
			locus_tag	Faunusvirus_17_11
			product	hypothetical protein
			transl_table	11
9240	12470	CDS
			locus_tag	Faunusvirus_17_12
			product	isoleucine--tRNA ligase, cytoplasmic
			transl_table	11
12715	13254	CDS
			locus_tag	Faunusvirus_17_13
			product	hypothetical protein
			transl_table	11
13657	13824	CDS
			locus_tag	Faunusvirus_17_14
			product	hypothetical protein
			transl_table	11
>Faunusvirus_18
1	609	CDS
			locus_tag	Faunusvirus_18_1
			product	putative polyadenylate polymerase catalytic subunit
			transl_table	11
671	1627	CDS
			locus_tag	Faunusvirus_18_2
			product	putative dsRNA-specific ribonuclease III
			transl_table	11
2209	1934	CDS
			locus_tag	Faunusvirus_18_3
			product	hypothetical protein
			transl_table	11
2274	2924	CDS
			locus_tag	Faunusvirus_18_4
			product	hypothetical protein
			transl_table	11
2998	5391	CDS
			locus_tag	Faunusvirus_18_5
			product	DEAD/SNF2-like helicase
			transl_table	11
5471	6292	CDS
			locus_tag	Faunusvirus_18_6
			product	hypothetical protein, partial
			transl_table	11
6373	8058	CDS
			locus_tag	Faunusvirus_18_7
			product	hypothetical protein CVV05_00120
			transl_table	11
8154	8053	CDS
			locus_tag	Faunusvirus_18_8
			product	hypothetical protein
			transl_table	11
8153	9382	CDS
			locus_tag	Faunusvirus_18_9
			product	hypothetical protein
			transl_table	11
9433	10494	CDS
			locus_tag	Faunusvirus_18_10
			product	ankyrin, partial
			transl_table	11
10569	10793	CDS
			locus_tag	Faunusvirus_18_11
			product	hypothetical protein
			transl_table	11
10862	11062	CDS
			locus_tag	Faunusvirus_18_12
			product	hypothetical protein
			transl_table	11
11764	11120	CDS
			locus_tag	Faunusvirus_18_13
			product	hypothetical protein
			transl_table	11
12402	11836	CDS
			locus_tag	Faunusvirus_18_14
			product	hypothetical protein
			transl_table	11
13089	12475	CDS
			locus_tag	Faunusvirus_18_15
			product	hypothetical protein
			transl_table	11
>Faunusvirus_19
5174	3	CDS
			locus_tag	Faunusvirus_19_1
			product	HrpA-like RNA helicase
			transl_table	11
5631	5242	CDS
			locus_tag	Faunusvirus_19_2
			product	hypothetical protein
			transl_table	11
5659	6435	CDS
			locus_tag	Faunusvirus_19_3
			product	hypothetical protein
			transl_table	11
6518	7711	CDS
			locus_tag	Faunusvirus_19_4
			product	hypothetical protein
			transl_table	11
7766	8686	CDS
			locus_tag	Faunusvirus_19_5
			product	hypothetical protein
			transl_table	11
10387	8765	CDS
			locus_tag	Faunusvirus_19_6
			product	transposase
			transl_table	11
10584	11855	CDS
			locus_tag	Faunusvirus_19_7
			product	HD phosphohydrolase domain-containing protein
			transl_table	11
>Faunusvirus_20
2	424	CDS
			locus_tag	Faunusvirus_20_1
			product	hypothetical protein
			transl_table	11
475	1353	CDS
			locus_tag	Faunusvirus_20_2
			product	hypothetical protein
			transl_table	11
1404	2150	CDS
			locus_tag	Faunusvirus_20_3
			product	hypothetical protein
			transl_table	11
2206	2802	CDS
			locus_tag	Faunusvirus_20_4
			product	hypothetical protein
			transl_table	11
3079	2888	CDS
			locus_tag	Faunusvirus_20_5
			product	hypothetical protein
			transl_table	11
4123	3215	CDS
			locus_tag	Faunusvirus_20_6
			product	PREDICTED: ankyrin repeat and SOCS box protein 15 isoform X1
			transl_table	11
4267	4659	CDS
			locus_tag	Faunusvirus_20_7
			product	hypothetical protein
			transl_table	11
4767	5441	CDS
			locus_tag	Faunusvirus_20_8
			product	hypothetical protein
			transl_table	11
5566	5802	CDS
			locus_tag	Faunusvirus_20_9
			product	hypothetical protein ASPACDRAFT_47324
			transl_table	11
5875	6486	CDS
			locus_tag	Faunusvirus_20_10
			product	hypothetical protein
			transl_table	11
6618	7229	CDS
			locus_tag	Faunusvirus_20_11
			product	hypothetical protein
			transl_table	11
7330	7989	CDS
			locus_tag	Faunusvirus_20_12
			product	hypothetical protein
			transl_table	11
8076	8645	CDS
			locus_tag	Faunusvirus_20_13
			product	hypothetical protein
			transl_table	11
9445	8663	CDS
			locus_tag	Faunusvirus_20_14
			product	hypothetical protein
			transl_table	11
9570	10181	CDS
			locus_tag	Faunusvirus_20_15
			product	hypothetical protein
			transl_table	11
10199	10840	CDS
			locus_tag	Faunusvirus_20_16
			product	hypothetical protein
			transl_table	11
10966	11988	CDS
			locus_tag	Faunusvirus_20_17
			product	hypothetical protein
			transl_table	11
12762	12055	CDS
			locus_tag	Faunusvirus_20_18
			product	hypothetical protein
			transl_table	11
12761	12853	CDS
			locus_tag	Faunusvirus_20_19
			product	hypothetical protein
			transl_table	11
>Faunusvirus_21
3	146	CDS
			locus_tag	Faunusvirus_21_1
			product	hypothetical protein
			transl_table	11
617	150	CDS
			locus_tag	Faunusvirus_21_2
			product	hypothetical protein
			transl_table	11
694	1404	CDS
			locus_tag	Faunusvirus_21_3
			product	DNA-directed RNA polymerase, subunit E'/Rpb8
			transl_table	11
1509	2633	CDS
			locus_tag	Faunusvirus_21_4
			product	NUDIX hydrolase
			transl_table	11
3712	2630	CDS
			locus_tag	Faunusvirus_21_5
			product	hypothetical protein
			transl_table	11
4437	3787	CDS
			locus_tag	Faunusvirus_21_6
			product	hypothetical protein
			transl_table	11
5465	4458	CDS
			locus_tag	Faunusvirus_21_7
			product	hypothetical protein
			transl_table	11
6525	5554	CDS
			locus_tag	Faunusvirus_21_8
			product	ankyrin repeat domain-containing protein
			transl_table	11
7576	6608	CDS
			locus_tag	Faunusvirus_21_9
			product	ankyrin repeat domain-containing protein
			transl_table	11
7649	8395	CDS
			locus_tag	Faunusvirus_21_10
			product	hypothetical protein
			transl_table	11
8619	10901	CDS
			locus_tag	Faunusvirus_21_11
			product	ankyrin repeat protein
			transl_table	11
11360	10941	CDS
			locus_tag	Faunusvirus_21_12
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
12664	11414	CDS
			locus_tag	Faunusvirus_21_13
			product	hypothetical protein Klosneuvirus_2_187
			transl_table	11
>Faunusvirus_22
2	1393	CDS
			locus_tag	Faunusvirus_22_1
			product	AAA family ATPase
			transl_table	11
2433	1390	CDS
			locus_tag	Faunusvirus_22_2
			product	hypothetical protein Klosneuvirus_1_52
			transl_table	11
2597	3544	CDS
			locus_tag	Faunusvirus_22_3
			product	hypothetical protein
			transl_table	11
5000	3729	CDS
			locus_tag	Faunusvirus_22_4
			product	glycosyltransferase
			transl_table	11
5290	5069	CDS
			locus_tag	Faunusvirus_22_5
			product	hypothetical protein
			transl_table	11
5505	5320	CDS
			locus_tag	Faunusvirus_22_6
			product	hypothetical protein
			transl_table	11
5553	6041	CDS
			locus_tag	Faunusvirus_22_7
			product	hypothetical protein MVEG_07395
			transl_table	11
6063	6554	CDS
			locus_tag	Faunusvirus_22_8
			product	hypothetical protein
			transl_table	11
7461	6547	CDS
			locus_tag	Faunusvirus_22_9
			product	hypothetical protein
			transl_table	11
7607	8620	CDS
			locus_tag	Faunusvirus_22_10
			product	hypothetical protein
			transl_table	11
8674	9012	CDS
			locus_tag	Faunusvirus_22_11
			product	hypothetical protein
			transl_table	11
9043	9369	CDS
			locus_tag	Faunusvirus_22_12
			product	hypothetical protein
			transl_table	11
9506	10642	CDS
			locus_tag	Faunusvirus_22_13
			product	hypothetical protein
			transl_table	11
12380	10794	CDS
			locus_tag	Faunusvirus_22_14
			product	hypothetical protein Catovirus_1_954
			transl_table	11
>Faunusvirus_23
3	1574	CDS
			locus_tag	Faunusvirus_23_1
			product	superfamily II helicase
			transl_table	11
1571	2338	CDS
			locus_tag	Faunusvirus_23_2
			product	superfamily II helicase
			transl_table	11
4917	2380	CDS
			locus_tag	Faunusvirus_23_3
			product	hypothetical protein
			transl_table	11
5013	6242	CDS
			locus_tag	Faunusvirus_23_4
			product	hypothetical protein glt_00457
			transl_table	11
7110	6244	CDS
			locus_tag	Faunusvirus_23_5
			product	hypothetical protein
			transl_table	11
8238	7192	CDS
			locus_tag	Faunusvirus_23_6
			product	hypothetical protein
			transl_table	11
9164	8310	CDS
			locus_tag	Faunusvirus_23_7
			product	hypothetical protein
			transl_table	11
9980	9222	CDS
			locus_tag	Faunusvirus_23_8
			product	hypothetical protein
			transl_table	11
10833	10009	CDS
			locus_tag	Faunusvirus_23_9
			product	hypothetical protein
			transl_table	11
11017	11496	CDS
			locus_tag	Faunusvirus_23_10
			product	hypothetical protein
			transl_table	11
11857	11979	CDS
			locus_tag	Faunusvirus_23_11
			product	hypothetical protein
			transl_table	11
>Faunusvirus_24
425	3	CDS
			locus_tag	Faunusvirus_24_1
			product	hypothetical protein
			transl_table	11
2075	1017	CDS
			locus_tag	Faunusvirus_24_2
			product	hypothetical protein Klosneuvirus_1_204
			transl_table	11
2233	3231	CDS
			locus_tag	Faunusvirus_24_3
			product	hypothetical protein
			transl_table	11
7761	3463	CDS
			locus_tag	Faunusvirus_24_4
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
9789	8200	CDS
			locus_tag	Faunusvirus_24_5
			product	DNA-directed RNA polymerase subunit alpha
			transl_table	11
10504	9923	CDS
			locus_tag	Faunusvirus_24_6
			product	hypothetical protein
			transl_table	11
11240	10563	CDS
			locus_tag	Faunusvirus_24_7
			product	hypothetical protein
			transl_table	11
11564	11280	CDS
			locus_tag	Faunusvirus_24_8
			product	hypothetical protein
			transl_table	11
>Faunusvirus_25
272	1456	CDS
			locus_tag	Faunusvirus_25_1
			product	hypothetical protein
			transl_table	11
1594	2328	CDS
			locus_tag	Faunusvirus_25_2
			product	hypothetical protein
			transl_table	11
2442	3197	CDS
			locus_tag	Faunusvirus_25_3
			product	hypothetical protein
			transl_table	11
3265	3615	CDS
			locus_tag	Faunusvirus_25_4
			product	hypothetical protein
			transl_table	11
3701	4045	CDS
			locus_tag	Faunusvirus_25_5
			product	hypothetical protein
			transl_table	11
4221	4910	CDS
			locus_tag	Faunusvirus_25_6
			product	hypothetical protein
			transl_table	11
5022	5783	CDS
			locus_tag	Faunusvirus_25_7
			product	hypothetical protein
			transl_table	11
5844	6602	CDS
			locus_tag	Faunusvirus_25_8
			product	hypothetical protein
			transl_table	11
6755	7516	CDS
			locus_tag	Faunusvirus_25_9
			product	hypothetical protein
			transl_table	11
7805	8584	CDS
			locus_tag	Faunusvirus_25_10
			product	hypothetical protein
			transl_table	11
8639	9367	CDS
			locus_tag	Faunusvirus_25_11
			product	hypothetical protein
			transl_table	11
9422	10171	CDS
			locus_tag	Faunusvirus_25_12
			product	hypothetical protein
			transl_table	11
10703	10843	CDS
			locus_tag	Faunusvirus_25_13
			product	hypothetical protein
			transl_table	11
>Faunusvirus_26
1	1011	CDS
			locus_tag	Faunusvirus_26_1
			product	hypothetical protein
			transl_table	11
1048	2328	CDS
			locus_tag	Faunusvirus_26_2
			product	hypothetical protein Catovirus_2_292
			transl_table	11
2386	7920	CDS
			locus_tag	Faunusvirus_26_3
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
9003	7957	CDS
			locus_tag	Faunusvirus_26_4
			product	holliday junction resolvase
			transl_table	11
>Faunusvirus_27
2	679	CDS
			locus_tag	Faunusvirus_27_1
			product	hypothetical protein
			transl_table	11
2358	682	CDS
			locus_tag	Faunusvirus_27_2
			product	NCLDV major capsid protein
			transl_table	11
3628	2450	CDS
			locus_tag	Faunusvirus_27_3
			product	late transcription factor VLTF3-like protein
			transl_table	11
3766	3945	CDS
			locus_tag	Faunusvirus_27_4
			product	hypothetical protein
			transl_table	11
4507	5100	CDS
			locus_tag	Faunusvirus_27_5
			product	hypothetical protein BNJ_00233
			transl_table	11
5872	5210	CDS
			locus_tag	Faunusvirus_27_6
			product	hypothetical protein
			transl_table	11
6047	6931	CDS
			locus_tag	Faunusvirus_27_7
			product	NCLDV major capsid protein
			transl_table	11
8792	7383	CDS
			locus_tag	Faunusvirus_27_8
			product	hypothetical protein CBC11_10280
			transl_table	11
10042	8855	CDS
			locus_tag	Faunusvirus_27_9
			product	HNh endonuclease
			transl_table	11
10378	10193	CDS
			locus_tag	Faunusvirus_27_10
			product	hypothetical protein
			transl_table	11
>Faunusvirus_28
9316	2	CDS
			locus_tag	Faunusvirus_28_1
			product	putative capsid protein
			transl_table	11
9432	9680	CDS
			locus_tag	Faunusvirus_28_2
			product	hypothetical protein
			transl_table	11
>Faunusvirus_29
1057	2	CDS
			locus_tag	Faunusvirus_29_1
			product	ATP-dependent Lon protease
			transl_table	11
1131	1421	CDS
			locus_tag	Faunusvirus_29_2
			product	hypothetical protein crov238
			transl_table	11
1514	2011	CDS
			locus_tag	Faunusvirus_29_3
			product	hypothetical protein
			transl_table	11
2078	2713	CDS
			locus_tag	Faunusvirus_29_4
			product	hypothetical protein
			transl_table	11
2777	3523	CDS
			locus_tag	Faunusvirus_29_5
			product	hypothetical protein C9994_02955
			transl_table	11
3587	4264	CDS
			locus_tag	Faunusvirus_29_6
			product	hypothetical protein
			transl_table	11
4328	5008	CDS
			locus_tag	Faunusvirus_29_7
			product	Ankyrin repeat domain-containing protein 17
			transl_table	11
5072	5719	CDS
			locus_tag	Faunusvirus_29_8
			product	hypothetical protein
			transl_table	11
8443	5723	CDS
			locus_tag	Faunusvirus_29_9
			product	Hsp70 protein
			transl_table	11
9077	8499	CDS
			locus_tag	Faunusvirus_29_10
			product	hypothetical protein
			transl_table	11
>Faunusvirus_30
1201	59	CDS
			locus_tag	Faunusvirus_30_1
			product	serpin
			transl_table	11
1303	2451	CDS
			locus_tag	Faunusvirus_30_2
			product	hypothetical protein
			transl_table	11
2486	2716	CDS
			locus_tag	Faunusvirus_30_3
			product	hypothetical protein
			transl_table	11
2734	3195	CDS
			locus_tag	Faunusvirus_30_4
			product	hypothetical protein
			transl_table	11
3308	5152	CDS
			locus_tag	Faunusvirus_30_5
			product	hypothetical protein
			transl_table	11
5295	6218	CDS
			locus_tag	Faunusvirus_30_6
			product	UvrD/REP helicase family protein
			transl_table	11
6854	6228	CDS
			locus_tag	Faunusvirus_30_7
			product	hypothetical protein
			transl_table	11
8668	6923	CDS
			locus_tag	Faunusvirus_30_8
			product	DUF262 domain-containing protein
			transl_table	11
8781	8909	CDS
			locus_tag	Faunusvirus_30_9
			product	hypothetical protein
			transl_table	11
>Faunusvirus_31
617	105	CDS
			locus_tag	Faunusvirus_31_1
			product	hypothetical protein
			transl_table	11
883	1761	CDS
			locus_tag	Faunusvirus_31_2
			product	hypothetical protein Indivirus_3_28
			transl_table	11
1790	2299	CDS
			locus_tag	Faunusvirus_31_3
			product	hypothetical protein
			transl_table	11
2915	2292	CDS
			locus_tag	Faunusvirus_31_4
			product	hypothetical protein
			transl_table	11
3057	5312	CDS
			locus_tag	Faunusvirus_31_5
			product	cullin 3B
			transl_table	11
5412	8315	CDS
			locus_tag	Faunusvirus_31_6
			product	hypothetical protein Catovirus_2_214
			transl_table	11
8554	8402	CDS
			locus_tag	Faunusvirus_31_7
			product	hypothetical protein
			transl_table	11
8682	8888	CDS
			locus_tag	Faunusvirus_31_8
			product	hypothetical protein
			transl_table	11
>Faunusvirus_32
2	700	CDS
			locus_tag	Faunusvirus_32_1
			product	hypothetical protein
			transl_table	11
1203	697	CDS
			locus_tag	Faunusvirus_32_2
			product	tyrosine phosphatase
			transl_table	11
2510	1224	CDS
			locus_tag	Faunusvirus_32_3
			product	hypothetical protein COB99_06395
			transl_table	11
3933	2569	CDS
			locus_tag	Faunusvirus_32_4
			product	hypothetical protein
			transl_table	11
5935	4016	CDS
			locus_tag	Faunusvirus_32_5
			product	Halomucin
			transl_table	11
5999	7204	CDS
			locus_tag	Faunusvirus_32_6
			product	hypothetical protein
			transl_table	11
7289	8560	CDS
			locus_tag	Faunusvirus_32_7
			product	serine/threonine protein kinase
			transl_table	11
8884	8711	CDS
			locus_tag	Faunusvirus_32_8
			product	hypothetical protein
			transl_table	11
>Faunusvirus_33
825	175	CDS
			locus_tag	Faunusvirus_33_1
			product	hypothetical protein
			transl_table	11
1185	889	CDS
			locus_tag	Faunusvirus_33_2
			product	hypothetical protein
			transl_table	11
1599	1273	CDS
			locus_tag	Faunusvirus_33_3
			product	hypothetical protein
			transl_table	11
1890	1696	CDS
			locus_tag	Faunusvirus_33_4
			product	hypothetical protein
			transl_table	11
2159	2001	CDS
			locus_tag	Faunusvirus_33_5
			product	hypothetical protein
			transl_table	11
6313	2216	CDS
			locus_tag	Faunusvirus_33_6
			product	putative DNA repair protein
			transl_table	11
6424	6759	CDS
			locus_tag	Faunusvirus_33_7
			product	hypothetical protein
			transl_table	11
7652	6756	CDS
			locus_tag	Faunusvirus_33_8
			product	hypothetical protein
			transl_table	11
8588	7824	CDS
			locus_tag	Faunusvirus_33_9
			product	hypothetical protein
			transl_table	11
>Faunusvirus_34
1373	3	CDS
			locus_tag	Faunusvirus_34_1
			product	hypothetical protein CEUSTIGMA_g12192.t1
			transl_table	11
1797	1450	CDS
			locus_tag	Faunusvirus_34_2
			product	hypothetical protein Indivirus_2_126
			transl_table	11
1972	5265	CDS
			locus_tag	Faunusvirus_34_3
			product	putative helicase/exonuclease
			transl_table	11
5995	5465	CDS
			locus_tag	Faunusvirus_34_4
			product	hypothetical protein
			transl_table	11
6963	6409	CDS
			locus_tag	Faunusvirus_34_5
			product	hypothetical protein
			transl_table	11
7644	7093	CDS
			locus_tag	Faunusvirus_34_6
			product	hypothetical protein
			transl_table	11
>Faunusvirus_35
559	8	CDS
			locus_tag	Faunusvirus_35_1
			product	hypothetical protein
			transl_table	11
1233	694	CDS
			locus_tag	Faunusvirus_35_2
			product	hypothetical protein
			transl_table	11
1827	1330	CDS
			locus_tag	Faunusvirus_35_3
			product	hypothetical protein
			transl_table	11
2190	2966	CDS
			locus_tag	Faunusvirus_35_4
			product	hypothetical protein
			transl_table	11
3798	2995	CDS
			locus_tag	Faunusvirus_35_5
			product	hypothetical protein
			transl_table	11
4319	3855	CDS
			locus_tag	Faunusvirus_35_6
			product	hypothetical protein
			transl_table	11
4640	4443	CDS
			locus_tag	Faunusvirus_35_7
			product	hypothetical protein
			transl_table	11
5633	4677	CDS
			locus_tag	Faunusvirus_35_8
			product	hypothetical protein
			transl_table	11
5823	5689	CDS
			locus_tag	Faunusvirus_35_9
			product	hypothetical protein
			transl_table	11
6349	5864	CDS
			locus_tag	Faunusvirus_35_10
			product	hypothetical protein
			transl_table	11
6952	6467	CDS
			locus_tag	Faunusvirus_35_11
			product	hypothetical protein
			transl_table	11
7533	7060	CDS
			locus_tag	Faunusvirus_35_12
			product	hypothetical protein
			transl_table	11
>Faunusvirus_36
3771	1	CDS
			locus_tag	Faunusvirus_36_1
			product	SNF2-like helicase
			transl_table	11
3868	4320	CDS
			locus_tag	Faunusvirus_36_2
			product	hypothetical protein
			transl_table	11
5018	4317	CDS
			locus_tag	Faunusvirus_36_3
			product	hypothetical protein
			transl_table	11
5840	5073	CDS
			locus_tag	Faunusvirus_36_4
			product	Peptide chain release factor eRF1/aRF1
			transl_table	11
6051	5899	CDS
			locus_tag	Faunusvirus_36_5
			product	hypothetical protein
			transl_table	11
6274	6831	CDS
			locus_tag	Faunusvirus_36_6
			product	hypothetical protein
			transl_table	11
7121	6828	CDS
			locus_tag	Faunusvirus_36_7
			product	hypothetical protein
			transl_table	11
7177	7296	CDS
			locus_tag	Faunusvirus_36_8
			product	hypothetical protein
			transl_table	11
>Faunusvirus_37
3	503	CDS
			locus_tag	Faunusvirus_37_1
			product	serine/threonine protein kinase
			transl_table	11
621	1436	CDS
			locus_tag	Faunusvirus_37_2
			product	serine/threonine protein kinase
			transl_table	11
1652	1470	CDS
			locus_tag	Faunusvirus_37_3
			product	hypothetical protein
			transl_table	11
2548	1691	CDS
			locus_tag	Faunusvirus_37_4
			product	hypothetical protein
			transl_table	11
7255	2579	CDS
			locus_tag	Faunusvirus_37_5
			product	hypothetical protein Catovirus_2_323
			transl_table	11
>Faunusvirus_38
269	3	CDS
			locus_tag	Faunusvirus_38_1
			product	hypothetical protein
			transl_table	11
1517	327	CDS
			locus_tag	Faunusvirus_38_2
			product	DHH family phosphohydrolase
			transl_table	11
2728	1844	CDS
			locus_tag	Faunusvirus_38_3
			product	polyA polymerase regulatory subunit
			transl_table	11
3638	2748	CDS
			locus_tag	Faunusvirus_38_4
			product	polyA polymerase regulatory subunit
			transl_table	11
6131	3663	CDS
			locus_tag	Faunusvirus_38_5
			product	Baculoviral IAP repeat-containing protein
			transl_table	11
>Faunusvirus_39
2	364	CDS
			locus_tag	Faunusvirus_39_1
			product	hypothetical protein
			transl_table	11
1516	374	CDS
			locus_tag	Faunusvirus_39_2
			product	PREDICTED: neurogenic locus notch homolog protein 2-like
			transl_table	11
2327	1581	CDS
			locus_tag	Faunusvirus_39_3
			product	hypothetical protein ceV_230
			transl_table	11
2505	3305	CDS
			locus_tag	Faunusvirus_39_4
			product	SWIB-domain containing protein
			transl_table	11
3431	4222	CDS
			locus_tag	Faunusvirus_39_5
			product	hypothetical protein
			transl_table	11
4270	4965	CDS
			locus_tag	Faunusvirus_39_6
			product	ankyrin repeat domain-containing protein
			transl_table	11
5025	5720	CDS
			locus_tag	Faunusvirus_39_7
			product	hypothetical protein
			transl_table	11
5782	6465	CDS
			locus_tag	Faunusvirus_39_8
			product	hypothetical protein
			transl_table	11
6526	6939	CDS
			locus_tag	Faunusvirus_39_9
			product	hypothetical protein
			transl_table	11
>Faunusvirus_40
244	2	CDS
			locus_tag	Faunusvirus_40_1
			product	hypothetical protein
			transl_table	11
1178	315	CDS
			locus_tag	Faunusvirus_40_2
			product	Ulp1 protease
			transl_table	11
1983	1240	CDS
			locus_tag	Faunusvirus_40_3
			product	hypothetical protein
			transl_table	11
3559	2051	CDS
			locus_tag	Faunusvirus_40_4
			product	putative phage-type endonuclease
			transl_table	11
3701	4315	CDS
			locus_tag	Faunusvirus_40_5
			product	hypothetical protein
			transl_table	11
4999	4307	CDS
			locus_tag	Faunusvirus_40_6
			product	hypothetical protein BMW23_0576
			transl_table	11
5043	5549	CDS
			locus_tag	Faunusvirus_40_7
			product	RING finger domain protein
			transl_table	11
5934	5551	CDS
			locus_tag	Faunusvirus_40_8
			product	hypothetical protein
			transl_table	11
6023	6667	CDS
			locus_tag	Faunusvirus_40_9
			product	hypothetical protein
			transl_table	11
>Faunusvirus_41
692	3	CDS
			locus_tag	Faunusvirus_41_1
			product	hypothetical protein
			transl_table	11
1295	783	CDS
			locus_tag	Faunusvirus_41_2
			product	hypothetical protein crov328
			transl_table	11
2739	1453	CDS
			locus_tag	Faunusvirus_41_3
			product	AAA family ATPase
			transl_table	11
3627	2764	CDS
			locus_tag	Faunusvirus_41_4
			product	UDP-glucuronate:xylan alpha-glucuronosyltransferase 2-like
			transl_table	11
3773	4672	CDS
			locus_tag	Faunusvirus_41_5
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
6128	6712	CDS
			locus_tag	Faunusvirus_41_6
			product	ATP-dependent RNA helicase
			transl_table	11
>Faunusvirus_42
3	659	CDS
			locus_tag	Faunusvirus_42_1
			product	hypothetical protein
			transl_table	11
711	1424	CDS
			locus_tag	Faunusvirus_42_2
			product	hypothetical protein
			transl_table	11
1485	2237	CDS
			locus_tag	Faunusvirus_42_3
			product	hypothetical protein
			transl_table	11
2277	2393	CDS
			locus_tag	Faunusvirus_42_4
			product	hypothetical protein
			transl_table	11
2698	3459	CDS
			locus_tag	Faunusvirus_42_5
			product	hypothetical protein
			transl_table	11
4053	4865	CDS
			locus_tag	Faunusvirus_42_6
			product	hypothetical protein
			transl_table	11
5380	5478	CDS
			locus_tag	Faunusvirus_42_7
			product	hypothetical protein
			transl_table	11
6686	5829	CDS
			locus_tag	Faunusvirus_42_8
			product	hypothetical protein BpV2_130
			transl_table	11
>Faunusvirus_43
3	431	CDS
			locus_tag	Faunusvirus_43_1
			product	hypothetical protein
			transl_table	11
1198	428	CDS
			locus_tag	Faunusvirus_43_2
			product	hypothetical protein
			transl_table	11
4844	1251	CDS
			locus_tag	Faunusvirus_43_3
			product	unnamed protein product, partial
			transl_table	11
4982	5608	CDS
			locus_tag	Faunusvirus_43_4
			product	hypothetical protein Klosneuvirus_2_232
			transl_table	11
6014	5670	CDS
			locus_tag	Faunusvirus_43_5
			product	hypothetical protein
			transl_table	11
6169	6011	CDS
			locus_tag	Faunusvirus_43_6
			product	hypothetical protein
			transl_table	11
6323	6463	CDS
			locus_tag	Faunusvirus_43_7
			product	hypothetical protein
			transl_table	11
>Faunusvirus_44
2	157	CDS
			locus_tag	Faunusvirus_44_1
			product	hypothetical protein
			transl_table	11
343	726	CDS
			locus_tag	Faunusvirus_44_2
			product	hypothetical protein
			transl_table	11
861	1295	CDS
			locus_tag	Faunusvirus_44_3
			product	hypothetical protein
			transl_table	11
1738	1292	CDS
			locus_tag	Faunusvirus_44_4
			product	hypothetical protein
			transl_table	11
1978	2082	CDS
			locus_tag	Faunusvirus_44_5
			product	hypothetical protein
			transl_table	11
2845	2345	CDS
			locus_tag	Faunusvirus_44_6
			product	hypothetical protein
			transl_table	11
3222	3001	CDS
			locus_tag	Faunusvirus_44_7
			product	hypothetical protein
			transl_table	11
3362	4549	CDS
			locus_tag	Faunusvirus_44_8
			product	hypothetical protein
			transl_table	11
4630	5211	CDS
			locus_tag	Faunusvirus_44_9
			product	hypothetical protein
			transl_table	11
5291	5695	CDS
			locus_tag	Faunusvirus_44_10
			product	hypothetical protein
			transl_table	11
>Faunusvirus_45
132	1	CDS
			locus_tag	Faunusvirus_45_1
			product	hypothetical protein
			transl_table	11
1255	356	CDS
			locus_tag	Faunusvirus_45_2
			product	hypothetical protein Hokovirus_3_254
			transl_table	11
1798	1376	CDS
			locus_tag	Faunusvirus_45_3
			product	hypothetical protein
			transl_table	11
2708	1953	CDS
			locus_tag	Faunusvirus_45_4
			product	hypothetical protein
			transl_table	11
3342	2758	CDS
			locus_tag	Faunusvirus_45_5
			product	hypothetical protein
			transl_table	11
4095	3415	CDS
			locus_tag	Faunusvirus_45_6
			product	hypothetical protein
			transl_table	11
4748	4170	CDS
			locus_tag	Faunusvirus_45_7
			product	hypothetical protein
			transl_table	11
5377	4805	CDS
			locus_tag	Faunusvirus_45_8
			product	hypothetical protein
			transl_table	11
5719	5883	CDS
			locus_tag	Faunusvirus_45_9
			product	hypothetical protein
			transl_table	11
>Faunusvirus_46
526	2	CDS
			locus_tag	Faunusvirus_46_1
			product	hypothetical protein
			transl_table	11
660	1895	CDS
			locus_tag	Faunusvirus_46_2
			product	hypothetical protein
			transl_table	11
2674	1907	CDS
			locus_tag	Faunusvirus_46_3
			product	hypothetical protein crov353
			transl_table	11
2762	3214	CDS
			locus_tag	Faunusvirus_46_4
			product	hypothetical protein
			transl_table	11
3264	3719	CDS
			locus_tag	Faunusvirus_46_5
			product	hypothetical protein
			transl_table	11
4240	4025	CDS
			locus_tag	Faunusvirus_46_6
			product	putative DNA-directed RNA polymerase subunit Rpb10
			transl_table	11
4499	4314	CDS
			locus_tag	Faunusvirus_46_7
			product	hypothetical protein
			transl_table	11
5006	4545	CDS
			locus_tag	Faunusvirus_46_8
			product	hypothetical protein
			transl_table	11
5522	5061	CDS
			locus_tag	Faunusvirus_46_9
			product	hypothetical protein
			transl_table	11
5723	5619	CDS
			locus_tag	Faunusvirus_46_10
			product	hypothetical protein
			transl_table	11
>Faunusvirus_47
1	900	CDS
			locus_tag	Faunusvirus_47_1
			product	putative ankyrin repeat protein
			transl_table	11
1536	904	CDS
			locus_tag	Faunusvirus_47_2
			product	hypothetical protein
			transl_table	11
1670	2203	CDS
			locus_tag	Faunusvirus_47_3
			product	hypothetical protein
			transl_table	11
3259	2222	CDS
			locus_tag	Faunusvirus_47_4
			product	DNA polymerase family X protein
			transl_table	11
3368	4018	CDS
			locus_tag	Faunusvirus_47_5
			product	hypothetical protein
			transl_table	11
5617	4076	CDS
			locus_tag	Faunusvirus_47_6
			product	hypothetical protein Klosneuvirus_1_338
			transl_table	11
>Faunusvirus_48
1	534	CDS
			locus_tag	Faunusvirus_48_1
			product	patatin-like phospholipase
			transl_table	11
549	905	CDS
			locus_tag	Faunusvirus_48_2
			product	hypothetical protein
			transl_table	11
1941	985	CDS
			locus_tag	Faunusvirus_48_3
			product	DnaJ domain protein
			transl_table	11
2036	3229	CDS
			locus_tag	Faunusvirus_48_4
			product	ADP-ribosylglycohydrolase
			transl_table	11
3620	3231	CDS
			locus_tag	Faunusvirus_48_5
			product	hypothetical protein PSEUBRA_SCAF21g03417
			transl_table	11
5010	4852	CDS
			locus_tag	Faunusvirus_48_6
			product	hypothetical protein
			transl_table	11
5009	5410	CDS
			locus_tag	Faunusvirus_48_7
			product	hypothetical protein
			transl_table	11
>Faunusvirus_49
2	2044	CDS
			locus_tag	Faunusvirus_49_1
			product	putative minor capsid protein
			transl_table	11
5285	2448	CDS
			locus_tag	Faunusvirus_49_2
			product	ankyrin repeat protein
			transl_table	11
>Faunusvirus_50
292	191	CDS
			locus_tag	Faunusvirus_50_1
			product	hypothetical protein
			transl_table	11
375	1577	CDS
			locus_tag	Faunusvirus_50_2
			product	hypothetical protein
			transl_table	11
2442	1696	CDS
			locus_tag	Faunusvirus_50_3
			product	hypothetical protein
			transl_table	11
2698	3123	CDS
			locus_tag	Faunusvirus_50_4
			product	hypothetical protein
			transl_table	11
3936	3139	CDS
			locus_tag	Faunusvirus_50_5
			product	hypothetical protein
			transl_table	11
4692	4066	CDS
			locus_tag	Faunusvirus_50_6
			product	hypothetical protein
			transl_table	11
>Faunusvirus_51
34	3309	CDS
			locus_tag	Faunusvirus_51_1
			product	putative helicase
			transl_table	11
3675	4343	CDS
			locus_tag	Faunusvirus_51_2
			product	putative DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
4407	5147	CDS
			locus_tag	Faunusvirus_51_3
			product	putative DNA-dependent RNA polymerase subunit Rpb6
			transl_table	11
>Faunusvirus_52
141	1	CDS
			locus_tag	Faunusvirus_52_1
			product	hypothetical protein
			transl_table	11
266	805	CDS
			locus_tag	Faunusvirus_52_2
			product	hypothetical protein crov215
			transl_table	11
1053	808	CDS
			locus_tag	Faunusvirus_52_3
			product	hypothetical protein
			transl_table	11
1362	1099	CDS
			locus_tag	Faunusvirus_52_4
			product	hypothetical protein
			transl_table	11
1877	1422	CDS
			locus_tag	Faunusvirus_52_5
			product	hypothetical protein Klosneuvirus_1_187
			transl_table	11
2670	1951	CDS
			locus_tag	Faunusvirus_52_6
			product	hypothetical protein crov228
			transl_table	11
2786	3847	CDS
			locus_tag	Faunusvirus_52_7
			product	ankyrin repeat protein
			transl_table	11
4403	3837	CDS
			locus_tag	Faunusvirus_52_8
			product	hypothetical protein
			transl_table	11
4429	4641	CDS
			locus_tag	Faunusvirus_52_9
			product	hypothetical protein
			transl_table	11
4666	5079	CDS
			locus_tag	Faunusvirus_52_10
			product	hypothetical protein
			transl_table	11
>Faunusvirus_53
669	1	CDS
			locus_tag	Faunusvirus_53_1
			product	hypothetical protein
			transl_table	11
1556	684	CDS
			locus_tag	Faunusvirus_53_2
			product	apurinic endonuclease
			transl_table	11
4648	4259	CDS
			locus_tag	Faunusvirus_53_3
			product	replication factor C small subunit
			transl_table	11
>Faunusvirus_54
3	803	CDS
			locus_tag	Faunusvirus_54_1
			product	hypothetical protein AQUCO_01400252v1
			transl_table	11
832	2139	CDS
			locus_tag	Faunusvirus_54_2
			product	hypothetical protein Catovirus_1_929
			transl_table	11
3228	2146	CDS
			locus_tag	Faunusvirus_54_3
			product	phosphoribosylpyrophosphate synthetase
			transl_table	11
3466	3284	CDS
			locus_tag	Faunusvirus_54_4
			product	hypothetical protein
			transl_table	11
4177	3494	CDS
			locus_tag	Faunusvirus_54_5
			product	hypothetical protein
			transl_table	11
4583	4209	CDS
			locus_tag	Faunusvirus_54_6
			product	dual specificity phosphatase
			transl_table	11
>Faunusvirus_55
470	856	CDS
			locus_tag	Faunusvirus_55_1
			product	hypothetical protein
			transl_table	11
923	1723	CDS
			locus_tag	Faunusvirus_55_2
			product	hypothetical protein
			transl_table	11
1823	2575	CDS
			locus_tag	Faunusvirus_55_3
			product	hypothetical protein
			transl_table	11
2635	3381	CDS
			locus_tag	Faunusvirus_55_4
			product	hypothetical protein
			transl_table	11
3449	4198	CDS
			locus_tag	Faunusvirus_55_5
			product	hypothetical protein
			transl_table	11
>Faunusvirus_56
3	452	CDS
			locus_tag	Faunusvirus_56_1
			product	hypothetical protein
			transl_table	11
892	1635	CDS
			locus_tag	Faunusvirus_56_2
			product	hypothetical protein
			transl_table	11
1834	2979	CDS
			locus_tag	Faunusvirus_56_3
			product	hypothetical protein
			transl_table	11
3219	2992	CDS
			locus_tag	Faunusvirus_56_4
			product	hypothetical protein
			transl_table	11
3511	4086	CDS
			locus_tag	Faunusvirus_56_5
			product	hypothetical protein
			transl_table	11
>Faunusvirus_57
124	894	CDS
			locus_tag	Faunusvirus_57_1
			product	hypothetical protein
			transl_table	11
948	1289	CDS
			locus_tag	Faunusvirus_57_2
			product	hypothetical protein
			transl_table	11
1376	1726	CDS
			locus_tag	Faunusvirus_57_3
			product	hypothetical protein
			transl_table	11
1786	2016	CDS
			locus_tag	Faunusvirus_57_4
			product	hypothetical protein
			transl_table	11
2124	2840	CDS
			locus_tag	Faunusvirus_57_5
			product	hypothetical protein
			transl_table	11
2894	3247	CDS
			locus_tag	Faunusvirus_57_6
			product	hypothetical protein
			transl_table	11
3334	3681	CDS
			locus_tag	Faunusvirus_57_7
			product	hypothetical protein
			transl_table	11
3714	3863	CDS
			locus_tag	Faunusvirus_57_8
			product	hypothetical protein
			transl_table	11
>Faunusvirus_58
15	614	CDS
			locus_tag	Faunusvirus_58_1
			product	hypothetical protein UV38_C0002G0300
			transl_table	11
3120	631	CDS
			locus_tag	Faunusvirus_58_2
			product	Topoisomerase I
			transl_table	11
3500	3201	CDS
			locus_tag	Faunusvirus_58_3
			product	hypothetical protein
			transl_table	11
3879	3670	CDS
			locus_tag	Faunusvirus_58_4
			product	hypothetical protein
			transl_table	11
>Faunusvirus_59
3	1010	CDS
			locus_tag	Faunusvirus_59_1
			product	hypothetical protein
			transl_table	11
1045	1308	CDS
			locus_tag	Faunusvirus_59_2
			product	hypothetical protein
			transl_table	11
1995	1303	CDS
			locus_tag	Faunusvirus_59_3
			product	hypothetical protein
			transl_table	11
2101	2241	CDS
			locus_tag	Faunusvirus_59_4
			product	hypothetical protein
			transl_table	11
3776	2337	CDS
			locus_tag	Faunusvirus_59_5
			product	RING finger protein 151-like isoform X1
			transl_table	11
>Faunusvirus_60
1196	3	CDS
			locus_tag	Faunusvirus_60_1
			product	hypothetical protein
			transl_table	11
1825	1283	CDS
			locus_tag	Faunusvirus_60_2
			product	hypothetical protein
			transl_table	11
2636	1806	CDS
			locus_tag	Faunusvirus_60_3
			product	hypothetical protein
			transl_table	11
3701	2694	CDS
			locus_tag	Faunusvirus_60_4
			product	hypothetical protein
			transl_table	11
>Faunusvirus_61
55	822	CDS
			locus_tag	Faunusvirus_61_1
			product	hypothetical protein
			transl_table	11
926	1555	CDS
			locus_tag	Faunusvirus_61_2
			product	hypothetical protein CBC05_03310
			transl_table	11
1617	2714	CDS
			locus_tag	Faunusvirus_61_3
			product	putative ORFan
			transl_table	11
3262	2711	CDS
			locus_tag	Faunusvirus_61_4
			product	hypothetical protein
			transl_table	11
3407	3493	CDS
			locus_tag	Faunusvirus_61_5
			product	hypothetical protein
			transl_table	11
>Faunusvirus_62
778	2	CDS
			locus_tag	Faunusvirus_62_1
			product	hypothetical protein
			transl_table	11
1481	837	CDS
			locus_tag	Faunusvirus_62_2
			product	hypothetical protein
			transl_table	11
1650	3101	CDS
			locus_tag	Faunusvirus_62_3
			product	hypothetical protein CAPTEDRAFT_176561
			transl_table	11
3220	3489	CDS
			locus_tag	Faunusvirus_62_4
			product	hypothetical protein
			transl_table	11
>Faunusvirus_63
121	2	CDS
			locus_tag	Faunusvirus_63_1
			product	hypothetical protein
			transl_table	11
340	1869	CDS
			locus_tag	Faunusvirus_63_2
			product	RNA ligase
			transl_table	11
3027	1945	CDS
			locus_tag	Faunusvirus_63_3
			product	putative DNA-dependent RNA polymerase subunit Rpb3/Rpb11
			transl_table	11
3255	3103	CDS
			locus_tag	Faunusvirus_63_4
			product	hypothetical protein
			transl_table	11
3403	3290	CDS
			locus_tag	Faunusvirus_63_5
			product	hypothetical protein
			transl_table	11
>Faunusvirus_64
811	65	CDS
			locus_tag	Faunusvirus_64_1
			product	hypothetical protein
			transl_table	11
1590	865	CDS
			locus_tag	Faunusvirus_64_2
			product	hypothetical protein
			transl_table	11
1803	1645	CDS
			locus_tag	Faunusvirus_64_3
			product	hypothetical protein
			transl_table	11
2363	1878	CDS
			locus_tag	Faunusvirus_64_4
			product	hypothetical protein
			transl_table	11
3160	2417	CDS
			locus_tag	Faunusvirus_64_5
			product	hypothetical protein
			transl_table	11
3337	3215	CDS
			locus_tag	Faunusvirus_64_6
			product	hypothetical protein
			transl_table	11
>Faunusvirus_65
1	1515	CDS
			locus_tag	Faunusvirus_65_1
			product	hypothetical protein SBOR_6374
			transl_table	11
>Faunusvirus_66
2	1111	CDS
			locus_tag	Faunusvirus_66_1
			product	hypothetical protein
			transl_table	11
1153	1707	CDS
			locus_tag	Faunusvirus_66_2
			product	hypothetical protein
			transl_table	11
1956	2087	CDS
			locus_tag	Faunusvirus_66_3
			product	hypothetical protein
			transl_table	11
2511	2624	CDS
			locus_tag	Faunusvirus_66_4
			product	hypothetical protein
			transl_table	11
2842	2681	CDS
			locus_tag	Faunusvirus_66_5
			product	hypothetical protein
			transl_table	11
>Faunusvirus_67
794	3	CDS
			locus_tag	Faunusvirus_67_1
			product	hypothetical protein
			transl_table	11
1608	895	CDS
			locus_tag	Faunusvirus_67_2
			product	ankyrin repeat domain-containing protein 7-like isoform X2
			transl_table	11
2555	1743	CDS
			locus_tag	Faunusvirus_67_3
			product	hypothetical protein
			transl_table	11
2701	2591	CDS
			locus_tag	Faunusvirus_67_4
			product	hypothetical protein
			transl_table	11
