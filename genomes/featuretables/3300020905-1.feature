>3300020905-1_1
1	1464	CDS
			locus_tag	3300020905-1_1_1
			product	topoisomerase I
			transl_table	11
1572	3827	CDS
			locus_tag	3300020905-1_1_2
			product	hypothetical protein
			transl_table	11
4626	3922	CDS
			locus_tag	3300020905-1_1_3
			product	putative glycosyltransferase
			transl_table	11
4768	5406	CDS
			locus_tag	3300020905-1_1_4
			product	orotate phosphoribosyltransferase
			transl_table	11
5938	6354	CDS
			locus_tag	3300020905-1_1_5
			product	hypothetical protein
			transl_table	11
6426	6716	CDS
			locus_tag	3300020905-1_1_6
			product	hypothetical protein
			transl_table	11
6783	7478	CDS
			locus_tag	3300020905-1_1_7
			product	hypothetical protein
			transl_table	11
8786	7539	CDS
			locus_tag	3300020905-1_1_8
			product	hypothetical protein
			transl_table	11
8919	9857	CDS
			locus_tag	3300020905-1_1_9
			product	hypothetical protein
			transl_table	11
10020	12083	CDS
			locus_tag	3300020905-1_1_10
			product	putative ankyrin repeat protein
			transl_table	11
17186	12138	CDS
			locus_tag	3300020905-1_1_11
			product	putative ORFan
			transl_table	11
>3300020905-1_2
10985	11056	tRNA	3300020905-1_tRNA_1
			product	tRNA-Lys(TTT)
10787	10858	tRNA	3300020905-1_tRNA_2
			product	tRNA-Ile(GAT)
10714	10785	tRNA	3300020905-1_tRNA_3
			product	tRNA-Val(AAC)
8184	8254	tRNA	3300020905-1_tRNA_4
			product	tRNA-Val(TAC)
8111	8182	tRNA	3300020905-1_tRNA_5
			product	tRNA-Lys(CTT)
8038	8109	tRNA	3300020905-1_tRNA_6
			product	tRNA-Arg(TCG)
7962	8032	tRNA	3300020905-1_tRNA_7
			product	tRNA-Lys(TTT)
7846	7917	tRNA	3300020905-1_tRNA_8
			product	tRNA-Ile(TAT)
7772	7844	tRNA	3300020905-1_tRNA_9
			product	tRNA-Arg(TCT)
7695	7766	tRNA	3300020905-1_tRNA_10
			product	tRNA-Pseudo(CAC)
7483	7554	tRNA	3300020905-1_tRNA_11
			product	tRNA-Arg(CCT)
7380	7450	tRNA	3300020905-1_tRNA_12
			product	tRNA-Ala(AGC)
7272	7342	tRNA	3300020905-1_tRNA_13
			product	tRNA-Ala(TGC)
6995	7065	tRNA	3300020905-1_tRNA_14
			product	tRNA-Ala(CGC)
6891	6961	tRNA	3300020905-1_tRNA_15
			product	tRNA-Trp(CCA)
6782	6852	tRNA	3300020905-1_tRNA_16
			product	tRNA-Cys(GCA)
6685	6756	tRNA	3300020905-1_tRNA_17
			product	tRNA-Arg(ACG)
6613	6683	tRNA	3300020905-1_tRNA_18
			product	tRNA-Phe(GAA)
584	3	CDS
			locus_tag	3300020905-1_2_1
			product	hypothetical protein
			transl_table	11
1817	651	CDS
			locus_tag	3300020905-1_2_2
			product	hypothetical protein
			transl_table	11
1969	2241	CDS
			locus_tag	3300020905-1_2_3
			product	hypothetical protein
			transl_table	11
3638	2274	CDS
			locus_tag	3300020905-1_2_4
			product	hypothetical protein
			transl_table	11
5077	3782	CDS
			locus_tag	3300020905-1_2_5
			product	hypothetical protein
			transl_table	11
6194	5148	CDS
			locus_tag	3300020905-1_2_6
			product	hypothetical protein
			transl_table	11
9374	8433	CDS
			locus_tag	3300020905-1_2_7
			product	putative transposase
			transl_table	11
10570	9434	CDS
			locus_tag	3300020905-1_2_8
			product	putative transposase
			transl_table	11
10908	11045	CDS
			locus_tag	3300020905-1_2_9
			product	hypothetical protein
			transl_table	11
12134	11193	CDS
			locus_tag	3300020905-1_2_10
			product	hypothetical protein CHLRE_01g046501v5
			transl_table	11
13237	12230	CDS
			locus_tag	3300020905-1_2_11
			product	hypothetical protein Klosneuvirus_2_178
			transl_table	11
13407	14585	CDS
			locus_tag	3300020905-1_2_12
			product	hypothetical protein
			transl_table	11
14799	14554	CDS
			locus_tag	3300020905-1_2_13
			product	hypothetical protein
			transl_table	11
15721	14849	CDS
			locus_tag	3300020905-1_2_14
			product	hypothetical protein
			transl_table	11
16173	15823	CDS
			locus_tag	3300020905-1_2_15
			product	hypothetical protein
			transl_table	11
16728	16339	CDS
			locus_tag	3300020905-1_2_16
			product	hypothetical protein
			transl_table	11
17102	16971	CDS
			locus_tag	3300020905-1_2_17
			product	hypothetical protein
			transl_table	11
>3300020905-1_3
1132	86	CDS
			locus_tag	3300020905-1_3_1
			product	hypothetical protein
			transl_table	11
1279	3018	CDS
			locus_tag	3300020905-1_3_2
			product	collagen triple helix repeat motif-containing protein
			transl_table	11
4516	3080	CDS
			locus_tag	3300020905-1_3_3
			product	hypothetical protein
			transl_table	11
5874	4681	CDS
			locus_tag	3300020905-1_3_4
			product	hypothetical protein
			transl_table	11
6020	6889	CDS
			locus_tag	3300020905-1_3_5
			product	hypothetical protein AURDEDRAFT_179421
			transl_table	11
6867	7451	CDS
			locus_tag	3300020905-1_3_6
			product	mg989 protein
			transl_table	11
8451	7633	CDS
			locus_tag	3300020905-1_3_7
			product	hypothetical protein
			transl_table	11
9277	8585	CDS
			locus_tag	3300020905-1_3_8
			product	hypothetical protein
			transl_table	11
9876	9343	CDS
			locus_tag	3300020905-1_3_9
			product	hypothetical protein
			transl_table	11
10241	9987	CDS
			locus_tag	3300020905-1_3_10
			product	hypothetical protein
			transl_table	11
10240	10620	CDS
			locus_tag	3300020905-1_3_11
			product	hypothetical protein
			transl_table	11
10809	12437	CDS
			locus_tag	3300020905-1_3_12
			product	hypothetical protein RclHR1_00830012
			transl_table	11
12566	13156	CDS
			locus_tag	3300020905-1_3_13
			product	hypothetical protein
			transl_table	11
13826	13245	CDS
			locus_tag	3300020905-1_3_14
			product	hypothetical protein
			transl_table	11
14692	14006	CDS
			locus_tag	3300020905-1_3_15
			product	hypothetical protein
			transl_table	11
15343	14789	CDS
			locus_tag	3300020905-1_3_16
			product	PREDICTED: LOW QUALITY PROTEIN: E3 ubiquitin-protein ligase LRSAM1
			transl_table	11
15700	15924	CDS
			locus_tag	3300020905-1_3_17
			product	hypothetical protein
			transl_table	11
>3300020905-1_4
12878	12962	tRNA	3300020905-1_tRNA_19
			product	tRNA-Ser(GCT)
12805	12876	tRNA	3300020905-1_tRNA_20
			product	tRNA-Gln(TTG)
12680	12763	tRNA	3300020905-1_tRNA_21
			product	tRNA-Ser(TGA)
12548	12634	tRNA	3300020905-1_tRNA_22
			product	tRNA-Ser(AGA)
12358	12441	tRNA	3300020905-1_tRNA_23
			product	tRNA-Ser(CGA)
12285	12356	tRNA	3300020905-1_tRNA_24
			product	tRNA-Gln(CTG)
1530	1	CDS
			locus_tag	3300020905-1_4_1
			product	hypothetical protein
			transl_table	11
2107	1664	CDS
			locus_tag	3300020905-1_4_2
			product	mg347 protein
			transl_table	11
3400	2159	CDS
			locus_tag	3300020905-1_4_3
			product	ankyrin repeat protein
			transl_table	11
3536	3862	CDS
			locus_tag	3300020905-1_4_4
			product	hypothetical protein
			transl_table	11
3993	4652	CDS
			locus_tag	3300020905-1_4_5
			product	hypothetical protein
			transl_table	11
5974	4649	CDS
			locus_tag	3300020905-1_4_6
			product	hypothetical protein
			transl_table	11
7751	6039	CDS
			locus_tag	3300020905-1_4_7
			product	hypothetical protein MegaChil _gp0668
			transl_table	11
9316	7868	CDS
			locus_tag	3300020905-1_4_8
			product	putative orfan
			transl_table	11
9471	9875	CDS
			locus_tag	3300020905-1_4_9
			product	hypothetical protein
			transl_table	11
10257	9877	CDS
			locus_tag	3300020905-1_4_10
			product	thioredoxin domain-containing protein
			transl_table	11
10567	10409	CDS
			locus_tag	3300020905-1_4_11
			product	hypothetical protein
			transl_table	11
10554	12191	CDS
			locus_tag	3300020905-1_4_12
			product	putative ORFan
			transl_table	11
12483	12388	CDS
			locus_tag	3300020905-1_4_13
			product	hypothetical protein
			transl_table	11
13678	13091	CDS
			locus_tag	3300020905-1_4_14
			product	hypothetical protein LBA_00692
			transl_table	11
13965	13807	CDS
			locus_tag	3300020905-1_4_15
			product	hypothetical protein
			transl_table	11
>3300020905-1_5
1810	218	CDS
			locus_tag	3300020905-1_5_1
			product	putative transposase/mobile element protein
			transl_table	11
1902	2501	CDS
			locus_tag	3300020905-1_5_2
			product	putative resolvase
			transl_table	11
3016	2543	CDS
			locus_tag	3300020905-1_5_3
			product	hypothetical protein
			transl_table	11
4479	3103	CDS
			locus_tag	3300020905-1_5_4
			product	hypothetical protein
			transl_table	11
5069	4566	CDS
			locus_tag	3300020905-1_5_5
			product	hypothetical protein
			transl_table	11
5209	8292	CDS
			locus_tag	3300020905-1_5_6
			product	DEAD-like helicase
			transl_table	11
8698	9129	CDS
			locus_tag	3300020905-1_5_7
			product	hypothetical protein Klosneuvirus_1_84
			transl_table	11
9215	9691	CDS
			locus_tag	3300020905-1_5_8
			product	hypothetical protein Klosneuvirus_1_84
			transl_table	11
10114	9707	CDS
			locus_tag	3300020905-1_5_9
			product	hypothetical protein
			transl_table	11
10269	10991	CDS
			locus_tag	3300020905-1_5_10
			product	hypothetical protein
			transl_table	11
10921	12192	CDS
			locus_tag	3300020905-1_5_11
			product	hypothetical protein
			transl_table	11
12843	13235	CDS
			locus_tag	3300020905-1_5_12
			product	hypothetical protein
			transl_table	11
>3300020905-1_6
250	2	CDS
			locus_tag	3300020905-1_6_1
			product	hypothetical protein
			transl_table	11
1922	330	CDS
			locus_tag	3300020905-1_6_2
			product	hypothetical protein
			transl_table	11
3831	2089	CDS
			locus_tag	3300020905-1_6_3
			product	hypothetical protein
			transl_table	11
4009	4935	CDS
			locus_tag	3300020905-1_6_4
			product	hypothetical protein
			transl_table	11
5053	6216	CDS
			locus_tag	3300020905-1_6_5
			product	hypothetical protein
			transl_table	11
6347	7498	CDS
			locus_tag	3300020905-1_6_6
			product	putative flap endonuclease 1-like
			transl_table	11
7661	8518	CDS
			locus_tag	3300020905-1_6_7
			product	SWIB/MDM2 domain-containing protein
			transl_table	11
8607	9638	CDS
			locus_tag	3300020905-1_6_8
			product	hypothetical protein
			transl_table	11
10951	9695	CDS
			locus_tag	3300020905-1_6_9
			product	hypothetical protein
			transl_table	11
12550	11108	CDS
			locus_tag	3300020905-1_6_10
			product	hypothetical protein
			transl_table	11
>3300020905-1_7
570	1	CDS
			locus_tag	3300020905-1_7_1
			product	hypothetical protein
			transl_table	11
665	2851	CDS
			locus_tag	3300020905-1_7_2
			product	ankyrin repeat protein
			transl_table	11
2978	3700	CDS
			locus_tag	3300020905-1_7_3
			product	hypothetical protein
			transl_table	11
3745	5181	CDS
			locus_tag	3300020905-1_7_4
			product	hypothetical protein
			transl_table	11
5673	5260	CDS
			locus_tag	3300020905-1_7_5
			product	hypothetical protein
			transl_table	11
6503	5733	CDS
			locus_tag	3300020905-1_7_6
			product	DNA polymerase family X
			transl_table	11
7093	6776	CDS
			locus_tag	3300020905-1_7_7
			product	hypothetical protein
			transl_table	11
7989	7144	CDS
			locus_tag	3300020905-1_7_8
			product	hypothetical protein
			transl_table	11
8917	8045	CDS
			locus_tag	3300020905-1_7_9
			product	hypothetical protein
			transl_table	11
9869	9012	CDS
			locus_tag	3300020905-1_7_10
			product	hypothetical protein
			transl_table	11
10844	9945	CDS
			locus_tag	3300020905-1_7_11
			product	hypothetical protein
			transl_table	11
>3300020905-1_8
1	1044	CDS
			locus_tag	3300020905-1_8_1
			product	hypothetical protein
			transl_table	11
1458	1096	CDS
			locus_tag	3300020905-1_8_2
			product	hypothetical protein
			transl_table	11
1649	2272	CDS
			locus_tag	3300020905-1_8_3
			product	hypothetical protein
			transl_table	11
2812	2273	CDS
			locus_tag	3300020905-1_8_4
			product	hypothetical protein
			transl_table	11
2897	4201	CDS
			locus_tag	3300020905-1_8_5
			product	hypothetical protein
			transl_table	11
5888	4224	CDS
			locus_tag	3300020905-1_8_6
			product	BTB/POZ domain protein
			transl_table	11
5961	6353	CDS
			locus_tag	3300020905-1_8_7
			product	hypothetical protein
			transl_table	11
7019	6411	CDS
			locus_tag	3300020905-1_8_8
			product	hypothetical protein
			transl_table	11
7018	7125	CDS
			locus_tag	3300020905-1_8_9
			product	hypothetical protein
			transl_table	11
7955	7122	CDS
			locus_tag	3300020905-1_8_10
			product	hypothetical protein mv_L482
			transl_table	11
7983	8255	CDS
			locus_tag	3300020905-1_8_11
			product	putative ORFan
			transl_table	11
8306	9289	CDS
			locus_tag	3300020905-1_8_12
			product	hypothetical protein
			transl_table	11
10137	9298	CDS
			locus_tag	3300020905-1_8_13
			product	hypothetical protein
			transl_table	11
>3300020905-1_9
1	1020	CDS
			locus_tag	3300020905-1_9_1
			product	thioredoxin domain-containing protein
			transl_table	11
2704	1079	CDS
			locus_tag	3300020905-1_9_2
			product	DEAD/SNF2 helicase
			transl_table	11
3222	2761	CDS
			locus_tag	3300020905-1_9_3
			product	hypothetical protein
			transl_table	11
3354	4559	CDS
			locus_tag	3300020905-1_9_4
			product	hypothetical protein
			transl_table	11
4661	5875	CDS
			locus_tag	3300020905-1_9_5
			product	putative ATP-dependent RNA helicase
			transl_table	11
6446	6297	CDS
			locus_tag	3300020905-1_9_6
			product	hypothetical protein
			transl_table	11
8524	8946	CDS
			locus_tag	3300020905-1_9_7
			product	ATP-dependent RNA helicase
			transl_table	11
>3300020905-1_10
3	584	CDS
			locus_tag	3300020905-1_10_1
			product	hypothetical protein
			transl_table	11
2676	937	CDS
			locus_tag	3300020905-1_10_2
			product	putative ORFan
			transl_table	11
2989	4590	CDS
			locus_tag	3300020905-1_10_3
			product	hypothetical protein
			transl_table	11
4703	5116	CDS
			locus_tag	3300020905-1_10_4
			product	hypothetical protein
			transl_table	11
5196	5633	CDS
			locus_tag	3300020905-1_10_5
			product	hypothetical protein
			transl_table	11
5858	6358	CDS
			locus_tag	3300020905-1_10_6
			product	hypothetical protein
			transl_table	11
6593	6775	CDS
			locus_tag	3300020905-1_10_7
			product	hypothetical protein
			transl_table	11
6810	8237	CDS
			locus_tag	3300020905-1_10_8
			product	deoxyribodipyrimidine photolyase
			transl_table	11
8611	8760	CDS
			locus_tag	3300020905-1_10_9
			product	hypothetical protein
			transl_table	11
>3300020905-1_11
2	154	CDS
			locus_tag	3300020905-1_11_1
			product	hypothetical protein
			transl_table	11
319	1839	CDS
			locus_tag	3300020905-1_11_2
			product	hypothetical protein
			transl_table	11
3116	1977	CDS
			locus_tag	3300020905-1_11_3
			product	zf-C2H2-N-terminal protein
			transl_table	11
4295	3363	CDS
			locus_tag	3300020905-1_11_4
			product	zf-C2H2-N-terminal protein
			transl_table	11
4777	4595	CDS
			locus_tag	3300020905-1_11_5
			product	hypothetical protein
			transl_table	11
4932	4780	CDS
			locus_tag	3300020905-1_11_6
			product	hypothetical protein
			transl_table	11
6096	5239	CDS
			locus_tag	3300020905-1_11_7
			product	hypothetical protein
			transl_table	11
6749	6186	CDS
			locus_tag	3300020905-1_11_8
			product	hypothetical protein
			transl_table	11
7019	6888	CDS
			locus_tag	3300020905-1_11_9
			product	hypothetical protein
			transl_table	11
7811	7590	CDS
			locus_tag	3300020905-1_11_10
			product	hypothetical protein
			transl_table	11
8043	7951	CDS
			locus_tag	3300020905-1_11_11
			product	hypothetical protein
			transl_table	11
>3300020905-1_12
2	1330	CDS
			locus_tag	3300020905-1_12_1
			product	glycosyltransferase family 2
			transl_table	11
1483	2808	CDS
			locus_tag	3300020905-1_12_2
			product	hypothetical protein
			transl_table	11
3325	2843	CDS
			locus_tag	3300020905-1_12_3
			product	hypothetical protein
			transl_table	11
3364	3483	CDS
			locus_tag	3300020905-1_12_4
			product	hypothetical protein
			transl_table	11
3471	5102	CDS
			locus_tag	3300020905-1_12_5
			product	AAA family ATPase
			transl_table	11
6347	5151	CDS
			locus_tag	3300020905-1_12_6
			product	putative orfan
			transl_table	11
7214	7612	CDS
			locus_tag	3300020905-1_12_7
			product	hypothetical protein
			transl_table	11
>3300020905-1_13
3	242	CDS
			locus_tag	3300020905-1_13_1
			product	hypothetical protein
			transl_table	11
524	354	CDS
			locus_tag	3300020905-1_13_2
			product	hypothetical protein
			transl_table	11
619	521	CDS
			locus_tag	3300020905-1_13_3
			product	hypothetical protein
			transl_table	11
1871	684	CDS
			locus_tag	3300020905-1_13_4
			product	putative ORFan
			transl_table	11
2245	1958	CDS
			locus_tag	3300020905-1_13_5
			product	hypothetical protein
			transl_table	11
6048	2281	CDS
			locus_tag	3300020905-1_13_6
			product	hypothetical protein
			transl_table	11
6169	6603	CDS
			locus_tag	3300020905-1_13_7
			product	hypothetical protein Klosneuvirus_1_84
			transl_table	11
6703	7065	CDS
			locus_tag	3300020905-1_13_8
			product	hypothetical protein Klosneuvirus_3_8
			transl_table	11
>3300020905-1_14
316	2	CDS
			locus_tag	3300020905-1_14_1
			product	hypothetical protein
			transl_table	11
501	2150	CDS
			locus_tag	3300020905-1_14_2
			product	hypothetical protein
			transl_table	11
2284	2156	CDS
			locus_tag	3300020905-1_14_3
			product	hypothetical protein
			transl_table	11
2832	2320	CDS
			locus_tag	3300020905-1_14_4
			product	hypothetical protein Moumou_00647
			transl_table	11
3423	3250	CDS
			locus_tag	3300020905-1_14_5
			product	hypothetical protein
			transl_table	11
3937	3524	CDS
			locus_tag	3300020905-1_14_6
			product	hypothetical protein
			transl_table	11
3936	4034	CDS
			locus_tag	3300020905-1_14_7
			product	hypothetical protein
			transl_table	11
4027	4377	CDS
			locus_tag	3300020905-1_14_8
			product	hypothetical protein
			transl_table	11
4432	5622	CDS
			locus_tag	3300020905-1_14_9
			product	hypothetical protein
			transl_table	11
6222	5674	CDS
			locus_tag	3300020905-1_14_10
			product	hypothetical protein
			transl_table	11
6352	6726	CDS
			locus_tag	3300020905-1_14_11
			product	hypothetical protein
			transl_table	11
6919	6794	CDS
			locus_tag	3300020905-1_14_12
			product	hypothetical protein
			transl_table	11
>3300020905-1_15
3	1013	CDS
			locus_tag	3300020905-1_15_1
			product	hypothetical protein BMW23_0649
			transl_table	11
1861	1160	CDS
			locus_tag	3300020905-1_15_2
			product	hypothetical protein
			transl_table	11
2076	2693	CDS
			locus_tag	3300020905-1_15_3
			product	hypothetical protein
			transl_table	11
2745	3410	CDS
			locus_tag	3300020905-1_15_4
			product	hypothetical protein
			transl_table	11
3452	4144	CDS
			locus_tag	3300020905-1_15_5
			product	2Og-Fe(II) oxygenase
			transl_table	11
4237	5928	CDS
			locus_tag	3300020905-1_15_6
			product	packaging ATPase
			transl_table	11
6296	6132	CDS
			locus_tag	3300020905-1_15_7
			product	hypothetical protein
			transl_table	11
6295	6531	CDS
			locus_tag	3300020905-1_15_8
			product	hypothetical protein
			transl_table	11
>3300020905-1_16
1105	155	CDS
			locus_tag	3300020905-1_16_1
			product	hypothetical protein
			transl_table	11
1256	1936	CDS
			locus_tag	3300020905-1_16_2
			product	Rab family GTPase
			transl_table	11
1974	2552	CDS
			locus_tag	3300020905-1_16_3
			product	hypothetical protein
			transl_table	11
2616	3131	CDS
			locus_tag	3300020905-1_16_4
			product	hypothetical protein
			transl_table	11
3195	3656	CDS
			locus_tag	3300020905-1_16_5
			product	hypothetical protein
			transl_table	11
3690	4013	CDS
			locus_tag	3300020905-1_16_6
			product	hypothetical protein
			transl_table	11
4079	4621	CDS
			locus_tag	3300020905-1_16_7
			product	hypothetical protein
			transl_table	11
4710	5294	CDS
			locus_tag	3300020905-1_16_8
			product	hypothetical protein
			transl_table	11
5671	5381	CDS
			locus_tag	3300020905-1_16_9
			product	hypothetical protein
			transl_table	11
5844	6143	CDS
			locus_tag	3300020905-1_16_10
			product	hypothetical protein
			transl_table	11
6169	6282	CDS
			locus_tag	3300020905-1_16_11
			product	hypothetical protein
			transl_table	11
>3300020905-1_17
1647	1	CDS
			locus_tag	3300020905-1_17_1
			product	SNF2 family helicase
			transl_table	11
5430	1729	CDS
			locus_tag	3300020905-1_17_2
			product	hypothetical protein
			transl_table	11
5760	5527	CDS
			locus_tag	3300020905-1_17_3
			product	hypothetical protein
			transl_table	11
>3300020905-1_18
669	1	CDS
			locus_tag	3300020905-1_18_1
			product	hypothetical protein
			transl_table	11
1759	746	CDS
			locus_tag	3300020905-1_18_2
			product	hypothetical protein
			transl_table	11
3491	1863	CDS
			locus_tag	3300020905-1_18_3
			product	hypothetical protein
			transl_table	11
3849	3655	CDS
			locus_tag	3300020905-1_18_4
			product	hypothetical protein
			transl_table	11
5557	5673	CDS
			locus_tag	3300020905-1_18_5
			product	hypothetical protein
			transl_table	11
>3300020905-1_19
325	549	CDS
			locus_tag	3300020905-1_19_1
			product	hypothetical protein
			transl_table	11
644	1426	CDS
			locus_tag	3300020905-1_19_2
			product	hypothetical protein
			transl_table	11
2615	1461	CDS
			locus_tag	3300020905-1_19_3
			product	hypothetical protein
			transl_table	11
3128	2670	CDS
			locus_tag	3300020905-1_19_4
			product	hypothetical protein
			transl_table	11
5297	5551	CDS
			locus_tag	3300020905-1_19_5
			product	Helicase
			transl_table	11
>3300020905-1_20
1	1689	CDS
			locus_tag	3300020905-1_20_1
			product	hypothetical protein
			transl_table	11
3226	1733	CDS
			locus_tag	3300020905-1_20_2
			product	TATA-box-binding protein-like protein
			transl_table	11
3242	3409	CDS
			locus_tag	3300020905-1_20_3
			product	hypothetical protein
			transl_table	11
3397	3948	CDS
			locus_tag	3300020905-1_20_4
			product	hypothetical protein
			transl_table	11
4735	4845	CDS
			locus_tag	3300020905-1_20_5
			product	hypothetical protein mc_365
			transl_table	11
>3300020905-1_21
346	2	CDS
			locus_tag	3300020905-1_21_1
			product	hypothetical protein
			transl_table	11
1138	482	CDS
			locus_tag	3300020905-1_21_2
			product	hypothetical protein
			transl_table	11
1345	2337	CDS
			locus_tag	3300020905-1_21_3
			product	mitochondrial chaperone
			transl_table	11
2393	3145	CDS
			locus_tag	3300020905-1_21_4
			product	hypothetical protein
			transl_table	11
3126	3797	CDS
			locus_tag	3300020905-1_21_5
			product	mg680 protein
			transl_table	11
3938	3807	CDS
			locus_tag	3300020905-1_21_6
			product	hypothetical protein
			transl_table	11
4515	4712	CDS
			locus_tag	3300020905-1_21_7
			product	Pentatricopeptide repeat-containing protein, chloroplastic
			transl_table	11
>3300020905-1_22
1	1029	CDS
			locus_tag	3300020905-1_22_1
			product	serine/threonine-protein kinase Nek1
			transl_table	11
1048	1599	CDS
			locus_tag	3300020905-1_22_2
			product	hypothetical protein
			transl_table	11
1648	2268	CDS
			locus_tag	3300020905-1_22_3
			product	hypothetical protein
			transl_table	11
2336	3328	CDS
			locus_tag	3300020905-1_22_4
			product	hypothetical protein
			transl_table	11
4021	4299	CDS
			locus_tag	3300020905-1_22_5
			product	DNA topoisomerase 1
			transl_table	11
>3300020905-1_23
670	2	CDS
			locus_tag	3300020905-1_23_1
			product	hypothetical protein
			transl_table	11
1618	737	CDS
			locus_tag	3300020905-1_23_2
			product	hypothetical protein
			transl_table	11
3926	1701	CDS
			locus_tag	3300020905-1_23_3
			product	hypothetical protein
			transl_table	11
>3300020905-1_24
662	384	CDS
			locus_tag	3300020905-1_24_1
			product	hypothetical protein
			transl_table	11
2051	723	CDS
			locus_tag	3300020905-1_24_2
			product	putative bifunctional metalloprotease/ubiquitin-protein ligase
			transl_table	11
2318	2211	CDS
			locus_tag	3300020905-1_24_3
			product	hypothetical protein
			transl_table	11
3194	3550	CDS
			locus_tag	3300020905-1_24_4
			product	prohibitin domain-containing protein
			transl_table	11
>3300020905-1_25
553	65	CDS
			locus_tag	3300020905-1_25_1
			product	hypothetical protein
			transl_table	11
1596	556	CDS
			locus_tag	3300020905-1_25_2
			product	hypothetical protein
			transl_table	11
2358	1657	CDS
			locus_tag	3300020905-1_25_3
			product	putative transmembrane protein
			transl_table	11
2641	3009	CDS
			locus_tag	3300020905-1_25_4
			product	hypothetical protein Moumou_00182
			transl_table	11
3490	3053	CDS
			locus_tag	3300020905-1_25_5
			product	mg760 protein
			transl_table	11
>3300020905-1_26
248	643	CDS
			locus_tag	3300020905-1_26_1
			product	hypothetical protein
			transl_table	11
2065	692	CDS
			locus_tag	3300020905-1_26_2
			product	hypothetical protein
			transl_table	11
3373	2117	CDS
			locus_tag	3300020905-1_26_3
			product	hypothetical protein
			transl_table	11
>3300020905-1_27
1175	3	CDS
			locus_tag	3300020905-1_27_1
			product	hypothetical protein
			transl_table	11
1384	1560	CDS
			locus_tag	3300020905-1_27_2
			product	hypothetical protein
			transl_table	11
1988	1632	CDS
			locus_tag	3300020905-1_27_3
			product	hypothetical protein
			transl_table	11
2110	2307	CDS
			locus_tag	3300020905-1_27_4
			product	hypothetical protein
			transl_table	11
3376	2324	CDS
			locus_tag	3300020905-1_27_5
			product	hypothetical protein
			transl_table	11
>3300020905-1_28
587	3	CDS
			locus_tag	3300020905-1_28_1
			product	hypothetical protein
			transl_table	11
763	1440	CDS
			locus_tag	3300020905-1_28_2
			product	hypothetical protein
			transl_table	11
1446	1811	CDS
			locus_tag	3300020905-1_28_3
			product	hypothetical protein
			transl_table	11
1935	2312	CDS
			locus_tag	3300020905-1_28_4
			product	hypothetical protein
			transl_table	11
2406	3029	CDS
			locus_tag	3300020905-1_28_5
			product	collagen-like protein
			transl_table	11
3100	3186	CDS
			locus_tag	3300020905-1_28_6
			product	hypothetical protein
			transl_table	11
>3300020905-1_29
150	770	CDS
			locus_tag	3300020905-1_29_1
			product	hypothetical protein
			transl_table	11
1043	798	CDS
			locus_tag	3300020905-1_29_2
			product	hypothetical protein Klosneuvirus_1_332
			transl_table	11
1341	1874	CDS
			locus_tag	3300020905-1_29_3
			product	hypothetical protein Klosneuvirus_1_333
			transl_table	11
1945	2718	CDS
			locus_tag	3300020905-1_29_4
			product	hypothetical protein
			transl_table	11
>3300020905-1_30
2	751	CDS
			locus_tag	3300020905-1_30_1
			product	hypothetical protein
			transl_table	11
1550	906	CDS
			locus_tag	3300020905-1_30_2
			product	hypothetical protein
			transl_table	11
2437	2766	CDS
			locus_tag	3300020905-1_30_3
			product	hypothetical protein
			transl_table	11
>3300020905-1_31
1247	3	CDS
			locus_tag	3300020905-1_31_1
			product	putative serine/threonine-protein kinase
			transl_table	11
2546	1311	CDS
			locus_tag	3300020905-1_31_2
			product	hypothetical protein
			transl_table	11
