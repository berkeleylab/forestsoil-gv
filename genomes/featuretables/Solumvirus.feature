>Solumvirus_1
2	739	CDS
			locus_tag	Solumvirus_1_1
			product	hypothetical protein
			transl_table	11
1259	843	CDS
			locus_tag	Solumvirus_1_2
			product	hypothetical protein
			transl_table	11
2711	1437	CDS
			locus_tag	Solumvirus_1_3
			product	hypothetical protein
			transl_table	11
2830	4179	CDS
			locus_tag	Solumvirus_1_4
			product	glycosyltransferase
			transl_table	11
4327	5013	CDS
			locus_tag	Solumvirus_1_5
			product	hypothetical protein
			transl_table	11
5795	5046	CDS
			locus_tag	Solumvirus_1_6
			product	hypothetical protein
			transl_table	11
5850	6071	CDS
			locus_tag	Solumvirus_1_7
			product	hypothetical protein
			transl_table	11
6745	6119	CDS
			locus_tag	Solumvirus_1_8
			product	hypothetical protein
			transl_table	11
7808	6825	CDS
			locus_tag	Solumvirus_1_9
			product	hypothetical protein
			transl_table	11
7869	8708	CDS
			locus_tag	Solumvirus_1_10
			product	hypothetical protein
			transl_table	11
9630	8761	CDS
			locus_tag	Solumvirus_1_11
			product	DNA-directed RNA Pol II C-term-like phosphatase
			transl_table	11
10372	9773	CDS
			locus_tag	Solumvirus_1_12
			product	hypothetical protein
			transl_table	11
12040	10631	CDS
			locus_tag	Solumvirus_1_13
			product	Hypothetical protein ORPV_960
			transl_table	11
12093	12590	CDS
			locus_tag	Solumvirus_1_14
			product	hypothetical protein
			transl_table	11
13180	12722	CDS
			locus_tag	Solumvirus_1_15
			product	hypothetical protein
			transl_table	11
13314	16013	CDS
			locus_tag	Solumvirus_1_16
			product	ATP-dependent RecD-like DNA helicase
			transl_table	11
16096	17181	CDS
			locus_tag	Solumvirus_1_17
			product	hypothetical protein
			transl_table	11
17257	18360	CDS
			locus_tag	Solumvirus_1_18
			product	hypothetical protein
			transl_table	11
18838	19551	CDS
			locus_tag	Solumvirus_1_19
			product	hypothetical protein
			transl_table	11
20260	19646	CDS
			locus_tag	Solumvirus_1_20
			product	hypothetical protein
			transl_table	11
20596	20393	CDS
			locus_tag	Solumvirus_1_21
			product	hypothetical protein
			transl_table	11
21716	20808	CDS
			locus_tag	Solumvirus_1_22
			product	hypothetical protein
			transl_table	11
21710	23260	CDS
			locus_tag	Solumvirus_1_23
			product	hypothetical protein
			transl_table	11
24615	23479	CDS
			locus_tag	Solumvirus_1_24
			product	hypothetical protein
			transl_table	11
25046	25843	CDS
			locus_tag	Solumvirus_1_25
			product	hypothetical protein BQ9231_00400
			transl_table	11
27504	25888	CDS
			locus_tag	Solumvirus_1_26
			product	hypothetical protein
			transl_table	11
27562	28044	CDS
			locus_tag	Solumvirus_1_27
			product	hypothetical protein
			transl_table	11
29542	28097	CDS
			locus_tag	Solumvirus_1_28
			product	hypothetical protein COA94_02180
			transl_table	11
30203	29637	CDS
			locus_tag	Solumvirus_1_29
			product	hypothetical protein COA94_02980
			transl_table	11
31364	30297	CDS
			locus_tag	Solumvirus_1_30
			product	hypothetical protein
			transl_table	11
32809	31427	CDS
			locus_tag	Solumvirus_1_31
			product	VV A18-like helicase
			transl_table	11
33016	33741	CDS
			locus_tag	Solumvirus_1_32
			product	TFIIB transcription initiation factor
			transl_table	11
33743	34513	CDS
			locus_tag	Solumvirus_1_33
			product	hypothetical protein
			transl_table	11
35475	34693	CDS
			locus_tag	Solumvirus_1_34
			product	hypothetical protein pv_68
			transl_table	11
35552	36328	CDS
			locus_tag	Solumvirus_1_35
			product	hypothetical protein OlV7_192c
			transl_table	11
37847	36624	CDS
			locus_tag	Solumvirus_1_36
			product	hypothetical protein
			transl_table	11
39467	37953	CDS
			locus_tag	Solumvirus_1_37
			product	Flap endonuclease 1
			transl_table	11
39698	40204	CDS
			locus_tag	Solumvirus_1_38
			product	hypothetical protein
			transl_table	11
40255	40683	CDS
			locus_tag	Solumvirus_1_39
			product	hypothetical protein
			transl_table	11
40853	41398	CDS
			locus_tag	Solumvirus_1_40
			product	hypothetical protein
			transl_table	11
41432	42091	CDS
			locus_tag	Solumvirus_1_41
			product	hypothetical protein
			transl_table	11
42159	45722	CDS
			locus_tag	Solumvirus_1_42
			product	DNA polymerase delta catalytic subunit
			transl_table	11
47317	46394	CDS
			locus_tag	Solumvirus_1_43
			product	hypothetical protein pv_456
			transl_table	11
47598	48113	CDS
			locus_tag	Solumvirus_1_44
			product	hypothetical protein
			transl_table	11
48422	50230	CDS
			locus_tag	Solumvirus_1_45
			product	Serine/Threonine protein kinase
			transl_table	11
50288	51274	CDS
			locus_tag	Solumvirus_1_46
			product	hypothetical protein
			transl_table	11
51355	52680	CDS
			locus_tag	Solumvirus_1_47
			product	hypothetical protein
			transl_table	11
53015	57160	CDS
			locus_tag	Solumvirus_1_48
			product	topoisomerase IIA
			transl_table	11
58303	57224	CDS
			locus_tag	Solumvirus_1_49
			product	hypothetical protein
			transl_table	11
58398	59021	CDS
			locus_tag	Solumvirus_1_50
			product	hypothetical protein
			transl_table	11
59271	59083	CDS
			locus_tag	Solumvirus_1_51
			product	hypothetical protein
			transl_table	11
59331	59930	CDS
			locus_tag	Solumvirus_1_52
			product	hypothetical protein
			transl_table	11
62518	60233	CDS
			locus_tag	Solumvirus_1_53
			product	Protein kinase
			transl_table	11
62650	63327	CDS
			locus_tag	Solumvirus_1_54
			product	hypothetical protein
			transl_table	11
64306	63362	CDS
			locus_tag	Solumvirus_1_55
			product	hypothetical protein
			transl_table	11
64378	65748	CDS
			locus_tag	Solumvirus_1_56
			product	hypothetical protein
			transl_table	11
65852	66388	CDS
			locus_tag	Solumvirus_1_57
			product	hypothetical protein
			transl_table	11
67940	66816	CDS
			locus_tag	Solumvirus_1_58
			product	hypothetical protein
			transl_table	11
69339	68005	CDS
			locus_tag	Solumvirus_1_59
			product	Lyase; related to non-heme alpha ketoglutarate dependent carbocyclase
			transl_table	11
69982	69455	CDS
			locus_tag	Solumvirus_1_60
			product	hypothetical protein
			transl_table	11
71212	70085	CDS
			locus_tag	Solumvirus_1_61
			product	hypothetical protein
			transl_table	11
71549	71256	CDS
			locus_tag	Solumvirus_1_62
			product	hypothetical protein
			transl_table	11
71657	72382	CDS
			locus_tag	Solumvirus_1_63
			product	hypothetical protein ACA1_056890
			transl_table	11
73132	72470	CDS
			locus_tag	Solumvirus_1_64
			product	hypothetical protein
			transl_table	11
74233	73253	CDS
			locus_tag	Solumvirus_1_65
			product	PNK3P-domain-containing protein
			transl_table	11
74461	77769	CDS
			locus_tag	Solumvirus_1_66
			product	hypothetical protein
			transl_table	11
77843	78811	CDS
			locus_tag	Solumvirus_1_67
			product	hypothetical protein
			transl_table	11
80440	80565	CDS
			locus_tag	Solumvirus_1_68
			product	hypothetical protein
			transl_table	11
>Solumvirus_2
483	1	CDS
			locus_tag	Solumvirus_2_1
			product	hypothetical protein
			transl_table	11
1177	629	CDS
			locus_tag	Solumvirus_2_2
			product	hypothetical protein
			transl_table	11
2931	1216	CDS
			locus_tag	Solumvirus_2_3
			product	5'-3' exoribonuclease
			transl_table	11
4146	3058	CDS
			locus_tag	Solumvirus_2_4
			product	Putative HNH endonuclease
			transl_table	11
4511	4254	CDS
			locus_tag	Solumvirus_2_5
			product	hypothetical protein
			transl_table	11
4676	5608	CDS
			locus_tag	Solumvirus_2_6
			product	Leucine rich repeat N-terminal domain and Serine-threonine/tyrosine-protein kinase catalytic domain containing protein
			transl_table	11
5788	7923	CDS
			locus_tag	Solumvirus_2_7
			product	predicted protein
			transl_table	11
8034	8735	CDS
			locus_tag	Solumvirus_2_8
			product	meiosis specific serine/threonine protein kinase MEK1, putative
			transl_table	11
8945	9568	CDS
			locus_tag	Solumvirus_2_9
			product	hypothetical protein
			transl_table	11
10111	9719	CDS
			locus_tag	Solumvirus_2_10
			product	hypothetical protein
			transl_table	11
10333	11001	CDS
			locus_tag	Solumvirus_2_11
			product	hypothetical protein
			transl_table	11
11147	11641	CDS
			locus_tag	Solumvirus_2_12
			product	hypothetical protein
			transl_table	11
11714	12520	CDS
			locus_tag	Solumvirus_2_13
			product	hypothetical protein
			transl_table	11
12999	12589	CDS
			locus_tag	Solumvirus_2_14
			product	Domain of unknown function (DUF4326)
			transl_table	11
13637	13837	CDS
			locus_tag	Solumvirus_2_15
			product	hypothetical protein
			transl_table	11
14102	14218	CDS
			locus_tag	Solumvirus_2_16
			product	hypothetical protein
			transl_table	11
14391	14873	CDS
			locus_tag	Solumvirus_2_17
			product	hypothetical protein
			transl_table	11
14913	15755	CDS
			locus_tag	Solumvirus_2_18
			product	hypothetical protein SteCoe_2939
			transl_table	11
15851	16267	CDS
			locus_tag	Solumvirus_2_19
			product	hypothetical protein
			transl_table	11
16357	16773	CDS
			locus_tag	Solumvirus_2_20
			product	hypothetical protein
			transl_table	11
17508	17068	CDS
			locus_tag	Solumvirus_2_21
			product	hypothetical protein Catovirus_2_26
			transl_table	11
17607	18773	CDS
			locus_tag	Solumvirus_2_22
			product	hypothetical protein
			transl_table	11
18822	19715	CDS
			locus_tag	Solumvirus_2_23
			product	hypothetical protein CDL15_Pgr003820
			transl_table	11
21389	19791	CDS
			locus_tag	Solumvirus_2_24
			product	hypothetical protein
			transl_table	11
23029	21575	CDS
			locus_tag	Solumvirus_2_25
			product	hypothetical protein
			transl_table	11
24941	23094	CDS
			locus_tag	Solumvirus_2_26
			product	hypothetical protein
			transl_table	11
25126	25548	CDS
			locus_tag	Solumvirus_2_27
			product	hypothetical protein
			transl_table	11
26948	25644	CDS
			locus_tag	Solumvirus_2_28
			product	hypothetical protein
			transl_table	11
27163	27486	CDS
			locus_tag	Solumvirus_2_29
			product	hypothetical protein
			transl_table	11
27981	27556	CDS
			locus_tag	Solumvirus_2_30
			product	hypothetical protein
			transl_table	11
28245	30836	CDS
			locus_tag	Solumvirus_2_31
			product	hypothetical protein
			transl_table	11
30953	31750	CDS
			locus_tag	Solumvirus_2_32
			product	hypothetical protein
			transl_table	11
31938	32042	CDS
			locus_tag	Solumvirus_2_33
			product	hypothetical protein
			transl_table	11
32710	32039	CDS
			locus_tag	Solumvirus_2_34
			product	hypothetical protein PBRA_007909
			transl_table	11
34220	32904	CDS
			locus_tag	Solumvirus_2_35
			product	hypothetical protein
			transl_table	11
35420	34332	CDS
			locus_tag	Solumvirus_2_36
			product	hypothetical protein
			transl_table	11
37435	35555	CDS
			locus_tag	Solumvirus_2_37
			product	hypothetical protein BK133_14225
			transl_table	11
39931	37760	CDS
			locus_tag	Solumvirus_2_38
			product	hypothetical protein
			transl_table	11
40040	41449	CDS
			locus_tag	Solumvirus_2_39
			product	hypothetical protein
			transl_table	11
41572	42603	CDS
			locus_tag	Solumvirus_2_40
			product	hypothetical protein
			transl_table	11
43817	42645	CDS
			locus_tag	Solumvirus_2_41
			product	hypothetical protein
			transl_table	11
43893	44603	CDS
			locus_tag	Solumvirus_2_42
			product	hypothetical protein
			transl_table	11
46801	44648	CDS
			locus_tag	Solumvirus_2_43
			product	hypothetical protein COA94_09205
			transl_table	11
46915	48774	CDS
			locus_tag	Solumvirus_2_44
			product	Hypothetical protein ORPV_824
			transl_table	11
48971	49282	CDS
			locus_tag	Solumvirus_2_45
			product	hypothetical protein
			transl_table	11
50553	49339	CDS
			locus_tag	Solumvirus_2_46
			product	hypothetical protein
			transl_table	11
51410	50733	CDS
			locus_tag	Solumvirus_2_47
			product	hypothetical protein
			transl_table	11
51632	53380	CDS
			locus_tag	Solumvirus_2_48
			product	hypothetical protein
			transl_table	11
54988	53723	CDS
			locus_tag	Solumvirus_2_49
			product	hypothetical protein
			transl_table	11
55079	55852	CDS
			locus_tag	Solumvirus_2_50
			product	hypothetical protein
			transl_table	11
55948	56574	CDS
			locus_tag	Solumvirus_2_51
			product	hypothetical protein
			transl_table	11
57484	57741	CDS
			locus_tag	Solumvirus_2_52
			product	hypothetical protein
			transl_table	11
57840	58256	CDS
			locus_tag	Solumvirus_2_53
			product	hypothetical protein
			transl_table	11
58331	58687	CDS
			locus_tag	Solumvirus_2_54
			product	hypothetical protein
			transl_table	11
58821	59216	CDS
			locus_tag	Solumvirus_2_55
			product	hypothetical protein
			transl_table	11
59998	59279	CDS
			locus_tag	Solumvirus_2_56
			product	hypothetical protein
			transl_table	11
60080	60319	CDS
			locus_tag	Solumvirus_2_57
			product	hypothetical protein
			transl_table	11
61023	60649	CDS
			locus_tag	Solumvirus_2_58
			product	hypothetical protein
			transl_table	11
61314	61775	CDS
			locus_tag	Solumvirus_2_59
			product	hypothetical protein
			transl_table	11
62044	61850	CDS
			locus_tag	Solumvirus_2_60
			product	hypothetical protein
			transl_table	11
62739	62410	CDS
			locus_tag	Solumvirus_2_61
			product	hypothetical protein
			transl_table	11
63272	62748	CDS
			locus_tag	Solumvirus_2_62
			product	hypothetical protein
			transl_table	11
63380	64564	CDS
			locus_tag	Solumvirus_2_63
			product	His-Me finger endonuclease, partial
			transl_table	11
67437	64609	CDS
			locus_tag	Solumvirus_2_64
			product	hypothetical protein A2928_00365, partial
			transl_table	11
67534	68847	CDS
			locus_tag	Solumvirus_2_65
			product	hypothetical protein
			transl_table	11
68916	70184	CDS
			locus_tag	Solumvirus_2_66
			product	hypothetical protein
			transl_table	11
70739	70266	CDS
			locus_tag	Solumvirus_2_67
			product	hypothetical protein
			transl_table	11
71714	71013	CDS
			locus_tag	Solumvirus_2_68
			product	hypothetical protein
			transl_table	11
71923	72372	CDS
			locus_tag	Solumvirus_2_69
			product	hypothetical protein
			transl_table	11
73807	72440	CDS
			locus_tag	Solumvirus_2_70
			product	hypothetical protein
			transl_table	11
74986	75117	CDS
			locus_tag	Solumvirus_2_71
			product	hypothetical protein
			transl_table	11
>Solumvirus_3
317	1612	CDS
			locus_tag	Solumvirus_3_1
			product	ATP-dependent DNA ligase
			transl_table	11
1876	2178	CDS
			locus_tag	Solumvirus_3_2
			product	hypothetical protein
			transl_table	11
3656	2235	CDS
			locus_tag	Solumvirus_3_3
			product	divergent major capsid protein
			transl_table	11
4103	5374	CDS
			locus_tag	Solumvirus_3_4
			product	hypothetical protein
			transl_table	11
6094	5570	CDS
			locus_tag	Solumvirus_3_5
			product	hypothetical protein COA94_02950
			transl_table	11
6221	7513	CDS
			locus_tag	Solumvirus_3_6
			product	hypothetical protein
			transl_table	11
7942	8772	CDS
			locus_tag	Solumvirus_3_7
			product	hypothetical protein
			transl_table	11
8928	12437	CDS
			locus_tag	Solumvirus_3_8
			product	mRNA-capping enzyme
			transl_table	11
14335	12809	CDS
			locus_tag	Solumvirus_3_9
			product	hypothetical protein
			transl_table	11
15633	14500	CDS
			locus_tag	Solumvirus_3_10
			product	hypothetical protein
			transl_table	11
15670	16002	CDS
			locus_tag	Solumvirus_3_11
			product	hypothetical protein
			transl_table	11
15999	16271	CDS
			locus_tag	Solumvirus_3_12
			product	hypothetical protein
			transl_table	11
16987	16514	CDS
			locus_tag	Solumvirus_3_13
			product	hypothetical protein
			transl_table	11
17836	17318	CDS
			locus_tag	Solumvirus_3_14
			product	hypothetical protein
			transl_table	11
18304	17924	CDS
			locus_tag	Solumvirus_3_15
			product	hypothetical protein
			transl_table	11
18850	18401	CDS
			locus_tag	Solumvirus_3_16
			product	hypothetical protein
			transl_table	11
20019	18907	CDS
			locus_tag	Solumvirus_3_17
			product	patatin-like phospholipase
			transl_table	11
20953	20141	CDS
			locus_tag	Solumvirus_3_18
			product	hypothetical protein
			transl_table	11
21087	21725	CDS
			locus_tag	Solumvirus_3_19
			product	hypothetical protein
			transl_table	11
23214	21898	CDS
			locus_tag	Solumvirus_3_20
			product	hypothetical protein
			transl_table	11
23499	24521	CDS
			locus_tag	Solumvirus_3_21
			product	hypothetical protein
			transl_table	11
24706	25926	CDS
			locus_tag	Solumvirus_3_22
			product	hypothetical protein
			transl_table	11
26140	25967	CDS
			locus_tag	Solumvirus_3_23
			product	hypothetical protein
			transl_table	11
27106	26516	CDS
			locus_tag	Solumvirus_3_24
			product	hypothetical protein COA94_02840
			transl_table	11
28856	27948	CDS
			locus_tag	Solumvirus_3_25
			product	hypothetical protein pv_445
			transl_table	11
30015	28912	CDS
			locus_tag	Solumvirus_3_26
			product	hypothetical protein
			transl_table	11
32980	30374	CDS
			locus_tag	Solumvirus_3_27
			product	DNA double-stranded break repair ATPase
			transl_table	11
33061	34200	CDS
			locus_tag	Solumvirus_3_28
			product	hypothetical protein
			transl_table	11
35165	34515	CDS
			locus_tag	Solumvirus_3_29
			product	hypothetical protein
			transl_table	11
36872	35334	CDS
			locus_tag	Solumvirus_3_30
			product	RNAse III
			transl_table	11
37122	37847	CDS
			locus_tag	Solumvirus_3_31
			product	hypothetical protein
			transl_table	11
38533	38018	CDS
			locus_tag	Solumvirus_3_32
			product	broad specificity phosphatase PhoE
			transl_table	11
38764	42327	CDS
			locus_tag	Solumvirus_3_33
			product	VETF-like early transcription factor large subunit
			transl_table	11
42475	43260	CDS
			locus_tag	Solumvirus_3_34
			product	hypothetical protein
			transl_table	11
43765	43355	CDS
			locus_tag	Solumvirus_3_35
			product	hypothetical protein
			transl_table	11
44591	43920	CDS
			locus_tag	Solumvirus_3_36
			product	hypothetical protein
			transl_table	11
45195	46301	CDS
			locus_tag	Solumvirus_3_37
			product	hypothetical protein SAMD00019534_024380
			transl_table	11
46946	47041	CDS
			locus_tag	Solumvirus_3_38
			product	hypothetical protein
			transl_table	11
>Solumvirus_4
188	3	CDS
			locus_tag	Solumvirus_4_1
			product	hypothetical protein
			transl_table	11
1619	687	CDS
			locus_tag	Solumvirus_4_2
			product	hypothetical protein
			transl_table	11
1708	4314	CDS
			locus_tag	Solumvirus_4_3
			product	hypothetical protein DICPUDRAFT_95528
			transl_table	11
4430	6232	CDS
			locus_tag	Solumvirus_4_4
			product	hypothetical protein
			transl_table	11
7252	6284	CDS
			locus_tag	Solumvirus_4_5
			product	hypothetical protein
			transl_table	11
7400	8587	CDS
			locus_tag	Solumvirus_4_6
			product	hypothetical protein
			transl_table	11
8789	10639	CDS
			locus_tag	Solumvirus_4_7
			product	hypothetical protein COA94_02040
			transl_table	11
10782	11783	CDS
			locus_tag	Solumvirus_4_8
			product	hypothetical protein
			transl_table	11
11867	12013	CDS
			locus_tag	Solumvirus_4_9
			product	hypothetical protein
			transl_table	11
13370	12000	CDS
			locus_tag	Solumvirus_4_10
			product	hypothetical protein
			transl_table	11
14260	13484	CDS
			locus_tag	Solumvirus_4_11
			product	hypothetical protein
			transl_table	11
14461	14958	CDS
			locus_tag	Solumvirus_4_12
			product	hypothetical protein
			transl_table	11
15544	15011	CDS
			locus_tag	Solumvirus_4_13
			product	hypothetical protein
			transl_table	11
16287	15703	CDS
			locus_tag	Solumvirus_4_14
			product	hypothetical protein
			transl_table	11
16480	17001	CDS
			locus_tag	Solumvirus_4_15
			product	Uncharacterized protein XD50_1029
			transl_table	11
17191	18165	CDS
			locus_tag	Solumvirus_4_16
			product	hypothetical protein
			transl_table	11
19911	18202	CDS
			locus_tag	Solumvirus_4_17
			product	hypothetical protein CBE49_07015
			transl_table	11
20019	20657	CDS
			locus_tag	Solumvirus_4_18
			product	hypothetical protein COW78_09265
			transl_table	11
20733	22436	CDS
			locus_tag	Solumvirus_4_19
			product	methyltransferase domain-containing protein
			transl_table	11
22505	24250	CDS
			locus_tag	Solumvirus_4_20
			product	hypothetical protein A2355_14860
			transl_table	11
24376	25794	CDS
			locus_tag	Solumvirus_4_21
			product	hypothetical protein A3D18_05065
			transl_table	11
25874	28627	CDS
			locus_tag	Solumvirus_4_22
			product	hypothetical protein COW79_08650
			transl_table	11
28747	29703	CDS
			locus_tag	Solumvirus_4_23
			product	glycosyltransferase
			transl_table	11
31623	30628	CDS
			locus_tag	Solumvirus_4_24
			product	hypothetical protein
			transl_table	11
31679	32599	CDS
			locus_tag	Solumvirus_4_25
			product	glycosyltransferase family 25
			transl_table	11
33688	32711	CDS
			locus_tag	Solumvirus_4_26
			product	hypothetical protein A3F66_05280
			transl_table	11
33876	35453	CDS
			locus_tag	Solumvirus_4_27
			product	hypothetical protein
			transl_table	11
35571	35993	CDS
			locus_tag	Solumvirus_4_28
			product	hypothetical protein
			transl_table	11
36003	36272	CDS
			locus_tag	Solumvirus_4_29
			product	hypothetical protein
			transl_table	11
36316	36816	CDS
			locus_tag	Solumvirus_4_30
			product	DUF2493 domain-containing protein
			transl_table	11
37438	37259	CDS
			locus_tag	Solumvirus_4_31
			product	hypothetical protein
			transl_table	11
>Solumvirus_5
2	946	CDS
			locus_tag	Solumvirus_5_1
			product	DNA repair exonuclease
			transl_table	11
1390	1608	CDS
			locus_tag	Solumvirus_5_2
			product	hypothetical protein
			transl_table	11
2916	1696	CDS
			locus_tag	Solumvirus_5_3
			product	hypothetical protein
			transl_table	11
3390	3977	CDS
			locus_tag	Solumvirus_5_4
			product	hypothetical protein
			transl_table	11
4474	5535	CDS
			locus_tag	Solumvirus_5_5
			product	hypothetical protein
			transl_table	11
5668	6942	CDS
			locus_tag	Solumvirus_5_6
			product	hypothetical protein
			transl_table	11
6990	8162	CDS
			locus_tag	Solumvirus_5_7
			product	Patatin-like phospholipase
			transl_table	11
8159	8935	CDS
			locus_tag	Solumvirus_5_8
			product	hypothetical protein
			transl_table	11
9815	8994	CDS
			locus_tag	Solumvirus_5_9
			product	hypothetical protein
			transl_table	11
10037	11788	CDS
			locus_tag	Solumvirus_5_10
			product	hypothetical protein
			transl_table	11
11898	13190	CDS
			locus_tag	Solumvirus_5_11
			product	hypothetical protein
			transl_table	11
13689	13234	CDS
			locus_tag	Solumvirus_5_12
			product	hypothetical protein
			transl_table	11
15815	13719	CDS
			locus_tag	Solumvirus_5_13
			product	hypothetical protein AYO45_02875
			transl_table	11
17095	15893	CDS
			locus_tag	Solumvirus_5_14
			product	hypothetical protein
			transl_table	11
17233	18867	CDS
			locus_tag	Solumvirus_5_15
			product	PDZ domain-containing protein
			transl_table	11
18967	20199	CDS
			locus_tag	Solumvirus_5_16
			product	hypothetical protein
			transl_table	11
20955	20254	CDS
			locus_tag	Solumvirus_5_17
			product	hypothetical protein
			transl_table	11
22349	21024	CDS
			locus_tag	Solumvirus_5_18
			product	hypothetical protein
			transl_table	11
22520	23170	CDS
			locus_tag	Solumvirus_5_19
			product	hypothetical protein
			transl_table	11
23354	23205	CDS
			locus_tag	Solumvirus_5_20
			product	hypothetical protein
			transl_table	11
23469	24521	CDS
			locus_tag	Solumvirus_5_21
			product	PREDICTED: eukaryotic translation initiation factor 2-alpha kinase 1
			transl_table	11
24590	26293	CDS
			locus_tag	Solumvirus_5_22
			product	hypothetical protein
			transl_table	11
26451	26999	CDS
			locus_tag	Solumvirus_5_23
			product	hypothetical protein
			transl_table	11
27062	27475	CDS
			locus_tag	Solumvirus_5_24
			product	hypothetical protein
			transl_table	11
27931	27674	CDS
			locus_tag	Solumvirus_5_25
			product	hypothetical protein
			transl_table	11
29205	28168	CDS
			locus_tag	Solumvirus_5_26
			product	hypothetical protein
			transl_table	11
29296	30162	CDS
			locus_tag	Solumvirus_5_27
			product	hypothetical protein
			transl_table	11
32585	31224	CDS
			locus_tag	Solumvirus_5_28
			product	Cyclin-dependent kinase
			transl_table	11
32665	33153	CDS
			locus_tag	Solumvirus_5_29
			product	hypothetical protein
			transl_table	11
33228	33455	CDS
			locus_tag	Solumvirus_5_30
			product	hypothetical protein
			transl_table	11
34507	33452	CDS
			locus_tag	Solumvirus_5_31
			product	hypothetical protein
			transl_table	11
35868	34609	CDS
			locus_tag	Solumvirus_5_32
			product	DNA-directed RNA polymerase RPB1
			transl_table	11
>Solumvirus_6
2	724	CDS
			locus_tag	Solumvirus_6_1
			product	hypothetical protein
			transl_table	11
886	1245	CDS
			locus_tag	Solumvirus_6_2
			product	hypothetical protein
			transl_table	11
2557	1340	CDS
			locus_tag	Solumvirus_6_3
			product	hypothetical protein
			transl_table	11
2666	3793	CDS
			locus_tag	Solumvirus_6_4
			product	hypothetical protein
			transl_table	11
7531	4223	CDS
			locus_tag	Solumvirus_6_5
			product	VV D5-like helicase
			transl_table	11
8770	8003	CDS
			locus_tag	Solumvirus_6_6
			product	hypothetical protein
			transl_table	11
9474	8944	CDS
			locus_tag	Solumvirus_6_7
			product	hypothetical protein
			transl_table	11
9537	10934	CDS
			locus_tag	Solumvirus_6_8
			product	glycosyltransferase family 1 protein
			transl_table	11
10944	11408	CDS
			locus_tag	Solumvirus_6_9
			product	hypothetical protein
			transl_table	11
11509	13416	CDS
			locus_tag	Solumvirus_6_10
			product	DNA helicase/primase
			transl_table	11
14022	13447	CDS
			locus_tag	Solumvirus_6_11
			product	hypothetical protein
			transl_table	11
14265	14831	CDS
			locus_tag	Solumvirus_6_12
			product	hypothetical protein
			transl_table	11
16423	14960	CDS
			locus_tag	Solumvirus_6_13
			product	hypothetical protein
			transl_table	11
16440	17804	CDS
			locus_tag	Solumvirus_6_14
			product	RPB5 subunit of DNA-directed RNA polymerase
			transl_table	11
18697	18284	CDS
			locus_tag	Solumvirus_6_15
			product	hypothetical protein
			transl_table	11
19197	18733	CDS
			locus_tag	Solumvirus_6_16
			product	hypothetical protein
			transl_table	11
19388	20173	CDS
			locus_tag	Solumvirus_6_17
			product	Formamidopyrimidine-DNA glycosylase
			transl_table	11
20553	20275	CDS
			locus_tag	Solumvirus_6_18
			product	hypothetical protein
			transl_table	11
21077	20598	CDS
			locus_tag	Solumvirus_6_19
			product	hypothetical protein
			transl_table	11
22365	21196	CDS
			locus_tag	Solumvirus_6_20
			product	hypothetical protein
			transl_table	11
>Solumvirus_7
3	140	CDS
			locus_tag	Solumvirus_7_1
			product	deoxyuridine 5 -triphosphate nucleotidohydrolase
			transl_table	11
635	276	CDS
			locus_tag	Solumvirus_7_2
			product	hypothetical protein
			transl_table	11
808	1812	CDS
			locus_tag	Solumvirus_7_3
			product	class II apurinic/apyrimidinic(AP)-endonuclease
			transl_table	11
2384	1899	CDS
			locus_tag	Solumvirus_7_4
			product	hypothetical protein
			transl_table	11
2488	3924	CDS
			locus_tag	Solumvirus_7_5
			product	hypothetical protein
			transl_table	11
4282	3965	CDS
			locus_tag	Solumvirus_7_6
			product	hypothetical protein
			transl_table	11
4424	5470	CDS
			locus_tag	Solumvirus_7_7
			product	hypothetical protein
			transl_table	11
5840	5553	CDS
			locus_tag	Solumvirus_7_8
			product	hypothetical protein
			transl_table	11
6022	7713	CDS
			locus_tag	Solumvirus_7_9
			product	Pqq-dependent dehydrogenase
			transl_table	11
8470	8069	CDS
			locus_tag	Solumvirus_7_10
			product	hypothetical protein
			transl_table	11
>Solumvirus_8
583	2	CDS
			locus_tag	Solumvirus_8_1
			product	hypothetical protein
			transl_table	11
696	1694	CDS
			locus_tag	Solumvirus_8_2
			product	VV D6-like helicase
			transl_table	11
1867	3594	CDS
			locus_tag	Solumvirus_8_3
			product	VV D6-like helicase
			transl_table	11
4350	4192	CDS
			locus_tag	Solumvirus_8_4
			product	hypothetical protein
			transl_table	11
4799	5167	CDS
			locus_tag	Solumvirus_8_5
			product	hypothetical protein
			transl_table	11
>Solumvirus_9
3	872	CDS
			locus_tag	Solumvirus_9_1
			product	hypothetical protein
			transl_table	11
943	2205	CDS
			locus_tag	Solumvirus_9_2
			product	hypothetical protein SAMD00019534_047700
			transl_table	11
2374	3270	CDS
			locus_tag	Solumvirus_9_3
			product	hypothetical protein
			transl_table	11
3810	4040	CDS
			locus_tag	Solumvirus_9_4
			product	hypothetical protein
			transl_table	11
