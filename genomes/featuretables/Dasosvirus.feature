>Dasosvirus_1
180	1	CDS
			locus_tag	Dasosvirus_1_1
			product	hypothetical protein
			transl_table	11
466	882	CDS
			locus_tag	Dasosvirus_1_2
			product	hypothetical protein
			transl_table	11
980	1258	CDS
			locus_tag	Dasosvirus_1_3
			product	hypothetical protein
			transl_table	11
1367	1272	CDS
			locus_tag	Dasosvirus_1_4
			product	hypothetical protein
			transl_table	11
3754	1448	CDS
			locus_tag	Dasosvirus_1_5
			product	hypothetical protein Indivirus_1_31
			transl_table	11
3835	4689	CDS
			locus_tag	Dasosvirus_1_6
			product	HTH and integrase core domain protein
			transl_table	11
5458	4886	CDS
			locus_tag	Dasosvirus_1_7
			product	hypothetical protein
			transl_table	11
5524	6369	CDS
			locus_tag	Dasosvirus_1_8
			product	hypothetical protein
			transl_table	11
6466	6924	CDS
			locus_tag	Dasosvirus_1_9
			product	hypothetical protein
			transl_table	11
7800	6925	CDS
			locus_tag	Dasosvirus_1_10
			product	metal dependent phosphohydrolase
			transl_table	11
8006	9037	CDS
			locus_tag	Dasosvirus_1_11
			product	hypothetical protein
			transl_table	11
9149	10291	CDS
			locus_tag	Dasosvirus_1_12
			product	hypothetical protein
			transl_table	11
10365	11489	CDS
			locus_tag	Dasosvirus_1_13
			product	hypothetical protein
			transl_table	11
11546	12682	CDS
			locus_tag	Dasosvirus_1_14
			product	hypothetical protein
			transl_table	11
12750	13808	CDS
			locus_tag	Dasosvirus_1_15
			product	hypothetical protein
			transl_table	11
13987	15042	CDS
			locus_tag	Dasosvirus_1_16
			product	hypothetical protein
			transl_table	11
15144	16277	CDS
			locus_tag	Dasosvirus_1_17
			product	hypothetical protein
			transl_table	11
16336	17517	CDS
			locus_tag	Dasosvirus_1_18
			product	hypothetical protein
			transl_table	11
17636	17980	CDS
			locus_tag	Dasosvirus_1_19
			product	hypothetical protein
			transl_table	11
18123	18797	CDS
			locus_tag	Dasosvirus_1_20
			product	hypothetical protein
			transl_table	11
19615	18809	CDS
			locus_tag	Dasosvirus_1_21
			product	AAA family ATPase
			transl_table	11
19921	23868	CDS
			locus_tag	Dasosvirus_1_22
			product	Flap endonuclease 1
			transl_table	11
23989	25875	CDS
			locus_tag	Dasosvirus_1_23
			product	hypothetical protein
			transl_table	11
25945	27210	CDS
			locus_tag	Dasosvirus_1_24
			product	hypothetical protein
			transl_table	11
27272	28693	CDS
			locus_tag	Dasosvirus_1_25
			product	hypothetical protein
			transl_table	11
28788	30371	CDS
			locus_tag	Dasosvirus_1_26
			product	hypothetical protein
			transl_table	11
31091	30438	CDS
			locus_tag	Dasosvirus_1_27
			product	hypothetical protein
			transl_table	11
31219	32508	CDS
			locus_tag	Dasosvirus_1_28
			product	hypothetical protein
			transl_table	11
32745	33797	CDS
			locus_tag	Dasosvirus_1_29
			product	hypothetical protein
			transl_table	11
34708	33833	CDS
			locus_tag	Dasosvirus_1_30
			product	hypothetical protein
			transl_table	11
35622	34798	CDS
			locus_tag	Dasosvirus_1_31
			product	hypothetical protein
			transl_table	11
35729	35848	CDS
			locus_tag	Dasosvirus_1_32
			product	hypothetical protein
			transl_table	11
35835	37229	CDS
			locus_tag	Dasosvirus_1_33
			product	hypothetical protein
			transl_table	11
37298	38710	CDS
			locus_tag	Dasosvirus_1_34
			product	hypothetical protein
			transl_table	11
38792	40201	CDS
			locus_tag	Dasosvirus_1_35
			product	Ankyrin repeat protein
			transl_table	11
40264	41064	CDS
			locus_tag	Dasosvirus_1_36
			product	kinase inhibitor
			transl_table	11
41379	41083	CDS
			locus_tag	Dasosvirus_1_37
			product	hypothetical protein
			transl_table	11
42364	41654	CDS
			locus_tag	Dasosvirus_1_38
			product	hypothetical protein
			transl_table	11
42801	42427	CDS
			locus_tag	Dasosvirus_1_39
			product	hypothetical protein
			transl_table	11
>Dasosvirus_2
1	2199	CDS
			locus_tag	Dasosvirus_2_1
			product	PREDICTED: uncharacterized protein LOC105849702, partial
			transl_table	11
2256	4292	CDS
			locus_tag	Dasosvirus_2_2
			product	hypothetical protein Catovirus_2_262
			transl_table	11
4341	4868	CDS
			locus_tag	Dasosvirus_2_3
			product	hypothetical protein Catovirus_2_265
			transl_table	11
5813	5100	CDS
			locus_tag	Dasosvirus_2_4
			product	hypothetical protein
			transl_table	11
5949	6857	CDS
			locus_tag	Dasosvirus_2_5
			product	hypothetical protein
			transl_table	11
6911	7294	CDS
			locus_tag	Dasosvirus_2_6
			product	ribonuclease HI
			transl_table	11
8367	7522	CDS
			locus_tag	Dasosvirus_2_7
			product	hypothetical protein
			transl_table	11
9363	8389	CDS
			locus_tag	Dasosvirus_2_8
			product	hypothetical protein
			transl_table	11
10025	9417	CDS
			locus_tag	Dasosvirus_2_9
			product	hypothetical protein
			transl_table	11
10675	10073	CDS
			locus_tag	Dasosvirus_2_10
			product	hypothetical protein
			transl_table	11
11397	10729	CDS
			locus_tag	Dasosvirus_2_11
			product	hypothetical protein
			transl_table	11
13322	11505	CDS
			locus_tag	Dasosvirus_2_12
			product	core protein
			transl_table	11
15068	13425	CDS
			locus_tag	Dasosvirus_2_13
			product	replication factor C large subunit
			transl_table	11
16445	15117	CDS
			locus_tag	Dasosvirus_2_14
			product	hypothetical protein BMW23_0529
			transl_table	11
16540	16962	CDS
			locus_tag	Dasosvirus_2_15
			product	hypothetical protein
			transl_table	11
17691	17044	CDS
			locus_tag	Dasosvirus_2_16
			product	hypothetical protein
			transl_table	11
17696	18925	CDS
			locus_tag	Dasosvirus_2_17
			product	hypothetical protein Catovirus_2_272
			transl_table	11
19063	19668	CDS
			locus_tag	Dasosvirus_2_18
			product	hypothetical protein Catovirus_2_273
			transl_table	11
20365	19682	CDS
			locus_tag	Dasosvirus_2_19
			product	recombinase family protein
			transl_table	11
21892	20426	CDS
			locus_tag	Dasosvirus_2_20
			product	hypothetical protein
			transl_table	11
22680	21940	CDS
			locus_tag	Dasosvirus_2_21
			product	hypothetical protein
			transl_table	11
22834	22694	CDS
			locus_tag	Dasosvirus_2_22
			product	hypothetical protein
			transl_table	11
23830	22889	CDS
			locus_tag	Dasosvirus_2_23
			product	hypothetical protein
			transl_table	11
25453	23894	CDS
			locus_tag	Dasosvirus_2_24
			product	hypothetical protein
			transl_table	11
26481	25570	CDS
			locus_tag	Dasosvirus_2_25
			product	packaging ATPase
			transl_table	11
26967	26530	CDS
			locus_tag	Dasosvirus_2_26
			product	hypothetical protein
			transl_table	11
28378	27017	CDS
			locus_tag	Dasosvirus_2_27
			product	hypothetical protein Indivirus_4_32
			transl_table	11
28701	28432	CDS
			locus_tag	Dasosvirus_2_28
			product	hypothetical protein Klosneuvirus_1_23
			transl_table	11
29796	28723	CDS
			locus_tag	Dasosvirus_2_29
			product	hypothetical protein Catovirus_2_278
			transl_table	11
30110	29838	CDS
			locus_tag	Dasosvirus_2_30
			product	hypothetical protein
			transl_table	11
30419	30111	CDS
			locus_tag	Dasosvirus_2_31
			product	PREDICTED: uncharacterized protein LOC101235145
			transl_table	11
30687	30478	CDS
			locus_tag	Dasosvirus_2_32
			product	hypothetical protein
			transl_table	11
32091	30724	CDS
			locus_tag	Dasosvirus_2_33
			product	PREDICTED: uncharacterized protein LOC101235145
			transl_table	11
32288	32467	CDS
			locus_tag	Dasosvirus_2_34
			product	hypothetical protein
			transl_table	11
32662	34518	CDS
			locus_tag	Dasosvirus_2_35
			product	NCLDV major capsid protein
			transl_table	11
34679	38152	CDS
			locus_tag	Dasosvirus_2_36
			product	hypothetical protein Indivirus_4_15
			transl_table	11
38788	38345	CDS
			locus_tag	Dasosvirus_2_37
			product	hypothetical protein
			transl_table	11
>Dasosvirus_3
1068	1	CDS
			locus_tag	Dasosvirus_3_1
			product	predicted protein
			transl_table	11
2345	1221	CDS
			locus_tag	Dasosvirus_3_2
			product	PREDICTED: small RNA degrading nuclease 5-like
			transl_table	11
2425	2685	CDS
			locus_tag	Dasosvirus_3_3
			product	hypothetical protein
			transl_table	11
2872	3414	CDS
			locus_tag	Dasosvirus_3_4
			product	hypothetical protein
			transl_table	11
3463	5148	CDS
			locus_tag	Dasosvirus_3_5
			product	hypothetical protein
			transl_table	11
6290	5103	CDS
			locus_tag	Dasosvirus_3_6
			product	hypothetical protein
			transl_table	11
6463	8391	CDS
			locus_tag	Dasosvirus_3_7
			product	hypothetical protein COB67_05535
			transl_table	11
9257	8616	CDS
			locus_tag	Dasosvirus_3_8
			product	hypothetical protein
			transl_table	11
10050	9457	CDS
			locus_tag	Dasosvirus_3_9
			product	hypothetical protein
			transl_table	11
10225	10812	CDS
			locus_tag	Dasosvirus_3_10
			product	hypothetical protein
			transl_table	11
11271	11807	CDS
			locus_tag	Dasosvirus_3_11
			product	hypothetical protein
			transl_table	11
11998	12537	CDS
			locus_tag	Dasosvirus_3_12
			product	hypothetical protein
			transl_table	11
12908	12555	CDS
			locus_tag	Dasosvirus_3_13
			product	hypothetical protein
			transl_table	11
13044	13574	CDS
			locus_tag	Dasosvirus_3_14
			product	hypothetical protein
			transl_table	11
14931	13678	CDS
			locus_tag	Dasosvirus_3_15
			product	hypothetical protein
			transl_table	11
15005	15871	CDS
			locus_tag	Dasosvirus_3_16
			product	hypothetical protein
			transl_table	11
16336	15926	CDS
			locus_tag	Dasosvirus_3_17
			product	hypothetical protein
			transl_table	11
16488	17309	CDS
			locus_tag	Dasosvirus_3_18
			product	Mitochondrial carrier protein
			transl_table	11
17415	17849	CDS
			locus_tag	Dasosvirus_3_19
			product	hypothetical protein
			transl_table	11
17971	18372	CDS
			locus_tag	Dasosvirus_3_20
			product	PREDICTED: peptidyl-tRNA hydrolase 2, mitochondrial-like
			transl_table	11
18434	18955	CDS
			locus_tag	Dasosvirus_3_21
			product	hypothetical protein
			transl_table	11
19056	20075	CDS
			locus_tag	Dasosvirus_3_22
			product	E3 ubiquitin ligase triad3, putative
			transl_table	11
20131	21339	CDS
			locus_tag	Dasosvirus_3_23
			product	packaging ATPase
			transl_table	11
22068	21361	CDS
			locus_tag	Dasosvirus_3_24
			product	hypothetical protein
			transl_table	11
22203	25334	CDS
			locus_tag	Dasosvirus_3_25
			product	superfamily II helicase
			transl_table	11
28967	28170	CDS
			locus_tag	Dasosvirus_3_26
			product	hypothetical protein
			transl_table	11
28945	29205	CDS
			locus_tag	Dasosvirus_3_27
			product	hypothetical protein
			transl_table	11
30682	29195	CDS
			locus_tag	Dasosvirus_3_28
			product	hypothetical protein AK812_SmicGene10904
			transl_table	11
30753	31319	CDS
			locus_tag	Dasosvirus_3_29
			product	hypothetical protein
			transl_table	11
31340	31495	CDS
			locus_tag	Dasosvirus_3_30
			product	hypothetical protein
			transl_table	11
31835	31485	CDS
			locus_tag	Dasosvirus_3_31
			product	hypothetical protein
			transl_table	11
32646	31900	CDS
			locus_tag	Dasosvirus_3_32
			product	hypothetical protein
			transl_table	11
32800	33690	CDS
			locus_tag	Dasosvirus_3_33
			product	hypothetical protein
			transl_table	11
33862	33683	CDS
			locus_tag	Dasosvirus_3_34
			product	hypothetical protein
			transl_table	11
34063	34929	CDS
			locus_tag	Dasosvirus_3_35
			product	hypothetical protein
			transl_table	11
35062	35307	CDS
			locus_tag	Dasosvirus_3_36
			product	hypothetical protein
			transl_table	11
36086	35379	CDS
			locus_tag	Dasosvirus_3_37
			product	hypothetical protein
			transl_table	11
36281	36156	CDS
			locus_tag	Dasosvirus_3_38
			product	hypothetical protein
			transl_table	11
>Dasosvirus_4
3	797	CDS
			locus_tag	Dasosvirus_4_1
			product	hypothetical protein
			transl_table	11
832	2064	CDS
			locus_tag	Dasosvirus_4_2
			product	hypothetical protein
			transl_table	11
2890	2072	CDS
			locus_tag	Dasosvirus_4_3
			product	hypothetical protein
			transl_table	11
3730	2996	CDS
			locus_tag	Dasosvirus_4_4
			product	hypothetical protein
			transl_table	11
5333	3792	CDS
			locus_tag	Dasosvirus_4_5
			product	hypothetical protein
			transl_table	11
7122	5467	CDS
			locus_tag	Dasosvirus_4_6
			product	hypothetical protein
			transl_table	11
8065	7307	CDS
			locus_tag	Dasosvirus_4_7
			product	hypothetical protein
			transl_table	11
9058	8135	CDS
			locus_tag	Dasosvirus_4_8
			product	hypothetical protein
			transl_table	11
9425	9312	CDS
			locus_tag	Dasosvirus_4_9
			product	hypothetical protein
			transl_table	11
9663	9544	CDS
			locus_tag	Dasosvirus_4_10
			product	hypothetical protein
			transl_table	11
10595	9735	CDS
			locus_tag	Dasosvirus_4_11
			product	hypothetical protein
			transl_table	11
11890	10652	CDS
			locus_tag	Dasosvirus_4_12
			product	hypothetical protein
			transl_table	11
11898	12053	CDS
			locus_tag	Dasosvirus_4_13
			product	hypothetical protein
			transl_table	11
13149	12373	CDS
			locus_tag	Dasosvirus_4_14
			product	hypothetical protein
			transl_table	11
13643	13287	CDS
			locus_tag	Dasosvirus_4_15
			product	hypothetical protein
			transl_table	11
14972	13755	CDS
			locus_tag	Dasosvirus_4_16
			product	hypothetical protein
			transl_table	11
16114	15338	CDS
			locus_tag	Dasosvirus_4_17
			product	hypothetical protein
			transl_table	11
16261	17175	CDS
			locus_tag	Dasosvirus_4_18
			product	hypothetical protein
			transl_table	11
17841	17239	CDS
			locus_tag	Dasosvirus_4_19
			product	BRO-N domain protein
			transl_table	11
18250	17843	CDS
			locus_tag	Dasosvirus_4_20
			product	BRO-N domain protein
			transl_table	11
18716	18426	CDS
			locus_tag	Dasosvirus_4_21
			product	hypothetical protein
			transl_table	11
18861	18745	CDS
			locus_tag	Dasosvirus_4_22
			product	hypothetical protein
			transl_table	11
19271	18918	CDS
			locus_tag	Dasosvirus_4_23
			product	hypothetical protein
			transl_table	11
20557	19322	CDS
			locus_tag	Dasosvirus_4_24
			product	hypothetical protein
			transl_table	11
20788	20663	CDS
			locus_tag	Dasosvirus_4_25
			product	hypothetical protein
			transl_table	11
20968	20855	CDS
			locus_tag	Dasosvirus_4_26
			product	hypothetical protein
			transl_table	11
21122	21682	CDS
			locus_tag	Dasosvirus_4_27
			product	hypothetical protein
			transl_table	11
22394	21747	CDS
			locus_tag	Dasosvirus_4_28
			product	hypothetical protein
			transl_table	11
22476	23522	CDS
			locus_tag	Dasosvirus_4_29
			product	hypothetical protein
			transl_table	11
23638	23937	CDS
			locus_tag	Dasosvirus_4_30
			product	hypothetical protein
			transl_table	11
25226	23976	CDS
			locus_tag	Dasosvirus_4_31
			product	hypothetical protein
			transl_table	11
25377	25784	CDS
			locus_tag	Dasosvirus_4_32
			product	hypothetical protein
			transl_table	11
25828	25974	CDS
			locus_tag	Dasosvirus_4_33
			product	hypothetical protein
			transl_table	11
25990	26553	CDS
			locus_tag	Dasosvirus_4_34
			product	hypothetical protein
			transl_table	11
26620	27084	CDS
			locus_tag	Dasosvirus_4_35
			product	HNH endonuclease
			transl_table	11
27572	27093	CDS
			locus_tag	Dasosvirus_4_36
			product	hypothetical protein
			transl_table	11
28273	27650	CDS
			locus_tag	Dasosvirus_4_37
			product	hypothetical protein
			transl_table	11
29718	28387	CDS
			locus_tag	Dasosvirus_4_38
			product	hypothetical protein
			transl_table	11
30281	29802	CDS
			locus_tag	Dasosvirus_4_39
			product	hypothetical protein
			transl_table	11
32864	30477	CDS
			locus_tag	Dasosvirus_4_40
			product	Type I restriction-modification system, M subunit, partial
			transl_table	11
35196	32935	CDS
			locus_tag	Dasosvirus_4_41
			product	type III restriction enzyme, res subunit
			transl_table	11
35366	35560	CDS
			locus_tag	Dasosvirus_4_42
			product	hypothetical protein
			transl_table	11
>Dasosvirus_5
2	679	CDS
			locus_tag	Dasosvirus_5_1
			product	ribonuclease III
			transl_table	11
594	1613	CDS
			locus_tag	Dasosvirus_5_2
			product	hypothetical protein
			transl_table	11
1683	2441	CDS
			locus_tag	Dasosvirus_5_3
			product	hypothetical protein
			transl_table	11
2997	2431	CDS
			locus_tag	Dasosvirus_5_4
			product	hypothetical protein
			transl_table	11
3378	5300	CDS
			locus_tag	Dasosvirus_5_5
			product	hypothetical protein
			transl_table	11
6463	5306	CDS
			locus_tag	Dasosvirus_5_6
			product	hypothetical protein CO114_03860
			transl_table	11
6575	9334	CDS
			locus_tag	Dasosvirus_5_7
			product	hypothetical protein Catovirus_2_214
			transl_table	11
9403	11802	CDS
			locus_tag	Dasosvirus_5_8
			product	DEAD/SNF2-like helicase
			transl_table	11
12823	11783	CDS
			locus_tag	Dasosvirus_5_9
			product	hypothetical protein Klosneuvirus_2_225
			transl_table	11
12932	15706	CDS
			locus_tag	Dasosvirus_5_10
			product	hypothetical protein BMW23_0676
			transl_table	11
15870	15998	CDS
			locus_tag	Dasosvirus_5_11
			product	hypothetical protein
			transl_table	11
16482	16093	CDS
			locus_tag	Dasosvirus_5_12
			product	hypothetical protein
			transl_table	11
16610	18979	CDS
			locus_tag	Dasosvirus_5_13
			product	YqaJ-like viral recombinase
			transl_table	11
19253	18942	CDS
			locus_tag	Dasosvirus_5_14
			product	hypothetical protein
			transl_table	11
20194	19274	CDS
			locus_tag	Dasosvirus_5_15
			product	hypothetical protein
			transl_table	11
21243	20380	CDS
			locus_tag	Dasosvirus_5_16
			product	hypothetical protein
			transl_table	11
21453	21328	CDS
			locus_tag	Dasosvirus_5_17
			product	hypothetical protein
			transl_table	11
>Dasosvirus_6
1708	2772	CDS
			locus_tag	Dasosvirus_6_1
			product	hypothetical protein PTSG_11909
			transl_table	11
3224	2793	CDS
			locus_tag	Dasosvirus_6_2
			product	hypothetical protein
			transl_table	11
3448	3296	CDS
			locus_tag	Dasosvirus_6_3
			product	hypothetical protein
			transl_table	11
3627	3445	CDS
			locus_tag	Dasosvirus_6_4
			product	hypothetical protein
			transl_table	11
3779	4672	CDS
			locus_tag	Dasosvirus_6_5
			product	hypothetical protein
			transl_table	11
4771	5703	CDS
			locus_tag	Dasosvirus_6_6
			product	hypothetical protein
			transl_table	11
6296	5706	CDS
			locus_tag	Dasosvirus_6_7
			product	hypothetical protein mc_207
			transl_table	11
6379	7356	CDS
			locus_tag	Dasosvirus_6_8
			product	hypothetical protein
			transl_table	11
7499	8386	CDS
			locus_tag	Dasosvirus_6_9
			product	hypothetical protein
			transl_table	11
8616	8798	CDS
			locus_tag	Dasosvirus_6_10
			product	hypothetical protein
			transl_table	11
9299	8970	CDS
			locus_tag	Dasosvirus_6_11
			product	hypothetical protein
			transl_table	11
9342	9512	CDS
			locus_tag	Dasosvirus_6_12
			product	hypothetical protein
			transl_table	11
9513	9737	CDS
			locus_tag	Dasosvirus_6_13
			product	hypothetical protein
			transl_table	11
11929	9839	CDS
			locus_tag	Dasosvirus_6_14
			product	putative tRNA(Ile)lysidine synthase
			transl_table	11
14702	12042	CDS
			locus_tag	Dasosvirus_6_15
			product	putative ATP-binding protein
			transl_table	11
>Dasosvirus_7
2	715	CDS
			locus_tag	Dasosvirus_7_1
			product	hypothetical protein
			transl_table	11
760	1680	CDS
			locus_tag	Dasosvirus_7_2
			product	hypothetical protein
			transl_table	11
1800	2834	CDS
			locus_tag	Dasosvirus_7_3
			product	hypothetical protein
			transl_table	11
4672	2840	CDS
			locus_tag	Dasosvirus_7_4
			product	superfamily II DNA or RNA helicase
			transl_table	11
4786	5544	CDS
			locus_tag	Dasosvirus_7_5
			product	hypothetical protein
			transl_table	11
5707	6258	CDS
			locus_tag	Dasosvirus_7_6
			product	hypothetical protein
			transl_table	11
6541	7710	CDS
			locus_tag	Dasosvirus_7_7
			product	metallophosphatase/phosphoesterase
			transl_table	11
8804	7992	CDS
			locus_tag	Dasosvirus_7_8
			product	XRN 5'-3' exonuclease
			transl_table	11
9815	8832	CDS
			locus_tag	Dasosvirus_7_9
			product	XRN 5'-3' exonuclease
			transl_table	11
9924	10718	CDS
			locus_tag	Dasosvirus_7_10
			product	hypothetical protein
			transl_table	11
11537	10920	CDS
			locus_tag	Dasosvirus_7_11
			product	hypothetical protein
			transl_table	11
11536	13308	CDS
			locus_tag	Dasosvirus_7_12
			product	serine/threonine protein kinase
			transl_table	11
13636	13824	CDS
			locus_tag	Dasosvirus_7_13
			product	hypothetical protein
			transl_table	11
>Dasosvirus_8
2957	3	CDS
			locus_tag	Dasosvirus_8_1
			product	hypothetical protein
			transl_table	11
3051	5543	CDS
			locus_tag	Dasosvirus_8_2
			product	MULTISPECIES: patatin
			transl_table	11
6386	5544	CDS
			locus_tag	Dasosvirus_8_3
			product	putative alpha/beta hydrolase
			transl_table	11
6368	6649	CDS
			locus_tag	Dasosvirus_8_4
			product	hypothetical protein
			transl_table	11
6716	8671	CDS
			locus_tag	Dasosvirus_8_5
			product	DNA topoisomerase IB
			transl_table	11
9228	8668	CDS
			locus_tag	Dasosvirus_8_6
			product	hypothetical protein
			transl_table	11
9328	10617	CDS
			locus_tag	Dasosvirus_8_7
			product	PREDICTED: uncharacterized protein LOC105844216
			transl_table	11
10741	12732	CDS
			locus_tag	Dasosvirus_8_8
			product	hypothetical protein
			transl_table	11
12986	12729	CDS
			locus_tag	Dasosvirus_8_9
			product	hypothetical protein
			transl_table	11
13015	13584	CDS
			locus_tag	Dasosvirus_8_10
			product	hypothetical protein
			transl_table	11
>Dasosvirus_9
996	1	CDS
			locus_tag	Dasosvirus_9_1
			product	hypothetical protein G210_1362
			transl_table	11
1143	1319	CDS
			locus_tag	Dasosvirus_9_2
			product	hypothetical protein
			transl_table	11
1614	1303	CDS
			locus_tag	Dasosvirus_9_3
			product	hypothetical protein CO137_03825
			transl_table	11
1755	1904	CDS
			locus_tag	Dasosvirus_9_4
			product	hypothetical protein
			transl_table	11
1947	2303	CDS
			locus_tag	Dasosvirus_9_5
			product	hypothetical protein
			transl_table	11
2382	3935	CDS
			locus_tag	Dasosvirus_9_6
			product	hypothetical protein
			transl_table	11
3990	5102	CDS
			locus_tag	Dasosvirus_9_7
			product	hypothetical protein
			transl_table	11
6597	5104	CDS
			locus_tag	Dasosvirus_9_8
			product	hypothetical protein
			transl_table	11
6818	8401	CDS
			locus_tag	Dasosvirus_9_9
			product	hypothetical protein
			transl_table	11
9982	8498	CDS
			locus_tag	Dasosvirus_9_10
			product	hypothetical protein
			transl_table	11
10317	10865	CDS
			locus_tag	Dasosvirus_9_11
			product	hypothetical protein
			transl_table	11
10896	11891	CDS
			locus_tag	Dasosvirus_9_12
			product	serine/threonine protein kinase
			transl_table	11
12361	11879	CDS
			locus_tag	Dasosvirus_9_13
			product	hypothetical protein Catovirus_1_839
			transl_table	11
>Dasosvirus_10
3	1148	CDS
			locus_tag	Dasosvirus_10_1
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
1784	1149	CDS
			locus_tag	Dasosvirus_10_2
			product	putative RNA methylase
			transl_table	11
2402	1827	CDS
			locus_tag	Dasosvirus_10_3
			product	hypothetical protein
			transl_table	11
3584	2427	CDS
			locus_tag	Dasosvirus_10_4
			product	putative ORFan
			transl_table	11
3653	7468	CDS
			locus_tag	Dasosvirus_10_5
			product	DNA polymerase family B elongation subunit
			transl_table	11
9184	7469	CDS
			locus_tag	Dasosvirus_10_6
			product	hypothetical protein
			transl_table	11
9288	9896	CDS
			locus_tag	Dasosvirus_10_7
			product	protein of unknown function DUF45
			transl_table	11
10775	10479	CDS
			locus_tag	Dasosvirus_10_8
			product	hypothetical protein
			transl_table	11
11919	12167	CDS
			locus_tag	Dasosvirus_10_9
			product	hypothetical protein
			transl_table	11
12581	12673	CDS
			locus_tag	Dasosvirus_10_10
			product	hypothetical protein
			transl_table	11
>Dasosvirus_11
2	256	CDS
			locus_tag	Dasosvirus_11_1
			product	PREDICTED: uncharacterized threonine-rich GPI-anchored glycoprotein PJ4664.02-like
			transl_table	11
271	2112	CDS
			locus_tag	Dasosvirus_11_2
			product	PREDICTED: uncharacterized threonine-rich GPI-anchored glycoprotein PJ4664.02-like
			transl_table	11
2198	2566	CDS
			locus_tag	Dasosvirus_11_3
			product	hypothetical protein
			transl_table	11
4063	2570	CDS
			locus_tag	Dasosvirus_11_4
			product	hypothetical protein
			transl_table	11
4682	4119	CDS
			locus_tag	Dasosvirus_11_5
			product	hypothetical protein
			transl_table	11
5623	4904	CDS
			locus_tag	Dasosvirus_11_6
			product	hypothetical protein
			transl_table	11
5703	6272	CDS
			locus_tag	Dasosvirus_11_7
			product	hypothetical protein CBD58_03680
			transl_table	11
6478	6999	CDS
			locus_tag	Dasosvirus_11_8
			product	hypothetical protein
			transl_table	11
7461	7652	CDS
			locus_tag	Dasosvirus_11_9
			product	hypothetical protein
			transl_table	11
>Dasosvirus_12
3	356	CDS
			locus_tag	Dasosvirus_12_1
			product	hypothetical protein
			transl_table	11
1093	359	CDS
			locus_tag	Dasosvirus_12_2
			product	hypothetical protein
			transl_table	11
1336	4047	CDS
			locus_tag	Dasosvirus_12_3
			product	stage V sporulation protein K
			transl_table	11
5241	4243	CDS
			locus_tag	Dasosvirus_12_4
			product	hypothetical protein
			transl_table	11
5383	6297	CDS
			locus_tag	Dasosvirus_12_5
			product	hypothetical protein
			transl_table	11
7310	6309	CDS
			locus_tag	Dasosvirus_12_6
			product	hypothetical protein
			transl_table	11
>Dasosvirus_13
892	44	CDS
			locus_tag	Dasosvirus_13_1
			product	PREDICTED: DNA-(apurinic or apyrimidinic site) lyase-like
			transl_table	11
958	1992	CDS
			locus_tag	Dasosvirus_13_2
			product	replication factor C small subunit
			transl_table	11
2874	1996	CDS
			locus_tag	Dasosvirus_13_3
			product	hypothetical protein CBD11_00930
			transl_table	11
3617	2922	CDS
			locus_tag	Dasosvirus_13_4
			product	MULTISPECIES: glycosyltransferase
			transl_table	11
4296	3637	CDS
			locus_tag	Dasosvirus_13_5
			product	hypothetical protein AURANDRAFT_36689
			transl_table	11
4552	4415	CDS
			locus_tag	Dasosvirus_13_6
			product	hypothetical protein
			transl_table	11
4657	4568	CDS
			locus_tag	Dasosvirus_13_7
			product	hypothetical protein
			transl_table	11
5466	4663	CDS
			locus_tag	Dasosvirus_13_8
			product	E3 ubiquitin-protein ligase RLIM
			transl_table	11
>Dasosvirus_14
1026	646	CDS
			locus_tag	Dasosvirus_14_1
			product	hypothetical protein
			transl_table	11
1335	2732	CDS
			locus_tag	Dasosvirus_14_2
			product	hypothetical protein UW31_C0007G0013
			transl_table	11
3579	3043	CDS
			locus_tag	Dasosvirus_14_3
			product	hypothetical protein
			transl_table	11
4110	3727	CDS
			locus_tag	Dasosvirus_14_4
			product	hypothetical protein
			transl_table	11
4839	4162	CDS
			locus_tag	Dasosvirus_14_5
			product	hypothetical protein
			transl_table	11
>Dasosvirus_15
3	209	CDS
			locus_tag	Dasosvirus_15_1
			product	hypothetical protein
			transl_table	11
362	1195	CDS
			locus_tag	Dasosvirus_15_2
			product	D5 family helicase-primase
			transl_table	11
2486	1278	CDS
			locus_tag	Dasosvirus_15_3
			product	hypothetical protein
			transl_table	11
3577	2501	CDS
			locus_tag	Dasosvirus_15_4
			product	hypothetical protein
			transl_table	11
3943	3632	CDS
			locus_tag	Dasosvirus_15_5
			product	hypothetical protein Catovirus_1_66
			transl_table	11
4211	4038	CDS
			locus_tag	Dasosvirus_15_6
			product	hypothetical protein
			transl_table	11
>Dasosvirus_16
394	83	CDS
			locus_tag	Dasosvirus_16_1
			product	hypothetical protein
			transl_table	11
484	1062	CDS
			locus_tag	Dasosvirus_16_2
			product	hypothetical protein Indivirus_3_26
			transl_table	11
1900	1070	CDS
			locus_tag	Dasosvirus_16_3
			product	hypothetical protein BMW23_0689
			transl_table	11
1981	2169	CDS
			locus_tag	Dasosvirus_16_4
			product	hypothetical protein
			transl_table	11
2862	2188	CDS
			locus_tag	Dasosvirus_16_5
			product	transcription elongation factor TFIIS
			transl_table	11
3349	3209	CDS
			locus_tag	Dasosvirus_16_6
			product	hypothetical protein
			transl_table	11
4023	4112	CDS
			locus_tag	Dasosvirus_16_7
			product	hypothetical protein
			transl_table	11
>Dasosvirus_17
3	122	CDS
			locus_tag	Dasosvirus_17_1
			product	hypothetical protein
			transl_table	11
185	1723	CDS
			locus_tag	Dasosvirus_17_2
			product	hypothetical protein
			transl_table	11
1803	2363	CDS
			locus_tag	Dasosvirus_17_3
			product	hypothetical protein
			transl_table	11
3515	2496	CDS
			locus_tag	Dasosvirus_17_4
			product	hypothetical protein
			transl_table	11
3924	3496	CDS
			locus_tag	Dasosvirus_17_5
			product	acetylornithine deacetylase
			transl_table	11
>Dasosvirus_18
2585	2340	CDS
			locus_tag	Dasosvirus_18_1
			product	hypothetical protein
			transl_table	11
3628	3849	CDS
			locus_tag	Dasosvirus_18_2
			product	hypothetical protein
			transl_table	11
>Dasosvirus_19
913	2	CDS
			locus_tag	Dasosvirus_19_1
			product	glycylpeptide N-tetradecanoyltransferase 1
			transl_table	11
2713	1346	CDS
			locus_tag	Dasosvirus_19_2
			product	IS4 family transposase
			transl_table	11
2905	3174	CDS
			locus_tag	Dasosvirus_19_3
			product	hypothetical protein
			transl_table	11
>Dasosvirus_20
1	633	CDS
			locus_tag	Dasosvirus_20_1
			product	hypothetical protein
			transl_table	11
712	1371	CDS
			locus_tag	Dasosvirus_20_2
			product	hypothetical protein
			transl_table	11
1513	2490	CDS
			locus_tag	Dasosvirus_20_3
			product	hypothetical protein
			transl_table	11
2590	3054	CDS
			locus_tag	Dasosvirus_20_4
			product	hypothetical protein
			transl_table	11
>Dasosvirus_21
2	508	CDS
			locus_tag	Dasosvirus_21_1
			product	hypothetical protein
			transl_table	11
463	2556	CDS
			locus_tag	Dasosvirus_21_2
			product	hypothetical protein Indivirus_2_58
			transl_table	11
3022	2498	CDS
			locus_tag	Dasosvirus_21_3
			product	hypothetical protein
			transl_table	11
>Dasosvirus_22
1312	353	CDS
			locus_tag	Dasosvirus_22_1
			product	putative sensor histidine kinase
			transl_table	11
1478	1924	CDS
			locus_tag	Dasosvirus_22_2
			product	hypothetical protein
			transl_table	11
2560	1952	CDS
			locus_tag	Dasosvirus_22_3
			product	putative sensor histidine kinase
			transl_table	11
2765	2604	CDS
			locus_tag	Dasosvirus_22_4
			product	hypothetical protein
			transl_table	11
>Dasosvirus_23
3	899	CDS
			locus_tag	Dasosvirus_23_1
			product	hypothetical protein
			transl_table	11
937	1734	CDS
			locus_tag	Dasosvirus_23_2
			product	pentapeptide repeat-containing protein
			transl_table	11
1997	2095	CDS
			locus_tag	Dasosvirus_23_3
			product	hypothetical protein
			transl_table	11
2146	2322	CDS
			locus_tag	Dasosvirus_23_4
			product	hypothetical protein
			transl_table	11
2319	2699	CDS
			locus_tag	Dasosvirus_23_5
			product	hypothetical protein
			transl_table	11
>Dasosvirus_24
2	1195	CDS
			locus_tag	Dasosvirus_24_1
			product	hypothetical protein Hokovirus_2_167
			transl_table	11
2057	1188	CDS
			locus_tag	Dasosvirus_24_2
			product	glycosyltransferase
			transl_table	11
2664	2095	CDS
			locus_tag	Dasosvirus_24_3
			product	glycosyltransferase
			transl_table	11
