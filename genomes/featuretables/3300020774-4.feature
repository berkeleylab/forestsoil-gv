>3300020774-4_1
2	1453	CDS
			locus_tag	3300020774-4_1_1
			product	hypothetical protein
			transl_table	11
1608	3776	CDS
			locus_tag	3300020774-4_1_2
			product	putative 5'-3' exonuclease 20
			transl_table	11
3863	6232	CDS
			locus_tag	3300020774-4_1_3
			product	putative 5'-3'exonuclease20
			transl_table	11
6910	6269	CDS
			locus_tag	3300020774-4_1_4
			product	hypothetical protein
			transl_table	11
7012	7488	CDS
			locus_tag	3300020774-4_1_5
			product	hypothetical protein
			transl_table	11
7746	7510	CDS
			locus_tag	3300020774-4_1_6
			product	hypothetical protein
			transl_table	11
7864	9360	CDS
			locus_tag	3300020774-4_1_7
			product	putative DNA helicase
			transl_table	11
9419	10225	CDS
			locus_tag	3300020774-4_1_8
			product	putative orfan
			transl_table	11
11784	10267	CDS
			locus_tag	3300020774-4_1_9
			product	hypothetical protein
			transl_table	11
12785	11835	CDS
			locus_tag	3300020774-4_1_10
			product	hydrolase
			transl_table	11
12927	14651	CDS
			locus_tag	3300020774-4_1_11
			product	hypothetical protein
			transl_table	11
14786	14697	CDS
			locus_tag	3300020774-4_1_12
			product	hypothetical protein
			transl_table	11
15112	15204	CDS
			locus_tag	3300020774-4_1_13
			product	hypothetical protein
			transl_table	11
15392	15517	CDS
			locus_tag	3300020774-4_1_14
			product	hypothetical protein
			transl_table	11
15815	15666	CDS
			locus_tag	3300020774-4_1_15
			product	hypothetical protein
			transl_table	11
16153	15890	CDS
			locus_tag	3300020774-4_1_16
			product	hypothetical protein
			transl_table	11
17390	16191	CDS
			locus_tag	3300020774-4_1_17
			product	hypothetical protein
			transl_table	11
17573	18910	CDS
			locus_tag	3300020774-4_1_18
			product	putative ORFan
			transl_table	11
19052	19546	CDS
			locus_tag	3300020774-4_1_19
			product	hypothetical protein
			transl_table	11
19561	19665	CDS
			locus_tag	3300020774-4_1_20
			product	hypothetical protein
			transl_table	11
19732	19920	CDS
			locus_tag	3300020774-4_1_21
			product	hypothetical protein
			transl_table	11
21573	20239	CDS
			locus_tag	3300020774-4_1_22
			product	putative ATP-dependent RNA helicase
			transl_table	11
22427	21576	CDS
			locus_tag	3300020774-4_1_23
			product	hypothetical protein
			transl_table	11
23366	22479	CDS
			locus_tag	3300020774-4_1_24
			product	hypothetical protein
			transl_table	11
23745	23425	CDS
			locus_tag	3300020774-4_1_25
			product	hypothetical protein
			transl_table	11
25834	23807	CDS
			locus_tag	3300020774-4_1_26
			product	HrpA-like helicase
			transl_table	11
26096	25875	CDS
			locus_tag	3300020774-4_1_27
			product	hypothetical protein
			transl_table	11
26243	26377	CDS
			locus_tag	3300020774-4_1_28
			product	hypothetical protein
			transl_table	11
26451	27893	CDS
			locus_tag	3300020774-4_1_29
			product	BTB POZ domain and WD-repeat
			transl_table	11
27940	29415	CDS
			locus_tag	3300020774-4_1_30
			product	putative BTB/POZ domain and WD-repeat protein
			transl_table	11
29489	29728	CDS
			locus_tag	3300020774-4_1_31
			product	hypothetical protein
			transl_table	11
29814	29963	CDS
			locus_tag	3300020774-4_1_32
			product	hypothetical protein
			transl_table	11
30091	31071	CDS
			locus_tag	3300020774-4_1_33
			product	Hypothetical protein PINS_012194
			transl_table	11
31185	31748	CDS
			locus_tag	3300020774-4_1_34
			product	putative ORFan
			transl_table	11
32743	31760	CDS
			locus_tag	3300020774-4_1_35
			product	mg661 protein
			transl_table	11
33870	32776	CDS
			locus_tag	3300020774-4_1_36
			product	hypothetical protein
			transl_table	11
34063	34818	CDS
			locus_tag	3300020774-4_1_37
			product	hypothetical protein
			transl_table	11
34915	35373	CDS
			locus_tag	3300020774-4_1_38
			product	hypothetical protein
			transl_table	11
36630	35395	CDS
			locus_tag	3300020774-4_1_39
			product	putative ORFan
			transl_table	11
36781	36689	CDS
			locus_tag	3300020774-4_1_40
			product	hypothetical protein
			transl_table	11
>3300020774-4_2
2	253	CDS
			locus_tag	3300020774-4_2_1
			product	hypothetical protein
			transl_table	11
520	1668	CDS
			locus_tag	3300020774-4_2_2
			product	hypothetical protein C1H57_20220, partial
			transl_table	11
1777	2094	CDS
			locus_tag	3300020774-4_2_3
			product	hypothetical protein
			transl_table	11
2306	2160	CDS
			locus_tag	3300020774-4_2_4
			product	hypothetical protein
			transl_table	11
3028	2438	CDS
			locus_tag	3300020774-4_2_5
			product	hypothetical protein
			transl_table	11
3537	3196	CDS
			locus_tag	3300020774-4_2_6
			product	hypothetical protein
			transl_table	11
3746	4183	CDS
			locus_tag	3300020774-4_2_7
			product	nucleoside 2-deoxyribosyltransferase
			transl_table	11
4262	5617	CDS
			locus_tag	3300020774-4_2_8
			product	histidyl-tRNA synthetase
			transl_table	11
6477	5878	CDS
			locus_tag	3300020774-4_2_9
			product	hypothetical protein
			transl_table	11
7051	6872	CDS
			locus_tag	3300020774-4_2_10
			product	hypothetical protein
			transl_table	11
7265	7110	CDS
			locus_tag	3300020774-4_2_11
			product	hypothetical protein
			transl_table	11
7397	7272	CDS
			locus_tag	3300020774-4_2_12
			product	hypothetical protein
			transl_table	11
7556	9085	CDS
			locus_tag	3300020774-4_2_13
			product	aromatic amino acid lyase
			transl_table	11
9487	9155	CDS
			locus_tag	3300020774-4_2_14
			product	hypothetical protein
			transl_table	11
10019	9792	CDS
			locus_tag	3300020774-4_2_15
			product	hypothetical protein
			transl_table	11
10065	10220	CDS
			locus_tag	3300020774-4_2_16
			product	hypothetical protein
			transl_table	11
10787	10440	CDS
			locus_tag	3300020774-4_2_17
			product	hypothetical protein
			transl_table	11
11339	10989	CDS
			locus_tag	3300020774-4_2_18
			product	hypothetical protein
			transl_table	11
11942	11568	CDS
			locus_tag	3300020774-4_2_19
			product	hypothetical protein
			transl_table	11
12163	11942	CDS
			locus_tag	3300020774-4_2_20
			product	hypothetical protein
			transl_table	11
12274	12375	CDS
			locus_tag	3300020774-4_2_21
			product	hypothetical protein
			transl_table	11
12472	14070	CDS
			locus_tag	3300020774-4_2_22
			product	cytosol nonspecific dipeptidase
			transl_table	11
14250	14453	CDS
			locus_tag	3300020774-4_2_23
			product	hypothetical protein
			transl_table	11
14653	16176	CDS
			locus_tag	3300020774-4_2_24
			product	radical SAM protein
			transl_table	11
16265	18418	CDS
			locus_tag	3300020774-4_2_25
			product	hypothetical protein
			transl_table	11
18511	19023	CDS
			locus_tag	3300020774-4_2_26
			product	hypothetical protein
			transl_table	11
19480	19061	CDS
			locus_tag	3300020774-4_2_27
			product	hypothetical protein
			transl_table	11
20758	19505	CDS
			locus_tag	3300020774-4_2_28
			product	putative orfan
			transl_table	11
20837	21397	CDS
			locus_tag	3300020774-4_2_29
			product	hypothetical protein
			transl_table	11
24796	21815	CDS
			locus_tag	3300020774-4_2_30
			product	DNA primase
			transl_table	11
25217	25309	CDS
			locus_tag	3300020774-4_2_31
			product	hypothetical protein
			transl_table	11
25306	25719	CDS
			locus_tag	3300020774-4_2_32
			product	hypothetical protein
			transl_table	11
25827	26312	CDS
			locus_tag	3300020774-4_2_33
			product	hypothetical protein
			transl_table	11
26403	27236	CDS
			locus_tag	3300020774-4_2_34
			product	hypothetical protein
			transl_table	11
27856	27755	CDS
			locus_tag	3300020774-4_2_35
			product	hypothetical protein
			transl_table	11
31223	27864	CDS
			locus_tag	3300020774-4_2_36
			product	DNA gyrase/topoisomerase IV
			transl_table	11
31325	33301	CDS
			locus_tag	3300020774-4_2_37
			product	hypothetical protein MIMI_L786b
			transl_table	11
33638	33402	CDS
			locus_tag	3300020774-4_2_38
			product	hypothetical protein
			transl_table	11
34885	35235	CDS
			locus_tag	3300020774-4_2_39
			product	hypothetical protein
			transl_table	11
>3300020774-4_3
2	859	CDS
			locus_tag	3300020774-4_3_1
			product	hypothetical protein
			transl_table	11
1484	999	CDS
			locus_tag	3300020774-4_3_2
			product	hypothetical protein
			transl_table	11
1459	1614	CDS
			locus_tag	3300020774-4_3_3
			product	hypothetical protein
			transl_table	11
1780	2268	CDS
			locus_tag	3300020774-4_3_4
			product	hypothetical protein
			transl_table	11
2395	2505	CDS
			locus_tag	3300020774-4_3_5
			product	hypothetical protein
			transl_table	11
2636	3346	CDS
			locus_tag	3300020774-4_3_6
			product	hypothetical protein Hokovirus_3_254
			transl_table	11
3670	3581	CDS
			locus_tag	3300020774-4_3_7
			product	hypothetical protein
			transl_table	11
3727	4278	CDS
			locus_tag	3300020774-4_3_8
			product	DUF1186 domain-containing protein
			transl_table	11
4399	4956	CDS
			locus_tag	3300020774-4_3_9
			product	hypothetical protein
			transl_table	11
6709	5351	CDS
			locus_tag	3300020774-4_3_10
			product	glucose dehydrogenase
			transl_table	11
6993	7172	CDS
			locus_tag	3300020774-4_3_11
			product	hypothetical protein
			transl_table	11
7609	7256	CDS
			locus_tag	3300020774-4_3_12
			product	hypothetical protein
			transl_table	11
9002	7845	CDS
			locus_tag	3300020774-4_3_13
			product	hypothetical protein
			transl_table	11
9264	9109	CDS
			locus_tag	3300020774-4_3_14
			product	hypothetical protein
			transl_table	11
11085	10225	CDS
			locus_tag	3300020774-4_3_15
			product	hypothetical protein
			transl_table	11
12428	11340	CDS
			locus_tag	3300020774-4_3_16
			product	hypothetical protein BOTBODRAFT_29942
			transl_table	11
13638	12661	CDS
			locus_tag	3300020774-4_3_17
			product	hypothetical protein CE11_01208
			transl_table	11
14590	15303	CDS
			locus_tag	3300020774-4_3_18
			product	hypothetical protein
			transl_table	11
15516	15370	CDS
			locus_tag	3300020774-4_3_19
			product	hypothetical protein
			transl_table	11
16065	15829	CDS
			locus_tag	3300020774-4_3_20
			product	hypothetical protein
			transl_table	11
16490	16380	CDS
			locus_tag	3300020774-4_3_21
			product	hypothetical protein
			transl_table	11
17323	16700	CDS
			locus_tag	3300020774-4_3_22
			product	hypothetical protein
			transl_table	11
17816	17295	CDS
			locus_tag	3300020774-4_3_23
			product	hypothetical protein
			transl_table	11
18758	17925	CDS
			locus_tag	3300020774-4_3_24
			product	hypothetical protein
			transl_table	11
18984	19778	CDS
			locus_tag	3300020774-4_3_25
			product	hypothetical protein MegaChil _gp0011
			transl_table	11
21184	20303	CDS
			locus_tag	3300020774-4_3_26
			product	hypothetical protein
			transl_table	11
21544	21822	CDS
			locus_tag	3300020774-4_3_27
			product	hypothetical protein
			transl_table	11
22181	21858	CDS
			locus_tag	3300020774-4_3_28
			product	hypothetical protein
			transl_table	11
24688	22349	CDS
			locus_tag	3300020774-4_3_29
			product	glycosyltransferase
			transl_table	11
24890	26638	CDS
			locus_tag	3300020774-4_3_30
			product	hypothetical protein A3D18_05010
			transl_table	11
26689	28278	CDS
			locus_tag	3300020774-4_3_31
			product	hypothetical protein A2Z85_03945
			transl_table	11
30190	28394	CDS
			locus_tag	3300020774-4_3_32
			product	hypothetical protein
			transl_table	11
>3300020774-4_4
2	517	CDS
			locus_tag	3300020774-4_4_1
			product	hypothetical protein
			transl_table	11
1214	633	CDS
			locus_tag	3300020774-4_4_2
			product	synaptobrevin YKT6
			transl_table	11
1270	3822	CDS
			locus_tag	3300020774-4_4_3
			product	putative ORFan
			transl_table	11
4742	3909	CDS
			locus_tag	3300020774-4_4_4
			product	putative ORFan
			transl_table	11
4873	6021	CDS
			locus_tag	3300020774-4_4_5
			product	hypothetical protein
			transl_table	11
6339	6022	CDS
			locus_tag	3300020774-4_4_6
			product	putative ORFan
			transl_table	11
6631	6383	CDS
			locus_tag	3300020774-4_4_7
			product	hypothetical protein
			transl_table	11
6884	6705	CDS
			locus_tag	3300020774-4_4_8
			product	putative ORFan
			transl_table	11
6980	8167	CDS
			locus_tag	3300020774-4_4_9
			product	hypothetical protein
			transl_table	11
8244	9377	CDS
			locus_tag	3300020774-4_4_10
			product	T4 1 superfamily RNA ligase
			transl_table	11
11051	9405	CDS
			locus_tag	3300020774-4_4_11
			product	WD40 superfamily protein
			transl_table	11
11695	11144	CDS
			locus_tag	3300020774-4_4_12
			product	hypothetical protein
			transl_table	11
12267	11767	CDS
			locus_tag	3300020774-4_4_13
			product	hypothetical protein
			transl_table	11
12309	13013	CDS
			locus_tag	3300020774-4_4_14
			product	hypothetical protein CAPTEDRAFT_172660
			transl_table	11
13167	15386	CDS
			locus_tag	3300020774-4_4_15
			product	NAD-dependent DNA ligase
			transl_table	11
15859	15422	CDS
			locus_tag	3300020774-4_4_16
			product	hypothetical protein
			transl_table	11
15953	16048	CDS
			locus_tag	3300020774-4_4_17
			product	hypothetical protein
			transl_table	11
16094	17173	CDS
			locus_tag	3300020774-4_4_18
			product	hypothetical protein
			transl_table	11
17944	17204	CDS
			locus_tag	3300020774-4_4_19
			product	hypothetical protein
			transl_table	11
19186	17981	CDS
			locus_tag	3300020774-4_4_20
			product	helicase
			transl_table	11
19604	19227	CDS
			locus_tag	3300020774-4_4_21
			product	putative ORFan
			transl_table	11
>3300020774-4_5
12203	12125	tRNA	3300020774-4_tRNA_1
			product	tRNA-Leu(TAA)
1764	1	CDS
			locus_tag	3300020774-4_5_1
			product	hypothetical protein
			transl_table	11
2386	2111	CDS
			locus_tag	3300020774-4_5_2
			product	hypothetical protein
			transl_table	11
2544	2425	CDS
			locus_tag	3300020774-4_5_3
			product	hypothetical protein
			transl_table	11
2915	2694	CDS
			locus_tag	3300020774-4_5_4
			product	hypothetical protein
			transl_table	11
3001	3495	CDS
			locus_tag	3300020774-4_5_5
			product	hypothetical protein
			transl_table	11
3688	3557	CDS
			locus_tag	3300020774-4_5_6
			product	hypothetical protein
			transl_table	11
4217	4765	CDS
			locus_tag	3300020774-4_5_7
			product	hypothetical protein
			transl_table	11
5198	5064	CDS
			locus_tag	3300020774-4_5_8
			product	hypothetical protein
			transl_table	11
5308	6570	CDS
			locus_tag	3300020774-4_5_9
			product	orotidine-5'-phosphate decarboxylase
			transl_table	11
6681	6902	CDS
			locus_tag	3300020774-4_5_10
			product	hypothetical protein
			transl_table	11
6972	7139	CDS
			locus_tag	3300020774-4_5_11
			product	hypothetical protein
			transl_table	11
7264	7956	CDS
			locus_tag	3300020774-4_5_12
			product	hypothetical protein
			transl_table	11
8059	7964	CDS
			locus_tag	3300020774-4_5_13
			product	hypothetical protein
			transl_table	11
8071	8469	CDS
			locus_tag	3300020774-4_5_14
			product	hypothetical protein
			transl_table	11
9225	8581	CDS
			locus_tag	3300020774-4_5_15
			product	hypothetical protein
			transl_table	11
9720	9250	CDS
			locus_tag	3300020774-4_5_16
			product	hypothetical protein
			transl_table	11
10478	9759	CDS
			locus_tag	3300020774-4_5_17
			product	hypothetical protein
			transl_table	11
10645	10824	CDS
			locus_tag	3300020774-4_5_18
			product	hypothetical protein
			transl_table	11
10954	12057	CDS
			locus_tag	3300020774-4_5_19
			product	AAA ATPase domain-containing protein
			transl_table	11
13087	12719	CDS
			locus_tag	3300020774-4_5_20
			product	putative ORFan
			transl_table	11
13807	13169	CDS
			locus_tag	3300020774-4_5_21
			product	hypothetical protein
			transl_table	11
13988	15154	CDS
			locus_tag	3300020774-4_5_22
			product	ankyrin repeat protein
			transl_table	11
15638	16282	CDS
			locus_tag	3300020774-4_5_23
			product	putative ankyrin repeat protein
			transl_table	11
16408	16833	CDS
			locus_tag	3300020774-4_5_24
			product	mg347 protein
			transl_table	11
17048	16947	CDS
			locus_tag	3300020774-4_5_25
			product	hypothetical protein
			transl_table	11
17126	17383	CDS
			locus_tag	3300020774-4_5_26
			product	hypothetical protein
			transl_table	11
17777	17601	CDS
			locus_tag	3300020774-4_5_27
			product	putative orfan
			transl_table	11
17857	18126	CDS
			locus_tag	3300020774-4_5_28
			product	hypothetical protein
			transl_table	11
>3300020774-4_6
2	487	CDS
			locus_tag	3300020774-4_6_1
			product	putative serpin-like protein
			transl_table	11
666	1958	CDS
			locus_tag	3300020774-4_6_2
			product	putative ORFan
			transl_table	11
2449	2021	CDS
			locus_tag	3300020774-4_6_3
			product	maninose-6P isomerase
			transl_table	11
2535	3452	CDS
			locus_tag	3300020774-4_6_4
			product	ankyrin repeat protein
			transl_table	11
3471	4205	CDS
			locus_tag	3300020774-4_6_5
			product	mg989 protein
			transl_table	11
4205	4411	CDS
			locus_tag	3300020774-4_6_6
			product	hypothetical protein
			transl_table	11
4502	4654	CDS
			locus_tag	3300020774-4_6_7
			product	hypothetical protein
			transl_table	11
4627	4857	CDS
			locus_tag	3300020774-4_6_8
			product	ribonuclease HI
			transl_table	11
5272	4907	CDS
			locus_tag	3300020774-4_6_9
			product	putative ORFan
			transl_table	11
5354	6100	CDS
			locus_tag	3300020774-4_6_10
			product	glycosyltransferase family 25
			transl_table	11
6140	6580	CDS
			locus_tag	3300020774-4_6_11
			product	hypothetical protein
			transl_table	11
6559	7020	CDS
			locus_tag	3300020774-4_6_12
			product	hypothetical protein
			transl_table	11
7091	8722	CDS
			locus_tag	3300020774-4_6_13
			product	hypothetical protein
			transl_table	11
8777	8992	CDS
			locus_tag	3300020774-4_6_14
			product	hypothetical protein
			transl_table	11
9037	9231	CDS
			locus_tag	3300020774-4_6_15
			product	hypothetical protein
			transl_table	11
9278	9658	CDS
			locus_tag	3300020774-4_6_16
			product	hypothetical protein
			transl_table	11
9805	11175	CDS
			locus_tag	3300020774-4_6_17
			product	hypothetical protein
			transl_table	11
13360	11408	CDS
			locus_tag	3300020774-4_6_18
			product	phosphoinositide 3-kinase
			transl_table	11
13597	14340	CDS
			locus_tag	3300020774-4_6_19
			product	hypothetical protein
			transl_table	11
14513	15031	CDS
			locus_tag	3300020774-4_6_20
			product	hypothetical protein
			transl_table	11
15121	15630	CDS
			locus_tag	3300020774-4_6_21
			product	hypothetical protein
			transl_table	11
17029	15665	CDS
			locus_tag	3300020774-4_6_22
			product	branched-chain amino acid ABC transporter substrate-binding protein
			transl_table	11
17383	17511	CDS
			locus_tag	3300020774-4_6_23
			product	putative ORFan
			transl_table	11
>3300020774-4_7
176	3	CDS
			locus_tag	3300020774-4_7_1
			product	hypothetical protein
			transl_table	11
690	220	CDS
			locus_tag	3300020774-4_7_2
			product	hypothetical protein
			transl_table	11
970	1692	CDS
			locus_tag	3300020774-4_7_3
			product	hypothetical protein
			transl_table	11
2763	4160	CDS
			locus_tag	3300020774-4_7_4
			product	hypothetical protein
			transl_table	11
4311	6170	CDS
			locus_tag	3300020774-4_7_5
			product	hypothetical protein
			transl_table	11
7232	6582	CDS
			locus_tag	3300020774-4_7_6
			product	hypothetical protein
			transl_table	11
7407	7655	CDS
			locus_tag	3300020774-4_7_7
			product	hypothetical protein
			transl_table	11
7859	7755	CDS
			locus_tag	3300020774-4_7_8
			product	hypothetical protein
			transl_table	11
9315	8188	CDS
			locus_tag	3300020774-4_7_9
			product	hypothetical protein
			transl_table	11
9603	10091	CDS
			locus_tag	3300020774-4_7_10
			product	putative ORFan
			transl_table	11
11334	10246	CDS
			locus_tag	3300020774-4_7_11
			product	hypothetical protein
			transl_table	11
13332	11626	CDS
			locus_tag	3300020774-4_7_12
			product	DNA primase
			transl_table	11
>3300020774-4_8
232	771	CDS
			locus_tag	3300020774-4_8_1
			product	hypothetical protein
			transl_table	11
828	2195	CDS
			locus_tag	3300020774-4_8_2
			product	hypothetical protein glt_00046
			transl_table	11
4258	2276	CDS
			locus_tag	3300020774-4_8_3
			product	putative core protein
			transl_table	11
4374	4736	CDS
			locus_tag	3300020774-4_8_4
			product	putative ORFan
			transl_table	11
4867	5406	CDS
			locus_tag	3300020774-4_8_5
			product	putative ORFan
			transl_table	11
5512	7026	CDS
			locus_tag	3300020774-4_8_6
			product	mg482 protein
			transl_table	11
7900	7067	CDS
			locus_tag	3300020774-4_8_7
			product	hypothetical protein
			transl_table	11
8035	9159	CDS
			locus_tag	3300020774-4_8_8
			product	hypothetical protein
			transl_table	11
9232	9651	CDS
			locus_tag	3300020774-4_8_9
			product	hypothetical protein
			transl_table	11
9971	9648	CDS
			locus_tag	3300020774-4_8_10
			product	hypothetical protein
			transl_table	11
10265	10050	CDS
			locus_tag	3300020774-4_8_11
			product	hypothetical protein
			transl_table	11
11633	10419	CDS
			locus_tag	3300020774-4_8_12
			product	hypothetical protein
			transl_table	11
11891	12196	CDS
			locus_tag	3300020774-4_8_13
			product	hypothetical protein
			transl_table	11
12570	12334	CDS
			locus_tag	3300020774-4_8_14
			product	hypothetical protein
			transl_table	11
12713	12835	CDS
			locus_tag	3300020774-4_8_15
			product	hypothetical protein
			transl_table	11
>3300020774-4_9
1	1179	CDS
			locus_tag	3300020774-4_9_1
			product	polyA polymerase catalitic subunit
			transl_table	11
3110	1266	CDS
			locus_tag	3300020774-4_9_2
			product	T5orf172 domain-containing protein
			transl_table	11
3315	4949	CDS
			locus_tag	3300020774-4_9_3
			product	putative ribonuclease 3
			transl_table	11
5358	4966	CDS
			locus_tag	3300020774-4_9_4
			product	hypothetical protein
			transl_table	11
5915	5454	CDS
			locus_tag	3300020774-4_9_5
			product	hypothetical protein
			transl_table	11
6190	9213	CDS
			locus_tag	3300020774-4_9_6
			product	hypothetical protein
			transl_table	11
9313	10053	CDS
			locus_tag	3300020774-4_9_7
			product	hypothetical protein
			transl_table	11
10150	11796	CDS
			locus_tag	3300020774-4_9_8
			product	hypothetical protein
			transl_table	11
11765	11899	CDS
			locus_tag	3300020774-4_9_9
			product	hypothetical protein
			transl_table	11
11905	12267	CDS
			locus_tag	3300020774-4_9_10
			product	hypothetical protein
			transl_table	11
>3300020774-4_10
626	3	CDS
			locus_tag	3300020774-4_10_1
			product	hypothetical protein
			transl_table	11
728	1474	CDS
			locus_tag	3300020774-4_10_2
			product	hypothetical protein Moumou_00450
			transl_table	11
2554	1469	CDS
			locus_tag	3300020774-4_10_3
			product	NUDIX hydrolase
			transl_table	11
3219	2620	CDS
			locus_tag	3300020774-4_10_4
			product	putative DNA-directed RNA polymerase subunit
			transl_table	11
3661	3308	CDS
			locus_tag	3300020774-4_10_5
			product	hypothetical protein
			transl_table	11
3798	3899	CDS
			locus_tag	3300020774-4_10_6
			product	hypothetical protein
			transl_table	11
4217	3885	CDS
			locus_tag	3300020774-4_10_7
			product	hypothetical protein glt_00490
			transl_table	11
4428	5354	CDS
			locus_tag	3300020774-4_10_8
			product	hypothetical protein
			transl_table	11
5837	5379	CDS
			locus_tag	3300020774-4_10_9
			product	hypothetical protein
			transl_table	11
6880	5942	CDS
			locus_tag	3300020774-4_10_10
			product	hypothetical protein
			transl_table	11
7459	7001	CDS
			locus_tag	3300020774-4_10_11
			product	hypothetical protein
			transl_table	11
7554	7456	CDS
			locus_tag	3300020774-4_10_12
			product	hypothetical protein
			transl_table	11
7852	7568	CDS
			locus_tag	3300020774-4_10_13
			product	hypothetical protein
			transl_table	11
8441	7929	CDS
			locus_tag	3300020774-4_10_14
			product	MULTISPECIES: hypothetical protein
			transl_table	11
8653	8811	CDS
			locus_tag	3300020774-4_10_15
			product	hypothetical protein
			transl_table	11
11249	8841	CDS
			locus_tag	3300020774-4_10_16
			product	putative NTPase
			transl_table	11
>3300020774-4_11
2	1150	CDS
			locus_tag	3300020774-4_11_1
			product	HNh endonuclease
			transl_table	11
2665	1325	CDS
			locus_tag	3300020774-4_11_2
			product	capsid protein 1
			transl_table	11
3309	3094	CDS
			locus_tag	3300020774-4_11_3
			product	hypothetical protein
			transl_table	11
3848	3964	CDS
			locus_tag	3300020774-4_11_4
			product	hypothetical protein
			transl_table	11
4641	5288	CDS
			locus_tag	3300020774-4_11_5
			product	hypothetical protein
			transl_table	11
5375	6814	CDS
			locus_tag	3300020774-4_11_6
			product	hypothetical protein
			transl_table	11
6915	8168	CDS
			locus_tag	3300020774-4_11_7
			product	mg462 protein
			transl_table	11
8332	9474	CDS
			locus_tag	3300020774-4_11_8
			product	uncharacterized HNH endonuclease
			transl_table	11
10149	9661	CDS
			locus_tag	3300020774-4_11_9
			product	hypothetical protein
			transl_table	11
11125	10250	CDS
			locus_tag	3300020774-4_11_10
			product	putative orfan
			transl_table	11
>3300020774-4_12
2	169	CDS
			locus_tag	3300020774-4_12_1
			product	hypothetical protein LBA_01178
			transl_table	11
189	410	CDS
			locus_tag	3300020774-4_12_2
			product	hypothetical protein LBA_01178
			transl_table	11
559	3132	CDS
			locus_tag	3300020774-4_12_3
			product	RecD-like DNA helicase
			transl_table	11
3122	3238	CDS
			locus_tag	3300020774-4_12_4
			product	hypothetical protein
			transl_table	11
3428	3952	CDS
			locus_tag	3300020774-4_12_5
			product	DEAD-like helicase
			transl_table	11
5398	4493	CDS
			locus_tag	3300020774-4_12_6
			product	hypothetical protein
			transl_table	11
5525	5746	CDS
			locus_tag	3300020774-4_12_7
			product	NUDIX hydrolase
			transl_table	11
6331	6113	CDS
			locus_tag	3300020774-4_12_8
			product	hypothetical protein
			transl_table	11
7808	6357	CDS
			locus_tag	3300020774-4_12_9
			product	hypothetical protein MIMI_L786b
			transl_table	11
8784	7837	CDS
			locus_tag	3300020774-4_12_10
			product	hypothetical protein
			transl_table	11
>3300020774-4_13
666	1	CDS
			locus_tag	3300020774-4_13_1
			product	UDP-glucose 4 epimerase
			transl_table	11
1663	692	CDS
			locus_tag	3300020774-4_13_2
			product	NAD-dependent epimerase/dehydratase
			transl_table	11
1718	2806	CDS
			locus_tag	3300020774-4_13_3
			product	putative glycosyltransferase
			transl_table	11
2835	3641	CDS
			locus_tag	3300020774-4_13_4
			product	putative UDP-glucose 6-dehydrogenase
			transl_table	11
5996	3711	CDS
			locus_tag	3300020774-4_13_5
			product	putative ORFan
			transl_table	11
7588	6038	CDS
			locus_tag	3300020774-4_13_6
			product	DEAD/SNF2 helicase
			transl_table	11
>3300020774-4_14
2904	1	CDS
			locus_tag	3300020774-4_14_1
			product	hypothetical protein
			transl_table	11
3044	4627	CDS
			locus_tag	3300020774-4_14_2
			product	NTPase family protein
			transl_table	11
4725	5957	CDS
			locus_tag	3300020774-4_14_3
			product	hypothetical protein
			transl_table	11
6736	6026	CDS
			locus_tag	3300020774-4_14_4
			product	hypothetical protein
			transl_table	11
6770	6883	CDS
			locus_tag	3300020774-4_14_5
			product	hypothetical protein
			transl_table	11
7336	7133	CDS
			locus_tag	3300020774-4_14_6
			product	hypothetical protein
			transl_table	11
>3300020774-4_15
2	364	CDS
			locus_tag	3300020774-4_15_1
			product	hypothetical protein
			transl_table	11
380	751	CDS
			locus_tag	3300020774-4_15_2
			product	hypothetical protein
			transl_table	11
1039	2277	CDS
			locus_tag	3300020774-4_15_3
			product	hypothetical protein MIMI_L786b
			transl_table	11
2736	3158	CDS
			locus_tag	3300020774-4_15_4
			product	hypothetical protein
			transl_table	11
3416	3294	CDS
			locus_tag	3300020774-4_15_5
			product	hypothetical protein
			transl_table	11
4541	6115	CDS
			locus_tag	3300020774-4_15_6
			product	leucine rich repeat protein
			transl_table	11
6180	7136	CDS
			locus_tag	3300020774-4_15_7
			product	hypothetical protein
			transl_table	11
>3300020774-4_16
1698	1	CDS
			locus_tag	3300020774-4_16_1
			product	HNH endonuclease
			transl_table	11
2747	1905	CDS
			locus_tag	3300020774-4_16_2
			product	transposase
			transl_table	11
3561	2935	CDS
			locus_tag	3300020774-4_16_3
			product	hypothetical protein
			transl_table	11
3728	4435	CDS
			locus_tag	3300020774-4_16_4
			product	hypothetical protein
			transl_table	11
4620	4802	CDS
			locus_tag	3300020774-4_16_5
			product	hypothetical protein
			transl_table	11
4896	5204	CDS
			locus_tag	3300020774-4_16_6
			product	hypothetical protein
			transl_table	11
6541	5201	CDS
			locus_tag	3300020774-4_16_7
			product	hypothetical protein
			transl_table	11
>3300020774-4_17
3	560	CDS
			locus_tag	3300020774-4_17_1
			product	glycosyl transferase
			transl_table	11
913	1668	CDS
			locus_tag	3300020774-4_17_2
			product	glycosyl transferase
			transl_table	11
1819	2568	CDS
			locus_tag	3300020774-4_17_3
			product	glycosyl transferase
			transl_table	11
3324	2644	CDS
			locus_tag	3300020774-4_17_4
			product	hypothetical protein
			transl_table	11
3442	3311	CDS
			locus_tag	3300020774-4_17_5
			product	hypothetical protein
			transl_table	11
3602	4051	CDS
			locus_tag	3300020774-4_17_6
			product	putative ORFan
			transl_table	11
4548	4177	CDS
			locus_tag	3300020774-4_17_7
			product	putative orfan
			transl_table	11
5194	5307	CDS
			locus_tag	3300020774-4_17_8
			product	hypothetical protein
			transl_table	11
5555	5304	CDS
			locus_tag	3300020774-4_17_9
			product	hypothetical protein
			transl_table	11
6087	6461	CDS
			locus_tag	3300020774-4_17_10
			product	ankyrin repeat-containing protein
			transl_table	11
>3300020774-4_18
565	2	CDS
			locus_tag	3300020774-4_18_1
			product	hypothetical protein
			transl_table	11
702	1559	CDS
			locus_tag	3300020774-4_18_2
			product	hypothetical protein Hokovirus_3_55
			transl_table	11
1664	1900	CDS
			locus_tag	3300020774-4_18_3
			product	hypothetical protein
			transl_table	11
2323	2781	CDS
			locus_tag	3300020774-4_18_4
			product	hypothetical protein
			transl_table	11
5237	2814	CDS
			locus_tag	3300020774-4_18_5
			product	SNF2 family helicase
			transl_table	11
5801	5289	CDS
			locus_tag	3300020774-4_18_6
			product	Fe-S cluster assembly protein hesB
			transl_table	11
>3300020774-4_19
1635	133	CDS
			locus_tag	3300020774-4_19_1
			product	putative ORFan
			transl_table	11
3372	1750	CDS
			locus_tag	3300020774-4_19_2
			product	putative orfan
			transl_table	11
4897	3446	CDS
			locus_tag	3300020774-4_19_3
			product	putative orfan
			transl_table	11
5331	4969	CDS
			locus_tag	3300020774-4_19_4
			product	hypothetical protein
			transl_table	11
5818	5351	CDS
			locus_tag	3300020774-4_19_5
			product	hypothetical protein
			transl_table	11
>3300020774-4_20
142	2	CDS
			locus_tag	3300020774-4_20_1
			product	putative poly(A) polymerase catalytic subunit
			transl_table	11
1068	355	CDS
			locus_tag	3300020774-4_20_2
			product	polyA polymerase catalitic subunit
			transl_table	11
1253	2395	CDS
			locus_tag	3300020774-4_20_3
			product	hypothetical protein
			transl_table	11
2731	2540	CDS
			locus_tag	3300020774-4_20_4
			product	hypothetical protein
			transl_table	11
3201	2839	CDS
			locus_tag	3300020774-4_20_5
			product	DNA ligase
			transl_table	11
4174	3386	CDS
			locus_tag	3300020774-4_20_6
			product	putative UDP-N-acetylglucosamine pyrophosphorylase
			transl_table	11
4285	4181	CDS
			locus_tag	3300020774-4_20_7
			product	hypothetical protein
			transl_table	11
4658	5584	CDS
			locus_tag	3300020774-4_20_8
			product	hypothetical protein
			transl_table	11
5733	5617	CDS
			locus_tag	3300020774-4_20_9
			product	hypothetical protein
			transl_table	11
>3300020774-4_21
3	1955	CDS
			locus_tag	3300020774-4_21_1
			product	hypothetical protein
			transl_table	11
3308	2064	CDS
			locus_tag	3300020774-4_21_2
			product	TATA-box-binding protein-like protein
			transl_table	11
3504	4919	CDS
			locus_tag	3300020774-4_21_3
			product	putative orfan
			transl_table	11
5203	5325	CDS
			locus_tag	3300020774-4_21_4
			product	hypothetical protein
			transl_table	11
5325	5591	CDS
			locus_tag	3300020774-4_21_5
			product	holliday junction resolvase
			transl_table	11
>3300020774-4_22
138	473	CDS
			locus_tag	3300020774-4_22_1
			product	hypothetical protein
			transl_table	11
517	909	CDS
			locus_tag	3300020774-4_22_2
			product	hypothetical protein
			transl_table	11
2420	960	CDS
			locus_tag	3300020774-4_22_3
			product	putative amine oxidase
			transl_table	11
3057	2662	CDS
			locus_tag	3300020774-4_22_4
			product	E3 ubiquitin-protein ligase RNF181 homolog
			transl_table	11
5439	3385	CDS
			locus_tag	3300020774-4_22_5
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
>3300020774-4_23
1	1422	CDS
			locus_tag	3300020774-4_23_1
			product	putative BTB_POZ domain and WD-repeat protein
			transl_table	11
1477	1917	CDS
			locus_tag	3300020774-4_23_2
			product	hypothetical protein
			transl_table	11
2010	3371	CDS
			locus_tag	3300020774-4_23_3
			product	hypothetical protein
			transl_table	11
3420	4859	CDS
			locus_tag	3300020774-4_23_4
			product	hypothetical protein
			transl_table	11
4959	5369	CDS
			locus_tag	3300020774-4_23_5
			product	hypothetical protein
			transl_table	11
5452	5544	CDS
			locus_tag	3300020774-4_23_6
			product	hypothetical protein
			transl_table	11
>3300020774-4_24
1	258	CDS
			locus_tag	3300020774-4_24_1
			product	membrane protein
			transl_table	11
429	1859	CDS
			locus_tag	3300020774-4_24_2
			product	hypothetical protein
			transl_table	11
2358	1996	CDS
			locus_tag	3300020774-4_24_3
			product	hypothetical protein
			transl_table	11
2766	2473	CDS
			locus_tag	3300020774-4_24_4
			product	hypothetical protein
			transl_table	11
2910	2800	CDS
			locus_tag	3300020774-4_24_5
			product	hypothetical protein
			transl_table	11
4018	2897	CDS
			locus_tag	3300020774-4_24_6
			product	hypothetical protein Hokovirus_1_18
			transl_table	11
4917	4294	CDS
			locus_tag	3300020774-4_24_7
			product	hypothetical protein
			transl_table	11
>3300020774-4_25
1	162	CDS
			locus_tag	3300020774-4_25_1
			product	hypothetical protein
			transl_table	11
163	2229	CDS
			locus_tag	3300020774-4_25_2
			product	biosynthetic-type acetolactate synthase large subunit
			transl_table	11
2426	3511	CDS
			locus_tag	3300020774-4_25_3
			product	hypothetical protein
			transl_table	11
3489	3584	CDS
			locus_tag	3300020774-4_25_4
			product	hypothetical protein
			transl_table	11
4437	3568	CDS
			locus_tag	3300020774-4_25_5
			product	hypothetical protein
			transl_table	11
>3300020774-4_26
2	445	CDS
			locus_tag	3300020774-4_26_1
			product	SET domain protein
			transl_table	11
505	720	CDS
			locus_tag	3300020774-4_26_2
			product	hypothetical protein
			transl_table	11
787	1506	CDS
			locus_tag	3300020774-4_26_3
			product	hypothetical protein
			transl_table	11
1570	2472	CDS
			locus_tag	3300020774-4_26_4
			product	putative orfan
			transl_table	11
3545	2607	CDS
			locus_tag	3300020774-4_26_5
			product	hypothetical protein
			transl_table	11
3657	4226	CDS
			locus_tag	3300020774-4_26_6
			product	putative ATP-dependent DNA helicase
			transl_table	11
>3300020774-4_27
2	1990	CDS
			locus_tag	3300020774-4_27_1
			product	hypothetical protein
			transl_table	11
2826	2026	CDS
			locus_tag	3300020774-4_27_2
			product	alpha/beta hydrolase family protein
			transl_table	11
3780	2881	CDS
			locus_tag	3300020774-4_27_3
			product	hypothetical protein
			transl_table	11
3927	4115	CDS
			locus_tag	3300020774-4_27_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_28
411	1	CDS
			locus_tag	3300020774-4_28_1
			product	hypothetical protein
			transl_table	11
563	2452	CDS
			locus_tag	3300020774-4_28_2
			product	hypothetical protein
			transl_table	11
2563	3657	CDS
			locus_tag	3300020774-4_28_3
			product	putative viral transcription factor
			transl_table	11
3873	3706	CDS
			locus_tag	3300020774-4_28_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_29
1	126	CDS
			locus_tag	3300020774-4_29_1
			product	hypothetical protein
			transl_table	11
160	318	CDS
			locus_tag	3300020774-4_29_2
			product	hypothetical protein
			transl_table	11
2311	674	CDS
			locus_tag	3300020774-4_29_3
			product	hypothetical protein
			transl_table	11
2726	2475	CDS
			locus_tag	3300020774-4_29_4
			product	hypothetical protein
			transl_table	11
3386	2778	CDS
			locus_tag	3300020774-4_29_5
			product	hypothetical protein
			transl_table	11
3793	3497	CDS
			locus_tag	3300020774-4_29_6
			product	hypothetical protein
			transl_table	11
>3300020774-4_30
822	70	CDS
			locus_tag	3300020774-4_30_1
			product	hypothetical protein
			transl_table	11
1708	881	CDS
			locus_tag	3300020774-4_30_2
			product	thiol oxidoreductase E10R
			transl_table	11
2034	1765	CDS
			locus_tag	3300020774-4_30_3
			product	hypothetical protein
			transl_table	11
3040	2105	CDS
			locus_tag	3300020774-4_30_4
			product	putative ORFan
			transl_table	11
3620	3129	CDS
			locus_tag	3300020774-4_30_5
			product	putative peptidase S9
			transl_table	11
>3300020774-4_31
383	3	CDS
			locus_tag	3300020774-4_31_1
			product	low complexity protein
			transl_table	11
567	1808	CDS
			locus_tag	3300020774-4_31_2
			product	hypothetical protein
			transl_table	11
1912	2190	CDS
			locus_tag	3300020774-4_31_3
			product	endonuclease
			transl_table	11
2682	3038	CDS
			locus_tag	3300020774-4_31_4
			product	hypothetical protein
			transl_table	11
3479	3595	CDS
			locus_tag	3300020774-4_31_5
			product	restriction endonuclease
			transl_table	11
>3300020774-4_32
184	2	CDS
			locus_tag	3300020774-4_32_1
			product	hypothetical protein
			transl_table	11
903	1031	CDS
			locus_tag	3300020774-4_32_2
			product	hypothetical protein
			transl_table	11
1558	1319	CDS
			locus_tag	3300020774-4_32_3
			product	hypothetical protein
			transl_table	11
1902	1675	CDS
			locus_tag	3300020774-4_32_4
			product	hypothetical protein DFA_00300
			transl_table	11
2680	3483	CDS
			locus_tag	3300020774-4_32_5
			product	hypothetical protein Klosneuvirus_2_178
			transl_table	11
>3300020774-4_33
2	1102	CDS
			locus_tag	3300020774-4_33_1
			product	highly derived d5-like helicase-primase: PROVISIONAL
			transl_table	11
1644	1345	CDS
			locus_tag	3300020774-4_33_2
			product	hypothetical protein
			transl_table	11
2021	1878	CDS
			locus_tag	3300020774-4_33_3
			product	hypothetical protein
			transl_table	11
3443	3544	CDS
			locus_tag	3300020774-4_33_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_34
3039	481	CDS
			locus_tag	3300020774-4_34_1
			product	DNA replication protein
			transl_table	11
3169	3369	CDS
			locus_tag	3300020774-4_34_2
			product	hypothetical protein
			transl_table	11
>3300020774-4_35
620	96	CDS
			locus_tag	3300020774-4_35_1
			product	hypothetical protein
			transl_table	11
1101	781	CDS
			locus_tag	3300020774-4_35_2
			product	leucyl-tRNA synthetase
			transl_table	11
2441	1173	CDS
			locus_tag	3300020774-4_35_3
			product	hypothetical protein
			transl_table	11
2820	2503	CDS
			locus_tag	3300020774-4_35_4
			product	hypothetical protein
			transl_table	11
2971	2882	CDS
			locus_tag	3300020774-4_35_5
			product	hypothetical protein
			transl_table	11
>3300020774-4_36
290	3	CDS
			locus_tag	3300020774-4_36_1
			product	hypothetical protein
			transl_table	11
459	1106	CDS
			locus_tag	3300020774-4_36_2
			product	hypothetical protein
			transl_table	11
1154	2455	CDS
			locus_tag	3300020774-4_36_3
			product	hypothetical protein MegaChil _gp0801
			transl_table	11
3067	2498	CDS
			locus_tag	3300020774-4_36_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_37
1635	196	CDS
			locus_tag	3300020774-4_37_1
			product	HNH endonuclease
			transl_table	11
1814	1632	CDS
			locus_tag	3300020774-4_37_2
			product	hypothetical protein
			transl_table	11
2083	1859	CDS
			locus_tag	3300020774-4_37_3
			product	ubiquitin domain-containing protein
			transl_table	11
2939	2196	CDS
			locus_tag	3300020774-4_37_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_38
1	210	CDS
			locus_tag	3300020774-4_38_1
			product	putative ATP-dependent DNA helicase
			transl_table	11
334	2196	CDS
			locus_tag	3300020774-4_38_2
			product	putative ATP-dependent DNA helicase
			transl_table	11
2774	2923	CDS
			locus_tag	3300020774-4_38_3
			product	hypothetical protein
			transl_table	11
>3300020774-4_39
261	1	CDS
			locus_tag	3300020774-4_39_1
			product	hypothetical protein
			transl_table	11
366	836	CDS
			locus_tag	3300020774-4_39_2
			product	hypothetical protein HIRU_S677
			transl_table	11
867	971	CDS
			locus_tag	3300020774-4_39_3
			product	hypothetical protein
			transl_table	11
1691	1119	CDS
			locus_tag	3300020774-4_39_4
			product	hypothetical protein
			transl_table	11
2888	1746	CDS
			locus_tag	3300020774-4_39_5
			product	biotin--protein ligase
			transl_table	11
>3300020774-4_40
3	827	CDS
			locus_tag	3300020774-4_40_1
			product	hypothetical protein
			transl_table	11
2147	810	CDS
			locus_tag	3300020774-4_40_2
			product	hypothetical protein
			transl_table	11
2780	2301	CDS
			locus_tag	3300020774-4_40_3
			product	hypothetical protein
			transl_table	11
>3300020774-4_41
1	447	CDS
			locus_tag	3300020774-4_41_1
			product	hypothetical protein
			transl_table	11
986	567	CDS
			locus_tag	3300020774-4_41_2
			product	hypothetical protein
			transl_table	11
1160	1492	CDS
			locus_tag	3300020774-4_41_3
			product	hypothetical protein
			transl_table	11
1654	1538	CDS
			locus_tag	3300020774-4_41_4
			product	hypothetical protein
			transl_table	11
1755	2657	CDS
			locus_tag	3300020774-4_41_5
			product	putative orfan
			transl_table	11
>3300020774-4_42
1634	1837	CDS
			locus_tag	3300020774-4_42_1
			product	hypothetical protein
			transl_table	11
1938	1834	CDS
			locus_tag	3300020774-4_42_2
			product	hypothetical protein
			transl_table	11
2051	2242	CDS
			locus_tag	3300020774-4_42_3
			product	hypothetical protein
			transl_table	11
2611	2712	CDS
			locus_tag	3300020774-4_42_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_43
1	231	CDS
			locus_tag	3300020774-4_43_1
			product	hypothetical protein
			transl_table	11
385	810	CDS
			locus_tag	3300020774-4_43_2
			product	putative orfan
			transl_table	11
1202	855	CDS
			locus_tag	3300020774-4_43_3
			product	hypothetical protein
			transl_table	11
1396	1307	CDS
			locus_tag	3300020774-4_43_4
			product	hypothetical protein
			transl_table	11
1596	1393	CDS
			locus_tag	3300020774-4_43_5
			product	hypothetical protein
			transl_table	11
2552	1665	CDS
			locus_tag	3300020774-4_43_6
			product	HSP70-like protein
			transl_table	11
>3300020774-4_44
2	736	CDS
			locus_tag	3300020774-4_44_1
			product	hypothetical protein
			transl_table	11
809	1279	CDS
			locus_tag	3300020774-4_44_2
			product	hypothetical protein
			transl_table	11
1626	1504	CDS
			locus_tag	3300020774-4_44_3
			product	hypothetical protein
			transl_table	11
1697	2050	CDS
			locus_tag	3300020774-4_44_4
			product	hypothetical protein
			transl_table	11
2337	2143	CDS
			locus_tag	3300020774-4_44_5
			product	hypothetical protein
			transl_table	11
2676	2362	CDS
			locus_tag	3300020774-4_44_6
			product	hypothetical protein
			transl_table	11
>3300020774-4_45
1	786	CDS
			locus_tag	3300020774-4_45_1
			product	hypothetical protein Hokovirus_2_58
			transl_table	11
2118	1093	CDS
			locus_tag	3300020774-4_45_2
			product	hypothetical protein
			transl_table	11
2655	2272	CDS
			locus_tag	3300020774-4_45_3
			product	hypothetical protein
			transl_table	11
>3300020774-4_46
1152	1	CDS
			locus_tag	3300020774-4_46_1
			product	hypothetical protein
			transl_table	11
1484	1320	CDS
			locus_tag	3300020774-4_46_2
			product	hypothetical protein
			transl_table	11
1670	2257	CDS
			locus_tag	3300020774-4_46_3
			product	hypothetical protein mv_R1036
			transl_table	11
2273	2536	CDS
			locus_tag	3300020774-4_46_4
			product	hypothetical protein
			transl_table	11
>3300020774-4_47
2	268	CDS
			locus_tag	3300020774-4_47_1
			product	exodeoxyribonuclease III
			transl_table	11
431	610	CDS
			locus_tag	3300020774-4_47_2
			product	hypothetical protein
			transl_table	11
684	1001	CDS
			locus_tag	3300020774-4_47_3
			product	hypothetical protein
			transl_table	11
1781	1017	CDS
			locus_tag	3300020774-4_47_4
			product	putative helicase
			transl_table	11
1857	1738	CDS
			locus_tag	3300020774-4_47_5
			product	hypothetical protein
			transl_table	11
2059	2175	CDS
			locus_tag	3300020774-4_47_6
			product	hypothetical protein
			transl_table	11
2298	2402	CDS
			locus_tag	3300020774-4_47_7
			product	hypothetical protein
			transl_table	11
