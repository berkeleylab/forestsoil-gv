>3300020918-1_1
580	2	CDS
			locus_tag	3300020918-1_1_1
			product	R3H domain protein
			transl_table	11
649	1566	CDS
			locus_tag	3300020918-1_1_2
			product	hypothetical protein Klosneuvirus_3_39
			transl_table	11
2701	1622	CDS
			locus_tag	3300020918-1_1_3
			product	hypothetical protein
			transl_table	11
3036	2803	CDS
			locus_tag	3300020918-1_1_4
			product	hypothetical protein
			transl_table	11
3861	3067	CDS
			locus_tag	3300020918-1_1_5
			product	putative ankyrin repeat protein
			transl_table	11
3993	4535	CDS
			locus_tag	3300020918-1_1_6
			product	hypothetical protein
			transl_table	11
4763	4602	CDS
			locus_tag	3300020918-1_1_7
			product	hypothetical protein
			transl_table	11
5613	4810	CDS
			locus_tag	3300020918-1_1_8
			product	AAA domain protein
			transl_table	11
5978	5616	CDS
			locus_tag	3300020918-1_1_9
			product	hypothetical protein
			transl_table	11
6475	5990	CDS
			locus_tag	3300020918-1_1_10
			product	hypothetical protein
			transl_table	11
6630	6947	CDS
			locus_tag	3300020918-1_1_11
			product	hypothetical protein
			transl_table	11
7780	6989	CDS
			locus_tag	3300020918-1_1_12
			product	hypothetical protein
			transl_table	11
8572	7859	CDS
			locus_tag	3300020918-1_1_13
			product	hypothetical protein
			transl_table	11
8645	9223	CDS
			locus_tag	3300020918-1_1_14
			product	hypothetical protein OM95_07370
			transl_table	11
9341	10111	CDS
			locus_tag	3300020918-1_1_15
			product	hypothetical protein
			transl_table	11
10688	12112	CDS
			locus_tag	3300020918-1_1_16
			product	HNh endonuclease
			transl_table	11
13587	12187	CDS
			locus_tag	3300020918-1_1_17
			product	capsid protein 1
			transl_table	11
13712	13584	CDS
			locus_tag	3300020918-1_1_18
			product	hypothetical protein
			transl_table	11
14682	13984	CDS
			locus_tag	3300020918-1_1_19
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
16285	15305	CDS
			locus_tag	3300020918-1_1_20
			product	hypothetical protein
			transl_table	11
16461	17090	CDS
			locus_tag	3300020918-1_1_21
			product	hypothetical protein
			transl_table	11
17176	17811	CDS
			locus_tag	3300020918-1_1_22
			product	hypothetical protein
			transl_table	11
17935	18312	CDS
			locus_tag	3300020918-1_1_23
			product	hypothetical protein
			transl_table	11
18405	19028	CDS
			locus_tag	3300020918-1_1_24
			product	collagen-like protein
			transl_table	11
19099	19767	CDS
			locus_tag	3300020918-1_1_25
			product	hypothetical protein
			transl_table	11
19976	21328	CDS
			locus_tag	3300020918-1_1_26
			product	hypothetical protein SAMN03159341_1342
			transl_table	11
21405	21872	CDS
			locus_tag	3300020918-1_1_27
			product	nucleoside deaminase
			transl_table	11
23982	21910	CDS
			locus_tag	3300020918-1_1_28
			product	dynamin family GTPase
			transl_table	11
24098	24625	CDS
			locus_tag	3300020918-1_1_29
			product	phosphatidylethanolamine-binding protein-like protein
			transl_table	11
26148	24622	CDS
			locus_tag	3300020918-1_1_30
			product	FkbM family methyltransferase
			transl_table	11
26205	27104	CDS
			locus_tag	3300020918-1_1_31
			product	predicted ORF
			transl_table	11
27110	27889	CDS
			locus_tag	3300020918-1_1_32
			product	hypothetical protein
			transl_table	11
27952	28590	CDS
			locus_tag	3300020918-1_1_33
			product	hypothetical protein
			transl_table	11
28703	30388	CDS
			locus_tag	3300020918-1_1_34
			product	hypothetical protein CBD74_00380
			transl_table	11
30427	31692	CDS
			locus_tag	3300020918-1_1_35
			product	hypothetical protein
			transl_table	11
33166	31724	CDS
			locus_tag	3300020918-1_1_36
			product	collagen triple helix repeat containing protein
			transl_table	11
36044	33258	CDS
			locus_tag	3300020918-1_1_37
			product	collagen triple helix repeat motif-containing protein
			transl_table	11
36242	37573	CDS
			locus_tag	3300020918-1_1_38
			product	mg462 protein
			transl_table	11
37685	38311	CDS
			locus_tag	3300020918-1_1_39
			product	putative heat shock 70 kDa protein
			transl_table	11
38614	39723	CDS
			locus_tag	3300020918-1_1_40
			product	hypothetical protein
			transl_table	11
39752	41080	CDS
			locus_tag	3300020918-1_1_41
			product	HSP70-like protein
			transl_table	11
41559	41116	CDS
			locus_tag	3300020918-1_1_42
			product	hypothetical protein
			transl_table	11
41592	41720	CDS
			locus_tag	3300020918-1_1_43
			product	hypothetical protein
			transl_table	11
42195	41737	CDS
			locus_tag	3300020918-1_1_44
			product	hypothetical protein
			transl_table	11
42723	42292	CDS
			locus_tag	3300020918-1_1_45
			product	hypothetical protein
			transl_table	11
43266	42781	CDS
			locus_tag	3300020918-1_1_46
			product	hypothetical protein
			transl_table	11
43759	43391	CDS
			locus_tag	3300020918-1_1_47
			product	hypothetical protein
			transl_table	11
44627	43806	CDS
			locus_tag	3300020918-1_1_48
			product	hypothetical protein
			transl_table	11
45301	44723	CDS
			locus_tag	3300020918-1_1_49
			product	hypothetical protein
			transl_table	11
47072	45369	CDS
			locus_tag	3300020918-1_1_50
			product	HNH endonuclease
			transl_table	11
47928	47806	CDS
			locus_tag	3300020918-1_1_51
			product	PREDICTED: polyubiquitin-B
			transl_table	11
48623	48093	CDS
			locus_tag	3300020918-1_1_52
			product	hypothetical protein
			transl_table	11
48822	49130	CDS
			locus_tag	3300020918-1_1_53
			product	endonuclease of the XPG family
			transl_table	11
49192	50115	CDS
			locus_tag	3300020918-1_1_54
			product	endonuclease of the XPG family
			transl_table	11
50250	50399	CDS
			locus_tag	3300020918-1_1_55
			product	hypothetical protein
			transl_table	11
50947	51198	CDS
			locus_tag	3300020918-1_1_56
			product	hypothetical protein
			transl_table	11
>3300020918-1_2
171	722	CDS
			locus_tag	3300020918-1_2_1
			product	hypothetical protein
			transl_table	11
816	2081	CDS
			locus_tag	3300020918-1_2_2
			product	mg437 protein
			transl_table	11
3292	2144	CDS
			locus_tag	3300020918-1_2_3
			product	putative viral transcription factor
			transl_table	11
5281	3374	CDS
			locus_tag	3300020918-1_2_4
			product	hypothetical protein
			transl_table	11
5518	6255	CDS
			locus_tag	3300020918-1_2_5
			product	hypothetical protein
			transl_table	11
6670	6362	CDS
			locus_tag	3300020918-1_2_6
			product	hypothetical protein
			transl_table	11
6798	7814	CDS
			locus_tag	3300020918-1_2_7
			product	phospholipase
			transl_table	11
9015	7828	CDS
			locus_tag	3300020918-1_2_8
			product	putative J domain-containing protein
			transl_table	11
9126	10667	CDS
			locus_tag	3300020918-1_2_9
			product	putative ADP-ribosylglycohydrolase
			transl_table	11
11171	10677	CDS
			locus_tag	3300020918-1_2_10
			product	thioredoxin domain-containing protein
			transl_table	11
11332	13932	CDS
			locus_tag	3300020918-1_2_11
			product	hypothetical protein Moumou_00371
			transl_table	11
14695	14012	CDS
			locus_tag	3300020918-1_2_12
			product	hypothetical protein
			transl_table	11
16282	14699	CDS
			locus_tag	3300020918-1_2_13
			product	mg448 protein
			transl_table	11
23729	16332	CDS
			locus_tag	3300020918-1_2_14
			product	hypothetical protein
			transl_table	11
26002	23807	CDS
			locus_tag	3300020918-1_2_15
			product	putative ankyrin repeat protein
			transl_table	11
27275	26124	CDS
			locus_tag	3300020918-1_2_16
			product	hypothetical protein
			transl_table	11
29498	27372	CDS
			locus_tag	3300020918-1_2_17
			product	putative ankyrin repeat protein
			transl_table	11
31355	29811	CDS
			locus_tag	3300020918-1_2_18
			product	putative transposase/mobile element protein
			transl_table	11
32529	31447	CDS
			locus_tag	3300020918-1_2_19
			product	hypothetical protein
			transl_table	11
>3300020918-1_3
604	2	CDS
			locus_tag	3300020918-1_3_1
			product	hypothetical protein
			transl_table	11
761	1186	CDS
			locus_tag	3300020918-1_3_2
			product	hypothetical protein
			transl_table	11
1649	1317	CDS
			locus_tag	3300020918-1_3_3
			product	hypothetical protein
			transl_table	11
2773	2102	CDS
			locus_tag	3300020918-1_3_4
			product	hypothetical protein
			transl_table	11
3301	2921	CDS
			locus_tag	3300020918-1_3_5
			product	hypothetical protein
			transl_table	11
4398	3352	CDS
			locus_tag	3300020918-1_3_6
			product	hypothetical protein
			transl_table	11
4545	6284	CDS
			locus_tag	3300020918-1_3_7
			product	collagen triple helix repeat motif-containing protein
			transl_table	11
7782	6346	CDS
			locus_tag	3300020918-1_3_8
			product	hypothetical protein
			transl_table	11
9140	7947	CDS
			locus_tag	3300020918-1_3_9
			product	hypothetical protein
			transl_table	11
9286	10155	CDS
			locus_tag	3300020918-1_3_10
			product	hypothetical protein AURDEDRAFT_179421
			transl_table	11
10133	10717	CDS
			locus_tag	3300020918-1_3_11
			product	mg989 protein
			transl_table	11
11717	10899	CDS
			locus_tag	3300020918-1_3_12
			product	hypothetical protein
			transl_table	11
12543	11851	CDS
			locus_tag	3300020918-1_3_13
			product	hypothetical protein
			transl_table	11
13142	12609	CDS
			locus_tag	3300020918-1_3_14
			product	hypothetical protein
			transl_table	11
13331	13921	CDS
			locus_tag	3300020918-1_3_15
			product	hypothetical protein
			transl_table	11
14591	14010	CDS
			locus_tag	3300020918-1_3_16
			product	hypothetical protein
			transl_table	11
15457	14771	CDS
			locus_tag	3300020918-1_3_17
			product	hypothetical protein
			transl_table	11
16879	15554	CDS
			locus_tag	3300020918-1_3_18
			product	PREDICTED: LOW QUALITY PROTEIN: E3 ubiquitin-protein ligase LRSAM1
			transl_table	11
17140	18024	CDS
			locus_tag	3300020918-1_3_19
			product	hypothetical protein
			transl_table	11
18579	18148	CDS
			locus_tag	3300020918-1_3_20
			product	hypothetical protein
			transl_table	11
19799	18582	CDS
			locus_tag	3300020918-1_3_21
			product	hypothetical protein
			transl_table	11
21545	19905	CDS
			locus_tag	3300020918-1_3_22
			product	hypothetical protein
			transl_table	11
21705	22172	CDS
			locus_tag	3300020918-1_3_23
			product	hypothetical protein
			transl_table	11
22307	22495	CDS
			locus_tag	3300020918-1_3_24
			product	hypothetical protein
			transl_table	11
22692	22868	CDS
			locus_tag	3300020918-1_3_25
			product	hypothetical protein
			transl_table	11
30624	22948	CDS
			locus_tag	3300020918-1_3_26
			product	hypothetical protein DDG59_07150, partial
			transl_table	11
>3300020918-1_4
415	2	CDS
			locus_tag	3300020918-1_4_1
			product	hypothetical protein
			transl_table	11
548	1486	CDS
			locus_tag	3300020918-1_4_2
			product	hypothetical protein
			transl_table	11
1649	3712	CDS
			locus_tag	3300020918-1_4_3
			product	putative ankyrin repeat protein
			transl_table	11
11272	3767	CDS
			locus_tag	3300020918-1_4_4
			product	putative ORFan
			transl_table	11
11476	13029	CDS
			locus_tag	3300020918-1_4_5
			product	hypothetical protein
			transl_table	11
13097	14125	CDS
			locus_tag	3300020918-1_4_6
			product	putative transposase
			transl_table	11
14226	14747	CDS
			locus_tag	3300020918-1_4_7
			product	hypothetical protein
			transl_table	11
15646	14750	CDS
			locus_tag	3300020918-1_4_8
			product	hypothetical protein
			transl_table	11
15926	15696	CDS
			locus_tag	3300020918-1_4_9
			product	hypothetical protein
			transl_table	11
16935	15958	CDS
			locus_tag	3300020918-1_4_10
			product	hypothetical protein
			transl_table	11
17901	17023	CDS
			locus_tag	3300020918-1_4_11
			product	hypothetical protein
			transl_table	11
18752	18012	CDS
			locus_tag	3300020918-1_4_12
			product	hypothetical protein
			transl_table	11
19873	18827	CDS
			locus_tag	3300020918-1_4_13
			product	hypothetical protein
			transl_table	11
20003	21055	CDS
			locus_tag	3300020918-1_4_14
			product	hypothetical protein
			transl_table	11
21101	22135	CDS
			locus_tag	3300020918-1_4_15
			product	hypothetical protein
			transl_table	11
22228	23328	CDS
			locus_tag	3300020918-1_4_16
			product	hypothetical protein
			transl_table	11
23357	24484	CDS
			locus_tag	3300020918-1_4_17
			product	hypothetical protein
			transl_table	11
25680	24559	CDS
			locus_tag	3300020918-1_4_18
			product	hypothetical protein
			transl_table	11
>3300020918-1_5
1	432	CDS
			locus_tag	3300020918-1_5_1
			product	hypothetical protein Klosneuvirus_1_332
			transl_table	11
708	460	CDS
			locus_tag	3300020918-1_5_2
			product	hypothetical protein
			transl_table	11
1081	674	CDS
			locus_tag	3300020918-1_5_3
			product	hypothetical protein
			transl_table	11
1957	1196	CDS
			locus_tag	3300020918-1_5_4
			product	hypothetical protein
			transl_table	11
2910	2050	CDS
			locus_tag	3300020918-1_5_5
			product	hypothetical protein
			transl_table	11
3341	2994	CDS
			locus_tag	3300020918-1_5_6
			product	hypothetical protein
			transl_table	11
5231	3639	CDS
			locus_tag	3300020918-1_5_7
			product	putative transposase/mobile element protein
			transl_table	11
6283	6026	CDS
			locus_tag	3300020918-1_5_8
			product	hypothetical protein
			transl_table	11
7509	6394	CDS
			locus_tag	3300020918-1_5_9
			product	hypothetical protein
			transl_table	11
8587	7619	CDS
			locus_tag	3300020918-1_5_10
			product	hypothetical protein
			transl_table	11
8842	9279	CDS
			locus_tag	3300020918-1_5_11
			product	hypothetical protein Klosneuvirus_3_10
			transl_table	11
9391	10038	CDS
			locus_tag	3300020918-1_5_12
			product	hypothetical protein
			transl_table	11
10461	10078	CDS
			locus_tag	3300020918-1_5_13
			product	hypothetical protein
			transl_table	11
10628	10503	CDS
			locus_tag	3300020918-1_5_14
			product	hypothetical protein
			transl_table	11
10667	13048	CDS
			locus_tag	3300020918-1_5_15
			product	choice-of-anchor D domain-containing protein
			transl_table	11
13706	13119	CDS
			locus_tag	3300020918-1_5_16
			product	hypothetical protein
			transl_table	11
13843	13742	CDS
			locus_tag	3300020918-1_5_17
			product	hypothetical protein
			transl_table	11
13842	15155	CDS
			locus_tag	3300020918-1_5_18
			product	hypothetical protein
			transl_table	11
16609	15239	CDS
			locus_tag	3300020918-1_5_19
			product	hypothetical protein
			transl_table	11
16648	16767	CDS
			locus_tag	3300020918-1_5_20
			product	hypothetical protein
			transl_table	11
17175	16855	CDS
			locus_tag	3300020918-1_5_21
			product	hypothetical protein
			transl_table	11
17332	18147	CDS
			locus_tag	3300020918-1_5_22
			product	hypothetical protein
			transl_table	11
19563	18214	CDS
			locus_tag	3300020918-1_5_23
			product	hypothetical protein
			transl_table	11
20978	19695	CDS
			locus_tag	3300020918-1_5_24
			product	hypothetical protein
			transl_table	11
21272	22819	CDS
			locus_tag	3300020918-1_5_25
			product	hypothetical protein
			transl_table	11
22918	23370	CDS
			locus_tag	3300020918-1_5_26
			product	hypothetical protein
			transl_table	11
>3300020918-1_6
1	2007	CDS
			locus_tag	3300020918-1_6_1
			product	hypothetical protein
			transl_table	11
3818	2085	CDS
			locus_tag	3300020918-1_6_2
			product	alpha/beta hydrolase family protein
			transl_table	11
3969	4343	CDS
			locus_tag	3300020918-1_6_3
			product	hypothetical protein
			transl_table	11
4404	4658	CDS
			locus_tag	3300020918-1_6_4
			product	hypothetical protein
			transl_table	11
4829	6961	CDS
			locus_tag	3300020918-1_6_5
			product	putative transposase
			transl_table	11
7075	7248	CDS
			locus_tag	3300020918-1_6_6
			product	hypothetical protein
			transl_table	11
7340	8650	CDS
			locus_tag	3300020918-1_6_7
			product	ornithine decarboxylase
			transl_table	11
8738	10084	CDS
			locus_tag	3300020918-1_6_8
			product	TIGR03118 family protein
			transl_table	11
10657	10142	CDS
			locus_tag	3300020918-1_6_9
			product	hypothetical protein
			transl_table	11
10773	11441	CDS
			locus_tag	3300020918-1_6_10
			product	hypothetical protein
			transl_table	11
13400	11520	CDS
			locus_tag	3300020918-1_6_11
			product	putative core protein
			transl_table	11
13499	13999	CDS
			locus_tag	3300020918-1_6_12
			product	putative orfan
			transl_table	11
14100	15644	CDS
			locus_tag	3300020918-1_6_13
			product	mg482 protein
			transl_table	11
16721	15714	CDS
			locus_tag	3300020918-1_6_14
			product	hypothetical protein
			transl_table	11
16844	17908	CDS
			locus_tag	3300020918-1_6_15
			product	hypothetical protein
			transl_table	11
17937	19442	CDS
			locus_tag	3300020918-1_6_16
			product	hypothetical protein
			transl_table	11
19528	20160	CDS
			locus_tag	3300020918-1_6_17
			product	hypothetical protein
			transl_table	11
20225	21106	CDS
			locus_tag	3300020918-1_6_18
			product	hypothetical protein
			transl_table	11
22168	21125	CDS
			locus_tag	3300020918-1_6_19
			product	hypothetical protein
			transl_table	11
>3300020918-1_7
3	230	CDS
			locus_tag	3300020918-1_7_1
			product	hypothetical protein
			transl_table	11
423	734	CDS
			locus_tag	3300020918-1_7_2
			product	hypothetical protein
			transl_table	11
818	1252	CDS
			locus_tag	3300020918-1_7_3
			product	hypothetical protein
			transl_table	11
1516	1821	CDS
			locus_tag	3300020918-1_7_4
			product	hypothetical protein
			transl_table	11
1840	2238	CDS
			locus_tag	3300020918-1_7_5
			product	hypothetical protein
			transl_table	11
2462	2911	CDS
			locus_tag	3300020918-1_7_6
			product	hypothetical protein
			transl_table	11
3110	3457	CDS
			locus_tag	3300020918-1_7_7
			product	hypothetical protein
			transl_table	11
4693	3539	CDS
			locus_tag	3300020918-1_7_8
			product	hypothetical protein
			transl_table	11
4700	4828	CDS
			locus_tag	3300020918-1_7_9
			product	hypothetical protein
			transl_table	11
4825	6141	CDS
			locus_tag	3300020918-1_7_10
			product	hypothetical protein OXYTRI_24530 (macronuclear)
			transl_table	11
6266	9325	CDS
			locus_tag	3300020918-1_7_11
			product	structural ppiase-like protein
			transl_table	11
9383	9826	CDS
			locus_tag	3300020918-1_7_12
			product	hypothetical protein
			transl_table	11
10307	9855	CDS
			locus_tag	3300020918-1_7_13
			product	hypothetical protein
			transl_table	11
10912	10343	CDS
			locus_tag	3300020918-1_7_14
			product	hypothetical protein
			transl_table	11
12900	10930	CDS
			locus_tag	3300020918-1_7_15
			product	ankyrin repeat protein
			transl_table	11
12974	13720	CDS
			locus_tag	3300020918-1_7_16
			product	double-stranded RNA binding motif-containing protein
			transl_table	11
14670	13777	CDS
			locus_tag	3300020918-1_7_17
			product	hypothetical protein
			transl_table	11
15726	14761	CDS
			locus_tag	3300020918-1_7_18
			product	hypothetical protein
			transl_table	11
15885	19187	CDS
			locus_tag	3300020918-1_7_19
			product	ankyrin repeat-containing protein
			transl_table	11
19299	20498	CDS
			locus_tag	3300020918-1_7_20
			product	hypothetical protein
			transl_table	11
20665	20817	CDS
			locus_tag	3300020918-1_7_21
			product	hypothetical protein
			transl_table	11
>3300020918-1_8
1	180	CDS
			locus_tag	3300020918-1_8_1
			product	hypothetical protein
			transl_table	11
634	230	CDS
			locus_tag	3300020918-1_8_2
			product	hypothetical protein
			transl_table	11
952	671	CDS
			locus_tag	3300020918-1_8_3
			product	hypothetical protein
			transl_table	11
1089	2705	CDS
			locus_tag	3300020918-1_8_4
			product	hypothetical protein RclHR1_00830012
			transl_table	11
2979	2782	CDS
			locus_tag	3300020918-1_8_5
			product	hypothetical protein
			transl_table	11
3810	3043	CDS
			locus_tag	3300020918-1_8_6
			product	hypothetical protein
			transl_table	11
3941	4672	CDS
			locus_tag	3300020918-1_8_7
			product	hypothetical protein
			transl_table	11
5007	5849	CDS
			locus_tag	3300020918-1_8_8
			product	hypothetical protein
			transl_table	11
5933	8986	CDS
			locus_tag	3300020918-1_8_9
			product	DEXDc helicase
			transl_table	11
9125	9232	CDS
			locus_tag	3300020918-1_8_10
			product	hypothetical protein
			transl_table	11
9365	12391	CDS
			locus_tag	3300020918-1_8_11
			product	hypothetical protein
			transl_table	11
15172	12524	CDS
			locus_tag	3300020918-1_8_12
			product	exodeoxyribonuclease III
			transl_table	11
15173	16801	CDS
			locus_tag	3300020918-1_8_13
			product	hypothetical protein
			transl_table	11
16901	19144	CDS
			locus_tag	3300020918-1_8_14
			product	hypothetical protein
			transl_table	11
19812	19480	CDS
			locus_tag	3300020918-1_8_15
			product	hypothetical protein
			transl_table	11
>3300020918-1_9
120	722	CDS
			locus_tag	3300020918-1_9_1
			product	hypothetical protein
			transl_table	11
786	2018	CDS
			locus_tag	3300020918-1_9_2
			product	hypothetical protein
			transl_table	11
2113	3081	CDS
			locus_tag	3300020918-1_9_3
			product	hypothetical protein
			transl_table	11
4030	3143	CDS
			locus_tag	3300020918-1_9_4
			product	peptide chain release factor ERF1
			transl_table	11
4277	4164	CDS
			locus_tag	3300020918-1_9_5
			product	hypothetical protein
			transl_table	11
4404	5177	CDS
			locus_tag	3300020918-1_9_6
			product	hypothetical protein
			transl_table	11
5280	5702	CDS
			locus_tag	3300020918-1_9_7
			product	hypothetical protein
			transl_table	11
5773	6954	CDS
			locus_tag	3300020918-1_9_8
			product	hypothetical protein
			transl_table	11
7152	7370	CDS
			locus_tag	3300020918-1_9_9
			product	hypothetical protein
			transl_table	11
8102	8776	CDS
			locus_tag	3300020918-1_9_10
			product	hypothetical protein
			transl_table	11
8788	9327	CDS
			locus_tag	3300020918-1_9_11
			product	Cu-Zn superoxide dismutase
			transl_table	11
9413	9324	CDS
			locus_tag	3300020918-1_9_12
			product	hypothetical protein
			transl_table	11
9428	10735	CDS
			locus_tag	3300020918-1_9_13
			product	tyrosine-tRNA synthetase
			transl_table	11
11406	10783	CDS
			locus_tag	3300020918-1_9_14
			product	hypothetical protein
			transl_table	11
11548	14505	CDS
			locus_tag	3300020918-1_9_15
			product	hypothetical protein
			transl_table	11
15201	14533	CDS
			locus_tag	3300020918-1_9_16
			product	hypothetical protein
			transl_table	11
15670	15284	CDS
			locus_tag	3300020918-1_9_17
			product	hypothetical protein
			transl_table	11
16384	15830	CDS
			locus_tag	3300020918-1_9_18
			product	hypothetical protein
			transl_table	11
17334	16504	CDS
			locus_tag	3300020918-1_9_19
			product	hypothetical protein
			transl_table	11
17496	17356	CDS
			locus_tag	3300020918-1_9_20
			product	hypothetical protein
			transl_table	11
17483	18697	CDS
			locus_tag	3300020918-1_9_21
			product	hypothetical protein
			transl_table	11
19632	18751	CDS
			locus_tag	3300020918-1_9_22
			product	hypothetical protein Klosneuvirus_1_404
			transl_table	11
>3300020918-1_10
3	101	CDS
			locus_tag	3300020918-1_10_1
			product	hypothetical protein
			transl_table	11
1365	139	CDS
			locus_tag	3300020918-1_10_2
			product	hypothetical protein
			transl_table	11
1642	2013	CDS
			locus_tag	3300020918-1_10_3
			product	hypothetical protein c7_R1072
			transl_table	11
2774	2046	CDS
			locus_tag	3300020918-1_10_4
			product	hypothetical protein
			transl_table	11
2902	3369	CDS
			locus_tag	3300020918-1_10_5
			product	hypothetical protein
			transl_table	11
4562	6694	CDS
			locus_tag	3300020918-1_10_6
			product	putative transposase
			transl_table	11
7155	6850	CDS
			locus_tag	3300020918-1_10_7
			product	hypothetical protein
			transl_table	11
7819	7217	CDS
			locus_tag	3300020918-1_10_8
			product	hypothetical protein
			transl_table	11
8383	7880	CDS
			locus_tag	3300020918-1_10_9
			product	hypothetical protein
			transl_table	11
8885	8532	CDS
			locus_tag	3300020918-1_10_10
			product	hypothetical protein
			transl_table	11
9538	9020	CDS
			locus_tag	3300020918-1_10_11
			product	hypothetical protein
			transl_table	11
10243	9881	CDS
			locus_tag	3300020918-1_10_12
			product	hypothetical protein
			transl_table	11
10407	10958	CDS
			locus_tag	3300020918-1_10_13
			product	hypothetical protein
			transl_table	11
11527	11048	CDS
			locus_tag	3300020918-1_10_14
			product	hypothetical protein
			transl_table	11
11950	11627	CDS
			locus_tag	3300020918-1_10_15
			product	hypothetical protein
			transl_table	11
12397	12074	CDS
			locus_tag	3300020918-1_10_16
			product	hypothetical protein
			transl_table	11
12798	12457	CDS
			locus_tag	3300020918-1_10_17
			product	hypothetical protein
			transl_table	11
14253	13105	CDS
			locus_tag	3300020918-1_10_18
			product	hypothetical protein
			transl_table	11
14612	14370	CDS
			locus_tag	3300020918-1_10_19
			product	hypothetical protein
			transl_table	11
14781	16013	CDS
			locus_tag	3300020918-1_10_20
			product	hypothetical protein
			transl_table	11
16407	16075	CDS
			locus_tag	3300020918-1_10_21
			product	hypothetical protein
			transl_table	11
17064	16417	CDS
			locus_tag	3300020918-1_10_22
			product	isoprenylcysteine carboxyl methyltransferase
			transl_table	11
17682	17212	CDS
			locus_tag	3300020918-1_10_23
			product	hypothetical protein
			transl_table	11
17850	17740	CDS
			locus_tag	3300020918-1_10_24
			product	hypothetical protein
			transl_table	11
18141	17887	CDS
			locus_tag	3300020918-1_10_25
			product	hypothetical protein CBB97_22660
			transl_table	11
18743	18495	CDS
			locus_tag	3300020918-1_10_26
			product	hypothetical protein
			transl_table	11
>3300020918-1_11
3	1481	CDS
			locus_tag	3300020918-1_11_1
			product	hypothetical protein
			transl_table	11
1559	2203	CDS
			locus_tag	3300020918-1_11_2
			product	mg356 protein
			transl_table	11
2641	2186	CDS
			locus_tag	3300020918-1_11_3
			product	putative ORFan
			transl_table	11
4457	2724	CDS
			locus_tag	3300020918-1_11_4
			product	hypothetical protein
			transl_table	11
5276	4584	CDS
			locus_tag	3300020918-1_11_5
			product	hypothetical protein
			transl_table	11
5678	9103	CDS
			locus_tag	3300020918-1_11_6
			product	lon protease-like protein
			transl_table	11
9136	9993	CDS
			locus_tag	3300020918-1_11_7
			product	hypothetical protein
			transl_table	11
10060	11460	CDS
			locus_tag	3300020918-1_11_8
			product	putative transcription initiation factor IIb-like protein
			transl_table	11
12865	11528	CDS
			locus_tag	3300020918-1_11_9
			product	hypothetical protein
			transl_table	11
14044	12953	CDS
			locus_tag	3300020918-1_11_10
			product	hypothetical protein
			transl_table	11
14249	15352	CDS
			locus_tag	3300020918-1_11_11
			product	putative uracil-DNA glycosylase
			transl_table	11
>3300020918-1_12
12235	12151	tRNA	3300020918-1_tRNA_1
			product	tRNA-Ser(GCT)
12308	12237	tRNA	3300020918-1_tRNA_2
			product	tRNA-Gln(TTG)
12425	12342	tRNA	3300020918-1_tRNA_3
			product	tRNA-Ser(TGA)
12557	12471	tRNA	3300020918-1_tRNA_4
			product	tRNA-Ser(AGA)
12747	12664	tRNA	3300020918-1_tRNA_5
			product	tRNA-Ser(CGA)
12820	12749	tRNA	3300020918-1_tRNA_6
			product	tRNA-Gln(CTG)
144	1	CDS
			locus_tag	3300020918-1_12_1
			product	hypothetical protein
			transl_table	11
344	1987	CDS
			locus_tag	3300020918-1_12_2
			product	hypothetical protein
			transl_table	11
3391	2048	CDS
			locus_tag	3300020918-1_12_3
			product	hypothetical protein
			transl_table	11
3811	3515	CDS
			locus_tag	3300020918-1_12_4
			product	hypothetical protein
			transl_table	11
5991	3826	CDS
			locus_tag	3300020918-1_12_5
			product	hypothetical protein GUITHDRAFT_122707
			transl_table	11
6151	7044	CDS
			locus_tag	3300020918-1_12_6
			product	hypothetical protein
			transl_table	11
9119	7074	CDS
			locus_tag	3300020918-1_12_7
			product	putative ATP-dependent RNA helicase
			transl_table	11
9141	9254	CDS
			locus_tag	3300020918-1_12_8
			product	hypothetical protein
			transl_table	11
10378	9251	CDS
			locus_tag	3300020918-1_12_9
			product	hypothetical protein
			transl_table	11
10563	11306	CDS
			locus_tag	3300020918-1_12_10
			product	hypothetical protein
			transl_table	11
11435	12022	CDS
			locus_tag	3300020918-1_12_11
			product	hypothetical protein LBA_00692
			transl_table	11
12300	12419	CDS
			locus_tag	3300020918-1_12_12
			product	hypothetical protein
			transl_table	11
12622	12717	CDS
			locus_tag	3300020918-1_12_13
			product	hypothetical protein
			transl_table	11
14194	12914	CDS
			locus_tag	3300020918-1_12_14
			product	putative ORFan
			transl_table	11
>3300020918-1_13
484	20	CDS
			locus_tag	3300020918-1_13_1
			product	hypothetical protein
			transl_table	11
2831	690	CDS
			locus_tag	3300020918-1_13_2
			product	DHH family phosphohydrolase-like protein
			transl_table	11
7243	2885	CDS
			locus_tag	3300020918-1_13_3
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
7422	10244	CDS
			locus_tag	3300020918-1_13_4
			product	putative BTB/POZ domain-containing protein
			transl_table	11
11096	10254	CDS
			locus_tag	3300020918-1_13_5
			product	hypothetical protein
			transl_table	11
13237	11150	CDS
			locus_tag	3300020918-1_13_6
			product	putative zinc metalloproteinase
			transl_table	11
13574	13332	CDS
			locus_tag	3300020918-1_13_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_14
350	27	CDS
			locus_tag	3300020918-1_14_1
			product	hypothetical protein
			transl_table	11
845	384	CDS
			locus_tag	3300020918-1_14_2
			product	hypothetical protein
			transl_table	11
1424	909	CDS
			locus_tag	3300020918-1_14_3
			product	hypothetical protein
			transl_table	11
2066	1488	CDS
			locus_tag	3300020918-1_14_4
			product	hypothetical protein
			transl_table	11
2784	2104	CDS
			locus_tag	3300020918-1_14_5
			product	Rab family GTPase
			transl_table	11
2935	3885	CDS
			locus_tag	3300020918-1_14_6
			product	hypothetical protein
			transl_table	11
3979	5058	CDS
			locus_tag	3300020918-1_14_7
			product	UDP-glucose 4-epimerase
			transl_table	11
5167	5637	CDS
			locus_tag	3300020918-1_14_8
			product	hypothetical protein
			transl_table	11
7438	5594	CDS
			locus_tag	3300020918-1_14_9
			product	hypothetical protein
			transl_table	11
7519	8394	CDS
			locus_tag	3300020918-1_14_10
			product	hypothetical protein glt_00708
			transl_table	11
9586	8417	CDS
			locus_tag	3300020918-1_14_11
			product	hypothetical protein
			transl_table	11
9682	11019	CDS
			locus_tag	3300020918-1_14_12
			product	RNA ligase 2
			transl_table	11
11920	11048	CDS
			locus_tag	3300020918-1_14_13
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
12083	12664	CDS
			locus_tag	3300020918-1_14_14
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
13305	13571	CDS
			locus_tag	3300020918-1_14_15
			product	putative helicase
			transl_table	11
>3300020918-1_15
3	731	CDS
			locus_tag	3300020918-1_15_1
			product	hypothetical protein
			transl_table	11
828	4619	CDS
			locus_tag	3300020918-1_15_2
			product	glycosyltransferase family 2
			transl_table	11
4772	6097	CDS
			locus_tag	3300020918-1_15_3
			product	hypothetical protein
			transl_table	11
6614	6132	CDS
			locus_tag	3300020918-1_15_4
			product	hypothetical protein
			transl_table	11
6653	6772	CDS
			locus_tag	3300020918-1_15_5
			product	hypothetical protein
			transl_table	11
6760	8391	CDS
			locus_tag	3300020918-1_15_6
			product	AAA family ATPase
			transl_table	11
9636	8440	CDS
			locus_tag	3300020918-1_15_7
			product	putative orfan
			transl_table	11
9780	11141	CDS
			locus_tag	3300020918-1_15_8
			product	hypothetical protein
			transl_table	11
11208	11852	CDS
			locus_tag	3300020918-1_15_9
			product	unnamed protein product, partial
			transl_table	11
12385	11900	CDS
			locus_tag	3300020918-1_15_10
			product	hypothetical protein
			transl_table	11
13177	12461	CDS
			locus_tag	3300020918-1_15_11
			product	predicted protein
			transl_table	11
>3300020918-1_16
1	144	CDS
			locus_tag	3300020918-1_16_1
			product	hypothetical protein
			transl_table	11
1207	215	CDS
			locus_tag	3300020918-1_16_2
			product	hypothetical protein
			transl_table	11
5439	1315	CDS
			locus_tag	3300020918-1_16_3
			product	putative DNA repair protein
			transl_table	11
6589	5543	CDS
			locus_tag	3300020918-1_16_4
			product	hypothetical protein
			transl_table	11
8498	6723	CDS
			locus_tag	3300020918-1_16_5
			product	hypothetical protein
			transl_table	11
9072	8632	CDS
			locus_tag	3300020918-1_16_6
			product	mg347 protein
			transl_table	11
10428	9124	CDS
			locus_tag	3300020918-1_16_7
			product	ankyrin repeat protein
			transl_table	11
10564	10878	CDS
			locus_tag	3300020918-1_16_8
			product	hypothetical protein
			transl_table	11
11732	11496	CDS
			locus_tag	3300020918-1_16_9
			product	hypothetical protein
			transl_table	11
>3300020918-1_17
35	859	CDS
			locus_tag	3300020918-1_17_1
			product	hypothetical protein
			transl_table	11
897	2873	CDS
			locus_tag	3300020918-1_17_2
			product	serine/threonine-protein kinase Nek1
			transl_table	11
2892	3443	CDS
			locus_tag	3300020918-1_17_3
			product	hypothetical protein
			transl_table	11
3492	4112	CDS
			locus_tag	3300020918-1_17_4
			product	hypothetical protein
			transl_table	11
4180	5172	CDS
			locus_tag	3300020918-1_17_5
			product	hypothetical protein
			transl_table	11
5319	7907	CDS
			locus_tag	3300020918-1_17_6
			product	topoisomerase I
			transl_table	11
8015	10270	CDS
			locus_tag	3300020918-1_17_7
			product	hypothetical protein
			transl_table	11
11051	10365	CDS
			locus_tag	3300020918-1_17_8
			product	class I SAM-dependent methyltransferase
			transl_table	11
>3300020918-1_18
218	3	CDS
			locus_tag	3300020918-1_18_1
			product	hypothetical protein
			transl_table	11
598	311	CDS
			locus_tag	3300020918-1_18_2
			product	hypothetical protein
			transl_table	11
712	1002	CDS
			locus_tag	3300020918-1_18_3
			product	hypothetical protein
			transl_table	11
2536	1064	CDS
			locus_tag	3300020918-1_18_4
			product	Flotillin domain-containing protein
			transl_table	11
2757	4058	CDS
			locus_tag	3300020918-1_18_5
			product	hypothetical protein
			transl_table	11
4242	4412	CDS
			locus_tag	3300020918-1_18_6
			product	hypothetical protein
			transl_table	11
4466	6106	CDS
			locus_tag	3300020918-1_18_7
			product	hypothetical protein
			transl_table	11
6160	7161	CDS
			locus_tag	3300020918-1_18_8
			product	hypothetical protein crov369
			transl_table	11
7574	7206	CDS
			locus_tag	3300020918-1_18_9
			product	hypothetical protein
			transl_table	11
7921	7637	CDS
			locus_tag	3300020918-1_18_10
			product	hypothetical protein
			transl_table	11
8850	7951	CDS
			locus_tag	3300020918-1_18_11
			product	hypothetical protein
			transl_table	11
10217	8982	CDS
			locus_tag	3300020918-1_18_12
			product	hypothetical protein
			transl_table	11
>3300020918-1_19
555	1	CDS
			locus_tag	3300020918-1_19_1
			product	putative flap endonuclease 1-like
			transl_table	11
798	1199	CDS
			locus_tag	3300020918-1_19_2
			product	hypothetical protein
			transl_table	11
1487	1813	CDS
			locus_tag	3300020918-1_19_3
			product	hypothetical protein MegaChil _gp0368
			transl_table	11
3126	1870	CDS
			locus_tag	3300020918-1_19_4
			product	hypothetical protein
			transl_table	11
4725	3283	CDS
			locus_tag	3300020918-1_19_5
			product	hypothetical protein
			transl_table	11
4919	6322	CDS
			locus_tag	3300020918-1_19_6
			product	hypothetical protein
			transl_table	11
9430	6377	CDS
			locus_tag	3300020918-1_19_7
			product	DNA-directed RNA polymerase subunit 1
			transl_table	11
>3300020918-1_20
3452	3	CDS
			locus_tag	3300020918-1_20_1
			product	hypothetical protein
			transl_table	11
3551	4285	CDS
			locus_tag	3300020918-1_20_2
			product	Phosphoribosyl-ATP pyrophosphohydrolase-domain containing protein (two domains)
			transl_table	11
4333	4770	CDS
			locus_tag	3300020918-1_20_3
			product	putative orfan
			transl_table	11
5756	4860	CDS
			locus_tag	3300020918-1_20_4
			product	hypothetical protein
			transl_table	11
6974	5844	CDS
			locus_tag	3300020918-1_20_5
			product	hypothetical protein
			transl_table	11
8280	7135	CDS
			locus_tag	3300020918-1_20_6
			product	hypothetical protein
			transl_table	11
8412	9449	CDS
			locus_tag	3300020918-1_20_7
			product	putative replication factor C small subunit
			transl_table	11
>3300020918-1_21
454	2	CDS
			locus_tag	3300020918-1_21_1
			product	hypothetical protein
			transl_table	11
1083	565	CDS
			locus_tag	3300020918-1_21_2
			product	hypothetical protein
			transl_table	11
2272	1208	CDS
			locus_tag	3300020918-1_21_3
			product	hypothetical protein
			transl_table	11
2480	4012	CDS
			locus_tag	3300020918-1_21_4
			product	putative ORFan
			transl_table	11
4708	4115	CDS
			locus_tag	3300020918-1_21_5
			product	hypothetical protein
			transl_table	11
4972	4748	CDS
			locus_tag	3300020918-1_21_6
			product	hypothetical protein
			transl_table	11
5036	6829	CDS
			locus_tag	3300020918-1_21_7
			product	mg301 protein
			transl_table	11
8319	6859	CDS
			locus_tag	3300020918-1_21_8
			product	hypothetical protein
			transl_table	11
8821	8615	CDS
			locus_tag	3300020918-1_21_9
			product	hypothetical protein
			transl_table	11
9337	9104	CDS
			locus_tag	3300020918-1_21_10
			product	putative serine threonine protein kinase
			transl_table	11
>3300020918-1_22
3	866	CDS
			locus_tag	3300020918-1_22_1
			product	putative FtsJ-like methyltransferase
			transl_table	11
2149	1028	CDS
			locus_tag	3300020918-1_22_2
			product	putative replication factor c small subunit
			transl_table	11
4279	2270	CDS
			locus_tag	3300020918-1_22_3
			product	hypothetical protein
			transl_table	11
4893	4360	CDS
			locus_tag	3300020918-1_22_4
			product	hypothetical protein
			transl_table	11
5040	6101	CDS
			locus_tag	3300020918-1_22_5
			product	hypothetical protein
			transl_table	11
7797	6139	CDS
			locus_tag	3300020918-1_22_6
			product	putative ATP-dependent RNA helicase
			transl_table	11
8531	7890	CDS
			locus_tag	3300020918-1_22_7
			product	hypothetical protein
			transl_table	11
9294	8584	CDS
			locus_tag	3300020918-1_22_8
			product	hypothetical protein
			transl_table	11
>3300020918-1_23
1	981	CDS
			locus_tag	3300020918-1_23_1
			product	flavin containing amine oxidoreductase
			transl_table	11
1388	1026	CDS
			locus_tag	3300020918-1_23_2
			product	glyoxalase
			transl_table	11
1773	1495	CDS
			locus_tag	3300020918-1_23_3
			product	hypothetical protein
			transl_table	11
4015	1874	CDS
			locus_tag	3300020918-1_23_4
			product	putative lanosterol 14-alpha demethylase
			transl_table	11
4523	4005	CDS
			locus_tag	3300020918-1_23_5
			product	Fe-S cluster assembly protein hesB
			transl_table	11
5968	4541	CDS
			locus_tag	3300020918-1_23_6
			product	putative BTB/POZ domain and WD-repeat protein
			transl_table	11
6096	6740	CDS
			locus_tag	3300020918-1_23_7
			product	hypothetical protein
			transl_table	11
8031	6895	CDS
			locus_tag	3300020918-1_23_8
			product	hypothetical protein
			transl_table	11
8368	9135	CDS
			locus_tag	3300020918-1_23_9
			product	hypothetical protein
			transl_table	11
>3300020918-1_24
3553	3481	tRNA	3300020918-1_tRNA_7
			product	tRNA-Glu(CTC)
3625	3555	tRNA	3300020918-1_tRNA_8
			product	tRNA-Glu(TTC)
3738	3665	tRNA	3300020918-1_tRNA_9
			product	tRNA-Pseudo(GTC)
3810	3740	tRNA	3300020918-1_tRNA_10
			product	tRNA-Gly(TCC)
5981	5911	tRNA	3300020918-1_tRNA_11
			product	tRNA-Gly(GCC)
6175	6104	tRNA	3300020918-1_tRNA_12
			product	tRNA-Pseudo(CAT)
6450	6381	tRNA	3300020918-1_tRNA_13
			product	tRNA-Tyr(GTA)
6582	6512	tRNA	3300020918-1_tRNA_14
			product	tRNA-Asn(GTT)
44	1273	CDS
			locus_tag	3300020918-1_24_1
			product	hypothetical protein
			transl_table	11
1951	1373	CDS
			locus_tag	3300020918-1_24_2
			product	hypothetical protein
			transl_table	11
3249	2194	CDS
			locus_tag	3300020918-1_24_3
			product	hypothetical protein
			transl_table	11
5554	3857	CDS
			locus_tag	3300020918-1_24_4
			product	hypothetical protein Klosneuvirus_1_324
			transl_table	11
6677	7066	CDS
			locus_tag	3300020918-1_24_5
			product	hypothetical protein
			transl_table	11
7185	7790	CDS
			locus_tag	3300020918-1_24_6
			product	hypothetical protein
			transl_table	11
7915	8370	CDS
			locus_tag	3300020918-1_24_7
			product	hypothetical protein
			transl_table	11
8456	8995	CDS
			locus_tag	3300020918-1_24_8
			product	hypothetical protein
			transl_table	11
9118	8966	CDS
			locus_tag	3300020918-1_24_9
			product	hypothetical protein
			transl_table	11
>3300020918-1_25
48	2246	CDS
			locus_tag	3300020918-1_25_1
			product	ankyrin repeat protein
			transl_table	11
2360	3082	CDS
			locus_tag	3300020918-1_25_2
			product	hypothetical protein
			transl_table	11
3127	4599	CDS
			locus_tag	3300020918-1_25_3
			product	hypothetical protein
			transl_table	11
5092	4679	CDS
			locus_tag	3300020918-1_25_4
			product	hypothetical protein
			transl_table	11
6228	5152	CDS
			locus_tag	3300020918-1_25_5
			product	DNA polymerase family X
			transl_table	11
6391	7077	CDS
			locus_tag	3300020918-1_25_6
			product	hypothetical protein
			transl_table	11
8757	7123	CDS
			locus_tag	3300020918-1_25_7
			product	ABC transporter ATP-binding domain protein
			transl_table	11
9024	8860	CDS
			locus_tag	3300020918-1_25_8
			product	hypothetical protein
			transl_table	11
>3300020918-1_26
142	1734	CDS
			locus_tag	3300020918-1_26_1
			product	putative transposase/mobile element protein
			transl_table	11
2064	3341	CDS
			locus_tag	3300020918-1_26_2
			product	hypothetical protein
			transl_table	11
5310	3394	CDS
			locus_tag	3300020918-1_26_3
			product	NAD-dependent DNA ligase
			transl_table	11
6241	5405	CDS
			locus_tag	3300020918-1_26_4
			product	putative ORFan
			transl_table	11
7448	6234	CDS
			locus_tag	3300020918-1_26_5
			product	putative serine threonine protein kinase
			transl_table	11
7474	7965	CDS
			locus_tag	3300020918-1_26_6
			product	putative endo/excinuclease amino terminal domain protein
			transl_table	11
8671	7973	CDS
			locus_tag	3300020918-1_26_7
			product	ribonuclease H
			transl_table	11
8904	8779	CDS
			locus_tag	3300020918-1_26_8
			product	hypothetical protein
			transl_table	11
>3300020918-1_27
1669	119	CDS
			locus_tag	3300020918-1_27_1
			product	YqaJ viral recombinase family protein
			transl_table	11
1791	2750	CDS
			locus_tag	3300020918-1_27_2
			product	thiol protease
			transl_table	11
4863	2872	CDS
			locus_tag	3300020918-1_27_3
			product	putative low complexity protein
			transl_table	11
4884	5138	CDS
			locus_tag	3300020918-1_27_4
			product	hypothetical protein
			transl_table	11
6606	5119	CDS
			locus_tag	3300020918-1_27_5
			product	hypothetical protein
			transl_table	11
7901	6765	CDS
			locus_tag	3300020918-1_27_6
			product	putative ORFan
			transl_table	11
8050	8514	CDS
			locus_tag	3300020918-1_27_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_28
291	1	CDS
			locus_tag	3300020918-1_28_1
			product	hypothetical protein
			transl_table	11
779	318	CDS
			locus_tag	3300020918-1_28_2
			product	hypothetical protein
			transl_table	11
1582	905	CDS
			locus_tag	3300020918-1_28_3
			product	hypothetical protein
			transl_table	11
1737	2861	CDS
			locus_tag	3300020918-1_28_4
			product	hypothetical protein
			transl_table	11
3349	2954	CDS
			locus_tag	3300020918-1_28_5
			product	intein-containing dna-directed rna polymerase subunit 2
			transl_table	11
4927	3656	CDS
			locus_tag	3300020918-1_28_6
			product	putative intron HNH endonuclease
			transl_table	11
6057	5491	CDS
			locus_tag	3300020918-1_28_7
			product	hypothetical protein
			transl_table	11
7122	6142	CDS
			locus_tag	3300020918-1_28_8
			product	DNA-directed RNA polymerase subunit 2
			transl_table	11
7719	7393	CDS
			locus_tag	3300020918-1_28_9
			product	hypothetical protein
			transl_table	11
8236	7799	CDS
			locus_tag	3300020918-1_28_10
			product	DNA-directed RNA polymerase subunit 2
			transl_table	11
>3300020918-1_29
3997	2	CDS
			locus_tag	3300020918-1_29_1
			product	hypothetical protein
			transl_table	11
8205	4129	CDS
			locus_tag	3300020918-1_29_2
			product	hypothetical protein
			transl_table	11
>3300020918-1_30
2	238	CDS
			locus_tag	3300020918-1_30_1
			product	hypothetical protein
			transl_table	11
1165	284	CDS
			locus_tag	3300020918-1_30_2
			product	hypothetical protein
			transl_table	11
6473	1248	CDS
			locus_tag	3300020918-1_30_3
			product	hypothetical protein
			transl_table	11
6504	7889	CDS
			locus_tag	3300020918-1_30_4
			product	hypothetical protein
			transl_table	11
>3300020918-1_31
733	2	CDS
			locus_tag	3300020918-1_31_1
			product	hypothetical protein
			transl_table	11
899	1873	CDS
			locus_tag	3300020918-1_31_2
			product	putative RNA methylase
			transl_table	11
1948	3042	CDS
			locus_tag	3300020918-1_31_3
			product	hypothetical protein
			transl_table	11
4737	3112	CDS
			locus_tag	3300020918-1_31_4
			product	putative ubiquitin-specific protease
			transl_table	11
4896	6437	CDS
			locus_tag	3300020918-1_31_5
			product	hypothetical protein
			transl_table	11
7163	6837	CDS
			locus_tag	3300020918-1_31_6
			product	hypothetical protein
			transl_table	11
7346	7200	CDS
			locus_tag	3300020918-1_31_7
			product	hypothetical protein
			transl_table	11
7345	7584	CDS
			locus_tag	3300020918-1_31_8
			product	hypothetical protein
			transl_table	11
>3300020918-1_32
3	113	CDS
			locus_tag	3300020918-1_32_1
			product	hypothetical protein
			transl_table	11
338	156	CDS
			locus_tag	3300020918-1_32_2
			product	hypothetical protein
			transl_table	11
490	341	CDS
			locus_tag	3300020918-1_32_3
			product	hypothetical protein
			transl_table	11
732	610	CDS
			locus_tag	3300020918-1_32_4
			product	hypothetical protein
			transl_table	11
1657	800	CDS
			locus_tag	3300020918-1_32_5
			product	hypothetical protein
			transl_table	11
2310	1747	CDS
			locus_tag	3300020918-1_32_6
			product	hypothetical protein
			transl_table	11
2580	2449	CDS
			locus_tag	3300020918-1_32_7
			product	hypothetical protein
			transl_table	11
3372	3151	CDS
			locus_tag	3300020918-1_32_8
			product	hypothetical protein
			transl_table	11
4015	3512	CDS
			locus_tag	3300020918-1_32_9
			product	hypothetical protein
			transl_table	11
4765	4250	CDS
			locus_tag	3300020918-1_32_10
			product	hypothetical protein
			transl_table	11
6113	5088	CDS
			locus_tag	3300020918-1_32_11
			product	hypothetical protein PBRA_008189
			transl_table	11
6663	6475	CDS
			locus_tag	3300020918-1_32_12
			product	hypothetical protein
			transl_table	11
7047	6781	CDS
			locus_tag	3300020918-1_32_13
			product	hypothetical protein
			transl_table	11
7502	7017	CDS
			locus_tag	3300020918-1_32_14
			product	hypothetical protein
			transl_table	11
>3300020918-1_33
1	249	CDS
			locus_tag	3300020918-1_33_1
			product	mg424 protein
			transl_table	11
309	917	CDS
			locus_tag	3300020918-1_33_2
			product	hypothetical protein MegaChil _gp0426
			transl_table	11
1011	1532	CDS
			locus_tag	3300020918-1_33_3
			product	putative ubiquitin-conjugating enzyme E2
			transl_table	11
2252	1782	CDS
			locus_tag	3300020918-1_33_4
			product	hypothetical protein
			transl_table	11
3262	2450	CDS
			locus_tag	3300020918-1_33_5
			product	DEAD/SNF2-like helicase
			transl_table	11
4277	3546	CDS
			locus_tag	3300020918-1_33_6
			product	mimivirus translation initiation factor 4a
			transl_table	11
5050	4448	CDS
			locus_tag	3300020918-1_33_7
			product	hypothetical protein
			transl_table	11
5837	5112	CDS
			locus_tag	3300020918-1_33_8
			product	hypothetical protein
			transl_table	11
7148	7342	CDS
			locus_tag	3300020918-1_33_9
			product	putative pore coat assembly factor
			transl_table	11
>3300020918-1_34
1818	118	CDS
			locus_tag	3300020918-1_34_1
			product	polyA polymerase catalitic subunit
			transl_table	11
1896	2735	CDS
			locus_tag	3300020918-1_34_2
			product	hypothetical protein
			transl_table	11
2817	3527	CDS
			locus_tag	3300020918-1_34_3
			product	heme oxygenase
			transl_table	11
4092	3577	CDS
			locus_tag	3300020918-1_34_4
			product	transcription factor S-II-related protein
			transl_table	11
7092	4156	CDS
			locus_tag	3300020918-1_34_5
			product	hypothetical protein
			transl_table	11
7305	7207	CDS
			locus_tag	3300020918-1_34_6
			product	hypothetical protein
			transl_table	11
>3300020918-1_35
204	1	CDS
			locus_tag	3300020918-1_35_1
			product	hypothetical protein
			transl_table	11
855	274	CDS
			locus_tag	3300020918-1_35_2
			product	hypothetical protein
			transl_table	11
1051	1494	CDS
			locus_tag	3300020918-1_35_3
			product	hypothetical protein
			transl_table	11
1445	2677	CDS
			locus_tag	3300020918-1_35_4
			product	putative DNA polymerase sliding clamp
			transl_table	11
2722	3165	CDS
			locus_tag	3300020918-1_35_5
			product	hypothetical protein
			transl_table	11
3735	3193	CDS
			locus_tag	3300020918-1_35_6
			product	hypothetical protein
			transl_table	11
4778	3858	CDS
			locus_tag	3300020918-1_35_7
			product	translation initiation factor 4E
			transl_table	11
6007	4856	CDS
			locus_tag	3300020918-1_35_8
			product	replication factor C small subunit
			transl_table	11
6738	6118	CDS
			locus_tag	3300020918-1_35_9
			product	hypothetical protein
			transl_table	11
>3300020918-1_36
361	2	CDS
			locus_tag	3300020918-1_36_1
			product	putative FAD-linked sulfhydryl oxidase
			transl_table	11
2326	800	CDS
			locus_tag	3300020918-1_36_2
			product	hypothetical protein
			transl_table	11
5571	2464	CDS
			locus_tag	3300020918-1_36_3
			product	putative ATP-dependent RNA helicase
			transl_table	11
6257	5547	CDS
			locus_tag	3300020918-1_36_4
			product	putative ATP-dependent RNA helicase
			transl_table	11
>3300020918-1_37
2460	1	CDS
			locus_tag	3300020918-1_37_1
			product	hypothetical protein
			transl_table	11
2538	2810	CDS
			locus_tag	3300020918-1_37_2
			product	hypothetical protein
			transl_table	11
3782	2853	CDS
			locus_tag	3300020918-1_37_3
			product	Proliferating cell nuclear antigen
			transl_table	11
4695	3841	CDS
			locus_tag	3300020918-1_37_4
			product	ATP-grasp domain protein
			transl_table	11
5095	4739	CDS
			locus_tag	3300020918-1_37_5
			product	hypothetical protein
			transl_table	11
5849	5220	CDS
			locus_tag	3300020918-1_37_6
			product	hypothetical protein
			transl_table	11
>3300020918-1_38
2	640	CDS
			locus_tag	3300020918-1_38_1
			product	RecD-like DNA helicase
			transl_table	11
706	1353	CDS
			locus_tag	3300020918-1_38_2
			product	hypothetical protein
			transl_table	11
1442	2488	CDS
			locus_tag	3300020918-1_38_3
			product	hypothetical protein
			transl_table	11
2605	3405	CDS
			locus_tag	3300020918-1_38_4
			product	hypothetical protein
			transl_table	11
3501	4544	CDS
			locus_tag	3300020918-1_38_5
			product	hypothetical protein
			transl_table	11
4642	5637	CDS
			locus_tag	3300020918-1_38_6
			product	hypothetical protein
			transl_table	11
5842	5678	CDS
			locus_tag	3300020918-1_38_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_39
104	397	CDS
			locus_tag	3300020918-1_39_1
			product	hypothetical protein
			transl_table	11
464	967	CDS
			locus_tag	3300020918-1_39_2
			product	putative orfan
			transl_table	11
995	1675	CDS
			locus_tag	3300020918-1_39_3
			product	hypothetical protein
			transl_table	11
1783	2790	CDS
			locus_tag	3300020918-1_39_4
			product	mg394 protein
			transl_table	11
2847	3383	CDS
			locus_tag	3300020918-1_39_5
			product	hypothetical protein
			transl_table	11
4000	3443	CDS
			locus_tag	3300020918-1_39_6
			product	hypothetical protein
			transl_table	11
5462	5728	CDS
			locus_tag	3300020918-1_39_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_40
2348	3	CDS
			locus_tag	3300020918-1_40_1
			product	DEAD-like helicase
			transl_table	11
2488	2991	CDS
			locus_tag	3300020918-1_40_2
			product	hypothetical protein
			transl_table	11
3079	4455	CDS
			locus_tag	3300020918-1_40_3
			product	hypothetical protein
			transl_table	11
4542	5015	CDS
			locus_tag	3300020918-1_40_4
			product	hypothetical protein
			transl_table	11
5395	5057	CDS
			locus_tag	3300020918-1_40_5
			product	putative resolvase
			transl_table	11
5690	5430	CDS
			locus_tag	3300020918-1_40_6
			product	putative transposase
			transl_table	11
>3300020918-1_41
22	1092	CDS
			locus_tag	3300020918-1_41_1
			product	hypothetical protein
			transl_table	11
1487	1194	CDS
			locus_tag	3300020918-1_41_2
			product	hypothetical protein
			transl_table	11
2067	1492	CDS
			locus_tag	3300020918-1_41_3
			product	hypothetical protein
			transl_table	11
3546	2482	CDS
			locus_tag	3300020918-1_41_4
			product	hypothetical protein
			transl_table	11
5660	3789	CDS
			locus_tag	3300020918-1_41_5
			product	superfamily II helicase
			transl_table	11
>3300020918-1_42
2	487	CDS
			locus_tag	3300020918-1_42_1
			product	DEAD-like helicase
			transl_table	11
542	1696	CDS
			locus_tag	3300020918-1_42_2
			product	hypothetical protein
			transl_table	11
2513	1731	CDS
			locus_tag	3300020918-1_42_3
			product	hypothetical protein
			transl_table	11
3475	2696	CDS
			locus_tag	3300020918-1_42_4
			product	hypothetical protein
			transl_table	11
4599	3598	CDS
			locus_tag	3300020918-1_42_5
			product	hypothetical protein
			transl_table	11
5233	5505	CDS
			locus_tag	3300020918-1_42_6
			product	hypothetical protein
			transl_table	11
>3300020918-1_43
710	138	CDS
			locus_tag	3300020918-1_43_1
			product	hypothetical protein
			transl_table	11
1449	637	CDS
			locus_tag	3300020918-1_43_2
			product	hypothetical protein
			transl_table	11
1668	1910	CDS
			locus_tag	3300020918-1_43_3
			product	hypothetical protein
			transl_table	11
2955	1969	CDS
			locus_tag	3300020918-1_43_4
			product	hypothetical protein
			transl_table	11
3091	2936	CDS
			locus_tag	3300020918-1_43_5
			product	hypothetical protein
			transl_table	11
3514	3092	CDS
			locus_tag	3300020918-1_43_6
			product	hypothetical protein
			transl_table	11
4956	3628	CDS
			locus_tag	3300020918-1_43_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_44
779	3	CDS
			locus_tag	3300020918-1_44_1
			product	hypothetical protein
			transl_table	11
2307	832	CDS
			locus_tag	3300020918-1_44_2
			product	hypothetical protein
			transl_table	11
3951	2434	CDS
			locus_tag	3300020918-1_44_3
			product	hypothetical protein
			transl_table	11
4635	4027	CDS
			locus_tag	3300020918-1_44_4
			product	hypothetical protein
			transl_table	11
>3300020918-1_45
3	416	CDS
			locus_tag	3300020918-1_45_1
			product	hypothetical protein
			transl_table	11
1009	566	CDS
			locus_tag	3300020918-1_45_2
			product	hypothetical protein
			transl_table	11
1127	2566	CDS
			locus_tag	3300020918-1_45_3
			product	hypothetical protein
			transl_table	11
2621	2953	CDS
			locus_tag	3300020918-1_45_4
			product	putative ORFan
			transl_table	11
3081	3362	CDS
			locus_tag	3300020918-1_45_5
			product	hypothetical protein Klosneuvirus_1_23
			transl_table	11
3432	4265	CDS
			locus_tag	3300020918-1_45_6
			product	hypothetical protein
			transl_table	11
>3300020918-1_46
1	342	CDS
			locus_tag	3300020918-1_46_1
			product	hypothetical protein
			transl_table	11
1200	415	CDS
			locus_tag	3300020918-1_46_2
			product	hypothetical protein
			transl_table	11
1962	1282	CDS
			locus_tag	3300020918-1_46_3
			product	hypothetical protein
			transl_table	11
2754	2065	CDS
			locus_tag	3300020918-1_46_4
			product	hypothetical protein
			transl_table	11
3578	2874	CDS
			locus_tag	3300020918-1_46_5
			product	hypothetical protein
			transl_table	11
3721	4038	CDS
			locus_tag	3300020918-1_46_6
			product	hypothetical protein
			transl_table	11
4361	4450	CDS
			locus_tag	3300020918-1_46_7
			product	hypothetical protein
			transl_table	11
>3300020918-1_47
168	1	CDS
			locus_tag	3300020918-1_47_1
			product	hypothetical protein
			transl_table	11
227	2653	CDS
			locus_tag	3300020918-1_47_2
			product	FtsJ-like methyltransferase
			transl_table	11
2695	3852	CDS
			locus_tag	3300020918-1_47_3
			product	FtsJ-like methyltransferase
			transl_table	11
>3300020918-1_48
1	1014	CDS
			locus_tag	3300020918-1_48_1
			product	hypothetical protein
			transl_table	11
1683	1063	CDS
			locus_tag	3300020918-1_48_2
			product	hypothetical protein
			transl_table	11
2848	1844	CDS
			locus_tag	3300020918-1_48_3
			product	beta-lactamase superfamily domain
			transl_table	11
3856	2945	CDS
			locus_tag	3300020918-1_48_4
			product	hypothetical protein
			transl_table	11
>3300020918-1_49
2	289	CDS
			locus_tag	3300020918-1_49_1
			product	putative metallopeptidase WLM
			transl_table	11
309	1148	CDS
			locus_tag	3300020918-1_49_2
			product	exodeoxyribonuclease III
			transl_table	11
1322	2599	CDS
			locus_tag	3300020918-1_49_3
			product	hypothetical protein
			transl_table	11
2963	2673	CDS
			locus_tag	3300020918-1_49_4
			product	ubiquitin-60S ribosomal protein L40-like isoform X1
			transl_table	11
>3300020918-1_50
585	46	CDS
			locus_tag	3300020918-1_50_1
			product	hypothetical protein
			transl_table	11
1726	641	CDS
			locus_tag	3300020918-1_50_2
			product	putative ORFan
			transl_table	11
1844	1695	CDS
			locus_tag	3300020918-1_50_3
			product	putative ORFan
			transl_table	11
1944	2543	CDS
			locus_tag	3300020918-1_50_4
			product	hypothetical protein
			transl_table	11
2937	2572	CDS
			locus_tag	3300020918-1_50_5
			product	hypothetical protein
			transl_table	11
>3300020918-1_51
1	486	CDS
			locus_tag	3300020918-1_51_1
			product	hypothetical protein
			transl_table	11
2042	534	CDS
			locus_tag	3300020918-1_51_2
			product	hypothetical protein
			transl_table	11
2886	2146	CDS
			locus_tag	3300020918-1_51_3
			product	NUDIX hydrolase
			transl_table	11
>3300020918-1_52
1413	1	CDS
			locus_tag	3300020918-1_52_1
			product	EAL domain-containing protein
			transl_table	11
1748	1365	CDS
			locus_tag	3300020918-1_52_2
			product	diguanylate cyclase
			transl_table	11
1879	2211	CDS
			locus_tag	3300020918-1_52_3
			product	hypothetical protein
			transl_table	11
2768	2469	CDS
			locus_tag	3300020918-1_52_4
			product	hypothetical protein COT85_04035
			transl_table	11
>3300020918-1_53
2	1567	CDS
			locus_tag	3300020918-1_53_1
			product	capsid protein
			transl_table	11
2468	2277	CDS
			locus_tag	3300020918-1_53_2
			product	hypothetical protein
			transl_table	11
>3300020918-1_54
1138	2	CDS
			locus_tag	3300020918-1_54_1
			product	dynamin family GTPase
			transl_table	11
1536	1216	CDS
			locus_tag	3300020918-1_54_2
			product	hypothetical protein
			transl_table	11
2083	1610	CDS
			locus_tag	3300020918-1_54_3
			product	dynamin family GTPase
			transl_table	11
2207	2085	CDS
			locus_tag	3300020918-1_54_4
			product	hypothetical protein
			transl_table	11
2496	2609	CDS
			locus_tag	3300020918-1_54_5
			product	hypothetical protein
			transl_table	11
>3300020918-1_55
654	1	CDS
			locus_tag	3300020918-1_55_1
			product	hypothetical protein
			transl_table	11
1962	706	CDS
			locus_tag	3300020918-1_55_2
			product	hypothetical protein
			transl_table	11
2117	2509	CDS
			locus_tag	3300020918-1_55_3
			product	hypothetical protein
			transl_table	11
