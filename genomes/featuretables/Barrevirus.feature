>Barrevirus_1
1	798	CDS
			locus_tag	Barrevirus_1_1
			product	PREDICTED: LOW QUALITY PROTEIN: cell division cycle protein 123-like
			transl_table	11
2051	2332	CDS
			locus_tag	Barrevirus_1_2
			product	hypothetical protein
			transl_table	11
2508	3983	CDS
			locus_tag	Barrevirus_1_3
			product	FAD-binding oxidoreductase
			transl_table	11
4036	5784	CDS
			locus_tag	Barrevirus_1_4
			product	hypothetical protein BGO51_01175
			transl_table	11
6219	7748	CDS
			locus_tag	Barrevirus_1_5
			product	hypothetical protein Klosneuvirus_4_106
			transl_table	11
8059	8583	CDS
			locus_tag	Barrevirus_1_6
			product	hypothetical protein
			transl_table	11
8938	9948	CDS
			locus_tag	Barrevirus_1_7
			product	hypothetical protein Klosneuvirus_3_188
			transl_table	11
11567	12115	CDS
			locus_tag	Barrevirus_1_8
			product	hypothetical protein
			transl_table	11
12239	12787	CDS
			locus_tag	Barrevirus_1_9
			product	hypothetical protein
			transl_table	11
13138	12821	CDS
			locus_tag	Barrevirus_1_10
			product	hypothetical protein
			transl_table	11
13337	13453	CDS
			locus_tag	Barrevirus_1_11
			product	hypothetical protein
			transl_table	11
13678	15015	CDS
			locus_tag	Barrevirus_1_12
			product	hypothetical protein Klosneuvirus_5_93
			transl_table	11
16537	15206	CDS
			locus_tag	Barrevirus_1_13
			product	hypothetical protein
			transl_table	11
17268	16585	CDS
			locus_tag	Barrevirus_1_14
			product	hypothetical protein
			transl_table	11
17782	17375	CDS
			locus_tag	Barrevirus_1_15
			product	hypothetical protein
			transl_table	11
18586	18197	CDS
			locus_tag	Barrevirus_1_16
			product	hypothetical protein
			transl_table	11
18983	18639	CDS
			locus_tag	Barrevirus_1_17
			product	hypothetical protein
			transl_table	11
19500	19090	CDS
			locus_tag	Barrevirus_1_18
			product	hypothetical protein
			transl_table	11
19674	21194	CDS
			locus_tag	Barrevirus_1_19
			product	hypothetical protein Klosneuvirus_4_106
			transl_table	11
22323	21748	CDS
			locus_tag	Barrevirus_1_20
			product	hypothetical protein
			transl_table	11
22637	22326	CDS
			locus_tag	Barrevirus_1_21
			product	hypothetical protein
			transl_table	11
23020	22703	CDS
			locus_tag	Barrevirus_1_22
			product	hypothetical protein
			transl_table	11
23574	23413	CDS
			locus_tag	Barrevirus_1_23
			product	hypothetical protein
			transl_table	11
23771	24259	CDS
			locus_tag	Barrevirus_1_24
			product	hypothetical protein
			transl_table	11
25653	24883	CDS
			locus_tag	Barrevirus_1_25
			product	hypothetical protein
			transl_table	11
28518	25771	CDS
			locus_tag	Barrevirus_1_26
			product	multicopper oxidase
			transl_table	11
28571	29596	CDS
			locus_tag	Barrevirus_1_27
			product	phosphoesterase
			transl_table	11
31499	31356	CDS
			locus_tag	Barrevirus_1_28
			product	hypothetical protein
			transl_table	11
31672	33684	CDS
			locus_tag	Barrevirus_1_29
			product	hypothetical protein
			transl_table	11
34428	33829	CDS
			locus_tag	Barrevirus_1_30
			product	hemerythrin
			transl_table	11
34479	34817	CDS
			locus_tag	Barrevirus_1_31
			product	Histidine triad (HIT) protein
			transl_table	11
34901	35509	CDS
			locus_tag	Barrevirus_1_32
			product	SWIM zinc finger protein
			transl_table	11
36269	35631	CDS
			locus_tag	Barrevirus_1_33
			product	F-box domain protein
			transl_table	11
37100	36465	CDS
			locus_tag	Barrevirus_1_34
			product	F-box domain protein
			transl_table	11
37942	37322	CDS
			locus_tag	Barrevirus_1_35
			product	F-box domain protein
			transl_table	11
38656	38039	CDS
			locus_tag	Barrevirus_1_36
			product	F-box domain protein
			transl_table	11
38931	38713	CDS
			locus_tag	Barrevirus_1_37
			product	hypothetical protein
			transl_table	11
39853	39239	CDS
			locus_tag	Barrevirus_1_38
			product	F-box domain protein
			transl_table	11
40837	39971	CDS
			locus_tag	Barrevirus_1_39
			product	hypothetical protein QJ48_07965
			transl_table	11
42043	42237	CDS
			locus_tag	Barrevirus_1_40
			product	hypothetical protein
			transl_table	11
42493	43275	CDS
			locus_tag	Barrevirus_1_41
			product	hypothetical protein
			transl_table	11
45089	43290	CDS
			locus_tag	Barrevirus_1_42
			product	hypothetical protein
			transl_table	11
45556	45362	CDS
			locus_tag	Barrevirus_1_43
			product	hypothetical protein
			transl_table	11
46361	48943	CDS
			locus_tag	Barrevirus_1_44
			product	serine protease
			transl_table	11
48994	49758	CDS
			locus_tag	Barrevirus_1_45
			product	hypothetical protein
			transl_table	11
49812	50696	CDS
			locus_tag	Barrevirus_1_46
			product	hypothetical protein
			transl_table	11
51065	50763	CDS
			locus_tag	Barrevirus_1_47
			product	hypothetical protein
			transl_table	11
51514	51137	CDS
			locus_tag	Barrevirus_1_48
			product	hypothetical protein
			transl_table	11
52074	51571	CDS
			locus_tag	Barrevirus_1_49
			product	hypothetical protein
			transl_table	11
52891	52115	CDS
			locus_tag	Barrevirus_1_50
			product	F-box domain protein
			transl_table	11
53050	53580	CDS
			locus_tag	Barrevirus_1_51
			product	hypothetical protein
			transl_table	11
54230	53718	CDS
			locus_tag	Barrevirus_1_52
			product	hypothetical protein Klosneuvirus_3_10
			transl_table	11
55665	55790	CDS
			locus_tag	Barrevirus_1_53
			product	hypothetical protein
			transl_table	11
56043	56489	CDS
			locus_tag	Barrevirus_1_54
			product	hypothetical protein
			transl_table	11
58048	56486	CDS
			locus_tag	Barrevirus_1_55
			product	RING finger domain protein
			transl_table	11
58272	58367	CDS
			locus_tag	Barrevirus_1_56
			product	hypothetical protein
			transl_table	11
58560	58685	CDS
			locus_tag	Barrevirus_1_57
			product	hypothetical protein
			transl_table	11
58862	59002	CDS
			locus_tag	Barrevirus_1_58
			product	hypothetical protein
			transl_table	11
59559	59077	CDS
			locus_tag	Barrevirus_1_59
			product	endonuclease I
			transl_table	11
62703	59641	CDS
			locus_tag	Barrevirus_1_60
			product	DNA primase
			transl_table	11
62780	63976	CDS
			locus_tag	Barrevirus_1_61
			product	hypothetical protein Indivirus_5_51
			transl_table	11
65509	64700	CDS
			locus_tag	Barrevirus_1_62
			product	hypothetical protein PBRA_002700
			transl_table	11
67120	65993	CDS
			locus_tag	Barrevirus_1_63
			product	hypothetical protein PBRA_002700
			transl_table	11
68323	67193	CDS
			locus_tag	Barrevirus_1_64
			product	hypothetical protein PBRA_002699
			transl_table	11
68484	68951	CDS
			locus_tag	Barrevirus_1_65
			product	hypothetical protein
			transl_table	11
69125	68964	CDS
			locus_tag	Barrevirus_1_66
			product	hypothetical protein
			transl_table	11
69141	69272	CDS
			locus_tag	Barrevirus_1_67
			product	hypothetical protein
			transl_table	11
69688	71277	CDS
			locus_tag	Barrevirus_1_68
			product	HNH endonuclease
			transl_table	11
71514	71365	CDS
			locus_tag	Barrevirus_1_69
			product	hypothetical protein
			transl_table	11
>Barrevirus_2
3	152	CDS
			locus_tag	Barrevirus_2_1
			product	hypothetical protein
			transl_table	11
254	700	CDS
			locus_tag	Barrevirus_2_2
			product	hypothetical protein Indivirus_3_31
			transl_table	11
1142	681	CDS
			locus_tag	Barrevirus_2_3
			product	hypothetical protein Indivirus_3_30
			transl_table	11
2169	1165	CDS
			locus_tag	Barrevirus_2_4
			product	hypothetical protein Indivirus_3_28
			transl_table	11
2252	3019	CDS
			locus_tag	Barrevirus_2_5
			product	hypothetical protein Indivirus_3_27
			transl_table	11
3606	3016	CDS
			locus_tag	Barrevirus_2_6
			product	hypothetical protein Indivirus_3_26
			transl_table	11
3815	3621	CDS
			locus_tag	Barrevirus_2_7
			product	hypothetical protein Indivirus_3_25
			transl_table	11
4317	3850	CDS
			locus_tag	Barrevirus_2_8
			product	hypothetical protein Indivirus_3_24
			transl_table	11
10474	4388	CDS
			locus_tag	Barrevirus_2_9
			product	early transcription factor VETF large subunit
			transl_table	11
11664	11353	CDS
			locus_tag	Barrevirus_2_10
			product	hypothetical protein
			transl_table	11
12309	12190	CDS
			locus_tag	Barrevirus_2_11
			product	hypothetical protein
			transl_table	11
12920	12333	CDS
			locus_tag	Barrevirus_2_12
			product	protein of unknown function DUF45
			transl_table	11
13028	14179	CDS
			locus_tag	Barrevirus_2_13
			product	hypothetical protein Indivirus_3_21
			transl_table	11
18094	14225	CDS
			locus_tag	Barrevirus_2_14
			product	DNA polymerase family B elongation subunit
			transl_table	11
19403	20107	CDS
			locus_tag	Barrevirus_2_15
			product	putative RNA methylase
			transl_table	11
21451	20156	CDS
			locus_tag	Barrevirus_2_16
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
21541	22602	CDS
			locus_tag	Barrevirus_2_17
			product	DNA polymerase family X protein
			transl_table	11
24754	22604	CDS
			locus_tag	Barrevirus_2_18
			product	ankyrin repeat protein
			transl_table	11
27378	24787	CDS
			locus_tag	Barrevirus_2_19
			product	ribonucleoside diphosphate reductase large subunit
			transl_table	11
27497	29668	CDS
			locus_tag	Barrevirus_2_20
			product	hypothetical protein COB52_05240
			transl_table	11
30374	30529	CDS
			locus_tag	Barrevirus_2_21
			product	hypothetical protein
			transl_table	11
31744	30767	CDS
			locus_tag	Barrevirus_2_22
			product	serine/threonine protein kinase
			transl_table	11
31823	32275	CDS
			locus_tag	Barrevirus_2_23
			product	hypothetical protein
			transl_table	11
32310	32477	CDS
			locus_tag	Barrevirus_2_24
			product	hypothetical protein
			transl_table	11
32482	32595	CDS
			locus_tag	Barrevirus_2_25
			product	hypothetical protein
			transl_table	11
33830	32601	CDS
			locus_tag	Barrevirus_2_26
			product	tubulin-tyrosine ligase family protein
			transl_table	11
34529	33873	CDS
			locus_tag	Barrevirus_2_27
			product	hypothetical protein Klosneuvirus_2_296
			transl_table	11
34545	35147	CDS
			locus_tag	Barrevirus_2_28
			product	hypothetical protein Indivirus_3_9
			transl_table	11
35466	36893	CDS
			locus_tag	Barrevirus_2_29
			product	hypothetical protein Indivirus_3_8
			transl_table	11
37754	36951	CDS
			locus_tag	Barrevirus_2_30
			product	serine/threonine protein phosphatase
			transl_table	11
38210	37806	CDS
			locus_tag	Barrevirus_2_31
			product	hypothetical protein
			transl_table	11
39675	38386	CDS
			locus_tag	Barrevirus_2_32
			product	eukaryotic translation initiation factor 2 gamma subunit
			transl_table	11
40061	39744	CDS
			locus_tag	Barrevirus_2_33
			product	hypothetical protein
			transl_table	11
>Barrevirus_3
1760	30	CDS
			locus_tag	Barrevirus_3_1
			product	hypothetical protein Klosneuvirus_1_400
			transl_table	11
1906	2490	CDS
			locus_tag	Barrevirus_3_2
			product	hypothetical protein
			transl_table	11
2938	4311	CDS
			locus_tag	Barrevirus_3_3
			product	hypothetical protein Indivirus_1_206
			transl_table	11
4370	4753	CDS
			locus_tag	Barrevirus_3_4
			product	hypothetical protein Indivirus_1_205
			transl_table	11
5233	4739	CDS
			locus_tag	Barrevirus_3_5
			product	phosphoribosyl-ATP pyrophosphohydrolase
			transl_table	11
6059	5310	CDS
			locus_tag	Barrevirus_3_6
			product	thymidine kinase
			transl_table	11
6926	6141	CDS
			locus_tag	Barrevirus_3_7
			product	hypothetical protein
			transl_table	11
7033	7482	CDS
			locus_tag	Barrevirus_3_8
			product	hypothetical protein Klosneuvirus_1_379
			transl_table	11
7783	7451	CDS
			locus_tag	Barrevirus_3_9
			product	hypothetical protein Indivirus_1_201
			transl_table	11
7849	8859	CDS
			locus_tag	Barrevirus_3_10
			product	hypothetical protein Indivirus_1_200
			transl_table	11
8889	9674	CDS
			locus_tag	Barrevirus_3_11
			product	hypothetical protein Indivirus_1_199
			transl_table	11
9705	10799	CDS
			locus_tag	Barrevirus_3_12
			product	hypothetical protein Indivirus_1_197
			transl_table	11
11489	10845	CDS
			locus_tag	Barrevirus_3_13
			product	hypothetical protein Indivirus_1_196
			transl_table	11
11641	12918	CDS
			locus_tag	Barrevirus_3_14
			product	AAA family ATPase
			transl_table	11
13310	12915	CDS
			locus_tag	Barrevirus_3_15
			product	hypothetical protein
			transl_table	11
14967	13372	CDS
			locus_tag	Barrevirus_3_16
			product	hypothetical protein Indivirus_1_192
			transl_table	11
17243	18193	CDS
			locus_tag	Barrevirus_3_17
			product	hypothetical protein Indivirus_1_191
			transl_table	11
18543	18133	CDS
			locus_tag	Barrevirus_3_18
			product	hypothetical protein
			transl_table	11
19392	18607	CDS
			locus_tag	Barrevirus_3_19
			product	zinc-finger and zinc-ribbon domain protein
			transl_table	11
20297	19410	CDS
			locus_tag	Barrevirus_3_20
			product	protein-tyrosine phosphatase
			transl_table	11
21155	20352	CDS
			locus_tag	Barrevirus_3_21
			product	hypothetical protein Indivirus_1_189
			transl_table	11
21263	23221	CDS
			locus_tag	Barrevirus_3_22
			product	dynamin family protein
			transl_table	11
24805	23738	CDS
			locus_tag	Barrevirus_3_23
			product	hypothetical protein Indivirus_1_183
			transl_table	11
24901	26073	CDS
			locus_tag	Barrevirus_3_24
			product	hypothetical protein Indivirus_1_182
			transl_table	11
27056	26070	CDS
			locus_tag	Barrevirus_3_25
			product	hypothetical protein Indivirus_1_181
			transl_table	11
27131	27232	CDS
			locus_tag	Barrevirus_3_26
			product	hypothetical protein
			transl_table	11
28122	28277	CDS
			locus_tag	Barrevirus_3_27
			product	hypothetical protein
			transl_table	11
29008	29112	CDS
			locus_tag	Barrevirus_3_28
			product	hypothetical protein
			transl_table	11
29202	29681	CDS
			locus_tag	Barrevirus_3_29
			product	hypothetical protein Indivirus_1_180
			transl_table	11
29980	29678	CDS
			locus_tag	Barrevirus_3_30
			product	translation initiation factor 1
			transl_table	11
31326	30073	CDS
			locus_tag	Barrevirus_3_31
			product	hypothetical protein Indivirus_1_178
			transl_table	11
31419	32279	CDS
			locus_tag	Barrevirus_3_32
			product	hypothetical protein Indivirus_1_177
			transl_table	11
34378	32282	CDS
			locus_tag	Barrevirus_3_33
			product	hypothetical protein Indivirus_1_175
			transl_table	11
35242	34442	CDS
			locus_tag	Barrevirus_3_34
			product	hypothetical protein Indivirus_1_173
			transl_table	11
35307	36188	CDS
			locus_tag	Barrevirus_3_35
			product	uridine kinase
			transl_table	11
36531	36193	CDS
			locus_tag	Barrevirus_3_36
			product	hypothetical protein Indivirus_1_171
			transl_table	11
>Barrevirus_4
2167	2097	tRNA	Barrevirus_tRNA_1
			product	tRNA-Arg(TCT)
1029	1	CDS
			locus_tag	Barrevirus_4_1
			product	hypothetical protein Indivirus_1_130
			transl_table	11
1215	2066	CDS
			locus_tag	Barrevirus_4_2
			product	hypothetical protein Indivirus_1_129
			transl_table	11
3856	2219	CDS
			locus_tag	Barrevirus_4_3
			product	hypothetical protein COT73_02000
			transl_table	11
4127	4636	CDS
			locus_tag	Barrevirus_4_4
			product	hypothetical protein
			transl_table	11
7006	4751	CDS
			locus_tag	Barrevirus_4_5
			product	AAA family ATPase
			transl_table	11
7139	8551	CDS
			locus_tag	Barrevirus_4_6
			product	hypothetical protein Indivirus_1_126
			transl_table	11
9322	8555	CDS
			locus_tag	Barrevirus_4_7
			product	hypothetical protein Indivirus_1_125
			transl_table	11
10656	9379	CDS
			locus_tag	Barrevirus_4_8
			product	hypothetical protein MegaChil _gp1010
			transl_table	11
10774	11997	CDS
			locus_tag	Barrevirus_4_9
			product	serine/threonine protein kinase
			transl_table	11
12346	12032	CDS
			locus_tag	Barrevirus_4_10
			product	hypothetical protein
			transl_table	11
14296	14718	CDS
			locus_tag	Barrevirus_4_11
			product	hypothetical protein
			transl_table	11
17002	14711	CDS
			locus_tag	Barrevirus_4_12
			product	ADP-ribosyltransferase exoenzyme domain protein
			transl_table	11
17451	17035	CDS
			locus_tag	Barrevirus_4_13
			product	hypothetical protein Indivirus_1_121
			transl_table	11
17884	17558	CDS
			locus_tag	Barrevirus_4_14
			product	hypothetical protein Indivirus_1_120
			transl_table	11
18423	17995	CDS
			locus_tag	Barrevirus_4_15
			product	hypothetical protein
			transl_table	11
18852	18577	CDS
			locus_tag	Barrevirus_4_16
			product	hypothetical protein
			transl_table	11
22590	19762	CDS
			locus_tag	Barrevirus_4_17
			product	D5-like helicase-primase
			transl_table	11
22822	23406	CDS
			locus_tag	Barrevirus_4_18
			product	DNA-dependent RNA polymerase subunit Rpb9
			transl_table	11
23513	24217	CDS
			locus_tag	Barrevirus_4_19
			product	DNA-directed RNA polymerase subunit 6
			transl_table	11
25150	24227	CDS
			locus_tag	Barrevirus_4_20
			product	patatin-like phospholipase family protein
			transl_table	11
28168	25217	CDS
			locus_tag	Barrevirus_4_21
			product	DNA topoisomerase IA
			transl_table	11
29212	29898	CDS
			locus_tag	Barrevirus_4_22
			product	BTB/POZ domain protein
			transl_table	11
29983	30618	CDS
			locus_tag	Barrevirus_4_23
			product	DNA-directed RNA polymerase subunit 5
			transl_table	11
31154	30615	CDS
			locus_tag	Barrevirus_4_24
			product	hypothetical protein Indivirus_1_110
			transl_table	11
33307	31178	CDS
			locus_tag	Barrevirus_4_25
			product	hypothetical protein Indivirus_1_109
			transl_table	11
>Barrevirus_5
138	1361	CDS
			locus_tag	Barrevirus_5_1
			product	ribonuclease III
			transl_table	11
3110	1410	CDS
			locus_tag	Barrevirus_5_2
			product	HNH endonuclease
			transl_table	11
3189	5522	CDS
			locus_tag	Barrevirus_5_3
			product	DEAD/SNF2-like helicase
			transl_table	11
6069	6830	CDS
			locus_tag	Barrevirus_5_4
			product	hypothetical protein Indivirus_3_42
			transl_table	11
7678	6827	CDS
			locus_tag	Barrevirus_5_5
			product	hypothetical protein Indivirus_3_43
			transl_table	11
7756	10119	CDS
			locus_tag	Barrevirus_5_6
			product	putative minor capsid protein
			transl_table	11
11078	10143	CDS
			locus_tag	Barrevirus_5_7
			product	Ulp1 protease
			transl_table	11
11460	11083	CDS
			locus_tag	Barrevirus_5_8
			product	hypothetical protein Indivirus_3_46
			transl_table	11
14842	12716	CDS
			locus_tag	Barrevirus_5_9
			product	YqaJ-like viral recombinase domain
			transl_table	11
14985	15239	CDS
			locus_tag	Barrevirus_5_10
			product	hypothetical protein
			transl_table	11
16613	18007	CDS
			locus_tag	Barrevirus_5_11
			product	GTP binding translation elongation factor
			transl_table	11
18280	18065	CDS
			locus_tag	Barrevirus_5_12
			product	hypothetical protein Indivirus_3_53
			transl_table	11
18793	18338	CDS
			locus_tag	Barrevirus_5_13
			product	hypothetical protein Indivirus_3_54
			transl_table	11
18831	19655	CDS
			locus_tag	Barrevirus_5_14
			product	RING finger protein
			transl_table	11
19748	20245	CDS
			locus_tag	Barrevirus_5_15
			product	hypothetical protein Indivirus_3_56
			transl_table	11
20287	20898	CDS
			locus_tag	Barrevirus_5_16
			product	hypothetical protein Indivirus_3_57
			transl_table	11
20977	21942	CDS
			locus_tag	Barrevirus_5_17
			product	translation initiation factor eIF-2 alpha subunit
			transl_table	11
24848	21894	CDS
			locus_tag	Barrevirus_5_18
			product	DNA mismatch repair ATPase MutS
			transl_table	11
25659	24916	CDS
			locus_tag	Barrevirus_5_19
			product	hypothetical protein Indivirus_3_61
			transl_table	11
26449	25685	CDS
			locus_tag	Barrevirus_5_20
			product	hypothetical protein Indivirus_3_62
			transl_table	11
27901	26477	CDS
			locus_tag	Barrevirus_5_21
			product	DEAD/SNF2-like helicase
			transl_table	11
>Barrevirus_6
1	441	CDS
			locus_tag	Barrevirus_6_1
			product	hypothetical protein Catovirus_2_144
			transl_table	11
1673	450	CDS
			locus_tag	Barrevirus_6_2
			product	hypothetical protein
			transl_table	11
2025	1771	CDS
			locus_tag	Barrevirus_6_3
			product	hypothetical protein
			transl_table	11
2166	2726	CDS
			locus_tag	Barrevirus_6_4
			product	hypothetical protein
			transl_table	11
3119	2715	CDS
			locus_tag	Barrevirus_6_5
			product	hypothetical protein
			transl_table	11
3253	4023	CDS
			locus_tag	Barrevirus_6_6
			product	hypothetical protein Indivirus_11_16
			transl_table	11
4656	4243	CDS
			locus_tag	Barrevirus_6_7
			product	hypothetical protein
			transl_table	11
4744	5298	CDS
			locus_tag	Barrevirus_6_8
			product	hypothetical protein
			transl_table	11
5724	5308	CDS
			locus_tag	Barrevirus_6_9
			product	hypothetical protein
			transl_table	11
5780	6211	CDS
			locus_tag	Barrevirus_6_10
			product	NUDIX hydrolase
			transl_table	11
6666	6190	CDS
			locus_tag	Barrevirus_6_11
			product	hypothetical protein
			transl_table	11
7184	6735	CDS
			locus_tag	Barrevirus_6_12
			product	hypothetical protein
			transl_table	11
7876	7259	CDS
			locus_tag	Barrevirus_6_13
			product	hypothetical protein
			transl_table	11
8618	7944	CDS
			locus_tag	Barrevirus_6_14
			product	hypothetical protein
			transl_table	11
8765	9952	CDS
			locus_tag	Barrevirus_6_15
			product	hypothetical protein
			transl_table	11
10220	10029	CDS
			locus_tag	Barrevirus_6_16
			product	hypothetical protein
			transl_table	11
12104	11886	CDS
			locus_tag	Barrevirus_6_17
			product	hypothetical protein MegaChil _gp0124
			transl_table	11
12401	12195	CDS
			locus_tag	Barrevirus_6_18
			product	hypothetical protein
			transl_table	11
12536	12721	CDS
			locus_tag	Barrevirus_6_19
			product	hypothetical protein
			transl_table	11
12699	14231	CDS
			locus_tag	Barrevirus_6_20
			product	hypothetical protein Klosneuvirus_3_274
			transl_table	11
14305	15831	CDS
			locus_tag	Barrevirus_6_21
			product	hypothetical protein
			transl_table	11
16667	15849	CDS
			locus_tag	Barrevirus_6_22
			product	DNA polymerase family B elongation subunit
			transl_table	11
19489	16742	CDS
			locus_tag	Barrevirus_6_23
			product	calcineurin-like phosphoesterase
			transl_table	11
20841	19546	CDS
			locus_tag	Barrevirus_6_24
			product	methyltransferase domain
			transl_table	11
22069	20930	CDS
			locus_tag	Barrevirus_6_25
			product	AAA family ATPase
			transl_table	11
22206	22892	CDS
			locus_tag	Barrevirus_6_26
			product	nicotinamide/nicotinic acid mononucleotide adenylyltransferase 1-like isoform X2
			transl_table	11
23786	22881	CDS
			locus_tag	Barrevirus_6_27
			product	serine/threonine protein kinase
			transl_table	11
24416	23847	CDS
			locus_tag	Barrevirus_6_28
			product	hypothetical protein
			transl_table	11
24900	24460	CDS
			locus_tag	Barrevirus_6_29
			product	hypothetical protein Indivirus_7_18
			transl_table	11
25264	24908	CDS
			locus_tag	Barrevirus_6_30
			product	Archaea-specific enzyme related to ProFAR isomerase
			transl_table	11
26246	25344	CDS
			locus_tag	Barrevirus_6_31
			product	hypothetical protein PPERSA_09713
			transl_table	11
>Barrevirus_7
1237	2	CDS
			locus_tag	Barrevirus_7_1
			product	UvrD/REP helicase family protein
			transl_table	11
1689	1294	CDS
			locus_tag	Barrevirus_7_2
			product	UvrD/REP helicase family protein
			transl_table	11
1771	3063	CDS
			locus_tag	Barrevirus_7_3
			product	HNH endonuclease
			transl_table	11
5492	3165	CDS
			locus_tag	Barrevirus_7_4
			product	DEAD/SNF2-like helicase
			transl_table	11
5832	5728	CDS
			locus_tag	Barrevirus_7_5
			product	hypothetical protein
			transl_table	11
6934	7329	CDS
			locus_tag	Barrevirus_7_6
			product	hypothetical protein
			transl_table	11
7470	7931	CDS
			locus_tag	Barrevirus_7_7
			product	hypothetical protein Indivirus_2_79
			transl_table	11
8675	8881	CDS
			locus_tag	Barrevirus_7_8
			product	hypothetical protein
			transl_table	11
8909	9970	CDS
			locus_tag	Barrevirus_7_9
			product	metallopeptidase family M24
			transl_table	11
10215	9976	CDS
			locus_tag	Barrevirus_7_10
			product	hypothetical protein
			transl_table	11
10501	12975	CDS
			locus_tag	Barrevirus_7_11
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
13431	13015	CDS
			locus_tag	Barrevirus_7_12
			product	translation initiation factor 2 subunit beta
			transl_table	11
14776	16080	CDS
			locus_tag	Barrevirus_7_13
			product	DHH family phosphohydrolase
			transl_table	11
16092	17573	CDS
			locus_tag	Barrevirus_7_14
			product	hypothetical protein
			transl_table	11
17612	18739	CDS
			locus_tag	Barrevirus_7_15
			product	hypothetical protein
			transl_table	11
18798	19172	CDS
			locus_tag	Barrevirus_7_16
			product	hypothetical protein Indivirus_2_85
			transl_table	11
19618	19202	CDS
			locus_tag	Barrevirus_7_17
			product	Erv1/Alr family disulfide thiol oxidoreductase
			transl_table	11
19736	20221	CDS
			locus_tag	Barrevirus_7_18
			product	helix-turn-helix protein
			transl_table	11
21040	20225	CDS
			locus_tag	Barrevirus_7_19
			product	hypothetical protein Indivirus_2_87
			transl_table	11
21128	21916	CDS
			locus_tag	Barrevirus_7_20
			product	dual specificity phosphatase
			transl_table	11
22743	22111	CDS
			locus_tag	Barrevirus_7_21
			product	hypothetical protein
			transl_table	11
>Barrevirus_8
2	3688	CDS
			locus_tag	Barrevirus_8_1
			product	flagellar basal-body rod protein FlgG
			transl_table	11
4129	4254	CDS
			locus_tag	Barrevirus_8_2
			product	hypothetical protein
			transl_table	11
4817	4470	CDS
			locus_tag	Barrevirus_8_3
			product	hypothetical protein Indivirus_2_61
			transl_table	11
5118	4807	CDS
			locus_tag	Barrevirus_8_4
			product	hypothetical protein Indivirus_2_60
			transl_table	11
6179	5256	CDS
			locus_tag	Barrevirus_8_5
			product	hypothetical protein Indivirus_2_59
			transl_table	11
8804	6258	CDS
			locus_tag	Barrevirus_8_6
			product	hypothetical protein Indivirus_2_58
			transl_table	11
10512	9376	CDS
			locus_tag	Barrevirus_8_7
			product	hypothetical protein
			transl_table	11
11521	11033	CDS
			locus_tag	Barrevirus_8_8
			product	hypothetical protein Indivirus_2_55
			transl_table	11
12743	11511	CDS
			locus_tag	Barrevirus_8_9
			product	exodeoxyribonuclease VII large subunit
			transl_table	11
14176	12812	CDS
			locus_tag	Barrevirus_8_10
			product	hypothetical protein Klosneuvirus_2_73
			transl_table	11
16576	18933	CDS
			locus_tag	Barrevirus_8_11
			product	phosphoenolpyruvate synthase
			transl_table	11
18983	19936	CDS
			locus_tag	Barrevirus_8_12
			product	formamidopyrimidine-DNA glycosylase
			transl_table	11
20557	19943	CDS
			locus_tag	Barrevirus_8_13
			product	hypothetical protein Indivirus_2_47
			transl_table	11
20812	20603	CDS
			locus_tag	Barrevirus_8_14
			product	hypothetical protein
			transl_table	11
20879	21718	CDS
			locus_tag	Barrevirus_8_15
			product	hypothetical protein Indivirus_2_46
			transl_table	11
22351	21725	CDS
			locus_tag	Barrevirus_8_16
			product	hypothetical protein Indivirus_2_45
			transl_table	11
22470	22712	CDS
			locus_tag	Barrevirus_8_17
			product	hypothetical protein
			transl_table	11
>Barrevirus_9
13	1014	CDS
			locus_tag	Barrevirus_9_1
			product	hypothetical protein Indivirus_1_132
			transl_table	11
1076	1420	CDS
			locus_tag	Barrevirus_9_2
			product	hypothetical protein
			transl_table	11
1514	2179	CDS
			locus_tag	Barrevirus_9_3
			product	hypothetical protein Indivirus_1_134
			transl_table	11
2268	3869	CDS
			locus_tag	Barrevirus_9_4
			product	hypothetical protein BV898_15177
			transl_table	11
4532	4909	CDS
			locus_tag	Barrevirus_9_5
			product	translation initiation factor 2 subunit beta
			transl_table	11
4927	6207	CDS
			locus_tag	Barrevirus_9_6
			product	AAA family ATPase
			transl_table	11
6275	6562	CDS
			locus_tag	Barrevirus_9_7
			product	hypothetical protein
			transl_table	11
8567	6609	CDS
			locus_tag	Barrevirus_9_8
			product	NAD-dependent DNA ligase
			transl_table	11
8669	9145	CDS
			locus_tag	Barrevirus_9_9
			product	hypothetical protein Indivirus_1_139
			transl_table	11
9193	10005	CDS
			locus_tag	Barrevirus_9_10
			product	hypothetical protein Indivirus_1_140
			transl_table	11
10070	10831	CDS
			locus_tag	Barrevirus_9_11
			product	Clp protease
			transl_table	11
11893	11735	CDS
			locus_tag	Barrevirus_9_12
			product	hypothetical protein
			transl_table	11
12684	12250	CDS
			locus_tag	Barrevirus_9_13
			product	hypothetical protein Indivirus_1_143
			transl_table	11
12720	13052	CDS
			locus_tag	Barrevirus_9_14
			product	protein of unknown function DUF814
			transl_table	11
13116	13640	CDS
			locus_tag	Barrevirus_9_15
			product	hypothetical protein
			transl_table	11
13719	14519	CDS
			locus_tag	Barrevirus_9_16
			product	endonuclease/exonuclease/phosphatase family protein
			transl_table	11
14562	16280	CDS
			locus_tag	Barrevirus_9_17
			product	hypothetical protein Indivirus_1_147
			transl_table	11
17315	16272	CDS
			locus_tag	Barrevirus_9_18
			product	hypothetical protein Indivirus_1_148
			transl_table	11
18186	17359	CDS
			locus_tag	Barrevirus_9_19
			product	hypothetical protein
			transl_table	11
18687	18304	CDS
			locus_tag	Barrevirus_9_20
			product	hypothetical protein
			transl_table	11
19386	19243	CDS
			locus_tag	Barrevirus_9_21
			product	hypothetical protein
			transl_table	11
19516	19400	CDS
			locus_tag	Barrevirus_9_22
			product	hypothetical protein
			transl_table	11
20638	20886	CDS
			locus_tag	Barrevirus_9_23
			product	hypothetical protein Indivirus_7_23
			transl_table	11
>Barrevirus_10
2	1306	CDS
			locus_tag	Barrevirus_10_1
			product	ring and ubiquitin domain
			transl_table	11
1369	3594	CDS
			locus_tag	Barrevirus_10_2
			product	subtilase family serine protease
			transl_table	11
5222	3639	CDS
			locus_tag	Barrevirus_10_3
			product	DUF2726 domain-containing protein
			transl_table	11
5896	5306	CDS
			locus_tag	Barrevirus_10_4
			product	hypothetical protein Klosneuvirus_5_67
			transl_table	11
6640	6362	CDS
			locus_tag	Barrevirus_10_5
			product	hypothetical protein
			transl_table	11
8243	6744	CDS
			locus_tag	Barrevirus_10_6
			product	SCP-like extracellular protein domain
			transl_table	11
8693	8502	CDS
			locus_tag	Barrevirus_10_7
			product	hypothetical protein
			transl_table	11
9164	8850	CDS
			locus_tag	Barrevirus_10_8
			product	hypothetical protein
			transl_table	11
9571	9236	CDS
			locus_tag	Barrevirus_10_9
			product	hypothetical protein
			transl_table	11
10012	12936	CDS
			locus_tag	Barrevirus_10_10
			product	structural ppiase-like protein
			transl_table	11
13007	13879	CDS
			locus_tag	Barrevirus_10_11
			product	hypothetical protein Indivirus_5_45
			transl_table	11
14507	13908	CDS
			locus_tag	Barrevirus_10_12
			product	hypothetical protein
			transl_table	11
14666	15085	CDS
			locus_tag	Barrevirus_10_13
			product	hypothetical protein
			transl_table	11
16245	15124	CDS
			locus_tag	Barrevirus_10_14
			product	DnaJ domain protein
			transl_table	11
16900	17844	CDS
			locus_tag	Barrevirus_10_15
			product	hypothetical protein Indivirus_5_43
			transl_table	11
17935	18057	CDS
			locus_tag	Barrevirus_10_16
			product	hypothetical protein
			transl_table	11
>Barrevirus_11
2	310	CDS
			locus_tag	Barrevirus_11_1
			product	hypothetical protein
			transl_table	11
1299	457	CDS
			locus_tag	Barrevirus_11_2
			product	hypothetical protein
			transl_table	11
2202	1360	CDS
			locus_tag	Barrevirus_11_3
			product	hypothetical protein
			transl_table	11
2350	3339	CDS
			locus_tag	Barrevirus_11_4
			product	hypothetical protein
			transl_table	11
3425	4420	CDS
			locus_tag	Barrevirus_11_5
			product	hypothetical protein
			transl_table	11
4499	4747	CDS
			locus_tag	Barrevirus_11_6
			product	hypothetical protein
			transl_table	11
4840	5391	CDS
			locus_tag	Barrevirus_11_7
			product	hypothetical protein
			transl_table	11
6890	5388	CDS
			locus_tag	Barrevirus_11_8
			product	hypothetical protein
			transl_table	11
8471	6978	CDS
			locus_tag	Barrevirus_11_9
			product	hypothetical protein
			transl_table	11
8650	8847	CDS
			locus_tag	Barrevirus_11_10
			product	hypothetical protein
			transl_table	11
8979	8848	CDS
			locus_tag	Barrevirus_11_11
			product	hypothetical protein
			transl_table	11
9848	9027	CDS
			locus_tag	Barrevirus_11_12
			product	hypothetical protein
			transl_table	11
11642	12010	CDS
			locus_tag	Barrevirus_11_13
			product	hypothetical protein
			transl_table	11
12652	12053	CDS
			locus_tag	Barrevirus_11_14
			product	hypothetical protein
			transl_table	11
13739	12744	CDS
			locus_tag	Barrevirus_11_15
			product	hypothetical protein
			transl_table	11
13857	14348	CDS
			locus_tag	Barrevirus_11_16
			product	hypothetical protein
			transl_table	11
14469	14672	CDS
			locus_tag	Barrevirus_11_17
			product	hypothetical protein
			transl_table	11
15181	14735	CDS
			locus_tag	Barrevirus_11_18
			product	hypothetical protein
			transl_table	11
15969	15457	CDS
			locus_tag	Barrevirus_11_19
			product	hypothetical protein
			transl_table	11
16324	16046	CDS
			locus_tag	Barrevirus_11_20
			product	hypothetical protein
			transl_table	11
17432	16422	CDS
			locus_tag	Barrevirus_11_21
			product	hypothetical protein
			transl_table	11
>Barrevirus_12
143	3	CDS
			locus_tag	Barrevirus_12_1
			product	hypothetical protein
			transl_table	11
767	1375	CDS
			locus_tag	Barrevirus_12_2
			product	hypothetical protein
			transl_table	11
1442	1972	CDS
			locus_tag	Barrevirus_12_3
			product	hypothetical protein
			transl_table	11
2118	2315	CDS
			locus_tag	Barrevirus_12_4
			product	hypothetical protein
			transl_table	11
2442	3233	CDS
			locus_tag	Barrevirus_12_5
			product	hypothetical protein ASPWEDRAFT_172752
			transl_table	11
3327	4742	CDS
			locus_tag	Barrevirus_12_6
			product	hypothetical protein
			transl_table	11
5159	4755	CDS
			locus_tag	Barrevirus_12_7
			product	hypothetical protein
			transl_table	11
7294	5309	CDS
			locus_tag	Barrevirus_12_8
			product	hypothetical protein PPERSA_04634
			transl_table	11
7580	7338	CDS
			locus_tag	Barrevirus_12_9
			product	hypothetical protein
			transl_table	11
8097	7612	CDS
			locus_tag	Barrevirus_12_10
			product	PREDICTED: store-operated calcium entry-associated regulatory factor isoform X2
			transl_table	11
8726	8286	CDS
			locus_tag	Barrevirus_12_11
			product	hypothetical protein
			transl_table	11
9107	8832	CDS
			locus_tag	Barrevirus_12_12
			product	hypothetical protein
			transl_table	11
9333	9935	CDS
			locus_tag	Barrevirus_12_13
			product	carbonic anhydrase family protein
			transl_table	11
10061	10543	CDS
			locus_tag	Barrevirus_12_14
			product	hypothetical protein
			transl_table	11
11167	10610	CDS
			locus_tag	Barrevirus_12_15
			product	hypothetical protein
			transl_table	11
11276	11590	CDS
			locus_tag	Barrevirus_12_16
			product	hypothetical protein
			transl_table	11
11708	12967	CDS
			locus_tag	Barrevirus_12_17
			product	HNH endonuclease
			transl_table	11
14384	15145	CDS
			locus_tag	Barrevirus_12_18
			product	hypothetical protein
			transl_table	11
15853	15662	CDS
			locus_tag	Barrevirus_12_19
			product	hypothetical protein
			transl_table	11
17060	15906	CDS
			locus_tag	Barrevirus_12_20
			product	hypothetical protein
			transl_table	11
>Barrevirus_13
3	602	CDS
			locus_tag	Barrevirus_13_1
			product	hypothetical protein Indivirus_2_103
			transl_table	11
984	604	CDS
			locus_tag	Barrevirus_13_2
			product	hypothetical protein Indivirus_2_104
			transl_table	11
2418	1081	CDS
			locus_tag	Barrevirus_13_3
			product	hypothetical protein Indivirus_1_31
			transl_table	11
3226	2384	CDS
			locus_tag	Barrevirus_13_4
			product	hypothetical protein Indivirus_1_31
			transl_table	11
3577	4011	CDS
			locus_tag	Barrevirus_13_5
			product	HTH and integrase core domain protein
			transl_table	11
4221	4310	CDS
			locus_tag	Barrevirus_13_6
			product	hypothetical protein
			transl_table	11
4619	5428	CDS
			locus_tag	Barrevirus_13_7
			product	aminotransferase class-V
			transl_table	11
5998	5423	CDS
			locus_tag	Barrevirus_13_8
			product	hypothetical protein
			transl_table	11
8689	6050	CDS
			locus_tag	Barrevirus_13_9
			product	valine--tRNA ligase
			transl_table	11
8808	9101	CDS
			locus_tag	Barrevirus_13_10
			product	hypothetical protein Indivirus_2_110
			transl_table	11
9132	9386	CDS
			locus_tag	Barrevirus_13_11
			product	hypothetical protein
			transl_table	11
10366	9383	CDS
			locus_tag	Barrevirus_13_12
			product	GDP-mannose 4,6 dehydratase
			transl_table	11
10467	11024	CDS
			locus_tag	Barrevirus_13_13
			product	hypothetical protein
			transl_table	11
11061	12212	CDS
			locus_tag	Barrevirus_13_14
			product	hypothetical protein
			transl_table	11
12326	13513	CDS
			locus_tag	Barrevirus_13_15
			product	hypothetical protein
			transl_table	11
13549	14259	CDS
			locus_tag	Barrevirus_13_16
			product	hypothetical protein Indivirus_2_111
			transl_table	11
14317	14916	CDS
			locus_tag	Barrevirus_13_17
			product	hypothetical protein
			transl_table	11
14937	16151	CDS
			locus_tag	Barrevirus_13_18
			product	hypothetical protein
			transl_table	11
16421	16146	CDS
			locus_tag	Barrevirus_13_19
			product	dTDP-4-dehydrorhamnose reductase
			transl_table	11
16930	16418	CDS
			locus_tag	Barrevirus_13_20
			product	dTDP-4-dehydrorhamnose reductase
			transl_table	11
>Barrevirus_14
2	319	CDS
			locus_tag	Barrevirus_14_1
			product	RING finger domain protein
			transl_table	11
376	1443	CDS
			locus_tag	Barrevirus_14_2
			product	hypothetical protein
			transl_table	11
1500	2228	CDS
			locus_tag	Barrevirus_14_3
			product	hypothetical protein
			transl_table	11
2285	2503	CDS
			locus_tag	Barrevirus_14_4
			product	hypothetical protein
			transl_table	11
2481	3080	CDS
			locus_tag	Barrevirus_14_5
			product	hypothetical protein
			transl_table	11
3140	3682	CDS
			locus_tag	Barrevirus_14_6
			product	hypothetical protein
			transl_table	11
5276	3687	CDS
			locus_tag	Barrevirus_14_7
			product	GMC family oxidoreductase
			transl_table	11
5333	5683	CDS
			locus_tag	Barrevirus_14_8
			product	hypothetical protein
			transl_table	11
6395	6198	CDS
			locus_tag	Barrevirus_14_9
			product	hypothetical protein
			transl_table	11
6857	7183	CDS
			locus_tag	Barrevirus_14_10
			product	hypothetical protein Indivirus_6_10
			transl_table	11
7183	7737	CDS
			locus_tag	Barrevirus_14_11
			product	hypothetical protein
			transl_table	11
7819	9036	CDS
			locus_tag	Barrevirus_14_12
			product	hypothetical protein
			transl_table	11
9125	10393	CDS
			locus_tag	Barrevirus_14_13
			product	AAA family ATPase
			transl_table	11
11238	10390	CDS
			locus_tag	Barrevirus_14_14
			product	hypothetical protein
			transl_table	11
11868	11659	CDS
			locus_tag	Barrevirus_14_15
			product	hypothetical protein
			transl_table	11
12161	11940	CDS
			locus_tag	Barrevirus_14_16
			product	hypothetical protein
			transl_table	11
12626	12955	CDS
			locus_tag	Barrevirus_14_17
			product	hypothetical protein
			transl_table	11
13486	12989	CDS
			locus_tag	Barrevirus_14_18
			product	hypothetical protein
			transl_table	11
14164	13607	CDS
			locus_tag	Barrevirus_14_19
			product	hypothetical protein
			transl_table	11
15281	14415	CDS
			locus_tag	Barrevirus_14_20
			product	hypothetical protein
			transl_table	11
15494	15312	CDS
			locus_tag	Barrevirus_14_21
			product	hypothetical protein
			transl_table	11
>Barrevirus_15
1281	265	CDS
			locus_tag	Barrevirus_15_1
			product	DnaJ domain protein
			transl_table	11
1370	2587	CDS
			locus_tag	Barrevirus_15_2
			product	ADP-ribosylglycohydrolase
			transl_table	11
3046	2588	CDS
			locus_tag	Barrevirus_15_3
			product	thioredoxin
			transl_table	11
3156	5273	CDS
			locus_tag	Barrevirus_15_4
			product	hypothetical protein Indivirus_1_4
			transl_table	11
6805	5318	CDS
			locus_tag	Barrevirus_15_5
			product	NCLDV major capsid protein
			transl_table	11
7375	7506	CDS
			locus_tag	Barrevirus_15_6
			product	hypothetical protein
			transl_table	11
10420	7814	CDS
			locus_tag	Barrevirus_15_7
			product	NCLDV major capsid protein
			transl_table	11
12276	10522	CDS
			locus_tag	Barrevirus_15_8
			product	NCLDV major capsid protein
			transl_table	11
12729	12307	CDS
			locus_tag	Barrevirus_15_9
			product	hypothetical protein
			transl_table	11
12961	12845	CDS
			locus_tag	Barrevirus_15_10
			product	hypothetical protein
			transl_table	11
>Barrevirus_16
97	873	CDS
			locus_tag	Barrevirus_16_1
			product	HNH endonuclease
			transl_table	11
956	1435	CDS
			locus_tag	Barrevirus_16_2
			product	HNH endonuclease
			transl_table	11
1484	2956	CDS
			locus_tag	Barrevirus_16_3
			product	polyADP-ribose polymerase
			transl_table	11
4229	2958	CDS
			locus_tag	Barrevirus_16_4
			product	hypothetical protein UT82_C0021G0009
			transl_table	11
4813	4577	CDS
			locus_tag	Barrevirus_16_5
			product	hypothetical protein
			transl_table	11
4907	5275	CDS
			locus_tag	Barrevirus_16_6
			product	polyADP-ribose polymerase
			transl_table	11
8315	5838	CDS
			locus_tag	Barrevirus_16_7
			product	glycosyltransferase
			transl_table	11
8649	8903	CDS
			locus_tag	Barrevirus_16_8
			product	hypothetical protein
			transl_table	11
8946	9248	CDS
			locus_tag	Barrevirus_16_9
			product	hypothetical protein
			transl_table	11
9314	9676	CDS
			locus_tag	Barrevirus_16_10
			product	thioredoxin
			transl_table	11
9718	11874	CDS
			locus_tag	Barrevirus_16_11
			product	HrpA-like RNA helicase
			transl_table	11
11912	12307	CDS
			locus_tag	Barrevirus_16_12
			product	autophagy protein Atg8 ubiquitin-like protein
			transl_table	11
>Barrevirus_17
2	418	CDS
			locus_tag	Barrevirus_17_1
			product	hypothetical protein
			transl_table	11
1610	507	CDS
			locus_tag	Barrevirus_17_2
			product	PREDICTED: uncharacterized protein LOC106804774
			transl_table	11
1748	2581	CDS
			locus_tag	Barrevirus_17_3
			product	hypothetical protein
			transl_table	11
2641	3543	CDS
			locus_tag	Barrevirus_17_4
			product	hypothetical protein
			transl_table	11
4509	3790	CDS
			locus_tag	Barrevirus_17_5
			product	hypothetical protein
			transl_table	11
5958	4609	CDS
			locus_tag	Barrevirus_17_6
			product	hypothetical protein Klosneuvirus_1_286
			transl_table	11
6661	6056	CDS
			locus_tag	Barrevirus_17_7
			product	putative family 25 glycosyltransferase
			transl_table	11
6737	7528	CDS
			locus_tag	Barrevirus_17_8
			product	HtrL protein family-containing protein
			transl_table	11
8844	7534	CDS
			locus_tag	Barrevirus_17_9
			product	hypothetical protein
			transl_table	11
11136	9886	CDS
			locus_tag	Barrevirus_17_10
			product	IS4 family transposase
			transl_table	11
12010	11816	CDS
			locus_tag	Barrevirus_17_11
			product	hypothetical protein
			transl_table	11
12319	12203	CDS
			locus_tag	Barrevirus_17_12
			product	hypothetical protein
			transl_table	11
>Barrevirus_18
229	35	CDS
			locus_tag	Barrevirus_18_1
			product	hypothetical protein
			transl_table	11
640	311	CDS
			locus_tag	Barrevirus_18_2
			product	hypothetical protein
			transl_table	11
1185	628	CDS
			locus_tag	Barrevirus_18_3
			product	hypothetical protein
			transl_table	11
2270	1230	CDS
			locus_tag	Barrevirus_18_4
			product	hypothetical protein
			transl_table	11
2713	2399	CDS
			locus_tag	Barrevirus_18_5
			product	hypothetical protein
			transl_table	11
2707	2865	CDS
			locus_tag	Barrevirus_18_6
			product	hypothetical protein
			transl_table	11
3595	3428	CDS
			locus_tag	Barrevirus_18_7
			product	hypothetical protein
			transl_table	11
4065	3697	CDS
			locus_tag	Barrevirus_18_8
			product	hypothetical protein Indivirus_4_14
			transl_table	11
4725	4135	CDS
			locus_tag	Barrevirus_18_9
			product	SCP-like extracellular protein domain
			transl_table	11
6415	4814	CDS
			locus_tag	Barrevirus_18_10
			product	hypothetical protein
			transl_table	11
7133	6462	CDS
			locus_tag	Barrevirus_18_11
			product	SCP-like extracellular protein domain
			transl_table	11
8351	7272	CDS
			locus_tag	Barrevirus_18_12
			product	hypothetical protein
			transl_table	11
8350	8682	CDS
			locus_tag	Barrevirus_18_13
			product	hypothetical protein
			transl_table	11
9220	8744	CDS
			locus_tag	Barrevirus_18_14
			product	hypothetical protein
			transl_table	11
9341	10045	CDS
			locus_tag	Barrevirus_18_15
			product	hypothetical protein
			transl_table	11
10164	10283	CDS
			locus_tag	Barrevirus_18_16
			product	hypothetical protein
			transl_table	11
10490	10368	CDS
			locus_tag	Barrevirus_18_17
			product	hypothetical protein
			transl_table	11
10825	11037	CDS
			locus_tag	Barrevirus_18_18
			product	hypothetical protein
			transl_table	11
11580	11257	CDS
			locus_tag	Barrevirus_18_19
			product	hypothetical protein
			transl_table	11
>Barrevirus_19
588	1	CDS
			locus_tag	Barrevirus_19_1
			product	hypothetical protein
			transl_table	11
726	637	CDS
			locus_tag	Barrevirus_19_2
			product	hypothetical protein
			transl_table	11
963	1931	CDS
			locus_tag	Barrevirus_19_3
			product	hypothetical protein
			transl_table	11
1921	2049	CDS
			locus_tag	Barrevirus_19_4
			product	hypothetical protein
			transl_table	11
3291	2242	CDS
			locus_tag	Barrevirus_19_5
			product	hypothetical protein
			transl_table	11
4419	3373	CDS
			locus_tag	Barrevirus_19_6
			product	hypothetical protein TTHERM_01169380
			transl_table	11
4936	5322	CDS
			locus_tag	Barrevirus_19_7
			product	hypothetical protein
			transl_table	11
5441	6193	CDS
			locus_tag	Barrevirus_19_8
			product	hypothetical protein
			transl_table	11
6371	6195	CDS
			locus_tag	Barrevirus_19_9
			product	hypothetical protein
			transl_table	11
7990	6536	CDS
			locus_tag	Barrevirus_19_10
			product	hypothetical protein
			transl_table	11
8682	8551	CDS
			locus_tag	Barrevirus_19_11
			product	hypothetical protein
			transl_table	11
8662	9414	CDS
			locus_tag	Barrevirus_19_12
			product	cytidylyltransferase
			transl_table	11
9411	9764	CDS
			locus_tag	Barrevirus_19_13
			product	hypothetical protein
			transl_table	11
10030	9788	CDS
			locus_tag	Barrevirus_19_14
			product	hypothetical protein
			transl_table	11
10343	10098	CDS
			locus_tag	Barrevirus_19_15
			product	hypothetical protein
			transl_table	11
10495	10980	CDS
			locus_tag	Barrevirus_19_16
			product	hypothetical protein
			transl_table	11
11371	10994	CDS
			locus_tag	Barrevirus_19_17
			product	hypothetical protein
			transl_table	11
>Barrevirus_20
2	1603	CDS
			locus_tag	Barrevirus_20_1
			product	hypothetical protein Indivirus_4_44
			transl_table	11
1630	3612	CDS
			locus_tag	Barrevirus_20_2
			product	hypothetical protein Indivirus_4_43
			transl_table	11
3645	4235	CDS
			locus_tag	Barrevirus_20_3
			product	hypothetical protein Indivirus_4_42
			transl_table	11
7490	4602	CDS
			locus_tag	Barrevirus_20_4
			product	hypothetical protein BWG23_11685
			transl_table	11
9438	7600	CDS
			locus_tag	Barrevirus_20_5
			product	P4B major core protein
			transl_table	11
11023	9545	CDS
			locus_tag	Barrevirus_20_6
			product	replication factor C large subunit
			transl_table	11
>Barrevirus_21
1	3759	CDS
			locus_tag	Barrevirus_21_1
			product	SNF2-like helicase
			transl_table	11
4082	4450	CDS
			locus_tag	Barrevirus_21_2
			product	hypothetical protein Indivirus_3_70
			transl_table	11
4681	5301	CDS
			locus_tag	Barrevirus_21_3
			product	DNA-directed RNA polymerase, subunit E'/Rpb8
			transl_table	11
5366	6442	CDS
			locus_tag	Barrevirus_21_4
			product	NUDIX hydrolase
			transl_table	11
6603	8702	CDS
			locus_tag	Barrevirus_21_5
			product	ankyrin repeat protein
			transl_table	11
9187	8699	CDS
			locus_tag	Barrevirus_21_6
			product	disulfide thiol oxidoreductase, Erv1 / Alr family
			transl_table	11
>Barrevirus_22
347	2149	CDS
			locus_tag	Barrevirus_22_1
			product	NCLDV major capsid protein
			transl_table	11
2836	2285	CDS
			locus_tag	Barrevirus_22_2
			product	hypothetical protein Indivirus_4_16
			transl_table	11
4353	2881	CDS
			locus_tag	Barrevirus_22_3
			product	hypothetical protein Indivirus_4_15
			transl_table	11
5431	4370	CDS
			locus_tag	Barrevirus_22_4
			product	hypothetical protein Klosneuvirus_1_39
			transl_table	11
5835	5539	CDS
			locus_tag	Barrevirus_22_5
			product	hypothetical protein Indivirus_4_15
			transl_table	11
6184	5924	CDS
			locus_tag	Barrevirus_22_6
			product	hypothetical protein
			transl_table	11
7218	6439	CDS
			locus_tag	Barrevirus_22_7
			product	hypothetical protein
			transl_table	11
7620	7279	CDS
			locus_tag	Barrevirus_22_8
			product	hypothetical protein VHEMI00625
			transl_table	11
9500	7704	CDS
			locus_tag	Barrevirus_22_9
			product	hypothetical protein
			transl_table	11
>Barrevirus_23
1199	2128	CDS
			locus_tag	Barrevirus_23_1
			product	hypothetical protein Indivirus_1_86
			transl_table	11
3020	2142	CDS
			locus_tag	Barrevirus_23_2
			product	DnaJ domain protein
			transl_table	11
3114	4046	CDS
			locus_tag	Barrevirus_23_3
			product	hypothetical protein Indivirus_1_88
			transl_table	11
4098	6434	CDS
			locus_tag	Barrevirus_23_4
			product	Hsp70 protein
			transl_table	11
6568	7167	CDS
			locus_tag	Barrevirus_23_5
			product	hypothetical protein Indivirus_1_84
			transl_table	11
7671	7228	CDS
			locus_tag	Barrevirus_23_6
			product	hypothetical protein Indivirus_1_80
			transl_table	11
9073	7727	CDS
			locus_tag	Barrevirus_23_7
			product	DnaJ domain protein
			transl_table	11
>Barrevirus_24
2	445	CDS
			locus_tag	Barrevirus_24_1
			product	hypothetical protein
			transl_table	11
1062	1199	CDS
			locus_tag	Barrevirus_24_2
			product	hypothetical protein
			transl_table	11
2768	4030	CDS
			locus_tag	Barrevirus_24_3
			product	hypothetical protein
			transl_table	11
4932	4033	CDS
			locus_tag	Barrevirus_24_4
			product	Protein kinase, catalytic domain-containing protein
			transl_table	11
7331	4992	CDS
			locus_tag	Barrevirus_24_5
			product	hypothetical protein F441_19114
			transl_table	11
8403	7411	CDS
			locus_tag	Barrevirus_24_6
			product	hypothetical protein
			transl_table	11
8951	8475	CDS
			locus_tag	Barrevirus_24_7
			product	hypothetical protein
			transl_table	11
>Barrevirus_25
3	827	CDS
			locus_tag	Barrevirus_25_1
			product	hypothetical protein Klosneuvirus_3_17
			transl_table	11
929	2182	CDS
			locus_tag	Barrevirus_25_2
			product	hypothetical protein
			transl_table	11
2242	2577	CDS
			locus_tag	Barrevirus_25_3
			product	hypothetical protein
			transl_table	11
2645	3133	CDS
			locus_tag	Barrevirus_25_4
			product	hypothetical protein
			transl_table	11
3264	4046	CDS
			locus_tag	Barrevirus_25_5
			product	hypothetical protein
			transl_table	11
4130	5242	CDS
			locus_tag	Barrevirus_25_6
			product	DUF4419 domain-containing protein
			transl_table	11
5221	5997	CDS
			locus_tag	Barrevirus_25_7
			product	hypothetical protein
			transl_table	11
6419	6009	CDS
			locus_tag	Barrevirus_25_8
			product	hypothetical protein
			transl_table	11
6650	7093	CDS
			locus_tag	Barrevirus_25_9
			product	hypothetical protein
			transl_table	11
8558	7389	CDS
			locus_tag	Barrevirus_25_10
			product	hypothetical protein IK1_05988
			transl_table	11
>Barrevirus_26
2509	2	CDS
			locus_tag	Barrevirus_26_1
			product	hypothetical protein
			transl_table	11
3797	4381	CDS
			locus_tag	Barrevirus_26_2
			product	hypothetical protein
			transl_table	11
4545	4997	CDS
			locus_tag	Barrevirus_26_3
			product	vitamin K epoxide reductase complex subunit 1-like protein 1
			transl_table	11
6250	5021	CDS
			locus_tag	Barrevirus_26_4
			product	desaturase
			transl_table	11
6423	7208	CDS
			locus_tag	Barrevirus_26_5
			product	hypothetical protein
			transl_table	11
7911	7237	CDS
			locus_tag	Barrevirus_26_6
			product	START domain containing protein
			transl_table	11
>Barrevirus_27
1	819	CDS
			locus_tag	Barrevirus_27_1
			product	hypothetical protein
			transl_table	11
1041	814	CDS
			locus_tag	Barrevirus_27_2
			product	hypothetical protein
			transl_table	11
1170	2342	CDS
			locus_tag	Barrevirus_27_3
			product	UV-endonuclease UvdE
			transl_table	11
3282	3106	CDS
			locus_tag	Barrevirus_27_4
			product	hypothetical protein
			transl_table	11
3925	4893	CDS
			locus_tag	Barrevirus_27_5
			product	helix-hairpin-helix repeat-containing competence protein ComEA
			transl_table	11
5844	4900	CDS
			locus_tag	Barrevirus_27_6
			product	hypothetical protein
			transl_table	11
6808	5945	CDS
			locus_tag	Barrevirus_27_7
			product	hypothetical protein Klosneuvirus_3_10
			transl_table	11
7073	7270	CDS
			locus_tag	Barrevirus_27_8
			product	hypothetical protein
			transl_table	11
7711	7869	CDS
			locus_tag	Barrevirus_27_9
			product	hypothetical protein
			transl_table	11
>Barrevirus_28
1	387	CDS
			locus_tag	Barrevirus_28_1
			product	hypothetical protein
			transl_table	11
470	2347	CDS
			locus_tag	Barrevirus_28_2
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
2370	6167	CDS
			locus_tag	Barrevirus_28_3
			product	DNA-directed RNA polymerase subunit beta
			transl_table	11
6285	6503	CDS
			locus_tag	Barrevirus_28_4
			product	hypothetical protein
			transl_table	11
7117	7569	CDS
			locus_tag	Barrevirus_28_5
			product	hypothetical protein
			transl_table	11
>Barrevirus_29
93	302	CDS
			locus_tag	Barrevirus_29_1
			product	hypothetical protein
			transl_table	11
305	544	CDS
			locus_tag	Barrevirus_29_2
			product	hypothetical protein
			transl_table	11
1182	574	CDS
			locus_tag	Barrevirus_29_3
			product	hypothetical protein
			transl_table	11
1564	1253	CDS
			locus_tag	Barrevirus_29_4
			product	hypothetical protein
			transl_table	11
1717	2421	CDS
			locus_tag	Barrevirus_29_5
			product	hypothetical protein
			transl_table	11
2409	3251	CDS
			locus_tag	Barrevirus_29_6
			product	hypothetical protein
			transl_table	11
3388	5661	CDS
			locus_tag	Barrevirus_29_7
			product	bifunctional AAA family ATPase chaperone/translocase BCS1
			transl_table	11
5780	6883	CDS
			locus_tag	Barrevirus_29_8
			product	hypothetical protein Klosneuvirus_7_10
			transl_table	11
>Barrevirus_30
367	5412	CDS
			locus_tag	Barrevirus_30_1
			product	HrpA-like RNA helicase
			transl_table	11
5481	6986	CDS
			locus_tag	Barrevirus_30_2
			product	hypothetical protein Indivirus_3_65
			transl_table	11
>Barrevirus_31
3	602	CDS
			locus_tag	Barrevirus_31_1
			product	hypothetical protein Indivirus_2_65
			transl_table	11
652	1005	CDS
			locus_tag	Barrevirus_31_2
			product	hypothetical protein Indivirus_2_66
			transl_table	11
1034	2002	CDS
			locus_tag	Barrevirus_31_3
			product	hypothetical protein Indivirus_2_67
			transl_table	11
2060	2926	CDS
			locus_tag	Barrevirus_31_4
			product	ubiquitin carboxyl-terminal hydrolase
			transl_table	11
2992	3870	CDS
			locus_tag	Barrevirus_31_5
			product	hypothetical protein Indivirus_5_51
			transl_table	11
4113	4874	CDS
			locus_tag	Barrevirus_31_6
			product	hypothetical protein Indivirus_7_23
			transl_table	11
6886	6767	CDS
			locus_tag	Barrevirus_31_7
			product	hypothetical protein
			transl_table	11
>Barrevirus_32
3	269	CDS
			locus_tag	Barrevirus_32_1
			product	hypothetical protein
			transl_table	11
598	275	CDS
			locus_tag	Barrevirus_32_2
			product	hypothetical protein
			transl_table	11
1347	772	CDS
			locus_tag	Barrevirus_32_3
			product	hypothetical protein
			transl_table	11
2604	1528	CDS
			locus_tag	Barrevirus_32_4
			product	hypothetical protein
			transl_table	11
4109	2601	CDS
			locus_tag	Barrevirus_32_5
			product	hypothetical protein
			transl_table	11
4892	5032	CDS
			locus_tag	Barrevirus_32_6
			product	hypothetical protein
			transl_table	11
>Barrevirus_33
3	2114	CDS
			locus_tag	Barrevirus_33_1
			product	peptidase
			transl_table	11
2848	2111	CDS
			locus_tag	Barrevirus_33_2
			product	ubiquitin family protein
			transl_table	11
2889	4079	CDS
			locus_tag	Barrevirus_33_3
			product	arginase family protein
			transl_table	11
4536	4228	CDS
			locus_tag	Barrevirus_33_4
			product	hypothetical protein
			transl_table	11
4781	5470	CDS
			locus_tag	Barrevirus_33_5
			product	hypothetical protein
			transl_table	11
>Barrevirus_34
1	747	CDS
			locus_tag	Barrevirus_34_1
			product	hypothetical protein
			transl_table	11
782	1603	CDS
			locus_tag	Barrevirus_34_2
			product	hypothetical protein
			transl_table	11
1670	2542	CDS
			locus_tag	Barrevirus_34_3
			product	RING finger domain protein
			transl_table	11
2600	4015	CDS
			locus_tag	Barrevirus_34_4
			product	hypothetical protein
			transl_table	11
4441	4016	CDS
			locus_tag	Barrevirus_34_5
			product	Hypothetical protein ORPV_1102
			transl_table	11
5244	5354	CDS
			locus_tag	Barrevirus_34_6
			product	hypothetical protein
			transl_table	11
>Barrevirus_35
3	851	CDS
			locus_tag	Barrevirus_35_1
			product	ubiquitin-conjugating enzyme E2
			transl_table	11
1332	886	CDS
			locus_tag	Barrevirus_35_2
			product	hypothetical protein
			transl_table	11
1417	1581	CDS
			locus_tag	Barrevirus_35_3
			product	hypothetical protein
			transl_table	11
3711	1576	CDS
			locus_tag	Barrevirus_35_4
			product	phosphoinositide 3-kinase
			transl_table	11
4313	3765	CDS
			locus_tag	Barrevirus_35_5
			product	isochorismatase
			transl_table	11
4727	4395	CDS
			locus_tag	Barrevirus_35_6
			product	hypothetical protein Indivirus_1_243
			transl_table	11
5060	4767	CDS
			locus_tag	Barrevirus_35_7
			product	hypothetical protein
			transl_table	11
>Barrevirus_36
17	1615	CDS
			locus_tag	Barrevirus_36_1
			product	NCLDV major capsid protein
			transl_table	11
2809	2033	CDS
			locus_tag	Barrevirus_36_2
			product	hypothetical protein Indivirus_4_9
			transl_table	11
3062	4057	CDS
			locus_tag	Barrevirus_36_3
			product	hypothetical protein
			transl_table	11
>Barrevirus_37
2060	687	CDS
			locus_tag	Barrevirus_37_1
			product	NCLDV major capsid protein
			transl_table	11
1982	2077	CDS
			locus_tag	Barrevirus_37_2
			product	hypothetical protein
			transl_table	11
2272	2051	CDS
			locus_tag	Barrevirus_37_3
			product	NCLDV major capsid protein
			transl_table	11
2582	2313	CDS
			locus_tag	Barrevirus_37_4
			product	hypothetical protein
			transl_table	11
2724	2623	CDS
			locus_tag	Barrevirus_37_5
			product	hypothetical protein
			transl_table	11
3407	3694	CDS
			locus_tag	Barrevirus_37_6
			product	hypothetical protein Indivirus_4_20
			transl_table	11
3888	4112	CDS
			locus_tag	Barrevirus_37_7
			product	GIY-YIG catalytic domain-containing endonuclease
			transl_table	11
4223	4107	CDS
			locus_tag	Barrevirus_37_8
			product	hypothetical protein
			transl_table	11
>Barrevirus_38
3	416	CDS
			locus_tag	Barrevirus_38_1
			product	cupin domain-containing protein
			transl_table	11
2139	727	CDS
			locus_tag	Barrevirus_38_2
			product	hypothetical protein CBC13_03140
			transl_table	11
2903	2685	CDS
			locus_tag	Barrevirus_38_3
			product	hypothetical protein CK425_01860
			transl_table	11
>Barrevirus_39
3	146	CDS
			locus_tag	Barrevirus_39_1
			product	hypothetical protein
			transl_table	11
230	1768	CDS
			locus_tag	Barrevirus_39_2
			product	hypothetical protein
			transl_table	11
3235	2993	CDS
			locus_tag	Barrevirus_39_3
			product	hypothetical protein
			transl_table	11
3234	3437	CDS
			locus_tag	Barrevirus_39_4
			product	hypothetical protein
			transl_table	11
>Barrevirus_40
97	885	CDS
			locus_tag	Barrevirus_40_1
			product	hypothetical protein Indivirus_1_13
			transl_table	11
1386	886	CDS
			locus_tag	Barrevirus_40_2
			product	hypothetical protein Indivirus_1_9
			transl_table	11
1440	2429	CDS
			locus_tag	Barrevirus_40_3
			product	patatin-like phospholipase
			transl_table	11
>Barrevirus_41
540	1	CDS
			locus_tag	Barrevirus_41_1
			product	hypothetical protein Indivirus_1_237
			transl_table	11
1214	594	CDS
			locus_tag	Barrevirus_41_2
			product	myristylated IMV envelope protein
			transl_table	11
1261	2316	CDS
			locus_tag	Barrevirus_41_3
			product	myristylated protein
			transl_table	11
2952	2278	CDS
			locus_tag	Barrevirus_41_4
			product	UTP--glucose-1-phosphate uridylyltransferase
			transl_table	11
>Barrevirus_42
820	2	CDS
			locus_tag	Barrevirus_42_1
			product	hypothetical protein
			transl_table	11
2340	889	CDS
			locus_tag	Barrevirus_42_2
			product	R3H domain protein
			transl_table	11
2610	2428	CDS
			locus_tag	Barrevirus_42_3
			product	hypothetical protein
			transl_table	11
2921	2763	CDS
			locus_tag	Barrevirus_42_4
			product	hypothetical protein
			transl_table	11
>Barrevirus_43
3	185	CDS
			locus_tag	Barrevirus_43_1
			product	hypothetical protein
			transl_table	11
191	487	CDS
			locus_tag	Barrevirus_43_2
			product	hypothetical protein Indivirus_9_14
			transl_table	11
633	956	CDS
			locus_tag	Barrevirus_43_3
			product	hypothetical protein
			transl_table	11
1023	1304	CDS
			locus_tag	Barrevirus_43_4
			product	hypothetical protein
			transl_table	11
2474	1527	CDS
			locus_tag	Barrevirus_43_5
			product	hypothetical protein
			transl_table	11
>Barrevirus_44
508	2	CDS
			locus_tag	Barrevirus_44_1
			product	polyprenyl synthetase
			transl_table	11
944	552	CDS
			locus_tag	Barrevirus_44_2
			product	hypothetical protein Klosneuvirus_4_44
			transl_table	11
2553	1078	CDS
			locus_tag	Barrevirus_44_3
			product	bifunctional phosphoribosyl-AMP cyclohydrolase/phosphoribosyl-ATP pyrophosphatase protein
			transl_table	11
